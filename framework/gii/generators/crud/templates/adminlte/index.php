<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $dataProvider CActiveDataProvider */

<?php
$label = $this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	'$label',
);\n";
?>

$this->menu=array(
array('label'=>'Create <?php echo $this->modelClass; ?>', 'url'=>array('create')),
array('label'=>'Manage <?php echo $this->modelClass; ?>', 'url'=>array('admin')),
);
?>

<div class="box-header">
    <h3 class="box-title"><?php echo $label; ?></h3>
    <div class="box-tools">
        <a class="btn btn-default pull-right" href="<?php echo "<?php"; ?> echo $this->createUrl('create'); ?>">Create <?php echo $this->modelClass; ?></a>
    </div>
</div>

<div class="box-body table-responsive">
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <?php foreach ($this->tableSchema->columns as $column) { ?>
                    <?php
                    if ($column->autoIncrement)
                        continue;
                    ?>
                    <th><?php echo "<?php"; ?> echo <?php echo $this->modelClass; ?>::model()->getAttributeLabel('<?php echo $column->name; ?>') ?></th>
                <?php } ?>
                    <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php echo "<?php"; ?> foreach($dataProvider as $item) { ?>
            <tr>
                <?php foreach ($this->tableSchema->columns as $column) { ?>
                    <?php
                    if ($column->autoIncrement)
                        continue;
                    ?>
                    <td><?php echo "<?php"; ?> echo $item-><?php echo $column->name ?>; ?></td>
                <?php } ?>
                    <td>
                        
                        <div class="btn-group">
                            <a class="btn btn-primary" style="padding-top: 0px;padding-bottom: 0px;" href="<?php echo "<?php"; ?> echo $this->createUrl('view', array('id' => $item->id)); ?>">View</a>
                            <button type="button" class="btn btn-primary dropdown-toggle" style="padding-top: 0px;padding-bottom: 0px;" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php echo "<?php"; ?> echo $this->createUrl('update', array('id' => $item->id)); ?>">Update</a></li>
                                <li class="divider"></li>
                                <li><a href="<?php echo "<?php"; ?> echo $this->createUrl('delete', array('id' => $item->id)); ?>">Delete</a></li>
                            </ul>
                        </div>
                        
                    </td>
            </tr>
            <?php echo "<?php"; ?> } ?>
        </tbody>
    </table>
</div>