var app = require('express')();
var fs = require("fs");
var server = require('http').Server(app);
//var io = require('socket.io')(server);
var io = require('socket.io').listen(100);
var ioredis = require('socket.io-redis'); //Adapter
var url = require('url'); 
var redisURL = process.env.REDISCLOUD_URL;

var redis = require('redis'),
// pub = redis.createClient(),
// sub = redis.createClient(),
 client = redis.createClient();

var redisOptions = {
//  pubClient: pub,
//  subClient: sub,
  host: "localhost",
  port: 6379
};

io.adapter(ioredis(process.env.REDISTOGO_URL));

var users = [];
var socket_array = [];
//server.listen(100);

app.get('/', function (req, res) {
  res.sendfile(__dirname + '/index.html');
});



io.on('connection', function (socket) {


//socket.broadcast.emit('broadcast','broadcast');
//  socket.emit('news', "my notification");
//  socket.on('my other event', function (data) {
//    console.log(data);
//  });

  console.log("->  Socket Connected.");

  socket.on('set_user_id', function(data){
      //disconnect socket
  socket.on('disconnect', function(){
      var index = users[data.id].indexOf(socket);
      if (index > -1) {
        users[data.id].splice(index, 1);
      }
    console.log('user disconnected');
      console.log(users[data.id].length+' sockets connected of this user');
  });
      console.log("-> New Socket Session - User ID = "+data.id);
      
      if(data.id in users){
//       socket_array = users[data.id];
        users[data.id].push(socket);
        //users[data.id] = socket_array;
//       console.log(users[data.id]); 
      }
      else{
          users[data.id] = [];
          users[data.id].push(socket);
      }
     if(client.connected){
         client.on("error", function (err) {
            console.log("Error " + err);
        });
        console.log(users[data.id].length+' sockets connected of this user');
     client.hset(data.id, socket.id, "some other value", redis.print);
     client.hkeys(data.id, function (err, replies) {
//    console.log(replies.length + " replies:");
    replies.forEach(function (reply, i) {
//        if(data.id === reply.id){
//        console.log(users[data.id]);
    
//        console.log("    " + i + ": " + reply);
//        console.log(socket.id);
//    }
    });
});
//        console.log(users[data.id].length);
     }
//     console.log(socket.id);
  });
  
  
  socket.on('server_notification', function(data){
      
      
       console.log("-> Notification Received From Server = "+data.msg);
       console.log("-> Notification Received From Server For = "+data.id);
      
       client.on("error", function (err) {
            console.log("Error " + err);
        });
     users[data.id].forEach(function(p_socket, i){
//         console.log(p_socket);
         console.log("-> Message Sent to "+data.id);
        p_socket.emit('private',{msg:data.msg,url:data.url,logo:data.logo,count:data.count});
    });
  });
  
  
});

console.log("Socket.io Started.......");