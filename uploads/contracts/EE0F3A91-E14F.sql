/*
SQLyog Ultimate v11.33 (32 bit)
MySQL - 10.1.9-MariaDB : Database - coffipad
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`coffipad` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `coffipad`;

/*Table structure for table `cp_ads` */

DROP TABLE IF EXISTS `cp_ads`;

CREATE TABLE `cp_ads` (
  `id` bigint(50) NOT NULL,
  `top_banner` tinyint(4) DEFAULT NULL,
  `status` enum('active','in-active') DEFAULT 'in-active',
  `is_dedicated` enum('active','in-active') DEFAULT 'in-active',
  `location` enum('one','two','three','top') DEFAULT 'one',
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `offer_id` bigint(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cp_ads_ibfk_1` (`created_by`),
  CONSTRAINT `cp_ads_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `cp_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cp_ads` */

insert  into `cp_ads`(`id`,`top_banner`,`status`,`is_dedicated`,`location`,`created_at`,`created_by`,`offer_id`) values (1,1,'active','active','three',NULL,NULL,0),(2,0,'in-active','active','two',NULL,NULL,0);

/*Table structure for table `cp_business` */

DROP TABLE IF EXISTS `cp_business`;

CREATE TABLE `cp_business` (
  `id` bigint(50) NOT NULL,
  `title` varchar(512) NOT NULL,
  `type_id` bigint(50) NOT NULL,
  `parent_id` bigint(50) DEFAULT NULL,
  `sort` bigint(30) DEFAULT NULL,
  `status` enum('active','in-active') DEFAULT 'active',
  `image` varchar(512) DEFAULT NULL,
  `country_id` bigint(50) NOT NULL,
  `state` varchar(512) DEFAULT NULL,
  `city` varchar(512) DEFAULT NULL,
  `postal_code` varchar(512) DEFAULT NULL,
  `address` varchar(512) DEFAULT NULL,
  `description` text,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cp_business_ibfk_1` (`created_by`),
  KEY `cp_business_ibfk_2` (`type_id`),
  KEY `cp_business_ibfk_3` (`country_id`),
  CONSTRAINT `cp_business_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `cp_user` (`id`),
  CONSTRAINT `cp_business_ibfk_2` FOREIGN KEY (`type_id`) REFERENCES `cp_business_type` (`id`),
  CONSTRAINT `cp_business_ibfk_3` FOREIGN KEY (`country_id`) REFERENCES `cp_country` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cp_business` */

insert  into `cp_business`(`id`,`title`,`type_id`,`parent_id`,`sort`,`status`,`image`,`country_id`,`state`,`city`,`postal_code`,`address`,`description`,`latitude`,`longitude`,`created_at`,`created_by`) values (1,'Gloria Jeanz',3,NULL,NULL,'in-active','',2,'Sindh','Karachi','112233','abc, streetfdg','abc...xyz\r\nasdadasd',120.366,-423.366,NULL,1),(2,'Melt Cafe',4,11115011,NULL,'active','',3,'Sindh','Karachi234234','78456','abc, street34234','abc...xyz',12.3856,-43.3658,NULL,1),(3,'GJ',2,1,NULL,'active',NULL,3,'Sindh','Karachi','112233','abc, street','abc...xyz',12.3856,-43.3658,NULL,1),(6341995,'again',4,11115011,NULL,'active','',3,'sdaed','dsafa','34','ewe654','sdasd',NULL,NULL,1461683780,22993139),(11115011,'Hello World',3,NULL,NULL,'active','',2,'','','','','',NULL,NULL,NULL,22993139),(22357127,'Abdul Ghafoor ltd',4,NULL,NULL,'active','',3,'asdasd','adasdas','423423','23423423','fddsfsdf',NULL,NULL,1461826236,24987430),(38889320,'business new',2,NULL,NULL,'active','',1,'dslkj','lkdj','2938479','saldkj','sdfsdf',NULL,NULL,1461690157,22993139),(59901134,'new sub',3,1,NULL,'active','',2,'sldjh','asldkj','234234','asdasjh','dasd',NULL,NULL,1461683680,22993139);

/*Table structure for table `cp_business_devices` */

DROP TABLE IF EXISTS `cp_business_devices`;

CREATE TABLE `cp_business_devices` (
  `id` bigint(50) NOT NULL,
  `business_id` bigint(50) NOT NULL,
  `device_id` bigint(50) NOT NULL,
  `sort` bigint(30) DEFAULT NULL,
  `status` enum('active','in-active') DEFAULT 'active',
  `device_status` enum('online','offline') DEFAULT 'online',
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cp_business_devices_ibfk_1` (`created_by`),
  KEY `cp_business_devices_ibfk_3` (`business_id`),
  KEY `cp_business_dev_ibfk1` (`device_id`),
  CONSTRAINT `cp_business_dev_ibfk1` FOREIGN KEY (`device_id`) REFERENCES `cp_devices` (`id`),
  CONSTRAINT `cp_business_devices_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `cp_user` (`id`),
  CONSTRAINT `cp_business_devices_ibfk_3` FOREIGN KEY (`business_id`) REFERENCES `cp_business` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cp_business_devices` */

insert  into `cp_business_devices`(`id`,`business_id`,`device_id`,`sort`,`status`,`device_status`,`latitude`,`longitude`,`created_at`,`created_by`) values (1,2,1,NULL,'active','online',NULL,NULL,1231231231,1),(2,11115011,2,NULL,'active','online',NULL,NULL,NULL,1),(3,3,3,NULL,'active','online',NULL,NULL,NULL,1),(15642665,6341995,31936341,1,'active','online',NULL,NULL,1461762972,22993139),(48008725,59901134,23654785,1,'active','online',NULL,NULL,1461764031,22993139);

/*Table structure for table `cp_business_feeds` */

DROP TABLE IF EXISTS `cp_business_feeds`;

CREATE TABLE `cp_business_feeds` (
  `id` bigint(50) NOT NULL,
  `business_id` bigint(50) NOT NULL,
  `feeds_id` bigint(50) NOT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_business_feeds` (`business_id`),
  KEY `fk_feeds` (`feeds_id`),
  CONSTRAINT `fk_business_feeds` FOREIGN KEY (`business_id`) REFERENCES `cp_business` (`id`),
  CONSTRAINT `fk_feeds` FOREIGN KEY (`feeds_id`) REFERENCES `cp_feeds` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cp_business_feeds` */

insert  into `cp_business_feeds`(`id`,`business_id`,`feeds_id`,`created_at`,`created_by`) values (1,2,3,NULL,1),(3,2,2,NULL,1),(4,3,4,NULL,4),(5,2,1,NULL,4),(6,2,4,NULL,4);

/*Table structure for table `cp_business_keyword` */

DROP TABLE IF EXISTS `cp_business_keyword`;

CREATE TABLE `cp_business_keyword` (
  `id` bigint(50) NOT NULL,
  `business_id` bigint(50) NOT NULL,
  `keyword_id` bigint(50) NOT NULL,
  `sort` bigint(30) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cp_business_keyword_ibfk_1` (`created_by`),
  KEY `cp_business_keyword_ibfk_2` (`keyword_id`),
  KEY `cp_business_keyword_ibfk_3` (`business_id`),
  CONSTRAINT `cp_business_keyword_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `cp_user` (`id`),
  CONSTRAINT `cp_business_keyword_ibfk_2` FOREIGN KEY (`keyword_id`) REFERENCES `cp_keywords` (`id`),
  CONSTRAINT `cp_business_keyword_ibfk_3` FOREIGN KEY (`business_id`) REFERENCES `cp_business` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cp_business_keyword` */

insert  into `cp_business_keyword`(`id`,`business_id`,`keyword_id`,`sort`,`created_at`,`created_by`) values (1,2,2,NULL,NULL,NULL),(2,1,5,NULL,NULL,NULL),(3,2,4,NULL,NULL,NULL),(4,1,2,NULL,NULL,NULL),(5,1,1,NULL,NULL,NULL);

/*Table structure for table `cp_business_social` */

DROP TABLE IF EXISTS `cp_business_social`;

CREATE TABLE `cp_business_social` (
  `id` bigint(50) NOT NULL,
  `business_id` bigint(50) NOT NULL,
  `social_id` bigint(50) NOT NULL,
  `status` enum('active','in-active') DEFAULT 'active',
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_business` (`business_id`),
  KEY `FK_social` (`social_id`),
  CONSTRAINT `FK_business` FOREIGN KEY (`business_id`) REFERENCES `cp_business` (`id`),
  CONSTRAINT `FK_social` FOREIGN KEY (`social_id`) REFERENCES `cp_social` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cp_business_social` */

insert  into `cp_business_social`(`id`,`business_id`,`social_id`,`status`,`created_at`,`created_by`) values (1,2,1,'active',NULL,NULL),(2,3,2,'active',NULL,NULL),(3,2,2,'active',NULL,NULL);

/*Table structure for table `cp_business_type` */

DROP TABLE IF EXISTS `cp_business_type`;

CREATE TABLE `cp_business_type` (
  `id` bigint(50) NOT NULL,
  `title` varchar(512) NOT NULL,
  `sort` bigint(30) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cp_business_type` */

insert  into `cp_business_type`(`id`,`title`,`sort`,`created_at`,`created_by`) values (1,'Airports',1,NULL,1),(2,'Subways',2,NULL,1),(3,'Coffee Shops',3,NULL,1),(4,'Resturants',4,NULL,1);

/*Table structure for table `cp_cms` */

DROP TABLE IF EXISTS `cp_cms`;

CREATE TABLE `cp_cms` (
  `id` bigint(50) NOT NULL,
  `title` varchar(512) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `content` text,
  `type` enum('email','page','notification') DEFAULT 'email',
  `sort` bigint(30) DEFAULT NULL,
  `is_created` datetime DEFAULT NULL,
  `is_modified` datetime DEFAULT NULL,
  `user_modified` bigint(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cp_cms` */

insert  into `cp_cms`(`id`,`title`,`slug`,`content`,`type`,`sort`,`is_created`,`is_modified`,`user_modified`) values (1,'Default Email Template','SiteEmailBody','<html>\r\n    <head>\r\n        <style type=\"text/css\">\r\n            body {\r\n                margin: 0 auto;\r\n                padding: 0;\r\n                font-size: 13px;\r\n                width:60%;\r\n            }\r\n            .oswald {\r\n                font-family: Oswald, sans-serif;\r\n            }\r\n            .roboto {\r\n                font-family: Roboto Condensed, sans-serif;\r\n            }\r\n            .kameron {\r\n                font-family: Kameron, serif;\r\n            }\r\n            table {\r\n                border-collapse: collapse;\r\n            }\r\n            td {\r\n                font-family: arial, sans-serif;\r\n                color: #333333;\r\n            }\r\n            a {\r\n                color: #000910;\r\n                text-decoration: none;\r\n            }\r\n            a:hover {\r\n                text-decoration: underline;\r\n            }\r\n            .btn {\r\n                -webkit-border-radius: 0;\r\n                -moz-border-radius: 0;\r\n                border-radius: 0px;\r\n                font-family: Arial;\r\n                color: #3ecbae;\r\n                font-size: 16px;\r\n                background: #e6e6e6;\r\n                padding: 5px;\r\n                border: solid #3ecbae 2px;\r\n                text-decoration: none;\r\n            }\r\n            .btn:hover {\r\n                background: #fff;\r\n                text-decoration: none;\r\n                cursor: pointer;\r\n            }\r\n            .readmore {\r\n                width: 120px;\r\n                margin: 0 auto;\r\n                text-align: center;\r\n            }\r\n            .btn2 {\r\n                -webkit-border-radius: 0;\r\n                -moz-border-radius: 0;\r\n                border-radius: 0px;\r\n                font-family: Arial;\r\n                color: #ffffff;\r\n                font-size: 16px;\r\n                background: #2eb196;\r\n                padding: 10px;\r\n                border: solid #2eb196 0px;\r\n                text-decoration: none;\r\n                cursor: pointer;\r\n                width: 180px;\r\n            }\r\n            .btn2:hover {\r\n                background: #25947c;\r\n                text-decoration: none;\r\n            }\r\n            .stats {\r\n                border-radius: 10px;\r\n                background: #d1d6db;\r\n                padding:10px;\r\n            }\r\n\r\n            #stats h1.big {\r\n                margin:0;font-size:56px;color:#354b60;font-family:Impact, Arial Black;\r\n            }\r\n\r\n            #stats h6.small {\r\n                color:#354b60;font-size:16px;margin:0;\r\n            }\r\n            #reset img {\r\n                margin: 20px; border: 3px solid #eee; -webkit-box-shadow: 4px 4px 4px rgba(0,0,0,0.2); -moz-box-shadow: 4px 4px 4px rgba(0,0,0,0.2); box-shadow: 4px 4px 4px rgba(0,0,0,0.2); -webkit-transition: all 0.25s ease-out; -moz-transition: all 0.25s ease; -o-transition: all 0.25s ease;\r\n            } \r\n            #reset img:hover {\r\n                -webkit-transform: rotate(-2deg); -moz-transform: rotate(-2deg); -o-transform: rotate(-2deg);\r\n            }\r\n\r\n            @media only screen and (max-width: 480px) {\r\n                body, table, td, p, a, li, blockquote {\r\n                    -webkit-text-size-adjust: none !important;\r\n                }\r\n                table {\r\n                    width: 100% !important;\r\n                }\r\n                .responsive-image img {\r\n                    height: auto !important;\r\n                    max-width: 100% !important;\r\n                    width: 100% !important;\r\n                }\r\n            }\r\n        </style>\r\n    </head>\r\n    <body>\r\n        <div style=\"background:#e6e6e6;height:400px;\">\r\n            <div style=\"background:#282828;height:100px;text-align: center;\"> \r\n                <a href=\"http://dev.siliconplex.com/icard\" target = \"_blank\" >\r\n                    <img src=\"http://dev.siliconplex.com/icard/themes/icard/png/logo2.png\" width=\"70\" height=\"70\" style=\"margin-top:10px;float: left;\" alt=\"\"/>\r\n                </a>\r\n                <h1 style=\"color: white;padding-right: 95px;font-weight: 300;font-size: 5em;font-family: Segoe UI, Open Sans, sans-serif, serif;\"><span style=\"color: #cc0000;\">i</span>-Card</h1>\r\n            </div>\r\n\r\n            <div style=\"height:10px;\"></div>\r\n            <div style=\"background:#fff;height:125px;width:95%;margin:0 auto;border-color: #354b60;border-width: thin;border-style: double;\">\r\n                <table width=\"100%\">\r\n                    <tr>\r\n                        <td>\r\n                            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\" width=\"760\">\r\n                                <tr>\r\n                                    <td><p class=\"kameron\" align=\"justify\">[{BODYCONTENT}]</p></td>\r\n                                </tr>\r\n                            </table>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <br />\r\n            </div>\r\n            <div style=\"height:99px;\"></div>\r\n            <div style=\"background:#fff;height:50px;width:100%;margin:0 auto;bottom: 0;text-align: center;\">\r\n                                        <p><span style=\"font-weight: bold;\">©</span> 2015 \r\n                                            <a href=\"http://dev.siliconplex.com/icard\"><span style=\"color: #cc0000;font-weight: bold;\">i</span><span style=\"color: black;\">-Card</span></a>, All rights reserved</p>\r\n                                  \r\n                <br />\r\n            </div>\r\n        </div>\r\n    </body>\r\n</html>','email',NULL,NULL,NULL,NULL),(2,'Contact Us Email Body','contact-us-email','<div>Dear Administrator,<br/><br/>\r\n                [{NAME}] has requested for the following enquiry on DirectInterconnect Portal.<br/><br/>\r\n                <strong>Subject:</strong>&nbsp;[{SUBJECT}]<br/>\r\n                <strong>Message:</strong>&nbsp;[{MESSAGE}]<br/><br/>\r\n                <strong>User Details:</strong><br/>\r\n                <strong>Name:</strong>&nbsp;[{NAME}]<br/>\r\n                <strong>Email:</strong>&nbsp;[{EMAIL}]<br/><br/></div>','email',NULL,NULL,NULL,NULL),(3,'Help Email Body','help-email','<div>Dear Administrator,<br/><br/>\r\n                [{NAME}] has requested for the following enquiry on DirectInterconnect Portal.<br/><br/>\r\n                <strong>Subject:</strong>&nbsp;[{SUBJECT}]<br/>\r\n                <strong>Message:</strong>&nbsp;[{MESSAGE}]<br/><br/>\r\n                <strong>User Details:</strong><br/>\r\n                <strong>Name:</strong>&nbsp;[{NAME}]<br/>\r\n                <strong>Email:</strong>&nbsp;[{EMAIL}]<br/>\r\n                <br/></div>','email',NULL,NULL,NULL,NULL),(4,'Notification','Email','[{BODY}]','email',NULL,NULL,NULL,NULL),(5,'Email Confirmation','ConfirmEmail','<h3 style=\"margin: 0;padding: 0;font-family: &quot;HelveticaNeue-Light&quot;,&quot;Helvetica Neue Light&quot;,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida Grande&quot;,sans-serif;line-height: 1.1;margin-bottom: 15px;color: #000;font-weight: 500;font-size: 27px;\">Hi, [{NAME}]</h3>\r\n                <p class=\"lead\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;,&quot;Helvetica&quot;,Helvetica,Arial,sans-serif;margin-bottom: 10px;font-weight: normal;font-size: 17px;line-height: 1.6;\">To continue, we need you to verify your email address:<br></p>\r\n                <p style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;,&quot;Helvetica&quot;,Helvetica,Arial,sans-serif;margin-bottom: 10px;font-weight: normal;font-size: 14px;line-height: 1.6;\">[{EMAIL}]<br></p>\r\n                <p class=\"callout\" style=\"margin: 0;padding: 15px;font-family: &quot;Helvetica Neue&quot;,&quot;Helvetica&quot;,Helvetica,Arial,sans-serif;margin-bottom: 15px;font-weight: normal;font-size: 14px;line-height: 1.6;background-color: #ECF8FF;\">\r\n                Please click below to complete the verification process:<br><a href=\"[{RESETLINK}]\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;,&quot;Helvetica&quot;,Helvetica,Arial,sans-serif;color: #2BA6CB;font-weight: bold;\">Verify my email</a>\r\n                </p>','email',NULL,NULL,NULL,NULL),(6,'Password Reset','ResetPassword','<h3 style=\"margin: 0;padding: 0;font-family: &quot;HelveticaNeue-Light&quot;,&quot;Helvetica Neue Light&quot;,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida Grande&quot;,sans-serif;line-height: 1.1;margin-bottom: 15px;color: #000;font-weight: 500;font-size: 27px;\">Hi, [{NAME}]</h3>\r\n                <p class=\"lead\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;,&quot;Helvetica&quot;,Helvetica,Arial,sans-serif;margin-bottom: 10px;font-weight: normal;font-size: 17px;line-height: 1.6;\">You have recently asked to reset the password for this Direct Interconnect ID:<br></p>\r\n                <p style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;,&quot;Helvetica&quot;,Helvetica,Arial,sans-serif;margin-bottom: 10px;font-weight: normal;font-size: 14px;line-height: 1.6;\">[{EMAIL}]<br></p>\r\n                <p class=\"callout\" style=\"margin: 0;padding: 15px;font-family: &quot;Helvetica Neue&quot;,&quot;Helvetica&quot;,Helvetica,Arial,sans-serif;margin-bottom: 15px;font-weight: normal;font-size: 14px;line-height: 1.6;background-color: #ECF8FF;\">\r\n                To update your password, click the button below:<br><a href=\"[{RESETLINK}]\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;,&quot;Helvetica&quot;,Helvetica,Arial,sans-serif;color: #2BA6CB;font-weight: bold;\">Reset Password</a>\r\n                </p> ','email',NULL,NULL,NULL,NULL),(7,'Signup Email','EmailSignup','<h3 style=\"margin: 0;padding: 0;font-family: &quot;HelveticaNeue-Light&quot;,&quot;Helvetica Neue Light&quot;,&quot;Helvetica Neue&quot;,Helvetica,Arial,&quot;Lucida Grande&quot;,sans-serif;line-height: 1.1;margin-bottom: 15px;color: #000;font-weight: 500;font-size: 27px;\">Hi, [{NAME}]</h3>\r\n                <p class=\"lead\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;,&quot;Helvetica&quot;,Helvetica,Arial,sans-serif;margin-bottom: 10px;font-weight: normal;font-size: 17px;line-height: 1.6;\">You have recently asked to reset the password for this Direct Interconnect ID:<br></p>\r\n                <p style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;,&quot;Helvetica&quot;,Helvetica,Arial,sans-serif;margin-bottom: 10px;font-weight: normal;font-size: 14px;line-height: 1.6;\">[{EMAIL}]<br></p>\r\n                <p class=\"callout\" style=\"margin: 0;padding: 15px;font-family: &quot;Helvetica Neue&quot;,&quot;Helvetica&quot;,Helvetica,Arial,sans-serif;margin-bottom: 15px;font-weight: normal;font-size: 14px;line-height: 1.6;background-color: #ECF8FF;\">\r\n                To update your password, click the button below:<br><a href=\"[{RESETLINK}]\" style=\"margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;,&quot;Helvetica&quot;,Helvetica,Arial,sans-serif;color: #2BA6CB;font-weight: bold;\">Reset Password</a>\r\n                </p> ','email',NULL,NULL,NULL,NULL);

/*Table structure for table `cp_country` */

DROP TABLE IF EXISTS `cp_country`;

CREATE TABLE `cp_country` (
  `id` bigint(50) NOT NULL,
  `countryCode` varchar(512) NOT NULL,
  `countryName` varchar(512) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cp_country` */

insert  into `cp_country`(`id`,`countryCode`,`countryName`) values (1,'UK','England'),(2,'PAK','Pakistan'),(3,'USA','United States of America');

/*Table structure for table `cp_currency` */

DROP TABLE IF EXISTS `cp_currency`;

CREATE TABLE `cp_currency` (
  `id` bigint(50) NOT NULL,
  `title` varchar(512) NOT NULL,
  `code` varchar(512) DEFAULT NULL,
  `exchange_rate` varchar(512) DEFAULT NULL,
  `is_default` bit(1) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cp_currency` */

insert  into `cp_currency`(`id`,`title`,`code`,`exchange_rate`,`is_default`,`created_at`,`created_by`) values (1,'England','UK',NULL,NULL,NULL,NULL),(2,'Germany','EUR',NULL,NULL,NULL,NULL),(3,'US Dollar','USD',NULL,NULL,NULL,NULL);

/*Table structure for table `cp_device_type` */

DROP TABLE IF EXISTS `cp_device_type`;

CREATE TABLE `cp_device_type` (
  `id` bigint(50) NOT NULL,
  `title` varchar(512) NOT NULL,
  `vendor` varchar(512) DEFAULT NULL,
  `sort` bigint(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cp_device_type` */

insert  into `cp_device_type`(`id`,`title`,`vendor`,`sort`) values (1,'Ipad Type 3','iphone',1);

/*Table structure for table `cp_devices` */

DROP TABLE IF EXISTS `cp_devices`;

CREATE TABLE `cp_devices` (
  `id` bigint(50) NOT NULL,
  `title` varchar(512) NOT NULL,
  `type_id` bigint(50) NOT NULL,
  `identifier` varchar(512) DEFAULT NULL,
  `sort` bigint(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cp_devices_ibfk_1` (`type_id`),
  CONSTRAINT `cp_devices_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `cp_device_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cp_devices` */

insert  into `cp_devices`(`id`,`title`,`type_id`,`identifier`,`sort`) values (1,'Ipad 3.0',1,'EARADSADASJKLJL@KJ-DSADD',1),(2,'Ipad 3.0',1,'AXXDDSADASJKLJL@KJ-DSA55',1),(3,'Ipad 3.0',1,'AXXDDSADASJKLJL@KJ-DSA66',1),(23654785,'test',1,'kldj213890',1),(31936341,'latest device',1,'adlkaj789',1);

/*Table structure for table `cp_feed_type` */

DROP TABLE IF EXISTS `cp_feed_type`;

CREATE TABLE `cp_feed_type` (
  `id` bigint(50) NOT NULL,
  `title` varchar(512) NOT NULL,
  `sort` bigint(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cp_feed_type` */

insert  into `cp_feed_type`(`id`,`title`,`sort`) values (1,'Entertainment',1),(2,'UK',1),(3,'World',1),(4,'Sports',1),(5,'Fashion',1);

/*Table structure for table `cp_feeds` */

DROP TABLE IF EXISTS `cp_feeds`;

CREATE TABLE `cp_feeds` (
  `id` bigint(50) NOT NULL,
  `title` varchar(512) NOT NULL,
  `image` varchar(512) DEFAULT NULL,
  `url` varchar(512) NOT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `type_id` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cp_feeds_ibfk1` (`type_id`),
  CONSTRAINT `cp_feeds_ibfk1` FOREIGN KEY (`type_id`) REFERENCES `cp_feed_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cp_feeds` */

insert  into `cp_feeds`(`id`,`title`,`image`,`url`,`created_at`,`created_by`,`type_id`) values (1,'title1 ','','www.xyz.com',NULL,1,NULL),(2,'title2','','www.url2.com',NULL,1,NULL),(3,'title3','','www.magazine.com',NULL,1,NULL),(4,'title4','','www.mag.com',NULL,1,NULL);

/*Table structure for table `cp_keywords` */

DROP TABLE IF EXISTS `cp_keywords`;

CREATE TABLE `cp_keywords` (
  `id` bigint(50) NOT NULL,
  `title` varchar(512) NOT NULL,
  `sort` bigint(30) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cp_keywords` */

insert  into `cp_keywords`(`id`,`title`,`sort`,`description`) values (1,'gun',NULL,NULL),(2,'powder',NULL,NULL),(3,'cocaine',NULL,NULL),(4,'marijuana',NULL,NULL),(5,'weed',NULL,NULL),(25915837,'new keywords',1,'abcd new keyword');

/*Table structure for table `cp_offers` */

DROP TABLE IF EXISTS `cp_offers`;

CREATE TABLE `cp_offers` (
  `id` bigint(50) NOT NULL,
  `title` varchar(512) NOT NULL,
  `description` text,
  `amount` decimal(10,4) DEFAULT NULL,
  `currency_id` bigint(50) NOT NULL,
  `business_id` bigint(50) NOT NULL,
  `sort` bigint(30) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `type` varchar(512) NOT NULL,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  `valid_till` bigint(20) DEFAULT NULL,
  `quota` int(20) DEFAULT NULL,
  `end_time` bigint(20) DEFAULT NULL,
  `image` varchar(512) DEFAULT NULL,
  `is_top` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cp_offers_ibfk_1` (`created_by`),
  KEY `cp_offers_ibfk_2` (`currency_id`),
  KEY `cp_offers_ibfk_3` (`business_id`),
  CONSTRAINT `cp_offers_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `cp_user` (`id`),
  CONSTRAINT `cp_offers_ibfk_2` FOREIGN KEY (`currency_id`) REFERENCES `cp_currency` (`id`),
  CONSTRAINT `cp_offers_ibfk_3` FOREIGN KEY (`business_id`) REFERENCES `cp_business` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cp_offers` */

insert  into `cp_offers`(`id`,`title`,`description`,`amount`,`currency_id`,`business_id`,`sort`,`created_at`,`created_by`,`type`,`latitude`,`longitude`,`valid_till`,`quota`,`end_time`,`image`,`is_top`) values (123,'awesome','slkjdlk best offer ever','450.0000',1,2,1,NULL,NULL,'',0,0,NULL,NULL,NULL,NULL,0),(456,'awesome12','slkjdlk best offer ever','650.0000',1,1,3,NULL,NULL,'',0,0,NULL,NULL,NULL,NULL,0),(785,'coffee','it\'s a coffee shop','525.0000',3,3,5,NULL,NULL,'',0,0,NULL,NULL,NULL,NULL,0),(6969,'fsdf','scvbdrr','858.0000',3,3,5,NULL,NULL,'',0,0,NULL,NULL,NULL,NULL,0),(456789,'awesome78','slkjdlk','50.0000',2,2,2,NULL,NULL,'',0,0,NULL,NULL,NULL,NULL,0),(456799,'abx234','sdfsdfsaa','508.0000',3,3,4,NULL,NULL,'',0,0,NULL,NULL,NULL,NULL,0),(645785,'vjghjghj','vbnvbnvbn','65.0000',3,3,5,NULL,NULL,'',0,0,NULL,NULL,NULL,NULL,0);

/*Table structure for table `cp_privileges` */

DROP TABLE IF EXISTS `cp_privileges`;

CREATE TABLE `cp_privileges` (
  `id` bigint(50) NOT NULL,
  `title` varchar(512) DEFAULT NULL,
  `key` varchar(512) DEFAULT NULL,
  `function` varchar(512) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `group_id` bigint(50) NOT NULL,
  `function_id` bigint(50) NOT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `modified_by` bigint(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cp_priveledges_ibfk_1` (`modified_by`),
  KEY `cp_priveledges_ibfk_2` (`function_id`),
  KEY `cp_priveledges_ibfk_3` (`group_id`),
  CONSTRAINT `cp_privileges_ibfk_1` FOREIGN KEY (`modified_by`) REFERENCES `cp_user` (`id`),
  CONSTRAINT `cp_privileges_ibfk_2` FOREIGN KEY (`function_id`) REFERENCES `cp_privileges_function` (`id`),
  CONSTRAINT `cp_privileges_ibfk_3` FOREIGN KEY (`group_id`) REFERENCES `cp_privileges_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cp_privileges` */

insert  into `cp_privileges`(`id`,`title`,`key`,`function`,`status`,`group_id`,`function_id`,`created_at`,`modified_at`,`created_by`,`modified_by`) values (567658,'User Index','User_Index',NULL,1,1,123,NULL,NULL,NULL,1),(35069375,'Business Index','Business_Index',NULL,1,2,234363,1461332840,1461332840,22993139,22993139),(35069394,'Add Business','Add_Business',NULL,1,2,543453,1461332840,1461332840,22993139,22993139),(35069395,'Add Offer','Add_Offer',NULL,1,3,5675674,1461332840,1461332840,22993139,22993139),(56765889,'User Add','User_Add',NULL,1,1,456,NULL,NULL,NULL,1),(98787987,'Dashboard','Dashboard',NULL,1,4,78978900,NULL,NULL,NULL,1);

/*Table structure for table `cp_privileges_function` */

DROP TABLE IF EXISTS `cp_privileges_function`;

CREATE TABLE `cp_privileges_function` (
  `id` bigint(50) NOT NULL,
  `title` varchar(512) DEFAULT NULL,
  `module` varchar(512) DEFAULT NULL,
  `controller` varchar(512) DEFAULT NULL,
  `action` varchar(512) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cp_priveledges_function_ibfk_1` (`created_by`),
  CONSTRAINT `cp_privileges_function_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `cp_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cp_privileges_function` */

insert  into `cp_privileges_function`(`id`,`title`,`module`,`controller`,`action`,`created_at`,`created_by`) values (123,'List User','user','MainController.php','index',NULL,1),(456,'Add User','user','MainController.php','add',NULL,1),(23423,'View User','user','MainController.php','view',NULL,1),(234363,'List Business','business','MainController.php','index',NULL,1),(543453,'Add Business','business','MainController.php','add',NULL,1),(5675674,'Add Offer','offers','MainController.php','add',NULL,1),(78978900,'Dashboard','basic','SiteController.php','dashboard',NULL,1);

/*Table structure for table `cp_privileges_group` */

DROP TABLE IF EXISTS `cp_privileges_group`;

CREATE TABLE `cp_privileges_group` (
  `id` bigint(50) NOT NULL,
  `group` varchar(512) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cp_priveledges_group_ibfk_1` (`created_by`),
  CONSTRAINT `cp_privileges_group_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `cp_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cp_privileges_group` */

insert  into `cp_privileges_group`(`id`,`group`,`created_at`,`created_by`) values (1,'User',NULL,1),(2,'Business',NULL,1),(3,'Offers',NULL,1),(4,'Site',NULL,1);

/*Table structure for table `cp_role` */

DROP TABLE IF EXISTS `cp_role`;

CREATE TABLE `cp_role` (
  `id` bigint(50) NOT NULL,
  `title` varchar(512) NOT NULL,
  `sort` bigint(30) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cp_role` */

insert  into `cp_role`(`id`,`title`,`sort`,`created_at`,`created_by`) values (1,'Super Admin',NULL,NULL,1),(2,'Business Admin',NULL,NULL,1),(3,'Branch Admin',NULL,NULL,1),(4,'Sales Representative',NULL,NULL,1),(5,'iPad User',NULL,NULL,NULL);

/*Table structure for table `cp_role_privileges` */

DROP TABLE IF EXISTS `cp_role_privileges`;

CREATE TABLE `cp_role_privileges` (
  `id` bigint(50) NOT NULL,
  `priveledge_id` bigint(50) NOT NULL,
  `role_id` bigint(50) NOT NULL,
  `value` varchar(512) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cp_role_priveledges_ibfk_1` (`created_by`),
  KEY `cp_role_priveledges_ibfk_2` (`priveledge_id`),
  KEY `cp_role_priveledges_ibfk_3` (`role_id`),
  CONSTRAINT `cp_role_privileges_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `cp_user` (`id`),
  CONSTRAINT `cp_role_privileges_ibfk_2` FOREIGN KEY (`priveledge_id`) REFERENCES `cp_privileges` (`id`),
  CONSTRAINT `cp_role_privileges_ibfk_3` FOREIGN KEY (`role_id`) REFERENCES `cp_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cp_role_privileges` */

insert  into `cp_role_privileges`(`id`,`priveledge_id`,`role_id`,`value`,`created_at`,`created_by`) values (4574540,35069394,4,NULL,1461828017,22993139),(5365487,56765889,2,NULL,1461427142,44854916),(6537446,35069395,2,NULL,1461427142,44854916),(6822793,98787987,3,NULL,1461923092,22993139),(8451696,567658,1,NULL,1461427130,44854916),(11376018,35069375,1,NULL,1461427130,44854916),(12080753,56765889,1,NULL,1461427130,44854916),(15353961,56765889,3,NULL,1461923092,22993139),(17176041,567658,4,NULL,1461828018,22993139),(20708455,35069375,4,NULL,1461828017,22993139),(26548054,35069394,2,NULL,1461427142,44854916),(29427218,567658,2,NULL,1461427142,44854916),(33729459,56765889,4,NULL,1461828018,22993139),(37929165,35069395,4,NULL,1461828017,22993139),(46534208,35069394,1,NULL,1461427130,44854916),(46793437,567658,3,NULL,1461923092,22993139),(57199875,35069375,2,NULL,1461427142,44854916),(68170721,35069395,1,NULL,1461427130,44854916),(179272585,35069395,3,NULL,1461923092,22993139);

/*Table structure for table `cp_searches` */

DROP TABLE IF EXISTS `cp_searches`;

CREATE TABLE `cp_searches` (
  `id` bigint(50) NOT NULL,
  `keyword` varchar(512) NOT NULL,
  `business_device_id` bigint(50) NOT NULL,
  `sort` bigint(30) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cp_searches_ibfk_1` (`created_by`),
  KEY `cp_searches_ibfk_2` (`business_device_id`),
  CONSTRAINT `cp_searches_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `cp_user` (`id`),
  CONSTRAINT `cp_searches_ibfk_2` FOREIGN KEY (`business_device_id`) REFERENCES `cp_business_devices` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cp_searches` */

/*Table structure for table `cp_settings` */

DROP TABLE IF EXISTS `cp_settings`;

CREATE TABLE `cp_settings` (
  `id` bigint(50) NOT NULL,
  `title` varchar(512) DEFAULT NULL,
  `key` varchar(512) NOT NULL,
  `value` varchar(512) DEFAULT NULL,
  `category` varchar(512) DEFAULT NULL,
  `image` varchar(512) DEFAULT NULL,
  `business_id` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cp_settings` */

insert  into `cp_settings`(`id`,`title`,`key`,`value`,`category`,`image`,`business_id`) values (1,'Support Email Addres','SUPPORT_EMAIL','support@coffipad.com','email',NULL,NULL),(2,'Contact Us Email','CONTACT_EMAIL','contact@coffipad.com','email',NULL,NULL),(3,'User Session Timeout','SESSION_TIMEOUT','10',NULL,NULL,NULL),(4,'Support Email Name','SUPPORT_NAME','Team Coffipad','email',NULL,NULL),(5,'App Timeout','APP_TIMEOUT','30 seconds',NULL,NULL,NULL),(6,'Allow Ads','ALLOW_ADS','1',NULL,NULL,NULL),(7,'Allow Feeds','ALLOW_FEEDS','1',NULL,NULL,NULL),(8,'Allow Offers','ALLOW_OFFERS','1',NULL,NULL,NULL);

/*Table structure for table `cp_social` */

DROP TABLE IF EXISTS `cp_social`;

CREATE TABLE `cp_social` (
  `id` bigint(50) NOT NULL,
  `name` varchar(512) NOT NULL,
  `url` varchar(512) NOT NULL,
  `sort` bigint(30) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cp_social` */

insert  into `cp_social`(`id`,`name`,`url`,`sort`,`created_at`,`created_by`) values (1,'HOTMAIL','www.hotmail.com',1,NULL,NULL),(2,'YAHOO','www.yahoo.com',2,NULL,NULL),(3,'MSN','www.msn.com',3,NULL,NULL);

/*Table structure for table `cp_user` */

DROP TABLE IF EXISTS `cp_user`;

CREATE TABLE `cp_user` (
  `id` bigint(50) NOT NULL,
  `f_name` varchar(512) NOT NULL,
  `email` varchar(512) NOT NULL,
  `password` varchar(512) NOT NULL,
  `business_id` bigint(50) DEFAULT NULL,
  `image` varchar(512) DEFAULT NULL,
  `country_id` bigint(50) NOT NULL,
  `state` varchar(512) DEFAULT NULL,
  `city` varchar(512) DEFAULT NULL,
  `postal_code` varchar(512) DEFAULT NULL,
  `address` varchar(512) DEFAULT NULL,
  `description` text,
  `token` varchar(512) DEFAULT NULL,
  `token_expiry` bigint(20) DEFAULT NULL,
  `reset_code` varchar(512) DEFAULT NULL,
  `verification_code` varchar(512) DEFAULT NULL,
  `last_login` bigint(20) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `modified_by` bigint(50) NOT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `login_count` int(20) DEFAULT NULL,
  `local_offers` tinyint(4) DEFAULT NULL,
  `third_party_offers` tinyint(4) DEFAULT NULL,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  `dob` bigint(20) DEFAULT NULL,
  `phone` varchar(512) DEFAULT NULL,
  `ni_number` varchar(512) DEFAULT NULL,
  `l_name` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `cp_user_ibfk_1` (`modified_by`),
  KEY `cp_user_ibfk_3` (`country_id`),
  CONSTRAINT `cp_user_ibfk_1` FOREIGN KEY (`modified_by`) REFERENCES `cp_user` (`id`),
  CONSTRAINT `cp_user_ibfk_3` FOREIGN KEY (`country_id`) REFERENCES `cp_country` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cp_user` */

insert  into `cp_user`(`id`,`f_name`,`email`,`password`,`business_id`,`image`,`country_id`,`state`,`city`,`postal_code`,`address`,`description`,`token`,`token_expiry`,`reset_code`,`verification_code`,`last_login`,`created_at`,`modified_at`,`created_by`,`modified_by`,`status`,`login_count`,`local_offers`,`third_party_offers`,`latitude`,`longitude`,`dob`,`phone`,`ni_number`,`l_name`) values (1,'Super Administrator','superadmin@coffipad.com','908fb44620910c09e1cd1d92ec901202',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'active',NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL),(2,'Gloria Admin','admin@gloria.com','cb0a7091352010a117aa3fd0d1fb65e5',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'active',NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL),(3,'Melt Cafe','admin@melt.com','cb0a7091352010a117aa3fd0d1fb65e5',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'active',NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL),(4093421,'usama','usama@haq.connn','a8f5f167f44f4964e6c998dee827110c',NULL,NULL,1,'asd','asdas','3423','234234',NULL,'zpgWwGs61i8e9EyL',NULL,NULL,'L7QIPn8tYa0dNS9r',NULL,1461779333,1461779333,22993139,22993139,'active',NULL,NULL,NULL,NULL,NULL,1459634400,'342',NULL,'haq'),(5336461,'hello','hello@user.com','cb0a7091352010a117aa3fd0d1fb65e5',1,NULL,1,'asda','sadas','34535','sfsdf',NULL,'vCPFdQzmDE3uHVrN',NULL,NULL,'vsFViA7hMXeaWQZk',NULL,1461824565,1461824565,22993139,22993139,'active',NULL,NULL,NULL,NULL,NULL,1463004000,'5646546',NULL,'user'),(22993139,'Fahd Shakir','fahd@user.com','cb0a7091352010a117aa3fd0d1fb65e5',1,'3DFEAF8A-196G.PNG',2,NULL,NULL,NULL,NULL,NULL,'xyz',NULL,'vgdYibLHPF71Qaxu','rK8QaqzboeHnjNpL',1460706413,1457593020,1460706413,1,1,'active',9,NULL,NULL,9729380000,23904800,NULL,NULL,NULL,NULL),(24720725,'Rajo Rastogye','fahd_shakir12@user.com','908fb44620910c09e1cd1d92ec901202',NULL,'5.png',2,'karanchiiyuuu',NULL,NULL,NULL,NULL,'123',NULL,'9lfG7H2UPryz4WtK','rnVMeCJOyPqulhoK',1460720483,1457602381,1460720483,1,1,'active',4,NULL,NULL,9729380000,23904800,NULL,NULL,NULL,NULL),(24987430,'abdul','abdul@ghafoor.com','cb0a7091352010a117aa3fd0d1fb65e5',1,NULL,3,'asdasd','aasdasd','234234','asdasd',NULL,'SXhWmJO2nc4EN9Yz',NULL,NULL,'tMVkaYrgRGHvIW7K',NULL,1461825645,1461825645,22993139,22993139,'active',NULL,NULL,NULL,NULL,NULL,1459202400,'456345',NULL,'ghafoor'),(30844859,'usama','usama@haq.uk','d41d8cd98f00b204e9800998ecf8427e',NULL,NULL,1,'asd','asdas','3423','234234',NULL,'vLEMX9rxN0muIiZt',NULL,NULL,'ZYks1GnoNULXeJCi',NULL,1461779363,1461779363,22993139,22993139,'active',NULL,NULL,NULL,NULL,NULL,1459634400,'342',NULL,'haq'),(31077976,'hello','hello@user.commm','cb0a7091352010a117aa3fd0d1fb65e5',NULL,NULL,2,'asda','sadas','34535','sfsdf',NULL,'RI2Jup1rnDdMgbow',NULL,NULL,'2brsHfB0wTM9476K',NULL,1461824684,1461824684,22993139,22993139,'active',NULL,NULL,NULL,NULL,NULL,1463004000,'5646546',NULL,'user'),(38596866,'abdul','abdul@basit.com','cb0a7091352010a117aa3fd0d1fb65e5',38889320,NULL,2,'aslkdj','aslkdj','','sdas',NULL,'3vHVZBcAQg2iorDF',NULL,NULL,'nT4WiaxjrVRgAdQM',NULL,1461824964,1461824964,22993139,22993139,'active',NULL,NULL,NULL,NULL,NULL,1463004000,'236454',NULL,'basit'),(39752612,'admin','admin@abdul.com','cb0a7091352010a117aa3fd0d1fb65e5',22357127,NULL,3,'asdasd','asdasd','24234234','asdasd',NULL,'3ZdfbGASX0jWIxMB',NULL,NULL,'CqPodnw9WaJtiROV',NULL,1461827494,1461827494,22993139,22993139,'active',NULL,NULL,NULL,NULL,NULL,1462399200,'654674987',NULL,'abdul'),(44435669,'great','alpha@bravo.com','cb0a7091352010a117aa3fd0d1fb65e5',59901134,NULL,2,'asda','assdasd','45345','asdas',NULL,'qiEnLXyIm37CbwFp',NULL,NULL,'UAEqIVZrHfR9n0lx',NULL,1461831092,1461921658,22993139,22993139,'active',NULL,NULL,NULL,NULL,NULL,0,'654654',NULL,'alex'),(44854916,'Rajo','fahd_shakir112@user.com','6137111d263e5a390642a0cf4f7eb81b',59901134,NULL,1,'karanchii','sdfsdfsd','43534324','dfsdf',NULL,'abc',NULL,NULL,NULL,NULL,1457602404,1461919041,1,1,'active',NULL,NULL,NULL,0,0,0,NULL,NULL,' Rastogieeee'),(45299171,'usama','usama@haq.com','cb0a7091352010a117aa3fd0d1fb65e5',NULL,NULL,1,'asd','asdas','3423','234234',NULL,'uwfInYAv3qKGxZtH',NULL,NULL,'LG4EBfCFqAXSxHDP',NULL,1461779230,1461779230,22993139,22993139,'active',NULL,NULL,NULL,NULL,NULL,1459634400,'342',NULL,'haq'),(45633395,'bizz','bizz@admin.com','71b3e2555817f91c5869ae22fafaa803',1,NULL,2,'sdfsdf','sdfsdf','24234','dsfsdf',NULL,'wsQSx97jAdXcJybZ',NULL,NULL,'B23HmReus9rtw7dV',NULL,1461913512,1461925419,22993139,22993139,'active',NULL,NULL,NULL,NULL,NULL,0,'456456',NULL,'admin'),(320554483,'usama','usama@haq.comm','d41d8cd98f00b204e9800998ecf8427e',NULL,NULL,1,'asd','asdas','3423','234234',NULL,'hMVfbP0Un1zIAwNG',NULL,NULL,'NPa3JSIom4CZ0wkj',NULL,1461779292,1461779292,22993139,22993139,'active',NULL,NULL,NULL,NULL,NULL,1459634400,'342',NULL,'haq');

/*Table structure for table `cp_user_offers` */

DROP TABLE IF EXISTS `cp_user_offers`;

CREATE TABLE `cp_user_offers` (
  `id` bigint(50) NOT NULL,
  `offer_id` bigint(50) NOT NULL,
  `user_id` bigint(50) NOT NULL,
  `device_id` bigint(50) NOT NULL,
  `status` enum('pending','approve','redeemed') DEFAULT 'pending',
  `sort` bigint(30) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) DEFAULT NULL,
  `offer_code` varchar(512) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cp_user_offers_ibfk_1` (`created_by`),
  KEY `cp_user_offers_ibfk_2` (`offer_id`),
  KEY `cp_user_offers_ibfk_3` (`user_id`),
  KEY `cp_user_offers_ibfk_4` (`device_id`),
  CONSTRAINT `cp_user_offers_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `cp_user` (`id`),
  CONSTRAINT `cp_user_offers_ibfk_2` FOREIGN KEY (`offer_id`) REFERENCES `cp_offers` (`id`),
  CONSTRAINT `cp_user_offers_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `cp_user` (`id`),
  CONSTRAINT `cp_user_offers_ibfk_4` FOREIGN KEY (`device_id`) REFERENCES `cp_devices` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cp_user_offers` */

/*Table structure for table `cp_user_privileges` */

DROP TABLE IF EXISTS `cp_user_privileges`;

CREATE TABLE `cp_user_privileges` (
  `id` bigint(50) NOT NULL,
  `user_id` bigint(50) NOT NULL,
  `priveledge_id` bigint(50) NOT NULL,
  `value` varchar(512) DEFAULT NULL,
  `is_extra` int(1) DEFAULT NULL,
  `expiry` bigint(20) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) NOT NULL,
  `user_role_id` bigint(50) NOT NULL,
  `status` enum('active','in-active') DEFAULT 'active',
  `start_time` bigint(20) NOT NULL,
  `end_time` bigint(20) NOT NULL,
  `quota` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cp_user_priveledges_ibfk_1` (`created_by`),
  KEY `cp_user_priveledges_ibfk_2` (`priveledge_id`),
  KEY `cp_user_priveledges_ibfk_3` (`user_id`),
  KEY `cp_user_privileges_ibfk_4` (`user_role_id`),
  CONSTRAINT `cp_user_privileges_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `cp_user` (`id`),
  CONSTRAINT `cp_user_privileges_ibfk_2` FOREIGN KEY (`priveledge_id`) REFERENCES `cp_privileges` (`id`),
  CONSTRAINT `cp_user_privileges_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `cp_user` (`id`),
  CONSTRAINT `cp_user_privileges_ibfk_4` FOREIGN KEY (`user_role_id`) REFERENCES `cp_user_role` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cp_user_privileges` */

insert  into `cp_user_privileges`(`id`,`user_id`,`priveledge_id`,`value`,`is_extra`,`expiry`,`created_at`,`created_by`,`user_role_id`,`status`,`start_time`,`end_time`,`quota`) values (4122755,45633395,56765889,NULL,NULL,NULL,1461925419,22993139,4,'active',0,0,''),(5219074,45633395,35069394,NULL,NULL,NULL,1461925419,22993139,4,'active',0,0,''),(11892410,44435669,567658,NULL,NULL,NULL,1461923652,22993139,3,'active',1461880800,1468015199,'-1'),(13344473,44435669,56765889,NULL,NULL,NULL,1461923652,22993139,3,'active',1461880800,1468015199,'-1'),(17333163,45633395,567658,NULL,NULL,NULL,1461925419,22993139,4,'active',0,0,''),(25654932,45633395,35069375,NULL,NULL,NULL,1461925419,22993139,4,'active',0,0,''),(31620590,44435669,35069395,NULL,NULL,NULL,1461923652,22993139,3,'active',1461880800,1464472799,'-1'),(32201344,44435669,98787987,NULL,NULL,NULL,1461923652,22993139,3,'active',1461880800,1464472799,'-1'),(40428004,45633395,35069395,NULL,NULL,NULL,1461925419,22993139,4,'active',0,0,'');

/*Table structure for table `cp_user_role` */

DROP TABLE IF EXISTS `cp_user_role`;

CREATE TABLE `cp_user_role` (
  `id` bigint(50) NOT NULL,
  `role_id` bigint(50) NOT NULL,
  `user_id` bigint(50) NOT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_FK_role` (`role_id`),
  KEY `user_FK_user` (`user_id`),
  CONSTRAINT `user_FK_role` FOREIGN KEY (`role_id`) REFERENCES `cp_role` (`id`),
  CONSTRAINT `user_FK_user` FOREIGN KEY (`user_id`) REFERENCES `cp_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cp_user_role` */

insert  into `cp_user_role`(`id`,`role_id`,`user_id`,`created_at`,`created_by`) values (1,1,1,NULL,1),(2,2,2,NULL,1),(4,1,22993139,NULL,0),(5,4,3,NULL,0),(6,3,44854916,NULL,0),(557789,1,30844859,1461779364,30844859),(2100844,1,4093421,1461779333,4093421),(9986872,3,44435669,1461831093,44435669),(16620647,2,38596866,1461824964,38596866),(22740996,1,320554483,1461779292,320554483),(30519623,2,39752612,1461827495,39752612),(33215045,4,24987430,1461825645,24987430),(33347628,1,5336461,1461824567,5336461),(38372913,1,31077976,1461824685,31077976),(43234321,4,45633395,1461913513,45633395),(52614115,1,45299171,1461779230,45299171);

/*Table structure for table `migration` */

DROP TABLE IF EXISTS `migration`;

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `migration` */

insert  into `migration`(`version`,`apply_time`) values ('m000000_000000_base',1456557585),('m160223_085434_AddCMSTable',1456557590),('m160223_090133_AddSettingTable',1456557591),('m160223_090259_AddCountryTable',1456557592),('m160223_090433_AddCurrencyTable',1456557592),('m160223_090603_AddKeywordsTable',1456557593),('m160223_091423_AddRolesTable',1456557594),('m160223_091836_AddDeviceTypeTable',1456557594),('m160223_092327_AddDevicesTable',1456557596),('m160223_092835_AddUserTable',1456557598),('m160223_094236_AddUserRoleTable',1456557598),('m160223_095846_AddPriveledgesGroupTable',1456557599),('m160223_100736_AddPriveledgeFunctionTable',1456557602),('m160223_101017_AddPriveledgesTable',1456557605),('m160223_101501_AddRolePriveledgesTable',1456557608),('m160223_102302_AddBusinessTypeTable',1456557608),('m160223_103003_AddBusinessTable',1456557611),('m160223_112400_AddBusinessKeywordTable',1456557615),('m160223_113540_AddBusinessDevicesTable',1456557618),('m160223_114232_AddSearchesTable',1456557620),('m160223_114911_AddOffersTable',1456557625),('m160223_115901_AddUserOfferTable',1458126416),('m160223_124154_AddSeedDataTable',1456557630),('m160311_130605_addfeedstable',1458053727),('m160315_141726_addbusinessfeedstable',1458053729),('m160325_122553_addSocialTable',1458909001),('m160325_131745_AddBusinesSocialTable',1458913420),('m160325_150743_alterFeedsTable',1458918515),('m160407_132622_AddUserStatus',1460035729),('m160413_144317_AlterUser',1460634071),('m160414_094115_Alteration',1460634075),('m160414_115335_AlterUserForLocation',1460635185),('m160414_130920_LocationType',1460639642),('m160414_131934_SettingAlter',1460652597),('m160414_150342_AlterUserDetails',1460652600),('m160415_071642_AlterUserName',1460704902),('m160415_094312_AlterOfferStatus',1460717154),('m160415_110136_AlterBusinessName',1460718143),('m160415_113555_AlterBiznessName',1460720189),('m160415_135558_AlterSettings',1460728811),('m160415_143052_AdvertiseTable',1460797808),('m160419_073024_AlterOffer',1461060809),('m160419_133535_AlterUserTable',1461074121),('m160420_095427_AddUserPrivilege',1461146295),('m160420_101113_AlterUserPrivilege',1461148533),('m160421_163742_AlterUserPriv',1461256764),('m160421_164244_alterUserPrivilege',1461257397),('m160425_152214_AlterBusinessDevices',1461597877),('m160426_095600_addFeedsType',1461828274),('m160426_153242_AlterBusinessDevices',1461685022),('m160427_064107_alterFeedsTable',1461828274),('m160427_151833_AlterFeedsTable',1461829559);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
