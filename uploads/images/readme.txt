*************************************************************************
*									*
*			 FileImporter class				*
*		     Author : Md. Kausar Alam				*
*		   Email  : kausar_ss2003@yahoo.com			*
*									*
*									*
*									*
*************************************************************************

CLASS NAME
===========
fileImporter
	Import file into database table 

	
DESCRIPTION
===========
	This class used to import txt file into database table.When any text file is imported successfully this means file is uploaded into a temp dir.Then open
the text file from temp dir and read the contents from this file.Insert all datas into a table.After inserting all
datas of text file into table , text file will be removed from temp dir.


REQUIREMENTS:
=============
	1. PHP5
	2. only txt file is permitted to import
	3. support tab and comma delimiter

INSTALL
=======
sample.php is an example file.let can be checked how install this class.
