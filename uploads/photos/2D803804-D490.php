<?php
/* @var $this RoutePriceController */
/* @var $model RoutePrice */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'route-price-form',
        'enableAjaxValidation' => false,
    ));
    ?>
    <script>
        function removefav(id)
        {

            $.get("<?php echo Yii::app()->request->baseUrl; ?>/route/routeprice/removefavorites/id/" + id,
                    {},
                    function(res) {
                        if (res == 1) {
                            alert("Successful");
                            // tbd();
                        }
                        else
                        {
                            alert("Your route may already exists");
                        }

                    });

        }
        function isNumberKey(evt)
        {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
                    && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
        function addtofav(id)
        {
            var route = $("#rname").val();
            var rp = $("#rprice").val();
            var type = $("#type").val();
            var bs = $("#buy_sell").val();
            var bk = $("#bk").val();
            $.get("<?php echo Yii::app()->request->baseUrl; ?>/route/routeprice/favorites/id/" + id,
                    {
//                            route: route,
//                            rp: rp,
//                            type: type,
//                            bs: bs,
//                            bk: bk
                    },
                    function(res) {
                        if (res == 1) {
                            alert("Successful");
                            // tbd();
                        }
                        else
                        {
                            alert("Already Added to Your favorite List");
                        }

                    });

        }


        function match_route(name, breakout, price, type)
        {
            $.post("<?php echo Yii::app()->createUrl('route/routeprice/match') ?>",
                    {
                        name: name,
                        breakout: breakout,
                        price: price,
                        type: type
                    },
            function(result) {
                alert(result);
            });
        }

        $(document).ready(function() {
            table_data();
            $("#search").click(function() {
                var route = $("#rname").val();
                var rp = $("#rprice").val();
                var type = $("#type").val();
                var bs = $("#buy_sell").val();
                var bk = $("#bk").val();
                $.post("<?php echo Yii::app()->createUrl('route/routeprice/add') ?>",
                        {
                            route: route,
                            rp: rp,
                            type: type,
                            bs: bs,
                            bk: bk
                        },
                function(result) {
                    if (result === 1) {
                        alert("Successful");
                        table_data();
                    }
                    else
                    {
                        alert("Your route may already exists");
                    }

                });
            });



            function table_data()
            {
                var route = $("#rname").val();
                var rp = $("#rprice").val();
                var type = $("#type").val();
                var bs = $("#buy_sell").val();
                var bk = $("#bk").val();
                $.post("<?php echo Yii::app()->createUrl('route/routeprice/tbdata') ?>",
                        {
                            route: route,
                            rp: rp,
                            type: type,
                            bs: bs,
                            bk: bk
                        },
                function(result) {
                    var obj = JSON.parse(result);
                    //  console.log(result);
                    if (obj != null) {
                        console.log(obj);
                        for (var k in obj) {

                            //       console.log(k, obj[k].route);
                            //       $('#route-table').append("<tr><td>"+obj[k].route.route_name+"</td><td>"+obj[k].route.route_breakout+"</td><td>"+obj[k].rprice_price+"</td><td>"+obj[k].rprice_type+"</td><td><a onclick=\"addtofav("+obj[k].rprice_route_id+");\"href=\"<?php echo Yii::app()->request->baseUrl; ?>/route/routeprice/favorites/id/"+obj[k].rprice_route_id+"\"><img src=\"<?php echo Yii::app()->request->baseUrl; ?>/images/add2fav.png\" alt=\"Add To Favorite\"></a></td></tr> ");
                            if (obj[k].fav == 'false') {
                                $('#route-table').append("<tr onclick=\" match_route('" + obj[k].route.route_name + "','" + obj[k].route.route_breakout + "','" + obj[k].rprice_price + "','" + obj[k].rprice_type + "'); \"><td>" + obj[k].route.route_name + "</td><td>" + obj[k].route.route_breakout + "</td><td>" + obj[k].rprice_price + "</td><td>" + obj[k].rprice_type + "</td><td><a onclick=\"addtofav('" + obj[k].rprice_id + "');\" href=\"#\"><img src=\"<?php echo Yii::app()->request->baseUrl; ?>/images/add2fav.png\" alt=\"Add To Favorite\"></a></td></tr> ");
                            } else {
                                $('#route-table').append("<tr onclick=\" match_route('" + obj[k].route.route_name + "','" + obj[k].route.route_breakout + "','" + obj[k].rprice_price + "','" + obj[k].rprice_type + "'); \" ><td>" + obj[k].route.route_name + "</td><td>" + obj[k].route.route_breakout + "</td><td>" + obj[k].rprice_price + "</td><td>" + obj[k].rprice_type + "</td><td><a onclick=\"removefav('" + obj[k].rprice_id + "');\" href=\"#\"><img src=\"<?php echo Yii::app()->request->baseUrl; ?>/images/unlike.png\" alt=\"Remove From Favorite\"></a></td></tr> ");
                            }
                        }
                    }
                    else {
                        alert("Empty");
                    }

//                    if (result == 1) {
                    //  alert("Successful");
                    // }
                }
                );
            }




        });
    </script>
    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>
    <div class="col-lg-12">
        <div class="col-md-2 col-md-offset-1">
            <!--            <div class="col-sm-7 col-sm-offset-3">-->
            <div class="form-group">
                <input type="text" id="rname" id="rname" placeholder="Route" class="form-control" >
            </div>
        </div>
        <div class="col-md-2" style="padding-left: 20px;">
            <input type="text" name="rprice" id="rprice" placeholder="Target Price" class="form-control" onkeypress="return isNumberKey(event);">
        </div>
        <div class="col-md-2" style="padding-left: 20px;">
            <select name="type" id="type" class="form-control" >
                <option>CLi</option>
                <option>Non-CLi</option>
            </select>
        </div>
        <div class="col-md-1" style="padding-left: 20px;">
            <select id="buy_sell"  class="form-control">
                <option>Buy</option>
                <option>Sell</option>
            </select>
        </div>
        <div class="col-md-1" style="padding-left: 20px;">
<!--            <select name="break" id="break" class="form-control">
                <option>Select</option>
            </select>-->
            <input type="text" name="bk" id="bk" placeholdekr="Breakout" class="form-control" onkeypress="return isNumberKey(event);">
        </div>
        <!--            </div>-->
        <div class="col-md-1" style="padding-left: 20px;">
            <button type="button" name="search" id="search" class="btn ">ADD</button>
        </div>

        <div class="table-responsive" style="margin-top: 50px;">
            <div class="col-lg-5 col-md-offset-1">
                <table id="route-table" class="table table-condensed" style ="border: 1px solid black;"> 
                    <tr style ="border: 1px solid black;">
                        <td>Route</td>
                        <td>Breakout</td>
                        <td>Price</td>
                        <td>Type</td>
                        <td>Add to Favorites</td>
                    </tr>

                </table>
            </div>
            <div class="col-lg-5" style="margin-left: 20px;">
                <div>
                    <div class="col-lg-4">Total Matches Found</div>
                    <div >0</div>
                    <div style="margin-top: 20px;">
                        <table class="table" >
                            <tr style ="border: 1px solid black;">
                                <td>Route</td>
                                <td>Price</td>
                                <td>Type</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div
    </div>
</div>
<!--<div class="row buttons">
<?php //echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save');  ?>
</div>-->

<?php $this->endWidget(); ?>

</div><!-- form -->