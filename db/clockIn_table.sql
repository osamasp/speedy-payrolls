/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 5.6.17 : Database - speedy_payroll
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`speedy_payroll` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `speedy_payroll`;

/*Table structure for table `sp_company_shifts` */

DROP TABLE IF EXISTS `sp_company_shifts`;

CREATE TABLE `sp_company_shifts` (
  `id` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `start_time` bigint(20) NOT NULL,
  `end_time` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sp_company_shifts` */

insert  into `sp_company_shifts`(`id`,`title`,`start_time`,`end_time`,`company_id`) values (40824033,'Morning',1445500800,1445533200,11469208);

/*Table structure for table `sp_user_lunch` */

DROP TABLE IF EXISTS `sp_user_lunch`;

CREATE TABLE `sp_user_lunch` (
  `id` bigint(20) NOT NULL,
  `lunch_in` bigint(20) DEFAULT '0',
  `lunch_out` bigint(20) DEFAULT '0',
  `user_timing_id` bigint(20) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_timing_id` (`user_timing_id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `sp_user_lunch_ibfk_1` FOREIGN KEY (`user_timing_id`) REFERENCES `sp_user_timings` (`id`),
  CONSTRAINT `sp_user_lunch_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `sp_user` (`id`),
  CONSTRAINT `sp_user_lunch_ibfk_3` FOREIGN KEY (`modified_by`) REFERENCES `sp_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sp_user_lunch` */

insert  into `sp_user_lunch`(`id`,`lunch_in`,`lunch_out`,`user_timing_id`,`created_by`,`created_at`,`modified_by`,`modified_at`) values (8877023,1447156030,1447156107,18904606,1449760716,1447156030,1449760716,1447156107),(35169243,1447155714,1447155743,18904606,1449760716,1447155714,1449760716,1447155743),(46211368,1447156112,1447156115,18904606,1449760716,1447156112,1449760716,1447156115);

/*Table structure for table `sp_user_timings` */

DROP TABLE IF EXISTS `sp_user_timings`;

CREATE TABLE `sp_user_timings` (
  `id` bigint(20) NOT NULL,
  `time_in` bigint(20) DEFAULT NULL,
  `time_out` bigint(20) DEFAULT '0',
  `total_time` bigint(20) DEFAULT '0',
  `total_lunch` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `contract_id` bigint(20) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `status` enum('pending','approve','disapprove') DEFAULT 'pending',
  `reason` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `sp_user_timings_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `sp_user` (`id`),
  CONSTRAINT `sp_user_timings_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `sp_user` (`id`),
  CONSTRAINT `sp_user_timings_ibfk_3` FOREIGN KEY (`modified_by`) REFERENCES `sp_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sp_user_timings` */

insert  into `sp_user_timings`(`id`,`time_in`,`time_out`,`total_time`,`total_lunch`,`user_id`,`contract_id`,`created_at`,`modified_at`,`created_by`,`modified_by`,`status`,`reason`) values (18904606,1447153639,1447158966,5327,109,1449760716,1429845283,1444518000,1447167388,1449760716,1449760716,'disapprove','some reason');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

ALTER TABLE `speedy_payroll`.`sp_user`   
  ADD COLUMN `shift_id` BIGINT(20) NULL AFTER `first_login`

insert  into `sp_privilege_groups`(`id`,`group`) values (905519,'Two_Way'),(15616423,'Non_Billable'),(17117812,'Company Shifts'),(18315176,'Timesheet_Approvals'),(28869145,'Account'),(29315456,'One_Way'),(30350180,'Epayslips'),(31275786,'Contact'),(35530143,'Payslips'),(42440754,'Staff_Management'),(42686431,'SpeedyPayrolls'),(43720102,'Three_Way'),(48465085,'Payroll_Accounts'),(49818216,'Clock_In/Out'),(91173899,'Admin');