DELIMITER $$

DROP FUNCTION IF EXISTS `getDistance`$$

CREATE DEFINER=`root`@`localhost` FUNCTION `getDistance`(deg_lat1 FLOAT, deg_lng1 FLOAT, deg_lat2 FLOAT, deg_lng2 FLOAT) RETURNS FLOAT
    DETERMINISTIC
BEGIN 
  DECLARE distance FLOAT;
  DECLARE delta_lat FLOAT; 
  DECLARE delta_lng FLOAT; 
  DECLARE lat1 FLOAT; 
  DECLARE lat2 FLOAT;
  DECLARE a FLOAT;
  SET distance = 0;
  /*Convert degrees to radians and get the variables I need.*/
  SET delta_lat = RADIANS(deg_lat2 - deg_lat1); 
  SET delta_lng = RADIANS(deg_lng2 - deg_lng1); 
  SET lat1 = RADIANS(deg_lat1); 
  SET lat2 = RADIANS(deg_lat2); 
  /*Formula found here: http://www.movable-type.co.uk/scripts/latlong.html*/
  SET a = SIN(delta_lat/2.0) * SIN(delta_lat/2.0) + SIN(delta_lng/2.0) * SIN(delta_lng/2.0) * COS(lat1) * COS(lat2); 
  SET distance = 3956.6 * 2 * ATAN2(SQRT(a),  SQRT(1-a)); 
  RETURN distance;
END$$

DELIMITER ;