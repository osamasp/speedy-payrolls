UPDATE sp_quick_timesheet SET DATE = UNIX_TIMESTAMP(STR_TO_DATE(DATE,'%d/%m/%Y'));
ALTER TABLE `speedy_payroll`.`sp_quick_timesheet`   
  CHANGE `date` `date` BIGINT(20) NOT NULL;