DELIMITER $$

DROP FUNCTION IF EXISTS `getDistance`$$

CREATE DEFINER=`root`@`localhost` FUNCTION `getDistance`(deg_lat1 FLOAT, deg_lng1 FLOAT, deg_lat2 FLOAT, deg_lng2 FLOAT) RETURNS FLOAT
    DETERMINISTIC
BEGIN 
  DECLARE distance FLOAT;
  DECLARE delta_lat FLOAT; 
  DECLARE delta_lng FLOAT; 
  DECLARE lat1 FLOAT; 
  DECLARE lat2 FLOAT;
  DECLARE a FLOAT;
  SET distance = 0;
  /*Convert degrees to radians and get the variables I need.*/
  SET delta_lat = RADIANS(deg_lat2 - deg_lat1); 
  SET delta_lng = RADIANS(deg_lng2 - deg_lng1); 
  SET lat1 = RADIANS(deg_lat1); 
  SET lat2 = RADIANS(deg_lat2); 
  /*Formula found here: http://www.movable-type.co.uk/scripts/latlong.html*/
  SET a = SIN(delta_lat/2.0) * SIN(delta_lat/2.0) + SIN(delta_lng/2.0) * SIN(delta_lng/2.0) * COS(lat1) * COS(lat2); 
  SET distance = 3956.6 * 2 * ATAN2(SQRT(a),  SQRT(1-a)); 
  RETURN distance;
END$$

DELIMITER ;


INSERT INTO sp_cms (title, slug, content, TYPE) VALUES ('Speedy Payrolls Invitation', 'invite_admin', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Speedy Payrolls</title>
<style>
/* -------------------------------------
		GLOBAL
------------------------------------- */
* {
	margin: 0;
	padding: 0;
	font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
	font-size: 100%;
	line-height: 1.6;
}

img {
	max-width: 100%;
}

body {
	-webkit-font-smoothing: antialiased;
	-webkit-text-size-adjust: none;
	width: 100%!important;
	height: 100%;
}


/* -------------------------------------
		ELEMENTS
------------------------------------- */
a {
	color: #348eda;
}

.btn-primary {
	text-decoration: none;
	color: #FFF;
	background-color: #348eda;
	border: solid #348eda;
	border-width: 10px 20px;
	line-height: 2;
	font-weight: bold;
	margin-right: 10px;
	text-align: center;
	cursor: pointer;
	display: inline-block;
	border-radius: 25px;
}

.btn-secondary {
	text-decoration: none;
	color: #FFF;
	background-color: #aaa;
	border: solid #aaa;
	border-width: 10px 20px;
	line-height: 2;
	font-weight: bold;
	margin-right: 10px;
	text-align: center;
	cursor: pointer;
	display: inline-block;
	border-radius: 25px;
}

.last {
	margin-bottom: 0;
}

.first {
	margin-top: 0;
}

.padding {
	padding: 10px 0;
}


/* -------------------------------------
		BODY
------------------------------------- */
table.body-wrap {
	width: 100%;
	padding: 20px;
}

table.body-wrap .container {
	border: 1px solid #f0f0f0;
}


/* -------------------------------------
		FOOTER
------------------------------------- */
table.footer-wrap {
	width: 100%;	
	clear: both!important;
}

.footer-wrap .container p {
	font-size: 12px;
	color: #666;
	
}

table.footer-wrap a {
	color: #999;
}


/* -------------------------------------
		TYPOGRAPHY
------------------------------------- */
h1, h2, h3 {
	font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
	color: #000;
	margin: 40px 0 10px;
	line-height: 1.2;
	font-weight: 200;
}

h1 {
	font-size: 36px;
}
h2 {
	font-size: 28px;
}
h3 {
	font-size: 22px;
}

p, ul, ol {
	margin-bottom: 10px;
	font-weight: normal;
	font-size: 14px;
}

ul li, ol li {
	margin-left: 5px;
	list-style-position: inside;
}


/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
.container {
	display: block!important;
	max-width: 600px!important;
	margin: 0 auto!important; /* makes it centered */
	clear: both!important;
}

/* Set the padding on the td rather than the div for Outlook compatibility */
.body-wrap .container {
	padding: 20px;
}

/* This should also be a block element, so that it will fill 100% of the .container */
.content {
	max-width: 600px;
	margin: 0 auto;
	display: block;
}

.content table {
	width: 100%;
}

</style>
</head>

<body bgcolor="#f6f6f6">

<!-- body -->
<table class="body-wrap" bgcolor="#f6f6f6">
	<tr>
		<td></td>
		<td class="container" bgcolor="#FFFFFF">

			<!-- content -->
			<div class="content">
			<table>
				<tr>
					<td>
						<p>Dear [{NAME}]</p>
						<p>You have been given access to Speedy Payrolls by [{INVITED_BY}] to view and monitor their timesheets.</p>
						<h1>Next Steps</h1>
						<p>You can either approve this request by logging in, or you can report to Speedy Payrolls administrators by sending a quick email.</p>
						<h2>Access Details</h2>
						<p>Username: [{USERNAME}]</p>
						<p>Password: [{PASSWORD}]</p>
						<table>
							<tr>
								<td class="padding">
									<p><a href="[{LINK}]" class="btn-primary">Click here to login</a></p>
								</td>
							</tr>
						</table>
						<h2>Report Abuse</h2>
						<p>If you do not approve of this email and would like to report it to Speedy Administrators then contact us on the email address below:</p>
						<p>info@speedypayrolls.com</p>
						<p>Speedy Payrolls provides paperless timekeeping and payrolling.</p>
						
					</td>
				</tr>
			</table>
			</div>
			<!-- /content -->
			
		</td>
		<td></td>
	</tr>
</table>
<!-- /body -->

<!-- footer -->
<table class="footer-wrap">
	<tr>
		<td></td>
		<td class="container">
			
			<!-- content -->
			<div class="content">
				<table>
					<tr>
						<td align="center">
							<p>Don\'t like these annoying emails? <a href="#"><unsubscribe>Unsubscribe</unsubscribe></a>.
							</p>
						</td>
					</tr>
				</table>
			</div>
			<!-- /content -->
			
		</td>
		<td></td>
	</tr>
</table>
<!-- /footer -->

</body>
</html>
', 'email');


update sp_cms set content = ' <style>a { color: #348eda; }  .btn-primary { text-decoration: none; color: #FFF; 
 background-color: #348eda; border: solid #348eda; border-width: 10px 20px; line-height: 2;
 font-weight: bold; margin-right: 10px; text-align: center; cursor: pointer; display: inline-block;
 border-radius: 25px; }  .btn-secondary { text-decoration: none; color: #FFF; background-color: #aaa;
 border: solid #aaa; border-width: 10px 20px; line-height: 2; font-weight: bold; margin-right: 10px; 
 text-align: center; cursor: pointer; display: inline-block; border-radius: 25px; } 
 .last { margin-bottom: 0; }  .first { margin-top: 0; }  .padding { padding: 10px 0; }
 table.body-wrap{ width: 100%; padding: 20px; }  table.body-wrap .container { border: 1px solid #f0f0f0; } 
 table.footer-wrap { width: 100%; clear: both!important; }  .footer-wrap .container p { font-size: 12px; color: #666;  }
 table.footer-wrap a { color: #999; }  
 h1, h2, h3 { font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; color: #000; 
 margin: 40px 0 10px; line-height: 1.2; font-weight: 200; }  h1 { font-size: 36px; } h2 { font-size:28px; }
 h3 { font-size: 22px; }  p, ul, ol { margin-bottom: 10px; font-weight: normal; font-size: 14px; }  
 ul li, ol li { margin-left: 5px; list-style-position: inside; }   
 .container { display: block!important; max-width: 600px!important; margin: 0 auto!important; 
.body-wrap .container { padding: 20px; }  
.content { max-width: 600px; margin: 0 auto; display: block; }  .content table { width: 100%; }  </style> 
<div bgcolor="#f6f6f6">  <table class="body-wrap" bgcolor="#f6f6f6"> 
 <tr> <td></td> <td class="container" bgcolor="#FFFFFF">   <div class="content">
 <table> <tr> <td> <p>[{BODY}]</p>
 </table> 
 <h1>Next Steps</h1> <p>Please click on the link below to login </p>
  <table> <tr> <td class="padding">
 <p><a href="[{LOGIN_URL}]" class="btn-primary">Click here to login</a></p> </td> </tr> </table>
 <h2>Report Abuse</h2> <p>If you do not approve of this email and would like to report it to Speedy Administrators then contact us on the email address below:</p> <p>info@speedypayrolls.com</p> 
 <p>Speedy Payrolls provides paperless timekeeping and payrolling.</p>  </td> </tr> </table> 
 </div>   </td> <td></td> </tr> </table> 
 <table class="footer-wrap"> <tr> <td></td> <td class="container">  
 <div class="content"> <table> <tr> <td align="center"> <p>Don\'t like these annoying emails? 
 <a href="#"><unsubscribe>Unsubscribe</unsubscribe></a>. </p> </td> </tr> </table> </div> 
 </td> <td></td> </tr> </table>  </div> '
 where slug='Email';
 
 


update sp_cms set content = ' <style>a { color: #348eda; }  .btn-primary { text-decoration: none; color: #FFF; 
 background-color: #348eda; border: solid #348eda; border-width: 10px 20px; line-height: 2;
 font-weight: bold; margin-right: 10px; text-align: center; cursor: pointer; display: inline-block;
 border-radius: 25px; }  .btn-secondary { text-decoration: none; color: #FFF; background-color: #aaa;
 border: solid #aaa; border-width: 10px 20px; line-height: 2; font-weight: bold; margin-right: 10px; 
 text-align: center; cursor: pointer; display: inline-block; border-radius: 25px; } 
 .last { margin-bottom: 0; }  .first { margin-top: 0; }  .padding { padding: 10px 0; }
 table.body-wrap{ width: 100%; padding: 20px; }  table.body-wrap .container { border: 1px solid #f0f0f0; } 
 table.footer-wrap { width: 100%; clear: both!important; }  .footer-wrap .container p { font-size: 12px; color: #666;  }
 table.footer-wrap a { color: #999; }  
 h1, h2, h3 { font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; color: #000; 
 margin: 40px 0 10px; line-height: 1.2; font-weight: 200; }  h1 { font-size: 36px; } h2 { font-size:28px; }
 h3 { font-size: 22px; }  p, ul, ol { margin-bottom: 10px; font-weight: normal; font-size: 14px; }  
 ul li, ol li { margin-left: 5px; list-style-position: inside; }   
 .container { display: block!important; max-width: 600px!important; margin: 0 auto!important; 
.body-wrap .container { padding: 20px; }  
.content { max-width: 600px; margin: 0 auto; display: block; }  .content table { width: 100%; }  </style> 
<div bgcolor="#f6f6f6">   <table class="body-wrap" bgcolor="#f6f6f6"> 
 <tr> <td></td> <td class="container" bgcolor="#FFFFFF"> <div class="content">
 <table> <tr> <td> <p>Dear [{NAME}]</p> <p> Thank you for signing up with SpeedyPayrolls. Click the link below to verify your account and start using SpeedyPayrolls.</p> <h1>Next Steps</h1> <p>Please click below to complete the verification process:</p> 
  <h4>Email Address you registered with:</h4> <p>Email: [{EMAIL}]</p> 
  <table> <tr> <td class="padding"><a href="[{CONFIRMLINK}]" class="btn-primary">Verify my email</a></p> </td> 
 <br><p>NOTE: Please do not click on the verification link if you did not register for Speedy Payrolls. Please refer to the note below:</p>
</tr> </table> <h2>Report Abuse</h2> <p>If you do not approve of this email and would like to report it to Speedy Administrators then contact us on the email address below:</p> <p>info@speedypayrolls.com</p> 
 <p>Speedy Payrolls provides paperless timekeeping and payrolling.</p>  </td> </tr> </table> 
 </div> <!-- /content -->  </td> <td></td> </tr> </table> 
 <table class="footer-wrap"> <tr> <td></td> <td class="container">  
 <div class="content"> <table> <tr> <td align="center"> <p>Don\'t like these annoying emails? 
 <a href="#"><unsubscribe>Unsubscribe</unsubscribe></a>. </p> </td> </tr> </table> </div>
 </td> <td></td> </tr> </table>  </div> '
 where slug='ConfirmEmail';
 
 
 


update sp_cms set content = ' <style>a { color: #348eda; }  .btn-primary { text-decoration: none; color: #FFF; 
 background-color: #348eda; border: solid #348eda; border-width: 10px 20px; line-height: 2;
 font-weight: bold; margin-right: 10px; text-align: center; cursor: pointer; display: inline-block;
 border-radius: 25px; }  .btn-secondary { text-decoration: none; color: #FFF; background-color: #aaa;
 border: solid #aaa; border-width: 10px 20px; line-height: 2; font-weight: bold; margin-right: 10px; 
 text-align: center; cursor: pointer; display: inline-block; border-radius: 25px; } 
 .last { margin-bottom: 0; }  .first { margin-top: 0; }  .padding { padding: 10px 0; }
 table.body-wrap{ width: 100%; padding: 20px; }  table.body-wrap .container { border: 1px solid #f0f0f0; } 
 table.footer-wrap { width: 100%; clear: both!important; }  .footer-wrap .container p { font-size: 12px; color: #666;  }
 table.footer-wrap a { color: #999; }  
 h1, h2, h3 { font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; color: #000; 
 margin: 40px 0 10px; line-height: 1.2; font-weight: 200; }  h1 { font-size: 36px; } h2 { font-size:28px; }
 h3 { font-size: 22px; }  p, ul, ol { margin-bottom: 10px; font-weight: normal; font-size: 14px; }  
 ul li, ol li { margin-left: 5px; list-style-position: inside; }   
 .container { display: block!important; max-width: 600px!important; margin: 0 auto!important; 
.body-wrap .container { padding: 20px; }  
.content { max-width: 600px; margin: 0 auto; display: block; }  .content table { width: 100%; }  </style> 
<div bgcolor="#f6f6f6"> <table class="body-wrap" bgcolor="#f6f6f6"> 
 <tr> <td></td> <td class="container" bgcolor="#FFFFFF">  <div class="content">
 <table> <tr> <td> <p>Dear [{NAME}]</p> <p>  Your new password is : [{PASSWORD}]</p>
 </table> 
 <h1>Next Steps</h1> <p>Please click on the link below to login </p>
  <table> <tr> <td class="padding">
 <p><a href="[{LOGIN_URL}]" class="btn-primary">Click here to login</a></p> </td> </tr> </table>
 <h2>Report Abuse</h2> <p>If you do not approve of this email and would like to report it to Speedy Administrators then contact us on the email address below:</p> <p>info@speedypayrolls.com</p> 
 <p>Speedy Payrolls provides paperless timekeeping and payrolling.</p>  </td> </tr> </table> 
 </div> </td> <td></td> </tr> </table> 
 <table class="footer-wrap"> <tr> <td></td> <td class="container">  <!-- content --> 
 <div class="content"> <table> <tr> <td align="center"> <p>Don\'t like these annoying emails? 
 <a href="#"><unsubscribe>Unsubscribe</unsubscribe></a>. </p> </td> </tr> </table> </div> 
 </td> <td></td> </tr> </table></div> '
 where slug='ResetPassword';
 
 
 update sp_cms set content = ' <style>a { color: #348eda; }  .btn-primary { text-decoration: none; color: #FFF; 
 background-color: #348eda; border: solid #348eda; border-width: 10px 20px; line-height: 2;
 font-weight: bold; margin-right: 10px; text-align: center; cursor: pointer; display: inline-block;
 border-radius: 25px; }  .btn-secondary { text-decoration: none; color: #FFF; background-color: #aaa;
 border: solid #aaa; border-width: 10px 20px; line-height: 2; font-weight: bold; margin-right: 10px; 
 text-align: center; cursor: pointer; display: inline-block; border-radius: 25px; } 
 .last { margin-bottom: 0; }  .first { margin-top: 0; }  .padding { padding: 10px 0; }
 table.body-wrap{ width: 100%; padding: 20px; }  table.body-wrap .container { border: 1px solid #f0f0f0; } 
 table.footer-wrap { width: 100%; clear: both!important; }  .footer-wrap .container p { font-size: 12px; color: #666;  }
 table.footer-wrap a { color: #999; }  
 h1, h2, h3 { font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; color: #000; 
 margin: 40px 0 10px; line-height: 1.2; font-weight: 200; }  h1 { font-size: 36px; } h2 { font-size:28px; }
 h3 { font-size: 22px; }  p, ul, ol { margin-bottom: 10px; font-weight: normal; font-size: 14px; }  
 ul li, ol li { margin-left: 5px; list-style-position: inside; }   
 .container { display: block!important; max-width: 600px!important; margin: 0 auto!important; 
.body-wrap .container { padding: 20px; }  
.content { max-width: 600px; margin: 0 auto; display: block; }  .content table { width: 100%; }  </style> 
<div bgcolor="#f6f6f6">  <table class="body-wrap" bgcolor="#f6f6f6"> 
 <tr> <td></td> <td class="container" bgcolor="#FFFFFF"> <div class="content">
 <table> <tr> <td> <p>Dear [{NAME}]</p> <p>  [{MESSAGE}]</p>
 </table>
<h1>Next Steps</h1> <p>Please click on the link below to login </p>
  <table> <tr> <td class="padding">
 <p><a href="[{LOGIN_URL}]" class="btn-primary">Click here to login</a></p> </td> </tr> </table>
 <h2>Report Abuse</h2> <p>If you do not approve of this email and would like to report it to Speedy Administrators then contact us on the email address below:</p> <p>info@speedypayrolls.com</p> 
 <p>Speedy Payrolls provides paperless timekeeping and payrolling.</p>  </td> </tr> </table> 
 </div> <!-- /content -->  </td> <td></td> </tr> </table> 
 <table class="footer-wrap"> <tr> <td></td> <td class="container"> 
 <div class="content"> <table> <tr> <td align="center"> <p>Don\'t like these annoying emails? 
 <a href="#"><unsubscribe>Unsubscribe</unsubscribe></a>. </p> </td> </tr> </table> </div> 
 </td> <td></td> </tr> </table>  </div> '
 where slug='timesheet-notify';
 

insert  into `sp_function_list`(`id`,`title`,`module`,`controller`,`action`) values (22460092,'Export Shift','company','CompanyShiftsController.php','Export');

insert  into `sp_priveledge`(`id`,`name`,`key`,`function`,`group`,`status`,`function_list_id`,`created_at`,`modified_at`,`created_by`,`modified_by`) values (3074771,'Export Shift','Export_Shift','','Company Shifts',1,22460092,1469531034,1469531034,1,1)

insert  into `sp_function_list`(`id`,`title`,`module`,`controller`,`action`) values 
(22974368,'Assign Shift','user','EmployeeController.php','Assignshift'),
(29566461,'Change Payrate','user','EmployeeController.php','Changepayrate'),
(58791846,'Assign Supervisor','user','EmployeeController.php','Assignsupervisor'),
(27607753,'Approver Export','timesheet','ApproveController.php','Export');

/*Data for the table `sp_priveledge` */

insert  into `sp_priveledge`(`id`,`name`,`key`,`function`,`group`,`status`,`function_list_id`,`created_at`,`modified_at`,`created_by`,`modified_by`) values 
(6291198,'Assign Supervisor','Assign_Supervisor','','Staff_Management',1,58791846,1468943412,1468943412,1,1),
(53352057,'Assign Shift','Assign_Shift','','Staff_Management',1,22974368,1468419392,1468419392,1,1),
(53915495,'Change Payrate','Change_Payrate','','Staff_Management',1,29566461,1468943519,1468943519,1,1),
(16650662,'Approver Export','Approver_Export','','Staff_Management',1,27607753,1469794128,1469794128,1,1);



insert  into `sp_function_list`(`id`,`title`,`module`,`controller`,`action`) values 
(14773434,'Shift Export','company','CompanyShiftsController.php','ShiftExport'),
(13979290,'Export','company','CompanyShiftsController.php','Export');


insert  into `sp_priveledge`(`id`,`name`,`key`,`function`,`group`,`status`,`function_list_id`,`created_at`,`modified_at`,`created_by`,`modified_by`) values 
(12451809,'Shift Export','Shift_Export','','Company Shifts',1,14773434,1453545655,1453545655,1,1),
(42606990,'Shift_Export','Shift_Export','','Company Shifts',1,13979290,1453545855,1453545855,1,1);


ALTER TABLE sp_invoice_items  
  CHANGE `invoice_id` `invoice_id` BIGINT(20) NOT NULL, 
  ADD PRIMARY KEY (`id`);

ALTER TABLE sp_invoice_items 
  ADD FOREIGN KEY (`invoice_id`) REFERENCES `speedy_payrolls`.`sp_invoice`(`id`);

ALTER TABLE sp_invoice   
  ADD COLUMN `date` BIGINT(20) NULL AFTER `vat_number`;

ALTER TABLE sp_invoice 
  CHANGE `logo_url` `logo_url` VARCHAR(100) CHARSET latin1 COLLATE latin1_swedish_ci NULL;

ALTER TABLE sp_invoice_items  
  CHANGE `id` `id` BIGINT(20) NOT NULL;

DROP TABLE IF EXISTS `sp_user_devices`;

CREATE TABLE `sp_user_devices` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `device_id` bigint(20) DEFAULT NULL,
  `device_type` enum('ios','android') DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `device_id` (`device_id`,`device_type`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `sp_user_devices_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `sp_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
