/*Table structure for table `sp_company_message` */

DROP TABLE IF EXISTS `sp_company_message`;

CREATE TABLE `sp_company_message` (
  `id` bigint(20) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `message` varchar(512) DEFAULT NULL,
  `sender_id` bigint(20) DEFAULT NULL,
  `notification_id` bigint(20) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sender_id` (`sender_id`),
  KEY `notification_id` (`notification_id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `sp_company_message_ibfk_1` FOREIGN KEY (`sender_id`) REFERENCES `sp_user` (`id`),
  CONSTRAINT `sp_company_message_ibfk_2` FOREIGN KEY (`notification_id`) REFERENCES `sp_notification` (`id`),
  CONSTRAINT `sp_company_message_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `sp_user` (`id`),
  CONSTRAINT `sp_company_message_ibfk_4` FOREIGN KEY (`modified_by`) REFERENCES `sp_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `sp_message_users` */

DROP TABLE IF EXISTS `sp_message_users`;

CREATE TABLE `sp_message_users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `receiver_id` bigint(20) DEFAULT NULL,
  `message_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `message_id` (`message_id`),
  KEY `receiver_id` (`receiver_id`),
  CONSTRAINT `sp_message_users_ibfk_1` FOREIGN KEY (`message_id`) REFERENCES `sp_company_message` (`id`),
  CONSTRAINT `sp_message_users_ibfk_2` FOREIGN KEY (`receiver_id`) REFERENCES `sp_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=167 DEFAULT CHARSET=latin1;

/*Table structure for table `sp_notification` */

DROP TABLE IF EXISTS `sp_notification`;

CREATE TABLE `sp_notification` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `is_read` int(11) DEFAULT NULL,
  `sender_id` bigint(20) DEFAULT NULL,
  `receiver_id` bigint(20) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `status` enum('active','deleted') DEFAULT 'active',
  `timesheet_id` bigint(20) DEFAULT NULL,
  `contract_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `notification_type_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sender_user` (`sender_id`),
  KEY `receiver_id` (`receiver_id`),
  KEY `notification_type_id` (`notification_type_id`),
  CONSTRAINT `sp_notification_ibfk_1` FOREIGN KEY (`sender_id`) REFERENCES `sp_user` (`id`),
  CONSTRAINT `sp_notification_ibfk_2` FOREIGN KEY (`receiver_id`) REFERENCES `sp_user` (`id`),
  CONSTRAINT `sp_notification_ibfk_3` FOREIGN KEY (`notification_type_id`) REFERENCES `sp_notification_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=886 DEFAULT CHARSET=latin1;

/*Table structure for table `sp_notification_type` */

DROP TABLE IF EXISTS `sp_notification_type`;

CREATE TABLE `sp_notification_type` (
  `id` bigint(20) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `notice` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `sp_user`   
  DROP COLUMN `notif_seen_at`, 
  ADD COLUMN `notif_seen_at` BIGINT(20) DEFAULT 0  NULL AFTER `shift_id`;