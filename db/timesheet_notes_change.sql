ALTER TABLE sp_timesheet_notes   
  ADD COLUMN `type` ENUM('user_timing','timesheet','quick_timesheet') NULL AFTER `user_timing_id`,
  DROP FOREIGN KEY `sp_timesheet_notes_ibfk_1`;