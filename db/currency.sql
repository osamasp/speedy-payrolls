ALTER TABLE `sp_currency`   
  ADD COLUMN `symbol` VARCHAR(10) NULL AFTER `name`;
  
insert  into `sp_currency`(`id`,`name`,`symbol`) values (1,'GBP','£');
insert  into `sp_currency`(`id`,`name`,`symbol`) values (2,'USD','$');
insert  into `sp_currency`(`id`,`name`,`symbol`) values (3,'Euro','€');