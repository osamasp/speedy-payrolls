UPDATE sp_cms SET content = ' <style>a { color: #348eda; }  .btn-primary { text-decoration: none; color: #FFF; 
 background-color: #348eda; border: solid #348eda; border-width: 10px 20px; line-height: 2;
 font-weight: bold; margin-right: 10px; text-align: center; cursor: pointer; display: inline-block;
 border-radius: 25px; }  .btn-secondary { text-decoration: none; color: #FFF; background-color: #aaa;
 border: solid #aaa; border-width: 10px 20px; line-height: 2; font-weight: bold; margin-right: 10px; 
 text-align: center; cursor: pointer; display: inline-block; border-radius: 25px; } 
 .last { margin-bottom: 0; }  .first { margin-top: 0; }  .padding { padding: 10px 0; }
 table.body-wrap{ width: 100%; padding: 20px; }  table.body-wrap .container { border: 1px solid #f0f0f0; } 
 table.footer-wrap { width: 100%; clear: both!important; }  .footer-wrap .container p { font-size: 12px; color: #666;  }
 table.footer-wrap a { color: #999; }  
 h1, h2, h3 { font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; color: #000; 
 margin: 40px 0 10px; line-height: 1.2; font-weight: 200; }  h1 { font-size: 36px; } h2 { font-size:28px; }
 h3 { font-size: 22px; }  p, ul, ol { margin-bottom: 10px; font-weight: normal; font-size: 14px; }  
 ul li, ol li { margin-left: 5px; list-style-position: inside; }   
 .container { display: block!important; max-width: 600px!important; margin: 0 auto!important; 
.body-wrap .container { padding: 20px; }  
.content { max-width: 600px; margin: 0 auto; display: block; }  .content table { width: 100%; }  </style> 
 <div bgcolor="#f6f6f6">   <table class="body-wrap" bgcolor="#f6f6f6"> 
 <tr> <td></td> <td class="container" bgcolor="#FFFFFF">  <!-- content --> <div class="content">
 <table> <tr> <td> <p>Dear Administrator,</p> <p>[{NAME}] has requested for the following enquiry on DirectInterconnect Portal.</p> 
<p>Subject: [{SUBJECT}]</p> <p>Message: [{MESSAGE}]
 <h2>User Details</h2>   <p>Name: [{NAME}]</p> <p>Email: [{EMAIL}]
 <h1>Next Steps</h1> <p>Please click on the link below to login </p>
  <table> <tr> <td class="padding">
 <p><a href="[{LINK}]" class="btn-primary">Click here to login</a></p> </td> </tr> </table>
 </p><h2>Report Abuse</h2> <p>If you do not approve of this email and would like to report it to Speedy Administrators then contact us on the email address below:</p> <p>info@speedypayrolls.com</p> 
 <p>Speedy Payrolls provides paperless timekeeping and payrolling.</p>  </td> </tr> </table> 
 </div>   </td> <td></td> </tr> </table> 
 <table class="footer-wrap"> <tr>
 <td></td> <td class="container"> 
 <div class="content"> <table> <tr> <td align="center"> <p>Don\'t like these annoying emails? 
 </td> <td></td> </tr> </table> </div>'
WHERE slug = 'help-email';



update sp_cms set content = ' <style>a { color: #348eda; }  .btn-primary { text-decoration: none; color: #FFF; 
 background-color: #348eda; border: solid #348eda; border-width: 10px 20px; line-height: 2;
 font-weight: bold; margin-right: 10px; text-align: center; cursor: pointer; display: inline-block;
 border-radius: 25px; }  .btn-secondary { text-decoration: none; color: #FFF; background-color: #aaa;
 border: solid #aaa; border-width: 10px 20px; line-height: 2; font-weight: bold; margin-right: 10px; 
 text-align: center; cursor: pointer; display: inline-block; border-radius: 25px; } 
 .last { margin-bottom: 0; }  .first { margin-top: 0; }  .padding { padding: 10px 0; }
 table.body-wrap{ width: 100%; padding: 20px; }  table.body-wrap .container { border: 1px solid #f0f0f0; } 
 table.footer-wrap { width: 100%; clear: both!important; }  .footer-wrap .container p { font-size: 12px; color: #666;  }
 table.footer-wrap a { color: #999; }  
 h1, h2, h3 { font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; color: #000; 
 margin: 40px 0 10px; line-height: 1.2; font-weight: 200; }  h1 { font-size: 36px; } h2 { font-size:28px; }
 h3 { font-size: 22px; }  p, ul, ol { margin-bottom: 10px; font-weight: normal; font-size: 14px; }  
 ul li, ol li { margin-left: 5px; list-style-position: inside; }   
 .container { display: block!important; max-width: 600px!important; margin: 0 auto!important; 
.body-wrap .container { padding: 20px; }  
.content { max-width: 600px; margin: 0 auto; display: block; }  .content table { width: 100%; }  </style> 
<div bgcolor="#f6f6f6">  <table class="body-wrap" bgcolor="#f6f6f6"> 
 <tr> <td></td> <td class="container" bgcolor="#FFFFFF">   <div class="content">
 <table> <tr> <td> <p>[{BODY}]</p>
 </table> 
 <h1>Next Steps</h1> <p>Please click on the link below to login </p>
  <table> <tr> <td class="padding">
 <p><a href="[{LOGIN_URL}]" class="btn-primary">Click here to login</a></p> </td> </tr> </table>
 <h2>Report Abuse</h2> <p>If you do not approve of this email and would like to report it to Speedy Administrators then contact us on the email address below:</p> <p>info@speedypayrolls.com</p> 
 <p>Speedy Payrolls provides paperless timekeeping and payrolling.</p>  </td> </tr> </table> 
 </div>   </td> <td></td> </tr> </table> 
 <table class="footer-wrap"> <tr> <td></td> <td class="container">  
 <div class="content"> <table> <tr> <td align="center"> <p>Don\'t like these annoying emails? 
 <a href="#"><unsubscribe>Unsubscribe</unsubscribe></a>. </p> </td> </tr> </table> </div> 
 </td> <td></td> </tr> </table>  </div> '
 where slug='Email';
 
 


update sp_cms set content = ' <style>a { color: #348eda; }  .btn-primary { text-decoration: none; color: #FFF; 
 background-color: #348eda; border: solid #348eda; border-width: 10px 20px; line-height: 2;
 font-weight: bold; margin-right: 10px; text-align: center; cursor: pointer; display: inline-block;
 border-radius: 25px; }  .btn-secondary { text-decoration: none; color: #FFF; background-color: #aaa;
 border: solid #aaa; border-width: 10px 20px; line-height: 2; font-weight: bold; margin-right: 10px; 
 text-align: center; cursor: pointer; display: inline-block; border-radius: 25px; } 
 .last { margin-bottom: 0; }  .first { margin-top: 0; }  .padding { padding: 10px 0; }
 table.body-wrap{ width: 100%; padding: 20px; }  table.body-wrap .container { border: 1px solid #f0f0f0; } 
 table.footer-wrap { width: 100%; clear: both!important; }  .footer-wrap .container p { font-size: 12px; color: #666;  }
 table.footer-wrap a { color: #999; }  
 h1, h2, h3 { font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; color: #000; 
 margin: 40px 0 10px; line-height: 1.2; font-weight: 200; }  h1 { font-size: 36px; } h2 { font-size:28px; }
 h3 { font-size: 22px; }  p, ul, ol { margin-bottom: 10px; font-weight: normal; font-size: 14px; }  
 ul li, ol li { margin-left: 5px; list-style-position: inside; }   
 .container { display: block!important; max-width: 600px!important; margin: 0 auto!important; 
.body-wrap .container { padding: 20px; }  
.content { max-width: 600px; margin: 0 auto; display: block; }  .content table { width: 100%; }  </style> 
<div bgcolor="#f6f6f6">   <table class="body-wrap" bgcolor="#f6f6f6"> 
 <tr> <td></td> <td class="container" bgcolor="#FFFFFF"> <div class="content">
 <table> <tr> <td> <p>Dear [{NAME}]</p> <p> Thank you for signing up with SpeedyPayrolls. Click the link below to verify your account and start using SpeedyPayrolls.</p> <h1>Next Steps</h1> <p>Please click below to complete the verification process:</p> 
  <h4>Email Address you registered with:</h4> <p>Email: [{EMAIL}]</p> 
  <table> <tr> <td class="padding"><a href="[{CONFIRMLINK}]" class="btn-primary">Verify my email</a></p> </td> 
 <br><p>NOTE: Please do not click on the verification link if you did not register for Speedy Payrolls. Please refer to the note below:</p>
</tr> </table> <h2>Report Abuse</h2> <p>If you do not approve of this email and would like to report it to Speedy Administrators then contact us on the email address below:</p> <p>info@speedypayrolls.com</p> 
 <p>Speedy Payrolls provides paperless timekeeping and payrolling.</p>  </td> </tr> </table> 
 </div> <!-- /content -->  </td> <td></td> </tr> </table> 
 <table class="footer-wrap"> <tr> <td></td> <td class="container">  
 <div class="content"> <table> <tr> <td align="center"> <p>Don\'t like these annoying emails? 
 <a href="#"><unsubscribe>Unsubscribe</unsubscribe></a>. </p> </td> </tr> </table> </div>
 </td> <td></td> </tr> </table>  </div> '
 where slug='ConfirmEmail';
 
 
 


update sp_cms set content = ' <style>a { color: #348eda; }  .btn-primary { text-decoration: none; color: #FFF; 
 background-color: #348eda; border: solid #348eda; border-width: 10px 20px; line-height: 2;
 font-weight: bold; margin-right: 10px; text-align: center; cursor: pointer; display: inline-block;
 border-radius: 25px; }  .btn-secondary { text-decoration: none; color: #FFF; background-color: #aaa;
 border: solid #aaa; border-width: 10px 20px; line-height: 2; font-weight: bold; margin-right: 10px; 
 text-align: center; cursor: pointer; display: inline-block; border-radius: 25px; } 
 .last { margin-bottom: 0; }  .first { margin-top: 0; }  .padding { padding: 10px 0; }
 table.body-wrap{ width: 100%; padding: 20px; }  table.body-wrap .container { border: 1px solid #f0f0f0; } 
 table.footer-wrap { width: 100%; clear: both!important; }  .footer-wrap .container p { font-size: 12px; color: #666;  }
 table.footer-wrap a { color: #999; }  
 h1, h2, h3 { font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; color: #000; 
 margin: 40px 0 10px; line-height: 1.2; font-weight: 200; }  h1 { font-size: 36px; } h2 { font-size:28px; }
 h3 { font-size: 22px; }  p, ul, ol { margin-bottom: 10px; font-weight: normal; font-size: 14px; }  
 ul li, ol li { margin-left: 5px; list-style-position: inside; }   
 .container { display: block!important; max-width: 600px!important; margin: 0 auto!important; 
.body-wrap .container { padding: 20px; }  
.content { max-width: 600px; margin: 0 auto; display: block; }  .content table { width: 100%; }  </style> 
<div bgcolor="#f6f6f6"> <table class="body-wrap" bgcolor="#f6f6f6"> 
 <tr> <td></td> <td class="container" bgcolor="#FFFFFF">  <div class="content">
 <table> <tr> <td> <p>Dear [{NAME}]</p> <p>  Your new password is : [{PASSWORD}]</p>
 </table> 
 <h1>Next Steps</h1> <p>Please click on the link below to login </p>
  <table> <tr> <td class="padding">
 <p><a href="[{LOGIN_URL}]" class="btn-primary">Click here to login</a></p> </td> </tr> </table>
 <h2>Report Abuse</h2> <p>If you do not approve of this email and would like to report it to Speedy Administrators then contact us on the email address below:</p> <p>info@speedypayrolls.com</p> 
 <p>Speedy Payrolls provides paperless timekeeping and payrolling.</p>  </td> </tr> </table> 
 </div> </td> <td></td> </tr> </table> 
 <table class="footer-wrap"> <tr> <td></td> <td class="container">  <!-- content --> 
 <div class="content"> <table> <tr> <td align="center"> <p>Don\'t like these annoying emails? 
 <a href="#"><unsubscribe>Unsubscribe</unsubscribe></a>. </p> </td> </tr> </table> </div> 
 </td> <td></td> </tr> </table></div> '
 where slug='ResetPassword';
 
 
 update sp_cms set content = ' <style>a { color: #348eda; }  .btn-primary { text-decoration: none; color: #FFF; 
 background-color: #348eda; border: solid #348eda; border-width: 10px 20px; line-height: 2;
 font-weight: bold; margin-right: 10px; text-align: center; cursor: pointer; display: inline-block;
 border-radius: 25px; }  .btn-secondary { text-decoration: none; color: #FFF; background-color: #aaa;
 border: solid #aaa; border-width: 10px 20px; line-height: 2; font-weight: bold; margin-right: 10px; 
 text-align: center; cursor: pointer; display: inline-block; border-radius: 25px; } 
 .last { margin-bottom: 0; }  .first { margin-top: 0; }  .padding { padding: 10px 0; }
 table.body-wrap{ width: 100%; padding: 20px; }  table.body-wrap .container { border: 1px solid #f0f0f0; } 
 table.footer-wrap { width: 100%; clear: both!important; }  .footer-wrap .container p { font-size: 12px; color: #666;  }
 table.footer-wrap a { color: #999; }  
 h1, h2, h3 { font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; color: #000; 
 margin: 40px 0 10px; line-height: 1.2; font-weight: 200; }  h1 { font-size: 36px; } h2 { font-size:28px; }
 h3 { font-size: 22px; }  p, ul, ol { margin-bottom: 10px; font-weight: normal; font-size: 14px; }  
 ul li, ol li { margin-left: 5px; list-style-position: inside; }   
 .container { display: block!important; max-width: 600px!important; margin: 0 auto!important; 
.body-wrap .container { padding: 20px; }  
.content { max-width: 600px; margin: 0 auto; display: block; }  .content table { width: 100%; }  </style> 
<div bgcolor="#f6f6f6">  <table class="body-wrap" bgcolor="#f6f6f6"> 
 <tr> <td></td> <td class="container" bgcolor="#FFFFFF"> <div class="content">
 <table> <tr> <td> <p>Dear [{NAME}]</p> <p>  [{MESSAGE}]</p>
 </table>
<h1>Next Steps</h1> <p>Please click on the link below to login </p>
  <table> <tr> <td class="padding">
 <p><a href="[{LOGIN_URL}]" class="btn-primary">Click here to login</a></p> </td> </tr> </table>
 <h2>Report Abuse</h2> <p>If you do not approve of this email and would like to report it to Speedy Administrators then contact us on the email address below:</p> <p>info@speedypayrolls.com</p> 
 <p>Speedy Payrolls provides paperless timekeeping and payrolling.</p>  </td> </tr> </table> 
 </div> <!-- /content -->  </td> <td></td> </tr> </table> 
 <table class="footer-wrap"> <tr> <td></td> <td class="container"> 
 <div class="content"> <table> <tr> <td align="center"> <p>Don\'t like these annoying emails? 
 <a href="#"><unsubscribe>Unsubscribe</unsubscribe></a>. </p> </td> </tr> </table> </div> 
 </td> <td></td> </tr> </table>  </div> '
 where slug='timesheet-notify';
 
 

UPDATE sp_cms SET content = ' <style>a { color: #348eda; }  .btn-primary { text-decoration: none; color: #FFF; 
 background-color: #348eda; border: solid #348eda; border-width: 10px 20px; line-height: 2;
 font-weight: bold; margin-right: 10px; text-align: center; cursor: pointer; display: inline-block;
 border-radius: 25px; }  .btn-secondary { text-decoration: none; color: #FFF; background-color: #aaa;
 border: solid #aaa; border-width: 10px 20px; line-height: 2; font-weight: bold; margin-right: 10px; 
 text-align: center; cursor: pointer; display: inline-block; border-radius: 25px; } 
 .last { margin-bottom: 0; }  .first { margin-top: 0; }  .padding { padding: 10px 0; }
 table.body-wrap{ width: 100%; padding: 20px; }  table.body-wrap .container { border: 1px solid #f0f0f0; } 
 table.footer-wrap { width: 100%; clear: both!important; }  .footer-wrap .container p { font-size: 12px; color: #666;  }
 table.footer-wrap a { color: #999; }  
 h1, h2, h3 { font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; color: #000; 
 margin: 40px 0 10px; line-height: 1.2; font-weight: 200; }  h1 { font-size: 36px; } h2 { font-size:28px; }
 h3 { font-size: 22px; }  p, ul, ol { margin-bottom: 10px; font-weight: normal; font-size: 14px; }  
 ul li, ol li { margin-left: 5px; list-style-position: inside; }   
 .container { display: block!important; max-width: 600px!important; margin: 0 auto!important; 
.body-wrap .container { padding: 20px; }  
.content { max-width: 600px; margin: 0 auto; display: block; }  .content table { width: 100%; }  </style> 
<div bgcolor="#f6f6f6">   <table class="body-wrap" bgcolor="#f6f6f6"> 
 <tr> <td></td> <td class="container" bgcolor="#FFFFFF">  <div class="content">
 <table> <tr> <td> <p>Dear [{NAME}]</p> <p> You have been invited by [{FULLNAME}] to join SpeedyPayrolls.</p> <h1>Next Steps</h1> 
<table> <tr> <td class="padding"> <p>Click the link below to complete your registration</td></tr>
<tr><td><a href="[{RESETLINK}]" class="btn-primary">[{RESETLINK}]</a></p> </td> </tr> </table> 
<p> If you wish to learn more about this system, the please visit SpeedyPayrolls.com for more information.</p>
<h2>Report Abuse</h2> <p>If you do not approve of this email and would like to report it to Speedy Administrators then contact us on the email address below:</p> <p>info@speedypayrolls.com</p> 
 <p>Speedy Payrolls provides paperless timekeeping and payrolling.</p>  </td> </tr> </table> 
 </div>  </td> <td></td> </tr> </table>
 <table class="footer-wrap"> <tr> <td></td> <td class="container"> 
 <div class="content"> <table> <tr> <td align="center"> <p>Don\'t like these annoying emails? 
 <a href="#"><unsubscribe>Unsubscribe</unsubscribe></a>. </p> </td> </tr> </table> </div> 
 </td> <td></td> </tr> </table>  </div> '
 WHERE slug='invitation';
 
 
 UPDATE sp_cms SET content = ' <style>a { color: #348eda; }  .btn-primary { text-decoration: none; color: #FFF; 
 background-color: #348eda; border: solid #348eda; border-width: 10px 20px; line-height: 2;
 font-weight: bold; margin-right: 10px; text-align: center; cursor: pointer; display: inline-block;
 border-radius: 25px; }  .btn-secondary { text-decoration: none; color: #FFF; background-color: #aaa;
 border: solid #aaa; border-width: 10px 20px; line-height: 2; font-weight: bold; margin-right: 10px; 
 text-align: center; cursor: pointer; display: inline-block; border-radius: 25px; } 
 .last { margin-bottom: 0; }  .first { margin-top: 0; }  .padding { padding: 10px 0; }
 table.body-wrap{ width: 100%; padding: 20px; }  table.body-wrap .container { border: 1px solid #f0f0f0; } 
 table.footer-wrap { width: 100%; clear: both!important; }  .footer-wrap .container p { font-size: 12px; color: #666;  }
 table.footer-wrap a { color: #999; }  
 h1, h2, h3 { font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; color: #000; 
 margin: 40px 0 10px; line-height: 1.2; font-weight: 200; }  h1 { font-size: 36px; } h2 { font-size:28px; }
 h3 { font-size: 22px; }  p, ul, ol { margin-bottom: 10px; font-weight: normal; font-size: 14px; }  
 ul li, ol li { margin-left: 5px; list-style-position: inside; }   
 .container { display: block!important; max-width: 600px!important; margin: 0 auto!important; 
.body-wrap .container { padding: 20px; }  
.content { max-width: 600px; margin: 0 auto; display: block; }  .content table { width: 100%; }  </style> 
 <div bgcolor="#f6f6f6">  <table class="body-wrap" bgcolor="#f6f6f6"> 
 <tr> <td></td> <td class="container" bgcolor="#FFFFFF">  
 <div class="content">
 <table> <tr> <td> <p>Dear [{USERNAME}],</p> <p> Welcome to SpeedyPayrolls. Your SpeedyPayrolls account was created by [{CREATOR}]. Your login details are below:</p>
<h2>Access Details</h2> <p>Username: [{USERNAME}]</p> <p>Password: [{PASSWORD}]</p>
 <table> <tr> <td class="padding">
 <p><a href="[{LOGIN_URL}]" class="btn-primary">Click here to login</a></p> </td> </tr> </table>
<h2>Report Abuse</h2> <p>If you do not approve of this email and would like to report it to Speedy Administrators then contact us on the email address below:</p> <p>info@speedypayrolls.com</p> 
 <p>Speedy Payrolls provides paperless timekeeping and payrolling.</p>  </td> </tr> </table> 
 </div>  </td> <td></td> </tr> </table>  
 <table class="footer-wrap"> <tr> <td></td> <td class="container">  
 <div class="content"> <table> <tr> <td align="center"> <p>Don\'t like these annoying emails? 
 <a href="#"><unsubscribe>Unsubscribe</unsubscribe></a>. </p> </td> </tr> </table> </div> 
 </td> <td></td> </tr> </table>  </div> '
 WHERE slug='addemployee';
 
 
update sp_cms set content = ' <style>a { color: #348eda; }  .btn-primary { text-decoration: none; color: #FFF; 
 background-color: #348eda; border: solid #348eda; border-width: 10px 20px; line-height: 2;
 font-weight: bold; margin-right: 10px; text-align: center; cursor: pointer; display: inline-block;
 border-radius: 25px; }  .btn-secondary { text-decoration: none; color: #FFF; background-color: #aaa;
 border: solid #aaa; border-width: 10px 20px; line-height: 2; font-weight: bold; margin-right: 10px; 
 text-align: center; cursor: pointer; display: inline-block; border-radius: 25px; } 
 .last { margin-bottom: 0; }  .first { margin-top: 0; }  .padding { padding: 10px 0; }
 table.body-wrap{ width: 100%; padding: 20px; }  table.body-wrap .container { border: 1px solid #f0f0f0; } 
 table.footer-wrap { width: 100%; clear: both!important; }  .footer-wrap .container p { font-size: 12px; color: #666;  }
 table.footer-wrap a { color: #999; }  
 h1, h2, h3 { font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; color: #000; 
 margin: 40px 0 10px; line-height: 1.2; font-weight: 200; }  h1 { font-size: 36px; } h2 { font-size:28px; }
 h3 { font-size: 22px; }  p, ul, ol { margin-bottom: 10px; font-weight: normal; font-size: 14px; }  
 ul li, ol li { margin-left: 5px; list-style-position: inside; }   
 .container { display: block!important; max-width: 600px!important; margin: 0 auto!important; 
.body-wrap .container { padding: 20px; }  
.content { max-width: 600px; margin: 0 auto; display: block; }  .content table { width: 100%; }  </style> 
<div bgcolor="#f6f6f6">  <!-- body --> <table class="body-wrap" bgcolor="#f6f6f6"> 
 <tr> <td></td> <td class="container" bgcolor="#FFFFFF">  <div class="content">
 <table> <tr> <td> <p>[{BODY}]</p>
 </table> 
 <h1>Next Steps</h1> <p>Please click on the link below to login </p>
  <table> <tr> <td class="padding">
 <p><a href="[{LOGIN_URL}]" class="btn-primary">Click here to login</a></p> </td> </tr> </table>
 <h2>Report Abuse</h2> <p>If you do not approve of this email and would like to report it to Speedy Administrators then contact us on the email address below:</p> <p>info@speedypayrolls.com</p> 
 <p>Speedy Payrolls provides paperless timekeeping and payrolling.</p>  </td> </tr> </table> 
 </div></td> <td></td> </tr> </table> 
 <table class="footer-wrap"> <tr> <td></td> <td class="container"> 
 <div class="content"> <table> <tr> <td align="center"> <p>Don\'t like these annoying emails? 
 <a href="#"><unsubscribe>Unsubscribe</unsubscribe></a>. </p> </td> </tr> </table> </div> 
 </td> <td></td> </tr> </table> </div> '
 where slug='SpeedyPayrolls Notification';
 
 


UPDATE sp_cms SET content = ' <style>a { color: #348eda; }  .btn-primary { text-decoration: none; color: #FFF; 
 background-color: #348eda; border: solid #348eda; border-width: 10px 20px; line-height: 2;
 font-weight: bold; margin-right: 10px; text-align: center; cursor: pointer; display: inline-block;
 border-radius: 25px; }  .btn-secondary { text-decoration: none; color: #FFF; background-color: #aaa;
 border: solid #aaa; border-width: 10px 20px; line-height: 2; font-weight: bold; margin-right: 10px; 
 text-align: center; cursor: pointer; display: inline-block; border-radius: 25px; } 
 .last { margin-bottom: 0; }  .first { margin-top: 0; }  .padding { padding: 10px 0; }
 table.body-wrap{ width: 100%; padding: 20px; }  table.body-wrap .container { border: 1px solid #f0f0f0; } 
 table.footer-wrap { width: 100%; clear: both!important; }  .footer-wrap .container p { font-size: 12px; color: #666;  }
 table.footer-wrap a { color: #999; }  
 h1, h2, h3 { font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; color: #000; 
 margin: 40px 0 10px; line-height: 1.2; font-weight: 200; }  h1 { font-size: 36px; } h2 { font-size:28px; }
 h3 { font-size: 22px; }  p, ul, ol { margin-bottom: 10px; font-weight: normal; font-size: 14px; }  
 ul li, ol li { margin-left: 5px; list-style-position: inside; }   
 .container { display: block!important; max-width: 600px!important; margin: 0 auto!important; 
.body-wrap .container { padding: 20px; }  
.content { max-width: 600px; margin: 0 auto; display: block; }  .content table { width: 100%; }  </style> 
<div bgcolor="#f6f6f6">  <!-- body --> <table class="body-wrap" bgcolor="#f6f6f6"> 
 <tr> <td></td> <td class="container" bgcolor="#FFFFFF">   <div class="content">
 <table> <tr> <td> <p>[{INVOICE}]</p>
 </table> 
 <h1>Next Steps</h1> <p>Please click on the link below to login </p>
  <table> <tr> <td class="padding">
 <p><a href="[{LOGIN_URL}]" class="btn-primary">Click here to login</a></p> </td> </tr> </table>
 <h2>Report Abuse</h2> <p>If you do not approve of this email and would like to report it to Speedy Administrators then contact us on the email address below:</p> <p>info@speedypayrolls.com</p> 
 <p>Speedy Payrolls provides paperless timekeeping and payrolling.</p>  </td> </tr> </table> 
 </div> </td> <td></td> </tr> </table>
 <table class="footer-wrap"> <tr> <td></td> <td class="container"> 
 <div class="content"> <table> <tr> <td align="center"> <p>Don\'t like these annoying emails? 
 <a href="#"><unsubscribe>Unsubscribe</unsubscribe></a>. </p> </td> </tr> </table> </div>
 </td> <td></td> </tr> </table>  </div> '
 WHERE slug='invoice';
