DROP TABLE IF EXISTS `sp_company_branch`;

CREATE TABLE `sp_company_branch` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `address` varchar(255) NOT NULL,
  `radius` double NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` bigint(20) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_at` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_sp_company_branch` (`company_id`),
  KEY `FK_sp_company_branch_2` (`created_by`),
  KEY `FK_sp_company_branch_3` (`modified_by`),
  CONSTRAINT `FK_sp_company_branch` FOREIGN KEY (`company_id`) REFERENCES `sp_company` (`id`),
  CONSTRAINT `FK_sp_company_branch_2` FOREIGN KEY (`created_by`) REFERENCES `sp_user` (`id`),
  CONSTRAINT `FK_sp_company_branch_3` FOREIGN KEY (`modified_by`) REFERENCES `sp_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
