

CREATE TABLE `sp_company_branch` (
  `id` BIGINT(30) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `latitude` DOUBLE NOT NULL,
  `longitude` DOUBLE NOT NULL,
  `address` VARCHAR(255) NOT NULL,
  `diameter` DOUBLE NOT NULL,
  `branch_admin_id` BIGINT(30) NOT NULL,
  `company_id` BIGINT(30) NOT NULL,
  `created_at` BIGINT(20) NOT NULL,
  `created_by` BIGINT(20) NOT NULL,
  `modified_by` BIGINT(20) NOT NULL,
  `modified_at` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_sp_company_branch_1` (`branch_admin_id`),
  KEY `FK_sp_company_branch` (`company_id`),
  KEY `FK_sp_company_branch_2` (`created_by`),
  KEY `FK_sp_company_branch_3` (`modified_by`),
  CONSTRAINT `FK_sp_company_branch` FOREIGN KEY (`company_id`) REFERENCES `sp_company` (`id`),
  CONSTRAINT `FK_sp_company_branch_1` FOREIGN KEY (`branch_admin_id`) REFERENCES `sp_user` (`id`),
  CONSTRAINT `FK_sp_company_branch_2` FOREIGN KEY (`created_by`) REFERENCES `sp_user` (`id`),
  CONSTRAINT `FK_sp_company_branch_3` FOREIGN KEY (`modified_by`) REFERENCES `sp_user` (`id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1

