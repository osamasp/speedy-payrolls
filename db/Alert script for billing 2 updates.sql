ALTER TABLE `sp_company`   
  CHANGE `last_bill_time` `ts_reset_time` BIGINT(20) DEFAULT 0  NOT NULL,
  ADD COLUMN `ps_reset_time` BIGINT DEFAULT 0  NOT NULL AFTER `ts_reset_time`,
  ADD COLUMN `contract_note` TEXT NULL AFTER `ps_reset_time`;
