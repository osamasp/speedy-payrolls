/*
SQLyog Ultimate v8.55 
MySQL - 5.5.20-log : Database - speedy_payroll
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `sp_company` */

DROP TABLE IF EXISTS `sp_company`;

CREATE TABLE `sp_company` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `sector` varchar(255) NOT NULL,
  `registration_number` varchar(255) DEFAULT NULL,
  `vat_number` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `post_code` varchar(10) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` bigint(20) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `modified_at` bigint(20) NOT NULL,
  `modified_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `sp_company_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `sp_user` (`id`),
  CONSTRAINT `sp_company_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `sp_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sp_company` */

insert  into `sp_company`(`id`,`name`,`logo`,`email`,`phone`,`sector`,`registration_number`,`vat_number`,`address`,`street`,`city`,`country`,`post_code`,`type`,`status`,`created_at`,`created_by`,`modified_at`,`modified_by`) values ('0','Siliconplex',NULL,'test@siliconplex.com',NULL,'Information Technology',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0,'0',0,'0'),('B53C9F4E-A88B-AC0D-AB1F-D4A338587AF9','Siliconplex',NULL,'info@siliconplex.com','7788996654','IT',NULL,'11223344','','University Road','Karachi','Pakistan','75850',1,1,1423574732,'0',1423574732,'0'),('B53C9F4E-A88B-AC0D-AB1F-D4A338587CC112','Nimble 3',NULL,'info@nimble3.com','456465','Information Technology',NULL,'2313123123',NULL,NULL,'London','United Kingdom','6544',1,1,2121313,'0',0,'0');

/*Table structure for table `sp_contact` */

DROP TABLE IF EXISTS `sp_contact`;

CREATE TABLE `sp_contact` (
  `id` varchar(255) NOT NULL,
  `company_id` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `type` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` bigint(20) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `modified_at` bigint(20) NOT NULL,
  `modified_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`email`),
  KEY `company_id` (`company_id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `sp_contact_ibfk_1` FOREIGN KEY (`email`) REFERENCES `sp_user` (`id`),
  CONSTRAINT `sp_contact_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `sp_company` (`id`),
  CONSTRAINT `sp_contact_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `sp_user` (`id`),
  CONSTRAINT `sp_contact_ibfk_4` FOREIGN KEY (`modified_by`) REFERENCES `sp_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sp_contact` */

/*Table structure for table `sp_contract` */

DROP TABLE IF EXISTS `sp_contract`;

CREATE TABLE `sp_contract` (
  `id` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `company_id` varchar(255) NOT NULL,
  `role` varchar(255) DEFAULT NULL,
  `employee_type` enum('full_time','part_time') DEFAULT NULL,
  `approver_id` varchar(255) DEFAULT NULL,
  `payroller_id` varchar(255) DEFAULT NULL,
  `type` enum('in','out','in_out','out_b') NOT NULL,
  `policy` text,
  `start_time` bigint(20) NOT NULL,
  `end_time` bigint(20) NOT NULL,
  `status` int(11) NOT NULL,
  `invitation_code` varchar(255) DEFAULT NULL,
  `created_at` bigint(20) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `modified_at` bigint(20) NOT NULL,
  `modified_by` varchar(255) NOT NULL,
  `timesheetTypeSel` enum('daily','weekly','monthly') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `company_id` (`company_id`),
  KEY `approver_id` (`approver_id`),
  KEY `payroller_id` (`payroller_id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `sp_contract_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `sp_user` (`id`),
  CONSTRAINT `sp_contract_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `sp_company` (`id`),
  CONSTRAINT `sp_contract_ibfk_3` FOREIGN KEY (`approver_id`) REFERENCES `sp_user` (`id`),
  CONSTRAINT `sp_contract_ibfk_4` FOREIGN KEY (`payroller_id`) REFERENCES `sp_user` (`id`),
  CONSTRAINT `sp_contract_ibfk_5` FOREIGN KEY (`created_by`) REFERENCES `sp_user` (`id`),
  CONSTRAINT `sp_contract_ibfk_6` FOREIGN KEY (`modified_by`) REFERENCES `sp_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sp_contract` */

insert  into `sp_contract`(`id`,`user_id`,`company_id`,`role`,`employee_type`,`approver_id`,`payroller_id`,`type`,`policy`,`start_time`,`end_time`,`status`,`invitation_code`,`created_at`,`created_by`,`modified_at`,`modified_by`,`timesheetTypeSel`) values ('010D001D-C6D1-766B-FE07-95D2B2C52E22','BFA13392-3780-954E-3513-723E29B98EFF','B53C9F4E-A88B-AC0D-AB1F-D4A338587AF9',NULL,'full_time','BFA13392-3780-954E-3513-723E29B98EFF',NULL,'in',NULL,1423574732,-1,1,NULL,1423574732,'0',1423574732,'0','daily'),('010D001D-C6D1-766B-FE07-95D2B2C5CCCCC','BFA13392-3780-954E-3513-723E29B98EFF','B53C9F4E-A88B-AC0D-AB1F-D4A338587AF9',NULL,'part_time','BFA13392-3780-954E-3513-723E29B98EFF',NULL,'out',NULL,1423574732,-1,1,NULL,1423574732,'0',1423574732,'0','weekly'),('010D001D-C6D1-766B-FE07-95D2B2C5DD112','BFA13392-3780-954E-3513-723E29B98EFF','B53C9F4E-A88B-AC0D-AB1F-D4A338587CC112',NULL,'full_time','BFA13392-3780-954E-3513-723E29B98EFF',NULL,'in_out',NULL,455644646,-1,1,NULL,1423574732,'0',1423574732,'0','monthly');

/*Table structure for table `sp_logging` */

DROP TABLE IF EXISTS `sp_logging`;

CREATE TABLE `sp_logging` (
  `id` varchar(255) NOT NULL,
  `action` text NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `time` bigint(20) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `sp_logging_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `sp_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sp_logging` */

/*Table structure for table `sp_payrate` */

DROP TABLE IF EXISTS `sp_payrate`;

CREATE TABLE `sp_payrate` (
  `id` varchar(255) NOT NULL,
  `timesheet_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` bigint(20) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `modified_at` bigint(20) NOT NULL,
  `modified_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `timesheet_id` (`timesheet_id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `sp_payrate_ibfk_1` FOREIGN KEY (`timesheet_id`) REFERENCES `sp_timesheet` (`id`),
  CONSTRAINT `sp_payrate_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `sp_user` (`id`),
  CONSTRAINT `sp_payrate_ibfk_3` FOREIGN KEY (`modified_by`) REFERENCES `sp_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sp_payrate` */

/*Table structure for table `sp_timesheet` */

DROP TABLE IF EXISTS `sp_timesheet`;

CREATE TABLE `sp_timesheet` (
  `id` varchar(255) NOT NULL,
  `contract_id` varchar(255) NOT NULL,
  `type` enum('daily','weekly','monthly') NOT NULL,
  `start_time` bigint(20) NOT NULL,
  `end_time` bigint(20) NOT NULL,
  `time` float(10,2) DEFAULT NULL,
  `overtime` float(10,2) DEFAULT NULL,
  `lunchtime` bigint(20) DEFAULT NULL,
  `status` enum('pending','current','approved') NOT NULL DEFAULT 'pending',
  `created_at` bigint(20) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `modified_at` bigint(20) NOT NULL,
  `modified_by` varchar(255) NOT NULL,
  `parent` varchar(255) DEFAULT NULL,
  `expense` float(10,4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contract_id` (`contract_id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  KEY `FK_sp_timesheet` (`parent`),
  CONSTRAINT `FK_sp_timesheet` FOREIGN KEY (`parent`) REFERENCES `sp_timesheet` (`id`) ON DELETE CASCADE,
  CONSTRAINT `sp_timesheet_ibfk_1` FOREIGN KEY (`contract_id`) REFERENCES `sp_contract` (`id`),
  CONSTRAINT `sp_timesheet_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `sp_user` (`id`),
  CONSTRAINT `sp_timesheet_ibfk_3` FOREIGN KEY (`modified_by`) REFERENCES `sp_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sp_timesheet` */

insert  into `sp_timesheet`(`id`,`contract_id`,`type`,`start_time`,`end_time`,`time`,`overtime`,`lunchtime`,`status`,`created_at`,`created_by`,`modified_at`,`modified_by`,`parent`,`expense`) values ('245C8A73-073A-41EB-B61F-DF38B0922C17','010D001D-C6D1-766B-FE07-95D2B2C5CCCCC','weekly',1424304000,1424476800,20.50,12.50,NULL,'pending',1424295018,'BFA13392-3780-954E-3513-723E29B98EFF',1424295018,'BFA13392-3780-954E-3513-723E29B98EFF',NULL,100.0000),('6300DC66-BAC6-4A27-8E64-B0D844F74210','010D001D-C6D1-766B-FE07-95D2B2C52E22','daily',1424685600,1424725200,NULL,15.00,15,'pending',1424373369,'BFA13392-3780-954E-3513-723E29B98EFF',1424373369,'BFA13392-3780-954E-3513-723E29B98EFF',NULL,15.0000),('DB493474-3C53-4E1D-9392-DF25D3719A9F','010D001D-C6D1-766B-FE07-95D2B2C52E22','daily',1423224000,1423249200,NULL,12.00,12,'pending',1424370866,'BFA13392-3780-954E-3513-723E29B98EFF',1424373298,'BFA13392-3780-954E-3513-723E29B98EFF',NULL,12.0000),('FD0E27AC-1252-495E-B1EA-B21280D45F84','010D001D-C6D1-766B-FE07-95D2B2C5CCCCC','weekly',1424476800,1424736000,30.00,0.00,NULL,'pending',1424295164,'BFA13392-3780-954E-3513-723E29B98EFF',1424295277,'BFA13392-3780-954E-3513-723E29B98EFF',NULL,NULL);

/*Table structure for table `sp_user` */

DROP TABLE IF EXISTS `sp_user`;

CREATE TABLE `sp_user` (
  `id` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `ni_number` varchar(255) DEFAULT NULL,
  `dob` bigint(20) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `addressline_1` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `postcode` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `type` int(50) NOT NULL,
  `created_at` bigint(20) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `modified_at` bigint(20) NOT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sp_user` */

insert  into `sp_user`(`id`,`first_name`,`last_name`,`email`,`password`,`ni_number`,`dob`,`phone`,`addressline_1`,`street`,`city`,`country`,`postcode`,`photo`,`type`,`created_at`,`created_by`,`modified_at`,`modified_by`) values ('0','Usman','Shaikh','usman_segi@yahoo.com','908fb44620910c09e1cd1d92ec901202','1122334455',0,'03132122582','B-17 Alhamra palace','Shadman Town','Karachi','Pakistan','75850',NULL,1,0,NULL,0,NULL),('BFA13392-3780-954E-3513-723E29B98EFF','Mohammad','Taha','usama.ayaz@live.com','908fb44620910c09e1cd1d92ec901202','1122334456',0,'123233','','Johar  mor','Karachi','Pakistan','75850',NULL,1,1423574732,'0',1423574732,'0');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
