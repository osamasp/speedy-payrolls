/*
SQLyog Ultimate v8.55 
MySQL - 5.5.20-log : Database - speedy_payroll
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `sp_payslip_entry` */

DROP TABLE IF EXISTS `sp_payslip_entry`;

CREATE TABLE `sp_payslip_entry` (
  `id` bigint(50) NOT NULL AUTO_INCREMENT,
  `1T` varchar(255) DEFAULT NULL,
  `1V` varchar(255) DEFAULT NULL,
  `2T` varchar(255) DEFAULT NULL,
  `2V` varchar(255) DEFAULT NULL,
  `3T` varchar(255) DEFAULT NULL,
  `3V` varchar(255) DEFAULT NULL,
  `4T` varchar(255) DEFAULT NULL,
  `4V` varchar(255) DEFAULT NULL,
  `5V` varchar(255) DEFAULT NULL,
  `5T` varchar(255) DEFAULT NULL,
  `6T` varchar(255) DEFAULT NULL,
  `6V` varchar(255) DEFAULT NULL,
  `7T` varchar(255) DEFAULT NULL,
  `7V` varchar(255) DEFAULT NULL,
  `8T` varchar(255) DEFAULT NULL,
  `8V` varchar(255) DEFAULT NULL,
  `9T` varchar(255) DEFAULT NULL,
  `9V` varchar(255) DEFAULT NULL,
  `10T` varchar(255) DEFAULT NULL,
  `10V` varchar(255) DEFAULT NULL,
  `11T` varchar(255) DEFAULT NULL,
  `11V` varchar(255) DEFAULT NULL,
  `12T` varchar(255) DEFAULT NULL,
  `12V` varchar(255) DEFAULT NULL,
  `13T` varchar(255) DEFAULT NULL,
  `13V` varchar(255) DEFAULT NULL,
  `14T` varchar(255) DEFAULT NULL,
  `14V` varchar(255) DEFAULT NULL,
  `15T` varchar(255) DEFAULT NULL,
  `15V` varchar(255) DEFAULT NULL,
  `16T` varchar(255) DEFAULT NULL,
  `16V` varchar(255) DEFAULT NULL,
  `17T` varchar(255) DEFAULT NULL,
  `17V` varchar(255) DEFAULT NULL,
  `18T` varchar(255) DEFAULT NULL,
  `18V` varchar(255) DEFAULT NULL,
  `19T` varchar(255) DEFAULT NULL,
  `19V` varchar(255) DEFAULT NULL,
  `20T` varchar(255) DEFAULT NULL,
  `20V` varchar(255) DEFAULT NULL,
  `21T` varchar(255) DEFAULT NULL,
  `21V` varchar(255) DEFAULT NULL,
  `22T` varchar(255) DEFAULT NULL,
  `22V` varchar(255) DEFAULT NULL,
  `23T` varchar(255) DEFAULT NULL,
  `23V` varchar(255) DEFAULT NULL,
  `24T` varchar(255) DEFAULT NULL,
  `24V` varchar(255) DEFAULT NULL,
  `25T` varchar(255) DEFAULT NULL,
  `25V` varchar(255) DEFAULT NULL,
  `26T` varchar(255) DEFAULT NULL,
  `26V` varchar(255) DEFAULT NULL,
  `27T` varchar(255) DEFAULT NULL,
  `27V` varchar(255) DEFAULT NULL,
  `28T` varchar(255) DEFAULT NULL,
  `28V` varchar(255) DEFAULT NULL,
  `29T` varchar(255) DEFAULT NULL,
  `29V` varchar(255) DEFAULT NULL,
  `30T` varchar(255) DEFAULT NULL,
  `30V` varchar(255) DEFAULT NULL,
  `user_id` bigint(50) DEFAULT NULL,
  `created_at` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sp_payslip_entry` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
