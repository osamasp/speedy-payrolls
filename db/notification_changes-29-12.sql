ALTER TABLE `sp_notification`   
  ADD COLUMN `status` ENUM('active','deleted') DEFAULT 'active' AFTER `modified_by` ;
ALTER TABLE `sp_notification`   
  ADD COLUMN `timesheet_id` BIGINT(20) NULL AFTER `status`,
  ADD COLUMN `contract_id` BIGINT(20) NULL AFTER `timesheet_id`,
  ADD COLUMN `notification_type_id` BIGINT(20) NULL AFTER `contract_id`;

  CREATE TABLE `sp_notification_type`(  
  `id` BIGINT(20) NOT NULL,
  `type` VARCHAR(255),
  `logo` VARCHAR(255),
  PRIMARY KEY (`id`)
);

ALTER TABLE `sp_notification`  
  DROP FOREIGN KEY `sp_notification_ibfk_3`,
  ADD FOREIGN KEY (`notification_type_id`) REFERENCES `speedy_payroll`.`sp_notification_type`(`id`);

