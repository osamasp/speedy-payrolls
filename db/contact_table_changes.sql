ALTER TABLE `vistecho_speedy_alpha`.`sp_contact`   
  ADD COLUMN `address` VARCHAR(512) NOT NULL AFTER `modified_by`,
  ADD COLUMN `post_code` VARCHAR(512) NOT NULL AFTER `address`,
  ADD COLUMN `country` VARCHAR(512) NOT NULL AFTER `post_code`;