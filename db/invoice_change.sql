ALTER TABLE sp_invoice_items  
  CHANGE `invoice_id` `invoice_id` BIGINT(20) NOT NULL, 
  ADD PRIMARY KEY (`id`);

ALTER TABLE sp_invoice_items 
  ADD FOREIGN KEY (`invoice_id`) REFERENCES `speedy_payrolls`.`sp_invoice`(`id`);

ALTER TABLE sp_invoice   
  ADD COLUMN `date` BIGINT(20) NULL AFTER `vat_number`;

ALTER TABLE sp_invoice 
  CHANGE `logo_url` `logo_url` VARCHAR(100) CHARSET latin1 COLLATE latin1_swedish_ci NULL;

ALTER TABLE sp_invoice_items  
  CHANGE `id` `id` BIGINT(20) NOT NULL;