-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 10, 2015 at 08:36 PM
-- Server version: 5.5.41-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `speedy_payroll`
--

-- --------------------------------------------------------

--
-- Table structure for table `sp_company`
--

CREATE TABLE IF NOT EXISTS `sp_company` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `sector` varchar(255) NOT NULL,
  `registration_number` varchar(255) DEFAULT NULL,
  `vat_number` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `post_code` varchar(10) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` bigint(20) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `modified_at` bigint(20) NOT NULL,
  `modified_by` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sp_company`
--

INSERT INTO `sp_company` (`id`, `name`, `logo`, `email`, `phone`, `sector`, `registration_number`, `vat_number`, `address`, `street`, `city`, `country`, `post_code`, `type`, `status`, `created_at`, `created_by`, `modified_at`, `modified_by`) VALUES
('0', 'Siliconplex', NULL, 'test@siliconplex.com', NULL, 'Information Technology', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 0, '0', 0, '0'),
('B53C9F4E-A88B-AC0D-AB1F-D4A338587AF9', 'Siliconplex', NULL, 'info@siliconplex.com', '7788996654', 'IT', NULL, '11223344', '', 'University Road', 'Karachi', 'Pakistan', '75850', 1, 1, 1423574732, '0', 1423574732, '0');

-- --------------------------------------------------------

--
-- Table structure for table `sp_contact`
--

CREATE TABLE IF NOT EXISTS `sp_contact` (
  `id` varchar(255) NOT NULL,
  `company_id` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `type` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` bigint(20) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `modified_at` bigint(20) NOT NULL,
  `modified_by` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sp_contract`
--

CREATE TABLE IF NOT EXISTS `sp_contract` (
  `id` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `company_id` varchar(255) NOT NULL,
  `role` varchar(255) DEFAULT NULL,
  `employee_type` enum('full_time','part_time') DEFAULT NULL,
  `approver_id` varchar(255) DEFAULT NULL,
  `payroller_id` varchar(255) DEFAULT NULL,
  `type` enum('in','out','in_out','out_b') NOT NULL,
  `policy` text,
  `start_time` bigint(20) NOT NULL,
  `end_time` bigint(20) NOT NULL,
  `status` int(11) NOT NULL,
  `invitation_code` varchar(255) DEFAULT NULL,
  `created_at` bigint(20) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `modified_at` bigint(20) NOT NULL,
  `modified_by` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sp_contract`
--

INSERT INTO `sp_contract` (`id`, `user_id`, `company_id`, `role`, `employee_type`, `approver_id`, `payroller_id`, `type`, `policy`, `start_time`, `end_time`, `status`, `invitation_code`, `created_at`, `created_by`, `modified_at`, `modified_by`) VALUES
('0', '0', '0', NULL, NULL, NULL, NULL, 'in', NULL, 0, 0, 1, NULL, 0, '0', 0, '0'),
('010D001D-C6D1-766B-FE07-95D2B2C52E22', 'BFA13392-3780-954E-3513-723E29B98EFF', 'B53C9F4E-A88B-AC0D-AB1F-D4A338587AF9', NULL, 'full_time', 'BFA13392-3780-954E-3513-723E29B98EFF', NULL, 'in', NULL, 1423574732, -1, 1, NULL, 1423574732, '0', 1423574732, '0');

-- --------------------------------------------------------

--
-- Table structure for table `sp_logging`
--

CREATE TABLE IF NOT EXISTS `sp_logging` (
  `id` varchar(255) NOT NULL,
  `action` text NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `time` bigint(20) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sp_payrate`
--

CREATE TABLE IF NOT EXISTS `sp_payrate` (
  `id` varchar(255) NOT NULL,
  `timesheet_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` bigint(20) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `modified_at` bigint(20) NOT NULL,
  `modified_by` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sp_timesheet`
--

CREATE TABLE IF NOT EXISTS `sp_timesheet` (
  `id` varchar(255) NOT NULL,
  `contract_id` varchar(255) NOT NULL,
  `type` enum('daily','weekly','monthly') NOT NULL,
  `start_time` bigint(20) NOT NULL,
  `end_time` bigint(20) NOT NULL,
  `time` bigint(20) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` bigint(20) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `modified_at` bigint(20) NOT NULL,
  `modified_by` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sp_user`
--

CREATE TABLE IF NOT EXISTS `sp_user` (
  `id` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `ni_number` varchar(255) DEFAULT NULL,
  `dob` bigint(20) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `addressline_1` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `postcode` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `type` int(50) NOT NULL,
  `created_at` bigint(20) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `modified_at` bigint(20) NOT NULL,
  `modified_by` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sp_user`
--

INSERT INTO `sp_user` (`id`, `first_name`, `last_name`, `email`, `password`, `ni_number`, `dob`, `phone`, `addressline_1`, `street`, `city`, `country`, `postcode`, `photo`, `type`, `created_at`, `created_by`, `modified_at`, `modified_by`) VALUES
('0', 'Usman', 'Shaikh', 'usman_segi@yahoo.com', '908fb44620910c09e1cd1d92ec901202', '1122334455', 0, '03132122582', 'B-17 Alhamra palace', 'Shadman Town', 'Karachi', 'Pakistan', '75850', NULL, 1, 0, NULL, 0, NULL),
('BFA13392-3780-954E-3513-723E29B98EFF', 'Mohammad', 'Taha', 'mohammad.taha@siliconplex.com', '908fb44620910c09e1cd1d92ec901202', '1122334456', 0, '123233', '', 'Johar  mor', 'Karachi', 'Pakistan', '75850', NULL, 1, 1423574732, '0', 1423574732, '0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sp_company`
--
ALTER TABLE `sp_company`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `email` (`email`), ADD KEY `created_by` (`created_by`), ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `sp_contact`
--
ALTER TABLE `sp_contact`
  ADD PRIMARY KEY (`id`), ADD KEY `user_id` (`email`), ADD KEY `company_id` (`company_id`), ADD KEY `created_by` (`created_by`), ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `sp_contract`
--
ALTER TABLE `sp_contract`
  ADD PRIMARY KEY (`id`), ADD KEY `user_id` (`user_id`), ADD KEY `company_id` (`company_id`), ADD KEY `approver_id` (`approver_id`), ADD KEY `payroller_id` (`payroller_id`), ADD KEY `created_by` (`created_by`), ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `sp_logging`
--
ALTER TABLE `sp_logging`
  ADD PRIMARY KEY (`id`), ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `sp_payrate`
--
ALTER TABLE `sp_payrate`
  ADD PRIMARY KEY (`id`), ADD KEY `timesheet_id` (`timesheet_id`), ADD KEY `created_by` (`created_by`), ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `sp_timesheet`
--
ALTER TABLE `sp_timesheet`
  ADD PRIMARY KEY (`id`), ADD KEY `contract_id` (`contract_id`), ADD KEY `created_by` (`created_by`), ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `sp_user`
--
ALTER TABLE `sp_user`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `email` (`email`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `sp_company`
--
ALTER TABLE `sp_company`
ADD CONSTRAINT `sp_company_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `sp_user` (`id`),
ADD CONSTRAINT `sp_company_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `sp_user` (`id`);

--
-- Constraints for table `sp_contact`
--
ALTER TABLE `sp_contact`
ADD CONSTRAINT `sp_contact_ibfk_1` FOREIGN KEY (`email`) REFERENCES `sp_user` (`id`),
ADD CONSTRAINT `sp_contact_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `sp_company` (`id`),
ADD CONSTRAINT `sp_contact_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `sp_user` (`id`),
ADD CONSTRAINT `sp_contact_ibfk_4` FOREIGN KEY (`modified_by`) REFERENCES `sp_user` (`id`);

--
-- Constraints for table `sp_contract`
--
ALTER TABLE `sp_contract`
ADD CONSTRAINT `sp_contract_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `sp_user` (`id`),
ADD CONSTRAINT `sp_contract_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `sp_company` (`id`),
ADD CONSTRAINT `sp_contract_ibfk_3` FOREIGN KEY (`approver_id`) REFERENCES `sp_user` (`id`),
ADD CONSTRAINT `sp_contract_ibfk_4` FOREIGN KEY (`payroller_id`) REFERENCES `sp_user` (`id`),
ADD CONSTRAINT `sp_contract_ibfk_5` FOREIGN KEY (`created_by`) REFERENCES `sp_user` (`id`),
ADD CONSTRAINT `sp_contract_ibfk_6` FOREIGN KEY (`modified_by`) REFERENCES `sp_user` (`id`);

--
-- Constraints for table `sp_logging`
--
ALTER TABLE `sp_logging`
ADD CONSTRAINT `sp_logging_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `sp_user` (`id`);

--
-- Constraints for table `sp_payrate`
--
ALTER TABLE `sp_payrate`
ADD CONSTRAINT `sp_payrate_ibfk_1` FOREIGN KEY (`timesheet_id`) REFERENCES `sp_timesheet` (`id`),
ADD CONSTRAINT `sp_payrate_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `sp_user` (`id`),
ADD CONSTRAINT `sp_payrate_ibfk_3` FOREIGN KEY (`modified_by`) REFERENCES `sp_user` (`id`);

--
-- Constraints for table `sp_timesheet`
--
ALTER TABLE `sp_timesheet`
ADD CONSTRAINT `sp_timesheet_ibfk_1` FOREIGN KEY (`contract_id`) REFERENCES `sp_contract` (`id`),
ADD CONSTRAINT `sp_timesheet_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `sp_user` (`id`),
ADD CONSTRAINT `sp_timesheet_ibfk_3` FOREIGN KEY (`modified_by`) REFERENCES `sp_user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
