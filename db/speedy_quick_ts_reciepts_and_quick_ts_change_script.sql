/*
SQLyog Ultimate v11.33 (32 bit)
MySQL - 5.6.17 : Database - speedy_payrolls
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`speedy_payrolls` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `speedy_payrolls`;

/*Table structure for table `sp_quick_ts_reciepts` */

DROP TABLE IF EXISTS `sp_quick_ts_reciepts`;

CREATE TABLE `sp_quick_ts_reciepts` (
  `id` bigint(20) NOT NULL,
  `reciept_file` varchar(255) NOT NULL,
  `ref_id` bigint(20) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_at` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_at` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `quick_timesheet_id` (`ref_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

/*sp_quick_timesheet changes*/
ALTER TABLE `speedy_payrolls`.`sp_quick_timesheet`   
  ADD COLUMN `rate` FLOAT NOT NULL AFTER `hours`;
