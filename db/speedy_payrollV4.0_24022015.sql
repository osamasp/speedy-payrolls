/*
SQLyog Ultimate v8.55 
MySQL - 5.5.20-log : Database - speedy_payroll
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `sp_payslip_entry` */

DROP TABLE IF EXISTS `sp_payslip_entry`;

CREATE TABLE `sp_payslip_entry` (
  `id` bigint(50) NOT NULL AUTO_INCREMENT,
  `_1T` varchar(255) DEFAULT NULL,
  `_1V` varchar(255) DEFAULT NULL,
  `_2T` varchar(255) DEFAULT NULL,
  `_2V` varchar(255) DEFAULT NULL,
  `_3T` varchar(255) DEFAULT NULL,
  `_3V` varchar(255) DEFAULT NULL,
  `_4T` varchar(255) DEFAULT NULL,
  `_4V` varchar(255) DEFAULT NULL,
  `_5T` varchar(255) DEFAULT NULL,
  `_5V` varchar(255) DEFAULT NULL,
  `_6T` varchar(255) DEFAULT NULL,
  `_6V` varchar(255) DEFAULT NULL,
  `_7T` varchar(255) DEFAULT NULL,
  `_7V` varchar(255) DEFAULT NULL,
  `_8T` varchar(255) DEFAULT NULL,
  `_8V` varchar(255) DEFAULT NULL,
  `_9T` varchar(255) DEFAULT NULL,
  `_9V` varchar(255) DEFAULT NULL,
  `_10T` varchar(255) DEFAULT NULL,
  `_10V` varchar(255) DEFAULT NULL,
  `_11T` varchar(255) DEFAULT NULL,
  `_11V` varchar(255) DEFAULT NULL,
  `_12T` varchar(255) DEFAULT NULL,
  `_12V` varchar(255) DEFAULT NULL,
  `_13T` varchar(255) DEFAULT NULL,
  `_13V` varchar(255) DEFAULT NULL,
  `_14T` varchar(255) DEFAULT NULL,
  `_14V` varchar(255) DEFAULT NULL,
  `_15T` varchar(255) DEFAULT NULL,
  `_15V` varchar(255) DEFAULT NULL,
  `_16T` varchar(255) DEFAULT NULL,
  `_16V` varchar(255) DEFAULT NULL,
  `_17T` varchar(255) DEFAULT NULL,
  `_17V` varchar(255) DEFAULT NULL,
  `_18T` varchar(255) DEFAULT NULL,
  `_18V` varchar(255) DEFAULT NULL,
  `_19T` varchar(255) DEFAULT NULL,
  `_19V` varchar(255) DEFAULT NULL,
  `_20T` varchar(255) DEFAULT NULL,
  `_20V` varchar(255) DEFAULT NULL,
  `_21T` varchar(255) DEFAULT NULL,
  `_21V` varchar(255) DEFAULT NULL,
  `_22T` varchar(255) DEFAULT NULL,
  `_22V` varchar(255) DEFAULT NULL,
  `_23T` varchar(255) DEFAULT NULL,
  `_23V` varchar(255) DEFAULT NULL,
  `_24T` varchar(255) DEFAULT NULL,
  `_24V` varchar(255) DEFAULT NULL,
  `_25T` varchar(255) DEFAULT NULL,
  `_25V` varchar(255) DEFAULT NULL,
  `_26T` varchar(255) DEFAULT NULL,
  `_26V` varchar(255) DEFAULT NULL,
  `_27T` varchar(255) DEFAULT NULL,
  `_27V` varchar(255) DEFAULT NULL,
  `_28T` varchar(255) DEFAULT NULL,
  `_28V` varchar(255) DEFAULT NULL,
  `_29T` varchar(255) DEFAULT NULL,
  `_29V` varchar(255) DEFAULT NULL,
  `_30T` varchar(255) DEFAULT NULL,
  `_30V` varchar(255) DEFAULT NULL,
  `user_id` bigint(50) DEFAULT NULL,
  `created_at` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `sp_payslip_entry` */

insert  into `sp_payslip_entry`(`id`,`_1T`,`_1V`,`_2T`,`_2V`,`_3T`,`_3V`,`_4T`,`_4V`,`_5T`,`_5V`,`_6T`,`_6V`,`_7T`,`_7V`,`_8T`,`_8V`,`_9T`,`_9V`,`_10T`,`_10V`,`_11T`,`_11V`,`_12T`,`_12V`,`_13T`,`_13V`,`_14T`,`_14V`,`_15T`,`_15V`,`_16T`,`_16V`,`_17T`,`_17V`,`_18T`,`_18V`,`_19T`,`_19V`,`_20T`,`_20V`,`_21T`,`_21V`,`_22T`,`_22V`,`_23T`,`_23V`,`_24T`,`_24V`,`_25T`,`_25V`,`_26T`,`_26V`,`_27T`,`_27V`,`_28T`,`_28V`,`_29T`,`_29V`,`_30T`,`_30V`,`user_id`,`created_at`) values (1,'Employer\r\n','Wafflemonster\r\n','Employee\r\n','Jack \r\n','NI Number\r\n','J12345\r\n','Date\r\n','10/10/2015\r\n','0\r\n','0\r\n','Payment Method\r\n','Cash\r\n','Period\r\n','WK26\r\n','Tax Code\r\n','TN250\r\n','Regular Hours\r\n','10\r\n','Night Hours\r\n','5\r\n','Overtime\r\n','5\r\n','0\r\n','0\r\n','0\r\n','0\r\n','0\r\n','0\r\n','Medical\r\n','25','Travel\r\n','15\r\n','0\r\n','0\r\n','0\r\n','0\r\n','Total Additions\r\n','40\r\n','Pension\r\n','20\r\n','Other Deduction\r\n','10\r\n','0\r\n','0\r\n','0\r\n','0\r\n','Total Deduction\r\n','30\r\n','0\r\n','0\r\n','0\r\n','0\r\n','Total Hours\r\n','20\r\n','NI\r\n','45\r\n','Gross Pay\r\n','500\r\n','Net Pay\r\n','350\r\n',NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
