/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 5.6.17 : Database - speedy_payroll
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`speedy_payroll` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `speedy_payroll`;

/*Table structure for table `sp_company_priveledge` */

DROP TABLE IF EXISTS `sp_company_priveledge`;

CREATE TABLE `sp_company_priveledge` (
  `id` bigint(20) NOT NULL,
  `priveledge_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `start_time` bigint(20) DEFAULT NULL,
  `end_time` bigint(20) DEFAULT NULL,
  `recursive` tinyint(1) DEFAULT NULL,
  `quota` int(11) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `priveledge_id` (`priveledge_id`),
  KEY `company_id` (`company_id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `sp_company_priveledge_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `sp_user` (`id`),
  CONSTRAINT `sp_company_priveledge_ibfk_4` FOREIGN KEY (`modified_by`) REFERENCES `sp_user` (`id`),
  CONSTRAINT `sp_company_priveledge_ibfk_1` FOREIGN KEY (`priveledge_id`) REFERENCES `sp_priveledge` (`id`),
  CONSTRAINT `sp_company_priveledge_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `sp_company` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sp_company_priveledge` */

/*Table structure for table `sp_plan` */

DROP TABLE IF EXISTS `sp_plan`;

CREATE TABLE `sp_plan` (
  `id` bigint(20) NOT NULL,
  `plan_type` varchar(255) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `sp_plan_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `sp_user` (`id`),
  CONSTRAINT `sp_plan_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `sp_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sp_plan` */

/*Table structure for table `sp_plan_priveledges` */

DROP TABLE IF EXISTS `sp_plan_priveledges`;

CREATE TABLE `sp_plan_priveledges` (
  `id` bigint(20) NOT NULL,
  `plan_id` bigint(20) DEFAULT NULL,
  `priveledge_id` bigint(20) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `plan_id` (`plan_id`),
  KEY `priveledge_id` (`priveledge_id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `sp_plan_priveledges_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `sp_user` (`id`),
  CONSTRAINT `sp_plan_priveledges_ibfk_4` FOREIGN KEY (`modified_by`) REFERENCES `sp_user` (`id`),
  CONSTRAINT `sp_plan_priveledges_ibfk_1` FOREIGN KEY (`plan_id`) REFERENCES `sp_plan` (`id`),
  CONSTRAINT `sp_plan_priveledges_ibfk_2` FOREIGN KEY (`priveledge_id`) REFERENCES `sp_priveledge` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sp_plan_priveledges` */

/*Table structure for table `sp_priveledge` */

DROP TABLE IF EXISTS `sp_priveledge`;

CREATE TABLE `sp_priveledge` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  `function` varchar(255) DEFAULT NULL,
  `controller` varchar(255) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `group` varchar(255) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `sp_priveledge_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `sp_user` (`id`),
  CONSTRAINT `sp_priveledge_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `sp_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sp_priveledge` */

/*Table structure for table `sp_user_priveldge` */

DROP TABLE IF EXISTS `sp_user_priveldge`;

CREATE TABLE `sp_user_priveldge` (
  `id` bigint(20) NOT NULL,
  `priveledge_value` int(11) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `priveledge_id` bigint(20) DEFAULT NULL,
  `start_time` bigint(20) DEFAULT NULL,
  `end_time` bigint(20) DEFAULT NULL,
  `recursive` tinyint(1) DEFAULT NULL,
  `quota` int(11) DEFAULT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `modified_at` bigint(20) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `priveledge_id` (`priveledge_id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `sp_user_priveldge_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `sp_user` (`id`),
  CONSTRAINT `sp_user_priveldge_ibfk_4` FOREIGN KEY (`modified_by`) REFERENCES `sp_user` (`id`),
  CONSTRAINT `sp_user_priveldge_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `sp_user` (`id`),
  CONSTRAINT `sp_user_priveldge_ibfk_2` FOREIGN KEY (`priveledge_id`) REFERENCES `sp_priveledge` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sp_user_priveldge` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
