ALTER TABLE `speedy_payroll`.`sp_user`   
  ADD COLUMN `staff_id` BIGINT(20) NOT NULL AFTER `plan_type`;
  
  ALTER TABLE `speedy_payrolls`.`sp_quick_timesheet`   
  ADD COLUMN `job_title` VARCHAR(255) NOT NULL AFTER `total`,
  ADD COLUMN `job_reference` VARCHAR(255) NOT NULL AFTER `job_title';
  
  //6/8/2015 change
  ALTER TABLE `speedy_payrolls`.`sp_quick_timesheet`   
  ADD COLUMN `job_description` VARCHAR(255) NOT NULL AFTER `job_reference`;