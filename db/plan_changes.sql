ALTER TABLE `sp_plan_priveledges`   
  ADD COLUMN `quota` BIGINT(20) NULL AFTER `priveledge_id`,
  ADD COLUMN `recursive_type` ENUM('None','Daily','Weekly','Monthly') NULL AFTER `quota`;

ALTER TABLE `sp_company_priveledge`   
  ADD COLUMN `status` ENUM('active','in-active') NULL AFTER `quota`;
CHANGE `status` `status` ENUM('active','in-active') CHARSET latin1 COLLATE latin1_swedish_ci DEFAULT 'active'   NULL;