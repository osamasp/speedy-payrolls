ALTER TABLE `sp_user`   
  ADD COLUMN `token` VARCHAR(255) NULL AFTER `notif_seen_at`,
  ADD COLUMN `validity` BIGINT(20) NULL AFTER `token`;