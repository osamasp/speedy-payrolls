ALTER TABLE `sp_company_shifts`   
  ADD COLUMN `created_at` BIGINT(20) NULL AFTER `company_id`,
  ADD COLUMN `created_by` BIGINT(20) NULL AFTER `created_at`,
  ADD COLUMN `modified_at` BIGINT(20) NULL AFTER `created_by`,
  ADD COLUMN `modified_by` BIGINT(20) NULL AFTER `modified_at`;

insert into `sp_priveledge` (`id`, `name`, `key`, `function`, `group`, `status`, `function_list_id`, `created_at`, `modified_at`, `created_by`, `modified_by`) values('39517452','Invoice One Way Timesheet','Invoice_One_Way_Timesheet','','One_Way','1','36509157','1449658737','1449658737','1','1');