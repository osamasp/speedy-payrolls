insert  into `sp_function_list`(`id`,`title`,`module`,`controller`,`action`) values 
(22974368,'Assign Shift','user','EmployeeController.php','Assignshift'),
(29566461,'Change Payrate','user','EmployeeController.php','Changepayrate'),
(58791846,'Assign Supervisor','user','EmployeeController.php','Assignsupervisor'),
(27607753,'Approver Export','timesheet','ApproveController.php','Export');

/*Data for the table `sp_priveledge` */

insert  into `sp_priveledge`(`id`,`name`,`key`,`function`,`group`,`status`,`function_list_id`,`created_at`,`modified_at`,`created_by`,`modified_by`) values 
(6291198,'Assign Supervisor','Assign_Supervisor','','Staff_Management',1,58791846,1468943412,1468943412,1,1),
(53352057,'Assign Shift','Assign_Shift','','Staff_Management',1,22974368,1468419392,1468419392,1,1),
(53915495,'Change Payrate','Change_Payrate','','Staff_Management',1,29566461,1468943519,1468943519,1,1),
(16650662,'Approver Export','Approver_Export','','Staff_Management',1,27607753,1469794128,1469794128,1,1);



insert  into `sp_function_list`(`id`,`title`,`module`,`controller`,`action`) values 
(14773434,'Shift Export','company','CompanyShiftsController.php','ShiftExport'),
(13979290,'Export','company','CompanyShiftsController.php','Export');


insert  into `sp_priveledge`(`id`,`name`,`key`,`function`,`group`,`status`,`function_list_id`,`created_at`,`modified_at`,`created_by`,`modified_by`) values 
(12451809,'Shift Export','Shift_Export','','Company Shifts',1,14773434,1453545655,1453545655,1,1),
(42606990,'Shift_Export','Shift_Export','','Company Shifts',1,13979290,1453545855,1453545855,1,1);
