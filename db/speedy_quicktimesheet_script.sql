/*
SQLyog Ultimate v11.33 (32 bit)
MySQL - 5.5.42-37.1-log : Database - vistecho_speedy_alpha
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`vistecho_speedy_alpha` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `vistecho_speedy_alpha`;

/*Table structure for table `sp_quick_timesheet` */

DROP TABLE IF EXISTS `sp_quick_timesheet`;

CREATE TABLE `sp_quick_timesheet` (
  `id` bigint(20) NOT NULL,
  `hours` bigint(20) NOT NULL,
  `date` varchar(255) NOT NULL,
  `status` enum('Open','Emailed') NOT NULL DEFAULT 'Open',
  `total` float NOT NULL,
  `contact_id` bigint(20) NOT NULL,
  `currency_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` bigint(20) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `modified_at` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `sp_quick_ts_expenses` */

DROP TABLE IF EXISTS `sp_quick_ts_expenses`;

CREATE TABLE `sp_quick_ts_expenses` (
  `id` bigint(20) NOT NULL,
  `expense_name` varchar(255) NOT NULL,
  `expense_value` float NOT NULL,
  `comment` varchar(512) DEFAULT NULL,
  `quick_time_id` bigint(20) NOT NULL,
  `created_at` bigint(20) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `modified_at` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `quick_time_id` (`quick_time_id`),
  CONSTRAINT `sp_quick_ts_expenses_ibfk_1` FOREIGN KEY (`quick_time_id`) REFERENCES `sp_quick_timesheet` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
