ALTER TABLE `sp_company`   
  ADD COLUMN `billing_date` INT DEFAULT 1  NOT NULL AFTER `legal_name`,
  ADD COLUMN `last_bill_time` BIGINT DEFAULT 0  NOT NULL AFTER `billing_date`;

