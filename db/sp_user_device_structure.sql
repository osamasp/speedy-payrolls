DROP TABLE IF EXISTS `sp_user_devices`;

CREATE TABLE `sp_user_devices` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `device_id` bigint(20) DEFAULT NULL,
  `device_type` enum('ios','android') DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `device_id` (`device_id`,`device_type`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `sp_user_devices_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `sp_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;