ALTER TABLE `sp_notification_type`   
  ADD COLUMN `notice` VARCHAR(255) NULL AFTER `logo`;
ALTER TABLE `sp_notification`   
  DROP COLUMN `notice`;
ALTER TABLE `sp_user`   
  ADD COLUMN `notif_seen_at` BIGINT(20) DEFAULT 0  NULL AFTER `shift_id`;