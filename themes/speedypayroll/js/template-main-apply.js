    $(document).ready(function () {
        $('#templates').on('change', function () {
            var ajaxLoader = $("#ajax_loader").val();
            $.ajax({
                url: $("#template").val(),
                data: {id: $(this).val()},
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    if (response.status == true) {
                        $("#template-data").html(response.data);
                    } else {
                        $("#template-data").html("No Contrator Found.");
                    }
                },
                beforeSend: function () {
                    $("#template-data").html(ajaxLoader);
                }
            });
        });
    });