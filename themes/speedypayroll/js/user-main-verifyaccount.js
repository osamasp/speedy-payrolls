jQuery(document).ready(function() {     
  ComingSoon.init();
  // init background slide images
    $.backstretch([
            $("#base").val()+"/css/pages/media/bg/1.jpg",
            $("#base").val()+"/css/pages/media/bg/2.jpg",
            $("#base").val()+"/css/pages/media/bg/3.jpg",
            $("#base").val()+"/css/admin/pages/media/bg/4.jpg"
        ], {
        fade: 1000,
        duration: 10000
   });
});