    $(document).ready(function() {
        check();
        $('#quick-ts-form').h5Validate({errorClass:'errorClass'});
        
        $('.Model').on('show.bs.modal', function (event) {
                            var button = $(event.relatedTarget); // Button that triggered the modal
                            var url = button.data('whatever'); // Extract info from data-* attributes
                            var modal = $(this);        
                              $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: $("#quick-ts-form").serialize(),
                                    beforeSend:function(jqXHR,settings){
                                            jQuery(modal).find('.modal-content').text("Loading..........");
                                            
                                    },
                                    success: function(response){
                                        if(response.status==true){
                                            jQuery(modal).find('.modal-content').html(response.data);
                                        }else{
                                            jQuery(modal).modal('hide');
                                             alert(response.reason);
                                    
                                        }
                                    },
                                    dataType: 'json'
                                  });   
                                    });
    });
    
    function validate()
    {
        if ($("#name").val() == "" || $("#email").val() == "" || $("#cl_add").val() == "" || $("#cl_city").val() == "" || $("#cl_pc").val() == "" || $("#cl_country").val() == "")
        {
            alert("Please fill in all required fields...");
            return false;
        }
        $("#sendinvoice").click();
        return true;
    }
    
    function check() {
        if (document.getElementById("contacts").value != 'out') {
            document.getElementById("email").disabled = true;
            document.getElementById("name").disabled = true;
            document.getElementById("cl_add").disabled = true;
            document.getElementById("cl_country").disabled = true;
            document.getElementById("cl_pc").disabled = true;
            document.getElementById("cl_city").disabled = true;
        }
        else {
            document.getElementById("email").disabled = false;
            document.getElementById("name").disabled = false;
            document.getElementById("cl_add").disabled = false;
            document.getElementById("cl_country").disabled = false;
            document.getElementById("cl_pc").disabled = false;
            document.getElementById("cl_city").disabled = false;
        }
        var id = $("#contacts").val();
        var url = $("#contact_details").val();
        $.post(url,{contact_id:id},
        function(result){
            var data = JSON.parse(result);
        if(data!==null)
        {
            document.getElementById("email").value = data.email;
            document.getElementById("name").value = data.name;
            document.getElementById("cl_add").value = data.address;
            document.getElementById("cl_country").value = data.country;
            document.getElementById("cl_pc").value = data.post_code;
            document.getElementById("cl_city").value = data.city;
        }
        else
        {
            document.getElementById("email").value = "";
            document.getElementById("name").value = "";
            document.getElementById("cl_add").value = "";
            document.getElementById("cl_country").value = "";
            document.getElementById("cl_pc").value = "";
            document.getElementById("cl_city").value = "";
        }
    });
    }