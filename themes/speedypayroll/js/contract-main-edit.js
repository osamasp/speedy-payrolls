    $(document).ready(function () {
        $("#start_date").datepicker({
            format: $("#date_format").val()
        });
        $("#end_date").datepicker({
            format: $("#date_format").val()
        });

        $(function () {
            $('#collection_type').on('change', function () {
                if ($(this).val() == 2) {

                    $("#collection_day").hide(300);
                    $("#collection_period").hide(300);
                }
                else if ($(this).val() == 0) {
                    $("#collection_day").show(300);
                    $("#collection_period").hide(300);
                }
                else {
                    $("#collection_day").show(300);
                    $("#collection_period").show(300);
                }
            });
        });
    });