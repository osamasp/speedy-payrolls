function setActions(action)
    {
       if($("#currTab").val() === "2"){
           if(action=='continue' && ($("#Company_name").val()!="" && $("#User_1_first_name").val()!=""&&$("#User_1_last_name").val()!=""&&$("#User_1_phone").val()!=""&&$("#User_1_staff_id").val()!=""))
           {
                $(".button-previous").show();
                $("#currTab").val("3");
                $("#tab3").show();
                $("#tab2").hide();
            }
            else
            {
                $(".button-previous").hide();
                $("#currTab").val("2");
                $("#tab2").show();
                $("#tab3").hide();
            }
       }
       else
       {
           if(action==='back')
           {
               $(".button-previous").hide();
               $("#currTab").val("2");
               $("#tab2").show();
               $("#tab3").hide();
               return false;
           }
           else{
               $('#submit_form').submit();
                return true;
            }
        }
    }