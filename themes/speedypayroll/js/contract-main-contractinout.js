function enable()
        {
            if($("#chBox").is(":checked"))
            {
                $("#submit").attr("disabled",false);
            }
            else
            {
                $("#submit").attr("disabled",true);
            }
        }
    $('#addmore').on('click', function() {
        var html = '<div class="col-sm-4">Name<input type="text" name="Payrate_key[]" class="form-control" placeholder="Pay rate name"></div><div class="col-sm-4">Cost<input type="text" name="Payrate_value1[]" class="form-control" placeholder="�/hr"></div><div class="col-sm-4">Bill<input type="text" name="Payrate_value2[]" class="form-control" placeholder="�/hr"></div>'
        $('#row-payrate').append(html);
    });
    var t;
    var t1;

    $(document).ready(function() {
        $('#cc-search1').click(function() {
            if ($('.find-ccompany').val() != '') {
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if (re.test($('.find-ccompany').val())) {
                    start_ajax2();
                }
            }

        });

        $('#cc-search2').click(function() {
            if ($('.find-ccompany2').val() != '') {
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if (re.test($('.find-ccompany2').val())) {
                    start_ajax22();
                }
            }

        });

        $('#approver-select').click(function() {
            if ($('#approver_email').val() != '') {
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if (re.test($('#approver_email').val())) {
                    checkapprover();
                }
            }

        });
        
        $('body').on('focusout','#company_name',function(){
            checkcompany();
        });

    });
    
    function checkcompany(){
        $.ajax({
            url: $("#company_exist").val(),
            data: {name: $("#company_name").val()},
            type: 'GET',
            success: function(response){
                if(response == 1){
                    $("#company_name").val('');
                    alert("Company with the same name already exist. Please enter any other name.");
                    $("#company_name").focus();
                }
            }
        });
    }    

    var t2;

    $(document).ready(function() {
        
        $("#coll_freq").focus();
        $("#tab2").show();
        $('body').on('keyup', '#contractor_email', function() {
            if ($('#contractor_email').val() != '') {
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if (re.test($('#contractor_email').val())) {
                    clearTimeout(t2);
                    t2 = setTimeout('start_ajax3()', 1500);
                }
            }
        });

    });

    function start_ajax3() {
        $.ajax({
            url: $('#user_exist').val(),
            data: {email: $('#contractor_email').val()},
            type: 'GET',
            success: function(response) {
                if (response == 1) {
                    $('#cc-email-message').html("<p style='color:red;'>User with this email address already exist.</p>");
                } else {
                    $('#cc-email-message').html("<p style='color:green;'>Available</p>");
                }
            }
        });
    }

    function start_ajax2() {
        var ajaxLoader = $('#ajax_loader').val();
        $.ajax({
            url: $('#company_details').val(),
            data: {email: $('.find-ccompany').val()},
            type: 'GET',
            dataType: 'json',
            success: function(response) {
                if (response.status == true) {
                    $("#cdetails").html(response.data);
                    if (response.company != null && response.company != 0) {
                        $("#submitBtn").removeAttr('disabled');
                        $("#companyId2").val(response.company);
                    } else {
                        $("#add-new-company").hide();
                        $("#submitBtn").attr('disabled', 'disabled');
                        $("#companyId").val('0');
                    }
                } else {
                    $("#cdetails").html("No Contrator Found.");
                }
            },
            beforeSend: function() {
                $("#cdetails").html(ajaxLoader);
            }
        });
    }

    function start_ajax22() {
        var ajaxLoader = $('#ajax_loader').val();
        $.ajax({
            url: $('#company_details').val(),
            data: {email: $('.find-ccompany2').val(), inputArrName: "cc2"},
            type: 'GET',
            dataType: 'json',
            success: function(response) {
                if (response.status == true) {
                    $("#cdetails2").html(response.data);
                    if (response.company != null && response.company != 0) {
                        $("#companyId").val(response.company);
                    } else {
                        $("#companyId").val('0');
                    }
                } else {
                    $("#cdetails2").html("No Company Found.");
                }
            },
            beforeSend: function() {
                $("#cdetails2").html(ajaxLoader);
            }
        });
    }
    
    function setday(day)
    {
        $("#day").val(day);
    }

    function fillEmployee(company_id) {
        var ajaxLoader = $('#ajax_loader').val();
        $.ajax({
            url: $('#get_employee').val(),
            data: {company_id: company_id, isinout: 1},
            type: 'POST',
            dataType: 'json',
            success: function(response) {

                if (response.status == true) {
                    jQuery(".contractor-contrainer").html(response.data);
                    jQuery('#payratesanditems').html(response.payrates);
                } else {
                    jQuery(".contractor-contrainer").html("No Contrator Found.");
                }
            },
            beforeSend: function() {
                jQuery(".contractor-contrainer").html(ajaxLoader);
            }
        });
    }

    var confirm1 = false;
    var confirm2 = false;

    
    
    function checkPayrates(){
                var isSetPayrates = true;
                $("input[name^='Payrate'],input[name^='Item']").each( function(index)
                {
                    if($(this).val()==="" || $(this).val()==="0.00" || $(this).val()===null)
                    {
                        isSetPayrates = false; 
                    }
                });
                
                    if(!isSetPayrates){
                        $("#successDiv").hide();
                        $("#errorDiv > span").text("Please set Payrates.");
                        $("#errorDiv").show();
                    }
                    else
                    {
                        $("#errorDiv").hide();
                        $("#successDiv > span").text("Staff payrates set successfully.");
                        $("#successDiv").show();
                    }
        }
    
    function fillApprover()
    {
     if($("#contractor_email").val() != '' && $("#contractor_email").val() != null){
        $("#approver_email").val($("#contractor_email").val());
        $("#approver_email").attr('disabled',true);
        $("#approver_id").val('');
    }
    else{
        $("#approver_email").val('');
        $("#approver_id").val('0');
        $("#approver_email").attr('disabled',false);
    }   
    }

    jQuery(document).ready(function() {
        $('body').on('click', '#confirm-company', function() {
        fillEmployee($('#cc_id').val());
        $('#confirm-company').remove();
	$("#confirmed").show();
        confirm1 = true;
    });

    $('body').on('click', '#add-new-company', function() {
        $('.cc-remove-me').remove();
        $('.find-ccompany').val('');
        $('.find-ccompany').attr('disabled', '');
        $('#outside-your-company').attr('disabled', '');
        $('#cc-show-me').show(800);
        $('#cc-show-me input').attr('required', '');

    });
    
    $('body').on('click', '#add-new-companycc2', function() {
        $('.cc-remove-mecc2').remove();
        $('.find-ccompany2').val('');
        $('.find-ccompany2').attr('disabled', '');
        $('#cc-show-mecc2').show(800);
        $('#cc-show-mecc2 input').attr('required', '');

    });

    $('body').on('click', '#confirm-companycc2', function() {
        //fillEmployee($('#cc_id2').val());
        $('#confirm-companycc2').remove();
        confirm2 = true;
        fillApprover();
	$("#confirmed").show();
    });
        var ajaxLoader = $('#ajax_loader').val();
        $(".employee-get").on('change', function() {
            $.ajax({
                url: $('#get_employee').val(),
                data: {company_id: jQuery(this).val()},
                type: 'POST',
                dataType: 'json',
                success: function(response) {

                    if (response.status == true) {
                        jQuery(".contractor-contrainer").html(response.data);
                        jQuery(".chosen-select").chosen();
                    } else {
                        jQuery(".contractor-contrainer").html("No Contrator Found.");
                    }
                },
                before: function() {
                    jQuery(".contractor-contrainer").html(ajaxLoader);
                }
            });
        });


    });


    var approvertimer;

    function checkapprover() {
        $.ajax({
            url: $('#exist_in_company').val(),
            data: {email: $('#approver_email').val(),
                company1: $('#companyId').val(),
                company2: $('#companyId2').val()},
            type: 'GET',
            success: function(response) {
               if (response == 0) {
                    $('#approver-email-message').html("<p style='color:red;'>No user found with this email address.</p>");
                } else {
                    console.log(response);
                    var obj = JSON.parse(response);
                    $('#approver-email-message').html(obj.data);
                }
            }
        });
    }

    $(document).ready(function() {
        $('#submitBtn').on('click', function(e) {
            var error = '';
            if (!confirm1) {
                error += 'Please select Contractor Company\n';
            }
            if (!confirm2) {
                error += 'Please select Company they will be working for\n';
            }
            if (error != '') {
                e.preventDefault();
                alert(error);
            } else {
                $('#submitBtnhidden').click();
            }
        });
    });

    function show()
    {
        if($("#tab2").is(':visible') )
        {
            if($("#Contract_admin_email").val()==""){
                alert('Please fill in all required fields....');
                return;
            }
            else if($("#companyId2").val()=="0")
            {
                if($("#company_name").val()==""||$("#contractor_email").val()==""||$("#company_phone").val()==""||$("#company_address").val()==""||$("#city").val()==""||$("#post_code").val()==""||$("#country").val()=="")
                {
                    alert('Please fill in all required fields.....');
                    return;
                }
            }
            var isSetPayrates = true;
            $("input[name^='Payrate'],input[name^='Item']").each( function(index)
            {
                if($(this).val()==="" || $(this).val()==="0.00" || $(this).val()===null)
                {
                    isSetPayrates = false; 
                }
            });
                
            if(!isSetPayrates){
                alert("Please apply payrates......");
                return;
            }
        }
        else if($("#tab3").is(':visible'))
        {
            if($("#Contract2").val()=="")
            {
                alert('Please fill in all required fields....');
                return;
            }
            else if($("#companyId").val()=="0")
            {
                if($("#company_name").val()==""||$("#contractor_email").val()==""||$("#company_phone").val()==""||$("#company_address").val()==""||$("#city").val()==""||$("#post_code").val()==""||$("#country").val()=="")
                {
                    alert('Please fill in all required fields.....');
                    return;
                }
            }
            
            if($("#timesheet_approver").is(':checked'))
            {
                if($("#approver").val()=='0')
                {
                    alert('Please fill in all required fields....');
                    return;
                }
                
            }
            else if($("#timesheet_approver1").is(':checked'))
            {
                if($("#approver_id").val()=='0')
                {
                    alert('Please fill in all required fields....');
                    return;
                }
                
            }
            else
            {
                alert('Please fill in all required fields....');
                    return;
            }
        }
        else if($("#tab3").is(':visible') && ($("#companyId").val()=="0"))
        {
            if($("#company_name").val()==""||$("#contractor_email").val()==""||$("#company_phone").val()==""||$("#company_address").val()==""||$("#city").val()==""||$("#post_code").val()==""||$("#country").val()=="")
            {
                alert('Please fill in all required fields.....');
                return;
            }
        }
        if ($("#tab3").is(':hidden') && $("#tab2").is(':visible')) {
            $("#menuTab2").removeClass("active");
            $("#menuTab2").addClass("done");
            $("#menuTab3").addClass("active");
            $("#progress").css('width', '75%');
            $("#tab2").hide();
            $("#tab3").show();
        }
        else if ($("#tab2").is(':hidden') && $("#tab3").is(':visible')) {
            $("#menuTab3").removeClass("active");
            $("#menuTab3").addClass("done");
            $("#menuTab4").addClass("active");
            $("#progress").css('width', '100%');
            $("#tab3").hide();
            $("#tab4").show();
            $("#continue").hide();
            $("#submit").show();
        }
    }
    $(function() {
        $('#collection_type').on('change', function () {
        if ($(this).val() == 0) {
            $("#collection_day").hide();
            $("#collection_period").hide();
        }
        else {
            $("#collection_day").show();
            $("#collection_period").show();
        }
    });
    });
    
    function run() {
        if (document.getElementById("timesheet_approver1").checked) {
            fillApprover();
            $("#outside").show();
            $("#inside").hide();
        }
        else {
            $("#outside").hide();
            $("#inside").show();
        }
    }
    
    function submitForm(){
        $("#overlay").show();
        $("form#user-form").submit();
    }

    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }