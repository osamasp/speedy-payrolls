    var approvertimer;
    function setday(day)
    {
        $("#day").val(day);
    }
    function enable()
        {
            if($("#chBox").is(":checked"))
            {
                $("#submit").attr("disabled",false);
            }
            else
            {
                $("#submit").attr("disabled",true);
            }
        }
    $(document).ready(function() {
$("#tab2").show();

        var t;

        $(document).ready(function() {
            $("#coll_freq").focus();
            $('#cc-search1').click(function() {
                if ($('.find-ccompany').val() != '') {
                    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    if (re.test($('.find-ccompany').val())) {
                        start_ajax2();
                    }
                }

            });

        });


        $('body').on('focusout','#company_name',function(){
            checkcompany();
        });

        $('body').on('keyup', '#approver_email', function() {
            if ($('#approver_email').val() != '') {
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if (re.test($('#approver_email').val())) {
                    clearTimeout(approvertimer);
                    approvertimer = setTimeout('checkapprover()', 1500);
                }
            }
        });

    });
    
    function show()
    {
        if($("#tab2").is(':visible'))
        {
            if($("#employee").val()==""){
                alert('Please fill in all required fields....');
                return;
            }
            var isSetPayrates = true;
            $("input[name^='Payrate'],input[name^='Item']").each( function(index)
            {
                if($(this).val()==="" || $(this).val()==="0.00" || $(this).val()===null)
                {
                    isSetPayrates = false; 
                }
            });
                
            if(!isSetPayrates){
                alert("Please apply payrates......");
                return;
            }
        }
        else if($("#tab3").is(':visible') && ($("#company_email").val()=="" && $("#approver").val()=="0" && $("#approver_valid").val()=="0"))
        {
            alert('Please fill in all required fields.....');
            return;
        }
        else if($("#tab3").is(':visible') && ($("#companyId").val()=="" || $("#approver").val()=="0" && $("#approver_valid").val()=="0"))
        {
            alert('Please fill in all required fields.....');
            return;
        }
        else if($("#tab3").is(":visible") && ($("#companyId").val()=="0" || $("#approver").val()=="0" && $("#approver_valid").val()=="0"))
        {
            if($("#company_name").val()==""||$("#contractor_email").val()==""||$("#company_phone").val()==""||$("#company_address").val()==""||$("#city").val()==""||$("#post_code").val()==""||$("#country").val()=="")
            {
                alert('Please fill in all required fields.....');
                return;
            }
        }
        if($("#tab3").is(':hidden') && $("#tab2").is(':visible')){
        $("#menuTab2").removeClass("active");
        $("#menuTab2").addClass("done");
        $("#menuTab3").addClass("active");
        $("#progress").css('width','75%');
        $("#tab2").hide();
        $("#tab3").show();
        }
        else if($("#tab2").is(':hidden') && $("#tab3").is(':visible')){
        $("#menuTab3").removeClass("active");
        $("#menuTab3").addClass("done");
        $("#menuTab4").addClass("active");
        $("#progress").css('width','100%');
        $("#tab3").hide();
        $("#tab4").show();
        $("#continue").hide();
        $("#submit").show();
    }
    }
    
    
    function selectInsideCompany()
    {
        jQuery("#timesheet_approver").prop('checked','checked')
        run()
        
    }
    
    function checkcompany(){
        $.ajax({
            url: $("#company_exist").val(),
            data: {name: $("#company_name").val()},
            type: 'GET',
            success: function(response){
                if(response == 1){
                    $("#company_name").val('');
                    alert("Company with the same name already exist. Please enter any other name.");
                    $("#company_name").focus();
                }
            }
        });
    }
    
    function checkapprover() {
        $.ajax({
            url: $("#user_exist").val(),
            data: {email: $('#approver_email').val(),
                    companyId: $('#companyId').val()},
            type: 'GET',
            success: function(response) {
                $('#approver_valid').val(response);        
                if (response == 0) {
                    $('#approver-email-message').html("<p style='color:red;width:400px'>No user found with this email address</p>");
                } else {
                    $('#approver-email-message').html("<p style='color:green;'><i class='fa fa-fw fa-check'></i> User found.</p>");
                }
            }
        });
    }


    function start_ajax2() {
        var ajaxLoader = $("#ajax_loader").val();
        $.ajax({
            url: $("#company_details").val(),
            data: {email: $('.find-ccompany').val()},
            type: 'GET',
            dataType: 'json',
            success: function(response) {
                if (response.status == true) {
                    $("#cdetails").html(response.data);

                    if (response.company != null && response.company != 0) {
                        $("#companyId").val(response.company);
                    } else {
                        $("#companyId").val('0');
                    }


                } else {
                    $("#cdetails").html("No Company Found.");

                }
            },
            beforeSend: function() {
                $("#cdetails").html(ajaxLoader);
            }
        });
    }

    var confirm2 = false;
    
    function fillApprover()
    {
        if($("#contractor_email").val() != '' && $("#contractor_email").val() != null){
        $("#approver_name").val($("#contractor_name").val());
        $("#approver_email").val($("#contractor_email").val());
        $("#approver_phone").val($("#contractor_phone").val());
        $("#approver_name").attr('disabled',true);
        $("#approver_email").attr('disabled',true);
        $("#approver_phone").attr('disabled',true);
        $('#approver_valid').val("1");
    }
    else{
        $("#approver_name").val('');
        $("#approver_email").val('');
        $("#approver_phone").val('');
        $("#approver_name").attr('disabled',false);
        $("#approver_email").attr('disabled',false);
        $("#approver_phone").attr('disabled',false);
    }
    }
    
    function checkPayrates(){
                var isSetPayrates = true;
                $("input[name^='Payrate'],input[name^='Item']").each( function(index)
                {
                    if($(this).val()==="" || $(this).val()==="0.00" || $(this).val()===null)
                    {
                        isSetPayrates = false; 
                    }
                });
                
                    if(!isSetPayrates){
                        $("#successDiv").hide();
                        $("#errorDiv > span").text("Please set Payrates.");
                        $("#errorDiv").show();
                    }
                    else
                    {
                        $("#errorDiv").hide();
                        $("#successDiv > span").text("Payrates have been applied successfully.");
                        $("#successDiv").show();
                    }
        }

    $(function() {
        $('#collection_type').on('change', function () {
        if ($(this).val() == 0) {
            $("#collection_day").hide();
            $("#collection_period").hide();
        }
        else {
            $("#collection_day").show();
            $("#collection_period").show();
        }
    });
    });

    $(document).ready(function() {
        $('body').on('click', '#add-new-company', function() {
        $('.cc-remove-me').remove();
        $('.find-ccompany').val('');
        $('.find-ccompany').attr('disabled', '');
        $('#cc-show-me').show(800);
        $('#cc-show-me input').attr('required', '');

    });
    
            $('body').on('click', '#confirm-company', function() {
        $('#confirm-company').remove();
        $("#confirmed").show();
        confirm2 = true;
        fillApprover();
    });
    
        $('#submitBtn').on('click', function(e) {
            var error = '';
            if (!confirm2) {
                error += 'Please select Company they will be working for\n';
            }
            if (error != '') {
                e.preventDefault();
                alert(error);
            } else {
                $('#submitBtnhidden').click();
            }
        });
    });
    
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
    
    function submitForm(){
        $("#overlay").show();
        $("form#submit-form").submit();
    }
    
    function run() {
        if (document.getElementById("timesheet_approver1").checked) {
            fillApprover();
            $("#outside").show();
            $("#inside").hide();
        }
        else {
            $("#outside").hide();
            $("#inside").show();
        }
    }