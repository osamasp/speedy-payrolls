jQuery(document).ready(function() {
    /**** Get Timesheets JS ****/

    $('#selectAll').click(function(event) {  //on click 
        if ($("#All").is(":checked")) { // check select status
            $('.select').each(function() { //loop through each checkbox
                this.checked = false;  //select all checkboxes with class "checkbox1"               
            });
        } else {

            $('.select').each(function() { //loop through each checkbox
                this.checked = true; //deselect all checkboxes with class "checkbox1"                       
            });
        }
    });
    $('#openButton').on('click', function(event) {
        $(this).parent().toggleClass('open');
    });

    $('#myDropdown .dropdown-menu').on({
        "click": function(e) {
            e.stopPropagation();
        }
    });

    $('.Model').on('show.bs.modal', function(event) {
//                                           alert("2");         
        var button = $(event.relatedTarget); // Button that triggered the modal
        var url = '';
        var type = '';
        var data = {'id': 0};
        if (button[0].attributes.id.value == "invoice") {
            url = button.data('whatever');
            type = "GET";
            data = {'id': url};
        }
        var modal = $(this);
        $.ajax({
            type: type,
            url: url,
            data: data,
            beforeSend: function(jqXHR, settings) {
                jQuery(modal).find('.modal-content').text("Loading..........");

            },
            success: function(response) {
                if (response.status == true) {
                    jQuery(modal).find('.modal-content').html(response.data);
                } else {
                    jQuery(modal).modal('hide');
                    alert(response.reason);

                }
            },
            dataType: 'json'
        });
    });
    $('.ajaxModel').on('show.bs.modal', function(event) {


        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('whatever') // Extract info from data-* attributes
        var modal = $(this)
        $.ajax({
            type: "POST",
            url: $("#sheet_url").val(),
            data: {'entryId': recipient},
            beforeSend: function(jqXHR, settings) {
                jQuery(modal).find('.modal-content').text("Loading..........");

            },
            success: function(response) {
                if (response.status == true) {
                    jQuery(modal).find('.modal-content').html(response.data);
                } else {
                    jQuery(modal).modal('hide');
                    alert(response.reason);

                }
            },
            dataType: 'json'
        });


//                           
    })

});

jQuery(function($) {

    var _oldShow = $.fn.show;

    $.fn.show = function(speed, oldCallback) {
        return $(this).each(function() {
            var obj = $(this),
                    newCallback = function() {
                        if ($.isFunction(oldCallback)) {
                            oldCallback.apply(obj);
                        }
                        obj.trigger('afterShow');
                    };

            // you can trigger a before show if you want
            obj.trigger('beforeShow');

            // now use the old function to show the element passing the new callback
            _oldShow.apply(obj, [speed, newCallback]);
        });
    }
});
jQuery(function($) {
    /*  $('.wklyLnkPrnt')
     .bind('beforeShow', function() {
     }) 
     .bind('afterShow', function() {
     $('.wklyLnkPrnt').on('click',);
     });*/
});

function exportData(id) {
    var url = '';
        var type = '';
        var data = {'id': 0};
        if (id == "export") {
            url = $("#export_url").val();
            type = "POST";
            data = $("#myForm").serialize();
            if (!$(".check").is(":checked"))
            {
                alert($('#select_msg').val());
                return;
            }
        } else if (id == "export_all") {
            url = $("#export_url").val();
            type = "POST";
            data = $("#myForm").serialize();
        }
    
        $.ajax({
            type: type,
            url: url,
            data: data,
            success: function(response) {
                if (response.status == true) {
                    window.location = response.url;
                } else {
                    alert(response.reason);

                }
            },
            dataType: 'json'
        });
}

function exportTimecard(id){
    $("#timecard_id").val(id);
    $("#is_timecard").val(1);
        $.ajax({
            type: "POST",
            url: $("#timecard_export_url").val(),
            data: $("#myForm").serialize(),
            success: function(response) {
                if (response.status == true) {
                    window.location = response.url;
                } else {
                    alert(response.reason);

                }
            },
            dataType: 'json'
        });
}

function showTimeCard(id) {

    $.ajax({
        type: "POST",
        url: $("#card_url").val(),
        data: {'parent': id},
        beforeSend: function(jqXHR, settings) {
            jQuery(".childEntryContainer").html("Loading..........");
            jQuery(".childEntryContainer").fadeIn();
        },
        success: function(response) {
            if (response.status == true) {
                jQuery(".childEntryContainer").html(response.data);
            } else {
                alert(response.reason);

            }
        },
        dataType: 'json'
    });

    return true;

}

//function exportData() {
//    $.ajax({
//        type: "POST",
//        url: $("#export_url").val(),
//        data: $("#myForm").serialize(),
//        success: function (response) {
//            if (response.status == true) {
//                window.location = response.url;
//            } else {
//                alert(response.reason);
//            }
//        },
//        dataType: 'json'
//    });
//}

function submitForm()
{
    if ($(".check").is(":checked"))
    {
        $.ajax({
            type: "POST",
            url: $("#url").val(),
            data: $("#myForm").serialize(),
            success: function(response) {
                if (response.status == true) {
                    window.location = response.url;
                } else {
                    alert(response.reason);

                }
            },
            dataType: 'json'
        });
    }
    else {
        alert($('#select_msg').val());
        return;
    }
}