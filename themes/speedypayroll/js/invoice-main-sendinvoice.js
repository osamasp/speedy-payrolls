    $(document).ready(function() {
        $('#date').datepicker({format: 'dd/mm/yyyy'});
        $('#form-invoice').h5Validate({errorClass: 'errorClass'});
        
        $('.Model').on('show.bs.modal', function (event) {         
                            var button = $(event.relatedTarget); // Button that triggered the modal
                            var url = button.data('whatever'); // Extract info from data-* attributes
                            var modal = $(this);
                              $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: $("#form-invoice").serialize(),
                                    beforeSend:function(jqXHR,settings){
                                            jQuery(modal).find('.modal-content').text("Loading..........");
                                            
                                    },
                                    success: function(response){
                                        if(response.status==true){
                                            jQuery(modal).find('.modal-content').html(response.data);
                                        }else{
                                            jQuery(modal).modal('hide');
                                             alert(response.reason);
                                    
                                        }
                                    },
                                    dataType: 'json'
                                  });  
                                    });
                                    
        
    });
    function validate()
    {
        if ($("#invoice_ref").val() == "" || $("#name").val() == "" || $("#email").val() == "" || $("#cl_add").val() == "" || $("#cl_city").val() == "" || $("#cl_pc").val() == "" || $("#cl_country").val() == "")
        {
            alert("Please fill in all required fields...");
            return false;
        }
        $("#sendinvoice").click();
        return true;
    }
    function check() {
        if (document.getElementById("contacts").value != 'out') {
            document.getElementById("email").disabled = true;
            document.getElementById("name").disabled = true;
            document.getElementById("cl_add").disabled = true;
            document.getElementById("cl_country").disabled = true;
            document.getElementById("cl_pc").disabled = true;
            document.getElementById("cl_city").disabled = true;
        }
        else {
            document.getElementById("email").disabled = false;
            document.getElementById("name").disabled = false;
            document.getElementById("cl_add").disabled = false;
            document.getElementById("cl_country").disabled = false;
            document.getElementById("cl_pc").disabled = false;
            document.getElementById("cl_city").disabled = false;
        }
        
        var id = jQuery("#contacts").val();
        var url = $("#contact_details").val();
        $.post(url, {contact_id: id},
        function(result) {
            var data = JSON.parse(result);
            if (data !== null)
            {
                document.getElementById("email").value = data.email;
                document.getElementById("name").value = data.name;
                document.getElementById("cl_add").value = data.address;
                document.getElementById("cl_country").value = data.country;
                document.getElementById("cl_pc").value = data.post_code;
                document.getElementById("cl_city").value = data.city;
            }
            else
            {
                document.getElementById("email").value = "";
                document.getElementById("name").value = "";
                document.getElementById("cl_add").value = "";
                document.getElementById("cl_country").value = "United Kingdom";
                document.getElementById("cl_pc").value = "";
                document.getElementById("cl_city").value = "";
            }
        });
    }

    function addItem(count)
    {
        var n = 'count';
        var c = count - 1;
        var res = n.concat(c);
        var elem = document.getElementById(res);
        var val = elem.value;
        val++;
        elem.value = val;
        var table = document.getElementById("myTable");

// Create an empty <tr> element and add it to the 1st position of the table:
        var row = table.insertRow(val);

// Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        var item = document.createElement("INPUT");
        var i = 'item';
        var itemName = i.concat(val - 1);
        item.setAttribute("type", "text");
        item.setAttribute("name", itemName);
        item.setAttribute("class", "form-control");
        item.setAttribute("required", true);
        var unit = document.createElement("INPUT");
        i = 'unit';
        itemName = i.concat(val - 1);
        unit.setAttribute("type", "text");
        unit.setAttribute("name", itemName);
        unit.setAttribute("class", "form-control");
        unit.setAttribute("required", true);
        var rate = document.createElement("INPUT");
        i = 'rate';
        itemName = i.concat(val - 1);
        rate.setAttribute("type", "text");
        rate.setAttribute("name", itemName);
        rate.setAttribute("class", "form-control");
        rate.setAttribute("required", true);
        var cost = document.createElement("INPUT");
        i = 'cost';
        itemName = i.concat(val - 1);
        cost.setAttribute("type", "text");
        cost.setAttribute("name", itemName);
        cost.setAttribute("class", "form-control");
        cost.setAttribute("required", true);
// Add some text to the new cells:
        cell1.appendChild(item);
        cell2.appendChild(unit);
        cell3.appendChild(rate);
        cell4.appendChild(cost);
    }

    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }