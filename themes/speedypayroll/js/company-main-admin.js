jQuery(document).ready(function(){
$('.search-button').click(function(){
            $('.search-form').toggle();
            return false;
    });
    $('.search-form form').submit(function(){
            $('#company-grid').yiiGridView('update', {
                    data: $(this).serialize()
            });
            return false;
    });

    $('.deleterow').on('click',{},deleteRow);
    
    $('#example1').on('draw',function(){
        $('.deleterow').on('click',{},deleteRow);
    });
});
    function deleteRow(event)
    {
        var curr = event.currentTarget;
        var ask = window.confirm('Are you sure you want to delete this company?');
        if (ask) {
            var url = $(curr).children('a').attr('data');
            
            $.ajax({url:url , success: function(result){
                var killrow = $(curr).closest('tr');
                killrow.addClass('danger');
                killrow.fadeOut(2000, function(){
                    $(curr).remove();
                });
            }});
        }
    }