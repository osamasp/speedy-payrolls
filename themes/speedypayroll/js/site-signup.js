jQuery(document).ready(function() {
        // initiate layout and plugins
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        Demo.init(); // init demo features
        FormWizard.init();
    });
    
    function selection(id) {
        if (id === 0) {
            $("#payrollerSelect").prop("checked", false);
            if($("#payrollerSelect").closest("span").hasClass("checked"))
                $("#payrollerSelect").closest("span").removeClass("checked");
            $("#companySelect").prop("checked", true);
            $("#companySelect").closest("span").addClass("checked");
        }
        else {
            $("#companySelect").prop("checked", false);
            if($("#companySelect").closest("span").hasClass("checked"))
                $("#companySelect").closest("span").removeClass("checked");
            $("#payrollerSelect").prop("checked", true);
            $("#payrollerSelect").closest("span").addClass("checked");
        }
    }