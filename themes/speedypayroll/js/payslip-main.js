jQuery(document).ready(function(){
                
                

                
                
                 /**** Get Timesheets JS ****/
                         
                    jQuery('.wklyLnkPrnt').on('click',function(){
                           
                           $.ajax({
                                    type: "POST",
                                    url: $("#url").val(),
                                    data: {'parent':jQuery(this).attr('tSheetId')},
                                    beforeSend:function(jqXHR,settings){
                                            jQuery(".childEntryContainer").html("Loading..........");
                                            jQuery(".childEntryContainer").fadeIn();
                                    },
                                    success: function(response){
                                        if(response.status==true){
                                            jQuery(".childEntryContainer").html(response.data);
                                        }else{
                                             alert(response.reason);
                                    
                                        }
                                    },
                                    dataType: 'json'
                                  });
                           
                           return false;
                           
                    });
                        
                                    
                        $('.ajaxModel').on('show.bs.modal', function (event) {
                                    
                                    
                            var button = $(event.relatedTarget) // Button that triggered the modal
                            var recipient = button.data('whatever') // Extract info from data-* attributes
                            var url = button.data('url');
                            var modal = $(this)
                              $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: {'entryId':recipient},
                                    beforeSend:function(jqXHR,settings){
                                            jQuery(modal).find('.modal-content').text("Loading..........");
                                            
                                    },
                                    success: function(response){
                                        if(response.status==true){
                                            jQuery(modal).find('.modal-content').html(response.data);
                                        }else{
                                            jQuery(modal).modal('hide');
                                             alert(response.reason);
                                             
                                        }
                                    },
                                    dataType: 'json'
                                  });            
                            
//                           
                          })            

                           
            });