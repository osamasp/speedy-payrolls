jQuery(document).ready(function () {

    //alert("1");

$('#myDropdown .dropdown-menu').on({
        "click": function(e) {
            e.stopPropagation();
        }
    });

    /**** Get Timesheets JS ****/

    $('.ajaxModel').on('show.bs.modal', function (event) {
        //alert("2");         
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('whatever') // Extract info from data-* attributes
        var modal = $(this)
        $.ajax({
            type: "POST",
            url: $("#url").val(),
            data: {'id': recipient},
            beforeSend: function (jqXHR, settings) {
                jQuery(modal).find('.modal-content').text("Loading..........");

            },
            success: function (response) {
                if (response.status == true) {
                    jQuery(modal).find('.modal-content').html(response.data);
                } else {
                    jQuery(modal).modal('hide');
                    alert(response.reason);

                }
            },
            dataType: 'json'
        });
    });

    $('.Model').on('show.bs.modal', function (event) {
//                                           alert("2");         
        var button = $(event.relatedTarget); // Button that triggered the modal
        var url = '';
        var type = '';
        var data = {'id': 0};
        if (button[0].attributes.id.value == "invoice") {
            url = button.data('whatever');
            type = "GET";
            data = {'id': url};
        }
        var modal = $(this);
        $.ajax({
            type: type,
            url: url,
            data: data,
            beforeSend: function (jqXHR, settings) {
                jQuery(modal).find('.modal-content').text("Loading..........");

            },
            success: function (response) {
                if (response.status == true) {
                    jQuery(modal).find('.modal-content').html(response.data);
                } else {
                    jQuery(modal).modal('hide');
                    alert(response.reason);

                }
            },
            dataType: 'json'
        });
    });
//                           
});

function exportData(id){
    var url = '';
        var type = '';
        var data = {'id': 0};
        if (id == "export") {
            url = $("#export_url").val();
            type = "POST";
            data = $("#myForm").serialize();
            if (!$(".check").is(":checked"))
            {
                jQuery(modal).modal('hide');
                alert($('#select_msg').val());
                return;
            }
        } else if (id == "export_all") {
            url = $("#export_url").val();
            type = "POST";
            data = $("#myForm").serialize();
        }
        
        $.ajax({
            type: type,
            url: url,
            data: data,
            success: function (response) {
                if (response.status == true) {
                    window.location = response.url;
                } else {
                    alert(response.reason);
                }
            },
            dataType: 'json'
        });
}