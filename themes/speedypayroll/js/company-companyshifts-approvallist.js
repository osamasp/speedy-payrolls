jQuery(document).ready(function(){
   $('#selectAll').click(function (event) {  //on click 
        if ($("#All").is(":checked")) { // check select status
            $('.select').each(function () { //loop through each checkbox
                this.checked = false;  //select all checkboxes with class "checkbox1"               
            });
        } else {

            $('.select').each(function () { //loop through each checkbox
                this.checked = true; //deselect all checkboxes with class "checkbox1"                       
            });
        }
    });
    $('#openButton').on('click', function (event) {
        $(this).parent().toggleClass('open');
    }); 
});

function showModel(id)
{
    var modal = $("#portlet-config");
    $("#portlet-config").show();
    $("#model_id").val(id);
    $("#model_html").val(jQuery(modal).find('.modal-body').html());
    jQuery(modal).find('.modal-body').html("<div class='form-group'><b>Reason:</b> <textarea name='reason' id='reason' class='form-control'></textarea></div><div class='form-group'><a onclick='backToModel(" + id + ")'>Back</a></div>");
}

function backToModel(id)
{
    var modal = $("#portlet-config");
    $("#portlet-config").hide();
    $("#portlet-config").show();
    jQuery(modal).find('.modal-body').html($("#model_html").val());
}

$('.Model').on('show.bs.modal', function (event) {
//                                           alert("2");         
    var button = $(event.relatedTarget) // Button that triggered the modal
    var recipient = button.data('whatever') // Extract info from data-* attributes
    var modal = $(this)
    var url = '';
    var type = '';
    var data = {'id': recipient};
    if (button[0].attributes.id.value == "updateShift") {
        url = $("#update_url").val();
        type = "GET";
        data = {'id': recipient};
    } else if (button[0].attributes.id.value == "viewShift") {
        url = $("#view_url");
        type = "POST";
        data = {'id': recipient};
    }

    $.ajax({
        type: type,
        url: url,
        data: data,
        beforeSend: function (jqXHR, settings) {
            jQuery(modal).find('.modal-title').html('<h2 style="font-family:\'Avenir Next\',\'Avenir Next LT W01 SC\',\'Helvetica Neue\', Helvetica, Arial, sans-serif;font-weight: bold;font-size:24px;color:#7b8cb8;">Edit Timesheet</h2>')
            jQuery(modal).find('.modal-body').text("Loading..........");

        },
        success: function (response) {
            if (response.status == true) {
                jQuery(modal).find('.modal-body').html(response.data);
            } else {
                jQuery(modal).modal('hide');
                alert(response.reason);

            }
        },
        dataType: 'json'
    });
});

$("#saveModel").on('click', function () {
    var button = $("#updateShift") // Button that triggered the modal
    var recipient = $("#model_id").val(); // Extract info from data-* attributes
    updateStatus(recipient, 'disapproved', $("#reason").val());
});

function updateStatus(id, status, reason)
{
    var url = $("#update_approval_url").val();
    $.ajax({
        url: url,
        type: 'POST',
        data: {'id': id, 'status': status, 'reason': reason},
        success: function (response) {
            if (response === 'success') {
                window.location.reload();
            }
            if (response === 'failed')
            {
                alert("Sorry, this timesheet can't be updated");
            }
        }
    });
}

function exportData(id){
        var url = '';
        var type = '';
        var data = {'id': ''};
        if (id == "export") {
            url = $("#export_url").val();
            type = "POST";
            data = $("#myForm").serialize();
            if (!$(".check").is(":checked"))
            {
                alert($('#select_msg').val());
                return;
            }
        } else if (id == "export_all") {
            url = $("#export_url").val();
            type = "POST";
            data = $("#myForm").serialize();
        }
        
        $.ajax({
            type: type,
            url: url,
            data: data,
            success: function (response) {
                if (response.status == true) {
                    window.location = response.url;
                } else {
                    alert(response.reason);

                }
            },
            dataType: 'json'
        });
    }