    $('body').on('click', '.remove-closest-item-row', function () {
        $(this).closest('.item-row').remove();
    });
    $('#add-more-payrates').on('click', function () {
        var html = '<div class="row form-group item-row"><div class="col-md-11"><input type="text" name="payrate[]" value="" class="form-control"></div><a href="javascript:void(0);" class="remove-closest-item-row" ><i class="fa fa-fw fa-times-circle"></i></a></div>';
        $(html).insertBefore('#payrate-before');
    });
    $('#add-more-items').on('click', function () {
        var html = '<div class="row form-group item-row"><div class="col-md-11"><input type="text" name="item[]" value="" class="form-control"></div><a href="javascript:void(0);" class="remove-closest-item-row" ><i class="fa fa-fw fa-times-circle"></i></a></div>';
        $(html).insertBefore('#item-before');
    });