var approvertimer;
function setday(day)
{
    $("#day").val(day);
}
$(document).ready(function () {
     
    $("#contract-form").validate({
        // Specify the validation rules
        rules: {
            'Contract[admin_email]': {
                required: true,
                email: true
            },
            'Contract[approver_id]': {
                required: true
            },
            'Contract[company_email]': {
                  required: { depends: function(element) {
                  return true;
                  }
                },
                
            },
            'Contract2[admin_email]':{
                required: {
                depends: function(element) {
                  return $("#optionsRadios3").is(":checked");
                }
            },
            },
            'Contract[user_id]':{
                required: {
                depends: function() {
                  return $("#optionsRadios2").is(":checked");
                }
            },
            }
        },
        // Specify the validation error messages
        messages: {
            'Contract[admin_email]': "Please enter Client Company Email",
            'Contract[approver_id]': "Kindly select Approver",
            'Contract[company_email]': "Please enter Contractor Company Email",
            'Contract2[admin_email]': "Please enter Client Company Email",
            'Contract[user_id]': "Please slelect Employee"
        },
        submitHandler: function () {
            addContract($("#token").val());
        }
    });

  $("#optionsRadios2").click(function () {
        $("#two").show();
        $("#three").hide();
        $("#collection_two").show();
        $("#collection_three").hide();
        $("#client").hide();
        $("#contractor").show();
        $("#contractors").show();
        $("#cdetails").html('');
        $("#payratesanditems").html('');
        $("#clients").val('');
        $("#contractors").val('');
        $("#client_three").val('');
        $("#contractor_three").val('');
        $(".contractor-contrainer").hide();
        $("#contract_email_label").html('');
        $("#contractors_show_link").hide();
    });
    $("#optionsRadios1").click(function () {
        $("#two").show();
        $("#three").hide();
        $("#contractor").hide();
        $("#collection_two").show();
        $("#collection_three").hide();
        $("#client").show();
        $("#cdetails").html('');
        $("#payratesanditems").html('');
        $("#clients").val('');
        $("#contractors").val('');
        $("#client_three").val('');
        $("#contractor_three").val('');
        $("#clients").show();
        $("#clients11").html('');
        $("#clients_show_link").hide();
        
    });
    $("#optionsRadios3").click(function () {
        $("#two").hide();
        $("#three").show();
        $("#collection_two").hide();
        $("#collection_three").show();
        $("#cdetails").html('');
        $("#payratesanditems").html('');
        $("#clients").val('');
        $("#contractors").val('');
        $("#client_three").val('');
        $("#contractor_three").show();
        $("#contractor_three").val('');
        $("#contractor_show_link").hide();
        $("#contractor_three_label").html('');
        $("#client_three").show();
        $("#client_three_label").html('');
    });
    
    $('body').on('keyup', '#approver_email', function () {
        if ($('#approver_email').val() != '') {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (re.test($('#approver_email').val())) {
                clearTimeout(approvertimer);
                approvertimer = setTimeout('checkapprover()', 1500);
            }
        }
    });
});


    $(document).ready(function () {
        $('#cc-search1').click(function () {
            if ($('#contractors').val() != '' || $("#clients").val() != '') {
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                if (re.test($('#contractors').val()) || re.test($("#clients").val())) {
                    start_ajax2();
                }
            }
        });

        $('#cc-three-way1').click(function () {
            if ($('#contractor_three').val() != '') {
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if (re.test($('#contractor_three').val())) {
                    start_ajax2();
                }
            }

        });

        $('body').on('click', '#confirm-companycc2', function () {
            //fillEmployee($('#cc_id2').val());
            //$('#confirm-companycc2').remove();
            confirm2 = true;
            fillApprover();
            $("#confirmed").show();
            $("#contractor_three").hide();
            $("#client_three").hide();
            $("#client_three_label").html( $("#client_three").val());
            $("#client_three_inv").hide();
            $("#contractor_three_lebel").html($("#contractor_three").val());
            
            $( "input[class*='contractor_name']" ).hide();
            $("#contractors_c_name_label").html($( "input[class*='contractor_name']" ).val());
            
            $( "input[name*='cc2[company_name]']" ).hide();
            $("#contractors_c_email_labelcc2").html($( "input[name*='cc2[company_name]']" ).val());
            
            $( "input[name*='cc2[user_email]']" ).hide();
            $("#contractor_email_labelcc2").html($( "input[name*='cc2[user_email]']" ).val());
            
            $( "input[name*='cc2[company_phone]']" ).hide();
            $("#company_phone_labelcc2").html($( "input[name*='cc2[company_phone]']" ).val());
            
            $( "input[name*='cc2[contractor_phone]']" ).hide();
            $("#contractor_phone_labelcc2").html($( "input[name*='cc2[contractor_phone]']" ).val());
            
            $( "input[name*='cc2[company_address]']" ).hide();
            $("#company_address_labelcc2").html($( "input[name*='cc2[company_address]']" ).val());
            
            $( "input[name*='cc2[city]']" ).hide();
            $("#city_labelcc2").html($( "input[name*='cc2[city]']" ).val());
            
            $( "input[name*='cc2[postcode]']" ).hide();
            $("#post_code_labelcc2").html($( "input[name*='cc2[postcode]']" ).val());
        });

        $('#cc-three-way2').click(function () {
            if ($('#client_three').val() != '') {
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if (re.test($('#client_three').val())) {
                    start_ajax22();
                }
            }

        });

    });

function addContract(token) {
//    $("#contract-form").validate();
//    return;
//    if(($("#clients").val() == "" || $("#approver").val() == "" || $("#collection_type").val() == "") && $("#optionsRadios1").is(":checked")){
//        alert('kindly provide all the required informaton');
//    }
//    else if(($("#contractors").val() == "" || $("#employee").val() == "" || $("#approver").val() == "" || $("#collection_type").val() == "" ) && $("#optionsRadios2").is(":checked")){
//        alert('kindly provide all the required informaton');
//    }
//    else if(($("#contractor_three").val() == "" || $("#client_three").val() == "" || $("#approver").val() == "" || $("#collection_type").val() == "" ) && $("#optionsRadios3").is(":checked")){
//        alert('kindly provide all the required informaton');
//    } else
//    {
    var str = $("#contract-form").serialize();
    var baseUrl = $("#baseUrl").val();
    $("#overlay").show();
    var type = $('input[name="Contract[type]"]:checked', '#contract-form').val();
    $.ajax({
        method: "POST",
        url: "" + baseUrl + "/webservice/Contract/type/" + type,
        data: str,
        headers: {'TOKEN': token},
        statusCode: {
            404: function (response) {
                alert('Page Not Found');
        $("#overlay").hide();
                console.log('404');
            },
            400: function(response){
              alert(JSON.parse(response.responseText).Result.company);
        $("#overlay").hide();
              console.log('400');
            },
            403: function (response) {
                console.log('403');
        $("#overlay").hide();
            },
            401: function (response) {
                alert('Invalide Access Token');
        $("#overlay").hide();
                console.log('401');
            },
            201: function () {
                console.log('201');
        $("#overlay").hide();
            }
        }
    }).success(function (msg) {
        $("#overlay").hide();
        window.location.href = $("#return_url").val();

    }
    );
//    }
}

function show()
{
    if ($("#contractor").val() == "" || $("#email").val() == "" || $("#companyId").val() == "")
    {
        alert('Please fill in all required fields....');
        return;
    }
    if ($("#company_email").val() == "") {
        alert('Please fill in all required fields.....');
        return;
    }
    var isSetPayrates = true;
    $("input[name^='Payrate'],input[name^='Item']").each(function (index)
    {
        if ($(this).val() === "" || $(this).val() === "0.00" || $(this).val() === null)
        {
            isSetPayrates = false;
        }
    });

    if (!isSetPayrates) {
        alert("Please apply payrates......");
        return;
    }


    if (($("#companyId").val() == "0" /*|| $("#policy").val()==""*/))
    {
        if ($("#company_name").val() == "" || $("#contractor_email").val() == "" || $("#company_phone").val() == "" || $("#company_address").val() == "" || $("#city").val() == "" || $("#post_code").val() == "" || $("#country").val() == "")
        {
            alert('Please fill in all required fields.....');
            return;
        }
    }
    else if ($("#approver").val() == "0" && $("#approver_valid").val() == "0")
    {
        alert('Please fill in all required fields.....');
        return;
    }
}

function checkapprover() {
    $.ajax({
        url: $('#user_exist').val(),
        data: {email: $('#approver_email').val(),
            companyId: $('#companyId').val()},
        type: 'GET',
        success: function (response) {
            $('#approver_valid').val(response);
            if (response == 0) {
                $('#approver-email-message').html("<p style='color:red;'>No user found with this email address.</p>");
            } else {
                $('#approver-email-message').html("<p style='color:green;'><i class='fa fa-fw fa-check'></i> User found.</p>");
            }
        }
    });
}


function start_ajax2() {
    var flag = 0;
    var ajaxLoader = $("#ajax_loader").val();
    if ($("#clients").val() != '') {
        var email = $("#clients").val();
    }
    else if ($("#contractors").val() != '') {
        var email = $("#contractors").val();
    }
    else if ($("#contractor_three").val() != '') {
        var email = $("#contractor_three").val();
        flag = 1;
    }
    else if ($("#client_three").val() != '') {
        var email = $("#client_three").val();
        flag = 1;
    }
    $.ajax({
        url: $("#company_details").val(),
        data: {email: email},
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            if (response.status == true) {
                if (flag == 1) {
                    $("#cdetails_three").html(response.data);
                }
                else {
                    $("#cdetails").html(response.data);
                }
                if (response.company != null && response.company != 0) {
                    if (flag == 1) {
                        $("#companyId2").val(response.company);
                    }
                    else {
                        $("#companyId").val(response.company);
                    }
                } else {
                    $("#companyId").val('0');
                }
            } else {
                $("#cdetails").html("No Company Found.");

            }
        },
        beforeSend: function () {
            $("#cdetails").html(ajaxLoader);
        }
    });
}

function start_ajax22() {
    var ajaxLoader = $('#ajax_loader').val();
    $.ajax({
        url: $('#company_details').val(),
        data: {email: $('#client_three').val(), inputArrName: "cc2"},
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            if (response.status == true) {
                $("#cdetails_three2").html(response.data);
                if (response.company != null && response.company != 0) {
                    $("#companyId").val(response.company);
                } else {
                    $("#companyId").val('0');
                }
            } else {
                $("#cdetails_three2").html("No Company Found.");
            }
        },
        beforeSend: function () {
            $("#cdetails_three2").html(ajaxLoader);
        }
    });
}

var confirm2 = false;

function checkPayrates() {
    var isSetPayrates = true;
    $("input[name^='Payrate'],input[name^='Item']").each(function (index)
    {
        if ($(this).val() === "" || $(this).val() === "0.00" || $(this).val() === null)
        {
            isSetPayrates = false;
        }
    });

    if (!isSetPayrates) {
        $("#successDiv").hide();
        $("#errorDiv > span").text("Please set Payrates.");
        $("#errorDiv").show();
    }
    else
    {
        $("#errorDiv").hide();
        $("#successDiv > span").text("Payrates have been applied successfully.");
        $("#successDiv").show();
    }
}

function fillApprover()
{
    if ($("#contractor_email").val() != '' && $("#contractor_email").val() != null) {
        $("#approver_name").val($("#contractor_name").val());
        $("#approver_email").val($("#contractor_email").val());
        $("#approver_phone").val($("#contractor_phone").val());
        $("#approver_name").attr('disabled', true);
        $("#approver_email").attr('disabled', true);
        $("#approver_phone").attr('disabled', true);
        $('#approver_valid').val("1");
    }
    else {
        $("#approver_name").val('');
        $("#approver_email").val('');
        $("#approver_phone").val('');
        $("#approver_name").attr('disabled', false);
        $("#approver_email").attr('disabled', false);
        $("#approver_phone").attr('disabled', false);
    }
}

$('body').on('click', '#add-new-company', function () {
    $('.cc-remove-me').remove();
    $('.find-ccompany').val('');
    $('.find-ccompany').attr('disabled', '');
    $('#cc-show-me').show(800);
    $('#cc-show-me input').attr('required', '');
});

$(function () {
    $('#collection_type').on('change', function () {
        if ($(this).val() == 0) {
            $("#collection_day").hide();
            $("#collection_period").hide();
        }
        else {
            $("#collection_day").show();
            $("#collection_period").show();
        }
    });
});

$(document).ready(function () {

    $('#submitBtn').on('click', function (e) {
        var error = '';
        if (!confirm2) {
            error += 'Please select Company they will be working for\n';
        }
        if (error != '') {
            e.preventDefault();
            alert(error);
        } else {
            $('#submitBtnhidden').click();
        }
    });
});

function submitForm() {
    $("#overlay").show();
    $("form#user-form").submit();
}

function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
function run() {
    if (document.getElementById("timesheet_approver1").checked) {
        $("#outside").show();
        $("#inside").hide();
        fillApprover();
    }
    else {
        $("#outside").hide();
        $("#inside").show();
    }
}


function fillEmployee(company_id) {
    var ajaxLoader = $("#ajax_loader").val();
    $.ajax({
        url: $("#get_employee").val(),
        data: {company_id: company_id},
        type: 'POST',
        dataType: 'json',
        success: function (response) {

            if (response.status == true && !$("#optionsRadios2").is(":checked")) {
                jQuery(".contractor-contrainer").html(response.data);
                jQuery('#payratesanditems').html(response.payrates);
            } else {
                jQuery(".contractor-contrainer").html("No Contrator Found.");
            }
        },
        beforeSend: function () {
            jQuery(".contractor-contrainer").html(ajaxLoader);
        }
    });
}

function checkcompany() {
    $.ajax({
        url: $("#company_exist").val(),
        data: {name: $("#company_name").val()},
        type: 'GET',
        success: function (response) {
            if (response == 1) {
                $("#company_name").val('');
                alert("Company with the same name already exist. Please enter any other name.");
                $("#company_name").focus();
            }
        }
    });
}

var confirm1 = false;

var approvertimer;
$(document).ready(function () {
    $('body').on('focusout', '#company_name', function () {
        checkcompany();
    });
    $("#coll_freq").focus();
    $('body').on('click', '#confirm-company', function () {
        $('#confirm-company').remove();
        $("#confirmed").show();
        confirm2 = true;
        $("#clients").hide();
        $("#contractors").hide();
        $("#client_env").hide();
        $("#contractors_env").hide();
        $("#contractors_show_link").show();
        $("#clients11").html($("#clients").val());
        $("#contract_email_label").html($("#contractors").val());
        $("#clients_show_link").show();
        $("#contractor_three").hide();
        $("#contractor_three_env").hide();
         $("#contractor_show_link").show();
         
        $("#contractor_three_label").html($("#contractor_three").val());
        $( "input[name*='company_name']" ).hide();
        $("#contractors_c_email_labelcc").html($( "input[name*='company_name']" ).val());
        $( "input[name*='user_name']" ).hide();
        $("#contractors_c_name_labelcc").html($( "input[name*='user_name']" ).val());
        $( "input[name*='user_email']" ).hide();
        $("#contractor_email_labelcc").html($( "input[name*='user_email']" ).val());
        
        $( "input[name*='company_phone']" ).hide();
        $("#company_phone_labelcc").html($( "input[name*='company_phone']" ).val());
        
        $( "input[name*='contractor_phone']" ).hide();
        $("#contractor_phone_labelcc").html($( "input[name*='contractor_phone']" ).val());
        
        $( "input[name*='company_address']" ).hide();
        $("#company_address_labelcc").html($( "input[name*='company_address']" ).val());
        
        $( "input[name*='city']" ).hide();
        $("#city_labelcc").html($( "input[name*='city']" ).val());
        
        $( "input[name*='postcode']" ).hide();
        $("#post_code_label").html($( "input[name*='postcode']" ).val());
        
        $( "input[name*='country']" ).hide();
        $("#country_label").html($( "input[name*='country']" ).val());
        
       
        fillApprover();
    });

    $('body').on('click', '#confirm-company', function () {
        fillEmployee($('#cc_id').val());
        $('#confirm-company').remove();
        confirm1 = true;
        $("#confirmed").show();
        
    });
    $('body').on('click', '#add-new-company', function () {
        $('.cc-remove-me').remove();
        $('.find-ccompany').val('');
        $('.find-ccompany').attr('disabled', '');
        $('#cc-show-me').show(800);
        $('#cc-show-me input').attr('required', '');
    });
    $('body').on('keyup', '#approver_email', function () {
        if ($('#approver_email').val() != '') {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (re.test($('#approver_email').val())) {
                clearTimeout(approvertimer);
                approvertimer = setTimeout('checkapprover()', 1500);
            }
        }
    });

});

function selectInsideCompany()
{
    jQuery("#timesheet_approver").prop('checked', 'checked')
    run()
}