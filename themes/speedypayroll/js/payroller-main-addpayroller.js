     function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

    $(document).ready(function () {
        $("#start_date").datepicker({
            format: $("#date_format").val(),
            todayHighlight: true,
            autoclose: true
         });
        $("#end_date").datepicker({
            format: $("#date_format").val(),
            todayHighlight: true,
            autoclose: true
         });

    });