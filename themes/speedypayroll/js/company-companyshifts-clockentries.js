$(document).ready(function () {
    
    $('#myDropdown .dropdown-menu').on({
        "click": function(e) {
            e.stopPropagation();
        }
    });
    
    $('.Model').on('show.bs.modal', function (event) {
//                                           alert("2");         
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('whatever') // Extract info from data-* attributes
        var modal = $(this)
        var url = '';
        var type = '';
        var data = {'id': recipient};
        if (button[0].attributes.id.value == "updateShift") {
            url = $("#update_shift_url").val();
            type = "GET";
            data = {'id': recipient};
        } else if (button[0].attributes.id.value == "viewShift") {
            url = $("#view_url");
            type = "POST";
            data = {'id': recipient};
        }


        $.ajax({
            type: type,
            url: url,
            data: data,
            beforeSend: function (jqXHR, settings) {
                jQuery(modal).find('.modal-title').html('<h2 style="font-family:\'Avenir Next\',\'Avenir Next LT W01 SC\',\'Helvetica Neue\', Helvetica, Arial, sans-serif;font-weight: bold;font-size:24px;color:#7b8cb8;">Edit Timesheet</h2>')
                jQuery(modal).find('.modal-body').text("Loading..........");

            },
            success: function (response) {
                if (response.status == true) {
                    jQuery(modal).find('.modal-body').html(response.data);
                } else {
                    jQuery(modal).modal('hide');
                    alert(response.reason);

                }
            },
            dataType: 'json'
        });
    });

    $('.ajaxModel').on('show.bs.modal', function (event) {
//                                           alert("2");         
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('whatever') // Extract info from data-* attributes
        var modal = $(this)
        var url = $("#view_url").val();
        $.ajax({
            type: "POST",
            url: url,
            data: {'id': recipient},
            beforeSend: function (jqXHR, settings) {
                jQuery(modal).find('.modal-content').text("Loading..........");

            },
            success: function (response) {
                if (response.status == true) {
                    jQuery(modal).find('.modal-content').html(response.data);
                } else {
                    jQuery(modal).modal('hide');
                    alert(response.reason);

                }
            },
            dataType: 'json'
        });
    });

    $("#saveModel").on('click', function () {
        $("#user-timings-form").submit();
    });
    
        $('#selectAll').click(function(event) {  //on click 
        if ($("#All").is(":checked")) { // check select status
            $('.select').each(function() { //loop through each checkbox
                this.checked = false;  //select all checkboxes with class "checkbox1"               
            });
        } else {

            $('.select').each(function() { //loop through each checkbox
                this.checked = true; //deselect all checkboxes with class "checkbox1"                       
            });
        }
    });
    $('#openButton').on('click', function(event) {
        $(this).parent().toggleClass('open');
    });
});

function exportData(id){
        var url = '';
        var type = '';
        var data = {'id': ''};
        if (id == "export") {
            url = $("#export_url").val();
            type = "POST";
            data = $("#myForm").serialize();
            if (!$(".check").is(":checked"))
            {
                alert($('#select_msg').val());
                return;
            }
        } else if (id == "export_all") {
            url = $("#export_url").val();
            type = "POST";
            data = $("#myForm").serialize();
        }
        
        $.ajax({
            type: type,
            url: url,
            data: data,
            success: function (response) {
                if (response.status == true) {
                    window.location = response.url;
                } else {
                    alert(response.reason);

                }
            },
            dataType: 'json'
        });
    }

//function submitForm()
//{
//    if ($(".check").is(":checked"))
//    {
//        $.ajax({
//            type: "POST",
//            url: $("#url").val(),
//            data: $("#myForm").serialize(),
//            success: function (response) {
//                if (response.status == true) {
//                    jQuery(modal).find('.modal-content').html(response.data);
//                } else {
//                    jQuery(modal).modal('hide');
//                    alert(response.reason);
//
//                }
//            },
//            dataType: 'json'
//        });
//    }
//    else {
//        jQuery(modal).modal('hide');
//        alert($('#select_msg').val());
//        return;
//    }
//}