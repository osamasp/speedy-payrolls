function exportData() {
    $.ajax({
        type: "POST",
        url: $("#url").val(),
        data: $("#myForm").serialize(),
        success: function (response) {
            if (response.status == true) {
                window.location = response.url;
            } else {
                alert(response.reason);

            }
        },
        dataType: 'json'
    });
}

function submitForm()
{
    if ($(".check").is(":checked"))
    {
        $("#myForm").submit();
    }
    else {
        alert($('#select_msg').val());
        return;
    }
}