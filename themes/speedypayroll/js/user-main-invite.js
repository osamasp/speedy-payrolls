$(document).ready(function() {
                        $('#user-form').h5Validate({errorClass: 'errorClass'});
                    });
                    $(function() {
                        $('#collection_type').on('change', function() {
                            if ($(this).val() == 2) {

                                document.getElementById("collection_day").style.display = 'none';
                                document.getElementById("collection_period").style.display = 'none';
                            }
                            else if ($(this).val() == 0) {
                                document.getElementById("collection_day").style.display = 'block';
                                document.getElementById("collection_period").style.display = 'none';
                            }
                            else {
                                document.getElementById("collection_day").style.display = 'block';
                                document.getElementById("collection_period").style.display = 'block';
                            }
                        });
                    });
                function isNumberKey(evt)
                {
                    var charCode = (evt.which) ? evt.which : evt.keyCode;
                    if (charCode != 46 && charCode > 31
                            && (charCode < 48 || charCode > 57))
                        return false;

                    return true;
                }

                $(document).ready(function() {
                    $("#start_date").datepicker({
                        format: $("#date_format").val(),
                        todayHighlight: true,
                        autoclose: true
                    });
                    $("#end_date").datepicker({
                        format: $("#date_format").val(),
                        todayHighlight: true,
                        autoclose: true
                    });

                });
                function sendinvite(addmore) {
                    $("#addmore").val(addmore);
                    $('#submit').click();
                }