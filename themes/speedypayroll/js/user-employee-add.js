        function checkPayrates(){
                var isSetPayrates = true;
                $("input[name^='Payrate'],input[name^='Item']").each( function(index)
                {
                    if($(this).val()==="" || $(this).val()==="0.00" || $(this).val()===null)
                    {
                        isSetPayrates = false; 
                    }
                });
                
                    if(!isSetPayrates){
                        $("#successDiv").hide();
                        $("#errorDiv > span").text("Please set Payrates.");
                        $("#errorDiv").show();
                    }
                    else
                    {
                        $("#errorDiv").hide();
                        $("#successDiv > span").text("Staff payrates set successfully.");
                        $("#successDiv").show();
                    }
        }
        function enable()
        {
            if($("#chBox").is(":checked"))
            {
                $("#submit").attr("disabled",false);
            }
            else
            {
                $("#submit").attr("disabled",true);
            }
        }
    function show()
        {
            
            if($("#tab1").is(':visible') && ($("#User_first_name").val() == "" || $("#User_last_name").val() == "" || $("#User_email").val()=="" || $("#User_password").val()=="" || $("#User_ni_number").val()=="" || $("#start_date").val()==""))
            {
                alert("Please fill in all required fields....");
                if($("#User_first_name").val()==""){
                    $("#User_first_name").focus();
                }
                else if($("#User_last_name").val()==""){
                    $("#User_last_name").focus();
                }
                else if($("#User_email").val()==""){
                    $("#User_email").focus();
                }
                else if($("#User_password").val() == ""){
                    $("#User_password").focus();
                }
                else if($("#User_ni_number").val()==""){
                    $("#User_ni_number").focus();
                }
                else if($("#\n\
    start_date").val()==""){
                    $("#start_date").focus();
                }
                return;
            }
            if($("#tab1").is(':visible') && ($("#User_password").val() != $("#retype").val())){
                alert("Passwords not matched");
                $("#retype").focus();
                return;
            }
            if ($("#tab3").is(':visible'))
            {   
                if($("#collection_type").val() === "2" || $("#collection_days").val()==="" || $("#approver_id").val()==="0")
                {
                    alert("Please fill in all required fields......");
                    return;
                }
                var isSetPayrates = true;
                $("input[name^='Payrate'],input[name^='Item']").each( function(index)
                {
                    if($(this).val()==="" || $(this).val()==="0.00" || $(this).val()===null)
                    {
                        isSetPayrates = false; 
                    }
                });
                
                    if(!isSetPayrates){
                        alert("Please apply payrates......");
                        return;
                    }
            }
            
            
            if($("#tab1").is(':visible'))
            {
                $("#menuTab1").removeClass("active");
                $("#menuTab1").addClass("done");
                $("#menuTab2").addClass("active");
                $("#progress").css('width', '50%');
                $(".step-title").html("");
                $(".step-title").html("Step 2 of 4");
                $("#tab1").hide();
                $("#tab2").show();
            }
            else if ($("#tab2").is(':visible')) {
                $("#menuTab2").removeClass("active");
                $("#menuTab2").addClass("done");
                $("#menuTab3").addClass("active");
                $(".step-title").html("");
                $(".step-title").html("Step 3 of 4");
                $("#progress").css('width', '75%');
                $("#tab2").hide();
                $("#tab3").show();
            }
            else if ($("#tab3").is(':visible')) {
                $("#menuTab3").removeClass("active");
                $("#menuTab3").addClass("done");
                $("#menuTab4").addClass("active");
                $("#progress").css('width', '100%');
                $(".step-title").html("");
                $(".step-title").html("Step 4 of 4");
                $("#tab3").hide();
                $("#tab4").show();
                $("#continue").hide();
                $("#submit").show();
            }
        }
        $(function() {
            $('#collection_type').on('change', function() {
                //        alert('hjer');
                if ($(this).val() == 2) {

                    document.getElementById("collection_day").style.display = 'none';
                    document.getElementById("collection_period").style.display = 'none';
                }
                else if ($(this).val() == 0) {
    //                                alert('here');
                    document.getElementById("collection_day").style.display = 'block';
                    document.getElementById("collection_period").style.display = 'none';
                }
                else {
    //                                 alert('here');
                    document.getElementById("collection_day").style.display = 'block';
                    document.getElementById("collection_period").style.display = 'block';
                }
            });
        });
        
        $(document).ready(function() {
            $('#user-form').h5Validate({errorClass: 'errorClass'});
        });
        $(function() {
            $('#collection_type').on('change', function() {
                //        alert('hjer');
                if ($(this).val() == 2) {

                    document.getElementById("collection_day").style.display = 'none';
                    document.getElementById("collection_period").style.display = 'none';
                }
                else if ($(this).val() == 0) {
                    //                                alert('here');
                    document.getElementById("collection_day").style.display = 'block';
                    document.getElementById("collection_period").style.display = 'none';
                }
                else {
                    //                                 alert('here');
                    document.getElementById("collection_day").style.display = 'block';
                    document.getElementById("collection_period").style.display = 'block';
                }
            });
        });
        
        function checkPassword() {
            if (document.getElementById('User_password').value != document.getElementById('retype').value) {
                alert('The passwords do not match!');
                return false;
            }
        }


        function isNumberKey(evt)
        {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
                    && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        $(document).ready(function() {
            $("#start_date").datepicker({
                format: $("#date_format").val(),
                todayHighlight: true,
                autoclose: true
            });
            $("#end_date").datepicker({
                format: $("#date_format").val(),
                todayHighlight: true,
                autoclose: true
            });

        });