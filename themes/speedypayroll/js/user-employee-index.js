$(document).ready(function() {
    $("#start_date").datepicker({
        format: $("#date_format").val(),
        todayHighlight: true,
        autoclose: true
    });
    $("#end_date").datepicker({
        format: $("#date_format").val(),
        todayHighlight: true,
        autoclose: true
    });
    $(".checker").each(function() {
        this.className = "";
    });
    $('#checkAll').click(function(event) {  //on click 
        if ($("#checkAll").is(":checked")) { // check select status
            $('.check').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        } else {
            $('.check').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });
        }
    });

    $('#myDropdown .dropdown-menu').on({
        "click": function(e) {
            e.stopPropagation();
        }
    });

    $('.Model').on('show.bs.modal', function(event) {
        var modal = $(this);
        $.ajax({
            type: "POST",
            url: $("#url").val(),
            data: $("#myForm").serialize(),
            beforeSend: function(jqXHR, settings) {
                jQuery(modal).find('.modal-content').text("Loading..........");

            },
            success: function(response) {
                if (response.status == true) {
                    jQuery(modal).find('.modal-content').html(response.data);
                } else {
                    jQuery(modal).modal('hide');
                    alert(response.reason);

                }
            },
            dataType: 'json'
        });
    });


});

function assignShift() {
    if ($(".check").is(":checked"))
    {
        var arr = [];
        $('.check').each(function() { //loop through each checkbox

            if (this.checked) {
                arr.push(this.value);
            }
        });

        $.ajax({
            type: "POST",
            url: $("#shift_url").val(),
            data: {shift_id: $('#assign_shift').val(), users: arr},
            success: function(response) {
                location.reload();
            },
            dataType: 'json'
        });
    }
    else {
        alert($('#select_msg').val());
        return;
    }
}

function bulkNotification() {
    if ($(".check").is(":checked"))
    {
    var arr = [];
    $('.check').each(function() { //loop through each checkbox

        if (this.checked) {
            arr.push(this.value);
        }
    });
    window.location = $("#notification_url").val()+'/users/'+arr;
    }
    else {
        alert($('#select_msg').val());
        return;
    }
}


function changePayrate() {
    if ($(".check").is(":checked")) {
        var arr = [];
        $('.check').each(function() { //loop through each checkbox

            if (this.checked) {
                arr.push(this.value);
            }
        });

        $.ajax({
            type: "POST",
            url: $("#payrate_url").val(),
            data: {payrates: $('#payrateForm').serialize(), users: arr},
            success: function(response) {
                location.reload();
            },
            dataType: 'json'
        });
    } else {
        alert($('#select_msg').val());
        return;
    }
}

function assignSupervisor() {
    if ($(".check").is(":checked")) {
        var arr = [];
        $('.check').each(function() { //loop through each checkbox

            if (this.checked) {
                arr.push(this.value);
            }
        });

        $.ajax({
            type: "POST",
            url: $("#supervisor_url").val(),
            data: {supervisor_id: $('#assign_supervisor').val(), users: arr},
            success: function(response) {
                location.reload();
            },
            dataType: 'json'
        });
    } else {
        alert($('#select_msg').val());
        return;
    }
}

function exportData() {
    $.ajax({
        type: "POST",
        url: $("#url").val(),
        data: $("#myForm").serialize(),
        success: function (response) {
            if (response.status == true) {
                window.location = response.url;
            } else {
                alert(response.reason);

            }
        },
        dataType: 'json'
    });
}

function submitForm()
{
    if ($(".check").is(":checked"))
    {
        $.ajax({
            type: "POST",
            url: $("#url").val(),
            data: $("#myForm").serialize(),
            success: function(response) {
                if (response.status == true) {
                window.location = response.url;
                } else {
                    alert(response.reason);

                }
            },
            dataType: 'json'
        });
    }
    else {
        alert($('#select_msg').val());
        return;
    }
}