        jQuery(document).ready(function() {
            // initiate layout and plugins
            Metronic.init(); // init metronic core components
            Layout.init(); // init current layout
            Demo.init(); // init demo features
            FormWizard.init();
	$('body').on('click', '.remove-closest-item-row', function() {
            $(this).closest('.item-row').remove();
        });
        $('#add-more-payrates').on('click', function() {
            var html = '<div class="row form-group item-row"><div class="col-md-11"><input type="text" name="payrate[]" value="" class="form-control"></div><a href="javascript:void(0);" class="remove-closest-item-row" ><i class="fa fa-fw fa-times-circle"></i></a></div>';
            if($("input[name*='payrate']").size()<8)
                $(html).insertBefore('#payrate-before');
            else
                $('#payrate-before').hide();

        });
        $('#add-more-items').on('click', function() {
            var html = '<div class="row form-group item-row"><div class="col-md-11"><input type="text" name="item[]" value="" class="form-control"></div><a href="javascript:void(0);" class="remove-closest-item-row" ><i class="fa fa-fw fa-times-circle"></i></a></div>';
            if($("input[name*='item']").size()<8)
                $(html).insertBefore('#item-before');
            else
                $('#item-before').hide();
        });
        });

        function applyDefaultTemplate(){
            $.ajax({
                url: $("#assign").val(),
                context: document.body
                }).done(function(data) {
                    if(data=="success"){
                        $("#errorDiv").hide();
                        $("#successDiv > span").text("Default template applied successfully.");
                        $("#successDiv").fadeIn();
                        $("#tab4 > .row:first").fadeOut("slow");
                    }
                    else
                    {
                        $("#successDiv").hide();
                        $("#errorDiv > span").text("Unable to apply default template.");
                        $("#errorDiv").show();
                    }

                });
        }
        
         function applyCustomTemplate(){
            //callback handler for form submit
            $("#customTempForm").submit(function(e)
            {
                var postData = $(this).serializeArray();
                var formURL = $(this).attr("action");
                $.ajax(
                {
                    url : formURL,
                    type: "POST",
                    data : postData,
                    success:function(data, textStatus, jqXHR) 
                    {
                        if(data==="success"){
                            $("#errorDiv").hide();
                            $("#successDiv > span").text("Custom template have been sent to admin successfully.");
                            $("#successDiv").fadeIn();
                            $("#tab4 > .row:first").fadeOut("slow");
                        }
                        else
                        {
                            $("#successDiv").hide();
                            $("#errorDiv > span").text("Unable to send custom template.");
                            $("#errorDiv").show();
                        }

                    }
                });
                e.preventDefault(); //STOP default action
                e.unbind(); //unbind. to stop multiple form submit.
            });
            $("#customTempForm").submit();
            return false;
        }
        
        
        function enable()
        {
            if($("#chBox").is(":checked"))
            {
                $("#sendBtn").attr("disabled",false);
            }
            else
            {
                $("#sendBtn").attr("disabled",true);
            }
        }
        
        function submitForm()
        {
            if(!$("#chBox").is(":checked"))
            {
                return;
            }
            else
            {
                window.location.href = $("#verify").val();
            }
        }