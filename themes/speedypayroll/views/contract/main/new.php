<?php

$this->breadcrumbs = array(
    'Users' => array('/user/employee'),
);

$this->menu = array(
    array('label' => 'Create User', 'url' => array('create')),
    array('label' => 'Manage User', 'url' => array('admin')),
);
?>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/contract-main-new.js" type="text/javascript"></script>
<div class="portlet box blue" id="form_wizard_1">
    <div class="portlet-title">
        <div class="caption">
            <?php if($id==1){?>
            <i class="icon-map"></i> Setup Two-way Contract - <span class="step-title">
                Step 1 of 4 </span>
            <?php } else{ ?>
                <i class="icon-map"></i> Setup Three-way Contract - <span class="step-title">
                Step 1 of 4 </span>
            <?php } ?>
        </div>
    </div>
    <div class="portlet-body form">
        <form class="form-horizontal" id="submit_form" method="POST">
            <div class="form-wizard">
                <div class="form-body">
                    <ul class="nav nav-pills nav-justified steps">
                        <li class="active">
                            <a href="#tab1" data-toggle="tab" class="step">
                                <span class="number">
                                    1 </span>
                                <span class="desc">
                                    <i class="fa fa-check"></i> Deployment </span>
                            </a>
                        </li>
                        <?php if ($id == 1) { ?>
                        <li>
                            <a href="#tab2" data-toggle="tab" onclick="javascript:$('#submit_form').submit();" class="step">
                                <span class="number">
                                    2 </span>
                                <span class="desc">
                                    <i class="fa fa-check"></i> Your Details </span>
                            </a>
                        </li>
                        <?php }else{ ?>
                        <li>
                            <a href="#tab2" data-toggle="tab" onclick="javascript:$('#submit_form').submit();" class="step">
                                <span class="number">
                                    2 </span>
                                <span class="desc">
                                    <i class="fa fa-check"></i> Contractor Details </span>
                            </a>
                        </li>
                        <?php } ?>
                        <li>
                            <a href="#tab3" class="step block">
                                <span class="number">
                                    3 </span>
                                <span class="desc">
                                    <i class="fa fa-check"></i> Client Details</span>
                            </a>
                        </li>
                        <li>
                            <a href="#tab4" class="step block">
                                <span class="number">
                                    4 </span>
                                <span class="desc">
                                    <i class="fa fa-check"></i> Confirm </span>
                            </a>
                        </li>
                    </ul>
                    <div id="bar" class="progress progress-striped" role="progressbar">
                        <div class="progress-bar progress-bar-success" style="width:25%">
                        </div>
                    </div>
                    <div class="tab-content">
                        <div class="alert alert-danger display-none">
                            <button class="close" data-dismiss="alert"></button>
                            You have some form errors. Please check below.
                        </div>
                        <div class="alert alert-success display-none">
                            <button class="close" data-dismiss="alert"></button>
                            Your form validation is successful!
                        </div>




                        <!-- tab1 -->


                        <div class="tab-pane active" id="tab1">
                            <h5 class="block">Select one of the options below:</h5>
                            <!--row-->	
                            <?php if ($id == 1) { ?>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="well">
                                            <p>
                                                <a class="btn blue btn-lg button-next hidden-xs" href="javascript:;" onclick="selection(0)">
                                                    <input class="md-radiobtn" id="outRadio" type="radio" name="type" value="out" checked> Create-Client Timesheet</a>
                                                    <a class="btn blue btn-sm button-next hidden-lg hidden-md hidden-xl" href="javascript:;" onclick="selection(0)">
                                                    <input class="md-radiobtn" id="outRadioSm" type="radio" name="type" value="out" checked> Create-Client Timesheet</a>

                                            </p>
                                            <p>
                                                Also works for sub-contractors. 
                                            </p>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="well">
                                            <!--                                                        <button type="button" class="" data-dismiss="alert"></button>-->
                                            <p>
                                                <a class="btn green btn-lg button-next hidden-xs" href="javascript:;" style="padding: 10px 10px 10px 10px;" onclick="selection(1)">
                                                    <input class="md-radiobtn" id="inRadio" type="radio" name="type" value="in"> Create-Contractor Timesheet</a>
                                                <a class="btn green btn-sm button-next hidden-lg hidden-md hidden-xl" href="javascript:;" style="padding: 10px 10px 10px 10px;" onclick="selection(1)">
                                                    <input class="md-radiobtn" id="inRadioSm" type="radio" name="type" value="in"> Create-Contractor Timesheet</a>
                                            </p>
                                            <p>
                                                Also works for sub-contractors.
                                            </p>
                                        </div>
                                    </div>

                                    <!--/span-->
                                </div>
                                <!--/row-->	
                            <?php } else { ?>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="well">
                                            <!--<button type="button" class="" data-dismiss="alert"></button>-->
                                            <p>
                                                <a class="btn blue btn-lg button-next hidden-xs hidden-sm" style="padding: 10px 4px 10px 10px;" href="javascript:;" onclick="javascript:$('#radioLg').prop('checked',true);">
                                                    <input type="radio" class="md-radiobtn" id="radioLg" name="type" value="inout" checked> Contractor working for a client on your behalf</a>
                                                    <a class="btn blue btn-sm button-next hidden-lg hidden-md hidden-xl" style="padding: 10px 4px 10px 10px;" href="javascript:;" onclick="javascript:$('#radioSm').prop('checked',true);">
                                                    <input type="radio" class="md-radiobtn" id="radioSm" name="type" value="inout"> Contractor working for a client on your behalf</a>
                                            </p>
                                            <p>
                                                Suitable for setting up agency role. 
                                            </p>
                                        </div>
                                    </div>
                                    <!--/span-->

                                    <div class="col-md-6 col-sm-12">
                                        <div class="portlet box blue">
                                            <?php $company = AppUser::getUserCompany();?>
                                            
                                                <div class="portlet-title">
                                                        <div class="caption">
                                                        Your Company Details</div></div>
                                                <div class="portlet-body">
                                               
                                                <div class="row static-info"> <div class="col-md-5 value"><?php echo $company->name; ?></div><br>
                                                    <div class="col-md-5 value"><?php echo $company->address; ?></div><br>
                                               <div class="col-md-5 value"><?php echo $company->city; ?></div></div>
                                                </div>
                                                 
                                           
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                            <?php } ?>

                        </div>

                        <!-- /tab1 -->
                    </div>
                </div>
                <div class="form-actions right">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" value="Continue" class='btn btn-circle green-haze'>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- END CONTENT -->