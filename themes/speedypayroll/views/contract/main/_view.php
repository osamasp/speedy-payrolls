<div class="box-body">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('company_id')); ?>:</b>
	<?php echo CHtml::encode($data->company_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('role')); ?>:</b>
	<?php echo CHtml::encode($data->role); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('employee_type')); ?>:</b>
	<?php echo CHtml::encode($data->employee_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('approver_id')); ?>:</b>
	<?php echo CHtml::encode($data->approver_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('payroller_id')); ?>:</b>
	<?php echo CHtml::encode($data->payroller_id); ?>
	<br />
</div>