<?php

$this->breadcrumbs = array(
    'SpeedyTimesheet Accounts' => array('/contract/main'),
    $model ? $model->user->full_name : '',
);

$this->menu = array(
    array('label' => 'Create User', 'url' => array('create')),
    array('label' => 'Manage User', 'url' => array('admin')),
);
?>
<style>
    .form-contract{
        padding-top:10px;
    }    
</style>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/contract-main-edit.js" type="text/javascript"></script>
<!-- BEGIN SAMPLE FORM PORTLET-->
<div class="portlet box green ">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i> Employee Timesheet Account
        </div>
    </div>
    <div class="portlet-body form" style="padding-left:10px;">

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'contract-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-contract',
        'enctype' => 'multipart/form-data',
    ),
        ));
?>

<div class="box-header" style="padding-left:20px;">
    <h4><b>Employee TimeSheet Account</b></h4>
</div>
<div class="box-body" style="padding-left:20px;">
    <div class="col-sm-10">
        <div class="row form-group">
            <label class="col-sm-4 control-label"><b>Name:</b></label>
            <div class="col-sm-4">
                <?php echo $model->user->full_name; ?>
            </div>
        </div>
        <div class="row form-group">
            <label class="col-sm-4 control-label"><b>Timesheet Approver Name:</b></label>
            <div class="col-sm-4">
                <?php echo $model->approver->full_name; ?>  
                <select class="form-control" name="Contract[approver_id]">
                    <option value =''>Change Approver</option>
                    <?php foreach ($approvers as $approver) { ?>
                        <option value="<?php echo $approver->id; ?>"><?php echo $approver->full_name; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>


        <div class="row form-group">
            <label class="col-sm-4 control-label"><b>Approver Telephone:</b></label>
            <div class="col-sm-4">
                <?php echo $model->approver->phone; ?>
            </div>
        </div>
        <div class="row form-group">
            <label class="col-sm-4 control-label"><b>Contract Start Date:</b></label>
            <div class="col-sm-4">
                <?php echo $model->start_time == -1 ? '-' : date(AppInterface::getdateformat(), $model->start_time); ?>
            </div>
        </div>

        <div class="row form-group">
            <label class="col-sm-4 control-label"><b>Contract End Date:</b></label>
            <div class="col-sm-4">
                <?php echo $model->end_time == -1 ? '-' : date(AppInterface::getdateformat(), $model->end_time); ?>
                <input type="text" id="start_date" name="Contract[end_time]" placeholder="End Date" class="form-control">
            </div>
        </div>
        
    <div class="row form-group">
        <label class="col-sm-4 control-label"><b>Upload Contract</b></label>
        <div class="col-sm-4">
            <?php echo $form->fileField($model, 'file_name'); ?>
        </div>
    </div>
        <div class="row form-group">
            <label class="col-sm-4 control-label"><b>Timesheet Frequency:</b></label>
            <div class="col-sm-4">
                <?php echo $model->timesheetfrequency; ?>
                <select name="Contract[collection_type]" class="form-control" id="collection_type">
                    <option value="2">Change Collection Frequency</option>
                    <option value="0">Weekly</option>
                    <option value="1">Monthly</option>
                </select>
            </div>
        </div>
        <div class="row form-group">
            <label class="col-sm-4 control-label"><b>Timesheet Preference:</b></label>
            <div class="col-sm-4">
                <?php echo $model->timesheetTypeSel ? ucfirst($model->timesheetTypeSel) : '-'; ?>
            </div>
            <div class="col-sm-4"> 
                <div class="row">
                    <div class="col-lg-6" id="collection_period" style="display: none;">
                        <input type="radio" name="Settings[collection_day_first]" value="1">First
                        <input type="radio" name="Settings[collection_day_first]" value="0">Last
                    </div>

                    <div class="col-lg-6"  id="collection_day" style="display: none;">
                        <select  name="Contract[collection_day]" class="form-control">
                            <option value="">Select Day</option>
                            <option value="sunday">Sunday</option>
                            <option value="monday">Monday</option>
                            <option value="tuesday">Tuesday</option>
                            <option value="wednesday">Wednesday</option>
                            <option value="thursday">Thursday</option>
                            <option value="friday">Friday</option>
                            <option value="saturday">Saturday</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if ($model->next_cf_date) { ?>
        <div class="row form-group">
            <div class="col-sm-12">
                <p class="text-danger">
                    Note: You have already changed collection frequency to <b><?php echo $model->nexttimesheetfrequency; ?></b> which will take effect from <?php echo date(AppInterface::getdateformat(), $cf_change_time); ?>.
                </p>
            </div>
        </div>
    <?php } else { ?>
        <div class="row form-group">
            <div class="col-sm-12">
                <p class="text-danger">
                    Note: If you change the timesheet collection frequency, it will take effect from <?php echo date(AppInterface::getdateformat(), $cf_change_time); ?> date.
                </p>
            </div>
        </div>
    <?php } ?>
    <?php if ($model->type != 'in_out') { ?>
        <div class="row form-group">
            <?php if ($payrates = AppContract::getContractPayrates($model->id)) { ?>
                <div class="col-lg-6">
                    <h4><b>Payrate Setting</b></h4>
                    <?php foreach ($payrates as $payrate) { ?>
                        <div class="row form-group">
                            <div class="col-md-8" style="padding-left:30px;">
                                <label class="control-label"><b><?php echo $payrate->key; ?>:</b></label>
                                <input type="text" name="Payrate[<?php echo $payrate->key; ?>]" class="form-control ui-state-valid" value="<?php echo $payrate->value; ?>">
                            </div>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>

            <?php if ($items = AppContract::getContractItems($model->id)) { ?>
                <div class="col-lg-6">
                    <h4><b>Item Setting</b></h4>
                    <?php foreach ($items as $item) { ?>
                        <div class="row form-group">
                            <div class="col-md-8" style="padding-left:30px;">
                                <label class="control-label"><b><?php echo $item->key; ?>:</b></label>
                                <input type="text" name="Item[<?php echo $item->key; ?>]" class="form-control ui-state-valid" value="<?php echo $item->value; ?>">
                            </div>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    <?php } else { ?>
        <div class="row form-group">
                     
            <?php if ($items = AppContract::getContractPayrates($model->id)) { ?>
                <div class="col-lg-6">
                    <h4>Item Setting</h4>
                    <?php 
                    $items2 = AppContract::getContractPayrates($model->contracts[0]->id);
                    foreach ($items as $key=>$item) { ?>
                        <div class="row form-group">
                            <div class="form-group">
                                <label class="control-label"><?php echo $item->key; ?>:</label>
                                <div class="row">
                                    <div class="col-md-8">
                                        <label>Cost</label>
                                        <input type="text" name="Payrate1[<?php echo $item->key; ?>]" class="form-control ui-state-valid" value="<?php echo $item->value; ?>">
                                    </div>
                                     <div class="col-md-8">
                                        <label>Bill</label>
                                        <input type="text" name="Payrate2[<?php echo $items2[$key]->key; ?>]" class="form-control ui-state-valid" value="<?php echo $items2[$key]->value; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>

            <?php if ($items = AppContract::getContractItems($model->id)) { ?>
                <div class="col-lg-6">
                    <h4><b>Item Setting</b></h4>
                    <?php 
                    $items2 = AppContract::getContractItems($model->contracts[0]->id);
                    foreach ($items as $key=>$item) { ?>
                        <div class="row form-group">
                            <div class="col-sm-12">
                                <label class="control-label"><?php echo $item->key; ?>:</label>
                                <div class="row">
                                    <div class="col-md-8">
                                        <label>Cost</label>
                                        <input type="text" name="Item1[<?php echo $item->key; ?>]" class="form-control ui-state-valid" value="<?php echo $item->value; ?>">
                                    </div>
                                     <div class="col-md-8">
                                        <label>Bill</label>
                                        <input type="text" name="Item2[<?php echo $items2[$key]->key; ?>]" class="form-control ui-state-valid" value="<?php echo $items2[$key]->value; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    <?php } ?>
    <div class="row form-group">
        <div class="col-lg-6" style="padding-left:30px;">
            <label class="control-label"><b>Policy:</b></label>
            <textarea rows="5" name="Contract[policy]" class="form-control ui-state-valid" ><?php echo $model->policy; ?></textarea>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-9">
    <input type="submit" class="btn btn-primary" value="Save Changes">
    <a href="<?php echo $this->createUrl('index'); ?>" class="btn btn-primary">Cancel</a>
                    </div>
                </div>
</div>
<?php $this->endWidget(); ?>
    </div>
</div>
<input type="hidden" id="date_format" value="<?php echo AppInterface::getdateformat(true); ?>"/>
