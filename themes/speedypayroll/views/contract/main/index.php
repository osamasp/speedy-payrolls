<?php
$this->breadcrumbs = array(
    'Users' => array('/user/employee'),
    $dataProvider ? $dataProvider[0]->user->full_name : '',
);

$this->menu = array(
    array('label' => 'Create User', 'url' => array('create')),
    array('label' => 'Manage User', 'url' => array('admin')),
);
$not_included_fields = array('id', 'company_id', 'parent_id', 'role', 'payroller_id', 'type', 'policy', 'collection_day', 'collection_type', 'status', 'invitation_code', 'created_at', 'created_by', 'modified_at', 'modified_by', 'timesheetTypeSel', 'owner_id', 'file_name', 'is_internal', 'next_cf_date', 'next_cf_day', 'next_cf_type');
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'myForm',
    'enableAjaxValidation' => false,
        ));
?>
<input type="hidden" id="select_msg" value="<?php echo ConstantMessages::$req_select; ?>">
<input type="hidden" id="export_url" value="<?php echo $this->createUrl('export'); ?>">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/company-companyshifts-approvallist.js" type="text/javascript"></script>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/dropdowns-enhancement.css" rel="stylesheet">

<div class="box-header">
    <h3 class="box-title"><?php $dataProvider ? $dataProvider[0]->user->full_name : 'USERS'; ?></h3>
    <div class="box-tools">
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box grey-cascade">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-eye"></i>Birds Eye View
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>

                </div>
            </div>
            <div class="portlet-body table-responsive">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-12">
                            <?php if (AppUser::isUserAdmin()) { ?>
                                <div class="btn-group pull-right"style="margin-top:12px; margin-right: 25px;">
                                    <a href="javascript:void(0);" class="btn green dropdown-toggle" id="openButton">Export <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">				
                                        <li><a id="export" href="javascript:void(0);" onclick="exportData('export')">Export Selected Record</a></li>
                                        <li class="divider"></li>

                                        <li><a href="javascript:void(0);" id="export_all" onclick="exportData('export_all')">Export All Record</a></li>
                                        <?php if (isset($dataProvider[0])) { ?>
                                            <li><input type="checkbox" id="All" value="1"> <label for="All">Select All</label></li>

                                            <?php
                                            foreach ($dataProvider[0] as $key => $item) {
                                                if (!in_array($key, $not_included_fields)) {
                                                    ?>
                                                    <li><input class="select" type="checkbox" name="export_fields[]" id="<?php echo $key; ?>" value="<?php echo $key; ?>"><label for="<?php echo $key; ?>"><?php echo UserTimings::model()->getAttributeLabel($key); ?></label></li>
                                                <?php }
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
<?php } ?>
                        </div>
                    </div>
                </div>
                <table id="example1" class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th><input type="checkbox" data-checkbox="true" class="group-checkable" id="checkAll" onchange="" data-set="#sample_1 .checkboxes"/></th>
                            <th>Employee name</th>
                            <th>Employment Type</th>
                            <th>Date Joined</th>
                            <th>Date Ending</th>
                            <th>Approver</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                        <?php foreach ($dataProvider as $item) { ?>
                            <?php
                            /*
                             * Jugarh start
                             * this jugarh hides out_b contract from admin of agency
                             */
                            if (AppContract::can_admin_view_out_b($item)) {
                                /*
                                 * jugarh end
                                 */
                                ?>
                                    <?php if ($item->is_internal && $item->company_id == AppUser::getUserCompany()->id) { ?>
                                    <tr style="background-color: #D7FAD7;">
                                    <?php } else if ($item->type == 'out' && $item->company_id == AppUser::getUserCompany()->id) { ?>
                                    <tr style="background-color:#FAD7FA;">
                                    <?php } else if ($item->type == 'out') { ?>
                                    <tr style="background-color:#D7E9FA;">                                 
                                    <?php } else if ($item->type == 'out_b' || $item->type == 'in_out') { ?>
                                    <tr style="background-color:#FACDCD;">                                 
        <?php } ?>
                                    <td><input type="checkbox" name="entry[]" class="check" value="<?php echo $item->id; ?>"/></td>    
                                    <td><?php echo $item->user->full_name; ?></td>
                                    <td><?php echo ucwords(str_replace('_', ' ', AppUser::isUserAdmin() ? $item->CustomEmploymentType : $item->CustomName)); ?></td>
                                    <td><?php echo date(AppInterface::getdateformat(), $item->start_time); ?></td>
                                    <td><?php echo $item->end_time <= 0 ? '-' : date(AppInterface::getdateformat(), $item->end_time); ?></td>
                                    <td><?php echo $item->approver ? $item->approver->full_name : '-'; ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <button class="btn green dropdown-toggle" data-toggle="dropdown">Actions <i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul class="dropdown-menu pull-right" role="menu">
                                                <li><a href="<?php echo $this->createUrl('/contract/main/view', array('id' => $item->id)); ?>">View Contract</a></li>
                                                <?php if (AppUser::isUserAdmin() && $item->owner_id == AppUser::getUserId()) { ?>
                                                    <li><a href="<?php echo $this->createUrl('update', array('id' => $item->id)) . "?Staff_Management"; ?>">Update</a></li>
                                                <?php } ?>

                                                <?php if ($item->type == 'in_out' && ($item->company_id == AppUser::getUserCompany()->id || $item->parent->company_id == AppUser::getUserCompany()->id)) { ?>
                                                    <li><a href="<?php echo $this->createUrl('/timesheet/main/index', array('contract' => (isset($item->contracts[0])) ? $item->contracts[0]->id : $item->id)); ?>">View timesheets</a></li>
                                                <?php } else { ?>
                                                    <li><a href="<?php echo $this->createUrl('/timesheet/main/index', array('contract' => $item->id)); ?>">View timesheets</a></li>
        <?php } ?>
                                            </ul>
                                        </div>

                                    </td>
                                </tr>
                            <?php } ?>
<?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>