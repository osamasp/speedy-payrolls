<?php
$this->breadcrumbs = array(
    'Employee',
);
?>
<script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.2/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/contract-main-contractinout.js" type="text/javascript"></script>
<input type="hidden" id="company_exist" value="<?php echo Yii::app()->createUrl('/user/main/checkcompanyexist'); ?>"/>
<input type="hidden" id="user_exist" value="<?php echo Yii::app()->createUrl('/user/main/checkuserexist'); ?>"/>
<input type="hidden" id="ajax_loader" value="<?php echo "<img src='" . Yii::app()->theme->baseUrl . "/img/ajax-loader.gif' />"; ?>"/>
<input type="hidden" id="company_details" value="<?php echo Yii::app()->createUrl('/company/main/getcompanydetails'); ?>"/>
<input type="hidden" id="get_employee" value="<?php echo Yii::app()->createUrl('/company/main/getEmployee'); ?>"/>
<input type="hidden" id="exist_in_company" value="<?php echo Yii::app()->createUrl('/user/main/checkuserexistincompany'); ?>"/>
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue" id="form_wizard_1">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-plus"></i> Setup Three-way Timesheet Account - <span class="step-title">
                        Step 2 of 4 </span>
                </div>
            </div>
            <div class="portlet-body form">
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'user-form',
                    'enableAjaxValidation' => false,
                    'htmlOptions' => array('enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'id' => 'submit-form'),
                ));
                ?>
                <div class="form-wizard">
                    <div class="form-body">
                        <ul class="nav nav-pills nav-justified steps">
                            <li class="done">
                                <a href="#tab1" data-toggle="tab" class="step">
                                    <span class="number">
                                        1 </span>
                                    <span class="desc">
                                        <i class="fa fa-check"></i>Deployment</span>
                                </a>
                            </li>
                            <li class="active" id="menuTab2" onclick="show();">
                                <a href="#tab2"class="step">
                                    <span class="number">
                                        2 </span>
                                    <span class="desc">
                                        <i class="fa fa-check"></i>Contractor Details</span>
                                </a>
                            </li>
                            <li id="menuTab3" onclick="show();">
                                <a href="#tab3" class="step active">
                                    <span class="number">
                                        3 </span>
                                    <span class="desc">
                                        <i class="fa fa-check"></i>Client Details</span>
                                </a>
                            </li>
                            <li class="menuTab4">
                                <a href="#tab4" data-toggle="tab" class="step">
                                    <span class="number">
                                        4 </span>
                                    <span class="desc">
                                        <i class="fa fa-check"></i>Confirm</span>
                                </a>
                            </li>
                        </ul>
                        <div id="bar" class="progress progress-striped" role="progressbar">
                            <div class="progress-bar progress-bar-success" id="progress" style="width:50%">
                            </div>
                        </div>
                        <div class="tab-content">
                            <div id="errorDiv" class="alert alert-danger display-none">
                                <button class="close" data-dismiss="alert"></button>
                                <span>You have some form errors. Please check below.</span>
                            </div>
                            <div id="successDiv" class="alert alert-success display-none">
                                <button class="close" data-dismiss="alert"></button>
                                <span>Your form validation is successful!</span>
                            </div>
                            <!-- tab2 -->

                            <div class="tab-pane" id="tab2">

                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 class="block">Enter contractor email; if company is not found on the system complete the form which will show below.</h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-8">
                                                <div class="col-md-4">
                                                <label class="control-label col-md-6">Contractor Company Email:</label>
                                                </div>
                                                <div class="input-group" style="text-align:left">
                                                    <input type="text" name="Contract[admin_email]" id="Contract_admin_email" class="form-control find-ccompany">
                                                    <span class="input-group-btn">
                                                        <input type="button" id='cc-search1' value="search" class="btn green">
                                                    </span>
                                                </div>
                                                <input type="hidden" name="Contract1[company_id]" value="0" id="companyId2" class="form-control">
                                                <div id="cdetails" class="col-sm-12">
                                                </div>
                                                <div class="contractor-contrainer">

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-6">

                                        <div class="alert alert-block alert-info fade in">
                                            <h4 class="alert-heading">Timesheet Collection</h4>
                                            <p> Please select a day for timesheet collection. All hours submitted in the last seven days will
                                                be accumulated and sent to the timesheet approver.</p>
<!--                                            <p>
                                                <a class="btn purple" id="coll_freq" href="javascript:setday('monday');"> MON </a>
                                                <a class="btn purple" href="javascript:setday('tuesday');"> TUE </a>
                                                <a class="btn purple" href="javascript:setday('wednesday');"> WED </a>
                                                <a class="btn purple" href="javascript:setday('thursday');"> THU </a>
                                                <a class="btn purple" href="javascript:setday('friday');"> FRI</a>
                                                <a class="btn purple" href="javascript:setday('saturday');"> SAT</a>
                                                <a class="btn purple" href="javascript:setday('sunday');"> SUN</a>
                                            </p>
                                            <input type="hidden" name="Contract2[collection_day]" value="sunday" id="day">-->
                                            <select name="Contract[collection_type]" class="form-control" id="collection_type" >
                                                <option value="0">Weekly</option>
                                                <option value="1">Monthly</option>
                                            </select>
                                            <div id="collection_period" style="display: none;">
                                                <input  type="radio" name="Settings[collection_day_first]"  value="1">First
                                                <input  type="radio" name="Settings[collection_day_first]"  value="0">Last
                                            </div>
                                            <select name="Contract2[collection_day]" class="form-control select2" style="margin-top:10px;">
                                                <option value="monday">MONDAY</option>
                                                <option value="tuesday">TUESDAY</option>
                                                <option value="wednesday">WEDNESDAY</option>
                                                <option value="thursday">THURSDAY</option>
                                                <option value="friday">FRIDAY</option>
                                                <option value="saturday">SATURDAY</option>
                                                <option value="sunday" selected>SUNDAY</option>
                                            </select>
                                            
                                        </div>

                                    </div>
                                    <div class="col-sm-6">
                                        Additional Company Policies <textarea class="form-control" rows="3" id="policy" name="Contract[policy]"></textarea>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Set Payrates</label>
                                            <div class="col-md-9">
                                                <a class="btn btn-circle btn-sm btn-default" data-toggle="modal" href="#responsive2">Apply Rates</a>
                                            </div>
                                        </div>
                                    </div>	

                                    <!-- set contractor rates form -->
                                    <!-- /.modal custom format builder-->
                                    <div id="responsive2" class="modal fade" tabindex="-1" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Set Pay Rates</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="scroller" style="height:350px" data-always-visible="1" data-rail-visible1="1">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="col-md-6" id='payratesanditems'>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" data-dismiss="modal" onclick="checkPayrates()" class="btn green">Apply Rates</button>
                                                    <button type="button" data-dismiss="modal" class="btn default">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- /modal custom format builder-->
                                    <!-- /set contractor rates form -->





                                </div>
                                <!--/row-->


                            </div>

                            <!-- /tab2 -->





                            <!-- tab 3 -->


                            <div class="tab-pane" id="tab3">
                                <p> Conditional to selection</p>
                                <!-- row -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 class="block">Enter client email; if company is not found on the system complete the form which will show below.</h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">

                                        <div class="form-group">
                                            <label class="control-label col-md-6" style="width:36%;">Client Company Email:</label>
                                            <div class="col-md-6">
                                                <div class="input-group" style="text-align:left">
                                                    <input type="text" id="Contract2" name="Contract2[admin_email]" placeholder="Admin Email Address" class="form-control find-ccompany2">
                                                    <span class="input-group-btn">
                                                        <input type="button" id='cc-search2' value="search" class="btn green">
                                                    </span>
                                                </div>
                                                <input type="hidden" name="Contract2[company_id]" value="0" id="companyId" class="form-control">
                                                <div id="cdetails2" class="col-sm-12">

                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="col-md-6">
                                        Upload Contract <?php echo $form->fileField($contract, 'file_name'); ?>
                                    </div>
                                </div>
                                <!--/row-->

                                <!-- row -->
                                <div class="row">
                                    <!--/span-->
                                    <div class="col-md-6">

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="control-label col-md-4">Timesheet approver</label>
                                                <div class="md-radio-inline">
                                                    <div class="md-radio col-md-3">
                                                        <input type="radio" id="timesheet_approver" onchange="run()" value="in" name="timesheet_approver" class="md-radiobtn" checked>
                                                        <label for="timesheet_approver">
                                                            <span></span>
                                                            <span class="check"></span>
                                                            <span class="box"></span>
                                                            Inside your company </label>
                                                    </div>
                                                    <div class="md-radio col-md-3">
                                                        <input type="radio" id="timesheet_approver1" onchange="run()" value="out" name="timesheet_approver" class="md-radiobtn">
                                                        <label for="timesheet_approver1">
                                                            <span></span>
                                                            <span class="check"></span>
                                                            <span class="box"></span>
                                                            Client Company </label>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <!--/span-->
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Select Approver</label>
                                            <div class="col-md-9" id="inside" style="display: block;">
                                                <select  name="Contract[approver_id]" id="approver"  class="form-control" required="required">
                                                    <option value="0">Select Approver</option>
                                                    <?php foreach ($approvers as $approver) { ?>
                                                        <option value="<?php echo $approver->id; ?>"><?php echo $approver->full_name; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="col-md-9" id="outside" style="display: none" >
                                                Approver Email <div class="row">
                                                    <div class="col-sm-8"> 
                                                        <input type="text" name="approver_email" id='approver_email' class="form-control" placeholder="Email">
                                                        <input type="hidden" name="approver_id" id='approver_id' value="0">
                                                    </div> 
                                                    <div class="col-sm-4"><input type="button" id='approver-select' value="search" class="btn btn-primary"></div>

                                                </div>
                                                <div id="approver-email-message"></div>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <!--/span-->
                                </div>
                                <!--/row-->




                            </div>


                            <!-- /tab3 -->
                            <div class="tab-pane" id="tab4">
                                <!--/row-->
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-12" style="text-align:left;"> 
                                                <div class="col-md-12">
                                                    <p> You agree to provide access to the third parties involved in this contract to view and manage the mutually shared timesheets.</p>
                                                    <input type="checkbox" onchange="enable()" name="tnc" id="chBox" checked /> I agree to the <a class="" data-toggle="modal" href="#terms">Terms of Service</a>
                                                </div>
                                            </label>
                                            <div id="register_tnc_error">
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->

                            </div>
                        </div>
                    </div>
                    <div class="form-actions right">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">

                                <a id="continue" href="javascript:show();" class="btn btn-circle btn-sm green-haze">
                                    Continue <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                                <button id="submit" type="submit" onclick="submitForm()" class='btn btn-circle btn-sm green-haze' style="display:none;">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
</div>
</div>
<div id="overlay" style="position:absolute;top:50%;left:30%;display: none;"><div id='PleaseWait' style=''><img src="<?php echo Yii::app()->theme->baseUrl . '/img/ajax-loader.gif'; ?>" style="width:200px;"/></div></div>
<!-- END CONTENT -->