<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/contract-main-contract.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.js"></script>

<!--  <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet">
  <script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>-->
  
  <!--<link href="runnable.css" rel="stylesheet" />-->
  <!-- Load jQuery and the validate plugin -->
  <!--<script src="//code.jquery.com/jquery-1.9.1.js"></script>-->
  <script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>


<input type="hidden" id="company_exist" value="<?php echo Yii::app()->createUrl('/user/main/checkcompanyexist'); ?>"/>
<input type="hidden" id="user_exist" value="<?php echo Yii::app()->createUrl('/user/main/checkuserexist'); ?>"/>
<input type="hidden" id="ajax_loader" value="<?php echo "<img src='" . Yii::app()->theme->baseUrl . "/img/ajax-loader.gif' />"; ?>"/>
<input type="hidden" id="company_details" value="<?php echo Yii::app()->createUrl('/company/main/getcompanydetails'); ?>"/>
<input type="hidden" id="get_employee" value="<?php echo Yii::app()->createUrl('/company/main/getEmployee'); ?>"/>
<input type="hidden" id="exist_in_company" value="<?php echo Yii::app()->createUrl('/user/main/checkuserexistincompany'); ?>"/>
<input type='hidden' id="baseUrl" value="<?php echo Yii::app()->baseUrl; ?>">
<input type="hidden" id="return_url" value="<?php echo Yii::app()->createUrl('/contract/main/index'); ?>">
<div class="row">
    <div class="col-md-6 ">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-users"></i> STAFF/CONTRACTOR DEPLOYMENT
                </div>
                <div class="tools">

                </div>
            </div>
            <div class="portlet-body form">
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'contract-form',
                    'enableAjaxValidation' => false,
                    'htmlOptions' => array('enctype' => 'multipart/form-data', 'class' => 'form-horizontal cmxform', 'novalidate' => 'true'),
                ));
                ?>
                <div class="form-body">
                    <div class="form-group">
                        <div class="col-md-6 caption font-red-sunglo">
                            <i class="fa fa-sitemap font-red-sunglo"></i>
                            <span class="caption-subject bold uppercase"> DEPLOYMENT TYPE</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="radio-list">
                                <?php if($two_way && $two_way_in){ ?>
                                <label>
                                    <input type="radio" name="Contract[type]" id="optionsRadios1" value="in" checked> Invite Outside Contractor or Staff</label>
                                <?php }
                                if($two_way && $two_way_out){ ?>
                                <label>
                                    <input type="radio" name="Contract[type]" id="optionsRadios2" value="out"> Send Staff to Work Outside</label>
                                <?php }
                                if($three_way){ ?>
                                <label>
                                    <input type="radio" name="Contract[type]" id="optionsRadios3" value="in_out"> Pick Outside Contractor or Staff to Work for a Third Party (Agency Role)</label>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <hr>
                    
                    <div class="form-group">
                        <div class="col-md-6 caption font-red-sunglo">
                            <i class="icon-user font-red-sunglo"></i>
                            <span class="caption-subject bold uppercase"> SELECT STAFF</span>
                        </div>

                    </div>
                    <!--                        <div class="form-group">
                                                <label class="col-md-3 control-label">Contractor or Staff Email Address</label>
                                                <div class="col-md-9">
                                                    <div class="input-icon right">
                                                        <i class="fa fa-envelope autoLeft"></i>
                                                        <input type="text" class="form-control" placeholder="Email Address">
                                                    </div>
                                                </div>
                                            </div>-->
                    <div id="two">
                        <div class="col-md-9">
                            <div id="contractor" style='display:none;'>
                                <div class="form-group">
                                    
                                    
                                    <label for="contractors" class="col-md-3 control-label">Contractor Company Email Address</label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa fa-envelope autoLeft" id="contractors_env"></i>
                                            <input type="text" id="contractors" name="Contract[company_email]" class="form-control" placeholder="Email Address">
                                            <label id="contract_email_label"></label>
                                            </br>
                                            <a href="" id ="contractors_show_link" style="display: none;" > Change Contractor Company Email Address </a>
                                    
                                        </div>
                                    </div>
                                   
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4 errorClass">Select Employee</label>
                                    <div class="col-md-8">
                                        <select id="employee" value="0" name="Contract[user_id]" class="form-control chosen-select" >
                                            <option value="">Select Employee</option>
                                            <?php foreach ($employees as $employee) { ?>
                                                <option value="<?php echo $employee->id; ?>"><?php echo $employee->full_name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="Contract[company_id]" id="companyId" class="form-control">
                            <div class="form-group" id='client'>
                                <label class="col-md-3 control-label ">Client Company Email Address</label>
                                <div class="col-md-9">
                                    <div class="input-icon right">
                                        <i class="fa fa-envelope autoLeft" id="client_env"></i>
                                        <input type="text" id="clients" name="Contract[admin_email]" class="form-control find-ccompany" placeholder="Email Address" >
                                        <label id="clients11"></label>
                                        <input type="text" id="Contract2" name="Contract2[admin_email]" placeholder="Admin Email Address" style="display:none;" class="form-control find-ccompany2" >
                                        </br>
                                        <a href="" id ="clients_show_link" style="display: none;" > Change Client Company Email Address </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <span class="input-group-btn text-right">
                                <a href="javascript:void(0)" id="cc-search1" class="btn green">Search</a>
                            </span>
                        </div>
                        <div class="form-group" id="company">
                            <div id="cdetails" class="col-sm-12">

                            </div>
                        </div>
                    </div>
                    <div id="three" style="display:none;">
                        <div class="form-group">
                            <label class="col-md-3 control-label errorClass">Contractor Company Email Address</label>
                            <div class="col-md-6">
                                <div class="input-icon text-right">
                                    <i class="fa fa-envelope autoLeft" id="contractor_three_env"></i>
                                    <input type="text" id="contractor_three" name="Contract[company_email]" class="form-control" placeholder="Email Address">
                                    <label id="contractor_three_label"></label>
                                    <a href="" id ="contractor_show_link" style="display: none;" > Change Contractor Company Email Address </a>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <span class="input-group-btn text-right">
                                    <a href="javascript:void(0)" id="cc-three-way1" class="btn green">Search</a>
                                </span>
                            </div>
                        </div>

                        <div class="form-group" id="company">
                            <div id="cdetails_three" class="col-sm-12">

                            </div>
                        </div>
                        <input type="hidden" name="Contract1[company_id]" value="0" id="companyId2" class="form-control">
                        <div class="form-group" id='client'>
                            <label class="col-md-3 control-label">Client Company Email Address</label>
                            <div class="col-md-6">
                                <div class="input-icon text-right">
                                    <i class="fa fa-envelope autoLeft" id="client_three_inv"></i>
                                    <!--<input type="text" id="clients" name="Contract[admin_email]" class="form-control find-ccompany" placeholder="Email Address">-->
                                    <input type="text" id="client_three" name="Contract2[admin_email]" placeholder="Admin Email Address" class="form-control find-ccompany2">
                                    <label id="client_three_label"></label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <span class="input-group-btn text-right">
                                    <a href="javascript:void(0)" id="cc-three-way2" class="btn green">Search</a>
                                </span>
                            </div>
                        </div>
                        <div class="form-group" id="company">
                            <div id="cdetails_three2" class="col-sm-12">

                            </div>
                        </div>
                        <input type="hidden" name="Contract2[company_id]" value="0" id="companyId" class="form-control">
                    </div>
                    <hr>
                    <div class="form-group" id="payratesanditems">

                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile" class="col-md-3 control-label">Upload Contract</label>
                        <div class="col-md-9">
                            <input type="file" name="Contract[file_name]" id="exampleInputFile">
                            <p class="help-block">
                                Accepted formats PDF, DOC, DOCX, JPG, PNG, GIF.
                            </p>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <div class="col-md-6 caption font-red-sunglo">
                            <i class="icon-user font-red-sunglo"></i>
                            <span class="caption-subject bold uppercase"> APPROVER</span>
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Approver</label>
                        <div class="col-md-9">
                            <div class="radio-list">
                                <label class="radio-inline">
                                    <input type="radio" onchange="run()" value="in" id="timesheet_approver" name="timesheet_approver" checked> Inside my company </label>
                                <label class="radio-inline">
                                    <input type="radio" onchange="run()" value="out" id="timesheet_approver1" name="timesheet_approver"> Outside my company </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Select Approver</label>
                        <div class="col-md-9"  id="inside" style="display: block;">
                            <select  name="Contract[approver_id]" id="approver" style="width: 260px;" class="form-control">
                                <option value="">Select Approver</option>
                                <?php foreach ($approvers as $approver) { ?>
                                <option value="<?php echo $approver->id; ?>"><?php echo $approver->full_name; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-sm-4" id="outside" style="display: none" >
                            Approver Name <input type="text" style="width: 260px;" id="approver_name" name="approver_name" class="form-control" placeholder="Name">
                            <br>
                            Approver Email <input type="text" style="width: 260px;" name="approver_email" id='approver_email' class="form-control" placeholder="Email">
                            <input type="hidden" value="0" id="approver_valid"/>
                            <div id='approver-email-message'></div>
                            <br>
                            Approver Phone <input type="text" style="width: 260px;" id="approver_phone" name="approver_phone" class="form-control" placeholder="Phone">

                        </div>
                    </div>

                    <hr>
                    <div class="form-group">
                        <div class='cc-remove-me'>
                            <div class="contractor-contrainer" >
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"> Additional Company Policy</label>
                        <div class="col-md-9">
                            <textarea cols="40" name='Contract[policy]' class="right"></textarea>
                        </div>
                    </div>
                    <hr>

                    <div class="form-group">    
                        <div class="col-md-12">
                            <div class="alert alert-block alert-info fade in">
                                <h4 class="alert-heading">Timesheet Collection</h4>
                                <p> Please select a day for timesheet collection. All hours submitted in the last seven days will
                                    be accumulated and sent to the timesheet approver.</p>
                                <div id="collection_two">
                                    <select name="Contract[collection_type]" class="form-control" id="collection_type" >
                                        <option value="0">Weekly</option>
                                        <option value="1">Monthly</option>
                                    </select>
                                    <div id="collection_period" style="display: none;">
                                        <input  type="radio" name="Settings[collection_day_first]"  value="1">First
                                        <input  type="radio" name="Settings[collection_day_first]"  value="0">Last
                                    </div>
                                    <select name="Contract[collection_day]" class="form-control select2 margin-top-10" style="margin-top:10px;">
                                        <option value="monday">MONDAY</option>
                                        <option value="tuesday">TUESDAY</option>
                                        <option value="wednesday">WEDNESDAY</option>
                                        <option value="thursday">THURSDAY</option>
                                        <option value="friday">FRIDAY</option>
                                        <option value="saturday">SATURDAY</option>
                                        <option value="sunday" selected>SUNDAY</option>
                                    </select>
                                </div>
                                <div id="collection_three" style="display:none;">
                                    <select name="Contract2[collection_type]" class="form-control" id="collection_type" >
                                        <option value="0">Weekly</option>
                                    </select>
                                    <div id="collection_period" style="display: none;">
                                        <input class="form-control" type="radio" name="Settings[collection_day_first]"  style="display: block;" value="1">First
                                        <input class="form-control" type="radio" name="Settings[collection_day_first]" style="display: block;" value="0">Last
                                    </div>
                                    <select name="Contract2[collection_day]" class="form-control select2 margin-top-10" style="margin-top:10px;">
                                        <option value="monday">MONDAY</option>
                                        <option value="tuesday">TUESDAY</option>
                                        <option value="wednesday">WEDNESDAY</option>
                                        <option value="thursday">THURSDAY</option>
                                        <option value="friday">FRIDAY</option>
                                        <option value="saturday">SATURDAY</option>
                                        <option value="sunday" selected>SUNDAY</option>
                                    </select>

                                </div>
                            </div>
                        </div>



                    </div>	
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <p>
                                <!--<input class="submit" type="submit" value="Submit">-->
                            </p>
                            <input type="hidden" id="token" name="token" value="<?php echo $token;  ?>">
                            <input type="submit" name="submit" value="Deploy" class="btn btn-primary">
                            <!--<button  onclick="addContract('<?php // echo $token;  ?>')" type="button" class="btn green">Deploy</button>-->
                            <button type="button" class="btn default">Cancel</button>
                        </div>
                    </div>
                </div>
                <?php $this->endWidget(); ?>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->


    </div>
    <div class="col-md-6 ">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-life-ring"></i> HELP NOTES
                </div>
                <div class="tools">

                </div>
            </div>
            <div class="portlet-body form">
                <form class="form-horizontal" role="form">
                    <div class="form-body">
                        <div class="form-group">
                            <div class="col-md-12 caption font-blue">
                                <i class="fa fa-cogs font-blue"></i>
                                <span class="caption-subject bold uppercase">Invite Outside Contractor or Staff</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="radio-list">
                                    <p>Use this option when your company is hiring contractors to work for you. You can upload a contract and set agreed pay rates for the contractors. Timesheets generated by the system for the contract created can be viewed and approved from your panel. Using the Birdseye view link located on the left side bar you can also view deployment status of employees and contractors.
                                    </p>
                                    <ul class="list-unstyled margin-top-10 margin-bottom-10">
                                        <li><i class="fa fa-check icon-info"></i>Colour for Outside Contractor or Staff under Birdseye View - XX</li>
                                        <li><i class="fa fa-check icon-info"></i>Timesheet formats can be requested to suit your requirements. Please contact our support team for assistance.</li>
                                    </ul>						
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <div class="col-md-12 caption font-blue">
                                <i class="fa fa-cogs font-blue"></i>
                                <span class="caption-subject bold uppercase">Send Staff to Work Outside</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="radio-list">
                                    <p>This option allows you to send your employees to your clients. You can track and view completed timesheets. Your company also has the option to set pay rates and send invoices created via the hours accumulated on the timesheet. Birdseye view will show you status of your employees who have been dispatched to a client.
                                    </p>
                                    <ul class="list-unstyled margin-top-10 margin-bottom-10">
                                        <li><i class="fa fa-check icon-info"></i>Colour for Staff Working Outside Under Birdseye View - XX</li>
                                        <li><i class="fa fa-check icon-info"></i>Timesheet formats can be requested to suit your requirements. Please contact our support team for assistance.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <div class="col-md-12 caption font-blue">
                                <i class="fa fa-cogs font-blue"></i>
                                <span class="caption-subject bold uppercase">Pick Outside Contractor or Staff to Work for a Third Party (Agency Role)</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="radio-list">
                                    <p>This option allows you to deploy a contractor to a client company while allowing you the ability to view the timesheet of the contractor on contracts and tasks that have been set by your company. The admin can also notify through our internal notifications system for any changes to the assigned tasks.
                                    </p>
                                    <ul class="list-unstyled margin-top-10 margin-bottom-10">
                                        <li><i class="fa fa-check icon-info"></i>Pay rates can be set on two levels; the rate of the contractor as well as the chargeable rate of the client.</li>
                                        <li><i class="fa fa-check icon-info"></i>Colour for Staff Working Outside Under Birdseye View – XX</li>
                                        <li><i class="fa fa-check icon-info"></i>Timesheet formats can be requested to suit your requirements. Please contact our support team for assistance.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <hr>
                    </div>
                </form>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->


    </div>
</div>
<div id="overlay" style=" position:fixed;
     top:0px;
     right:0px;
     width:100%;
     height:100%;
     background-color:#666;
     /*background-image:url('ajax-loader.gif');*/
     background-repeat:no-repeat;
     background-position:center;
     z-index:10000000;
     opacity: 0.4;
     filter: alpha(opacity=40);
     display:none;">
    <div id='PleaseWait' style='padding-left:40%;padding-top:20%;'>
        <img src="<?php echo Yii::app()->theme->baseUrl . '/img/ajax-loader.gif'; ?>" style="width:200px;"/>
    </div>
    </div>
<style>
    .error {
    color:red;
}
.valid {
    color:green;
}
</style>
</div>
