<?php

$this->breadcrumbs = array(
    'Employee',
);
?>
<script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.2/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/contract-main-contractin.js" type="text/javascript"></script>
<input type="hidden" id="company_exist" value="<?php echo Yii::app()->createUrl('/user/main/checkcompanyexist'); ?>"/>
<input type="hidden" id="user_exist" value="<?php echo Yii::app()->createUrl('/user/main/checkuserexist'); ?>"/>
<input type="hidden" id="ajax_loader" value="<?php echo "<img src='" . Yii::app()->theme->baseUrl . "/img/ajax-loader.gif' />"; ?>"/>
<input type="hidden" id="company_details" value="<?php echo Yii::app()->createUrl('/company/main/getcompanydetails'); ?>"/>
<input type="hidden" id="get_employee" value="<?php echo Yii::app()->createUrl('/company/main/getEmployee'); ?>"/>
<input type="hidden" id="exist_in_company" value="<?php echo Yii::app()->createUrl('/user/main/checkuserexistincompany'); ?>"/>


<div class="portlet box blue" id="form_wizard_1">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i> Setup Two-way Timesheet Account - <span class="step-title">
                Step 2 of 4 </span>
        </div>
    </div>
    <div class="portlet-body form">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'user-form',
            'enableAjaxValidation' => false,
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
        ?>
        <div class="form-wizard">
            <div class="form-body">
                <ul class="nav nav-pills nav-justified steps">
                    <li class="done">
                        <a href="#tab1" data-toggle="tab" class="step">
                            <span class="number">
                                1 </span>
                            <span class="desc">
                                <i class="fa fa-check"></i> Deployment </span>
                        </a>
                    </li>
                    <li class="active" id="menuTab2" onclick="show();">
                        <a href="#tab2" class="step">
                            <span class="number">
                                2 </span>
                            <span class="desc">
                                <i class="fa fa-check"></i> Your Details </span>
                        </a>
                    </li>
                    <li id="menuTab3" onclick="show();">
                        <a href="#tab3" class="step active">
                            <span class="number">
                                3 </span>
                            <span class="desc">
                                <i class="fa fa-check"></i> Client Details</span>
                        </a>
                    </li>
                    <li id="menuTab4">
                        <a href="#tab4" data-toggle="tab" class="step">
                            <span class="number">
                                4 </span>
                            <span class="desc">
                                <i class="fa fa-check"></i> Confirm </span>
                        </a>
                    </li>
                </ul>
                <div id="bar" class="progress progress-striped" role="progressbar">
                    <div id="progress" class="progress-bar progress-bar-success" style="width:50%">
                    </div>
                </div>
                <div class="tab-content">
                    <div id="errorDiv" class="alert alert-danger display-none">
                        <button class="close" data-dismiss="alert"></button>
                        <span>You have some form errors. Please check below.</span>
                    </div>
                    <div id="successDiv" class="alert alert-success display-none">
                        <button class="close" data-dismiss="alert"></button>
                        <span>Your form validation is successful!</span>
                    </div>

                    <div class="tab-pane" id="tab2">
                        <!-- row -->
                        <div class="row">
                            <div class="col-md-12">
                                <h5 class="block">Enter contractor email; if company is not found on the system complete the form which will show below.</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-6">Contractor Company Email</label>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                        <input type="text" id="email" name="Contract[admin_email]" class="form-control find-ccompany">
                                        <span class="input-group-btn">
                                            <input type="button" id='cc-search1' value="search" class="btn green">
                                        </span>
                                        <input type="hidden" name="Contract[company_id]" id="companyId" class="form-control">
                                        </div>
                                        <div id="cdetails" class="col-sm-12">

                                        </div>
                                        <div class='cc-remove-me'>
                                            
                                            <div class="contractor-contrainer" >
                                                
                                            </div>
                                        </div>
                                        </div>
                                </div>
                                
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Upload Contract</label>
                                        <div class="col-md-9">
                                            <?php echo $form->fileField($contract, 'file_name'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                        <!-- row --><hr>
                        <div class="row">

                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Set Payrates</label>
                                    <div class="col-md-9">
                                        <a class="btn btn-circle btn-sm green-haze" data-toggle="modal" href="#responsive">Apply Rates</a>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Additional Company Policy</label>
                                    <div class="col-md-9">
                                        <textarea class="form-control" rows="3" id="policy" name="Contract[policy]"></textarea>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->

                        <!-- /.modal custom format builder-->
                        <div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title">Set Pay Rates</h4>
                                    </div>

                                    <div class="modal-body">
                                        <div class="scroller" style="height:350px" data-always-visible="1" data-rail-visible1="1">
                                            <div class="row">
                                                <div class="col-md-12" id='payratesanditems'>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" data-dismiss="modal" onclick="checkPayrates()" class="btn btn-circle btn-sm green-haze">Apply Rates</button>
                                        <button type="button" data-dismiss="modal" class="btn btn-circle btn-sm btn-default">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- /modal custom format builder-->	

                    </div>
                    <!-- /tab2 -->

                    <!-- tab 3 -->
                    <div class="tab-pane" id="tab3">
                        <!-- row -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="alert alert-block alert-info fade in">
                                    <h4 class="alert-heading">Timesheet Collection</h4>
                                    <p> Please select a day for timesheet collection. All hours submitted in the last seven days will
                                        be accumulated and sent to the timesheet approver.</p>
<!--                                    <p>
                                        <a class="btn purple" id="coll_freq" href="javascript:setday('monday');"> MON </a>
                                        <a class="btn purple" href="javascript:setday('tuesday');"> TUE </a>
                                        <a class="btn purple" href="javascript:setday('wednesday');"> WED </a>
                                        <a class="btn purple" href="javascript:setday('thursday');"> THU </a>
                                        <a class="btn purple" href="javascript:setday('friday');"> FRI</a>
                                        <a class="btn purple" href="javascript:setday('saturday');"> SAT</a>
                                        <a class="btn purple" href="javascript:setday('sunday');"> SUN</a>
                                    </p>
                                    <input type="hidden" name="Contract[collection_day]" value="sunday" id="day">-->
                                    <select name="Contract[collection_type]" class="form-control" id="collection_type" >
                                        <option value="0">Weekly</option>
                                        <option value="1">Monthly</option>
                                    </select>
                                    <div id="collection_period" style="display: none;">
                                        <input  type="radio" name="Settings[collection_day_first]"  value="1">First
                                        <input  type="radio" name="Settings[collection_day_first]"  value="0">Last
                                    </div>
                                    <select name="Contract[collection_day]" class="form-control select2" style="margin-top:10px;">
                                            <option value="monday">MONDAY</option>
                                            <option value="tuesday">TUESDAY</option>
                                            <option value="wednesday">WEDNESDAY</option>
                                            <option value="thursday">THURSDAY</option>
                                            <option value="friday">FRIDAY</option>
                                            <option value="saturday">SATURDAY</option>
                                            <option value="sunday" selected>SUNDAY</option>
                                    </select>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <!--/span-->
                            <div class="col-md-6">

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label class="control-label col-md-4">Timesheet approver</label>
                                        <div class="md-radio-inline">
                                            <div class="md-radio col-md-3">

                                                <input type="radio" onchange="run()" value="in" id="timesheet_approver" name="timesheet_approver" class="md-radiobtn" checked>
                                                <label for="timesheet_approver">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>
                                                    Inside your company </label>
                                            </div>
                                            <div class="md-radio col-md-3">
                                                <input type="radio" onchange="run()" value="out" id="timesheet_approver1" name="timesheet_approver" class="md-radiobtn"  >
                                                <label for="timesheet_approver1">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>
                                                    Client Company </label>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!--/span-->
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Select Approver</label>
                                    <div class="col-md-9"  id="inside" style="display: block;">
                                        <select  name="Contract[approver_id]" id="approver" style="width: 260px;" class="form-control" required="required">
                                            <option value="0">Select Approver</option>
                                            <?php foreach ($approvers as $approver) { ?>
                                                <option value="<?php echo $approver->id; ?>"><?php echo $approver->full_name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-4" id="outside" style="display: none" >
                                    Approver Name <input type="text" style="width: 260px;" id="approver_name" name="approver_name" class="form-control" placeholder="Name">
                                    <br>
                                    Approver Email <input type="text" style="width: 260px;" name="approver_email" id='approver_email' class="form-control" placeholder="Email">
                                    <input type="hidden" value="0" id="approver_valid"/>
                                    <div id='approver-email-message'></div>
                                    <br>
                                    Approver Phone <input type="text" style="width: 260px;" id="approver_phone" name="approver_phone" class="form-control" placeholder="Phone">

                                </div>
                            </div>
                            <!--/span-->
                            <!--/span-->
                        </div>
                        <!--/row-->
                    </div>
                    <!-- /tab3 -->

                    <div class="tab-pane" id="tab4">
                        <!--/row-->
                        <div class="row">

                            <div class="form-group">
                                <label class="control-label col-md-5"> 
                                    <div class="col-md-12">
                                        <input type="checkbox" onchange="enable()" name="tnc" id="chBox" checked/> I agree to the <a class="" data-toggle="modal" href="#terms">Terms of Service</a>
                                    </div>
                                </label>
                                <div id="register_tnc_error">
                                </div>
                            </div>
                            <div class="col-md-6">

                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->

                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                        <a id="continue" href="javascript:show();" class="btn btn-circle green-haze" style="float: right;">
                            Continue <i class="m-icon-swapright m-icon-white"></i>
                        </a>
                        <!--<input id="submit" type="button" onclick="submitForm()" class='btn green button-submit' style="display:none;">-->
                        <button id="submit" type="submit" onclick="submitForm()" class='btn btn-circle green-haze' style="display:none; float: right;">Submit</button>
                    </div>
                </div>
            </div>

        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>
<div id="overlay" style="position:absolute;top:50%;left:30%;display: none;"><div id='PleaseWait' style=''><img src="<?php echo Yii::app()->theme->baseUrl . '/img/ajax-loader.gif'; ?>" style="width:200px;"/></div></div>
<!-- END CONTENT -->