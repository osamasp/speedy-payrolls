<?php
$this->breadcrumbs = array(
    'Employee',
);
?>
<h1>Contract Out B</h1>

<div class="col-sm-10 form-group">
    <div class="col-sm-4">
        Contract Type<br>
        <label>Inside Employee To Work Outside On Behalf Of An Agency</label>
    </div>
    <div class="col-sm-2">
    </div>
    <div class="col-sm-4">
        Company they will be working for
        <select name="Contract[company_id]" required=""  class="form-control chosen-select">
            <option value="">Select Company</option>
            <?php foreach ($companies as $company) { ?>
                <option value="<?php echo $company->id; ?>"><?php echo $company->name; ?></option>
            <?php } ?>
        </select>
    </div>

</div>
<div class="col-sm-10 form-group">
    <div class="col-sm-4">
        Select Employee<select name="employee_name" class="form-control" disabled="disabled">
            <option value="0">List Of Employee</option>
        </select ></div>
    <div class="col-sm-2">
    </div>
    <div class="col-sm-4">
        Upload Contract
        <?php echo $form->fileField($contract, 'file_name'); ?>
        <br>


        Regular Payrate
        <input type="text" name="regular_payrate" onkeypress="return isNumberKey(event);" class="form-control" placeholder="£/hr" required="required">    

    </div>

</div>
<div class="col-sm-10 form-group">
    <div class="col-sm-4">
        On behalf of(agency name)<input type="text" name="contractor_company_name" class="form-control"placeholder="Company" required="required">
    </div>
    <div class="col-sm-2">
    </div>
    <div class="col-sm-4">
        Overtime Payrate
        <input type="text" name="overtime_payrate" onkeypress="return isNumberKey(event);" class="form-control" placeholder="£/hr" required="required">
    </div>
</div>

<div class="col-sm-10 form-group">
    <div class="col-sm-4">
        Timesheet Approver<select id="timesheet_approver" onchange="run()" name="timesheet_approver" class="form-control" required="required">
            <option value="in">Inside Your Company</option>
            <option value="out">Client Company</option>   
        </select>
    </div>
    <div class="col-sm-2">
    </div>
    <div class="col-sm-4">

        Other Payrate<input type="text" onkeypress="return isNumberKey(event);" name="other_payrate" class="form-control" placeholder="£/hr" required="required">
        <br>             

        Addition Company Policies <textarea name="company_policy"></textarea>  

    </div>
</div>

<div class="col-sm-6 form-group">
    <div class="col-sm-4" id="inside" style="display: block;" >

        <select  name="inside_company_employees" style="width: 260px;" class="form-control" required="required">
            <option value="0">Select Approver</option>
            <option value="">Employee 1</option>
            <option value="">Employee 2</option>

        </select></div>

    <div class="col-sm-4" id="outside" style="display: none" >
        Approver Name <input type="text"style="width: 260px;" name="approver_name" class="form-control" placeholder="Name" required="required">
        <br>
        Approver Email <input type="text" style="width: 260px;" name="approver_email" class="form-control" placeholder="Email" required="required">
        <br>
        Approver Phone <input type="text" style="width: 260px;" name="approver_phone" class="form-control" placeholder="Phone" required="required">

    </div>


</div>

<div class="col-sm-2">
</div>


</div>

<div class="col-sm-10">
    <div class="col-sm-4">
        Timesheet Frequency<select id="timesheet_frequency" name="timesheet_frequency" class="form-control" required="required">
            <option value="0">Select Frequency</option>
            <option value="in">Every week</option>
            <option value="out">Every month</option>

        </select> <br>
        <p style="display: none;">
        Timesheet Template <select name="timesheet_template" class="form-control">
            <option value="0">Options</option>
            <option value="1">Standard</option>

        </select>
        </p>
    </div>

</div>
<div class="col-sm-10">
    <input type="submit" value="Done" class="btn btn-primary">
</div>
<div class="clearfix"></div>

<script type="text/javascript">
    function run() {
        if (document.getElementById("timesheet_approver").value == 'out') {
//       document.getElementById("inside").style.css.display
            document.getElementById("outside").style.display = 'block';
            document.getElementById("inside").style.display = 'none';
        }
        else {
            document.getElementById("inside").style.display = 'block';
            document.getElementById("outside").style.display = 'none';
        }
    }


</script>

<script type="text/javascript">
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
</script>

<script type="text/javascript">
    var config = {
        '.chosen-select': {},
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>