<?php

$this->breadcrumbs=array(
	'Contracts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Contract', 'url'=>array('index')),
	array('label'=>'Manage Contract', 'url'=>array('admin')),
);
?>

<div class="box-header">
    <h3 class="box-title">Create Contract</h3>
</div>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>