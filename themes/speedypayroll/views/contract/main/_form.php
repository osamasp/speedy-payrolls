
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'contract-form',
    'enableAjaxValidation' => false,
        ));
?>
<div class="box-body">
    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php if ($model->hasErrors()) { ?>
        <div class='callout callout-danger'>    <?php echo $form->errorSummary($model); ?>
        </div>
    <?php } ?>    

    <div class="form-group">
        <?php echo $form->labelEx($model, 'user_id'); ?><br>
        <a href="<?php echo $this->createUrl('/user/main/view', array('id' => $model->user->id)); ?>"><?php echo $model->user->full_name; ?></a>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'company_id'); ?><br>
        <?php echo $model->company->name; ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'role'); ?>
        <?php echo $form->textField($model, 'role', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
        <?php echo $form->error($model, 'role', array('class' => 'text-red')); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'employee_type'); ?>
        <?php echo $form->dropDownList($model, 'employee_type', array('full_time' => 'Full Time', 'part_time' => 'Part Time'), array('class' => 'form-control')); ?>
        <?php echo $form->error($model, 'employee_type', array('class' => 'text-red')); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'approver_id'); ?>
        <?php
        echo $form->dropDownList($model, 'approver_id', CHtml::listData(AppContract::get_all_in_users(), 'id', function($user) {
                    return $user->full_name;
                }), array('class' => 'form-control'));
        ?>
        <?php echo $form->error($model, 'approver_id', array('class' => 'text-red')); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'payroller_id'); ?>
        <?php echo $form->textField($model, 'payroller_id', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
        <?php echo $form->error($model, 'payroller_id', array('class' => 'text-red')); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'type'); ?>
        <?php echo $form->dropDownList($model, 'type', array('in' => 'In', 'out' => 'Out', 'in_out' => 'In Out', 'out_b' => 'Out B'), array('class' => 'form-control')); ?>
        <?php echo $form->error($model, 'type', array('class' => 'text-red')); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'policy'); ?>
        <?php echo $form->textArea($model, 'policy', array('rows' => 6, 'cols' => 50, 'class' => 'form-control')); ?>
        <?php echo $form->error($model, 'policy', array('class' => 'text-red')); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'start_time'); ?>
        <?php echo $form->textField($model, 'start_time', array('size' => 20, 'maxlength' => 20, 'class' => 'form-control')); ?>
        <?php echo $form->error($model, 'start_time', array('class' => 'text-red')); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'end_time'); ?>
        <?php echo $form->textField($model, 'end_time', array('size' => 20, 'maxlength' => 20, 'class' => 'form-control')); ?>
        <?php echo $form->error($model, 'end_time', array('class' => 'text-red')); ?>
    </div>
    <h3>Pay Rates</h3>
    <?php if ($model->contractSettings) { ?>
        <?php foreach ($model->contractSettings as $setting) { ?>
            <?php if ($setting->category != 'payrate') { ?>
                <?php continue; ?>
            <?php } ?>
            <div class="form-group">
                <label><?php echo ucwords(str_replace('_', ' ', $setting->key)); ?></label>
                <input type="text" class="form-control" name="Payrate[<?php echo $setting->key; ?>]" value='<?php echo $setting->value; ?>'>
            </div>
        <?php } ?>
    <?php } ?>

</div>
<div class="box-footer">
    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary')); ?>
</div>

<?php $this->endWidget(); ?>
<script type="text/javascript">
$(document).ready(function() {
        $('#contract-form').h5Validate({errorClass:'errorClass'});
    });
</script>