<?php
$this->breadcrumbs = array(
    'Contracts' => array('index'),
    $model->user->first_name,
);

$this->menu = array(
    array('label' => 'List Contract', 'url' => array('index')),
    array('label' => 'Create Contract', 'url' => array('create')),
    array('label' => 'Update Contract', 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Delete Contract', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage Contract', 'url' => array('admin')),
);
?>
<div class="portlet box grey-cascade">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-file-o"></i>View Contract
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">

<div class="box-header row">
    <h3 class="box-title col-md-10">View Contract of <b><?php echo ucwords($model->user->full_name); ?></b></h3>
    <div class="col-md-2" style="text-align:right;padding-top:18px;">
        <a href="<?php echo $this->createUrl('/contract/main/update/id/'.$model->id.'?Staff_Management'); ?>" class="btn btn-primary">Update Contract</a> 
    </div>
</div>
<div class="table-responsive">
    <div class="box-body">
        <?php
        $this->widget('zii.widgets.CDetailView', array(
            'data' => $model,
            'attributes' => array(
           array('name'=>'User',
                    'value'=>$model->user->full_name),
                array('name'=>'Company',
                    'value'=>$model->company_name),
                array('name'=>'Role','value'=>$model->customrole),
                'employee_type',
                array('name'=>'Approver',
                    'value'=>$model->approver != NULL ? $model->approver->full_name : '-'),
                array('name'=>'Payroller',
                    'value'=>$model->payroller != NULL ?$model->payroller->full_name : '-'),
                'type',
                 array('name'=>'Policy',
                    'value'=>$model->policy != NULL ?$model->policy : '-'),
                    AppInterface::getUploadedFiles($model,'Contract attachment'),
                array('name'=>'Start Time',
                    'value'=> $model->start_time != -1 ? date(AppInterface::getdateformat(),$model->start_time) : ''),
                array('name'=>'End Time',
                    'value'=> $model->end_time != -1 ? date(AppInterface::getdateformat(),$model->end_time) : '-'),
            ),
        ));
        ?>
    </div>
</div>
                    </div>
</div>