<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

        <div class="form-group">
        <?php echo $form->label($model,'id'); ?>
        <?php echo $form->textField($model,'id',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'user_id'); ?>
        <?php echo $form->textField($model,'user_id',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'company_id'); ?>
        <?php echo $form->textField($model,'company_id',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'role'); ?>
        <?php echo $form->textField($model,'role',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'employee_type'); ?>
        <?php echo $form->textField($model,'employee_type',array('size'=>9,'maxlength'=>9,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'approver_id'); ?>
        <?php echo $form->textField($model,'approver_id',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'payroller_id'); ?>
        <?php echo $form->textField($model,'payroller_id',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'type'); ?>
        <?php echo $form->textField($model,'type',array('size'=>6,'maxlength'=>6,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'policy'); ?>
        <?php echo $form->textArea($model,'policy',array('rows'=>6, 'cols'=>50,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'start_time'); ?>
        <?php echo $form->textField($model,'start_time',array('size'=>20,'maxlength'=>20,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'end_time'); ?>
        <?php echo $form->textField($model,'end_time',array('size'=>20,'maxlength'=>20,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'status'); ?>
        <?php echo $form->textField($model,'status', array('class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'invitation_code'); ?>
        <?php echo $form->textField($model,'invitation_code',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'created_at'); ?>
        <?php echo $form->textField($model,'created_at',array('size'=>20,'maxlength'=>20,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'created_by'); ?>
        <?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'modified_at'); ?>
        <?php echo $form->textField($model,'modified_at',array('size'=>20,'maxlength'=>20,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'modified_by'); ?>
        <?php echo $form->textField($model,'modified_by',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
    </div>

<div class="form-group">
    <?php echo CHtml::submitButton('Search'); ?>
</div>

<?php $this->endWidget(); ?>
<!-- search-form -->