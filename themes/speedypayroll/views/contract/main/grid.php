<?php

$this->breadcrumbs = array(
    'Users' => array('/user/employee'),
    $dataProvider ? $dataProvider[0]->user->full_name : '',
);

$this->menu = array(
    array('label' => 'Create User', 'url' => array('create')),
    array('label' => 'Manage User', 'url' => array('admin')),
);
?>
<div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-globe"></i>View Contract
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>

                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">

                        <div class="col-md-12">
                            
                        </div>
                    </div>
                </div>
<div class="box-header">
    <h3 class="box-title"><?php $dataProvider ? $dataProvider[0]->user->full_name : 'USERS'; ?></h3>
    <div class="box-tools">
    </div>
</div>

<div class="box-body table-responsive">
    <table id="example1" class="table table-responsive table-striped">
        <thead>
            <tr>
                <th><input type="checkbox" name="check" clas="form-control" /></th>
                <th>Employee name</th>
                <th>Company</th>
                <th>Contract Type</th>
                <th>Date Joined</th>
                <th>Date Ending</th>
                <th>Approver</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($dataProvider as $item) { ?>
              <?php if ($item->is_internal && $item->company_id == AppUser::getUserCompany()->id) { ?>
                        <tr style="background-color: #D7FAD7;">
                        <?php } else if ($item->type == 'out' && $item->company_id == AppUser::getUserCompany()->id) { ?>
                        <tr style="background-color:#FAD7FA;">
                        <?php } else if ($item->type == 'out') { ?>
                        <tr style="background-color:#D7E9FA;">                                 
                        <?php } else if ($item->type == 'out_b' || $item->type == 'in_out') { ?>
                        <tr style="background-color:#FACDCD;">                                 
                        <?php } ?>  
                    <td><input type="checkbox" value="<?php echo $item->id;?>" name="check" clas="form-control" /></td>
                            <td><?php echo $item->user->full_name; ?></td>
                    <td><?php echo $item->company_name; ?></td>
                    <td><?php echo ucwords(str_replace('_', ' ', $item->CustomType )); ?></td>
                    <td><?php echo date(AppInterface::getdateformat(),$item->start_time); ?></td>
                    <td><?php echo $item->end_time <= 0 ? '-' : date(AppInterface::getdateformat(),$item->end_time); ?></td>
                    <td><?php echo $item->approver ? $item->approver->full_name : '-'; ?></td>
                    <td>
                        <div class="btn-group">
                                <button class="btn green dropdown-toggle" data-toggle="dropdown">Actions <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="<?php echo $this->createUrl('/contract/main/view', array('id' => $item->id)); ?>">View</a></li>
                                    <li><a href="<?php echo $this->createUrl('/timesheet/main/index', array('contract' => $item->id)); ?>">View timesheets</a></li>
                            </ul>
                        </div>

                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
            </div>
</div>