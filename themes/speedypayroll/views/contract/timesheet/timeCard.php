<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Timesheet Details</h4>
      </div>
      <div class="modal-body row">
            
              
          <div class="col-md-6">
               <p>Name: <?php echo $employeeName; ?></p>
               <p>Staff ID: <?php echo $employeeId; ?></p><br/>
               <p>Company: <?php echo ucfirst($company); ?></p>
               <p>Location: <?php echo $companyCity.','.$companyCountry; ?></p>
               <p>Regular Hours: <?php echo $regularTime; ?></p>
               <p>Additional Hours: <?php echo $addTime; ?></p>
               
              
            <!-- Your first column here -->
          </div>
          <div class="col-md-6">
              <p>Date Submitted: <?php echo date('m/d/Y',$date); ?></p>
              <p></p>
              <p>PaySlip Period: <?php echo 'WK'.date('W',$date); ?></p>
              <p>Employment Type: <?php echo ucfirst($employeeType); ?></p>
            <!-- Your second column here -->
          </div>
          
          
           
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        
      </div>
