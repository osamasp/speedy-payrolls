<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'timesheet-form',
    'enableAjaxValidation' => false
        ));


?>
<div class="box-body">
    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php if ($model->hasErrors()) { ?>
        <div class='callout callout-danger'>    <?php echo $form->errorSummary($model); ?>
        </div>
    <?php } ?> 

    <div class="form-group">
        <?php echo CHtml::label("Timesheet Account *", 'type'); ?><br/>
        <?php
        if ($contracts != null && count($contracts) > 0) {
            $htmlOption = array('class' => 'autoPostDD form-control');
            if (!$model->isNewRecord) {
                $htmlOption['disabled'] = "disabled";
            }
            echo $form->dropDownList($model, 'contract_id', array_merge(array('0' => "Please Select Contract"), CHtml::listData($contracts, 'id', 'CustomName')), $htmlOption);
        }
        echo $form->error($model, 'contract_id', array('class' => 'text-red'));
        ?>
    </div>



    <div class="form-group">
        <?php echo CHtml::label("Timesheet Type:", 'type'); ?><br/>
        <?php
        if ($selectedType != '')
            echo CHtml::label($selectedType, 'type');
        else
            echo CHtml::dropDownList('type', $selectedType, array_merge(array('0' => "Please Select Type"), $tSheetTypes), array('class' => 'form-control', 'required' => 'required'));
        ?>
        <?php echo $form->error($model, 'type', array('class' => 'text-red')); ?>
    </div>

    <div class="form-group">
        <label>Select Day *</label>
        <div class="input-group">
            <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
            </div>
            <?php echo $form->textField($model, 'start_time', array('class' => 'form-control pull-left datePicker disableEle', 'placeholder' => 'Click To Select the Dates', 'required' => 'required')); ?>
        </div> 
    </div>
    <div >
        <div class="bootstrap-timepicker"><div class="bootstrap-timepicker-widget dropdown-menu"><table><tbody><tr><td><a href="#" data-action="incrementHour"><i class="glyphicon glyphicon-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="incrementMinute"><i class="glyphicon glyphicon-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td class="meridian-column"><a href="#" data-action="toggleMeridian"><i class="glyphicon glyphicon-chevron-up"></i></a></td></tr><tr><td><span class="bootstrap-timepicker-hour">10</span></td> <td class="separator">:</td><td><span class="bootstrap-timepicker-minute">45</span></td> <td class="separator">&nbsp;</td><td><span class="bootstrap-timepicker-meridian">PM</span></td></tr><tr><td><a href="#" data-action="decrementHour"><i class="glyphicon glyphicon-chevron-down"></i></a></td><td class="separator"></td><td><a href="#" data-action="decrementMinute"><i class="glyphicon glyphicon-chevron-down"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="toggleMeridian"><i class="glyphicon glyphicon-chevron-down"></i></a></td></tr></tbody></table></div>
            <div class="form-group">
                <label>Time In * (Click to Change the Time)</label>
                <div class="input-group">
                    <input type="text" name="timeIn" value="<?php echo (isset($_POST['timeIn']))?$_POST['timeIn']:((isset($tempStartTime))?date('h:i A',$tempStartTime):''); ?>" required="required" class="form-control timepickerIn disableEle">
                    <div class="input-group-addon">
                        <i class="fa fa-clock-o"></i>
                    </div>
                </div> 
            </div> 
        </div>
        <div class="bootstrap-timepicker"><div class="bootstrap-timepicker-widget dropdown-menu"><table><tbody><tr><td><a href="#" data-action="incrementHour"><i class="glyphicon glyphicon-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="incrementMinute"><i class="glyphicon glyphicon-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td class="meridian-column"><a href="#" data-action="toggleMeridian"><i class="glyphicon glyphicon-chevron-up"></i></a></td></tr><tr><td><span class="bootstrap-timepicker-hour">10</span></td> <td class="separator">:</td><td><span class="bootstrap-timepicker-minute">45</span></td> <td class="separator">&nbsp;</td><td><span class="bootstrap-timepicker-meridian">PM</span></td></tr><tr><td><a href="#" data-action="decrementHour"><i class="glyphicon glyphicon-chevron-down"></i></a></td><td class="separator"></td><td><a href="#" data-action="decrementMinute"><i class="glyphicon glyphicon-chevron-down"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="toggleMeridian"><i class="glyphicon glyphicon-chevron-down"></i></a></td></tr></tbody></table></div>
            <div class="form-group">
                <label>Time Out * (Click to Change the Time)</label>
                <div class="input-group">
                    <input type="text" name="timeOut" value="<?php echo (isset($_POST['timeOut']))?$_POST['timeOut']:(($model->end_time)?date('h:i A',$model->end_time):''); ?>" required="required" class="form-control timepickerOut disableEle">
                    <div class="input-group-addon">
                        <i class="fa fa-clock-o"></i>
                    </div>
                </div>  
            </div> 
        </div>


    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'lunchtime'); ?>
        <div class="input-group">
            <div class="input-group-addon">
                <i class="fa fa-clock-o"></i>
            </div>
            <?php echo $form->textField($model, 'lunchtime', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control', 'placeholder' => 'Minutes/Hours')); ?>
            <?php echo $form->error($model, 'lunchtime', array('class' => 'text-red')); ?>
        </div>

    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'overtime'); ?>

        <div class="input-group">
            <div class="input-group-addon">
                <i class="fa fa-clock-o"></i>
            </div>
            <?php echo $form->textField($model, 'overtime', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control', 'placeholder' => 'Minutes/Hours')); ?>
            <?php echo $form->error($model, 'overtime', array('class' => 'text-red')); ?>
        </div> 


    </div>
    <div class="form-group">
        <?php echo $form->labelEx($model, 'expense'); ?>
        <div class="input-group">
            <div class="input-group-addon">
                <i class="fa fa-gbp"></i>
            </div>
            <?php echo $form->textField($model, 'expense', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control', 'placeholder' => '0')); ?>
            <?php echo $form->error($model, 'expense', array('class' => 'text-red')); ?>
        </div> 

        <?php echo $form->error($model, 'expense', array('class' => 'text-red')); ?>
    </div>


</div>
<div class="box-footer">
    <?php echo CHtml::submitButton($model->isNewRecord ? 'Add Entry' : 'Save', array('class' => 'btn btn-primary')); ?>
</div>

<?php $this->endWidget(); ?>
