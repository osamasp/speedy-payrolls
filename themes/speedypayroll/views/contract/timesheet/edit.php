<?php
$this->breadcrumbs = array(
    'Timesheets' => array('index'),
    'Edit',
);

$this->menu = array(
    array('label' => 'List Contract', 'url' => array('index')),
    array('label' => 'Manage Contract', 'url' => array('admin')),
);
?>
<div class="box-header">
        <h3 class="box-title">Edit Timesheet</h3>
 </div>
<?php 
     switch($selectedType){
       
         case 'daily':
                $this->renderPartial('_dailyentry', array('model'=>$model,'contracts'=>$contracts,'tSheetTypes'=>$tSheetTypes,'selectedType'=>$selectedType,'tempStartTime'=>$tempStartTime));
               break;
         default:
                $this->renderPartial('_customentry', array('model'=>$model,'contracts'=>$contracts,'tSheetTypes'=>$tSheetTypes,'selectedType'=>$selectedType));
             break;
         
         
     }

 ?>
