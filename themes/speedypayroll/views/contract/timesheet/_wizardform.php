<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

    

<form method="post" action="" class="form-horizontal">
    <div class="box-body">
        <?php if ($error != '') { ?>
            <div class='callout callout-danger'>    <?php echo $error; ?> 
            </div>
        <?php } ?>
        <div class="form-group">
            Select Contract :

            <?php if ($contracts != null && count($contracts) > 0) {
                
                    echo CHtml::dropDownList('contract', Yii::app()->getRequest()->getPost('contract'), array_merge(array('0'=>"Please Select Contract"),  CHtml::listData($contracts,'id','CustomName')),array('class'=>'contractDD form-control'));
            }
                    ?>
                
        </div>
        <div class="form-group">
            Timesheet Type: 
            <?php echo CHtml::dropDownList('type', Yii::app()->getRequest()->getPost('type'), array_merge(array('0'=>"Please Select Type"),  $tSheetType),array('class'=>'tsheetDD form-control')); ?>
            <label class="tsheetLabel"></label>

        </div>

    </div>
    <div class="box-footer">
        <?php echo CHtml::submitButton('Add', array('class' => 'btn btn-primary')); ?>
    </div>
</form>
