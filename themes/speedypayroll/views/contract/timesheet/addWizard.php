<?php
$this->breadcrumbs = array(
    'Timesheets' => array('index'),
    'Add - Step 1',
);

$this->menu = array(
    array('label' => 'List Contract', 'url' => array('index')),
    array('label' => 'Manage Contract', 'url' => array('admin')),
);
?>
<div class="box-header">
        <h3 class="box-title">Add Timesheet</h3>
 </div>
<?php $this->renderPartial('_wizardform', array('tSheetType'=>$tSheetType,'contracts'=>$contracts,'error'=>$error)); ?>