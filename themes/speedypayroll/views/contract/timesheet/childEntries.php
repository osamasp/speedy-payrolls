<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$regSum = 0;
$addSum = 0;
$expSum = 0;
?>
<div class="box-header">
    <h3 class="box-title">Details - <?php
if ($parent->type == "daily") {
    echo "Week " . date('W', $parent->end_time) . ' - (<small>' . date('m/d/Y', $parent->start_time) . ' to ' . date('m/d/Y', $parent->end_time) . '</small>)';
} else {
    echo date('m/d/Y', $parent->start_time) . ' to ' . date('m/d/Y', $parent->end_time);
}
?></h3>
</div>
<div class="box-body">
    <table class="table table-responsive table-hover" aria-describedby="example2_info">

        <thead>
            <tr>
                <th rowspan="1" colspan="1">Day</th>
                <th rowspan="1" colspan="1">Date</th>
                <th rowspan="1" colspan="1">Start</th>
                <th rowspan="1" colspan="1">Finish</th>
                <th rowspan="1" colspan="1">Lunch</th>
                <th rowspan="1" colspan="1">Regular Hours</th>
                <th rowspan="1" colspan="1">Additional Hours</th>
                <th rowspan="1" colspan="1">Extra Charges</th>
                <th rowspan="1" colspan="1">Action</th>
            </tr>
        </thead>

        <?php if (count($dataSet) > 0) { ?>
            <tbody role="alert" aria-live="polite" aria-relevant="all">
                <?php
                foreach ($dataSet as $entry) {
                    $regularTime = AppContract::calHrsDiff($entry->start_time, $entry->end_time);
                    $regSum += $regularTime;
                    $addSum += $entry->overtime;
                    $expSum += $entry->expense;
                    ?>
                    <tr class="odd">
                        <td><?php echo date('l', $entry->start_time); ?></td>
                        <td><?php echo date('m/d/Y', $entry->start_time); ?></td>
                        <td><?php echo date('h:i A', $entry->start_time); ?></td>
                        <td><?php echo date('h:i A', $entry->end_time); ?></td>
                        <td><?php echo $entry->lunchtime; ?></td>
                        <td><?php echo $regularTime; ?></td>
                        <td><?php echo number_format($entry->overtime); ?></td>
                        <td>&pound;<?php echo number_format($entry->expense); ?></td>
                        <td><?php if ($entry->type == "daily") { ?><a href="<?php echo Yii::app()->createUrl("contract/timesheet/edit", array("entry" => $entry->id)); ?>"> Edit</a> <?php } ?></td>

                    </tr>
            <?php } ?>
            </tbody>
<?php } ?>
        <tfoot>
            <tr>
                <th rowspan="1" colspan="1"></th>
                <th rowspan="1" colspan="1"></th>
                <th rowspan="1" colspan="1"></th>
                <th rowspan="1" colspan="1"></th>
                <th rowspan="1" colspan="1">Total</th>
                <th rowspan="1" colspan="1"><?php echo number_format($regSum); ?> Hrs.</th>
                <th rowspan="1" colspan="1"><?php echo number_format($addSum); ?> Hrs.</th>
                <th rowspan="1" colspan="1">&pound;<?php echo number_format($expSum); ?></th>
                <th rowspan="1" colspan="1"></th>
            </tr>
        </tfoot>

    </table>

</div>
