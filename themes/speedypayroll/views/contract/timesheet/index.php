<?php
$this->breadcrumbs = array(
    'Timesheets' => array('index'),
    'Add - Step 1' => array('addWizard'),
    'Add',
);
?>
<div class="box-header">
    <h3 class="box-title">Timesheets</h3>
</div>
<div class="box-body">
    <table class="table table-responsive table-hover" aria-describedby="example2_info">

        <thead>
            <tr>
                <th rowspan="1" colspan="1">Contract</th>
                <th rowspan="1" colspan="1">Approver</th>
                <th rowspan="1" colspan="1">Period</th>
                <th rowspan="1" colspan="1">Status</th>
                <th rowspan="1" colspan="1">PaySlip</th>
                <th rowspan="1" colspan="1">Action</th>
                <th rowspan="1" colspan="1"></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th rowspan="1" colspan="1">Contract</th>
                <th rowspan="1" colspan="1">Approver</th>
                <th rowspan="1" colspan="1">Period</th>
                <th rowspan="1" colspan="1">Status</th>
                <th rowspan="1" colspan="1">PaySlip</th>
                <th rowspan="1" colspan="1">Action</th>
                <th rowspan="1" colspan="1"></th>
                
            </tr>
        </tfoot>
        <?php if (count($entries) > 0) { ?>
            <tbody role="alert" aria-live="polite" aria-relevant="all">
                <?php foreach ($entries as $entry) { ?>
                    <tr class="odd">
                        <td class=""><?php echo $entry->contract->company->name . ' - ' . $entry->contract->type; ?></td>
                        <td class=" "><?php echo $entry->contract->approver->full_name; ?></td>
                        <td class=" ">
                            <?php echo 'Week ' . date('W', $entry->end_time); ?><small> - (<?php echo date('m/d/Y', $entry->start_time) . ' to ' . date('m/d/Y', $entry->end_time); ?>)</small></td>
                        <td class=" "><?php echo $entry->status; ?></td>
                        <td class=" ">None</td>
                        <td class=" ">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".ajaxModel" data-whatever="<?php echo $entry->id; ?>">View</button>
                            <?php if ($entry->type != "daily") { ?>| <a href="<?php echo Yii::app()->createUrl("contract/timesheet/edit", array("entry" => $entry->id)); ?>"> Edit</a> <?php } ?>
                            | Print</td>
                        <td class="">
                            <button class="btn btn-info btn-flat <?php echo ($entry->type == "daily") ? "wklyLnkPrnt" : ""; ?>" tSheetId="<?php echo $entry->id; ?>" >Details</button>
                            </td>
                    </tr>
                <?php } ?>
            </tbody>
        <?php } ?>

    </table>
    <?php
                    $this->widget('CLinkPager', array(
                        'pages' => $pages,
                        'header' => '',
                        'footer' => '',
                        'maxButtonCount'=>3,
                        'nextPageLabel' => 'Next',
                        'prevPageLabel' => 'Prev',
                        'selectedPageCssClass' => 'active',
                        'hiddenPageCssClass' => 'disabled',
                        'htmlOptions' => array(
                            'class'=>'pagination pull-right',
                        )
                    ));

    ?>
</div>

<br/>
<div class="childEntryContainer" style="display:none">

</div>