<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box grey-cascade">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-bullhorn"></i>Send Notification to Employees
                </div>

            </div>
            <div class="portlet-body">
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'company_message-form',
                    // Please note: When you enable ajax validation, make sure the corresponding
                    // controller action is handling ajax validation correctly.
                    // There is a call to performAjaxValidation() commented in generated controller code.
                    // See class documentation of CActiveForm for details on this.
                    'enableAjaxValidation' => false,
                    'htmlOptions' => array(
                        'class' => 'form-horizontal',
                    ),
                ));
                ?>
                <div class="form">
                    <div class="form-group">
                        <!--                <label class="control-label col-md-3">Title</label>-->
                        <div class="col-md-12">
                            <input type="text" name="CompanyMessage[title]" id="title" class="form-control todo-taskbody-tasktitle" placeholder="Title..">
                        </div>
                    </div>
                    <div class="form-group">
                        <!--                <label class="control-label col-md-3">Message</label>-->
                        <div class="col-md-12">
                            <textarea class="form-control todo-taskbody-taskdesc" rows="8" placeholder="Message..." name="CompanyMessage[message]"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <!--<label class="control-label col-md-3"><b>Employees:</b></label>-->
                        <div class="col-md-12">
                            <select id="employeeTags" class="form-control" name="employee[]" placeholder="Search an employee or employees to send notification to..." multiple="multiple">
                                <?php
                                $value = null;
                                if (isset($selected)) {
                                    $value = explode(",", $selected);
                                }
                                foreach ($employees as $item) {
                                    if (isset($value)) {
                                        foreach ($value as $data) {
                                            $select = $data == $item->id ? 'selected=""' : '';
                                            ?>
                                            <option <?php echo $select; ?> value="<?php echo $item->id; ?>"><?php echo $item->first_name . " " . $item->last_name; ?></option>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <option value="<?php echo $item->id; ?>"><?php echo $item->first_name . " " . $item->last_name; ?></option>
                                    <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-offset-2 col-md-12">
                            <div class="checkbox-list">
                                <label><input type="checkbox" id="all" value="0"/>  Send to All</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions right todo-form-actions">
                        <button class="btn btn-circle btn-sm green-haze" onclick="create()" type="button">Send</button>
                        <button class="btn btn-circle btn-sm btn-default" type="button">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->endWidget(); ?>
<script>
    $(document).ready(function () {
        $('#all').click(function () {
            if ($(this).prop("checked")) {
                $("option").attr("selected", "selected");
            } else {
                $("option").removeAttr("selected");
            }
            $("#employeeTags").select2();
        });

    });
    function create()
    {
        if ($("#employeeTags").val() === null || $("#employeeTags").val().length === 0)
        {
            alert("Please select employee.");
        } else if ($('textarea').val() === "")
        {
            alert("Please write message.");
        } else
        {
            $("#company_message-form").submit();
        }
    }
</script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/thirdparty/js/todo.js" type="text/javascript"></script>
<script>
    jQuery(document).ready(function () {
        $("#employeeTags").select2();
    });

</script>