<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="todo-tasklist-item <?php echo AppUser::getNotificationColor($notification); ?>" id="<?php echo 'notif-'.$notification->id; ?>">													
    <div class="todo-tasklist-item-title">
        <?php if($notification->notification_type_id ==15) { 
                echo AppUser::getUserCompany()->name;
        }
        else{
            echo 'CloudTimeKeeper';
        }
?>
    </div>

    <div class="todo-tasklist-item-text">
        <a href="<?php echo AppUser::getNotificationLink($notification->id, $notification->notification_type_id, $notification->timesheet_id, $notification->contract_id, $notification->user_id); ?>"><?php echo AppUser::formatNotificationMessage($notification); ?></a>
    </div>
    <div class="todo-tasklist-controls pull-left">
	<span class="todo-tasklist-date"><i class="fa fa-thumb-tack"></i> <?php echo AppUser::getNotificationTime($notification); ?> </span>														
    </div>
</div>