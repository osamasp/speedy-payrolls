<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'company_message-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal form-bordered',
    ),
        ));
?>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i>View Message
        </div>
    </div>
    <div class="portlet-body form">
        <div class="box-body">
            <div class="form-group">
                <label class="control-label col-md-3"><b>Title:</b></label>
                <label class="control-label"><?php echo $model->title; ?></label>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3"><b>Message:</b></label>
                <label class="control-label"><?php echo $model->notice; ?></label>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>