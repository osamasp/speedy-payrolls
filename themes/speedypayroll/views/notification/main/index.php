<?php
/* @var $this MainController */

$this->breadcrumbs = array(
    'Main',
);
?>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box grey-cascade">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-inbox"></i>Notification Inbox
                </div>
            </div>
            <div>
                <div class="portlet-body">
                    <div class="todo-content">
                        <div class="portlet light">
                            <div class="portlet-body">
                                <div class="row">
<!--                                    <div class="scroller" style="max-height: 550px; overflow: hidden; width: auto; height: 550px;" data-always-visible="0" data-rail-visible="0" data-handle-color="#dae3e7" data-initialized="1">-->
<!--                                        <div class="scroller" style="max-height: 850px;" data-always-visible="0" data-rail-visible="0" data-handle-color="#dae3e7">-->
                                        <div class="todo-tasklist">
                                        <?php
                                            foreach ($notifications as $notification) {
                                                $this->renderPartial('_index', array('notification' => $notification));
                                            }
                                            ?>
                                        </div>
<!--                                    </div>-->
                                </div>
                                <div class="scroller-footer" style="padding-bottom: 15px; padding-left: 15px;">
                    <?php
                    $this->widget('CLinkPager', array(
                        'pages' => $pages,
                        'header' => '',
                        'footer' => '',
                        'maxButtonCount'=>3,
                        'nextPageLabel' => 'Next',
                        'prevPageLabel' => 'Prev',
                        'selectedPageCssClass' => 'active',
                        'hiddenPageCssClass' => 'disabled',
                        'htmlOptions' => array(
                            'class'=>'pagination pull-right',
                        )
                    ));
                    ?>   
                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
            </div>
        </div>
    </div>
</div>
<script src="<?php echo Yii::app()->theme->baseUrl;  ?>/thirdparty/js/todo.js" type="text/javascript"></script>
<script>
    jQuery(document).ready(function () {
        Todo.init();
        $("div[id^='notif']").on("click", function (e) {
            markRead(e.target.id.substr(6));
        });
    });
    function markRead(id)
    {
        var url = '<?php echo Yii::app()->createUrl("/notification/main/markread/id"); ?>';
        url = url.concat('/' + id);
        $.get(url, function (result) {
            $("#" + result).html('<i class="fa fa-circle"></li>');
        });
    }
</script>