<?php $this->beginContent('//layouts/main'); ?>
<div class="row">
    <div class="col-md-8">
        <div class="box box-primary">
            <?php echo $content; ?>
        </div>
    </div><!-- content -->
    <div class="col-md-4">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Operations</h3>
            </div>

            <div class="box-body">
                <?php
                $this->widget('zii.widgets.CMenu', array(
                    'items' => $this->menu,
                    'htmlOptions' => array('class' => 'operations'),
                ));
                ?>
            </div>
        </div>
    </div><!-- sidebar -->
</div>

<?php $this->endContent(); ?>