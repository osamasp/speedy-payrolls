<!DOCTYPE html>
<html>
    <head>
        <?php echo $this->renderPartial('//layouts/includes/head'); ?>
    </head>
    <body class="page-md page-header-fixed page-sidebar-closed-hide-logo page-sidebar-closed-hide-logo">
        <?php echo $this->renderPartial('//layouts/includes/header'); ?>
        <!--</header>-->
        <?php echo $this->renderPartial('//layouts/includes/modelPopUp'); ?>
        <div class="clearfix">
        </div>
        <div class="page-container">
            <!-- Left side column. contains the logo and sidebar -->
            <div class="page-sidebar-wrapper">               
                <?php echo $this->renderPartial('//layouts/includes/sidebar'); ?>
            </div>
            <!-- Right side column. Contains the navbar and content of the page -->
            <div class="page-content-wrapper">              
                <!-- Content Header (Page header) -->

                <section class="content-header">                    
                    <?php if (isset($_SERVER['HTTP_REFERER'])) { ?>
                        <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>"></a>
                    <?php } ?>
                </section>
                <!-- Main content -->
                <div class="page-content">
                                    <?php echo $this->renderPartial('//layouts/includes/flash-messages'); ?>

                    <?php if (isset($this->breadcrumbs)): ?>
                        <?php
                        $this->widget('zii.widgets.CBreadcrumbs', array(
                            'links' => $this->breadcrumbs,
                        ));
                        ?><!-- breadcrumbs -->
                    <?php endif ?>
                    <?php echo $content; ?>
                        
                        <!--Notification Popup-->
                        <div class="portlet mt-element-ribbon light portlet-fit bordered" id="notification" style="width:30%;right:0;display:none;bottom:0;position:fixed;">
                            <div class="ribbon ribbon-right ribbon-clip ribbon-shadow ribbon-border-dash-hor ribbon-color-success uppercase">
                                <div class="row"><div class="col-md-6"><div class="ribbon-sub ribbon-clip ribbon-right"></div> Notification </div></div></div>
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class=" icon-layers font-green"></i>
                                    <div class="row"><div class="col-md-12"><span class="caption-subject font-green bold uppercase">Seminant Solutions</span></div></div>
                                </div>
                            </div>
                            <div class="portlet-body" id="notification-msg"> </div>
                        </div>
                        <!--Notification Popup-->
                        
                </div><!-- /.content -->
            </div><!-- /.right-side -->
        </div><!-- ./wrapper -->
        <?php echo $this->renderPartial('//layouts/includes/footer'); ?>
    </body>
</html>
