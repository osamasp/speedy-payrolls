<?php header('Content-Type: text/html; charset=utf-8');
//Yii::app()->request->cookies['client_time'] = new CHttpCookie('client_time', $value);
?>
<meta charset="UTF-8">
<title><?php echo CHtml::encode($this->pageTitle); ?></title>
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<meta http-equiv="Cache-control" content="public">
<!--For IOS home screen icon-->
<link rel="apple-touch-icon" sizes="57x57" href="<?php echo Yii::app()->theme->baseUrl; ?>/img/apple-icon-57x57.png" />
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo Yii::app()->theme->baseUrl; ?>/img/apple-icon-72x72.png" />
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo Yii::app()->theme->baseUrl; ?>/img/apple-icon-114x114.png" />
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo Yii::app()->theme->baseUrl; ?>/img/apple-icon-144x144.png" />

<!--For android home screen icon-->
<link rel="icon" sizes="192x192" href="<?php echo Yii::app()->theme->baseUrl; ?>/img/apple-icon-57x57.png">
<link rel="icon" sizes="128x128" href="<?php echo Yii::app()->theme->baseUrl; ?>/img/apple-icon-57x57.png">

<link href="//cdn.datatables.net/responsive/1.0.0/css/dataTables.responsive.css" rel="stylesheet" type="text/css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/thirdparty/js/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/css/bootstrap3/bootstrap-switch.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN DATE PAGINATOR PLUGIN -->
<!--<link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.0/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css"/>-->
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/thirdparty/js/dropzone/css/dropzone.css" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.5.1/select2.css"/>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.10/css/dataTables.bootstrap.css"/>
<!-- END DATE PAGINATOR PLUGIN -->
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/thirdparty/js/clockface/css/clockface.css"/>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.0/css/bootstrap-datepicker3.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.timepicker/0.2.6/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2.1.17/daterangepicker.css"/>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css"/>

<!-- BEGIN THEME STYLES -->
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/components-md.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/components.min.css" id="style_components" rel="stylesheet" type="text/css"/>


<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/plugins-md.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/layout4/css/layout.css" rel="stylesheet" type="text/css"/>
<link id="style_color" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/layout4/css/themes/light.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/custom.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/error.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/todo.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.css">
<script>var basePath = "<?php echo Yii::app()->theme->baseUrl; ?>";</script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<!--<script src="//code.jquery.com/jquery-1.11.2.min.js" type="text/javascript"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.3.0/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<!--<script src="//code.jquery.com/ui/1.11.2/jquery-ui.min.js" type="text/javascript"></script>-->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" type="text/javascript"></script>
<!--<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-hover-dropdown/2.0.10/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>-->
<script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.2/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/thirdparty/js/jquery.blockui.min.js" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/3.5.1/select2.min.js"></script>
<!-- END CORE PLUGINS -->




<!-- BEGIN DATE PAGINATOR PLUGIN -->
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.7/jquery.slimscroll.min.js"></script>
<!--<script src="<?php // echo Yii::app()->theme->baseUrl; ?>/thirdparty/js/dropzone/dropzone.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.3/moment.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.1/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.0/js/bootstrap-datepicker.min.js"></script>-->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/thirdparty/js/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<!--<script type="text/javascript" src="<?php // echo Yii::app()->theme->baseUrl; ?>/thirdparty/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>-->
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/thirdparty/js/jquery.timer.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/thirdparty/js/metronic.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/css/layout4/scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/css/layout4/scripts/demo.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/css/pages/scripts/table-managed.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/css/pages/scripts/form-wizard.js"></script>
<link href="//cdn.bootcss.com/bootstrap-daterangepicker/1.3.23/daterangepicker-bs3.min.css" rel="stylesheet" type="text/css"/>
<!--<link href="<?php // echo Yii::app()->theme->baseUrl;  ?>/thirdparty/js/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css"/>-->
<!--<script src="<?php // echo Yii::app()->theme->baseUrl; ?>/css/pages/scripts/form-dropzone.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Knob/1.2.13/jquery.knob.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.3.7/socket.io.min.js"></script>
<script>
  var socket = io.connect('<?php echo Yii::app()->params['node_host'];?>',{'forceNew':true});
  socket.emit('set_user_id',{id:<?php echo AppUser::getUserId();?>});

  socket.on('private', function(data){
      
      $("#notification").show("slow", function(){
         $("#notification_count").html(data.count);   
      });
      
      $("#notification_count").show();
      
      $("#notification-msg").html(data.msg);
      console.log("Message Received via socket");  
      var notifyContent = '<li style="background-color:#404548"><a href="#" onclick="javascript:window.location.href=\''+data.url+'\'">'+data.logo+'<span style="margin-left:10px;">'+data.msg+'</span><div style="text-align:right;"><span><i class="fa fa-clock-o"></i>Just Now</span></div></a></li>';
      
      
      $("#notification_list").prepend(notifyContent);
      $("#notification_listli:eq(0)").before();
      setTimeout(function() {
    $('#notification').fadeOut('slow');
}, 5000);
    });

</script>

<!-- END PAGE LEVEL SCRIPTS -->
<!-- END DATE PAGINATOR PLUGIN -->
<script>
//    $(function($) {
//        $(".knob").knob();
//            });
    jQuery(document).ready(function() {
        $(".knob").knob();
        // initiate layout and plugins
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        Demo.init(); // init demo features
        //UIDatepaginator.init();
        TableManaged.init();
    });
setTimeout(function() {
    $('#notification').fadeOut('slow');
}, 5000);

</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/h5Validate/0.8.4/jquery.h5validate.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;  ?>/thirdparty/js/number-input/bootstrap-number-input.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;  ?>/thirdparty/js/general.js"></script>
