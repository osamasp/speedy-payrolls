<!-- sidebar: style can be found in sidebar.less -->
<?php
$current_user = AppUser::getCurrentUser();
$internal_contract = AppContract::getInternalContract(Yii::app()->user->id);
$privileges = Yii::app()->session['privileges'];
//dd($privileges);
//dd(AppUser::searchCompanyPrivilege(37335715, $privileges));

$menu = array(
    array(
        'text' => 'Dashboard',
        'icon' => '<i class="icon-home"></i>',
        'link' => Yii::app()->createUrl('/site/index'),
        'active' => true,
        'children' => array(),
        'roles' => array('employee', 'admin', 'super_admin', 'payroller'),
//        'access' => 1,
    ),
    array(
        'text' => 'Clock In/Out',
        'icon' => '<i class="icon-clock"></i>',
        'link' => '#',
        'active' => true,
        'children' => array(
            array(
                'text' => 'Set Clock',
                'icon' => '<i class="icon-hourglass"></i>',
                'link' => Yii::app()->createUrl('/company/companyShifts/timeinout'),
                'active' => true,
                'roles' => array('employee', 'admin'),
//                'access' => AppUser::searchCompanyPrivilege(34527276, $privileges),
            ),
            array(
                'text' => 'Clock Entries',
                'icon' => '<i class="fa fa-copy"></i>',
                'link' => Yii::app()->createUrl('/company/companyShifts/clockentries'),
                'active' => true,
                'roles' => array('employee', 'admin'),
//                'access' => AppUser::searchCompanyPrivilege(34527276, $privileges),
            ),
            array(
                'text' => 'Approval List',
                'icon' => '<i class="fa fa-check"></i>',
                'link' => Yii::app()->createUrl('/company/companyShifts/approvallist'),
                'active' => true,
                'roles' => array('admin', 'employee'),
//                'access' => AppUser::searchCompanyPrivilege(49312120, $privileges),
            ),
            array(
                'text' => 'My Report',
                'icon' => '<i class="fa fa-file-o"></i>',
                'link' => Yii::app()->createUrl('/company/companyShifts/timesheetreport'),
                'active' => true,
                'roles' => array('admin', 'employee'),
//                'access' => AppUser::searchCompanyPrivilege(37335715, $privileges),
            ),
            array(
                'text' => 'General Report',
                'icon' => '<i class="fa fa-file"></i>',
                'link' => Yii::app()->createUrl('/company/companyShifts/timesheetreport?Clock_In/Out'),
                'active' => true,
                'roles' => array('admin'),
//                'access' => AppUser::searchCompanyPrivilege(33569985, $privileges),
            ),
        ),
        'roles' => array('employee', 'admin',),
//        'access' => AppUser::showMainMenu(array(34527276, 37335715, 33569985, 49312120), $privileges),
    ),
    array(
        'text' => 'Company Shifts',
        'icon' => '<i class="fa fa-sitemap"></i>',
        'link' => '#',
        'active' => true,
        'children' => array(
            array(
                'text' => 'Create Shift',
                'icon' => '<i class="fa fa-plus"></i>',
                'link' => Yii::app()->createUrl('/company/companyShifts/create'),
                'active' => true,
                'roles' => array('admin'),
//                'access' => AppUser::searchCompanyPrivilege(9583481, $privileges),
            ),
            array(
                'text' => 'View All',
                'icon' => '<i class="fa fa-copy"></i>',
                'link' => Yii::app()->createUrl('/company/companyShifts/'),
                'active' => true,
                'roles' => array('admin'),
//                'access' => AppUser::searchCompanyPrivilege(51093196, $privileges),
            ),
            array(
                'text' => 'View Shift Users',
                'icon' => '<i class="fa fa-copy"></i>',
                'link' => Yii::app()->createUrl('/company/companyShifts/viewshiftusers'),
                'active' => true,
                'roles' => array('admin','employee'),
            )
        ),
        'roles' => array('employee', 'admin',),
//        'access' => AppUser::showMainMenu(array(9583481, 51093196), $privileges),
    ),
    array(
        'text' => '1 Way Timesheets',
        'icon' => '<i class="fa fa-calendar"></i>',
        'link' => '#',
        'active' => true,
        'children' => array(
            array(
                'text' => 'Add 1 Way Timesheet',
                'icon' => '<i class="fa fa-plus-square"></i>',
                'link' => Yii::app()->createUrl('/timesheet/quickTimesheet/create'),
                'active' => true,
                'roles' => array('employee', 'admin'),
//                'access' => AppUser::searchCompanyPrivilege(19291442, $privileges),
            ),
            array(
                'text' => 'Current',
                'icon' => '<i class="icon-arrow-right"></i>',
                'link' => Yii::app()->createUrl('/timesheet/quickTimesheet/index'),
                'active' => true,
                'roles' => array('admin', 'employee'),
//                'access' => AppUser::searchCompanyPrivilege(43526509, $privileges),
            ),
            array(
                'text' => 'Completed/Archives',
                'icon' => '<i class="icon-check"></i>',
                'link' => Yii::app()->createUrl('/timesheet/quickTimesheet/completed'),
                'active' => true,
                'roles' => array('employee', 'admin'),
//                'access' => AppUser::searchCompanyPrivilege(11243068, $privileges),
            ),
        ),
        'roles' => array('employee', 'admin',),
//        'access' => AppUser::showMainMenu(array(11243068, 43526509, 19291442), $privileges),
    ),
    array(
        'text' => 'Resource Deployment',
        'icon' => '<i class="fa fa-calendar"></i>',
        'link' => '#',
        'active' => true,
        'children' => array(
            array(
                'text' => 'Set up Deployment',
                'icon' => '<i class="fa fa-plus-square"></i>',
                'link' => Yii::app()->createUrl('/contract/main/contract'),
                'active' => true,
                'roles' => array('admin'),
//                'access' => AppUser::searchCompanyPrivilege(13245398, $privileges),
            ),
            array(
                'text' => 'Current',
                'icon' => '<i class="icon-arrow-right"></i>',
                'link' => Yii::app()->createUrl('/timesheet/main/index/id/4/current/1'),
                'active' =>true,
                'roles' => array('admin'),
            ),
            array(
                'text' => 'Archives',
                'icon' => '<i class="icon-arrow-right"></i>',
                'link' => Yii::app()->createUrl('/timesheet/main/index/id/4/current/0'),
                'active' =>true,
                'roles' => array('admin'),
            ),
            array(
                'text' => 'Current Timecard',
                'icon' => '<i class="icon-arrow-right"></i>',
                'link' => Yii::app()->createUrl('/timesheet/main/currenttimecard/id/4/current/1'),
                'active' =>true,
                'roles' => array('admin'),
            ),
      ),
        'roles' => array('admin'),
//        'access' => AppUser::showMainMenu(array(19291442, 13245398, 22809635, 46112685, 42401952, 13245398, 17323981), $privileges),
    ),
//    array(
//        'text' => '2 Way Timesheets',
//        'icon' => '<i class="fa fa-calendar"></i>',
//        'link' => '#',
//        'active' => true,
//        'children' => array(
//            array(
//                'text' => 'Set 2 Way Contract',
//                'icon' => '<i class="icon-map"></i>',
//                'link' => Yii::app()->createUrl('/contract/main/new/id/1'),
//                'active' => true,
//                'roles' => array('admin'),
////                'access' => AppUser::searchCompanyPrivilege(20363768, $privileges),
//            ),
//            array(
//                'text' => 'Add 2 Way Timesheet',
//                'icon' => '<i class="fa fa-plus-square"></i>',
//                'link' => Yii::app()->createUrl('/timesheet/main/addwizard/types/out'),
//                'active' => true,
//                'roles' => array('employee', 'admin',),
////                'access' => AppUser::searchCompanyPrivilege(19879092, $privileges),
//            ),
//            array(
//                'text' => 'Current',
//                'icon' => '<i class="icon-arrow-right"></i>',
//                'link' => Yii::app()->createUrl('/timesheet/main/index/id/1/current/1?Two_Way'),
//                'active' => true,
//                'roles' => array('admin', 'employee'),
////                'access' => AppUser::searchCompanyPrivilege(20363757, $privileges),
//            ),
//            array(
//                'text' => 'Completed/Archives',
//                'icon' => '<i class="fa fa-check-circle"></i>',
//                'link' => Yii::app()->createUrl('/timesheet/main/index/id/1/current/0'),
//                'active' => true,
//                'roles' => array('employee', 'admin'),
////                'access' => AppUser::searchCompanyPrivilege(15679447, $privileges),
//            ),
//            array(
//                'text' => 'Current 2 Way Timecard',
//                'icon' => '<i class="fa fa-plus-square"></i>',
//                'link' => Yii::app()->createUrl('/timesheet/main/currenttimecard/id/1/current/1?Two_Way'),
//                'active' => true,
//                'roles' => array('employee', 'admin',),
////                'access' => AppUser::searchCompanyPrivilege(19879092, $privileges),
//            ),
//        ),
//        'roles' => array('admin', 'employee'),
////        'access' => AppUser::showMainMenu(array(15679447, 20363757, 19879092, 20363768, 29495482), $privileges),
//    ),
//    array(
//        'text' => '3 Way Timesheets',
//        'icon' => '<i class="fa fa-calendar"></i>',
//        'link' => '#',
//        'active' => true,
//        'children' => array(
//            array(
//                'text' => 'Set 3 Way Contract',
//                'icon' => '<i class="icon-map"></i>',
//                'link' => Yii::app()->createUrl('/contract/main/new/id/2?Three_Way'),
//                'active' => true,
//                'roles' => array('admin'),
////                'access' => AppUser::searchCompanyPrivilege(44105321, $privileges),
//            ),
//            array(
//                'text' => 'Add 3 Way Timesheet',
//                'icon' => '<i class="fa fa-plus-square"></i>',
//                'link' => Yii::app()->createUrl('/timesheet/main/addwizard/types/out_b'),
//                'active' => true,
//                'roles' => array('employee', 'admin',),
////                'access' => AppUser::searchCompanyPrivilege(14592227, $privileges),
//            ),
//            array(
//                'text' => 'Current',
//                'icon' => '<i class="icon-arrow-right"></i>',
//                'link' => Yii::app()->createUrl('/timesheet/main/index/id/2/current/1'),
//                'active' => true,
//                'roles' => array('admin', 'employee'),
////                'access' => AppUser::searchCompanyPrivilege(53223032, $privileges),
//            ),
//            array(
//                'text' => 'Current 3 Way Timecard',
//                'icon' => '<i class="icon-arrow-right"></i>',
//                'link' => Yii::app()->createUrl('/timesheet/main/currenttimecard/id/2/current/1?Three_Way'),
//                'active' => true,
//                'roles' => array('admin', 'employee'),
////                'access' => AppUser::searchCompanyPrivilege(53223032, $privileges),
//            ),
//            array(
//                'text' => 'Completed/Archives',
//                'icon' => '<i class="fa fa-check-circle"></i>',
//                'link' => Yii::app()->createUrl('/timesheet/main/index/id/2/current/0'),
//                'active' => true,
//                'roles' => array('employee', 'admin'),
////                'access' => AppUser::searchCompanyPrivilege(41803431, $privileges),
//            ),
//        ),
//        'roles' => array('admin', 'employee'),
////        'access' => AppUser::showMainMenu(array(41803431, 53223032, 14592227, 44105321), $privileges),
//    ),
    array(
        'text' => 'Non Billable Timesheet',
        'icon' => '<i class="fa fa-building"></i>',
        'link' => '#',
        'active' => true,
        'children' => array(
            array(
                'text' => 'Add New Timesheet',
                'icon' => '<i class="fa fa-plus"></i>',
                'link' => (AppUser::getUserType() == 'admin' || AppUser::getUserType() == 'employee' ? Yii::app()->createUrl('/timesheet/main/addwizard/types/in?Non_Billable') /* Yii::app()->createUrl('/timesheet/main/add/contract/' . $internal_contract->id) */ : ''),
                'active' => true,
                'roles' => array('employee', 'admin'),
//                'access' => AppUser::searchCompanyPrivilege(23484903, $privileges),
            ),
            array(
                'text' => 'Current',
                'icon' => '<i class="icon-arrow-right"></i>',
                'link' => Yii::app()->createUrl('/timesheet/main/index'),
                'active' => true,
                'roles' => array('admin', 'employee'),
//                'access' => AppUser::searchCompanyPrivilege(34324662, $privileges),
            ),
            array(
                'text' => 'Completed/Archives',
                'icon' => '<i class="icon-check"></i>',
                'link' => Yii::app()->createUrl('/timesheet/main/archives'),
                'active' => true,
                'roles' => array('employee', 'admin'),
//                'access' => AppUser::searchCompanyPrivilege(38463626, $privileges),
            ),
        ),
        'roles' => array('employee', 'admin',),
//        'access' => AppUser::showMainMenu(array(38463626, 34324662, 23484903), $privileges),
    ),
    array(
        'text' => 'Notifications',
        'icon' => '<i class="fa fa-bell-o"></i>',
        'link' => '#',
        'active' => true,
        'children' => array(
            array(
                'text' => 'Send New Message',
                'icon' => '<i class="fa fa-plus"></i>',
                'link' => Yii::app()->createUrl('/notification/main/companynotification'),
                'active' => true,
                'roles' => array('admin'),
//                'access' => AppUser::searchCompanyPrivilege(23484903, $privileges),
            ),
            array(
                'text' => 'View All Notifications',
                'icon' => '<i class="icon-arrow-right"></i>',
                'link' => Yii::app()->createUrl('/notification/main/index'),
                'active' => true,
                'roles' => array('admin', 'employee'),
//                'access' => AppUser::searchCompanyPrivilege(34324662, $privileges),
            ),
        ),
        'roles' => array('employee','admin',),
//        'access' => AppUser::showMainMenu(array(38463626, 34324662, 23484903), $privileges),
    ),
    array(
        'text' => 'Timesheet Approvals',
        'icon' => '<i class="icon-check"></i>',
        'link' => '#',
        'active' => true,
        'children' => array(
            array(
                'text' => 'Awaiting Approvals',
                'icon' => '<i class="icon-home"></i>',
                'link' => Yii::app()->createUrl('/timesheet/approve'),
                'active' => true,
                'roles' => array('employee', 'admin'),
//                'access' => AppUser::searchCompanyPrivilege(11833496, $privileges),
            ),
            array(
                'text' => 'Archives',
                'icon' => '<i class="icon-tag"></i>',
                'link' => Yii::app()->createUrl('/timesheet/main/index/id/3'),
                'active' => true,
                'roles' => array('admin', 'employee'),
//                'access' => AppUser::searchCompanyPrivilege(19552026, $privileges),
            ),
        ),
        'roles' => AppUser::canAddSpeedyTimesheet(array('admin')),
//        'access' => AppUser::showMainMenu(array(19552026, 11833496), $privileges),
    ),
    array(
        'text' => 'ePayslips',
        'icon' => '<i class="fa fa-file-text"></i>',
        'link' => Yii::app()->createUrl('/payroller/payslip/main'),
        'active' => true,
        'roles' => AppUser::canUseTemplateAssignedThings(array('admin')),
//        'access' => AppUser::showMainMenu(array(115540, 3316730), $privileges),
        'children' => array(
            array(
                'text' => 'ePayslips Pending',
                'icon' => '<i class="fa fa-file-o"></i>',
                'link' => Yii::app()->createUrl('/payroller/main/ppayrolls'),
                'active' => true,
                'roles' => array('employee', 'admin'),
//                'access' => AppUser::searchCompanyPrivilege(115540, $privileges),
            ),
            array(
                'text' => 'ePayslips Archives',
                'icon' => '<i class="fa fa-check-circle"></i>',
                'link' => Yii::app()->createUrl('/payroller/main/cpayrolls'),
                'active' => true,
                'roles' => array('admin', 'employee'),
//                'access' => AppUser::searchCompanyPrivilege(3316730, $privileges),
            ),
        ),
    ),
    array(
        'text' => 'Staff Management',
        'icon' => '<i class="icon-users"></i>',
        'link' => '#',
        'active' => true,
        'children' => array(
            array(
                'text' => 'View All',
                'icon' => '<i class="fa fa-file-text"></i>',
                'link' => Yii::app()->createUrl('/user/employee'),
                'active' => true,
                'roles' => array('employee', 'admin'),
//                'access' => AppUser::searchCompanyPrivilege(5452318, $privileges),
            ),
//            array(
//                'text' => 'Invite',
//                'icon' => '<i class="fa fa-angle-double-right"></i>',
//                'link' => Yii::app()->createUrl('/user/main/invite'),
//                'active' => true,
//                'roles' => array('admin', 'employee'),
//            ),
            array(
                'text' => 'Add Staff',
                'icon' => '<i class="fa fa-user-plus"></i>',
                'link' => Yii::app()->createUrl('/user/employee/add'),
                'active' => true,
                'roles' => array('employee', 'admin'),
//                'access' => AppUser::searchCompanyPrivilege(42210626, $privileges),
            ),
            array(
                'text' => 'Add Multiple',
                'icon' => '<i class="fa fa-users"></i>',
                'link' => Yii::app()->createUrl('/user/employee/addmultiple'),
                'active' => true,
                'roles' => array('employee', 'admin'),
//                'access' => AppUser::searchCompanyPrivilege(8415317, $privileges),
            ),
            array(
                'text' => 'Accounts(Birds Eye-View)',
                'icon' => '<i class="fa fa-eye"></i>',
                'link' => Yii::app()->createUrl('/contract/main/index'),
                'active' => true,
                'roles' => array('admin', 'employee'),
//                'access' => AppUser::searchCompanyPrivilege(16657762, $privileges),
            ),
//            array(
//                'text' => 'Hours Report',
//                'icon' => '<i class="icon-hourglass"></i>',
//                'link' => '#',
//                'active' => true,
//                'roles' => array('employee', 'admin'),
////                'access' => AppUser::searchCompanyPrivilege(0, $privileges),
//            ),
//            array(
//                'text' => 'Income Report',
//                'icon' => '<i class="fa fa-angle-double-right"></i>',
//                'link' => '#',
//                'active' => true,
//                'roles' => array('employee', 'admin'),
////                'access' => AppUser::searchCompanyPrivilege(0, $privileges),
//            ),
//            array(
//                'text' => 'Expense Report',
//                'icon' => '<i class="fa fa-angle-double-right"></i>',
//                'link' => '#',
//                'active' => true,
//                'roles' => array('employee', 'admin'),
////                'access' => AppUser::searchCompanyPrivilege(0, $privileges),
//            ),
        ),
        'roles' => array('admin'),
//        'access' => AppUser::showMainMenu(array(16657762, 8415317, 42210626, 5452318), $privileges),
    ),
//    array(
//        'text' => 'Admin',
//        'icon' => '<i class="fa  fa-user"></i>',
//        'link' => '#',
//        'active' => true,
//        'children' => array(
//            array(
//                'text' => 'New Invoice',
//                'icon' => '<i class="fa fa-plus"></i>',
//                'link' => Yii::app()->createUrl('/invoice/main/sendinvoice?Admin'),
//                'active' => true,
//                'roles' => array('admin'),
////                'access' => AppUser::searchCompanyPrivilege(46112685, $privileges),
//            ),

//            array(
//                'text' => 'Invoice Inbox',
//                'icon' => '<i class="fa fa-envelope"></i>',
//                'link' => '#',
//                'active' => true,
//                'roles' => array('admin'),
//                'access' => AppUser::searchCompanyPrivilege(10854725, $privileges),
//            ),
            
//            array(
//                'text' => 'Company Profile',
//                'icon' => '<i class="fa fa-angle-double-right"></i>',
//                'link' => Yii::app()->createUrl('/company/main/profile'),
//                'active' => true,
//                'roles' => array('admin'),
////                'access' => AppUser::searchCompanyPrivilege(17323981, $privileges),
//            ),
//        ),
//        'roles' => array('admin'),
////        'access' => AppUser::showMainMenu(array(17323981, 10854724, 10854725, 46112685, 42401952), $privileges),
//    ),
    array(
        'text' => 'Payroll Accounts',
        'icon' => '<i class="fa fa-table"></i>',
        'link' => '#',
        'active' => true,
        'children' => array(
            array(
                'text' => 'Companies',
                'icon' => '<i class="fa fa-angle-double-right"></i>',
                'link' => Yii::app()->createUrl('/payroller/main/company'),
                'active' => true,
                'roles' => array('payroller'),
//                'access' => AppUser::searchCompanyPrivilege(51711404, $privileges),
            ),
            array(
                'text' => 'Employees',
                'icon' => '<i class="fa fa-angle-double-right"></i>',
                'link' => Yii::app()->createUrl('/payroller/main/employee'),
                'active' => true,
                'roles' => array('payroller'),
//                'access' => AppUser::searchCompanyPrivilege(42792095, $privileges),
            ),
        ),
        'roles' => array('payroller'),
//        'access' => AppUser::showMainMenu(array(42792095, 51711404), $privileges),
    ),
    array(
        'text' => 'Payslips',
        'icon' => '<i class="fa fa-table"></i>',
        'link' => '#',
        'active' => true,
        'children' => array(
            array(
                'text' => 'PaySlip Archives',
                'icon' => '<i class="fa fa-angle-double-right"></i>',
                'link' => Yii::app()->createUrl('/payroller/payslip/request'),
                'active' => true,
                'roles' => array('payroller'),
//                'access' => AppUser::searchCompanyPrivilege(39367122, $privileges),
            ),
            array(
                'text' => 'Issue Payslip',
                'icon' => '<i class="fa fa-angle-double-right"></i>',
                'link' => Yii::app()->createUrl('/payroller/payslip/bulkupload'),
                'active' => true,
                'roles' => array('payroller'),
//                'access' => AppUser::searchCompanyPrivilege(39630659, $privileges),
            ),
        ),
        'roles' => array('payroller'),
//        'access' => AppUser::showMainMenu(array(39630659, 39367122), $privileges),
    ),
    array(
        'text' => 'SpeedyPayrolls',
        'icon' => '<i class="fa fa-table"></i>',
        'link' => '#',
        'active' => AppUser::canPayroll(),
//        'access' => AppUser::showMainMenu(array(19319456, 28349699, 28349699), $privileges),
        'children' => array(
            array(
                'text' => 'View All',
                'icon' => '<i class="fa fa-angle-double-right"></i>',
                'link' => Yii::app()->createUrl('/payroller/main/AdminEmployee'),
                'active' => true,
                'roles' => array('admin'),
//                'access' => AppUser::searchCompanyPrivilege(28349699, $privileges),
            ),
            array(
                'text' => 'View All',
                'icon' => '<i class="fa fa-angle-double-right"></i>',
                'link' => Yii::app()->createUrl('/payroller/main/viewpayrolls'),
                'active' => true,
                'roles' => array('employee'),
//                'access' => AppUser::searchCompanyPrivilege(28349699, $privileges),
            ),
            array(
                'text' => 'Dispatch Payroll',
                'icon' => '<i class="fa fa-angle-double-right"></i>',
                'link' => Yii::app()->createUrl('/payroller/main/dispatchPayroll?SpeedyPayrolls'),
                'active' => true,
                'roles' => array('admin'),
//                'access' => AppUser::searchCompanyPrivilege(19319456, $privileges),
            ),
        ),
        'roles' => array('admin', 'employee'),
    ),
    array(
        'text' => 'Admin Menu',
        'icon' => '<i class="fa fa-table"></i>',
        'link' => '#',
        'active' => true,
        'children' => array(
            array(
                'text' => 'Users Management',
                'icon' => '<i class="fa fa-users"></i>',
                'link' => Yii::app()->createUrl('/admin/default/'),
                'active' => true,
                'roles' => array('super_admin'),
//                'access' => AppUser::searchCompanyPrivilege(0, $privileges),
            ),
            array(
                'text' => 'Companies Management',
                'icon' => '<i class="fa fa-building-o"></i>',
                'link' => Yii::app()->createUrl('/company/main/admin'),
                'active' => true,
                'roles' => array('super_admin'),
//                'access' => AppUser::searchCompanyPrivilege(0, $privileges),
            ),
            array(
                'text' => 'Contacts Management',
                'icon' => '<i class="fa fa-user"></i>',
                'link' => Yii::app()->createUrl('/company/contact/admin'),
                'active' => true,
                'roles' => array('super_admin'),
//                'access' => AppUser::searchCompanyPrivilege(0, $privileges),
            ),
            array(
                'text' => 'Contracts Management',
                'icon' => '<i class="fa fa-files-o"></i>',
                'link' => Yii::app()->createUrl('/contract/main/admin'),
                'active' => true,
                'roles' => array('super_admin'),
//                'access' => AppUser::searchCompanyPrivilege(0, $privileges),
            ),
            array(
                'text' => 'Company Permissions',
                'icon' => '<i class="fa fa-check-square-o"></i>',
                'link' => Yii::app()->createUrl('/company/main/usagepermission'),
                'active' => true,
                'roles' => array('super_admin'),
//                'access' => AppUser::searchCompanyPrivilege(0, $privileges),
            ),
            array(
                'text' => 'Payroller Permission',
                'icon' => '<i class="fa fa-check-square-o"></i>',
                'link' => Yii::app()->createUrl('/payroller/main/bulkpermission'),
                'active' => true,
                'roles' => array('super_admin'),
//                'access' => AppUser::searchCompanyPrivilege(0, $privileges),
            ),
        ),
        'roles' => array('super_admin'),
//        'access' => AppUser::showMainMenu(array(0), $privileges),
    ),
    array(
        'text' => 'Templates',
        'icon' => '<i class="fa fa-tasks"></i>',
        'link' => '#',
        'active' => true,
        'children' => array(
            array(
                'text' => 'Add New',
                'icon' => '<i class="fa fa-plus"></i>',
                'link' => Yii::app()->createUrl('/template/main/create'),
                'active' => true,
                'roles' => array('super_admin'),
//                'access' => AppUser::searchCompanyPrivilege(0, $privileges),
            ),
            array(
                'text' => 'View All',
                'icon' => '<i class="fa fa-copy"></i>',
                'link' => Yii::app()->createUrl('/template/main/index'),
                'active' => true,
                'roles' => array('super_admin'),
//                'access' => AppUser::searchCompanyPrivilege(0, $privileges),
            ),
            array(
                'text' => 'Apply Timesheet Template',
                'icon' => '<i class="fa fa-arrow-circle-right"></i>',
                'link' => Yii::app()->createUrl('/template/main/apply'),
                'active' => true,
                'roles' => array('super_admin'),
//                'access' => AppUser::searchCompanyPrivilege(0, $privileges),
            ),
        ),
        'roles' => array('super_admin'),
//        'access' => AppUser::showMainMenu(array(0), $privileges),
    ),
    array(
        'text' => 'Help Managment',
        'icon' => '<i class="fa fa-question-circle"></i>',
        'link' => '#',
        'active' => true,
        'children' => array(
            array(
                'text' => 'Add New',
                'icon' => '<i class="fa fa-plus"></i>',
                'link' => Yii::app()->createUrl('/help/main/create'),
                'active' => true,
                'roles' => array('super_admin'),
//                'access' => AppUser::searchCompanyPrivilege(0, $privileges),
            ),
            array(
                'text' => 'View All',
                'icon' => '<i class="fa fa-copy"></i>',
                'link' => Yii::app()->createUrl('/help/main/index'),
                'active' => true,
                'roles' => array('super_admin'),
//                'access' => AppUser::searchCompanyPrivilege(0, $privileges),
            ),
        ),
        'roles' => array('super_admin'),
//        'access' => AppUser::showMainMenu(array(0), $privileges),
    ),
    array(
        'text' => 'Billing',
        'icon' => '<i class="fa  fa-dollar "></i>',
        'link' => '#',
        'active' => true,
        'children' => array(
            array(
                'text' => 'View All Companies',
                'icon' => '<i class="fa fa-th-list"></i>',
                'link' => Yii::app()->createUrl('/billing/main/index'),
                'active' => true,
                'roles' => array('super_admin'),
//                'access' => AppUser::searchCompanyPrivilege(0, $privileges),
            ),
            array(
                'text' => 'View All',
                'icon' => '<i class="fa fa-th-list"></i>',
                'link' => Yii::app()->createUrl('/template/main/index'),
                'active' => false,
                'roles' => array('super_admin'),
//                'access' => AppUser::searchCompanyPrivilege(0, $privileges),
            ),
            array(
                'text' => 'Apply Timesheet Template',
                'icon' => '<i class="fa fa-angle-double-right"></i>',
                'link' => Yii::app()->createUrl('/template/main/apply'),
                'active' => false,
                'roles' => array('super_admin'),
//                'access' => AppUser::searchCompanyPrivilege(0, $privileges),
            ),
        ),
        'roles' => array('super_admin'),
//        'access' => AppUser::showMainMenu(array(0), $privileges),
    ),
    array(
        'text' => 'Plan & Privileges',
        'icon' => '<i class="fa fa-table"></i>',
        'link' => '#',
        'active' => true,
        'children' => array(
            array(
                'text' => 'Add New Plan',
                'icon' => '<i class="fa fa-plus"></i>',
                'link' => Yii::app()->createUrl('/priveledge/plan/create'),
                'active' => true,
                'roles' => array('super_admin'),
//                'access' => AppUser::searchCompanyPrivilege(0, $privileges),
            ),
            array(
                'text' => 'View All Plans',
                'icon' => '<i class="fa fa-angle-double-right"></i>',
                'link' => Yii::app()->createUrl('/priveledge/plan/index'),
                'active' => true,
                'roles' => array('super_admin'),
//                'access' => AppUser::searchCompanyPrivilege(0, $privileges),
            ),
            array(
                'text' => 'Assign Privileges to Plan',
                'icon' => '<i class="fa fa-check-square-o"></i>',
                'link' => Yii::app()->createUrl('/priveledge/plan/addprivilege'),
                'active' => true,
                'roles' => array('super_admin'),
//                'access' => AppUser::searchCompanyPrivilege(0, $privileges),
            ),
            array(
                'text' => 'Update Company Plan',
                'icon' => '<i class="icon-arrow-right"></i>',
                'link' => Yii::app()->createUrl('/priveledge/plan/editcompanyplan'),
                'active' => true,
                'roles' => array('super_admin'),
//                'access' => AppUser::searchCompanyPrivilege(0, $privileges),
            ),
            array(
                'text' => 'View All Privileges',
                'icon' => '<i class="fa fa-copy"></i>',
                'link' => Yii::app()->createUrl('/priveledge/priveledge/'),
                'active' => true,
                'roles' => array('super_admin'),
//                'access' => AppUser::searchCompanyPrivilege(0, $privileges),
            ),
            array(
                'text' => 'Company Privileges',
                'icon' => '<i class="fa fa-check-square-o"></i>',
                'link' => Yii::app()->createUrl('/priveledge/priveledge/assignprivilege'),
                'active' => true,
                'roles' => array('super_admin'),
//                'access' => AppUser::searchCompanyPrivilege(0, $privileges),
            ),
//            array(
//                'text' => 'Manage Privilege',
//                'icon' => '<i class="fa fa-angle-double-right"></i>',
//                'link' => Yii::app()->createUrl('/priveledge/priveledge/admin'),
//                'active' => true,
//                'roles' => array('super_admin'),
//                'access' => AppUser::searchCompanyPrivilege(0, $privileges),
//            ),
        ),
        'roles' => array('super_admin'),
//        'access' => AppUser::showMainMenu(array(0), $privileges),
    ),
    array(
        'text' => 'Account',
        'icon' => '<i class="fa fa-users"></i>',
        'link' => '#',
        'active' => true,
        'children' => array(
            array(
                'text' => 'Admin Profile',
                'icon' => '<i class="fa fa-user"></i>',
                'link' => Yii::app()->createUrl('/user/main/view'),
                'active' => true,
                'roles' => array('admin'),
//                'access' => AppUser::searchCompanyPrivilege(13245398, $privileges),
            ),
            array(
                'text' => 'Branches',
                'icon' => '<i class="fa fa-angle-double-right"></i>',
                'link' => Yii::app()->createUrl('/company/branch'),
                'active' => true,
                'roles' => array('admin'),
//                'access' => AppUser::searchCompanyPrivilege(42401952, $privileges),
            ),
            array(
                'text' => 'Preferences',
                'icon' => '<i class="fa fa-angle-double-right"></i>',
                'link' => Yii::app()->createUrl('/company/main/preferences'),
                'active' => true,
                'roles' => array('admin'),
//                'access' => AppUser::searchCompanyPrivilege(42401952, $privileges),
            ),
            array(
                'text' => 'Invoices',
                'icon' => '<i class="fa fa-angle-double-right"></i>',
                'link' => Yii::app()->createUrl('/invoice/main/index'),
                'active' => true,
                'roles' => array('admin'),
//                'access' => AppUser::searchCompanyPrivilege(10854724, $privileges),
            ),
            array(
                'text' => 'Company Profile',
                'icon' => '<i class="fa fa-angle-double-right"></i>',
                'link' => Yii::app()->createUrl('/payroller/main/profile'),
                'active' => true,
                'roles' => array('payroller'),
//                'access' => AppUser::searchCompanyPrivilege(17323981, $privileges),
            ),
            array(
                'text' => 'Company Profile',
                'icon' => '<i class="fa fa-angle-double-right"></i>',
                'link' => Yii::app()->createUrl('/company/main/profile'),
                'active' => true,
                'roles' => array('admin'),
//                'access' => AppUser::searchCompanyPrivilege(17323981, $privileges),
            ),
            
//            array(
//                'text' => 'Preferences',
//                'icon' => '<i class="fa fa-angle-double-right"></i>',
//                'link' => Yii::app()->createUrl('/company/main/preferences'),
//                'active' => true,
//                'roles' => array('admin'),
////                'access' => AppUser::searchCompanyPrivilege(42401952, $privileges),
//            ),
            array(
                'text' => 'Contact List',
                'icon' => '<i class="fa fa-copy"></i>',
                'link' => Yii::app()->createUrl('/company/contact'),
                'active' => true,
                'roles' => array('admin'),
//                'access' => AppUser::searchCompanyPrivilege(22809635, $privileges),
            ),
//            array(
//                'text' => 'Create New Invoice',
//                'icon' => '<i class="fa fa-plus"></i>',
//                'link' => Yii::app()->createUrl('/invoice/main/sendinvoice?Admin'),                
//                'active' => true,
//                'roles' => AppUser::canUseTemplateAssignedThings(array('admin')),
//                'access' => AppUser::searchCompanyPrivilege(46112685, $privileges),
//            ),
//            array(
//                'text' => 'Invoice Archives',
//                'icon' => '<i class="fa fa-angle-double-right"></i>',
//                'link' => Yii::app()->createUrl('/invoice/main'),
//                'active' => true,
//                'roles' => AppUser::canUseTemplateAssignedThings(array('admin')),
////                'access' => AppUser::searchCompanyPrivilege(19291442, $privileges),
//            ),
        ),
        'roles' => array('admin', 'payroller'),
//        'access' => AppUser::showMainMenu(array(19291442, 13245398, 22809635, 46112685, 42401952, 13245398, 17323981), $privileges),
    ),
    
//    array(
//        'text' => 'Quick Menu',
//        'icon' => '<i class="fa fa-hand-o-down"></i>',
//        'link' => '#',
//        'active' => true,
//        'children' => array(
//            array(
//                'text' => 'Add New Timesheet',
//                'icon' => '<i class="fa fa-circle-o text-info"></i>',
//                'link' => Yii::app()->createUrl('/timesheet/main/addWizard'),
//                'active' => true,
//                'roles' => AppUser::canUseTemplateAssignedThings(array('employee')),
//            ),
//            array(
//                'text' => 'View Payroll',
//                'icon' => '<i class="fa fa-circle-o text-info"></i>',
//                'link' => Yii::app()->createUrl('/timesheet/approve'),
//                'active' => true,
//                'roles' => AppUser::canUseTemplateAssignedThings(array('employee')),
//            ),
////            array(
////                'text' => 'Contact List',
////                'icon' => '<i class="fa fa-angle-double-right"></i>',
////                'link' => Yii::app()->createUrl('/company/contact'),
////                'active' => true,
////                'roles' => array('employee',),
////            ),
//             array(
//                'text' => 'Add New Timesheet',
//                'icon' => '<i class="fa fa-circle-o text-danger"></i>',
//                'link' => Yii::app()->createUrl('/timesheet/main/addWizard'),
//                'active' => true,
//                'roles' => AppUser::canUseTemplateAssignedThings(array('admin')),
//            ),
//            array(
//                'text' => 'View Payroll',
//                'icon' => '<i class="fa fa-circle-o text-danger"></i>',
//                'link' => Yii::app()->createUrl('/timesheet/approve'),
//                'active' => true,
//                'roles' => AppUser::canUseTemplateAssignedThings(array('admin')),
//            ),
//            array(
//                'text' => 'Add employee',
//                'icon' => '<i class="fa fa-circle-o text-danger"></i>',
//                'link' => Yii::app()->createUrl('/user/employee/add'),
//                'active' => true,
//                'roles' => AppUser::canUseTemplateAssignedThings(array('admin')),
//            ),
//            array(
//                'text' => 'View all',
//                'icon' => '<i class="fa fa-circle-o text-danger"></i>',
//                'link' => Yii::app()->createUrl('/user/employee/'),
//                'active' => true,
//                'roles' => AppUser::canUseTemplateAssignedThings(array('admin')),
//            ),
//            array(
//                'text' => 'Add SpeedyTimesheet account',
//                'icon' => '<i class="fa fa-circle-o text-danger"></i>',
//                'link' => Yii::app()->createUrl('/contract/main/new'),
//                'active' => true,
//                'roles' => AppUser::canAddSpeedyTimesheet(array('admin')),
//            ),
//            array(
//                'text' => 'Create New Invoice',
//                'icon' => '<i class="fa fa-circle-o text-danger"></i>',
//                'link' => Yii::app()->createUrl('/invoice/main/sendinvoice'),
//                'active' => true,
//                'roles' => AppUser::canUseTemplateAssignedThings(array('admin')),
//            ),
//            array(
//                'text' => 'New Invoice',
//                'icon' => '<i class="fa fa-circle-o text-danger"></i>',
//                'link' => Yii::app()->createUrl('/invoice/main/sendinvoice'),
//                'active' => true,
//                'roles' => AppUser::canUseTemplateAssignedThings(array('admin')),
//            ),
//            array(
//                'text' => 'Invoice Archives',
//                'icon' => '<i class="fa fa-circle-o text-danger"></i>',
//                'link' => Yii::app()->createUrl('/invoice/main'),
//                'active' => true,
//                'roles' => AppUser::canUseTemplateAssignedThings(array('admin')),
//            ),
//            array(
//                'text' => 'Preferences',
//                'icon' => '<i class="fa fa-circle-o text-danger"></i>',
//                'link' => Yii::app()->createUrl('/company/main/preferences'),
//                'active' => true,
//                'roles' => array('admin'),
//            ),
//        ),
//        'roles' => array('admin', 'employee'),
//    ),
);
/* }
  elseif($current_user->plan_type==0)
  {
  $menu = array(
  array(
  'text' => 'Dashboard',
  'icon' => '<i class="fa fa-dashboard"></i>',
  'link' => Yii::app()->createUrl('/site/index'),
  'active' => true,
  'children' => array(),
  'roles' => array('employee', 'admin', 'super_admin', 'payroller'),
  ),
  array(
  'text' => 'Employees',
  'icon' => '<i class="fa fa-user"></i>',
  'link' => '#',
  'active' => true,
  'children' => array(
  array(
  'text' => 'Invite',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/user/main/invite'),
  'active' => true,
  'roles' => array('admin',),
  ),
  array(
  'text' => 'View all',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/user/employee/'),
  'active' => true,
  'roles' => array('admin',),
  ),
  array(
  'text' => 'Add employee',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/user/employee/add'),
  'active' => true,
  'roles' => array('admin',),
  ),
  array(
  'text' => 'Add multiple employees',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/user/employee/addmultiple'),
  'active' => true,
  'roles' => array('admin',),
  ),
  ),
  'roles' => array('admin',),
  ),
  array(
  'text' => 'Speedy Timesheets',
  'icon' => '<i class="fa fa-table"></i>',
  'link' => '#',
  'active' => true,
  'children' => array(
  array(
  'text' => 'Add new timesheet',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/timesheet/main/addWizard'),
  'active' => true,
  'roles' => array('employee', 'admin',),
  ),
  //            array(
  //                'text' => 'View all',
  //                'icon' => '<i class="fa fa-angle-double-right"></i>',
  //                'link' => Yii::app()->createUrl('/timesheet/main'),
  //                'active' => true,
  //                'roles' => array('employee','super_admin'),
  //            ),
  array(
  'text' => 'Accounts',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/contract/main/index'),
  'active' => true,
  'roles' => array('admin', 'employee',),
  ),
  array(
  'text' => 'Timesheet Approvals',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/timesheet/approve'),
  'active' => true,
  'roles' => array('employee', 'admin',),
  ),
  //            array(
  //                'text' => 'Add SpeedyTimesheet account',
  //                'icon' => '<i class="fa fa-angle-double-right"></i>',
  //                'link' => Yii::app()->createUrl('/contract/main/new'),
  //                'active' => true,
  //                'roles' => array('admin',),
  //            ),
  ),
  'roles' => array('employee', 'admin',),
  ),
  array(
  'text' => 'Oneway Timesheets',
  'icon' => '<i class="fa fa-table"></i>',
  'link' => '#',
  'active' => true,
  'children' => array(
  array(
  'text' => 'Add New',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/timesheet/quickTimesheet/create'),
  'active' => true,
  'roles' => array('employee', 'admin',),
  ),
  array(
  'text' => 'Current',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/timesheet/quickTimesheet/index'),
  'active' => true,
  'roles' => array('admin', 'employee',),
  ),
  array(
  'text' => 'Completed/Archives',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/timesheet/quickTimesheet/completed'),
  'active' => true,
  'roles' => array('employee', 'admin',),
  ),
  ),
  'roles' => array('employee', 'admin',),
  ),
  array(
  'text' => 'Payroll Accounts',
  'icon' => '<i class="fa fa-table"></i>',
  'link' => '#',
  'active' => true,
  'children' => array(
  array(
  'text' => 'Companies',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/payroller/main/company'),
  'active' => true,
  'roles' => array('payroller',),
  ),
  array(
  'text' => 'Employees',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/payroller/main/employee'),
  'active' => true,
  'roles' => array('payroller',),
  ),
  ),
  'roles' => array('payroller',),
  ),
  array(
  'text' => 'Payslips',
  'icon' => '<i class="fa fa-table"></i>',
  'link' => '#',
  'active' => true,
  'children' => array(
  array(
  'text' => 'PaySlip Archives',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/payroller/payslip/request'),
  'active' => true,
  'roles' => array('payroller'),
  ),
  array(
  'text' => 'Issue Payslip',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/payroller/payslip/bulkupload'),
  'active' => true,
  'roles' => array('payroller',),
  ),
  ),
  'roles' => array('payroller'),
  ),
  //    array(
  //        'text' => 'SpeedyPayrolls',
  //        'icon' => '<i class="fa fa-table"></i>',
  //        'link' => '#',
  //        'active' => AppUser::canPayroll(),
  //        'children' => array(
  //            array(
  //                'text' => 'View All',
  //                'icon' => '<i class="fa fa-angle-double-right"></i>',
  //                'link' => Yii::app()->createUrl('/payroller/main/AdminEmployee'),
  //                'active' => true,
  //                'roles' => array('admin'),
  //            ),
  //            array(
  //                'text' => 'View All',
  //                'icon' => '<i class="fa fa-angle-double-right"></i>',
  //                'link' => Yii::app()->createUrl('/payroller/main/viewpayrolls'),
  //                'active' => true,
  //                'roles' => array('employee'),
  //            ),
  //            array(
  //                'text' => 'Dispatch Payroll',
  //                'icon' => '<i class="fa fa-angle-double-right"></i>',
  //                'link' => Yii::app()->createUrl('/payroller/main/dispatchPayroll'),
  //                'active' => true,
  //                'roles' => array('admin'),
  //            ),
  //        ),
  //        'roles' => array('admin', 'employee'),
  //    ),
  array(
  'text' => 'Admin Menu',
  'icon' => '<i class="fa fa-table"></i>',
  'link' => '#',
  'active' => true,
  'children' => array(
  array(
  'text' => 'Users Management',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/admin/default/'),
  'active' => true,
  'roles' => array('super_admin'),
  ),
  array(
  'text' => 'Companies Management',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/company/main/admin'),
  'active' => true,
  'roles' => array('super_admin'),
  ),
  array(
  'text' => 'Contacts Management',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/company/contact/admin'),
  'active' => true,
  'roles' => array('super_admin'),
  ),
  array(
  'text' => 'Contracts Management',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/contract/main/admin'),
  'active' => true,
  'roles' => array('super_admin'),
  ),
  array(
  'text' => 'Company Permissions',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/company/main/usagepermission'),
  'active' => true,
  'roles' => array('super_admin'),
  ),
  array(
  'text' => 'Payroller Permission',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/payroller/main/bulkpermission'),
  'active' => true,
  'roles' => array('super_admin'),
  ),
  ),
  'roles' => array('super_admin'),
  ),
  array(
  'text' => 'Templates',
  'icon' => '<i class="fa fa-tasks"></i>',
  'link' => '#',
  'active' => true,
  'children' => array(
  array(
  'text' => 'Add New',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/template/main/create'),
  'active' => true,
  'roles' => array('super_admin'),
  ),
  array(
  'text' => 'View All',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/template/main/index'),
  'active' => true,
  'roles' => array('super_admin'),
  ),
  array(
  'text' => 'Apply Timesheet Template',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/template/main/apply'),
  'active' => true,
  'roles' => array('super_admin'),
  ),
  ),
  'roles' => array('super_admin'),
  ),
  array(
  'text' => 'Help Managment',
  'icon' => '<i class="fa fa-question-circle"></i>',
  'link' => '#',
  'active' => true,
  'children' => array(
  array(
  'text' => 'Add New',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/help/main/create'),
  'active' => true,
  'roles' => array('super_admin'),
  ),
  array(
  'text' => 'View All',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/help/main/index'),
  'active' => true,
  'roles' => array('super_admin'),
  ),
  ),
  'roles' => array('super_admin'),
  ),
  array(
  'text' => 'Billing',
  'icon' => '<i class="fa  fa-dollar "></i>',
  'link' => '#',
  'active' => true,
  'children' => array(
  array(
  'text' => 'View All Companies',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/billing/main/index'),
  'active' => true,
  'roles' => array('super_admin'),
  ),
  array(
  'text' => 'View All',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/template/main/index'),
  'active' => false,
  'roles' => array('super_admin'),
  ),
  array(
  'text' => 'Apply Timesheet Template',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/template/main/apply'),
  'active' => false,
  'roles' => array('super_admin'),
  ),
  ),
  'roles' => array('super_admin'),
  ),
  array(
  'text' => 'Account',
  'icon' => '<i class="fa fa-users"></i>',
  'link' => '#',
  'active' => true,
  'children' => array(
  array(
  'text' => 'Company Profile',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/payroller/main/profile'),
  'active' => true,
  'roles' => array('payroller'),
  ),
  array(
  'text' => 'Company Profile',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/company/main/profile'),
  'active' => true,
  'roles' => array('admin'),
  ),
  array(
  'text' => 'Admin Profile',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/user/main/view'),
  'active' => true,
  'roles' => array('admin'),
  ),
  array(
  'text' => 'Preferences',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/company/main/preferences'),
  'active' => true,
  'roles' => array('admin'),
  ),
  array(
  'text' => 'Contact List',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/company/contact'),
  'active' => true,
  'roles' => array('admin'),
  ),
  array(
  'text' => 'Create New Invoice',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/invoice/main/sendinvoice'),
  'active' => true,
  'roles' => array('admin'),
  ),
  array(
  'text' => 'Invoice Archives',
  'icon' => '<i class="fa fa-angle-double-right"></i>',
  'link' => Yii::app()->createUrl('/invoice/main'),
  'active' => true,
  'roles' => array('admin'),
  ),
  ),
  'roles' => array('admin', 'payroller'),
  ),
  array(
  'text' => 'Quick Menu',
  'icon' => '<i class="fa fa-hand-o-down"></i>',
  'link' => '#',
  'active' => true,
  'children' => array(
  array(
  'text' => 'Add New Timesheet',
  'icon' => '<i class="fa fa-circle-o text-info"></i>',
  'link' => Yii::app()->createUrl('/timesheet/main/addWizard'),
  'active' => true,
  'roles' => array('employee',),
  ),
  //            array(
  //                'text' => 'View Payroll',
  //                'icon' => '<i class="fa fa-circle-o text-info"></i>',
  //                'link' => Yii::app()->createUrl('/timesheet/approve'),
  //                'active' => true,
  //                'roles' => array('employee',),
  //            ),
  //            array(
  //                'text' => 'Contact List',
  //                'icon' => '<i class="fa fa-angle-double-right"></i>',
  //                'link' => Yii::app()->createUrl('/company/contact'),
  //                'active' => true,
  //                'roles' => array('employee',),
  //            ),
  //             array(
  //                'text' => 'Add New Timesheet',
  //                'icon' => '<i class="fa fa-circle-o text-danger"></i>',
  //                'link' => Yii::app()->createUrl('/timesheet/main/addWizard'),
  //                'active' => true,
  //                'roles' => array('admin',),
  //            ),
  //            array(
  //                'text' => 'View Payroll',
  //                'icon' => '<i class="fa fa-circle-o text-danger"></i>',
  //                'link' => Yii::app()->createUrl('/timesheet/approve'),
  //                'active' => true,
  //                'roles' => array('admin',),
  //            ),
  array(
  'text' => 'Add employee',
  'icon' => '<i class="fa fa-circle-o text-danger"></i>',
  'link' => Yii::app()->createUrl('/user/employee/add'),
  'active' => true,
  'roles' => array('admin',),
  ),
  array(
  'text' => 'View all',
  'icon' => '<i class="fa fa-circle-o text-danger"></i>',
  'link' => Yii::app()->createUrl('/user/employee/'),
  'active' => true,
  'roles' => array('admin',),
  ),
  //            array(
  //                'text' => 'Add SpeedyTimesheet account',
  //                'icon' => '<i class="fa fa-circle-o text-danger"></i>',
  //                'link' => Yii::app()->createUrl('/contract/main/new'),
  //                'active' => true,
  //                'roles' => array('admin',),
  //            ),
  array(
  'text' => 'Create New Invoice',
  'icon' => '<i class="fa fa-circle-o text-danger"></i>',
  'link' => Yii::app()->createUrl('/invoice/main/sendinvoice'),
  'active' => true,
  'roles' => array('admin'),
  ),
  array(
  'text' => 'New Invoice',
  'icon' => '<i class="fa fa-circle-o text-danger"></i>',
  'link' => Yii::app()->createUrl('/invoice/main/sendinvoice'),
  'active' => true,
  'roles' => array('admin'),
  ),
  array(
  'text' => 'Invoice Archives',
  'icon' => '<i class="fa fa-circle-o text-danger"></i>',
  'link' => Yii::app()->createUrl('/invoice/main'),
  'active' => true,
  'roles' => array('admin'),
  ),
  array(
  'text' => 'Preferences',
  'icon' => '<i class="fa fa-circle-o text-danger"></i>',
  'link' => Yii::app()->createUrl('/company/main/preferences'),
  'active' => true,
  'roles' => array('admin'),
  ),
  ),
  'roles' => array('admin', 'employee'),
  ),
  );
  } */
?>
<div class="page-sidebar md-shadow-z-2-i  navbar-collapse collapse">
    <!-- Sidebar user panel -->
    <!--
    <?php $current_user = AppUser::getCurrentUser(); ?>
    <?php
    if ($current_user->type == 1) {
        $current_company = AppUser::getUserCompany('', false);
        ?>
                <div class="user-panel" style="height: 58px;">
                    <div class="pull-left info"> <p style="color:#1582dc;"> Company <br></p>
                        <p><?php echo ucwords($current_company->name); ?> </p>
                    </div>
                </div>
        <?php
    } if ($current_user->type == 2) {
        $current_company = Payroller::model()->findByAttributes(array('payroller_id' => $current_user->id))
        ?>
                <div class="user-panel" style="height: 58px;">
                    <div class="pull-left info"><p style="color:#1582dc;">Company </p>
                        <p><?php echo isset($current_company->company_name)?ucwords($current_company->company_name):""; ?> </p>
                    </div>
                </div>
    <?php } ?>
    <div class="user-panel">
        <div class="pull-left image">
    <?php $user = User::model()->findByPk(AppUser::getUserId()); ?>
            <img src="<?php echo $user->photo ? Yii::app()->baseUrl . '/uploads/photos/' . $user->photo : Yii::app()->theme->baseUrl . '/img/default.png'; ?>" class="img-square" alt="User Image" />
        </div>
        <div class="pull-left info">
            <p><?php echo ucwords($user->first_name) . " " . ucwords($user->last_name); ?></p>
            <!--<i class="fa fa-circle text-success">Online</i>
        </div>
    </div>
    <!-- search form -->
    <!--    <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                <span class="input-group-btn">
                    <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>-->
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->

    <ul class="page-sidebar-menu " data-keep-expanded="true" data-auto-scroll="true" data-slide-speed="200">
        <?php foreach ($menu as $menuItem) { ?>
            <?php if ($menuItem['active']) { ?>
         <!--condition for privilege access && $menuItem['access'] == true-->
                <?php if (array_intersect($menuItem['roles'], array(AppUser::getUserType())) != NULL) { ?>
                    <?php foreach ($menuItem['children'] as $menuSubItem) { ?>
                        <?php if ($menuSubItem['active']) { ?>
                            <?php
                            $active = 0;
                            if (Yii::app()->request->url == $menuSubItem['link']) {
                                $active = 1;
                                break;
                            }
                            ?>  
                        <?php } ?>
                    <?php } ?>
                    <?php if (Yii::app()->request->url == $menuItem['link']) { ?>
                        <li class="start active">
                            <?php
                        } else if ($menuItem['children']) {
                            if ($active || $menuItem['text'] == "Quick Menu") {
                                ?>
                            <li class="treeview active">
                            <?php } else { ?>
                            <li class="treeview">
                                <?php
                            }
                        } else {
                            ?>
                        <li>
                        <?php } ?>
                        <a href="<?php echo $menuItem['link']; ?>">
                            <?php echo $menuItem['icon']; ?> <span class="title"><?php echo $menuItem['text']; ?></span>
                            <?php if ($menuItem['children']) { ?>
                                <span class="arrow "></span>
                            <?php } ?>
                        </a>
                        <?php if ($menuItem['children']) { ?>
                            <ul class="sub-menu">
                                <?php foreach ($menuItem['children'] as $menuSubItem) { ?>
                                 <!--condition for privilege access && $menuSubItem['access'] == true-->
                                    <?php if (array_intersect($menuSubItem['roles'], array(AppUser::getUserType())) != NULL) { ?>
                                        <?php if ($menuSubItem['active']) { ?>
                                            <?php if (Yii::app()->request->url == $menuSubItem['link']) { ?>
                                                <li class="start active">
                                                <?php } else { ?>
                                                <li>
                                                <?php } ?>
                                                <a href="<?php echo $menuSubItem['link']; ?>">
                                                    <?php echo $menuSubItem['icon']; ?> <?php echo $menuSubItem['text']; ?>
                                                </a>
                                            </li>    
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                            </ul>
                        <?php } ?>
                    </li>
                    
                <?php } ?>
            <?php } ?>
        <?php } ?>

                    <li class="timecard-menu">

                        <a href="#" ><i class="icon-user"></i> Profile<span class="arrow "></span></a>
                        <ul class="sub-menu">
                    <li>
                                <?php if (AppUser::isUserAdmin()) { ?>
                                    <a href="<?php echo Yii::app()->createUrl('user/main/view'); ?>" ><i class="icon-user"></i> Profile</a>
                                <?php } elseif (AppUser::isUserPayroller()) { ?>
                                    <a href="<?php echo Yii::app()->createUrl('payroller/main/profile'); ?>" ><i class="icon-user"></i> Profile</a>
                                <?php } else { ?>
                                    <a href="<?php echo Yii::app()->createUrl('user/main/profile'); ?>" ><i class="icon-user"></i> Profile</a>
                                <?php } ?>
                            </li>
                            <li class="divider">
                            </li>
                            <?php
                                $value = Yii::app()->user->getState('sa_roles');
                                // $session = Yii::app()->session;
                                //  dd($session);
                                if (isset($value)) {
                                    ?>
                                    <li>
                                        <a href="<?php echo Yii::app()->createUrl('/user/main/logoutuser'); ?>"><i class="icon-key"></i> Sign out this user</a>
                                    </li>
                                <?php } ?>
                            <li>
                                <a href="<?php echo Yii::app()->createUrl('/user/main/logout'); ?>" ><i class="icon-key"></i> Sign out</a>
                            </li>
                        </ul>
                    </li>
    </ul>
</section>
<!-- /.sidebar -->