<?php
if (!AppUser::isUserLogin()) {
    Yii::app()->request->redirect($this->createUrl('user/main/login'));
}
?>
<div class="page-header md-shadow-z-1-i navbar navbar-fixed-top">
    <!--Begin Header Inner-->
    <div class="page-header-inner">
        <div class="page-logo">
            <a href="<?php echo Yii::app()->createUrl('site/index'); ?>" class="logo">
                <img src="<?php echo (Yii::app()->theme->baseUrl . '/css/layout4/img/sp-logo.png'); ?>" alt="Logo" class="logo-default">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                <!--    Speedy Payroll-->
            </a>
            <div class="menu-toggler sidebar-toggler">
            </div>
        </div>
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
        </a>
        <div class="page-top hide-on-mobile">
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    <li class="separator hide">
                    </li>
                  
                    <?php $user = AppUser::getCurrentUser(); ?>

                    <li class="dropdown dropdown-extended dropdown-notification dropdown-dark" id="header_notification_bar">
                        <a href="javascript:;" class="dropdown-toggle" onclick="updateSeenTime();" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="true">
                            <i class="icon-bell"></i>
                            <?php $noti = new Notification(); ?>
                                <?php if(!AppUser::isUserSuperAdmin($user->id) && !AppUser::isUserPayroller($user->id))
                                    {$admin_count = $noti->getUserNotificationUnseenCount($user->id,true);
                                        if($admin_count>0){?>
                                <span class="badge badge-success" id='notification_count'> 
                                       <?php echo $admin_count; ?>
                                </span>
                                       <?php } else{ ?> 
                            <span class="badge badge-success" id='notification_count' style="display:none;"> </span>
                                       <?php } }
                                else{
                                    $user_count = $noti->getUserNotificationUnseenCount($user->id,false);
                                    if($user_count>0){ ?>
                            <span class="badge badge-success" id='notification_count'>
                                    <?php echo $user_count; ?>
                            </span>
                                    <?php } else{ ?> 
                            <span class="badge badge-success" id='notification_count' style="display:none;"> </span>
                                       <?php }
                                    }
                            ?>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="external">
                                <h3><b>Notifications</b></h3>
                                <a href="<?php echo Yii::app()->createUrl('/notification/main/index'); ?>">View All</a>
                            </li>
                            <li>
                                <div class="slimScrollDiv" id="custScroll">
                                    <ul class="dropdown-menu-list scroller" id="notification_list">
                                        <?php
                                        $i = 0;
                                        $notification_list = $noti->getUserNotifications($user->id);
//                                        dd($notification_list);
                                        foreach ($notification_list as $item) {
                                            ?>
                                        <?php if($item->is_read == 0){ ?><li style="background-color:#404548"><?php }
                                        else{ ?>
                                            <li>
                                        <?php } ?>
                                                <a href="#" onclick="javascript:updateRead_Redirect(<?php echo $item->id; ?>,'<?php echo AppUser::getNotificationLink($item->id, $item->notification_type_id, $item->timesheet_id, $item->contract_id, $item->user_id); ?>');">
                                                    <?php if ($item->notification_type_id == 0) { ?>
                                                        <span class="label label-sm label-icon label-info">
                                                            <i class="fa fa-bullhorn"></i>
                                                        </span>
                                                        <?php
                                                    } else {
                                                        echo $item->notificationType->logo;
                                                    }
                                                    ?>
                                                    <span style="margin-left:10px;"><?php echo AppUser::formatNotificationMessage($item); ?></span><br>
                                                    <div style="text-align:right;"><span><i class="fa fa-clock-o"></i><?php echo AppUser::getNotificationTime($item);?></span></div>
                                                </a>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </li>

                    <li class="dropdown dropdown-user dropdown-dark desktop-only">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <span class="username username-hide-on-mobile">
                                <?php echo $user->first_name . " " . $user->last_name; ?> </span>
                            <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                            <img alt="" class="img-circle username username-hide-on-mobile" src="<?php echo $user->photo ? Yii::app()->baseUrl . '/uploads/photos/' . $user->photo : Yii::app()->theme->baseUrl . '/img/default.png'; ?>"/>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <?php if (AppUser::isUserAdmin()) { ?>
                                    <a href="<?php echo Yii::app()->createUrl('user/main/view'); ?>" ><i class="icon-user"></i> Profile</a>
                                <?php } elseif (AppUser::isUserPayroller()) { ?>
                                    <a href="<?php echo Yii::app()->createUrl('payroller/main/profile'); ?>" ><i class="icon-user"></i> Profile</a>
                                <?php } else { ?>
                                    <a href="<?php echo Yii::app()->createUrl('user/main/profile'); ?>" ><i class="icon-user"></i> Profile</a>
                                <?php } ?>
                            </li>
                            <li class="divider">
                            </li>
                            <?php
                            $value = Yii::app()->user->getState('sa_roles');
                            // $session = Yii::app()->session;
                            //  dd($session);
                            if (isset($value)) {
                                ?>
                                <li>
                                    <a href="<?php echo Yii::app()->createUrl('/user/main/logoutuser'); ?>"><i class="icon-key"></i> Sign out this user</a>
                                </li>
                            <?php } ?>
                            <li>
                                <a href="<?php echo Yii::app()->createUrl('/user/main/logout'); ?>" ><i class="icon-key"></i> Sign out</a>
                            </li>
                        </ul>
                    </li>
                    <?php // if (AppHelp::hasHelp()) { ?>
<!--                        <li class="dropdown dropdown-user dropdown-dark">
                            <a href="#" class="pocp_button dropdown-toggle"><i class="fa fa-question-circle" ></i> Help</a>
                        </li>-->
                    <?php // } ?>

                </ul>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('#custScroll').slimScroll({
            height: '350px',
            width: 'none',
            overflow: 'none'
        });
        /*$('.slimScroll').slimScroll({
            height: '350px',
            width: 'none',
            overflow: 'none'
        });*/
    }); 
    function updateRead_Redirect(id, url){
        $.ajax({
            type: "POST",
            url: '<?php echo YII::app()->createUrl('/notification/main/updateread'); ?>',
            data: {'id':id},
            success: function (response) {
            },
            dataType: 'json'
        });
        window.location.href = url;
    }
    function updateSeenTime()
    {
        $.ajax({
            type: "POST",
            url: '<?php echo YII::app()->createUrl('/notification/main/updateunseentime'); ?>',
            success: function (response) {
                $("#notification_count").html(response.data);
                    $("#notification_count").hide();
            },
            dataType: 'json'
        });
    }
</script>
