<?php
foreach (Yii::app()->user->getFlashes() as $key => $message) {

    switch ($key) {
        case 'success': $containerClass = $key;
            $icon = "fa-check";
            break;
        case 'info':$containerClass = $key;
            $icon = "fa-info";
            break;
        case 'error':$containerClass = 'warning';
            $icon = "fa-times";
            break;
        default: $containerClass = $key;
            $icon = "fa-info";
            break;
    }
    ?>
    <div class="pad margin no-print">        
        <div class="alert alert-<?php echo $containerClass; ?>" style="margin-bottom: 0!important;float:right;">
            <i class="fa <?php echo $icon; ?>"></i>
            <b><?php echo ucfirst($key); ?>:</b> <?php echo $message; ?>
        </div>
    </div>
    <?php
}?>