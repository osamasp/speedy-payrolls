<?php if (AppHelp::hasHelp()) { ?>
    <div id="pocp1" class="pocp_right"><!-- BEGIN PULL OUT CONTENT PANEL -->
        <div class="pocp"><!-- BEGIN PANEL CONTAINER -->
            <div class="pocp_content"><!-- BEGIN PANEL INNER CONTENT -->
                <?php echo AppHelp::getHelpHTML(); ?>
            </div><!-- END PANEL INNER CONTENT -->
        </div><!-- END PANEL CONTAINER -->
    </div><!-- END PULL OUT CONTENT PANEL -->
<?php } ?>