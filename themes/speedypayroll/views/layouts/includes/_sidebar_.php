<!-- sidebar: style can be found in sidebar.less -->
<?php
$menu = array(
    array(
        'text' => 'Dashboard',
        'icon' => '<i class="fa fa-dashboard"></i>',
        'link' => Yii::app()->createUrl('/site/index'),
        'active' => true,
        'children' => array(),
        'roles' => array('admin', 'badmin'),
    ),
    array(
        'text' => 'Inventory',
        'icon' => '<i class="fa fa-table"></i>',
        'link' => Yii::app()->createUrl('/product/main/index'),
        'active' => true,
        'children' => array(),
        'roles' => array('admin', 'badmin'),
    ),
    array(
        'text' => 'Relationships',
        'icon' => '<i class="fa fa-group"></i>',
        'link' => '#',
        'active' => true,
        'children' => array(
            array(
                'text' => 'All',
                'icon' => '<i class="fa fa-angle-double-right"></i>',
                'link' => Yii::app()->createUrl('/company/relation/index'),
                'active' => true,
                'roles' => array('admin', 'badmin'),
            ),
            array(
                'text' => 'Customers',
                'icon' => '<i class="fa fa-angle-double-right"></i>',
                'link' => Yii::app()->createUrl('/company/relation/index/type/customer'),
                'active' => true,
                'roles' => array('admin', 'badmin'),
            ),
            array(
                'text' => 'Suppliers',
                'icon' => '<i class="fa fa-angle-double-right"></i>',
                'link' => Yii::app()->createUrl('/company/relation/index/type/supplier'),
                'active' => true,
                'roles' => array('admin', 'badmin'),
            ),
        ),
        'roles' => array('admin', 'badmin'),
    ),
    array(
        'text' => 'Sales Orders',
        'icon' => '<i class="fa fa-shopping-cart"></i>',
        'link' => Yii::app()->createUrl('/sales/main/index'),
        'active' => true,
        'children' => array(),
        'roles' => array('admin', 'badmin'),
    ),
    array(
        'text' => 'Invoice',
        'link' => Yii::app()->createUrl('/product/main/invoice'),
        'active' => false,
        'icon' => '<i class="fa fa-money"></i>',
        'children' => array(),
        'roles' => array('admin', 'badmin'),
    ),
    array(
        'text' => 'Branches',
        'icon' => '<i class="fa fa-sitemap"></i>',
        'link' => Yii::app()->createUrl('/company/branch/index'),
        'active' => true,
        'children' => array(),
        'roles' => array('admin','creator'),
    ),
    array(
        'text' => 'Manage Users',
        'icon' => '<i class="fa fa-user"></i>',
        'link' => Yii::app()->createUrl('/user/main/index'),
        'active' => true,
        'children' => array(),
        'roles' => array('admin','creator'),
    ),
    array(
        'text' => 'Reports',
        'icon' => '<i class="fa fa-file-text"></i>',
        'link' => '#',
        'active' => true,
        'children' => array(
            array(
                'text' => 'Sales',
                'icon' => '<i class="fa fa-angle-double-right"></i>',
                'link' => Yii::app()->createUrl('/sales/main/report'),
                'active' => true,
                'roles' => array('admin', 'badmin'),
            ),
        ),
        'roles' => array('admin', 'badmin'),
    ),
);
?>
<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
        <div class="pull-left image">
            <?php $user = User::model()->findByPk(AppUser::getUserId()); ?>
            <?php if (isset($user->photo_url)) { ?>
                <img src="<?php echo Yii::app()->baseUrl . '/uploads/' . $user->photo_url; ?>" class="img-circle" alt="User Image" />
            <?php } else { ?>
                <img src="<?php echo Yii::app()->theme->baseUrl. '/img/default.png'; ?>" class="img-circle" alt="User Image" />

            <?php } ?>
        </div>
        <div class="pull-left info">
            <p><?php echo $user->name; ?></p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
    </div>
    <!-- search form -->
<!--    <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
            <input type="text" name="q" class="form-control" placeholder="Search..."/>
            <span class="input-group-btn">
                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
            </span>
        </div>
    </form>-->
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
        <?php foreach ($menu as $menuItem) { ?>
            <?php if ($menuItem['active']) { ?>
                <?php if (array_intersect($menuItem['roles'], $user->getRoleSlugs()) != NULL) { ?>
                    <?php foreach ($menuItem['children'] as $menuSubItem) { ?>
                        <?php if ($menuSubItem['active']) { ?>
                            <?php
                            $active = 0;
                            if (Yii::app()->request->url == $menuSubItem['link']) {
                                $active = 1;
                                break;
                            }
                            ?>  
                        <?php } ?>
                    <?php } ?>
                    <?php if (Yii::app()->request->url == $menuItem['link']) { ?>
                        <li class="active">
                            <?php
                        } else if ($menuItem['children']) {
                            if ($active) {
                                ?>
                            <li class="treeview active">
                            <?php } else { ?>
                            <li class="treeview">
                                <?php
                            }
                        } else {
                            ?>
                        <li>
                        <?php } ?>
                        <a href="<?php echo $menuItem['link']; ?>">
                            <?php echo $menuItem['icon']; ?> <span><?php echo $menuItem['text']; ?></span>
                            <?php if ($menuItem['children']) { ?>
                                <i class="fa fa-angle-left pull-right"></i>
                            <?php } ?>
                        </a>
                        <?php if ($menuItem['children']) { ?>
                            <ul class="treeview-menu">
                                <?php foreach ($menuItem['children'] as $menuSubItem) { ?>
                                    <?php if ($menuSubItem['active']) { ?>
                                        <?php if (Yii::app()->request->url == $menuSubItem['link']) { ?>
                                            <li class="active">
                                            <?php } else { ?>
                                            <li>
                                            <?php } ?>
                                            <a href="<?php echo $menuSubItem['link']; ?>">
                                                <?php echo $menuSubItem['icon']; ?> <?php echo $menuSubItem['text']; ?>
                                            </a>
                                        </li>    
                                    <?php } ?>

                                <?php } ?>
                            </ul>
                        <?php } ?>
                    </li>    
                <?php } ?>
            <?php } ?>
        <?php } ?>

        <!--        <li class="active">
                    <a href="index.html">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="pages/widgets.html">
                        <i class="fa fa-th"></i> <span>Widgets</span> <small class="badge pull-right bg-green">new</small>
                    </a>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-bar-chart-o"></i>
                        <span>Charts</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="pages/charts/morris.html">
                            <i class="fa fa-angle-double-right"></i> Morris</a>
                        </li>
                        <li><a href="pages/charts/flot.html"><i class="fa fa-angle-double-right"></i> Flot</a></li>
                        <li><a href="pages/charts/inline.html"><i class="fa fa-angle-double-right"></i> Inline charts</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-laptop"></i>
                        <span>UI Elements</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="pages/UI/general.html"><i class="fa fa-angle-double-right"></i> General</a></li>
                        <li><a href="pages/UI/icons.html"><i class="fa fa-angle-double-right"></i> Icons</a></li>
                        <li><a href="pages/UI/buttons.html"><i class="fa fa-angle-double-right"></i> Buttons</a></li>
                        <li><a href="pages/UI/sliders.html"><i class="fa fa-angle-double-right"></i> Sliders</a></li>
                        <li><a href="pages/UI/timeline.html"><i class="fa fa-angle-double-right"></i> Timeline</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-edit"></i> <span>Forms</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="pages/forms/general.html"><i class="fa fa-angle-double-right"></i> General Elements</a></li>
                        <li><a href="pages/forms/advanced.html"><i class="fa fa-angle-double-right"></i> Advanced Elements</a></li>
                        <li><a href="pages/forms/editors.html"><i class="fa fa-angle-double-right"></i> Editors</a></li>                                
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-table"></i> <span>Tables</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="pages/tables/simple.html"><i class="fa fa-angle-double-right"></i> Simple tables</a></li>
                        <li><a href="pages/tables/data.html"><i class="fa fa-angle-double-right"></i> Data tables</a></li>
                    </ul>
                </li>
                <li>
                    <a href="pages/calendar.html">
                        <i class="fa fa-calendar"></i> <span>Calendar</span>
                        <small class="badge pull-right bg-red">3</small>
                    </a>
                </li>
                <li>
                    <a href="pages/mailbox.html">
                        <i class="fa fa-envelope"></i> <span>Mailbox</span>
                        <small class="badge pull-right bg-yellow">12</small>
                    </a>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-folder"></i> <span>Examples</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="pages/examples/invoice.html"><i class="fa fa-angle-double-right"></i> Invoice</a></li>
                        <li><a href="pages/examples/login.html"><i class="fa fa-angle-double-right"></i> Login</a></li>
                        <li><a href="pages/examples/register.html"><i class="fa fa-angle-double-right"></i> Register</a></li>
                        <li><a href="pages/examples/lockscreen.html"><i class="fa fa-angle-double-right"></i> Lockscreen</a></li>
                        <li><a href="pages/examples/404.html"><i class="fa fa-angle-double-right"></i> 404 Error</a></li>
                        <li><a href="pages/examples/500.html"><i class="fa fa-angle-double-right"></i> 500 Error</a></li>                                
                        <li><a href="pages/examples/blank.html"><i class="fa fa-angle-double-right"></i> Blank Page</a></li>
                    </ul>
                </li>-->
    </ul>
</section>
<!-- /.sidebar -->