<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/thirdparty/js/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/themes/default/css/uniform.default.css" rel="stylesheet" type="text/css">
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pages/css/login.css" rel="stylesheet" type="text/css">
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/components-md.css" id="style_components" rel="stylesheet" type="text/css"/>
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/plugins-md.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/layout/css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/layout/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/layout/css/custom.css" rel="stylesheet" type="text/css"/>
        <script>var basePath = "<?php echo Yii::app()->theme->baseUrl; ?>";</script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.2.1/jquery-migrate.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js" type="text/javascript"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.66.0-2013.10.09/jquery.blockUI.min.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/thirdparty/js/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.3.1/jquery.cookie.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.0/jquery.validate.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.0/additional-methods.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.2/jquery.slimscroll.min.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/thirdparty/js/metronic.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/css/layout4/scripts/layout.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/css/layout4/scripts/demo.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/css/pages/scripts/login.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/css/pages/scripts/form-wizard.js"></script>
        <script>
    jQuery(document).ready(function() {
        // initiate layout and plugins
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        Demo.init(); // init demo features
        FormWizard.init();
    });
    </script>
        

    </head>
    <body class="page-md login">
        <?php echo $this->renderPartial('//layouts/includes/modelPopUp'); ?>
            <div class="logo">
                <img src="<?php echo (Yii::app()->theme->baseUrl . '/img/t-logo.png'); ?>" alt="Logo">
            </div><!-- /.login-logo -->
            <?php echo $content; ?>
            <div class="copyright">
                2019 © eminant Solutions Ltd.
            </div>

        <?php if (AppHelp::hasHelp()) { ?>
            <a href="#" class="pocp_button"><i class="fa fa-question-circle"></i></a>
            <?php } ?>

    </body>
</html>
