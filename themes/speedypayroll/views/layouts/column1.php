<?php $this->beginContent('//layouts/main'); ?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <?php echo $content; ?>
        </div>
    </div><!-- content -->
</div>

<?php $this->endContent(); ?>