<div class="col-md-6">
    <p>Name: <?php echo $employeeName; ?></p>
    <p>NI Number: <?php echo $ni_number; ?></p>
    <p>Company: <?php echo ucfirst($company); ?></p>
    <p>Location: <?php echo $companyCity . ',' . $companyCountry; ?></p>
    <?php if (isset($payHours) && count($payHours) > 0) { ?>
        <?php foreach ($payHours as $key => $payRate) { ?>
            <p><?php echo ucfirst($key) . ' hours: ' . $payRate . ' x £' . ($payRates ? $payRates[$key] : ''); ?> </p>
        <?php } ?>
    <?php } ?>
    <?php if (isset($itemCount) && count($itemCount) > 0) { ?>
        <?php foreach ($itemCount as $key => $item) { ?>
            <p><?php echo ucfirst($key) . ': ' . $item . ' x £' . ($itemRates ? $itemRates[$key] : ''); ?> </p>
        <?php } ?>
    <?php } ?>
    <p>Expense: <?php echo '£' . $expense; ?></p>

    <!-- Your first column here -->
</div>
<div class="col-md-6">
    <p>Staff ID: <?php echo $employeeId; ?></p>
    <p>PaySlip Period: <?php echo AppTimeSheet::formatSheetPeriod($entry); ?></p>
    <p>Date Submitted: <?php echo $date; ?></p>
    <p>Approver: <?php echo $approver; ?></p>

    <p>Employment Type: <?php echo AppContract::formatEmployeType($employeeType); ?></p>
    <!-- Your second column here -->
</div>
<div class="clearfix"></div>
