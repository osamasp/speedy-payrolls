<?php
$this->breadcrumbs = array(
    'Main',
);
?>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/invoice-main-sendinvoice.js" type="text/javascript"></script>
<input type="hidden" id="contact_details" value="<?php echo Yii::app()->baseUrl; ?>/timesheet/quickTimesheet/getContactDetails"/>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i> Create New Invoice
        </div>
    </div>
    <div class="portlet-body form">

        <form id="form-invoice" method="POST" action="<?php echo Yii::app()->createAbsoluteUrl('/invoice/main/sendInvoice') ?>">
            <div class="form-body">
                <?php if (!empty($company->logo)) { ?>
                            <img name="logo" class="logo-default" src="<?php echo Yii::app()->createAbsoluteUrl('uploads/photos') . '/' . $company->logo; ?>" width="120px" height="120px">
                            <input type="hidden" name="logo" value="<?php echo Yii::app()->createAbsoluteUrl('uploads/photos') . '/' . $company->logo; ?>">
                        <?php } else { ?>
                            <h3 class="form-section"><?php echo $company['name'] ?></h3>
                        <?php } ?>
                            <hr>
                            <h3 class="form-section">Client Details</h3>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Send to*</label>
                                        <select class="form-control" name="contacts" id="contacts" onchange="check()">
                                            <option value="out">Select Contact</option>
                                            <?php foreach ($contacts as $contact) { ?>
                                                <option value="<?php echo $contact->id; ?>"><?php echo $contact->name; ?></option>
                                            <?php } ?>
                                        </select>
                                        <span class="help-block">
                                            Select from your contact list </span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="alert alert-info">
                                            <strong>Info!</strong>
                                            If you wish to send this invoice to a new contact please use the form fields below
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="alert alert-info">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Company Name*</label>
                                            <input type="text" name="contact-name" class="form-control" id="name" placeholder="Enter Name" required>
                                            <span class="help-block">
                                                Enter the company name you want to send invoice to </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Person</label>
                                            <input type="text" id="lastName" class="form-control" placeholder="Enter Name">
                                            <span class="help-block">
                                                Enter the name the invoice is addressed to (optional) </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Email address*</label>
                                            <input type="text" name="contact-email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="E.g:example@abc.com" class="form-control" id="email" placeholder="Enter Email" required>
                                            <span class="help-block">
                                                Enter client email </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-12 ">
                                        <div class="form-group">
                                            <label>Street Address*</label>
                                            <input type="text" name="client_add" class="form-control" id="cl_add" placeholder="Enter Address" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>City*</label>
                                            <input type="text" name="client_city" class="form-control" id="cl_city" placeholder="Enter City" required>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Post Code*</label>
                                            <input type="text" name="client_pc" class="form-control" id="cl_pc" placeholder="Enter Post Code" required>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label">Country*</label>
                                        <input type="text" name="client_country" class="form-control" id="cl_country" placeholder="Enter Country" required>
                                    </div>
                                </div>
                            <h3 class="form-section">Invoice Details</h3>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Date*</label>
                                        <input type="text" value='<?php echo date('d/m/Y') ?>' name="date" id='date' class="form-control datePicker disableEle" style='cursor: context-menu;' required readonly>
                                        <span class="help-block">
                                            Selected date will appear on invoice </span>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Invoice Ref#*</label>
                                        <input type="text" class="form-control" id="invoice_ref" name="invoice_ref" required>
                                        <span class="help-block">
                                            Entered reference will appear on invoice</span>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <input type="hidden" name="timesheet_id" value="<?php echo $timesheet_id ?>">
                            <hr>
                            <h3>Invoice Items</h3>
                            <table  id="myTable" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Chargeable Items*</th>
                                        <th>Units*</th>
                                        <th>Rate*</th>
                                        <th>Cost*</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    $count = 0;
                                    $subtotal = 0;
                                    //dd($details['payHours']);
                                    if (!isset($details['invoice_att']) && isset($details['payHours'])) {
                                        ?>
                                        <?php
                                        if (isset($details['payHours']) && count($details['payHours']) > 0) {
                                            ?>
                                        <input type ='hidden' id="count<?php echo $count ?>" name='count' value='<?php echo count($details['payHours']) ?>'>

                                        <?php
                                        foreach ($details['payHours'] as $key => $payRate) {
                                            ?>
                                            <tr>
                                                <td><input class="form-control" type="text" name="item<?php echo $count ?>" value="<?php echo ucfirst($key) . ' hours'; ?>" required></td>
                                                <td><input class="form-control" type="text" name="unit<?php echo $count ?>" value="<?php echo $payRate; ?>"  onkeypress="return isNumberKey(event);" required></td>
                                                <td><input class="form-control" type="text" name="rate<?php echo $count ?>" value="<?php echo ($details['payRates'] ? $details['payRates'][$key] : ''); ?>"  onkeypress="return isNumberKey(event);" required></td>
                                                <td><input class="form-control" type="text" name="cost<?php echo $count ?>" value="<?php echo ($details['payRates'] ? $details['payRates'][$key] : '') * $payRate; ?>"  onkeypress="return isNumberKey(event);" required></td>
                                            </tr>
                                            <!--                                <?php
                                            $count++;
                                            $subtotal += ($details['payRates'] ? $details['payRates'][$key] : 0) * $payRate;
                                        }
                                    }
                                    if (isset($details['itemCount']) && count($details['itemCount']) > 0) {
                                        ?>
                                                                            <input type ='hidden' id="count<?php echo $count ?>" name='count' value='<?php echo count($details['itemCount']) + count($details['payHours']) ?>'>
                                        <?php foreach ($details['itemCount']as $key => $item) {
                                            ?>
                                                                                    
                                                                                    <tr>
                                                                                        <!--<td><label name="payrate_name[<?php // ucwords($value->contractSetting->key);      ?>]"><?php // echo ucwords($value->contractSetting->key);       ?></label></td>-->
                                            <td><input class="form-control" type="text" name="item<?php echo $count ?>" value="<?php echo ucfirst($key) ?>" required></td>
                                            <td><input class="form-control" type="text" name="unit<?php echo $count ?>" value="<?php echo $item ?>"  onkeypress="return isNumberKey(event);" required></td>
                                            <td><input class="form-control" type="text" name="rate<?php echo $count ?>" value="<?php echo ($details['itemRates'] ? $details['itemRates'][$key] : '') ?>"  onkeypress="return isNumberKey(event);" required></td>
                                            <td><input class="form-control" type="text" name="cost<?php echo $count ?>" value="<?php echo ($details['itemRates'] ? $details['itemRates'][$key] : '') * $item ?>"  onkeypress="return isNumberKey(event);" required></td>
                                            </tr>
                                            <?php
                                            $count++;
                                            $subtotal += ($details['itemRates'] ? $details['itemRates'][$key] : 0) * $item;
                                        }
                                    }
                                    if (isset($details['expense'])) {
                                        ?>
                                        <tr>
                                            <td><input class="form-control" type="text" name="item<?php echo $count ?>" value="Expense" required></td>
                                            <td><input class="form-control" type="text" name="unit<?php echo $count ?>" value="1"  onkeypress="return isNumberKey(event);" required></td>
                                            <td><input class="form-control" type="text" name="rate<?php echo $count ?>" value="<?php echo $details['expense'] ?>"  onkeypress="return isNumberKey(event);" required></td>
                                            <td><input class="form-control" type="text" name="cost<?php echo $count ?>" value="<?php echo $details['expense'] ?>"  onkeypress="return isNumberKey(event);" required></td>
                                        </tr>
                                        <?php if (isset($details['itemCount'])) {
                                            ?>
                                            <input type ='hidden' id="count<?php echo $count ?>" name='count' value='<?php echo count($details['itemCount']) + count($details['payHours']) + 1 ?>'>
                                        <?php } else {
                                            ?>
                                            <input type ='hidden' id="count<?php echo $count ?>" name='count' value='<?php echo count($details['payHours']) + 1 ?>'>
                                            <?php
                                        }
                                        $count++;
                                        $subtotal += $details['expense'];
                                    }
                                    ?>
                                    <tr>
                                        <td colspan="4"><a href="#" class="btn btn-circle btn-sm green-haze" onclick="addItem(<?php echo $count ?>)">Add more items</a></td>
                                    </tr>
                                    <?php
                                } elseif (isset($details['invoice_att'])) {
                                    $count = 0;
                                    if (count($details['invoice_att']) > 0) {
                                        ?> 
                                        <?php
                                        foreach ($details['invoice_att'] as $value) {
                                            ?>
                                            <tr>
                                                <td><input class="form-control" type="text" name="item<?php echo $count ?>" value="<?php echo $value->item; ?>" required></td>
                                                <td><input class="form-control" type="text" name="unit<?php echo $count ?>" value="<?php echo $value->unit; ?>"  onkeypress="return isNumberKey(event);" required></td>
                                                <td><input class="form-control" type="text" name="rate<?php echo $count ?>" value="<?php echo $value->rate; ?>"  onkeypress="return isNumberKey(event);" required></td>
                                                <td><input class="form-control" type="text" name="cost<?php echo $count ?>" value="<?php echo $value->cost; ?>"  onkeypress="return isNumberKey(event);" required></td>
                                            </tr>

                                            <?php
                                            $count++;
                                            $subtotal += $value->cost;
                                        }
                                        ?>
                                        <input type ='hidden' name='count' id="count<?php echo $count ?>" value='<?php echo count($details['invoice_att']) ?>'>
                                        <tr>
                                            <td colspan="4"><a href="#" class="btn btn-circle btn-sm green-haze" onclick="addItem(<?php echo $count + 1 ?>)">Add more items</a></td>
                                        </tr>
                                        <?php
                                    } else {
                                        ?>
                                        <tr>
                                        <input type ='hidden' name='count' id="count<?php echo $count ?>" value='1'>
                                        <td><input class="form-control" type="text" name="item0" value="<?php //echo $details['invoice_att'][0]->item;   ?>" required></td>
                                        <td><input class="form-control" type="text" name="unit0" value="<?php //echo $details['invoice_att'][0]->unit;   ?>"  onkeypress="return isNumberKey(event);" required></td>
                                        <td><input class="form-control" type="text" name="rate0" value="<?php //echo $details['invoice_att'][0]->rate;   ?>"  onkeypress="return isNumberKey(event);" required></td>
                                        <td><input class="form-control" type="text" name="cost0" value="<?php //echo $details['invoice_att'][0]->cost;   ?>"  onkeypress="return isNumberKey(event);" required></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4"><a href="#" class="btn btn-circle btn-sm green-haze" onclick="addItem(<?php echo $count + 1 ?>)">Add more items</a></td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    $count = 0;
                                    ?>
                                    <tr>
                                    <input type ='hidden' name='count' id="count<?php echo $count ?>" value='1'>
                                    <td><input class="form-control" type="text" name="item<?php echo $count ?>" required></td>
                                    <td><input class="form-control" type="text" name="unit<?php echo $count ?>"  onkeypress="return isNumberKey(event);" required></td>
                                    <td><input class="form-control" type="text" name="rate<?php echo $count ?>"  onkeypress="return isNumberKey(event);" required></td>
                                    <td><input class="form-control" type="text" name="cost<?php echo $count ?>"  onkeypress="return isNumberKey(event);" required></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4"><a href="#" class="btn btn-circle btn-sm green-haze" onclick="addItem(<?php echo $count + 1 ?>)">Add more items</a></td>
                                    </tr>
                                <?php }
                                ?>
                                <tr>
                                    <td colspan="3" style="text-align: right;">Subtotal*</td>
                                    <td><input type="text" class="form-control" name="subtotal" value="<?php echo (float)$subtotal; ?>"  onkeypress="return isNumberKey(event);" required></td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="text-align: right;">VAT*</td>
                                    <td><input type="text" class="form-control" name="vat" value="<?php echo (float)$subtotal*0.2; ?>" onkeypress="return isNumberKey(event);" required></td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="text-align: right;">Total*</td>
                                    <td><input type="text" class="form-control" name="total" value="<?php echo (float)($subtotal*0.2)+$subtotal;?>"  onkeypress="return isNumberKey(event);" required></td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="row">
                <div class="col-md-4">
                    <label>Contractor Company Name : </label><input type="text" name="contractor_name" value="<?php echo $company['name'] ?>" class="form-control"><br>
                </div>
                    <div class="col-md-4"><label>Contractor Company Address</label><input type="text" name="contractor_address" value="<?php echo $company['address'] ?>" class="form-control"></div>
                <div class="col-md-4">
                    <label>VAT Number : </label><input type="text" name="vat_number" value="<?php echo $company['vat_number'] ?>" class="form-control">
                </div>
                            </div><hr>
                      <div class="form-actions right">
    <div class="row">
        <div class="col-md-offset-3 col-md-9">
            <button type="button" onclick="javascript:parent.history.back();" class="btn btn-circle btn-sm btn-default">Cancel</button>
            <!--<input type="submit" value="Save & Invoice"class="btn blue" >-->
            <a href="javascript:void(0)" class="btn btn-circle btn-sm green-haze" onclick="validate()">Send Invoice</a>
            <a data-toggle="modal" style="display:none;" id="sendinvoice" class="btn btn-circle btn-sm green-haze" data-target=".Model" data-whatever="<?php echo $this->createUrl('/invoice/main/sendinvoice'); ?>">Send Invoice</a>
        </div>
    </div></div>      
            </div>
    <div class="clearfix">

    </div>
        </form>
</div>