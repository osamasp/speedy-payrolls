<?php
/* @var $this InvoiceController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Invoices',
);

$this->menu = array(
    array('label' => 'Create Invoice', 'url' => array('create')),
    array('label' => 'Manage Invoice', 'url' => array('admin')),
);
?>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/contract-main-list.js" type="text/javascript"></script>
<input type="hidden" id="url" value="<?php echo $this->createUrl('/invoice/main/GetData');?>"/>
<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-globe"></i> Sent Invoices
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
        
        </div>
    </div>
    <div class="portlet-body">
    <div class="col-md-12">
       <a class="btn btn-success pull-right btn-circle" href="<?php echo $this->createUrl('/invoice/main/sendinvoice?Admin'); ?>">Create Invoice</a>

    </div>
 
        <table class="table table-striped table-bordered table-hover" id="example1">
            <thead>
                <tr>
                    <th class="table-checkbox">
                        <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes"/>
                    </th>
                    <th>
                        Client
                    </th>
                    <th>
                        Sent to
                    </th>
                    <th>
                        Invoice Ref#
                    </th>
                    <th>
                        Date Sent
                    </th>
                    <th>
                        Status
                    </th>
                    <th>
                        Action
                    </th>


                </tr>
            </thead>
            <tbody>
                <?php foreach ($dataProvider as $item) {
                    ?> 
                    <tr class="odd gradeX">
                        <td><input type="checkbox" class="checkboxes" value="<?php echo $item->id; ?>"/></td>
                        <td><?php echo $item->contact->name; ?></td> 
                        <td><?php echo $item->contact->email; ?></td>
                        <td><?php echo $item->invoice_reference ? $item->invoice_reference : '-'; ?></td> 
                        <td><?php echo date(AppInterface::getdateformat(), $item->created_at); ?></td> 
                        <td><?php echo $item->contact->type; ?></td>
                        <td> 

                            <div class="btn-group">
                                        <button class="btn green dropdown-toggle" data-toggle="dropdown">Actions <i class="fa fa-angle-down"></i>
                                        </button>
                                <ul class="dropdown-menu pull-right" role="menu"> 
                                    <li><a data-toggle="modal" data-target=".ajaxModel" data-whatever="<?php echo $item->id; ?>" >View</a></li>
                                    <li><a href="<?php echo $this->createUrl('sendinvoice', array('id' => $item->id)); ?>">Invoice</a></li> 
                                    <li class="divider"></li> 
                                    <li><a href="javascript:void(0);" onclick="if (confirm('Are you sure you want delete this invoice?')) {
                                                window.location = '<?php echo $this->createUrl('invoicedelete', array('id' => $item->id)); ?>';
                                            }" >Delete</a></li> 
                                </ul> 
                            </div> 

                        </td> 
                    </tr> 
<?php } ?> 
            </tbody>
        </table>
    </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->