<?php
/* @var $this MainController */

$this->breadcrumbs = array(
    'Main',
);
?>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/invoice-main-index.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
<!--<h1>Invoice</h1>-->
<div class="clearfix"></div>
<div class="col-sm-12">
    <div>
        <div class='box-header'>
            <h2 class='box-title'>Invoice Editor</h2>
            <!-- tools box -->
            <div class="pull-right box-tools">
            </div><!-- /. tools -->
        </div><!-- /.box-header -->
        <div>
            <form id="editor" method="POST" action="">
                <textarea class="textarea" name="email-text" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $timeSheetData; ?></textarea>
                <br>
                <div class="col-sm-8 form-group" style="padding-top : 15px;">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-3"><b>Send Invoice To:</b></div>
                    <div class="col-sm-4">
                        <select class="form-control" name="contacts" id="contacts" onchange="check()">
                            <option value="out">Select Contact</option>
                            <?php foreach ($contacts as $contact) { ?>
                                <option value="<?php echo $contact->id; ?>"><?php echo $contact->name; ?></option>
                            <?php } ?>
                        </select> 
                    </div>
                </div>
                <div class="col-sm-8 form-group" >
                    <div class="col-sm-1"></div>
                    <div class="col-sm-3"><b>Enter email to send invoice to:</b></div>
                    <div class="col-sm-4">
                        <input type="text" name="contact-email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="E.g:example@abc.com" class="form-control" id="email" placeholder="Enter Email">
                    </div>
                </div>
                <div class="col-sm-8 form-group">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-3"><b>Name of person or company you're sending invoice to:</b></div>
                    <div class="col-sm-4">
                        <input type="text" name="contact-name" class="form-control" id="name" placeholder="Enter Name">
                    </div>

                </div>
                <div class="col-sm-8">
                    <div class="col-sm-5"></div>
                    <div class="col-sm-3">
                        <input style="margin-bottom: 10px;" type="submit" class="btn btn-primary" value="Invoice"><br>
                    </div><br>
                </div>
            </form>
        </div>
    </div>



</div>
<div class="clearfix"></div>


<script type="text/javascript">
    $(function () {
        //bootstrap WYSIHTML5 - text editor
        $(".textarea").wysihtml5();
    });

</script>

