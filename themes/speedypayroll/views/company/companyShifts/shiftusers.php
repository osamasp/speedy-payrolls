<?php
/* @var $this CompanyShiftsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'View Shift Users',
);

//$this->menu=array(
//array('label'=>'Create CompanyShifts', 'url'=>array('create')),
//array('label'=>'Manage CompanyShifts', 'url'=>array('admin')),
//);
?>
<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                Widget settings form goes here
            </div>
            <div class="modal-footer">
                <button type="button" class="btn blue">Save changes</button>
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-copy"></i>View Shift Users
        </div>
    </div>
    <div class="portlet-body table-responsive">
        <div class="box-body table-responsive" style="padding-top:10px;">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                                                                <!--<th><?php // echo CompanyShifts::model()->getAttributeLabel('id')  ?></th>-->
                        <th>Name</th>
                        <th>Email</th>
                        <th>Shift</th>
                        <!--<th><?php // echo CompanyShifts::model()->getAttributeLabel('company_id')  ?></th>-->
                        <!--<th>Action</th>-->
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($users as $item) { ?>
                        <tr>
                                                                    <!--<td><?php // echo $item->id;  ?></td>-->
                            <td><?php echo $item->first_name." ".$item->last_name; ?></td>
                            <td><?php echo $item->email; ?></td>
                            <td><?php if(isset($item->companyShift)){
                                echo $item->companyShift->title;
                            }else{
                               echo "-";
                            } ?></td>
                            <!--<td><?php // echo $item->company_id;  ?></td>-->
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>