<?php
/* @var $this CompanyShiftsController */
/* @var $data CompanyShifts */
?>

<div class="box-body">
    
	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('start_time')); ?>:</b>
	<?php echo CHtml::encode(date('h:i a',$data->start_time)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('end_time')); ?>:</b>
	<?php echo CHtml::encode(date('h:i a',$data->end_time)); ?>
	<br />

</div>