<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
preg_match_all('!\d+!', Yii::app()->session["client_timezone"], $matches);
?>
<div class="portlet-title">
    <div class="caption">
	<i class="icon-bar-chart font-green-sharp hide"></i>
	<span class="caption-helper">JOB</span> &nbsp; <span class="caption-subject font-green-sharp bold uppercase">Comments</span>
    </div>
    <div class="actions">
        <div class="btn-group">
            <?php if($canAddNote){ ?>
                <a id="note" class="btn green-haze btn-circle btn-sm">Comment</a>
            <?php } ?>
                <a id="note" class="btn green-haze btn-circle btn-sm" style="display:none;">Comment</a>
        </div>
    </div>
</div>
<div class="portlet-body">
<div class="row">
<!--    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 350px;">    -->
    <div class="scroller" style="max-height: 350px;" data-always-visible="0" data-rail-visible="0" data-handle-color="#dae3e7">
    <div class='todo-tasklist'>
    <?php $i=0; $user = AppUser::getCurrentUser(); foreach ($notes as $item) { ?>    
        <div class="todo-tasklist-item <?php echo ($i%2==0)? "todo-tasklist-item-border-green":"todo-tasklist-item-border-red"; $i++; ?> ">
                <img alt="User" class="todo-userpic pull-left" width="27" height="27" src="<?php echo $user->photo ? Yii::app()->baseUrl . '/uploads/photos/' . $user->photo : Yii::app()->theme->baseUrl . '/img/default.png'; ?>"/>
                <div class="todo-tasklist-item-title" style="padding-left:10px;"><?php echo $user->first_name." ".$user->last_name; ?></div>
        
            <div class="todo-tasklist-item-text">
                <?php echo $item->note; ?>
            </div>
            <div class="todo-tasklist-controls pull-left">
                <span class="todo-tasklist-date" style="font-size:13px;">
                    <i class="fa fa-thumb-tack"></i>
                <?php echo " " . date('h:i a, d M Y', ($item->created_at + ($matches[0][0] * 3600)-3600)) . " "; ?>
                </span>
            </div>
        </div>
    <?php } if (count($notes) < 1) { ?>
        <div class="divider" style="background: #f1f3f6;height: 2px;"></div>
    
        <div class="row note note-success" style="padding-left:20px;">
            <div class="col-md-12">
                <label>No notes to show....</label>
            </div>
        </div>
    </div>
        
    <?php } ?>
        </div>
</div>
</div>
<script src="<?php echo Yii::app()->theme->baseUrl;  ?>/thirdparty/js/todo.js" type="text/javascript"></script>
<script>
jQuery(document).ready(function() {    
    Todo.init(); // init todo page
});

    </script>