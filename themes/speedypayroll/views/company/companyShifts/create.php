<?php
/* @var $this CompanyShiftsController */
/* @var $model CompanyShifts */

$this->breadcrumbs=array(
	'Company Shifts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CompanyShifts', 'url'=>array('index')),
	array('label'=>'Manage CompanyShifts', 'url'=>array('admin')),
);
?>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i>Company Shifts
        </div>
    </div>
    <div class="portlet-body form">
<div class="box-body" style="padding-left:20px;padding-top:10px;">
<!--<div class="box-header">
    <h3 class="box-title">Create Company Shifts</h3>
</div>-->
    </div>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
    </div>
    </div>