<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$counter = 0;
preg_match_all('!\d+!', Yii::app()->session["client_timezone"], $matches);
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
    <h4 class="modal-title" id="exampleModalLabel">Timesheet Details</h4>
</div>
<div class="modal-body" id="popup">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <label style="font-weight:bold;">Date:</label> <?php echo date('d/m/Y', $model->created_at); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label style="font-weight:bold;">Time In:</label>
                    <label id="time_in">
                        <?php echo date('h:i A', ($model->time_in + ($matches[0][0] * 3600)-3600)); ?>
                    </label> 
                </div>
                <div class="col-md-6">
                    <label style="font-weight:bold;">Time Out:</label>
                    <label id="time_out">
                        <?php echo date('h:i A', ($model->time_out + ($matches[0][0] * 3600)-3600)); ?>
                    </label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <h2 style="font-family:'Avenir Next','Avenir Next LT W01 SC','Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: bold;font-size:16px;color:lightgray;">Breaks</h2>
                </div>
            </div>
            <?php foreach ($lunches as $lunch) { ?>
                <div class="row">
                    <div class="col-md-6">
                        <label style="font-weight:bold;">Break In:</label>
                        <label id="lunch_in<?php echo $counter; ?>">
                            <?php echo date('h:i A', ($lunch->lunch_in + ($matches[0][0] * 3600)-3600)); ?>
                        </label>
                    </div>
                    <div class="col-md-6">
                        <label style="font-weight:bold;">Break Out:</label>
                        <label id="lunch_out<?php echo $counter; ?>">
                            <?php echo date('h:i A', ($lunch->lunch_out + ($matches[0][0] * 3600)-3600)); ?>
                        </label>
                    </div>
                </div>
                <?php $counter++;
            } if (count($lunches) < 1) { ?>
                <div class="row">
                    <div class="col-md-6">
                        <label>No Break to show.....</label>
                    </div>
                </div>
            <?php } ?>
            <div class="row">
                <div class="col-md-6">
                    <h2 style="font-family:'Avenir Next','Avenir Next LT W01 SC','Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: bold;font-size:16px;color:lightgray;">Notes</h2>
                </div>
            </div>
            <?php foreach ($notes as $item) { ?>
                <div class="row">
                    <div class="col-md-6">
                        <label><b>Note:</b></label> <label><?php echo $item["note"]; ?></label>
                    </div>
                    <div class="col-md-6">
                        <label><b>Time: </b></label> 
                        <label><?php echo date('d/m/Y', $item["created_at"]) . " "; ?>
                            <label id="times<?php echo $item['id']; ?>">
                                <?php echo date('h:i A', ($item["created_at"] + ($matches[0][0] * 3600)-3600)); ?>
                            </label>
                        </label>
                    </div>
                </div>
            <?php } if (count($notes) < 1) { ?>
                <div class="row">
                    <div class="col-md-6">
                        <label>No notes to show.....</label>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-primary" onclick="javascript:printDiv('popup');"><span class="md-click-circle" style="height: 63px; width: 63px; top: -17.5px; left: 10.8906px;"></span>Print</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
<script>
    function convertToLocal(times, id) {
        if (times == '01/01/1970 01:00 UTC')
        {
            return "00:00";
        } else {
            var date = new Date(times);
            var hours = date.getHours();
            hours = (hours < 10 ? "0" : "") + hours;
            var minute = date.getMinutes();
            var ampm = hours >= 12 ? 'PM' : 'AM';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minute = minute < 10 ? '0' + minute : minute;
            var strTime = hours + ':' + minute + ' ' + ampm;
            document.getElementById(id).innerHTML = strTime;
        }
    }
</script>