<?php
/* @var $this CompanyShiftsController */
/* @var $model CompanyShifts */

$this->breadcrumbs=array(
	'Company Shifts'=>array('index'),
	$model->title,
);

$this->menu=array(
array('label'=>'List CompanyShifts', 'url'=>array('index')),
array('label'=>'Create CompanyShifts', 'url'=>array('create')),
array('label'=>'Update CompanyShifts', 'url'=>array('update', 'id'=>$model->id)),
array('label'=>'Delete CompanyShifts', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage CompanyShifts', 'url'=>array('admin')),
);
?>
<h2 style="font-family:'Avenir Next','Avenir Next LT W01 SC','Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: bold;font-size:20px;color:lightgray;">Company Shift Details</h2>
<div class="table-responsive">
    <div class="box-body">
        <?php $this->widget('zii.widgets.CDetailView', array(
        'data'=>$model,
        'attributes'=>array(
		'title',
		'start_time',
		'end_time',
        ),
        )); ?>
    </div>
</div>