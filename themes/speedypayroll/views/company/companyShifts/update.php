<?php
/* @var $this CompanyShiftsController */
/* @var $model CompanyShifts */

$this->breadcrumbs=array(
	'Company Shifts'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

//$this->menu=array(
//	array('label'=>'List CompanyShifts', 'url'=>array('index')),
//	array('label'=>'Create CompanyShifts', 'url'=>array('create')),
//	array('label'=>'View CompanyShifts', 'url'=>array('view', 'id'=>$model->id)),
//	array('label'=>'Manage CompanyShifts', 'url'=>array('admin')),
//);
?>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Company Shifts
        </div>
    </div>
    <div class="portlet-body form">
<div class="box-body" style="padding-left:20px;padding-top:10px;">

    </div>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>
</div>
    </div>