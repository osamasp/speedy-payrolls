<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'user-timings-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => false,
        ));
$counter = 1;
preg_match_all('!\d+!', Yii::app()->session["client_timezone"], $matches);
?>
<script>
    function convertToLocal(value,id){
        $(id).val(value);
  }
    </script>
<!--<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
    <h4 class="modal-title" id="exampleModalLabel">Edit Timesheet</h4>
</div>-->
    
<input type="hidden" name="id" value="<?php echo $model->id?>">
<input type="hidden" id="count" name="count" value="<?php echo count($lunches);?>">
<div class="modal-body" id="popup">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <label style="font-weight:bold;">Date:</label> <?php echo date('d/m/Y', $model->created_at); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label style="font-weight:bold;">Time In:</label>  
                                        <?php echo $form->textField($model, 'time_in', array('class' => 'form-control timepickerIn disableEle', 'style'=> 'width:75%')); ?> 
                                </div><script>convertToLocal('<?php echo date('h:i A',($model->time_in+($matches[0][0]*3600))-3600);?>','#UserTimings_time_in');</script>
                                <div class="col-md-6" style="padding-left:0;">
                                    <label style="font-weight:bold;">Time Out:</label> <?php echo $form->textField($model, 'time_out', array('class' => 'form-control timepickerIn disableEle', 'style'=> 'width:75%')); ?> <?php //echo date('H:i a', $model->time_out); ?>
                                </div><script>convertToLocal('<?php echo date('h:i A',($model->time_out+($matches[0][0]*3600)-3600));?>','#UserTimings_time_out');</script>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <h2 style="font-family:'Avenir Next','Avenir Next LT W01 SC','Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: bold;font-size:16px;color:lightgray;">Breaks</h2>
                                </div>
                            </div>
                            <table id="myTable" class="table-responsive">
                            <?php foreach ($lunches as $lunch) { ?>
                                <tr>
                            <td class="col-md-3" style="padding-left:0;">
                                        <label style="font-weight:bold;">Break In:</label> <?php echo $form->textField($lunch, 'lunch_in', array('class' => 'form-control timepickerIn disableEle', 'id'=>'lunch_in'.$counter, 'name'=>'lunch_in'.$counter,'style'=> 'width:75%')); ?> <?php //echo date('H:i a', $lunch->lunch_in); ?>
                            </td><script>convertToLocal('<?php echo date('h:i A',($lunch->lunch_in+($matches[0][0]*3600))-3600);?>','#lunch_in<?php echo $counter?>');</script>
                            <td class="col-md-3" style="padding-left:0;">
                                        <label style="font-weight:bold;">Break Out:</label> <?php echo $form->textField($lunch, 'lunch_out', array('class' => 'form-control timepickerIn disableEle', 'id'=>'lunch_out'.$counter, 'name'=>'lunch_out'.$counter,'style'=> 'width:75%')); ?> <?php //echo date('H:i a', $lunch->lunch_out); ?>
                            </td><script>convertToLocal('<?php echo date('h:i A',($lunch->lunch_out+($matches[0][0]*3600))-3600);?>','#lunch_out<?php echo $counter?>');</script>
                                </tr>
                            <?php $counter++; } ?>
                            </table>
                            <div class="separator"></div><br>
                            <div class="row">
                                   <div class="col-md-6">
                                       <a href="javascript:void(0)" onclick="addItem()" class="btn btn-default">Add More</a>
                                   </div>
                            </div>
                        </div>
                    </div>
                </div>
<?php $this->endWidget(); ?>

<script>
jQuery(document).ready(function(){
     $(".timepickerIn").timepicker({
                    showInputs: false,
                    timeFormat: 'hh:mm p'
                });
});
function addItem() {
        var count = $("#count").val();
        var countVal = count;
        countVal++;
        var table = document.getElementById("myTable");

        // Create an empty <tr> element and add it to the 1st position of the table:
        var row = table.insertRow(count);
        // Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var label = document.createElement("LABEL");
        label.style.fontWeight = 'bold';
        var node = document.createTextNode("Break In:");
        label.appendChild(node);
        var item = document.createElement("INPUT");
        var i = 'lunch_in';
        var itemName = i.concat(countVal);
        item.setAttribute("type", "text");
        item.setAttribute("name", itemName);
        item.setAttribute("class", "form-control timepickerIn disableEle");
//        item.setAttribute("readonly","readonly");
        item.setAttribute("style","width:75%");
        var label1 = document.createElement("LABEL");
        label1.style.fontWeight = 'bold';
        var node1 = document.createTextNode("Break Out:");
        label1.appendChild(node1);
        var itemValue = document.createElement("INPUT");
        i = 'lunch_out';
        itemName = i.concat(countVal);
        itemValue.setAttribute("type", "text");
        itemValue.setAttribute("name", itemName);
        itemValue.setAttribute("id", itemName);
        itemValue.setAttribute("class", "form-control timepickerIn disableEle");
//        itemValue.setAttribute("readonly","readonly");
        itemValue.setAttribute("style","width:75%");

        cell1.appendChild(label);
        cell1.appendChild(item);
        cell2.appendChild(label1);
        cell2.appendChild(itemValue);
        var i = document.createElement("i");
        i.setAttribute("class","fa fa-times");
        var anchor = document.createElement("a");
        var anchorName = 'anchor'.concat(countVal);
        anchor.setAttribute("name",anchorName);
        anchor.setAttribute("href","javascript:void(0);");
        anchor.setAttribute("onclick","$(this).closest('tr').remove();$('#count').val($('#count').val()-1);");
        anchor.appendChild(i);
        anchor.setAttribute("id","close");
        cell2.appendChild(anchor);
        cell1.className = 'col-md-3';
        cell2.className = 'col-md-3';
        cell1.style.paddingLeft = '0';
        cell2.style.paddingLeft = '0';
        $("#count").val(countVal);
        $(".timepickerIn").timepicker({
                    showInputs: false,
                    timeFormat: 'hh:mm p'
                });
    }

</script>