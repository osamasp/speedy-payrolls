<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
preg_match_all('!\d+!', Yii::app()->session["client_timezone"], $matches);
$not_included_fields = array('id', 'password', 'first_login', 'notif_seen_at', 'token', 'validity', 'reason', 'modified_by', 'created_by', 'modified_at', 'contract_id');
?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'myForm',
    'enableAjaxValidation' => false,
        ));
?>
<input type="hidden" id="export_url" value="<?php echo $this->createUrl('shiftexport'); ?>">
<input type="hidden" id="update_shift_url" value="<?php echo $this->createUrl('updateshift'); ?>">
<input type="hidden" id="view_url" value="<?php echo $this->createUrl('viewdetails'); ?>">
<input type="hidden" id="select_msg" value="<?php echo ConstantMessages::$req_select; ?>">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/company-companyshifts-clockentries.js" type="text/javascript"></script>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/dropdowns-enhancement.css" rel="stylesheet">
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-hourglass"></i>Clock Entries
        </div>
    </div>
    <div class="portlet-body form">
        <div class="row">

            <div class="col-md-12">

                <div class="btn-group pull-right" style="padding-left:10px;">
                    <!--<a class="btn btn-success pull-right btn-circle" href="<?php // echo $this->createUrl('add');  ?>">Add Employee</a>-->
                </div>
                <?php if (AppUser::isUserAdmin()) { ?>
                    <div class="btn-group pull-right "style="margin-top:5px; margin-right: 22px;">
                        <a href="javascript:void(0);" class="btn green dropdown-toggle" id='openButton'>Export <i class="fa fa-angle-down"></i>
                        </a>
                        <ul id="myDropdown" class="dropdown-menu pull-right">				
                            <li><a id="export" href="javascript:void(0);" onclick="exportData('export')">Export Selected Record</a></li>
                            <li class="divider"></li>
                            <li><a href="javascript:void(0);" id="export_all" onclick="exportData('export_all')">Export All Record</a></li>
                            <?php if (isset($my_shifts[0])) { ?>
                                <li><input type="checkbox" id="All" value="1"> <label id='selectAll' for="All">Select All</label></li>
                                <?php
                                foreach ($my_shifts[0] as $key => $item) {
                                    if (!in_array($key, $not_included_fields)) {
                                        ?>
                                        <li><input class="select" type="checkbox" name="export_fields[]" id="<?php echo $key; ?>" value="<?php echo $key; ?>"><label for="<?php echo $key; ?>"><?php echo UserTimings::model()->getAttributeLabel($key); ?></label></li>
                                    <?php
                                    }
                                }
                            }
                            ?>
                        </ul>
                    </div>
<?php } ?>
            </div>
        </div>

        <div class="box-body table-responsive" style="padding-left:20px;padding-top:10px;">
        </div>
        <div style="padding-left:10px;padding-right:10px;">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th><input type="checkbox" data-checkbox="true" class="group-checkable" id="checkAll" onchange="" data-set="#sample_1 .checkboxes"/></th>
                        <th>Date</th>
                        <th><?php echo UserTimings::model()->getAttributeLabel('time_in'); ?></th>
                        <th><?php echo UserTimings::model()->getAttributeLabel('time_out'); ?></th>
                        <th><?php echo UserTimings::model()->getAttributeLabel('total_time'); ?></th>
                        <th><?php echo UserTimings::model()->getAttributeLabel('total_lunch'); ?></th>
                        <th>Action</th>
                    </tr>
                </thead>
                <script>
                    function convertToLocal(times) {
                        if (times == '01/01/1970 01:00 UTC' || times == '01/01/1970 01:00 am UTC')
                        {
                            return "00:00";
                        }
                        else {
                            var date = new Date(times);
                            var hours = date.getHours();
                            hours = (hours < 10 ? "0" : "") + hours;
                            var minute = date.getMinutes();
                            var ampm = hours >= 12 ? 'pm' : 'am';
                            hours = hours % 12;
                            hours = hours ? hours : 12; // the hour '0' should be '12'
                            minute = minute < 10 ? '0' + minute : minute;
                            var strTime = hours + ':' + minute + ' ' + ampm;
                            return strTime;
                        }
                    }
                </script>
<?php foreach ($my_shifts as $shift) { ?>
                    <tr>
                        <td><input type="checkbox" name="entry[]" class="check" value="<?php echo $shift->id; ?>"  style="width: 35px;"/></td>
                        <td><?php echo date('d/m/Y', $shift->created_at); ?></td>
                        <td>
                            <?php echo date('h:i A', ($shift->time_in + ($matches[0][0] * 3600)) - 3600); ?>
                        </td>
                        <td>
                            <?php if ($shift->time_out == 0) {
                                echo "--";
                            } else {
                                echo date('h:i A', ($shift->time_out + ($matches[0][0] * 3600)) - 3600);
                            } ?>
                        </td>
                        <td><?php
                            if ($shift->total_time == 0) {
                                echo "0:0";
                            } else {
                                if ($shift->total_time < 3600) {
                                    echo "0:" . date('i', $shift->total_time);
                                } else {
                                    echo AppInterface::secondsToTime($shift->total_time, array("h", "i"));
                                }
                            }
                            ?></td>
                        <td><?php
                        if ($shift->total_lunch == 0) {
                            echo "0:0";
                        } else {
                            if ($shift->total_lunch < 3600) {
                                echo "0:" . date('i', $shift->total_lunch);
                            } else {
                                echo AppInterface::secondsToTime($shift->total_lunch, array("h", "i"));
                            }
                        }
                        ?></td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn green dropdown-toggle" data-toggle="dropdown">
                                    Actions<i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a id="viewShift" data-toggle="modal" data-target=".ajaxModel" data-whatever="<?php echo $shift->id; ?>">View</a></li>
                                    <li class="divider"></li>
                                    <li><a id="updateShift" data-toggle="modal" data-target=".Model" data-whatever="<?php echo $shift->id; ?>">Update</a></li>
                                    <!--<li class="divider"></li>-->
                                    <!--<li><a href="<?php // echo $this->createUrl('delete', array('id' => $shift->id));   ?>">Delete</a></li>-->
                                </ul>
                            </div>
                        </td>
                    </tr>
<?php } ?>
            </table>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>
