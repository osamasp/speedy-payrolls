<?php
/* @var $this CompanyShiftsController */
/* @var $model CompanyShifts */
/* @var $form CActiveForm */
?>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'company-shifts-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => false,
        ));
?> 
<div class="box-body"style="padding-left:20px; padding-right:20px;">
   
    <?php if ($model->hasErrors()) { ?>
        <div class='callout callout-danger'>    <?php echo $form->errorSummary($model); ?>
        </div>
        <?php } ?>    
    <div >
        <?php echo $form->labelEx($model, 'title'); ?>
<?php echo $form->textField($model, 'title', array('size' => 60, 'maxlength' => 255,'required'=>'required', 'class' => 'form-control')); ?>
<?php echo $form->error($model, 'title', array('class' => 'text-red')); ?>
    </div>
    <div  style="margin-bottom:50px;">
        <div class="bootstrap-timepicker"  ><div class="bootstrap-timepicker-widget dropdown-menu"><table><tbody><tr><td><a href="#" data-action="incrementHour"><i class="glyphicon glyphicon-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="incrementMinute"><i class="glyphicon glyphicon-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td class="meridian-column"><a href="#" data-action="toggleMeridian"><i class="glyphicon glyphicon-chevron-up"></i></a></td></tr><tr><td><span class="bootstrap-timepicker-hour">10</span></td> <td class="separator">:</td><td><span class="bootstrap-timepicker-minute">45</span></td> <td class="separator">&nbsp;</td><td><span class="bootstrap-timepicker-meridian">PM</span></td></tr><tr><td><a href="#" data-action="decrementHour"><i class="glyphicon glyphicon-chevron-down"></i></a></td><td class="separator"></td><td><a href="#" data-action="decrementMinute"><i class="glyphicon glyphicon-chevron-down"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="toggleMeridian"><i class="glyphicon glyphicon-chevron-down"></i></a></td></tr></tbody></table></div>

                        <?php echo $form->labelEx($model, 'start_time'); ?>
                <?php echo $form->textField($model, 'start_time', array('size' => 20, 'maxlength' => 20, 'class' => 'form-control col-md-8 timepickerIn disableEle','readonly'=>'readonly','style'=>'cursor:context-menu')); ?>

<?php echo $form->error($model, 'start_time', array('class' => 'text-red')); ?>

        </div>
    </div>
    <div >
        <div class="bootstrap-timepicker"><div class="bootstrap-timepicker-widget dropdown-menu"><table><tbody><tr><td><a href="#" data-action="incrementHour"><i class="glyphicon glyphicon-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="incrementMinute"><i class="glyphicon glyphicon-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td class="meridian-column"><a href="#" data-action="toggleMeridian"><i class="glyphicon glyphicon-chevron-up"></i></a></td></tr><tr><td><span class="bootstrap-timepicker-hour">10</span></td> <td class="separator">:</td><td><span class="bootstrap-timepicker-minute">45</span></td> <td class="separator">&nbsp;</td><td><span class="bootstrap-timepicker-meridian">PM</span></td></tr><tr><td><a href="#" data-action="decrementHour"><i class="glyphicon glyphicon-chevron-down"></i></a></td><td class="separator"></td><td><a href="#" data-action="decrementMinute"><i class="glyphicon glyphicon-chevron-down"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="toggleMeridian"><i class="glyphicon glyphicon-chevron-down"></i></a></td></tr></tbody></table></div>

            <?php echo $form->labelEx($model, 'end_time'); ?>
<?php echo $form->textField($model, 'end_time', array('size' => 20, 'maxlength' => 20, 'class' => 'form-control col-md-8 timepickerOut disableEle','readonly'=>'readonly','style'=>'cursor:context-menu')); ?>
<?php echo $form->error($model, 'end_time', array('class' => 'text-red')); ?>

        </div>
    </div>
</div>

<div class="form-actions right" style="margin-top:50px;">
    <div class="row">
        
<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-circle green-haze')); ?>
        
    </div>
</div>

<?php $this->endWidget(); ?>
<script>
jQuery(document).ready(function(){
     $(".timepickerIn").timepicker({
                    showInputs: false,
                    defaultTime: '09:00 AM'
                });
                    
                  $(".timepickerOut").timepicker({
                    showInputs: false,
                    defaultTime: '06:00 PM'
                });
});
</script>