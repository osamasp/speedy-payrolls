<?php
/* @var $this CompanyShiftsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Company Shifts',
);

//$this->menu=array(
//array('label'=>'Create CompanyShifts', 'url'=>array('create')),
//array('label'=>'Manage CompanyShifts', 'url'=>array('admin')),
//);
$not_included_fields = array('id', 'password', 'first_login', 'notif_seen_at', 'token', 'validity', 'reason', 'modified_by', 'created_by', 'modified_at', 'contract_id', 'created_at');
?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'myForm',
    'enableAjaxValidation' => false,
        ));
?>
<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                Widget settings form goes here
            </div>
            <div class="modal-footer">
                <button type="button" class="btn blue">Save changes</button>
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<input type="hidden" id="select_msg" value="<?php echo ConstantMessages::$req_select; ?>">
<input type="hidden" id="url" value="<?php echo $this->createUrl('export'); ?>">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/user-employee-index.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/company-companyshifts-index.js" type="text/javascript"></script>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/dropdowns-enhancement.css" rel="stylesheet">
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-copy"></i>Company Shifts
        </div>
    </div>
    <div class="portlet-body table-responsive">
        <div class="row">

            <div class="col-md-12">

                <div class="btn-group pull-right" style="padding-left:10px;"></div>
                <?php if (AppUser::isUserAdmin()) { ?>
                    <div class="btn-group pull-right">
                        <a href="javascript:void(0)" class="btn green dropdown-toggle" id="openButton">Export <i class="fa fa-angle-down"></i>
                        </a>

                        <ul id="myDropdown" class="dropdown-menu pull-right">				
                            <li><a href="javascript:void(0);" onclick="exportData()">Export Selected Record</a></li>
                            <li class="divider"></li>
                            <li><a onclick='exportData()'>Export All Record</a></li>
                            <li><input type="checkbox" id="All" value="1"> <label id="checkAll" for="All">Select All</label></li>

                            <?php
                            foreach ($dataProvider[0] as $key => $item) {
                                if (!in_array($key, $not_included_fields)) {
                                    ?>
                                    <li>
                                        <input class="select" type="checkbox" name="export_fields[]" id="<?php echo $key; ?>" 
                                               value="<?php echo $key; ?>">
                                        <label for="<?php echo $key; ?>">
                                            <?php echo CompanyShifts::model()->getAttributeLabel($key); ?>
                                        </label>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </div>
                <?php } ?>
            </div>
        </div>

        <div class="box-body table-responsive" style="padding-top:10px;">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                             <!--<th><?php // echo CompanyShifts::model()->getAttributeLabel('id')            ?></th>-->
                        <th class="table-checkbox">
                            <input type="checkbox" data-checkbox="true" class="group-checkable" id="checkAll" onchange="" data-set="#sample_1 .checkboxes"/>
                        </th>
                        <th><?php echo CompanyShifts::model()->getAttributeLabel('title') ?></th>
                        <th><?php echo CompanyShifts::model()->getAttributeLabel('start_time') ?></th>
                        <th><?php echo CompanyShifts::model()->getAttributeLabel('end_time') ?></th>
                        <!--<th><?php // echo CompanyShifts::model()->getAttributeLabel('company_id')            ?></th>-->
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dataProvider as $item) { ?>
                        <tr>
                                                                    <!--<td><?php // echo $item->id;            ?></td>-->
                            <td><input type="checkbox" class="check" style="width: 35px;"></td>
                            <td><?php echo $item->title; ?></td>
                            <td><?php echo date('h:i a', $item->start_time); ?></td>
                            <td><?php echo date('h:i a', $item->end_time); ?></td>
                            <!--<td><?php // echo $item->company_id;            ?></td>-->
                            <td>


                                <div class="btn-group">
                                    <button type="button" class="btn green dropdown-toggle" data-toggle="dropdown">
                                        Actions<i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a id="viewShift" data-toggle="modal" data-target=".ajaxModel" data-whatever="<?php echo $item->id; ?>">View</a></li>
                                        <li><a href="<?php echo $this->createUrl('update', array('id' => $item->id)); ?>">Update</a></li>
                                        <li class="divider"></li>
                                        <li><a href="javascript:void(0)" onclick="confirmDelete(<?php echo $item->id; ?>)">Delete</a></li>
                                    </ul>
                                </div>

                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php $this->endWidget(); ?>
<script>
    function confirmDelete(id)
    {
        var conf = confirm("Are you sure? You want to delete.");
        if (conf)
        {
            var url = '<?php echo Yii::app()->baseUrl; ?>';
            url = url.concat('/company/companyShifts/delete/id/' + id);
            window.location.href = url;
        }
    }

    $('.ajaxModel').on('show.bs.modal', function(event) {
//                                           alert("2");         
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('whatever') // Extract info from data-* attributes
        var modal = $(this)
        var url = '<?php echo Yii::app()->baseUrl; ?>';
        url = url.concat('/company/companyShifts/view');
        $.ajax({
            type: "GET",
            url: url,
            data: {'id': recipient},
            beforeSend: function(jqXHR, settings) {
                jQuery(modal).find('.modal-content').text("Loading..........");

            },
            success: function(response) {
                if (response.status == true) {
                    jQuery(modal).find('.modal-content').html(response.data);
                } else {
                    jQuery(modal).modal('hide');
                    alert(response.reason);

                }
            },
            dataType: 'json'
        });
    });

    $('#myDropdown .dropdown-menu').on({
        "click": function(e) {
            e.stopPropagation();
        }
    });

    function submitForm()
    {
        if ($(".check").is(":checked"))
        {
            $("#myForm").submit();
        }
        else {
            alert($('#select_msg').val());
            return;
        }
    }

    
    $('#checkAll').click(function(event) {  //on click 
        if ($("#All").is(":checked")) { // check select status
            $('.select').each(function() { //loop through each checkbox
                this.checked = false;  //select all checkboxes with class "checkbox1"               
            });
        } else {

            $('.select').each(function() { //loop through each checkbox
                this.checked = true; //deselect all checkboxes with class "checkbox1"                       
            });
        }
    });
    $('#openButton').on('click', function(event) {
        $(this).parent().toggleClass('open');
    });
</script>
