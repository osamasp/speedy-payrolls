<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//dd($model);
//dd(date_default_timezone_get());
preg_match_all('!\d+!', Yii::app()->session["client_timezone"], $matches);
$initial_time = 0;
$in_lunch = false;
$lunch_time = 0;
$total_lunch = 0;
foreach ($lunches as $lunch) {
    if ($lunch->lunch_in != 0 && $lunch->lunch_out == 0) {
        $in_lunch = true;
        $_lunch = $lunch;
        $lunch_time = time() - $lunch->lunch_in;
        $total_lunch += $lunch_time;
    } else {
        $total_lunch += ($lunch->lunch_out - $lunch->lunch_in);
    }
}
if ($timeIn) {
    if ($model->time_in != 0) {
        $initial_time = (time() - $model->time_in) - $total_lunch;
    }
}

$this->breadcrumbs = array(
    'Company Shifts' => array('index'),
    'TimeIn/TimeOut',
);
?>

<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/timesheet.css" rel="stylesheet" type="text/css">
<div class="row">
    <div class="col-md-8" style="height:auto;min-height:100px;">
        <div class="portlet light" style="height:100%;">
            <div class="portlet-title hide-on-mobile">
                <div class="caption">
                    <i class="fa fa-clock-o"></i>Set Clock
                </div>
            </div>
            <div class="portlet-body">
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'timeInOut-form',
                    // Please note: When you enable ajax validation, make sure the corresponding
                    // controller action is handling ajax validation correctly.
                    // There is a call to performAjaxValidation() commented in generated controller code.
                    // See class documentation of CActiveForm for details on this.
                    'enableAjaxValidation' => false,
                ));
                ?>

                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6  col-xs-12">
                            <div class="form-group">

                                <select class="form-control select2_category" id="contract" onchange="changeContract()" name="contract">
                                    <?php foreach ($contracts as $contract) { ?>
                                        <option value="<?php echo $contract->id; ?>" <?php
                                        if ($model->contract_id == $contract->id) {
                                            echo 'selected="selected"';
                                        }
                                        ?>><?php echo AppContract::getContractCustomName($contract); ?></option>
                                            <?php } ?>
                                </select>

                            </div>
                        </div>
                        <div class="col-md-6  col-xs-12">
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="button" class="btn btn-md btn-sm btn-xs blue"><i class="fa fa-check"></i>TIMER</button>
                                        <button type="button" onclick="daily()" class="btn btn-md btn-sm btn-xs default">HRS INPUT</button>
                                        <button type="button" onclick="custom()" class="btn btn-md btn-sm btn-xs default">PERIODIC INPUT</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row margin-top-20">
                    <div class="col-md-9 margin-top-20 col-xs-12">

                        <?php
                        if ($model->time_in == 0 && $model->time_out == 0) {
                            $this->renderPartial('clock_elements', array('case' => 'in'));
                        } else if ($model->time_in > 0 && $model->time_out > 0) {
                            $this->renderPartial('clock_elements', array('case' => 'out'));
                        } else if ($model->time_in > 0 && $in_lunch == false) {
                            $this->renderPartial('clock_elements', array('case' => 'out_lunch'));
                        } else if ($model->time_in > 0 && $in_lunch) {
                            $this->renderPartial('clock_elements', array('case' => 'in_lunch'));
                        }
                        ?>

                    </div>
                    <div class="col-md-3 margin-top-20 col-xs-12">
                        <div id="pull_left_timer">
                            <h2 class="timeLogLabel">Date:<?php echo " " . date('d M, Y'); ?></h2>
                            <div id="clock" style="margin-bottom:10px;">
                                <h2 class="timeLogLabel">Time Log</h2>
                                <div id="clocks"><?php echo $this->renderPartial('clocks', array('model' => $model, 'lunches' => $lunches, 'lunch_id' => isset($_lunch) ? $_lunch->id : 0)); ?></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="todo-content">
            <div class="portlet light">
                <?php echo $this->renderPartial('notes', array('notes' => $notes, 'canAddNote' => ($model->time_in != 0) ? true : false)); ?>
            </div>
        </div>
    </div>
    <input type="hidden" id="types" value="in">
    <input type="hidden" id="id" value="<?php echo $model->id; ?>" name="id">
    <?php $this->endWidget(); ?>
</div>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/thirdparty/js/timer.jquery.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function () {
        setContract();

        if ($(window).width() <= 480) {
            $("#timeInOut-form div").removeClass("margin-top-20");
        }
        $(window).resize(function () {
            if ($(window).width() <= 480) {
                $("#timeInOut-form div").removeClass("margin-top-20");
            } /*else if (!$("#timeInOut-form div").hasClass("margin-top-20")) {
                $("#timeInOut-form div").addClass("margin-top-20");
            }*/
        });
        var hasTimer = false;
        var time = <?php echo $initial_time ?>;
        var lunchtime = <?php echo $lunch_time ?>;
<?php if ($in_lunch) {
    ?>
            $("#lunchTimer").show();
            $("#lunchTimer").timer({
                format: '%H:%M:%S',
                seconds: lunchtime
            });
<?php } else if ($timeIn) { ?>
            $("#timeIn").show();
            $('#timeIn').timer({
                format: '%H:%M:%S',
                seconds: time
            });
            $("#clockOutBtn").switchClass("clockOutBtn-dt", "clockOutBtn-active", 1000);
            $("#lunchBtn").switchClass("lunchBtn-dt", "lunchBtn-active", 1000);
<?php } ?>
        clock();
        clockHour();
        // Init timer start
        $('#clockInBtn').on('click', function () {
            var url = '<?php echo Yii::app()->baseUrl; ?>';
            url = url.concat('/company/companyShifts/timein');
            $.ajax({
                url: url,
                type: 'GET',
                data: {'contract': $("#contract").val()},
                success: function (response) {
                    if (response == "null") {
                        alert("You've already clocked in for today......");
                    } else {

                        $("#note").show();
                        $('#clocks').html(response);
//                                    $("#clockInBtn").animate({"marginTop": "+=30px"});
                        $("#clockInBtn").hide();
                        $("#workTimeCounter").show();
                        $("#lunchBtn").switchClass("lunchBtn-dt", "lunchBtn-active", 1000);
                        $("#clockOutBtn").switchClass("clockOutBtn-dt", "clockOutBtn-active", 1000);

                        $("#lunchBtn").show();
                        $("#clockOutBtn").show();
                        if ($(document).width() > 640) {
                            $("#clock").show();
                        }
//            hasTimer = true;
                        $("#timeIn").show();
                        $('#timeIn').timer({
                            format: '%H:%M:%S'
                        });
                    }
                }
            });
        });


        // Init timer resume
        $('#endLunchBtn').on('click', function () {
            var url = '<?php echo Yii::app()->baseUrl; ?>';
            url = url.concat('/company/companyShifts/lunchout');
            $.ajax({
                url: url,
                data: {time_id: $("#time_id").val(), lunch_id: $("#lunch_id").val()},
                type: 'GET',
                success: function (response) {
                    if (response == null) {
                        alert("You've already clocked in for today......");
                    } else {
                        $('#clocks').html(response);
                        // Init timer pause
                    }
                }});
            $("#lunchTimer").timer('remove');
            $("#timeIn").show();
            $('#timeIn').timer({
                format: '%H:%M:%S',
                seconds: time
            });
            var inLunch = '<?php echo $in_lunch ?>';
            $("#lunchCounter").hide();
            $("#workTimeCounter").show();
//            $("#lunchTimer").timer('remove');
            $("#endLunchBtn").hide();
            $("#workTimeCounter").show();
            $("#lunchCounter").hide();
            $("#clockOutBtn").switchClass("clockOutBtn-dt", "clockOutBtn-active", 1000);
            $("#lunchBtn").switchClass("lunchBtn-dt", "lunchBtn-active", 1000);
            if (inLunch == 'true' || inLunch == '1') {
                $("#clockOutBtn").switchClass("clockOutBtn-dt", "clockOutBtn-active", 1000);
                $("#lunchBtn").switchClass("lunchBtn-dt", "lunchBtn-active", 1000);
                $("#lunchCounter").hide();
                inLunch = '0';
            }
            $("#lunchBtn").show();
            $("#clockOutBtn").show();
            clock();
//            $.timer().stop().timeOut();
        });
        
        // Remove timer
        $('#lunchBtn').on('click', function () {
            var url = '<?php echo Yii::app()->baseUrl; ?>';
            url = url.concat('/company/companyShifts/lunchin');
            $.ajax({
                url: url,
                data: {time_id: $("#time_id").val()},
                type: 'GET',
                success: function (response) {
                    if (response == "null") {
                        alert("You've reached maximum lunch breaks");
                    } else {
                        $('#clocks').html(response);
                        $("#endLunchBtn").show();
                        $("#lunchCounter").show();
                        time = $("#timeIn").data('seconds');

                        $("#timeIn").timer('remove');
                        $('#lunchTimer').timer({
                            format: '%H:%M:%S',
                            seconds: 0
                        });
                        $("#clockOutBtn").hide();
                        $("#lunchBtn").hide();
                        $("#workTimeCounter").hide();
                        $("#clockOutBtn").switchClass("clockOutBtn-active", "clockOutBtn-dt", 0);
                        $("#lunchBtn").switchClass("lunchBtn-active", "lunchBtn-dt", 0);
                    }
                }
            });
        });

        $('#note').on('click', function () {
//                                           alert("2");
            var recipient = $("#id").val(); // Extract info from data-* attributes
            var modal = $("#portlet-config");
            $("#portlet-config").modal('toggle');
            var url = '<?php echo Yii::app()->baseUrl; ?>';
            url = url.concat('/company/companyShifts/addnote');
            $.ajax({
                type: "GET",
                url: url,
                data: {'id': recipient, 'contract_id': $("#contract").val()},
                beforeSend: function (jqXHR, settings) {
                    jQuery(modal).find('.modal-title').html('<h2 style="font-family:\'Avenir Next\',\'Avenir Next LT W01 SC\',\'Helvetica Neue\', Helvetica, Arial, sans-serif;font-weight: bold;font-size:24px;color:#7b8cb8;">ADD NOTE</h2>')
                    jQuery(modal).find('.modal-body').text("Loading..........");

                },
                success: function (response) {
                    if (response.status == true) {
                        jQuery(modal).find('.modal-body').html(response.data);
                    } else {
                        jQuery(modal).modal('hide');
                        alert(response.reason);

                    }
                },
                dataType: 'json'
            });
        $("#saveModel").off('click');
        $("#saveModel").on('click', function () {
            $("#add-note-form").submit();
        $("#saveModel").off('click');
            $("#saveModel").on('click', function () {
                           var url = '<?php echo Yii::app()->baseUrl; ?>';
                url = url.concat('/company/companyShifts/timeout');
                $.ajax({
                    url: url,
                    data: {time_id: $("#time_id").val(), total_time: $("#timeIn").html()},
                    type: 'GET',
                    success: function (response) {
                        if (response == null) {
                            alert("You've already clocked in for today......");
                        } else {
                            $('#clocks').html(response);
                            $("#note").hide();
                            jQuery(_modal).modal('hide');
                            $("#bye_msg").show();
                            // Init timer pause
                        }
                    }
                });
                $("#timeIn").hide();
                $('#timeIn').timer('pause');
                $("#lunchBtn").animate({"marginLeft": "+=70px"});
                $("#clockOutBtn").animate({"marginLeft": "-=70px"});
                $("#lunchBtn").hide();
                $("#clockOutBtn").hide();
                $("#workTimeCounter").hide();
//                        $("#clockInBtn").show();
                $("#clockInBtn").animate({"marginTop": "-=30px"});
                $("#clockOutBtn").animate({"marginTop": "-=50px"});
                $("#lunchBtn").animate({"marginTop": "-=50px"});
                    });
        });
        });
});
    function daily()
    {
        var url = '<?php echo Yii::app()->createUrl("/timesheet/main/addwizard/types"); ?>';
        url = url.concat('/' + $("#types").val());
        $.post(url,
                {
                    contract: $("#contract").val(),
                    type: 1,
                    ajax: 1,
                    yt0: 'Add',
                },
                function (result) {
                    if (result)
                    {
                        var url = '<?php echo Yii::app()->createUrl('/timesheet/main/add/contract'); ?>';
                        url = url.concat('/' + $("#contract").val() + '/types/' + $("#types").val());
                        window.location = url;
                    }
                }
        );
    }
    function custom() {
        var url = '<?php echo Yii::app()->createUrl("/timesheet/main/addwizard/types"); ?>';
        url = url.concat('/' + $("#types").val());
        $.post(url,
                {
                    contract: $("#contract").val(),
                    type: 2,
                    ajax: 1,
                    yt0: 'Add',
                },
                function (result) {
                    if (result)
                    {
                        var url = '<?php echo Yii::app()->createUrl('/timesheet/main/add/contract'); ?>';
                        url = url.concat('/' + $("#contract").val() + '/types/' + $("#types").val());
                        window.location = url;
                    }
                });
    }
    function setContract() {
        var contract = $("#contract option:selected").text();
        if (contract.indexOf("Out -") != -1) {
            $("#types").val("out");
        } else if (contract.indexOf("In Out") != -1 || contract.indexOf("Out B") != -1) {
            $("#types").val("out_b");
        } else if (contract.indexOf("Internal") != -1) {
            $("#types").val("in");
        }
    }
    function changeContract()
    {
        var contract = $("#contract option:selected").text();
        if (contract.indexOf("Out -") != -1) {
            $("#types").val("out");
        } else if (contract.indexOf("In Out") != -1 || contract.indexOf("Out B") != -1) {
            $("#types").val("out_b");
        } else if (contract.indexOf("Internal") != -1) {
            $("#types").val("in");
        }

        var url = '<?php echo Yii::app()->createUrl("/company/companyShifts/alreadytimein/contract_id"); ?>';
        url = url.concat('/' + $("#contract").val());
        $.get(url,
                function (result) {
//                                                        $("#bye_msg").hide();
//                                                        $("#clockInBtn").show();
                    window.location = '<?php echo Yii::app()->createUrl("/company/companyShifts/timeinout/contract_id"); ?>' + '/' + $("#contract").val();
                });
    }
    function clock() {
        var time = 0;
        var $ht = $("#timeIn");
        var hour_time = $("#timeIn").html();
        hour_time = hour_time.split(":");
        var hour = (hour_time[0] * 60 * 60);
        var min = hour_time[1] * 60;
        var sec = hour_time[2];
        hour_time = hour + min + parseInt(sec);

        $ht.val(hour_time).trigger("change");
        setTimeout("clock()", 1000);
    }
    function clockHour() {
        var $hl = $(".hourLunch");
        var hour_time = $("#lunchTimer").html();
        hour_time = hour_time.split(":");
        var hour = (hour_time[0] * 60 * 60);
        var min = hour_time[1] * 60;
        var sec = hour_time[2];
        hour_time = hour + min + parseInt(sec);
        $hl.val(hour_time).trigger("change");
        setTimeout("clockHour()", 1000);
    }
</script>