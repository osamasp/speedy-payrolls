<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$emp = array();
$count = 0;
$total_hours = 0;
$total_lunch = 0;
$last_element = end($model);
//dd($model);
preg_match_all('!\d+!', Yii::app()->session["client_timezone"], $matches);
?>
<script>
    function convertToLocal(times) {
        if (times == '01/01/1970 01:00 UTC' || times == '01/01/1970 01:00 am UTC')
        {
            return "00:00";
        }
        else {
            var date = new Date(times);
            var hours = date.getHours();
            hours = (hours < 10 ? "0" : "") + hours;
            var minute = date.getMinutes();
            var ampm = hours >= 12 ? 'pm' : 'am';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minute = minute < 10 ? '0' + minute : minute;
            var strTime = hours + ':' + minute + ' ' + ampm;
            return strTime;
        }
    }
</script>
<!--<script>document.write(convertToLocal('<?php // echo date('m/d/Y h:i a', $item["time_in"]) . " UTC";   ?>'));</script>-->
<div class="page-content-wrapper" style="background-color: white;">
    <div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <?php if ($is_html) { ?>
                            <div class="caption" style="text-align: center;width: 100%;margin: 0 auto;">
                                <h1 style="font-family:'Avenir Next','Avenir Next LT W01 SC','Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: bold;font-size:24px;color:white;">Timesheet Report</h1>
                            </div>
                        <?php } else { ?>
                            <div class="caption" style="padding-left:200px;">
                                <h1 style="font-family:'Avenir Next','Avenir Next LT W01 SC','Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: bold;font-size:24px;color:#7b8cb8;">Timesheet Report</h1>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="portlet-body form">
                        <div class="box-body" style="padding-left:20px;padding-top:10px;padding-right:20px;background-color: none;">
                            <table class="table">
                                <tr>
                                    <td colspan="6">
                                        <div class="pull-left"><h2 style="font-family:'Avenir Next','Avenir Next LT W01 SC','Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: bold;font-size:20px;color:#7b8cb8;">TIMESHEETS</h2></div>
                                    </td>
                                    <td colspan="6" style="text-align: right;">
                                        <div class="pull-right"><h2 style="font-family:'Avenir Next','Avenir Next LT W01 SC','Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: bold;font-size:20px;color:lightgray;"><?php echo date('d/m/Y') ?></h2></div>
                                    </td>
                                </tr>
                            </table>
                            <div class="clearfix"></div>
                            <table class="table" style="padding-left:10px;">

                                <?php
                                if (!$date_group) {
                                    foreach ($model as $item) {
                                        if (!in_array($item["user_id"], $emp)) {
                                            ?>
                                            <tr>
                                                <td colspan="6" style="padding-top:20px;padding-bottom: 20px;background-color:white;">
                                                    <?php array_push($emp, $item["user_id"]); ?>
                                                    <div class="pull-left"><h3 style="font-family:'Avenir Next','Avenir Next LT W01 SC','Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: bold;font-size:14px;color:#7b8cb8;"><?php echo $item["first_name"] . " " . $item["last_name"]; ?></h3></div>

                                                </td>
                                                <td colspan="6" style="padding-top:20px;padding-bottom: 20px;text-align: right;background-color:white;">
                                                    <div class="pull-right"><h3 style="font-family:'Avenir Next','Avenir Next LT W01 SC','Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: bold;font-size:12px;color:lightgray;"><?php echo $item["email"]; ?></h3></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="8" style="padding-bottom: 10px;color:#7b8cb8;background-color: #f9f9f9;"><label>Date</label></td>
                                                <td style="padding-bottom: 10px;color:#7b8cb8;background-color: #f9f9f9;"><label>Time In</label></td>
                                                <td style="padding-bottom: 10px;color:#7b8cb8;background-color: #f9f9f9;"><label>Time Out</label></td>
                                                <td style="padding-bottom: 10px;color:#7b8cb8;background-color: #f9f9f9;"><label>Lunch</label></td>
                                                <td style="padding-bottom: 10px;color:#7b8cb8;background-color: #f9f9f9;"><label>Hours</label></td>
                                            </tr>
                                            <?php
                                        }
                                        if (isset($item["created_at"])) {
                                            ?>
                                            <tr>
                                                <td colspan="8"><label><?php echo date('d/m//Y', $item["created_at"]) ?></label></td>
                                                <td><label><?php
                                                        if (!$is_html) {
                                                            echo date('h:i a', ($item["time_in"] + ($matches[0][0] * 3600))-3600);
                                                        } else {
                                                             echo date('h:i A', ($item["time_in"] + ($matches[0][0] * 3600)-3600)); } ?></label></td>
                                                <td><label><?php
                                                        if ($item["time_out"] != 0) {
                                                            if (!$is_html) {
                                                                echo date('h:i a', ($item["time_out"] + ($matches[0][0] * 3600))-3600);
                                                            } else {
                                                                echo date('h:i A', ($item["time_out"] + ($matches[0][0] * 3600))-3600);
                                                            }
                                                        } else {
                                                            echo "No Entry";
                                                        }
                                                        ?></label></td>
                                                <td><label><?php
                                            if (isset($item["id"])) {
                                                $lunches = UserLunch::model()->findAllByAttributes(array("user_timing_id" => $item["id"]));
                                                foreach ($lunches as $lunch) {
                                                                ?>
                                                                <label><?php
                                                                    if ($lunch->lunch_out == 0) {
                                                                        
                                                                    } else {
                                                                        if (($lunch->lunch_out - $lunch->lunch_in) < 3600) {
                                                                            echo "00 H " . AppInterface::secondsToTime($lunch->lunch_out - $lunch->lunch_in, array("i")) . " M";
                                                                        } else {
                                                                            echo AppInterface::secondsToTime($lunch->lunch_out - $lunch->lunch_in, array("h")) . " H " . AppInterface::secondsToTime($lunch->lunch_out - $lunch->lunch_in, array("i")) . " M";
                                                                        }
                                                                        if (($lunch->lunch_out - $lunch->lunch_in) > 60) {
                                                                            $total_lunch += $lunch->lunch_out - $lunch->lunch_in;
                                                                        }
                                                                    }
                                                                    ?></label><br>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </label></td>
                                                <td><label><?php
                                                        if ($item != $last_element) {
                                                            if (in_array($item["user_id"], $emp)) {
                                                                $total_hours += $item["total_time"];
                                                            } else {
                                                                $total_hours = $item["total_time"];
                                                            }
                                                        } else {
                                                            $total_hours += $item["total_time"];
                                                        }
                                                        if ($item["total_time"] == 0) {
                                                            echo "00 <b>H</b> 00 <b>M</b>";
                                                        } else {
                                                            if ($item["total_time"] < 3600) {
                                                                echo "00 <b>H</b> " . AppInterface::secondsToTime($item["total_time"], array("i")) . " <b>M</b>";
                                                            } else {
                                                                echo AppInterface::secondsToTime($item["total_time"], array("h")) . " <b>H</b> " . AppInterface::secondsToTime($item["total_time"], array("i")) . " <b>M</b>";
                                                            }
                                                        }
                                                        ?></label></td>
                                            </tr>
            <?php
            $count++;
            if ($item != $last_element) {
                if ($item["user_id"] != $model[$count]["user_id"]) {
                    ?>
                                                    <tr>
                                                        <td colspan="6" style="color:#7b8cb8;background-color: #f9f9f9;">
                                                            <label>Total</label>
                                                        </td>
                                                        <td colspan="6" style="background-color: #f9f9f9;text-align: right;">
                                                            <?php
                                                            if ($total_hours == 0) {
                                                                echo "00 <b>H</b> 00 <b>M</b>";
                                                            } else {
                                                                if ($total_hours < 3600) {
                                                                    echo "00 <b>H</b> " . date('i', $total_hours) . " <b>M</b>";
                                                                } else {
                                                                    echo AppInterface::secondsToTime($total_hours, array("h")) . " <b>H</b> " . AppInterface::secondsToTime($total_hours, array("i")) . " <b>M</b>";
                                                                }
                                                            }$total_hours = 0;
                                                            ?>
                                                        </td>
                                                    </tr>
                                                        <?php }
                                                    } else {
                                                        ?>
                                                <tr>
                                                    <td colspan="6" style="color:#7b8cb8;background-color: #f9f9f9;">
                                                        <label>Total</label>
                                                    </td>
                                                    <td colspan="6" style="background-color: #f9f9f9;text-align: right;">
                                                <?php
                                                if ($total_hours == 0) {
                                                    echo "00 <b>H</b> 00 <b>M</b>";
                                                } else {
                                                    if ($total_hours < 3600) {
                                                        echo "00 <b>H</b> " . date('i', $total_hours - $total_lunch) . " <b>M</b>";
                                                    } else {
                                                        echo AppInterface::secondsToTime($total_hours - $total_lunch, array("h")) . " <b>H</b> " . AppInterface::secondsToTime($total_hours - $total_lunch, array("i")) . " <b>M</b>";
                                                    }
                                                }$total_hours = 0;
                                                $total_lunch = 0;
                                                ?>
                                                    </td>
                                                </tr>
                                            <?php
                                            }
                                        } else {
                                            $count++;
                                            ?>
                                            <tr>
                                                <td colspan="12">
                                                    <label>No Entry Found</label>
                                                </td>
                                            </tr>
                                                    <?php
                                                }
                                            }
                                        } else {
                                            foreach ($model as $item) {
                                                $count++;
                                                if ($item["created_at"] != 0) {
                                                    if (!in_array(date('d/m/Y', $item["created_at"]), $emp)) {
                                                        ?>
                                                <?php if ($count > 1) { ?>
                                                    <tr>
                                                        <td colspan="6" style="color:#7b8cb8;background-color: #f9f9f9;">
                                                            <label>Total</label>
                                                        </td>
                                                        <td colspan="6" style="background-color: #f9f9f9;text-align: right;">
                    <?php
                    if ($total_hours == 0) {
                        echo "00 <b>H</b> 00 <b>M</b>";
                    } else {
                        if ($total_hours < 3600) {
                            echo "00 <b>H</b> " . date('i', $total_hours - $total_lunch) . " <b>M</b>";
                        } else {
                            echo date('h', $total_hours - $total_lunch) . " <b>H</b> " . date('i', $total_hours - $total_lunch) . " <b>M</b>";
                        }
                    }$total_hours = 0;
                    $total_lunch = 0;
                    ?>
                                                        </td>
                                                    </tr>
                <?php } ?>
                                                <tr>
                                                    <td colspan="12" style="padding-top:20px;padding-bottom: 20px;background-color:white;">
                                                            <?php array_push($emp, date('d/m/Y', $item["created_at"])); ?>
                                                        <div class="pull-left"><h3 style="font-family:'Avenir Next','Avenir Next LT W01 SC','Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: bold;font-size:16px;color:#7b8cb8;"><?php echo date('d/m/Y', $item["created_at"]) ?></h3></div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="8" style="padding-bottom: 10px;color:#7b8cb8;background-color: #f9f9f9;"><label>Employee</label></td>
                                                    <td style="padding-bottom: 10px;color:#7b8cb8;background-color: #f9f9f9;"><label>Time In</label></td>
                                                    <td style="padding-bottom: 10px;color:#7b8cb8;background-color: #f9f9f9;"><label>Time Out</label></td>
                                                    <td style="padding-bottom: 10px;color:#7b8cb8;background-color: #f9f9f9;"><label>Lunch</label></td>
                                                    <td style="padding-bottom: 10px;color:#7b8cb8;background-color: #f9f9f9;"><label>Hours</label></td>
                                                </tr>

                                                            <?php
                                                        }
                                                        if (isset($item["created_at"])) {
                                                            ?>
                                                <tr>
                                                    <td colspan="8"><label><?php echo $item["first_name"] . " " . $item["last_name"]; ?></label></td>
                                                    <td><label><?php
                                                                if (!$is_html) {
                                                                    echo date('h:i a', $item["time_in"] + ($matches[0][0] * 3600));
                                                                } else {
                                                                echo date('h:i A',$item["time_in"] + ($matches[0][0] * 3600));} ?></label></td>
                                                    <td><label><?php if ($item["time_out"] != 0) { ?>
                                                                <?php
                                                                if (!$is_html) {
                                                                    echo date('h:i a', $item["time_out"] + ($matches[0][0] * 3600));
                                                                } else {
                                                                    echo date('h:i A',$item["time_out"] + ($matches[0][0] * 3600)); } ?>
                                                                <?php
                                                            } else {
                                                                echo "No Entry";
                                                            }
                                                            ?></label></td>
                                                    <td><label><?php
                                                            if (isset($item["id"])) {
                                                                $lunches = UserLunch::model()->findAllByAttributes(array("user_timing_id" => $item["id"]));
                                                                foreach ($lunches as $lunch) {
                                                                    ?>
                                                                    <label><?php
                                                                    if ($lunch->lunch_out == 0) {
                                                                        
                                                                    } else {
                                                                        if (($lunch->lunch_out - $lunch->lunch_in) < 3600) {
                                                                            echo "00 H " . date('i', ($lunch->lunch_out - $lunch->lunch_in)) . " M";
                                                                        } else {
                                                                            echo date('h', ($lunch->lunch_out - $lunch->lunch_in)) . " H " . date('i', ($lunch->lunch_out - $lunch->lunch_in)) . " M";
                                                                        }
                                                                    }
                                                                    ?></label><br>
                        <?php
                    }
                }
                ?>
                                                        </label></td>
                                                    <td><label><?php
                if ($item != $last_element) {
                    if (in_array(date('d/m/Y', $item["created_at"]), $emp)) {
                        $total_hours += $item["total_time"];
                    } else {
                        $total_hours = $item["total_time"];
                    }
                } else {
                    $total_hours += $item["total_time"];
                }
                if ($item["total_time"] == 0) {
                    echo "00 <b>H</b> 00 <b>M</b>";
                } else {
                    if ($item["total_time"] < 3600) {
                        echo "00 <b>H</b> " . date('i', $item["total_time"]) . " <b>M</b>";
                    } else {
                        echo date('h', $item["total_time"]) . " <b>H</b> " . date('i', $item["total_time"]) . " <b>M</b>";
                    }
                }
                ?></label></td>
                                                </tr>

                                                <!--Insert Total code here-->

                                                <?php if ($item == $last_element) { ?>
                                                    <tr>
                                                        <td colspan="6" style="color:#7b8cb8;background-color: #f9f9f9;">
                                                            <label>Total</label>
                                                        </td>
                                                        <td colspan="6" style="background-color: #f9f9f9;text-align: right;">
                                                    <?php
                                                    if ($total_hours == 0) {
                                                        echo "00 <b>H</b> 00 <b>M</b>";
                                                    } else {
                                                        if ($total_hours < 3600) {
                                                            echo "00 <b>H</b> " . date('i', $total_hours - $total_lunch) . " <b>M</b>";
                                                        } else {
                                                            echo date('h', $total_hours - $total_lunch) . " <b>H</b> " . date('i', $total_hours - $total_lunch) . " <b>M</b>";
                                                        }
                                                    }$total_hours = 0;
                                                    $total_lunch = 0;
                                                    ?>
                                                        </td>
                                                    </tr>
                <?php } ?>
            <?php
            } else {
                $count++;
                ?>
                                                <tr>
                                                    <td colspan="12">
                                                        <label>No Entry Found</label>
                                                    </td>
                                                </tr>
                <?php
            }
        }
    }
}
?>
                                <tr>
                                    <td colspan="6">

                                    </td>
                                    <td colspan="6"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>