<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php if ($case == "in") { ?>
    <div class="col-md-6 col-sm-6 col-xs-12 margin-top-20">
        <div class="tiles" style="height:auto;min-height:100px;">
            <span class="workingHours workTimeCounter" id="workTimeCounter" style="display: none;text-decoration: none; font-size: x-large;"><span id="timeIn" data-min="0" data-readOnly="true" data-thickness=".2" data-max="28800" class="timer knob hourTime"> Test</span></span>
            <a href="javascript:void(0)"  class="clockInBtn margin-top-20" id="clockInBtn" style="text-decoration: none; font-size: x-large;">In</a>
            <div class="pulllefttimer" style="height:10%;">
                <span class="workingHours workTimeCounter" id="lunchCounter" style="display: none;"><span style="text-decoration: none; font-size: x-large;"><span id="lunchTimer" class="timer1 knob hourLunch" data-min="0" data-readOnly="true" data-thickness=".2" data-max="28800" value="0" data-displayPrevious=true></span></span></span>
            </div>
            <div class="buttonBlocks" id="bye_msg" style="display:none;">
                <h2 class="bye_msg">Good Bye!</h2><h3 class="bye_msg" style="width:240px;"> See you tomorrow.</h3>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-6 margin-top-20">
        <div class="tiles">
            <a href="javascript:void(0)"  class="lunchBtn" id="lunchBtn" style="display: none;text-decoration: none; font-size: x-large;">Break</a>
            <a href="javascript:void(0)" class="endLunchBtn" id="endLunchBtn" style="display: none; font-size: x-large;">End Break</a>
        </div>
    </div>

    <div class="col-md-3 col-sm-3 col-xs-6 margin-top-20">
        <div class="tiles">
            <a href="javascript:void(0)"  data-toggle="modal" data-target=".Model"  class="clockOutBtn" id="clockOutBtn" style="display: none;text-decoration: none; font-size: x-large;">End</a>
        </div>
    </div>
<?php } else if ($case == "out") { ?>
    <div class="col-md-6 col-sm-6 col-xs-12 margin-top-20">
        <div class="tiles" style="height:auto;min-height:100px;">
            <span class="workingHours workTimeCounter" id="workTimeCounter" style="display: none;text-decoration: none;"><span style="text-decoration: none;"><span id="timeIn" data-min="0" data-readOnly="true" data-thickness=".2" data-max="28800" class="timer knob hourTime"> Test</span></span></span>
            <a href="javascript:void(0)"  class="clockInBtn margin-top-20" id="clockInBtn" style="display: none;text-decoration: none; font-size: x-large;">In</a>
            <div class="pulllefttimer" style="height:10%;">
                <span class="workingHours workTimeCounter" id="lunchCounter" style="display: none;"><span style="text-decoration: none;"><span id="lunchTimer" class="timer1 knob hourLunch" data-min="0" data-readOnly="true" data-thickness=".2" data-max="28800" value="0" data-displayPrevious=true></span></span>
            </div>
            <div class="buttonBlocks" id="bye_msg" style="margin-left:0;">
                <h2 class="bye_msg">Good Bye!</h2><h3 class="bye_msg" style="width:240px;"> See you tomorrow.</h3>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-6 margin-top-20">
        <div class="tiles">
            <a href="javascript:void(0)"  class="lunchBtn" id="lunchBtn" style="display: none;text-decoration: none; font-size: x-large;">Break</a>
            <a href="javascript:void(0)" class="endLunchBtn" id="endLunchBtn" style="display: none; font-size: x-large;">End Break</a>
        </div>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-6 margin-top-20">
        <div class="tiles">
            <a href="javascript:void(0)"  data-toggle="modal" data-target=".Model"  class="clockOutBtn pull-left clockOutBtn-dt" id="clockOutBtn" style="display: none;text-decoration: none;">Shift</a>
        </div>
    </div>
<?php } else if ($case == "in_lunch") { ?>
    <div class="col-md-6 col-sm-6 col-xs-12 margin-top-20">
        <div class="tiles" style="height:auto;min-height:100px;">
            <span class="workingHours workTimeCounter" id="workTimeCounter" style="display: none;"><span style="text-decoration: none; font-size: x-large;"><span id="timeIn" data-min="0" data-readOnly="true" data-thickness=".2" data-max="28800" class="timer knob hourTime"> Test</span></span></span>
            <a href="javascript:void(0)"  class="clockInBtn margin-top-20" id="clockInBtn" style="display:none;text-decoration: none; font-size: x-large;">In</a>
            <div class="pulllefttimer" style="height:10%;">
                <span class="workingHours workTimeCounter" id="lunchCounter"><span style="text-decoration: none;"><span id="lunchTimer" class="timer1 knob hourLunch" data-readOnly="true" data-thickness=".2" data-min="0" data-max="28800" value="0" data-displayPrevious=true></span></span>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm3 col-xs-6 margin-top-20">
        <div class="tiles">
            <a href="javascript:void(0)" class="lunchBtn" id="lunchBtn" style="display:none;text-decoration: none;">Break</a>
            <a href="javascript:void(0)" class="endLunchBtn" id="endLunchBtn">End Break</a>
        </div>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-6 margin-top-20">
        <div class="tiles">
            <a href="javascript:void(0)"  data-toggle="modal" data-target=".Model"  class="clockOutBtn" id="clockOutBtn" style="display: none;text-decoration: none;">End</a>
        </div>
    </div>
<?php } else if ($case == "out_lunch") { ?>
    <div class="col-md-6 col-sm-6 col-xs-12 margin-top-20">
        <div class="tiles" style="height:auto;min-height:100px;">
            <span class="workingHours workTimeCounter" id="workTimeCounter" style="text-decoration: none;"><span id="timeIn" class="timer knob hourTime" data-readOnly="true" data-thickness=".2" data-min="0" data-max="28800" data-width="210" data-height="210" value="0" data-displayPrevious=true></span></span>
            <a href="javascript:void(0)"  class="clockInBtn margin-top-20" id="clockInBtn" style="display:none;text-decoration: none;">In</a>
            <div class="pulllefttimer" style="height:10%;">
                <span class="workingHours workTimeCounter" id="lunchCounter" style="display: none;"><span style="text-decoration: none;"><span id="lunchTimer" class="timer1 knob hourLunch" data-readOnly="true" data-thickness=".2" data-min="0" data-max="28800" value="0" data-displayPrevious=true></span></span></span>
                <div class="buttonBlocks" id="bye_msg" style="display:none;">
                    <h2 class="bye_msg">Good Bye!</h2><h3 class="bye_msg" style="width:240px;"> See you tomorrow.</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-6 margin-top-20">
        <div class="tiles">
            <a href="javascript:void(0)"  class="lunchBtn" id="lunchBtn" style="text-decoration: none; font-size: x-large;">Break</a>
            <a href="javascript:void(0)" class="endLunchBtn" id="endLunchBtn" style="display: none; font-size: x-large;">End Break</a>
        </div>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-6 margin-top-20">
        <div class="tiles">
            <a href="javascript:void(0)"  data-toggle="modal" data-target=".Model"  class="clockOutBtn" id="clockOutBtn" style="text-decoration: none;">End</a>
        </div>
    </div>
<?php } ?>
<script>
    $(document).ready(function(){
        var _modal;
       $('.Model').on('show.bs.modal', function (event) {
           var button = $(event.relatedTarget) // Button that triggered the modal
                        var recipient = button.data('whatever') // Extract info from data-* attributes
                        var modal = $(this)
                        _modal = modal;
                        jQuery(modal).find('.modal-title').html('<h2 style="font-family:\'Avenir Next\',\'Avenir Next LT W01 SC\',\'Helvetica Neue\', Helvetica, Arial, sans-serif;font-weight: bold;font-size:24px;color:#7b8cb8;">Edit Timesheet</h2>')
                        jQuery(modal).find('.modal-body').html('Are you sure? You want to clock out.');
                                
                    });

                    $("#saveModel").on('click', function () {
                        var url = '<?php echo Yii::app()->baseUrl; ?>';
                url = url.concat('/company/companyShifts/timeout');
                $.ajax({
                    url: url,
                    data: {time_id: $("#time_id").val(), total_time: $("#timeIn").html()},
                    type: 'GET',
                    success: function (response) {
                        if (response == null) {
                            alert("You've already clocked in for today......");
                        } else {
                            $('#clocks').html(response);
                            $("#note").hide();
                            jQuery(_modal).modal('hide');
                            $("#bye_msg").show();
                            // Init timer pause
                        }
                    }
                });
                $("#timeIn").hide();
                $('#timeIn').timer('pause');
                $("#lunchBtn").animate({"marginLeft": "+=70px"});
                $("#clockOutBtn").animate({"marginLeft": "-=70px"});
                $("#lunchBtn").hide();
                $("#clockOutBtn").hide();
                $("#workTimeCounter").hide();
//                        $("#clockInBtn").show();
                $("#clockInBtn").animate({"marginTop": "-=30px"});
                $("#clockOutBtn").animate({"marginTop": "-=50px"});
                $("#lunchBtn").animate({"marginTop": "-=50px"});
                    });
    });
</script>



