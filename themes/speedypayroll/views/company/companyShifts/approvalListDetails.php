<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$counter = 0;
preg_match_all('!\d+!', Yii::app()->session["client_timezone"], $matches);
?>
<script>
    function convertToLocal(times,id){
        if(times=='01/01/1970 01:00 UTC')
        {
            return "00:00";
        }
        else{
        var date = new Date(times);
        var hours = date.getHours();
        hours = (hours < 10 ? "0" : "") + hours;
        var minute = date.getMinutes();
        var ampm = hours >= 12 ? 'PM' : 'AM';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minute = minute < 10 ? '0'+minute : minute;
  var strTime = hours + ':' + minute + ' ' + ampm;
  document.getElementById(id).innerHTML = strTime;
  }}
    </script>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2 style="font-family:'Avenir Next','Avenir Next LT W01 SC','Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: bold;font-size:16px;color:lightgray;">Timesheet Details</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6">
                    <label style="font-weight:bold;">Date:</label> <?php echo date('m/d/Y',$model->created_at); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <label style="font-weight:bold;">Time In:</label><label id="time_in"></label> 
                    <?php echo date('h:i A',($model->time_in + ($matches[0][0] * 3600))-3600);?>
                </div>
                <div class="col-md-4">
                    <label style="font-weight:bold;">Time Out:</label><label id="time_out"></label> 
                    <?php echo date('h:i A',($model->time_out + ($matches[0][0] * 3600))-3600);?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <label style="font-weight:bold;">Status:</label> <?php echo $model->status;?>
                </div>
                <div class="col-md-4">
                    <label style="font-weight:bold;">Reason:</label> <?php echo $model->reason;?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <h2 style="font-family:'Avenir Next','Avenir Next LT W01 SC','Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: bold;font-size:16px;color:lightgray;">Lunch</h2>
                </div>
            </div>
            <?php foreach($lunches as $lunch){?>
            <div class="row">
                <div class="col-md-4">
                    <label style="font-weight:bold;">Lunch In:</label><label id="lunch_in<?php echo $counter;?>"></label>
                    <?php echo date('h:i A',($lunch->lunch_in + ($matches[0][0] * 3600))-3600);?>
                </div>
                <div class="col-md-4">
                    <label style="font-weight:bold;">Lunch Out:</label><label id="lunch_out<?php echo $counter;?>"></label>
                    <?php echo date('h:i A',($lunch->lunch_out + ($matches[0][0] * 3600))-3600);?>
                </div>
            </div>
            <?php $counter++; } if(count($lunches)<1){ ?>
            <div class="row">
                <div class="col-md-8">
                    <label>No lunch to show.....</label>
                </div>
            </div>
            <?php } ?>
            <div class="row">
                <div class="col-md-6">
                    <h2 style="font-family:'Avenir Next','Avenir Next LT W01 SC','Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: bold;font-size:16px;color:lightgray;">Notes</h2>
                </div>
            </div>
            <?php foreach($notes as $item){ ?>
            <div class="row">
                <div class="col-md-6">
                    <label><b>Note:</b></label> <label><?php echo $item["note"];?></label>
                </div>
                <div class="col-md-4">
                    <label><b>Time: </b></label> 
                    <label><?php echo date('d/m/Y',$item["created_at"])." ";?>
                        <label id="times<?php echo $item['id']; ?>"> 
                            <?php echo date('d/m/Y h:i A',($item["created_at"] + ($matches[0][0] * 3600))-3600);?>
                        </label>
                    </label>
                </div>
            </div>
            <?php } if(count($notes)<1){ ?>
            <div class="row">
                <div class="col-md-8">
                    <label>No notes to show.....</label>
                </div>
            </div>
            <?php } ?>
            <div class="row">
                <div class="col-md-4" style="text-align: right;">
                    <a id="viewShift" class="btn btn-primary" href="javascript:void(0)" onclick="updateStatus(<?php echo $model->id;?>,'approved','')">Approve</a>
                </div>
                <div class="col-md-4">
                    <a class="btn btn-danger" onclick="showModel(<?php echo $model->id;?>)">Disapprove</a>
                </div>
            </div>
            
        </div>
    </div>
</div>