<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//dd($employees);
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'timesheet-report-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class'=>'form-horizontal form-bordered',
    ),
        ));
?>

<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-clock-o"></i>Timesheet Reports
        </div>
    </div>
    <div class="portlet-body form">
        <div class="box-body" style="padding-left:20px;padding-top:10px;">
            <div class="form-group">
                <label class="control-label col-md-3"><b>Start Date & End Date *</b></label>
        <div class="col-md-4">
            <div class="input-group">
            <?php echo $form->textField($model, 'created_at', array('class' => 'form-control pull-left dRngPicker disableEle','placeholder' => 'Click To Select the Dates', 'readonly'=>'readonly', 'style'=>'cursor:context-menu')); ?>
            <div class="input-group-btn">
                <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
            </div>
        </div>
        </div> 
    </div>
            <div class="form-group">
                <label class="control-label col-md-3"><b>Timesheet Type:</b></label>
                <div class="input-group col-md-8">
                    <div class="col-md-12">
                        <input type="checkbox" name="type[]" id="type" value="1" style="width: 20px;margin-top: 12px;">
                    <!--</div>-->
                    <label for="type" class="control-label">Timesheet</label></div>
                <div class="col-md-12">
                        <input type="checkbox" name="type[]" id="type1" value="2" style="width: 20px;margin-top: 12px;">
                    
                    <label for="type1" class="control-label">Timesheet Request</label>
                </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3"><b>Export As:</b></label>
                <div class="input-group col-md-8">
                    <div class="col-md-2">
                    <input type="radio" id="html" name="export" value="1" checked>
                    <label for="html" class="control-label">HTML</label>
                    </div>
                    <div class="col-md-2">
                    <input type="radio" id="pdf" name="export" value="2">
                    <label for="pdf" class="control-label">PDF</label>
                    </div>
                </div>
            </div>
            <?php if($employees && $is_general != ''){?>
            <div class="form-group">
                <label class="control-label col-md-3"><b>Employees:</b></label>
                <div class="form-group">
                        <!--<label class="control-label col-md-3"><b>Employees:</b></label>-->
                        <div class="col-md-4">
                            <select id="employeeTags" class="form-control" name="employee[]" placeholder="Select an employee or employees..." multiple="multiple">
                                <?php foreach ($employees as $item) { ?>
                                    <option value="<?php echo $item->id; ?>"><?php echo $item->first_name . " " . $item->last_name; ?></option>
                                <?php } ?>
                            </select>
                            <div class="checkbox-list">
                                <label><input type="checkbox" id="all" value="0"/>  Select All</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-offset-2 col-md-4">
                        </div>
                    </div>
<!--                <div class="col-md-4">
                    <input type="checkbox" id="all" value="0" style="margin-right:5px;"><label for id="all"><b>All</b></label><br>
                    <?php // foreach($employees as $item){?>
                    <input type="checkbox" class="checkBox" name="employee[]" style="margin-right:5px;" id="<?php // echo $item->id;?>" value="<?php // echo $item->id;?>"><label for="<?php // echo $item->id;?>"><?php // echo $item->first_name." ".$item->last_name;?></label><br>
                    <?php //} ?>
                </select>
                </div>-->
            </div>
            <div class="form-group">
                <label class="control-label col-md-3"><b>Group:</b></label>
                <div class="col-md-2">
                    <input type="radio" name="group" id="user" value="1" style="margin-right:4px;" checked><label for="user" class="control-label"> Group by User</label>
                </div>
                <div class="col-md-2">
                    <input type="radio" name="group" id="date" value="2" style="margin-right:4px;"><label for="date" class="control-label"> Group by Date</label>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3"><b>Sort:</b></label>
                <div class="col-md-2">
                    <input type="radio" name="sort" id="first" value="1" style="margin-right:4px;" checked><label class="control-label" for="first">Sort by First Name</label>
                </div>
                <div class="col-md-2">
                    <input type="radio" name="sort" id="last" value="2" style="margin-right:4px;"><label class="control-label" for="last">Sort by Last Name</label>
                </div>
            </div>
            <?php }?>
        </div><div class="form-actions right">
        <div class="row">
            <div class="col-md-offset-3 col-md-9">
                <button type="button" class="btn btn-circle green-haze" onclick="create()">CREATE REPORT</button>
    <?php // echo CHtml::submitButton($model->isNewRecord ? 'CREATE REPORT' : 'Save', array('class' => 'btn btn-primary')); ?>
</div>
        </div>
</div>
    </div>
</div>
<?php $this->endWidget();?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/thirdparty/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/thirdparty/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script>
$(document).ready(function(){
    $("#employeeTags").select2();
    $("#UserTimings_created_at").prop('required',true);
    $('#all').click(function () {
            if ($(this).prop("checked")) {
                $("option").attr("selected", "selected");
            } else {
                $("option").removeAttr("selected");
            }
            $("#employeeTags").select2();
        });
    
    jQuery('.dRngPicker').daterangepicker({
//                    timePicker: true,
//                    timePickerIncrement: 30,
//                    minDate: '17/10/2015',
                    //maxDate:'17/11/2015',
                    format: 'DD/MM/YYYY',
                    opens: 'left',
                    separator: ' to '
                  }, function(start, end, label) {
                    console.log(start.toISOString(), end.toISOString(), label);
                  });
});    
function create()
{
    if($("#UserTimings_created_at").val() == "")
    {
        alert("Please set the date range for report");
    }
    else
    {
        $("#timesheet-report-form").submit();
    }
}
</script>