<?php
$counter = 0;
preg_match_all('!\d+!', Yii::app()->session["client_timezone"], $matches);
?>
<script>
    function convertToLocal(times,id){
        if(times=='01/01/1970 01:00 UTC')
        {
            return "00:00";
        }
        else{
        var date = new Date(times);
        var hours = date.getHours();
        hours = (hours < 10 ? "0" : "") + hours;
        var minute = date.getMinutes();
        var ampm = hours >= 12 ? 'PM' : 'AM';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minute = minute < 10 ? '0'+minute : minute;
  var strTime = hours + ':' + minute + ' ' + ampm;
  document.getElementById(id).innerHTML = strTime;
  }}
    </script>
<input type="hidden" name="lunch_id" id="lunch_id" value="<?php echo $lunch_id ?>">
<input type="hidden" name="time_id" id="time_id" value="<?php echo $model->id?>">
<?php if($model->time_in != 0){?>
<h2 class="timeLogLabel">
    Time In: 
    <label id="time_in" class="timeLogValue">
    <?php echo date('h:i a',($model->time_in + ($matches[0][0] * 3600))-3600);?>
    <!--<script>convertToLocal('<?php // echo date('m/d/Y h:i a',$model->time_in)." UTC"?>','time_in');</script>-->
</label>
</h2><?php } ?>
<?php if(count($lunches)>0){ foreach($lunches as $lunch){ $counter++;?>
<h2 class="timeLogLabel">
Break: 
<label id="lunch_in<?php echo $counter?>" class="timeLogValue">
    <?php echo date('h:i a',($lunch->lunch_in + ($matches[0][0] * 3600))-3600);?>
<!--<script>convertToLocal('<?php // echo date('m/d/Y h:i a',$lunch->lunch_in)." UTC"?>','lunch_in<?php // echo $counter?>')</script>-->
</label> - <?php if($lunch->lunch_out != 0){?>
<label id="lunch_out<?php echo $counter?>" class="timeLogValue">
    <?php echo date('h:i a',($lunch->lunch_out + ($matches[0][0] * 3600))-3600);?>
<!--<script>convertToLocal('<?php // echo date('m/d/Y h:i a',$lunch->lunch_out)." UTC"?>','lunch_out<?php // echo $counter?>');</script>-->
</label><?php }else{ ?><label class="timeLogValue">On Break</label></h2><?php } ?>
<?php }} if($model->time_out != 0){ ?>
<h2 class="timeLogLabel">
Time Out: 
<label id="time_out" class="timeLogValue">
    <?php echo date('h:i A',($model->time_out + ($matches[0][0] * 3600))-3600);?>
    <!--<script>convertToLocal('<?php // echo date('m/d/Y h:i a',$model->time_out)." UTC"?>','time_out');</script>-->
</label></h2>
    <?php } ?>
<!--lunch_out-->
<!--<script>document.write(convertToLocal('<?php // echo date('d/m/Y h:i a',strtotime(date('h:i a',$lunch->lunch_out)))." UTC"?>'));</script>-->

    <script>
        $(document).ready(function(){
           $("#id").val('<?php echo $model->id;?>'); 
        });
    </script>