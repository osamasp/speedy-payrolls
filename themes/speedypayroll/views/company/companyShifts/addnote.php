<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'add-note-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => false,
        ));
?>
<input type="hidden" id="redirect_url" name="url" value="">
<input type="hidden" id="contract" name="contract_id" value='<?php echo $contract_id; ?>'>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/timesheet.css" rel="stylesheet" type="text/css">
<input type="hidden" name="id" value="<?php echo $id ?>">
<div>    
    <div class="row">
        <div class="col-md-12">
            <label style="font-family:'Avenir Next','Avenir Next LT W01 SC','Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: bold;font-size:14px;color:black;">Note:</label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12"> 
                    <textarea name="note" class="form-control note-text" id="note"></textarea>
                </div>
            </div>
            <div class="separator"></div><br>
        </div>
    </div>
    </div>
    <?php $this->endWidget(); ?>