<?php
/* @var $this MainController */
/* @var $model User */
$this->breadcrumbs=array(
	'Company Usage',
);
?>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'login-form',
    'enableClientValidation' => true,
        ));
?>
<div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-check-square-o"></i>Company Permission
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>

                </div>
            </div>
            <div class="portlet-body form">
                <div class="table-toolbar">
                    <div class="row">

                        <div class="col-md-12">
                            
                        </div>
                    </div>
                </div>
<div class="box-header" style="padding-left:20px;">
    <h3 class="box-title">Allow Speedy Timesheet & Payroll</h3>
</div>
<div class="table-responsive">
    <div class="box-body">
        <div style="padding-left:30px;">
        <select id="company" name="Company" class="form-control" style="width: 30%;" required="required">
             <option value="">Select Company</option>
            <?php foreach ($companies as $company ) { ?>
            <option value ="<?php echo $company->id; ?>"><?php echo $company->name; ?></option>
            <?php } ?>
        </select><br>
        
        <select id="payroller" name="Payroller" class="form-control" style="width: 30%;" required="required">
             <option value="">Select Payroller</option>
            <?php foreach ($payrollers as $payroller ) {
                $_payroller = Payroller::model()->findByAttributes(array('payroller_id'=> $payroller->id));
                ?>
            <option value ="<?php echo $payroller->id; ?>"><?php echo (isset($_payroller->company_name))?$_payroller->company_name: "-"; ?></option>
            <?php } ?>
        </select><br>
        </div>
         <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-3 col-md-9">
    <?php echo CHtml::submitButton('Done', array('class' => 'btn btn-primary btn-file')); ?>
</div>
        </div>
         </div>
    </div>
</div>
<?php $this->endWidget(); ?>
            </div>
</div>