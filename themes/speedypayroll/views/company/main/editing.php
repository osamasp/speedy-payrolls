<?php
/* @var $this MainController */
/* @var $model User */

$this->breadcrumbs = array(
    'Users' => array('index'),
);
?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'edit-profile-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array('enctype' => 'multipart/form-data')
        ));
?>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/company-main-editing.js" type="text/javascript"></script>
<div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-globe"></i>Edit Profile
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
<div style="width: 50%;">
    <div class="row" style="padding-left:30px;padding-top:30px;">
    <div class="col-md-4">
        <div id="preview" class="img-preview" style="margin-top: 10px;">
        </div>
        <div class="form-group">
                Browse <?php echo $form->fileField($model, 'logo'); ?>
            <?php echo $form->error($model, 'logo');?>
        </div>
    </div>
     <div class="col-md-8" style="padding-left:40px;">
        <div class="form-group">
            <label>Company Name*</label>
            <?php echo $form->textField($model, 'name', array('placeholder' => 'Company Name *', 'class' => 'form-control')); ?>
        </div>
        <div class="form-group">
            <label>Company Director's First Name*</label>
            <?php echo $form->textField($user, 'first_name', array('placeholder' => 'First Name *', 'class' => 'form-control')); ?>
        </div>
        <div class="form-group">
            <label>Last Name*</label>
            <?php echo $form->textField($user, 'last_name', array('placeholder' => 'Last Name *', 'class' => 'form-control')); ?>
        </div>
        <div class="form-group">
            <label>Email</label>
            <?php echo $form->textField($user, 'email', array('placeholder' => 'Email Name *', 'class' => 'form-control')); ?>
        </div>
        
           <div class="form-group">
            <label>Phone Number*</label>
            <?php echo $form->textField($model, 'phone', array('placeholder' => 'Phone Number', 'class' => 'form-control')); ?>
        </div>
        <div class="form-group">
            <label>Registration Number*</label>
            <?php echo $form->textField($model, 'registration_number', array('placeholder' => 'Registration Number', 'class' => 'form-control')); ?>
        </div>
        <div class="form-group">
            <label>NI Number*</label>
            <?php echo $form->textField($model, 'vat_number', array('placeholder' => 'VAT Number', 'class' => 'form-control')); ?>
        </div>

        <div class="form-group">
            <label>Address*</label>
            <?php echo $form->textField($model, 'address', array('placeholder' => 'Address Line 1', 'class' => 'form-control')); ?>

        </div>
        <div class="form-group">
            <label>Street*</label>
            <?php echo $form->textField($model, 'street', array('placeholder' => 'Street', 'class' => 'form-control')); ?>
        </div>
        <div class="form-group">
            <label>Country*</label>
            <?php echo $form->dropDownList($model, 'country',  AppInterface::getCountries(), array('class' => 'form-control')); ?>
        </div>
        <div class="form-group">
            <div class="col-xs-6" style="padding-left: 0;"><label>City</label><?php echo $form->textField($model, 'city', array('placeholder' => 'City', 'class' => 'form-control' )); ?></div>
            <div class="col-xs-6" style="padding-right: 0;"><label>Postcode</label><?php echo $form->textField($model, 'post_code', array('placeholder' => 'Postcode', 'class' => 'form-control')); ?></div><br>
            <div class="clearfix"></div>
        </div>
         
         <div class="form-group">
             <label>Upload Passport Copy</label>
                <?php echo $form->fileField($model, 'passport_copy'); ?>
        </div>
         <div class="form-group">
             <label>Upload Bill Copy</label>
                <?php echo $form->fileField($model, 'bill_copy'); ?>
        </div>
         
    </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-9">
    <?php echo CHtml::submitButton('Done', array('class' => 'btn btn-primary btn-file')); ?>
</div>
                </div>
</div>
</div>

</div>
<?php $this->endWidget(); ?>