<?php
/* @var $this MainController */
/* @var $model Company */
/* @var $form CActiveForm */
?>
<div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-globe"></i>Edit Company Details
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>

                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">

                        <div class="col-md-12">
                            
                        </div>
                    </div>
                </div>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'company-form',
	'enableAjaxValidation'=>false,
)); ?>
<div class="box-body">
    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php if ($model->hasErrors()) { ?>
<div class='callout callout-danger'>    <?php echo $form->errorSummary($model); ?>
</div>
<?php } ?>    
            <div class="form-group">
            <?php echo $form->labelEx($model,'id'); ?>
            <?php echo $form->textField($model,'id',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
            <?php echo $form->error($model,'id', array('class' => 'text-red')); ?>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'name'); ?>
            <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
            <?php echo $form->error($model,'name', array('class' => 'text-red')); ?>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'logo'); ?>
            <?php echo $form->textField($model,'logo',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
            <?php echo $form->error($model,'logo', array('class' => 'text-red')); ?>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'email'); ?>
            <?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
            <?php echo $form->error($model,'email', array('class' => 'text-red')); ?>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'phone'); ?>
            <?php echo $form->textField($model,'phone',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
            <?php echo $form->error($model,'phone', array('class' => 'text-red')); ?>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'sector'); ?>
            <?php echo $form->textField($model,'sector',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
            <?php echo $form->error($model,'sector', array('class' => 'text-red')); ?>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'registration_number'); ?>
            <?php echo $form->textField($model,'registration_number',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
            <?php echo $form->error($model,'registration_number', array('class' => 'text-red')); ?>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'vat_number'); ?>
            <?php echo $form->textField($model,'vat_number',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
            <?php echo $form->error($model,'vat_number', array('class' => 'text-red')); ?>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'address'); ?>
            <?php echo $form->textField($model,'address',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
            <?php echo $form->error($model,'address', array('class' => 'text-red')); ?>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'street'); ?>
            <?php echo $form->textField($model,'street',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
            <?php echo $form->error($model,'street', array('class' => 'text-red')); ?>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'city'); ?>
            <?php echo $form->textField($model,'city',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
            <?php echo $form->error($model,'city', array('class' => 'text-red')); ?>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'country'); ?>
            <?php echo $form->textField($model,'country',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
            <?php echo $form->error($model,'country', array('class' => 'text-red')); ?>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'post_code'); ?>
            <?php echo $form->textField($model,'post_code',array('size'=>10,'maxlength'=>10,'class' => 'form-control')); ?>
            <?php echo $form->error($model,'post_code', array('class' => 'text-red')); ?>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'type'); ?>
            <?php echo $form->textField($model,'type', array('class' => 'form-control')); ?>
            <?php echo $form->error($model,'type', array('class' => 'text-red')); ?>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'status'); ?>
            <?php echo $form->textField($model,'status', array('class' => 'form-control')); ?>
            <?php echo $form->error($model,'status', array('class' => 'text-red')); ?>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'created_at'); ?>
            <?php echo $form->textField($model,'created_at',array('size'=>20,'maxlength'=>20,'class' => 'form-control')); ?>
            <?php echo $form->error($model,'created_at', array('class' => 'text-red')); ?>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'created_by'); ?>
            <?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
            <?php echo $form->error($model,'created_by', array('class' => 'text-red')); ?>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'modified_at'); ?>
            <?php echo $form->textField($model,'modified_at',array('size'=>20,'maxlength'=>20,'class' => 'form-control')); ?>
            <?php echo $form->error($model,'modified_at', array('class' => 'text-red')); ?>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'modified_by'); ?>
            <?php echo $form->textField($model,'modified_by',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
            <?php echo $form->error($model,'modified_by', array('class' => 'text-red')); ?>
        </div>

        </div>
<div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-9">
    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary')); ?>
</div>
                </div>
</div>
<?php $this->endWidget(); ?>
            </div>
</div>