<?php
?>
    <table id="example1" class="table table-responsive table-striped" style="">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Sector</th>
                <th>Registration Number</th>
                <th>Vat Number</th>
                <th>Payroller</th>
                <th>Status</th>
                <th>Plan Type</th>
                <th>Action</th>
            </tr>
        </thead>
        <?php foreach($model as $item)
        {?>
        <tr>
            <td><?php echo $item["name"]; ?></td>
            <td><?php echo $item["email"]; ?></td>
            <td><?php echo $item["sector"]; ?></td>
            <td><?php echo $item["registration_number"]; ?></td>
            <td><?php echo $item["vat_number"]; ?></td>
            <td><?php echo AppCompany::getPayrollerName($item["company_id"]); ?></td>
            <td><?php echo ($item["status"]=="1")?"Active":"Inactive"; ?></td>
            <td><?php echo ($item["plan_type"]=="1")?"Premium":"Free";?></td>
            <td><div class="btn-group">
                            <a class="btn btn-primary" style="padding-top: 0px;padding-bottom: 0px;" href="<?php echo $this->createUrl('/company/main/view', array('id' => $item["id"])); ?>">View</a>
                            <button type="button" class="btn btn-primary dropdown-toggle" style="padding-top: 0px;padding-bottom: 0px;" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php echo $this->createUrl('/user/main/loginascompany', array('id' => $item["company_id"])); ?>">Login</a></li>
                                <li><a href="<?php echo $this->createUrl('/company/main/update', array('id' => $item["id"])); ?>">Update</a></li>
                                <li><a href="javascript:deleteCompany('<?php echo $this->createUrl("/company/main/delete/id/".$item["id"]) ?>')">Delete</a></li>
                            </ul>
                        </div></td>
        </tr>
        <?php } ?>
    </table>