<?php
/* @var $this MainController */
/* @var $model User */
/* @var $form CActiveForm */

$this->breadcrumbs = array(
    'Users' => array('index'),
);

?>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/company-main-preferences.js" type="text/javascript"></script>
<!-- BEGIN SAMPLE FORM PORTLET-->
<div class="portlet box green ">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i> Preferences
        </div>
    </div>
    <div class="portlet-body form">
        <?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'login-form',
    'enableClientValidation' => true,
    'htmlOptions' => array('enctype' => 'multipart/form-data','class'=>'form-horizontal')
        ));
?>
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-3 control-label">Company Name</label>
                    <div class="col-md-3">
                        <?php echo $form->textField($model, 'legal_name', array('placeholder' => 'Enter Company Name', 'class' => 'form-control')); ?>
                        <span class="help-block">
                            Specify legal company name for invoicing.</span>
                    </div>
                </div>
                
            <?php if ($model->logo) { ?>
                <div class="form-group">
                    <label class="col-md-3 control-label">Company Logo</label>
                    <div class="col-md-9">
                        <img src="<?php echo Yii::app()->baseUrl . '/uploads/photos/' . $model->logo; ?>" alt="logo" class="logo-default" style="max-height: 150px; max-width: 225px;"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputFile" class="col-md-3 control-label">Upload Logo</label>
                    <div class="col-md-9">
                        <?php echo $form->fileField($model, 'logo'); ?>
                        <p class="help-block">
                            Allowed formats: JPG, PNG, GIF only.
                        </p>
                    </div>
                </div>
            <?php } else { ?>
            <div class="form-group">
                    <label class="col-md-3 control-label">Company Logo</label>
                    <div class="col-md-9">
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputFile" class="col-md-3 control-label">Upload Logo</label>
                    <div class="col-md-9">
                        <?php echo $form->fileField($model, 'logo'); ?>
                        <p class="help-block">
                            Allowed formats: JPG, PNG, GIF only.
                        </p>
                    </div>
                </div>
            <?php } ?>
                <div class="form-group">
                    <label class="col-md-3 control-label">Enable logo on invoices and timesheets</label>
                    <div class="col-md-9">
                        <div class="radio-list">
                            <label>
                                <input type="radio" name="is_enable_logo" id="optionsRadios22" value="1" checked> Enable</label>
                            <label>
                                <input type="radio" name="is_enable_logo" id="optionsRadios23" value="0"> Disable</label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Usage</label>
                    <div class="col-md-9">
                        <div class="radio-list">
                            <label>
                                <input type="radio" name="Company[usage]" id="optionsRadios22" value="0" checked> Speedy Timesheets Only </label>
                            <label>
                                <input type="radio" name="Company[usage]" id="optionsRadios23" value="1" checked> Speedy Timesheets and Payrolls</label>
                        </div>
                    </div>
                </div>

            </div>
            <div class="form-actions right">
                <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                        <?php echo CHtml::submitButton('Done', array('class' => 'btn btn-circle btn-sm green-haze')); ?>
                    </div>
                </div>
            </div>
        <?php $this->endWidget(); ?>
    </div>
</div>
<!-- END SAMPLE FORM PORTLET-->
