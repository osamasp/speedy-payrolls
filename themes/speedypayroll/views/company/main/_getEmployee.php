

<div class="form-group">
    <label class="control-label col-md-3">Contractor</label>
    <div class="col-md-9"  id="inside" style="display: block;">
        <select  name="Contract[user_id]" id="contractor" style="width: 260px;" class="form-control chosen-select" required="required">
            <option value="">Select Contractor</option>
            <?php foreach ($contracts as $user) { ?>
                <option value="<?php echo $user->id; ?>"><?php echo $user->full_name; ?></option>
            <?php } ?>
        </select>
    </div>
</div>