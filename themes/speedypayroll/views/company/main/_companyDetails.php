<?php
if ($inputArrName == '') {
    $inputArrName = 'cc';
}
?>
<div class="form-group">
    <div class="col-md-6 caption font-red-sunglo">
        <i class="fa fa-building font-red-sunglo"></i>
        <span class="caption-subject bold uppercase"> COMPANY DETAILS</span>
    </div>
</div>
<?php if (isset($company)) { ?>
    <div class='row'>
        <div class="col-md-12">
            <div class="row">
                <input type="hidden" value="<?php echo $company->id; ?>" name="asdas" id="cc_id">
                <div class="col-lg-4"><b>Company:</b></div>
                <div class="col-lg-6"><?php echo $company->name; ?></div>    
            </div>
            <div class="row">
                <div class="col-lg-4"><b>Phone no.:</b></div>
                <div class="col-lg-6"><?php echo $company->phone; ?></div>    
            </div>
            <div class="row">
                <div class="col-lg-4"><b>Address:</b></div>
                <div class="col-lg-6"><p><?php echo $company->address . ', ' . $company->street . ', ' . $company->city . ', ' . $company->country . '.'; ?></p></div>    
            </div>
            <div class="row">
                <div class="col-md-12 text-right" style="padding-bottom:10px;"><input type="button" style="border-color: red;border-width: 4px;font-weight: bold;" class="btn btn-primary" id="confirm-company<?php echo (($inputArrName != 'cc') ? 'cc2' : ''); ?>" value="Confirm"><input type="button" style="border-color: red;border-width: 4px;display:none;font-weight: bold;" class="btn btn-success" id="confirmed" value="Confirmed" disabled></div>        
            </div>
            <div class="row"></div>
        </div>
    </div>
<?php } else {
    ?>
    <div class='row'>
        <div class="cc-remove-me<?php echo (($inputArrName != 'cc') ? 'cc2' : ''); ?>">
            <div class="row">
                <div class="col-lg-12">No company found on system.</div>
            </div>

            <div id="cc-show-me<?php echo (($inputArrName != 'cc') ? 'cc2' : ''); ?>">
                <div class="form-group">
                    <div class="col-md-12 margin-top-10">
                        <div class="col-md-3">
                            Contractor's Company Name:
                        </div>
                        <div class="col-md-9">
                            <input type="text" name="<?php echo $inputArrName; ?>[company_name]" id="<?php echo $inputArrName; ?>[company_name]" class="form-control">
                            <label id="contractors_c_email_label<?php echo $inputArrName; ?>"></label>
                            
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="col-md-3">
                            Contractor Name:
                        </div>
                        <div class="col-md-9">
                            <input type="text" name="<?php echo $inputArrName; ?>[user_name]" id="<?php echo $inputArrName; ?>[contractor_name]" class="form-control contractor_name" required="required">
                            <label id="contractors_c_name_label<?php echo $inputArrName; ?>"></label>
                            
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">   
                        <div class="col-md-3">
                            Contractor Email:
                        </div>
                        <div class="col-md-9">
                            <input type="text" name="<?php echo $inputArrName; ?>[user_email]" value="<?php echo $email; ?>" id='<?php echo $inputArrName; ?>[contractor_email]' class="form-control" readonly>
                            <label id="contractor_email_label<?php echo $inputArrName; ?>"></label>
                            
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">    
                        <div class="col-md-3">
                            Company Phone#:
                        </div>
                        <div class="col-md-9">
                            <input type="text" name="<?php echo $inputArrName; ?>[company_phone]" id="<?php echo $inputArrName; ?>[company_phone]" class="form-control" required="required">
                            <label id="company_phone_label<?php echo $inputArrName; ?>"></label>
                            
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="col-md-3">
                            Contractor Mobile#:
                        </div>
                        <div class="col-md-9">
                            <input type="text" name="<?php echo $inputArrName; ?>[contractor_phone]" id="<?php echo $inputArrName; ?>[contractor_phone]" class="form-control" required="required">
                            <label id="contractor_phone_label<?php echo $inputArrName; ?>"></label>
                            
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="col-md-3">
                            Company Address:
                        </div>
                        <div class="col-md-9">
                            <input type="text" name="<?php echo $inputArrName; ?>[company_address]" id="<?php echo $inputArrName; ?>[company_address]" class="form-control" required="required">
                            <label id="company_address_label<?php echo $inputArrName; ?>"></label>
                           
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="col-md-3">
                            City:
                        </div>
                        <div class="col-md-9">
                            <input type="text" name="<?php echo $inputArrName; ?>[city]" id="<?php echo $inputArrName; ?>[city]" class="form-control" required="required">
                            <label id="city_label<?php echo $inputArrName; ?>"></label>
                            
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="col-md-3">
                            Post Code:
                        </div>
                        <div class="col-md-9">
                            <input type="text" name="<?php echo $inputArrName; ?>[postcode]" id="<?php echo $inputArrName; ?>[post_code]" class="form-control" required="required">
                            <label id="post_code_label<?php echo $inputArrName; ?>"></label>
                            
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="col-md-3">
                            Country:
                        </div>
                        <div class="col-md-9">
                            <select id="<?php echo $inputArrName; ?>[country]" name="<?php echo $inputArrName; ?>[country]" class="form-control" placeholder="Country" required="required">
                                <?php
                                foreach (AppInterface::getCountries() as $item) {
                                    if ($item == "United Kingdom") {
                                        ?>
                                        <option value="<?php echo $item; ?>" selected><?php echo $item; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $item; ?>" ><?php echo $item; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                            <label id="country_label"></label>
                        </div>
                    </div>
                </div>
                <div id='cc-email-message'></div>

            </div>

        </div>

    </div><br>
    <div class="row">
        <div class="form-group">
            <div class="col-md-12 text-right" style="padding-bottom:10px;">

                <input type="button" style="border-color: red;border-width: 4px;font-weight: bold;" class="btn btn-circle green-haze" id="confirm-company<?php echo (($inputArrName != 'cc') ? 'cc2' : ''); ?>" value="Confirm"><input type="button" style="border-color: red;border-width: 4px;font-weight: bold;display:none;" class="btn btn-circle btn-sm green-haze" id="confirmed" value="Confirmed" disabled></div>      
        </div>
    </div>
    <div class="row"></div>
    <script>
        $("#companyId").val("0");
    $(document).ready(function(){
        
        $("#contract-form").validate({
            rules: {
            '<?php echo $inputArrName; ?>[company_name]': {
                required: true,
                email: true
                },
            }
            message: {
                '<?php echo $inputArrName; ?>[company_name]' : "kindly provide company Name",
            }
            
            
        });
        
        
        
    });
    
    </script>
    
    
<?php } ?>
