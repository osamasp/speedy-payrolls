<?php
/* @var $this MainController */
/* @var $model Company */
$this->breadcrumbs = array(
    'Companies Manage',
);

$this->menu = array(
    array('label' => 'List Company', 'url' => array('index')),
    array('label' => 'Create Company', 'url' => array('create')),
);
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/company-main-admin.js"></script>
<!--<script src="<?php // echo Yii::app()->theme->baseUrl; ?>/js/company-main-admin.js" type="text/javascript"></script>-->
<div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-building-o"></i>Companies Management
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>

                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">

                        <div class="col-md-12">
                            
                        </div>
                    </div>
                </div>

<div class="box-header">
    <h3 class="box-title">Manage Companies</h3>
</div>

<div class="box-body table-responsive">
    <?php echo CHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?>
    <div class="search-form" style="display:none">
        <?php
        $this->renderPartial('_search', array(
            'model' => $model,
        ));
        ?>
    </div><!-- search-form -->
    <table id="example1" class="table table-striped table-bordered table-hover dataTable" style="">
        <thead>
            <tr>
                <th><input type="checkbox" name="check" clas="form-control" /></th>
                <th>Name</th>
                <th>Email</th>
                <th>Sector</th>
                <th>Registration Number</th>
                <th>Vat Number</th>
                <th>Payroller</th>
                <th>Plan Type</th>
                <th>Action</th>
            </tr>
        </thead>
        <?php foreach($models as $item)
        {?>
        <tr class="gradeX">
            <td><input type="checkbox" name="check" value="<?php echo $item['id']; ?>" clas="form-control" /></td>
            <td><?php echo $item["name"]; ?></td>
            <td><?php echo $item["email"]; ?></td>
            <td><?php echo $item["sector"]; ?></td>
            <td><?php echo $item["registration_number"]; ?></td>
            <td><?php echo $item["vat_number"]; ?></td>
            <td><?php echo AppCompany::getPayrollerName($item["company_id"]); ?></td>
            <td><?php $plan = Plan::model()->findByPk($item["plan_type"]); echo (isset($plan))?  $plan->plan_type:"-";?></td>
            <td>
                <div class="btn-group">
                                <button class="btn green dropdown-toggle" data-toggle="dropdown">Actions <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="<?php echo $this->createUrl('/user/main/loginascompany', array('id' => $item["company_id"])); ?>">Login</a></li>
                                <li><a href="<?php echo $this->createUrl('/company/main/view', array('id' => $item["id"])); ?>">View</a></li>
                                <li><a href="<?php echo $this->createUrl('/company/main/update', array('id' => $item["id"])); ?>">Update</a></li>
                                <li class="deleterow"><a data="<?php echo $this->createUrl("/company/main/delete/id/".$item["id"]) ?>" href="javascript:void(0);">Delete</a></li>
                            </ul>
                        </div></td>
        </tr>
        <?php } ?>
    </table>
    </div>
            </div>
</div>