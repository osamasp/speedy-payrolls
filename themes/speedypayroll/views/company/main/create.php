<?php
/* @var $this MainController */
/* @var $model Company */

$this->breadcrumbs=array(
	'Companies'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Company', 'url'=>array('index')),
	array('label'=>'Manage Company', 'url'=>array('admin')),
);
?>

<div class="box-header">
    <h3 class="box-title">Create Company</h3>
</div>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>