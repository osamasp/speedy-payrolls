<?php
/* @var $this MainController */
/* @var $model Company */

$this->breadcrumbs=array(
	'Companies'=>array('index'),
	$model->name,
);

$this->menu=array(
array('label'=>'List Company', 'url'=>array('index')),
array('label'=>'Create Company', 'url'=>array('create')),
array('label'=>'Update Company', 'url'=>array('update', 'id'=>$model->id)),
array('label'=>'Delete Company', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Company', 'url'=>array('admin')),
);
?>
<div class="portlet box green ">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i> View Companies
        </div>
        <div class="tools">
            <a href="" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body">
<div class="box-header">
    <h3 class="box-title">Details about Company "<?php echo $model->name; ?>"</h3>
</div>
<div class="table-responsive">
    <div class="box-body">
        <?php $this->widget('zii.widgets.CDetailView', array(
        'data'=>$model,
        'attributes'=>array(
		'name',
		'logo',
		'email',
		'phone',
		'sector',
		'registration_number',
		'vat_number',
		'address',
		'street',
		'city',
		'country',
		'post_code',
		'type',
		'status',
        ),
        )); ?>
    </div>
</div>
    </div>
</div>