<?php
/* @var $this MainController */
/* @var $model User */

$this->breadcrumbs = array(
    'Users' => array('index'),
);

?>

<div class="portlet box grey-cascade">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-globe"></i>Company Profile
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">

<div class="box-header ">
    <h3 class="box-title"><?php echo ucwords($company ? $company->name : '-'); ?></h3>
    <a class="btn btn-circle btn-sm green-haze pull-right" style=" margin-right:10px; float: right;" href="<?php echo Yii::app()->createUrl('company/main/editprofile'); ?>">Edit Profile</a> 
    <a class="btn btn-circle btn-sm green-haze pull-right"  href="<?php echo Yii::app()->createUrl('user/main/changepassword'); ?>">Change Password</a>
</div>
<div class="table-responsive">
    <div class="box-body">
        <?php
        $this->widget('zii.widgets.CDetailView', array(
            'data' => $model,
            'attributes' => array(                              
                array('name'=>'Company Name' , 'value'=>$company ? $company->name : '-') ,
                array('name'=>'Registration Number' , 'value'=>$company ? $company->registration_number : '-') ,
                array('name'=>'Phone Number' , 'value'=>$company ? $company->phone : '-') ,
                array('name'=>'Address' , 'value'=>$company ? $company->address : '-') ,
                array('name'=>'Street' , 'value'=>$company ? $company->street : '-') ,
                array('name'=>'City' , 'value'=>$company ? $company->city : '-') ,
                array('name'=>'Country' , 'value'=>$company ? $company->country : '-') ,
                AppInterface::getUploadedFiles($company, 'Bill' , 'bill_copy' , 'photos'),
                AppInterface::getUploadedFiles($company, 'Passport' , 'passport_copy' , 'photos'),
            ),
        ));
        ?>
    </div>
</div>
<div class="box-footer">
</div>
                    </div>
</div>