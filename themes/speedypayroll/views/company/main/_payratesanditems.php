<?php if ($isinout) { ?>
    <div class="row">
        <?php if ($payrates) { ?>
            <h3>Payrates</h3>
            <?php foreach ($payrates as $payratename) { ?>
                <div class="col-sm-12"><h4><?php echo ucwords(str_replace('_', ' ', $payratename->name)); ?></h4></div>
                <div class="col-sm-6">
                    Cost
                    <input type="text" name="Payrate1[<?php echo $payratename->name; ?>]" class="form-control" placeholder="£/hr" required="required">
                </div>
                <div class="col-sm-6">
                    Bill
                    <input type="text" name="Payrate2[<?php echo $payratename->name; ?>]" class="form-control" placeholder="£/hr" required="required">
                </div>
            <?php } ?>
        <?php } ?>
        <?php if ($items) { ?>
            <h3>Items</h3>
            <?php foreach ($items as $payratename) { ?>
                <div class="col-sm-12"><h4><?php echo ucwords(str_replace('_', ' ', $payratename->name)); ?></h4></div>
                <div class="col-sm-6">
                    Cost
                    <input type="text" name="Item1[<?php echo $payratename->name; ?>]" class="form-control" placeholder="£" required="required">
                </div>
                <div class="col-sm-6">
                    Bill
                    <input type="text" name="Item2[<?php echo $payratename->name; ?>]" class="form-control" placeholder="£" required="required">
                </div>
            <?php } ?>
        <?php } ?>
    </div>
<?php } else { ?>
    <?php if ($payrates) { ?>
        <div class="form-group">
            <div class="col-md-12 caption font-red-sunglo">
                <i class="icon-bag font-red-sunglo"></i>
                <span class="caption-subject bold uppercase">Payrates</span>
            </div>
        </div>
        <?php foreach ($payrates as $payratename) { ?>
            <div class="form-group col-md-12">
                <div class="col-md-3">
                    <?php echo ucwords(str_replace('_', ' ', $payratename->name)); ?>
                </div>
                <div class="col-md-9">
                    <input type="text" name="Payrate[<?php echo $payratename->name; ?>]" onkeypress="return isNumberKey(event);" class="form-control" placeholder="£/hr" required="required">
                </div>
            </div>
        <?php } ?>
    <?php } ?>
    <?php if ($items) { ?>
        <div class="form-group">
            <div class="col-md-12 caption font-red-sunglo">
                <i class="icon-bag font-red-sunglo"></i>
                <span class="caption-subject bold uppercase">Items</span>
            </div>
        </div>
        <?php foreach ($items as $payratename) { ?>
            <div class="form-group col-md-12">
                <div class="col-md-3">
                    <?php echo ucwords(str_replace('_', ' ', $payratename->name)); ?>
                </div>
                <div class="col-md-9">
                    <input type="text" name="Item[<?php echo $payratename->name; ?>]" onkeypress="return isNumberKey(event);" class="form-control" placeholder="£" required="required">
                </div>
            </div>
        <?php } ?>
    <?php } ?>
<?php } ?>

