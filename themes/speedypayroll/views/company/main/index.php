<?php
/* @var $this MainController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Companies',
);

$this->menu = array(
    array('label' => 'Create Company', 'url' => array('create')),
    array('label' => 'Manage Company', 'url' => array('admin')),
);
?>

<div class="box-header">
    <h3 class="box-title">Companies</h3>
    <div class="box-tools">
        <a class="btn btn-default pull-right" href="<?php echo $this->createUrl('create'); ?>">Create Company</a>
    </div>
</div>

<div class="box-body table-responsive">
    <table id="example1" class="table table-responsive table-striped dataTable" >
        <thead>
            <tr>
                <th><?php echo Company::model()->getAttributeLabel('name') ?></th>
                <th><?php echo Company::model()->getAttributeLabel('logo') ?></th>
                <th><?php echo Company::model()->getAttributeLabel('email') ?></th>
                <th><?php echo Company::model()->getAttributeLabel('phone') ?></th>
                <th><?php echo Company::model()->getAttributeLabel('sector') ?></th>
                <th><?php echo Company::model()->getAttributeLabel('registration_number') ?></th>
                <th><?php echo Company::model()->getAttributeLabel('vat_number') ?></th>
                <th><?php echo Company::model()->getAttributeLabel('address') ?></th>
                <th><?php echo Company::model()->getAttributeLabel('street') ?></th>
                <th><?php echo Company::model()->getAttributeLabel('city') ?></th>
                <th><?php echo Company::model()->getAttributeLabel('country') ?></th>
                <th><?php echo Company::model()->getAttributeLabel('post_code') ?></th>
                <th><?php echo Company::model()->getAttributeLabel('type') ?></th>
                <th><?php echo Company::model()->getAttributeLabel('status') ?></th>
                <th style="padding-right: 50px;">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($dataProvider as $item) { ?>
                <tr>
                    <td><?php echo $item->name; ?></td>
                    <td><?php echo $item->logo; ?></td>
                    <td><?php echo $item->email; ?></td>
                    <td><?php echo $item->phone; ?></td>
                    <td><?php echo $item->sector; ?></td>
                    <td><?php echo $item->registration_number; ?></td>
                    <td><?php echo $item->vat_number; ?></td>
                    <td><?php echo $item->address; ?></td>
                    <td><?php echo $item->street; ?></td>
                    <td><?php echo $item->city; ?></td>
                    <td><?php echo $item->country; ?></td>
                    <td><?php echo $item->post_code; ?></td>
                    <td><?php echo $item->type; ?></td>
                    <td><?php echo $item->status; ?></td>
                    <td>

                        <div class="btn-group">
                            <a class="btn btn-primary" style="padding-top: 0px;padding-bottom: 0px;" href="<?php echo $this->createUrl('view', array('id' => $item->id)); ?>">View</a>
                            <button type="button" class="btn btn-primary dropdown-toggle" style="padding-top: 0px;padding-bottom: 0px;" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php echo $this->createUrl('update', array('id' => $item->id)); ?>">Update</a></li>
                                <li class="divider"></li>
                                <li><a href="<?php echo $this->createUrl('delete', array('id' => $item->id)); ?>">Delete</a></li>
                                <?php if (AppUser::isUserSuperAdmin()){ ?>
                                <li><a href="<?php echo $this->createUrl('/admin/default/index/', array('id' => $item->id)); ?>">View Users</a></li>
                                <?php } ?>
                            </ul>
                        </div>

                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>