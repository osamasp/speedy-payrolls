<?php
/* @var $this ContactController */
/* @var $model Contact */

$this->breadcrumbs=array(
	'Contacts Manage',
);

$this->menu=array(
	array('label'=>'List Contact', 'url'=>array('index')),
	array('label'=>'Create Contact', 'url'=>array('create')),
);

?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/company-contact-admin.js"></script>
<div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user"></i>Contacts Management
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>

                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">

                        <div class="col-md-12">
                            
                        </div>
                    </div>
                </div>
<div class="box-header">
    <h3 class="box-title">Manage Contacts</h3>
</div>
<div class="box-body table-responsive">

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'contact-grid',
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		array('name'=>'company_id','value'=>'$data->company->name'),
		'name',
		'email',
		'type',
		'status',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
</div>
            </div></div>
<script>
$("table").attr('id','example1');
</script>