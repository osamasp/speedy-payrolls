<?php
/* @var $this ContactController */
/* @var $model Contact */
/* @var $form CActiveForm */
?>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'contact-form',
    'enableAjaxValidation' => false,
        ));
?>
<div class="box-body" style="padding-left:70px;">
<!--    <p class="note" >Fields with <span class="required">*</span> are required.</p>-->
    <?php if ($model->hasErrors()) { ?>
        <div class='callout callout-danger'>    <?php echo $form->errorSummary($model); ?>
        </div>
    <?php } ?>    
</div>
<div class="row" style="padding-left:70px;padding-top: 20px;padding-bottom: 20px;">
    <div class="col-md-6">

        <div class="form-group">
            <?php echo $form->labelEx($model, 'name'); ?>
            <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control ui-state-valid')); ?>
            <?php echo $form->error($model, 'name', array('class' => 'text-red')); ?>
        </div>

        <div>
            <?php echo $form->labelEx($model, 'email'); ?>
            <?php echo $form->textField($model, 'email', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control ui-state-valid', 'required' => 'required')); ?>
            <?php echo $form->error($model, 'email', array('class' => 'text-red')); ?>
        </div>

        <div class="form-group" style="display:none;">
            <?php echo $form->labelEx($model, 'type'); ?>
            <?php echo $form->dropDownList($model, 'type', array('Company' => 'Company'), array('class' => 'form-control ui-state-valid')); ?>
            <?php echo $form->error($model, 'type', array('class' => 'text-red')); ?>
        </div>
        <div>
            <?php echo $form->labelEx($model, 'address'); ?>
            <?php echo $form->textField($model, 'address', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control ui-state-valid', 'required' => 'required')); ?>
            <?php echo $form->error($model, 'address', array('class' => 'text-red')); ?>
        </div>
        <div>
            <?php echo $form->labelEx($model, 'city'); ?>
            <?php echo $form->textField($model, 'city', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control ui-state-valid', 'required' => 'required')); ?>
            <?php echo $form->error($model, 'city', array('class' => 'text-red')); ?>
        </div>
        <div>
            <?php echo $form->labelEx($model, 'country'); ?>
            <?php echo $form->dropDownList($model, 'country', AppInterface::getCountries(), array('class' => 'form-control ui-state-valid')); ?>        <?php echo $form->error($model, 'client_city', array('class' => 'text-red')); ?>

        </div>
        <div>
            <?php echo $form->labelEx($model, 'post_code'); ?>
            <?php echo $form->textField($model, 'post_code', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control ui-state-valid', 'required' => 'required')); ?>
            <?php echo $form->error($model, 'post_code', array('class' => 'text-red')); ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-9">
                    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary')); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>
