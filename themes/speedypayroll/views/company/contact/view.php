<?php
/* @var $this ContactController */
/* @var $model Contact */

$this->breadcrumbs = array(
    'Contacts' => array('index'),
    $model->name,
);

$this->menu = array(
    array('label' => 'List Contact', 'url' => array('index')),
    array('label' => 'Create Contact', 'url' => array('create')),
    array('label' => 'Update Contact', 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Delete Contact', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage Contact', 'url' => array('admin')),
);
?>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>View Contact
        </div>

    </div>
    <div class="portlet-body form">
        <div class="horizontal-form">
            <div class="form-body">

                <div class="portlet-body form">
                    <div class="box-header">
                        <h3 class="box-title">View Contact</h3>
                    </div>
                    <div class="table-responsive">
                        <div class="box-body">
                            <?php
                            $this->widget('zii.widgets.CDetailView', array(
                                'data' => $model,
                                'attributes' => array(
//                                    'id',
//                                    'company_id',
                                    'name',
                                    'email',
                                    'type',
                                    'status',
//                                    'created_at',
//                                    'created_by',
//                                    'modified_at',
//                                    'modified_by',
                                ),
                            ));
                            ?>
                        </div>
                    </div>
                </div></div></div></div></div>