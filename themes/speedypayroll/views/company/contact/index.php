<?php
/* @var $this ContactController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Contacts',
);

$this->menu = array(
    array('label' => 'Create Contact', 'url' => array('create')),
    array('label' => 'Manage Contact', 'url' => array('admin')),
);
?>

<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-copy"></i>Contact List
        </div>

    </div>
    <div class="portlet-body form">
        <div class="horizontal-form">
            <div class="form-body">

                <div class="portlet-body table-responsive">
                    <div class="box-body table-responsive">
                        <div class="row" style="padding-bottom: 10px;">
                            <div class="col-md-12">
                                <a class="btn btn-primary pull-right btn-circle" style="margin-left:20px;" href="<?php echo $this->createUrl('export'); ?>" >Export</a>
                                <a class="btn btn-primary pull-right btn-circle" href="<?php echo $this->createUrl('create'); ?>">Create Contact</a>
                            </div>
                        </div>
                        <table id="example1" class="table table-bordered table-responsive table-hover table-striped">
                            <thead>
                                <tr>

                                    <th><?php echo Contact::model()->getAttributeLabel('company_id') ?></th>
                                    <th><?php echo Contact::model()->getAttributeLabel('name') ?></th>
                                    <th><?php echo Contact::model()->getAttributeLabel('email') ?></th>
                                    <th><?php echo Contact::model()->getAttributeLabel('created_at') ?></th>               
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($dataProvider as $item) { ?>
                                    <tr>

                                        <td><?php echo $item->company->name; ?></td>
                                        <td><?php echo $item->name; ?></td>
                                        <td><?php echo $item->email; ?></td>
                                        <td><?php echo date('d M Y', $item->created_at); ?></td>
                                        <td>

                                            <div class="btn-group">
                                                <button type="button" class="btn green dropdown-toggle" data-toggle="dropdown">
                                                    Actions<i class="fa fa-angle-down"></i>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="<?php echo $this->createUrl('view', array('id' => $item->id)); ?>">View</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="<?php echo $this->createUrl('update', array('id' => $item->id)); ?>">Update</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="<?php echo $this->createUrl('delete', array('id' => $item->id)); ?>">Delete</a></li>
                                                </ul>
                                            </div>

                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div></div>