<?php
/* @var $this ContactController */
/* @var $model Contact */

$this->breadcrumbs=array(
	'Contacts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Contact', 'url'=>array('index')),
	array('label'=>'Manage Contact', 'url'=>array('admin')),
);
?>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Create Contact
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>

        </div>
    </div>
    <div class="portlet-body form">
<!--<div class="box-header" style="padding-left:30px;padding-top:10px;">
    <h3 class="box-title">Create Contact</h3>
</div>-->
<?php $this->renderPartial('_form', array('model'=>$model)); ?>
      </div>
</div>