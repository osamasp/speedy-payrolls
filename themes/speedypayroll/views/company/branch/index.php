<?php
/* @var $this BranchController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Branches',
);

$this->menu = array(
    array('label' => 'Create Branch', 'url' => array('create')),
    array('label' => 'Manage Branch', 'url' => array('admin')),
);
?>

<!--<div class="box-header">
    <h3 class="box-title">Branches</h3>
    <div class="box-tools">
        <a class="btn btn-default pull-right" href="<?php // echo $this->createUrl('create'); ?>">Create Branch</a>
    </div>
</div>-->




<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-copy"></i>Company Branches
        </div>
    </div>

    <div class="portlet-body table-responsive">
        <a class="btn btn-default blue pull-right" href="<?php echo $this->createUrl('create'); ?>">Create Branch</a>
        <div class="box-body table-responsive" style="padding-top:10px;">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                                                                <!--<th><?php // echo CompanyShifts::model()->getAttributeLabel('id')   ?></th>-->

                        <th><?php echo Branch::model()->getAttributeLabel('name') ?></th>
                        <th><?php echo Branch::model()->getAttributeLabel('latitude') ?></th>
                        <th><?php echo Branch::model()->getAttributeLabel('longitude') ?></th>
                        <th><?php echo Branch::model()->getAttributeLabel('address') ?></th>
                        <th><?php echo Branch::model()->getAttributeLabel('radius') ?></th>
                        <th><?php echo Branch::model()->getAttributeLabel('company_id') ?></th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                        <?php
//            dd($dataProvider);
                        foreach ($dataProvider as $item) {
                            ?>
                            <tr>
                                <td><?php echo $item->name; ?></td>
                                <td><?php echo $item->latitude; ?></td>
                                <td><?php echo $item->longitude; ?></td>
                                <td><?php echo $item->address; ?></td>
                                <td><?php echo $item->radius; ?></td>
                                <td><?php echo $item->company->name; ?></td>
                                <td>

                                    <div class="btn-group">
                                        <button type="button" class="btn green dropdown-toggle" data-toggle="dropdown">
                                            Actions<i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="<?php echo $this->createUrl('view', array('id' => $item->id)); ?>">View</a></li>
                                            <li><a href="<?php echo $this->createUrl('update', array('id' => $item->id)); ?>">Update</a></li>
                                            <li class="divider"></li>
                                            <li><a href="<?php echo $this->createUrl('delete', array('id' => $item->id)); ?>">Delete</a></li>
                                        </ul>
                                    </div>



                                </td>
                            </tr>
                        <?php
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>