<?php
/* @var $this BranchController */
/* @var $model Branch */

//$this->breadcrumbs=array(
//	'Branches'=>array('index'),
//	$model->name=>array('view','id'=>$model->id),
//	'Update',
//);
//
//$this->menu=array(
//	array('label'=>'List Branch', 'url'=>array('index')),
//	array('label'=>'Create Branch', 'url'=>array('create')),
//	array('label'=>'View Branch', 'url'=>array('view', 'id'=>$model->id)),
//	array('label'=>'Manage Branch', 'url'=>array('admin')),
//);
?>







<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Update Branch
        </div>

    </div>
    <div class="portlet-body form">
        <div class="horizontal-form">
            <div class="form-body">

                <div class="portlet-body form">


<?php $this->renderPartial('_form', array('model'=>$model,'is_update'=>1)); ?>
                </div>
            </div>
        </div>
    </div>
</div>