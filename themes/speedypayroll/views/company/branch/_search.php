<?php
/* @var $this BranchController */
/* @var $model Branch */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

        <div class="form-group">
        <?php echo $form->label($model,'id'); ?>
        <?php echo $form->textField($model,'id',array('size'=>30,'maxlength'=>30,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'name'); ?>
        <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'latitude'); ?>
        <?php echo $form->textField($model,'latitude', array('class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'longitude'); ?>
        <?php echo $form->textField($model,'longitude', array('class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'address'); ?>
        <?php echo $form->textField($model,'address',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'diameter'); ?>
        <?php echo $form->textField($model,'diameter', array('class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'branch_admin_id'); ?>
        <?php echo $form->textField($model,'branch_admin_id',array('size'=>30,'maxlength'=>30,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'company_id'); ?>
        <?php echo $form->textField($model,'company_id',array('size'=>30,'maxlength'=>30,'class' => 'form-control')); ?>
    </div>

<div class="form-group">
    <?php echo CHtml::submitButton('Search'); ?>
</div>

<?php $this->endWidget(); ?>
<!-- search-form -->