<?php
/* @var $this BranchController */
/* @var $model Branch */

//$this->breadcrumbs = array(
//    'Branches' => array('index'),
//    'Create',
//);
//
//$this->menu = array(
//    array('label' => 'List Branch', 'url' => array('index')),
//    array('label' => 'Manage Branch', 'url' => array('admin')),
//);
?>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i>Create Branch
        </div>
    </div>
    <div class="portlet-body form" >
        <div class="box-body" style="padding-left: 20px; padding-right: 20px;">
    <?php $this->renderPartial('_form', array('model' => $model,'user_id' => $user_id,'company_id' => $company_id)); ?>      
            </div>
        </div>
</div>
