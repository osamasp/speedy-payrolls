<!--for map--> 
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/gmaps.js" type="text/javascript"></script>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/examples.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="//maps.google.com/maps/api/js?sensor=true"></script>
 <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.ui.addresspicker.js"></script>
<!--end for map-->

<?php
/* @var $this BranchController */
/* @var $model Branch */
/* @var $form CActiveForm */
?>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'branch-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => false,
        ));
?>

<div class="box-body">
    <!--<p class="note" >Fields with <span class="required">*</span> are required.</p>-->
    <?php if ($model->hasErrors()) { ?>
        <div class='callout callout-danger'>    <?php echo $form->errorSummary($model); ?>
        </div>
    <?php } ?>    
</div>
<div class="row" style="padding-top: 20px;padding-bottom: 20px;">
    <div class="col-md-12">
        <div class="form-group">
            <label for="name">Name*</label>
            <?php echo $form->textField($model, 'name', array('size' => 60, 'required'=>'required', 'maxlength' => 255, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'name', array('class' => 'text-red')); ?>
        </div>
<!--        <div class="form-group">
            <label for="latitued">Latitude*</label>
            <?php // echo $form->textField($model, 'latitude', array('class' => 'form-control', 'id' => 'latitude', 'readonly' => 'true')); ?>
            <?php // echo $form->error($model, 'latitude', array('class' => 'text-red')); ?>
        </div>-->
<input type="hidden" name="Branch[latitude]" id="latitude">
<input type="hidden" name="Branch[longitude]" id="longitude">
<input type="hidden" name="Branch[latlng]" id="latlng">
<!--        <div class="form-group">
            <label for="longitude">Longitude*</label>
            <?php // echo $form->textField($model, 'longitude', array('class' => 'form-control', 'id' => 'longitude', 'readonly' => 'true')); ?>
            <?php // echo $form->error($model, 'longitude', array('class' => 'text-red')); ?>
        </div>-->
        
        <div class="form-group">
            <label for="radius">Radius*</label>
            <?php echo $form->textField($model, 'radius', array('class' => 'form-control', 'required'=>'required', 'id' => 'radius_cust', 'onchange' => 'setMap()')); ?>
            <?php echo $form->error($model, 'radius', array('class' => 'text-red')); ?>
        </div>
        <div class="form-group padding-left-10">
            <label for="address">Address*</label>
            <?php echo $form->textField($model, 'address', array('size' => 60, 'required'=>'required', 'maxlength' => 255, 'onkeyup' => 'setMap()', 'class' => 'form-control', 'id' => 'address')); ?>
            <?php echo $form->error($model, 'address', array('class' => 'text-red')); ?>
        </div>
        
        <div class="form-group padding-left-10">
            Latitude : <label for="latitude_label" id="latitude_label"> </label>
            
        </div>
        <div class="form-group padding-left-10">
            
            Longitude : <label for="longitude_label" id="longitude_label"></label>
        </div>
        
    </div>
    <div class="row">
        <div class="span11">
            <div id="map"></div>
        </div>

    </div>
</div>
<div class="row">

    <div class="col-md-12">
        <div class="form-actions right">
            <div class="row">
                <div class="col-md-offset-3 col-md-9">
                    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary green-haze circle')); ?>
                </div>
            </div>
        </div>
    </div>
</div>


<?php $this->endWidget(); ?>

<script type="text/javascript">

    var map;
    var marker;
    var circle;
    var rad = $("#radius_cust").val();
    var latitude = $("#latitude").val();
    var longitude = $("#longitude").val();

    function setRadius() {
        setMap();
    }

    $(document).ready(function () {

        if (latitude != '' && longitude != '') {
            setLocationAndMarkerOnMap(latitude, latitude);
            setMap();
        } else {
            setLocationAndMarkerOnMap(51.5073509, -0.12775829999998223);
            setMap();
        }
        
         });
        function setLocationAndMarkerOnMap(lat, lng)
        {
            map = new GMaps({
                el: '#map',
                lat: lat,
                lng: lng
            });
            map.addListener('click', function (event) {
                placeMarker(event.latLng);
            });
        }

        function placeMarker(location) {
            var rad = $("#radius_cust").val();
            var geocoder = new google.maps.Geocoder;
            var infowindow = new google.maps.InfoWindow;
            console.log(location);
            setLocationAndMarkerOnMap(location.lat(), location.lng());
            if (location != null) {
                map.addMarker({
                    lat: location.lat(),
                    lng: location.lng()
                });
                map.setCenter({
                    lat: location.lat(),
                    lng: location.lng()
                });
                circle = map.drawCircle({center: {
                            lat: location.lat(),
                            lng: location.lng()
                        }, radius: parseFloat(rad),
                        strokeColor: '#800000',
                        strokeOpacity: 1.0,
                        strokeWeight: 1,
                        fillColor: '#C64D45',
                        fillOpacity: 0.5,
                });
                $("#latitude").val(location.lat());
                $("#longitude").val(location.lng());
                $("#latitude_label").html($("#latitude").val());
                $("#longitude_label").html($("#longitude").val());
                $("#latlng").val($("#latitude").val() + ','+ $("#longitude").val());
                var input = $("#latlng").val();
                var latlngStr = input.split(',', 2);
                var latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
                
                geocoder.geocode({'location': latlng}, function(results, status) {
                            
                        if (status === google.maps.GeocoderStatus.OK) {
                              if (results[1]) {
//                                map.setZoom(11);
                                var marker = new google.maps.Marker({
                                  position: latlng,
                                  map: map
                                });
                                
                                infowindow.setContent(results[1].formatted_address);
                                $("#address").val(results[1].formatted_address);
                                infowindow.open(map, marker);
                              } else {
                                window.alert('No results found');
                              }
                            } else {
                              window.alert('Geocoder failed due to: ' + status);
                            }
                        });
                
            }
        }
   
    function setMap() {
        GMaps.geocode({
            address: $('#address').val().trim(),
            callback: function (results, status) {
                if (status == 'OK') {
                    if (marker != null) {
                        clearMarkers();
                        circle.setMap(null);
                    }
                    var latlng = results[0].geometry.location;
                    map.setCenter(latlng.lat(), latlng.lng());
                    var latitude = latlng.lat();
                    var longitude = latlng.lng();
                    var rad = $('#radius_cust').val();
                    setLocationAndMarkerOnMap(latlng.lat(), latlng.lng());
                    marker = map.addMarker({
                        lat: latlng.lat(),
                        lng: latlng.lng()
                    });
                    circle = map.drawCircle({center: {
                            lat: latlng.lat(),
                            lng: latlng.lng(),
                        }, radius: parseFloat(rad),
                        strokeColor: '#800000',
                        strokeOpacity: 1.0,
                        strokeWeight: 1,
                        fillColor: '#C64D45',
                        fillOpacity: 0.5,
                    });
                    
                    
                    $("#latitude").val(latitude);
                    $("#longitude").val(longitude);
                    $("#latitude_label").html($("#latitude").val());
                    $("#longitude_label").html($("#longitude").val());
                }
            }
        });

    }
    

    // Gmap new code for addressPicker..
//
 $(function() {
    var addresspicker = $( "#addresspicker" ).addresspicker({
      componentsFilter: 'country:FR'
    });
    var addresspickerMap = $( "#address" ).addresspicker({
      regionBias: "fr",
      language: "fr",
      updateCallback: showCallback,
      mapOptions: {
        zoom: 4,
        center: new google.maps.LatLng(46, 2),
        scrollwheel: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      },
    });
//
//    var gmarker = addresspickerMap.addresspicker( "marker");
//    gmarker.setVisible(true);
//    addresspickerMap.addresspicker( "updatePosition");

    $('#reverseGeocode').change(function(){
      $("#address").addresspicker($(this).val() === 'true');
    });
    function showCallback(geocodeResult, parsedGeocodeResult){
      $('#callback_result').text(JSON.stringify(parsedGeocodeResult, null, 4));
    }


  });
  
  // address icker code ends here..



    // Sets the map on all markers in the array.
    function setMapOnAll(map) {
        marker.setMap(map);
    }

// Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
        setMapOnAll(null);
    }

// Shows any markers currently in the array.
    function showMarkers() {
        setMapOnAll(map);
    }

// Deletes all markers in the array by removing references to them.
    function deleteMarkers() {
        clearMarkers();
    }
    
    

</script>

