<!--for map--> 
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/gmaps.js" type="text/javascript"></script>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/examples.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="//maps.google.com/maps/api/js?sensor=true"></script>
<!--end for map-->

<?php
/* @var $this BranchController */
/* @var $model Branch */

$this->breadcrumbs = array(
    'Branches' => array('index'),
    $model->name,
);

$this->menu = array(
    array('label' => 'List Branch', 'url' => array('index')),
    array('label' => 'Create Branch', 'url' => array('create')),
    array('label' => 'Update Branch', 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Delete Branch', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage Branch', 'url' => array('admin')),
);
?>



<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>View Branch
        </div>

    </div>
    <div class="portlet-body form">
        <div class="horizontal-form">
            <div class="form-body">
                <div class="title">
                    <h2><?php echo $model->company->name. " - ".$model->name; ?></h2>
                </div>
                <div class="portlet-body form">
                    
                    <div class="table-responsive">
                        <div class="box-body">
                            <?php
            $this->widget('zii.widgets.CDetailView', array(
                'data' => $model,
                'attributes' => array(
                    
                    'name',
                    'latitude',
                    'longitude',
                    'address',
                    'radius',
                    array('name' => 'Company', 'value' => $model->company->name),
                    ),
            ));
            ?>
                            <input type="hidden" name="address" id="address" value="<?php echo $model->address; ?>">
                        </div>
                    </div>
                </div></div></div></div>
    <div style="background-color: white;">
        <div class="span11">
            <div id="map"></div>
        </div>

    </div>


</div>



<script type="text/javascript">
        var map;
        var rad = <?php echo $model->radius; ?> ;
        var latitude = <?php echo $model->latitude; ?>;
        var longitude = <?php echo $model->longitude; ?>; 
        
        
        $(document).ready(function () {
                map = new GMaps({
                    el: '#map',
                    lat: latitude,
                    lng: longitude,
                });
//                var address = <?php // echo $model->address; ?>;
               
                setMap(rad);
                 });
        function setMap(rad) {
//             alert(rad);
        GMaps.geocode({
            address: $('#address').val().trim(),
            callback: function (results, status) {
                if (status == 'OK') {
                    var latlng = results[0].geometry.location;
                    map.setCenter(latlng.lat(), latlng.lng());
//                    var latitude = latlng.lat();
//                    var longitude = latlng.lng();
                    map.addMarker({
                        lat: latlng.lat(),
                        lng: latlng.lng()
                    });
                    map.drawCircle({center: {
                            lat: latlng.lat(),
                            lng: latlng.lng(),
                        }, radius: rad,
                        strokeColor: '#800000',
                        strokeOpacity: 1.0,
                        strokeWeight: 1,
                        fillColor: '#C64D45',
                        fillOpacity: 0.5,
                    });
//                    $("#latitude").val(latitude);
//                    $("#longitude").val(longitude);

                }
            }
        });

    }



</script>