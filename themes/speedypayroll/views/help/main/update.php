<?php
/* @var $this MainController */
/* @var $model Help */

$this->breadcrumbs=array(
	'Helps'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Help', 'url'=>array('index')),
	array('label'=>'Create Help', 'url'=>array('create')),
	array('label'=>'View Help', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Help', 'url'=>array('admin')),
);
?>

<div class="box-header">
    <h3 class="box-title">Update Help <?php echo $model->id; ?></h3>
</div>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>