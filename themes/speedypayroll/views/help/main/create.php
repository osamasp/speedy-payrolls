<?php
/* @var $this MainController */
/* @var $model Help */

$this->breadcrumbs=array(
	'Helps'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Help', 'url'=>array('index')),
	array('label'=>'Manage Help', 'url'=>array('admin')),
);
?>

<!--<div class="box-header">
    <h3 class="box-title">Create Help</h3>
</div>-->

<?php $this->renderPartial('_form', array('model'=>$model)); ?>