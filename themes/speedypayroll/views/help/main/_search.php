<?php
/* @var $this MainController */
/* @var $model Help */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

        <div class="form-group">
        <?php echo $form->label($model,'id'); ?>
        <?php echo $form->textField($model,'id',array('size'=>20,'maxlength'=>20,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'title'); ?>
        <?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'route'); ?>
        <?php echo $form->textField($model,'route',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'html'); ?>
        <?php echo $form->textArea($model,'html',array('rows'=>6, 'cols'=>50,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'status'); ?>
        <?php echo $form->textField($model,'status', array('class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'created_by'); ?>
        <?php echo $form->textField($model,'created_by',array('size'=>20,'maxlength'=>20,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'created_at'); ?>
        <?php echo $form->textField($model,'created_at',array('size'=>20,'maxlength'=>20,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'modified_by'); ?>
        <?php echo $form->textField($model,'modified_by',array('size'=>20,'maxlength'=>20,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'modified_at'); ?>
        <?php echo $form->textField($model,'modified_at',array('size'=>20,'maxlength'=>20,'class' => 'form-control')); ?>
    </div>

<div class="form-group">
    <?php echo CHtml::submitButton('Search'); ?>
</div>

<?php $this->endWidget(); ?>
<!-- search-form -->