<div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-plus"></i>Create Help
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>

                </div>
            </div>
            <div class="portlet-body form">
                <div class="table-toolbar">
                    <div class="row">

                        <div class="col-md-12">
                            
                        </div>
                    </div>
                </div>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'help-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => false,
        ));
?>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/ckeditor/ckeditor.js"></script>
<div class="box-body" style="padding-left:20px;padding-right:20px;">
    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php if ($model->hasErrors()) { ?>
        <div class='callout callout-danger'>    <?php echo $form->errorSummary($model); ?>
        </div>
    <?php } ?>    
    <div class="form-group">
        <?php echo $form->labelEx($model, 'title'); ?>
        <?php echo $form->textField($model, 'title', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
        <?php echo $form->error($model, 'title', array('class' => 'text-red')); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'route'); ?>
        <?php echo $form->textField($model, 'route', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
        <?php echo $form->error($model, 'route', array('class' => 'text-red')); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'html'); ?>
        <?php echo $form->textArea($model, 'html', array('id'=>'editor1','rows' => 6, 'cols' => 50, 'class' => 'form-control')); ?>
        <?php echo $form->error($model, 'html', array('class' => 'text-red')); ?>
    </div>

</div>
<div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-9">
    <input type="submit" name="save" value='Save' class="btn btn-primary">
    <?php if ($model->isNewRecord) { ?>
        <input type="submit" name="addmore" value="Save & Add More" class="btn btn-primary">
    <?php } ?>
</div>
                </div>
</div>
<script>
    CKEDITOR.replace('editor1');
</script>
<?php $this->endWidget(); ?>
            </div>
</div>