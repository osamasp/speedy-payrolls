<?php
/* @var $this MainController */
/* @var $model Help */

$this->breadcrumbs=array(
	'Helps'=>array('index'),
	$model->title,
);

$this->menu=array(
array('label'=>'List Help', 'url'=>array('index')),
array('label'=>'Create Help', 'url'=>array('create')),
array('label'=>'Update Help', 'url'=>array('update', 'id'=>$model->id)),
array('label'=>'Delete Help', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Help', 'url'=>array('admin')),
);
?>
<div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-globe"></i>Billing
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>

                </div>
            </div>
            <div class="portlet-body form">
                <div class="table-toolbar">
                    <div class="row">

                        <div class="col-md-12">
                            
                        </div>
                    </div>
                </div>
<div class="box-header">
    <h3 class="box-title">View Help #<?php echo $model->id; ?></h3>
</div>
<div class="table-responsive">
    <div class="box-body">
        <?php $this->widget('zii.widgets.CDetailView', array(
        'data'=>$model,
        'attributes'=>array(
        		'id',
		'title',
		'route',
		'html',
		'status',
		'created_by',
		'created_at',
		'modified_by',
		'modified_at',
        ),
        )); ?>
    </div>
</div>
            </div>
</div>