<?php
/* @var $this MainController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Helps',
);

$this->menu = array(
    array('label' => 'Create Help', 'url' => array('create')),
    array('label' => 'Manage Help', 'url' => array('admin')),
);
?>
<div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-copy"></i>Help List
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>

                </div>
            </div>
            <div class="portlet-body table-responsive">
                <div class="table-toolbar">
                    <div class="row">

                        <div class="col-md-12">
                            
                        </div>
                    </div>
                </div>
<div class="box-header">
    <h3 class="box-title">Helps</h3>
    <div class="box-tools">
        
    </div>
</div>
                <div class="row">
                    <div class="col-md-12">
                        <a class="btn btn-primary pull-right" href="<?php echo $this->createUrl('create'); ?>">Create Help</a>
                    </div>
                </div>
<div class="box-body table-responsive">
    <table id="example1" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th><input type="checkbox" name="check" clas="form-control" /></th>
                <th><?php echo Help::model()->getAttributeLabel('title') ?></th>
                <th><?php echo Help::model()->getAttributeLabel('route') ?></th>
                <th><?php echo Help::model()->getAttributeLabel('created_at') ?></th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($dataProvider as $item) { ?>
                <tr class="gradeX">
                    <td><input type="checkbox" name="check" value="<?php echo $item->id; ?>" clas="form-control" /></td>
                    <td><?php echo $item->title; ?></td>
                    <td><?php echo $item->route; ?></td>
                    <td><?php echo date(AppInterface::getdateformat(),$item->created_at); ?></td>
                    <td>
                        <div class="btn-group">
                                <button class="btn green dropdown-toggle" data-toggle="dropdown">Actions <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="<?php echo $this->createUrl('view', array('id' => $item->id)); ?>">View</a></li>
                        
                                <li><a href="<?php echo $this->createUrl('update', array('id' => $item->id)); ?>">Update</a></li>
                                <li class="divider"></li>
                                <li><a href="<?php echo $this->createUrl('delete', array('id' => $item->id)); ?>">Delete</a></li>
                            </ul>
                        </div>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
            </div>
            </div>