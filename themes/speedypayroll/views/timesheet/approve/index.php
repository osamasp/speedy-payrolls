<?php
/* @var $this ApproveController */
/* @var $model Timesheet */

$this->breadcrumbs = array(
    'Approve Timesheets' => array('index'),
);
?>
<!-- data table starts -->
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/dropdowns-enhancement.css" rel="stylesheet">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/timesheet-main.js"></script>
<input type="hidden" id="export_url" value="<?php echo $this->createUrl("export"); ?>"/>
<input type="hidden" id="card_url" value="<?php echo $this->createUrl("/timesheet/main/GetSubEntries"); ?>"/>
<input type="hidden" id="sheet_url" value="<?php echo $this->createUrl("/timesheet/main/GetSheetCard"); ?>"/>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'myForm',
    'enableAjaxValidation' => false,
        ));
?>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box grey-cascade">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-globe"></i> Timesheet Approvals
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="btn-group pull-right" style="padding-left:10px;">
                                        <!--<a class="btn btn-success pull-right btn-circle" href="<?php // echo $this->createUrl('add');       ?>">Add Employee</a>-->
                                    </div>
                                    <?php if (AppUser::isUserAdmin()) { ?>
                                        <div class="btn-group pull-right">
                                            <button class="btn green dropdown-toggle" data-toggle="dropdown">Export <i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul class="dropdown-menu pull-right">				
                                                <li><a id="export" href="javascript:void(0);" onclick="exportData('export')">Export Selected Record</a></li>
                                                <li class="divider"></li>
                                                <li><a  id="export_all" href="javascript:void(0);"  onclick="exportData('export_all')">Export All Record</a></li>
                                                <li><input type="checkbox" name="export_fields[]" id="All" value="1"> <label id="selectAll" for="All">Select All</label></li>
                                                <?php
                                                if (isset($entries[0])) {
                                                    foreach ($entries[0] as $key => $item) {
                                                        if (!in_array($key, $not_included_fields)) {
                                                            ?>
                                                            <li><input class="select" type="checkbox" name="export_fields[]" id="<?php echo $key; ?>" value="<?php echo $key; ?>"><label for="<?php echo $key; ?>"><?php echo Timesheet::model()->getAttributeLabel($key); ?></label></li>
                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                            </ul>
                                        </div>
<?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-bordered table-hover" id="example1">
                    <thead>
                        <tr>
                            <th class="table-checkbox">
                                <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes"/>
                            </th>
                            <th>
                                Contract
                            </th>
                            <th>
                                Employee
                            </th>
                            <th>
                                ID
                            </th>
                            <th>
                                Period
                            </th>
                            <th>
                                Status
                            </th>
                            <th>
                                PaySlip
                            </th>
                            <th>
                                Action
                            </th>

                        </tr>
                    </thead>
                        <?php if (count($entries) > 0) { ?>
                        <tbody>
    <?php foreach ($entries as $entry) { ?>
                                <tr class="odd gradeX">
                                    <td><input type="checkbox" class="checkboxes" value="<?php echo $entry->id; ?>"/></td>
                                    <td><?php echo AppContract::getContractCustomName($entry->contract); ?></td>
                                    <td><?php echo $entry->contract->user->full_name; ?></td>
                                    <td><?php echo AppInterface::formatID($entry->id); ?></td>
                                    <td><?php echo AppTimeSheet::formatSheetPeriod($entry); //ucwords(str_replace('_', ' ', $entry->contract->CustomEmploymentType));      ?></td>    
                                    <td><?php echo $entry->status; ?></td>
                                    <td>
        <?php echo Payslip//$entry->payrolls->payslipEntries;   ?></td>
                                    <td>
                                        <div class="btn-group pull-right">
                                            <a class="btn btn-primary" style="padding-top: 0px;padding-bottom: 0px;" href="javscript:">Actions</a>
                                            <button type="button" class="btn green dropdown-toggle" style="padding-top: 0px;padding-bottom: 0px;" data-toggle="dropdown">
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <ul class="dropdown-menu pull-right" role="menu">
                                                <li><a data-toggle="modal" data-target=".ajaxModel" data-whatever="<?php echo $entry->id; ?>" >View</a></li>
                                                <?php if (( $entry->type != "daily" && ( AppUser::isUserAdmin() || $entry->status == "pending"))) { ?>
                                                    <li><a href="<?php echo Yii::app()->createUrl("timesheet/main/edit", array("entry" => $entry->id)); ?>">Update</a></li>
                                                <?php } ?>

        <?php if ($entry->type == "daily") { ?>
                                                    <li class="divider"></li>
                                                    <li><a href="#childEntryContainer" class="<?php echo ($entry->type == "daily") ? "wklyLnkPrnt" : ""; ?>" tSheetId="<?php echo $entry->id; ?>">Show TimeCard</a></li>
                                                <?php } ?>
        <?php if ($entry->status == "approved" && count($entry->payrolls) == 0) { ?>
                                                    <li class="divider"></li>
                                                    <li><a href="<?php echo Yii::app()->createUrl("timesheet/main/DipatchPayroll", array("entry" => $entry->id)); ?>" class="confirmAct" >Request Payslip</a></li>
        <?php } ?>
                                                <li class="divider"></li>
                                                <li><a href="<?php echo Yii::app()->createUrl("timesheet/approve/update", array("entry" => $entry->id)); ?>" class="confirmAct" >Approve</a></li>
                                                <li class="divider"></li>
                                                <li><a href="<?php echo Yii::app()->createUrl("timesheet/approve/decline", array("entry" => $entry->id)); ?>" class="confirmAct" >Decline</a></li>
                                                <li class="divider"></li>
                                                <li><a href="<?php echo Yii::app()->createUrl("timesheet/main/ChangeStatus", array("entry" => $entry->id, "action" => "closed")); ?>" class="confirmAct" >Mark as Closed</a></li>

                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                        <?php } ?>
                        </tbody>
                <?php } ?>
                </table>
                <?php
                $this->widget('CLinkPager', array(
                    'pages' => $pages,
                    'header' => '',
                    'footer' => '',
                    'maxButtonCount' => 3,
                    'nextPageLabel' => 'Next',
                    'prevPageLabel' => 'Prev',
                    'selectedPageCssClass' => 'active',
                    'hiddenPageCssClass' => 'disabled',
                    'htmlOptions' => array(
                        'class' => 'pagination pull-right',
                    )
                ));
                ?>
                <div class="childEntryContainer" style="display:none">

                </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>      <!-- /data table ends -->
<?php $this->endWidget(); ?>
<script>
    $('#selectAll').click(function(event) {  //on click 
        if ($("#All").is(":checked")) { // check select status
            $('.select').each(function() { //loop through each checkbox
                this.checked = false;  //select all checkboxes with class "checkbox1"               
            });
        } else {

            $('.select').each(function() { //loop through each checkbox
                this.checked = true; //deselect all checkboxes with class "checkbox1"                       
            });
        }
    });
    $('#openButton').on('click', function(event) {
        $(this).parent().toggleClass('open');
    });
</script>