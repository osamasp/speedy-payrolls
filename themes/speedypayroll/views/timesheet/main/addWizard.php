<?php
/* @var $this MainController */
/* @var $model Timesheet */

$this->breadcrumbs = array(
    'Timesheets' => array('index'),
    'Add - Step 1',
);

$this->menu = array(
    array('label' => 'List Contract', 'url' => array('index')),
    array('label' => 'Manage Contract', 'url' => array('admin')),
);



?>


<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-plus-square"></i>Add New Daily Timesheet
							</div>
						</div>
						<div class="portlet-body form">
<?php 
if($contracts==null || count($contracts)==0){
    return;
}
$this->renderPartial('_wizardform', array('tSheetType' => $tSheetType, 'contracts' => $contracts, 'error' => $error)); ?>
                                                </div>
</div>