<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="row">
    <div class="col-md-11"><h3 style="font-size:22px;">Timesheet Details:</h3></div>
</div>
<div class="row" style="padding-left:10px;">

    <div class="col-md-6">
        <p><b>Name:</b> <?php echo $employeeName; ?></p>
        <p><b>NI Number:</b> <?php echo $ni_number; ?></p>
        <p><b>Company:</b> <?php echo ucfirst($company); ?></p>
        <p><b>Location:</b> <?php echo $companyCity . ',' . $companyCountry; ?></p>
        <br>
        <?php if (isset($payHours) && count($payHours) > 0) { ?>
            <?php foreach ($payHours as $key => $payRate) { ?>
                <p><?php echo ucfirst($key) . ' hours: ' . $payRate . ' x £' . ($payRates ? $payRates[$key] : ''); ?> </p>
            <?php } ?>
        <?php } ?>
        <br>
        <?php if (isset($itemCount) && count($itemCount) > 0) { ?>
            <?php foreach ($itemCount as $key => $item) { ?>
                <p><?php echo ucfirst($key) . ': ' . $item . ' x £' . ($itemRates ? $itemRates[$key] : ''); ?> </p>
            <?php } ?>
        <?php } ?>
        <br> 
        <p><b>Expense:</b> <?php echo '£' . $expense; ?></p>

        <!-- Your first column here -->
    </div>
    <div class="col-md-6">
        <p><b>Staff ID:</b> <?php echo $employeeId; ?></p>
        <p><b>PaySlip Period:</b> <?php echo AppTimeSheet::formatSheetPeriod($entry); ?></p>
        <p><b>Date Submitted:</b> <?php echo date(AppInterface::getdateformat(), $date); ?></p>
        <p><b>Approver:</b> <?php echo $approver; ?></p>

        <p><b>Employment Type:</b> <?php echo AppContract::formatEmployeType($employeeType); ?></p>
        <!-- Your second column here -->
    </div>


</div> 
<div class="row">
    <div class="col-md-11" >
        <h3 style="font-size:22px;">Applicable Company Policies:</h3>
        <div class='col-md-10'>
            <?php echo $company_policies; ?>
        </div>
    </div>

</div>


