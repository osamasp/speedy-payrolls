<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'invoice-form',
    'action' => Yii::app()->createUrl('/invoice/main/create'),
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => false
        ));
?>
<hr>
<div class="col-md-6">
    <input type="hidden" name="Invoice[timesheet_id]" value="<?php echo $timesheetId ?>"> 
    <h4>Send Invoice</h4>
    <div class="form-group">
        <label>Send Invoice To:</label>
        <?php if ($contacts = AppCompany::getCompanyContacts()) { ?>
        <select name='Invoice[contact_id]' required="" class="form-control">
                <option value="">Select Contact</option>
                <?php foreach ($contacts as $contact) { ?>
                    <option value="<?php echo $contact->id; ?>"><?php echo $contact->name; ?></option>
                <?php } ?>
            </select>
        <?php } else { ?>
            <p>Your company have no contacts. <a href="<?php echo Yii::app()->createUrl('/company/contact/create') ?>">Add Contacts</a></p>
        <?php } ?>
    </div>
    <div class="form-group">
        <label>Invoice From:</label>
        <input type="text" name="from" value='<?php echo AppUser::getUserCompany()->name; ?>' disabled="" class="form-control">
    </div>
    <div class="form-group">
        <input type="submit" value="Invoice" name="submit" class="btn btn-primary">
    </div>
</div>
<?php $this->endWidget(); ?>