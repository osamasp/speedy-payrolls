<?php
/* @var $this MainController */
/* @var $model Contract */
/* @var $form CActiveForm */

$form = $this->beginWidget('CActiveForm', array(
    'id' => 'timesheet-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class'=>'form-horizontal form-bordered',
    ),
        ));
?>
<div class="box-body">
    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php if ($model->hasErrors()) { ?>
        <div class='callout callout-danger'>    <?php echo $form->errorSummary($model); ?>
        </div>
    <?php } ?>    
    <div class="form-group">
                    <label class="control-label col-md-3"><?php echo CHtml::label("Timesheet Account *", 'type'); ?></label>
        <div class="col-md-8">
        <?php
        if ($contracts != null && count($contracts) > 0) {
            $htmlOption = array('class' => 'autoPostDD form-control');
            if (!$model->isNewRecord) {
                $htmlOption['disabled'] = "disabled";
            }
            // used + operator instead of array_merge because of int id 
            echo $form->dropDownList($model, 'contract_id', array('0' => "Please Select Contract") + CHtml::listData($contracts, 'id', 'CustomName'), $htmlOption);
        }
        echo $form->error($model, 'contract_id', array('class' => 'text-red'));
        ?>
        </div>
    </div>
    <?php if(isset($return_url)){ ?>
    <input type="hidden" name="return_url" value="<?php echo $return_url; ?>">
    <?php } ?>
    <div class="form-group">
        <label class="control-label col-md-3"><?php echo CHtml::label("Timesheet Type:", 'type'); ?></label>
        <div class="col-md-8">
        <?php
        if ($selectedType != ''){?>
            <label>Custom</label>
        <?php }else
            echo CHtml::dropDownList('type', $selectedType, array_merge(array('0' => "Please Select Type"), $tSheetTypes), array('class' => 'form-control', 'required' => 'required'));
        ?>
        </div>

        <?php echo $form->error($model, 'type', array('class' => 'text-red')); ?></div>
  

    <div class="form-group">
        <label class="control-label col-md-3">Start Date & End Date *</label>
        <div class="col-md-8">
            <div class="input-group">
            <?php echo $form->textField($model, 'start_time', array('class' => 'form-control pull-left dRngPicker disableEle', 'placeholder' => 'Click To Select the Dates', 'required' => 'required' , 'style'=>'cursor:context-menu')); ?>
            <div class="input-group-btn">
                <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
            </div>
        </div>
        </div> 
    </div>
    
    <?php $this->renderPartial('_payratesHours', array('model' => $model, 'payrateSettings' => $payrateSettings,'itemSettings' => isset($itemSettings) ? $itemSettings : array(), 'timelist' => $timelist,'selectedType'=>$selectedType)); ?>

    
    <div class="form-group">
        <label class="control-label col-md-3"><?php echo $form->labelEx($model, 'expense'); ?></label>
        <div class="col-md-8">
            <div class="input-group">
            <?php echo $form->textField($model, 'expense', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control', 'placeholder' => '0')); ?>
            <div class="input-group-btn">
                <button class="btn default" type="button"><i class="fa fa-gbp"></i></button>
            </div>
            </div>
            <?php echo $form->error($model, 'expense', array('class' => 'text-red')); ?>
        </div> 

        <?php echo $form->error($model, 'expense', array('class' => 'text-red')); ?>
    </div>


</div>

<?php $this->renderPartial('_payratesFields', array('model' => $model)); ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('#timesheet-form').h5Validate({errorClass: 'errorClass'});
    });
    var config = {
        '.chosen-select': {},
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>

<?php $this->endWidget(); ?>
