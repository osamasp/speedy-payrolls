<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$regSum = 0;
$addSum = 0;
$expSum = 0;
$break_hours = 0;
$total_hours = 0;
$duration = 0;
$last_element = end($inChild["dataSet"]);
$expence = 0;
?>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Timecard
        </div>

    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <div class="horizontal-form">
            <div class="form-body" style="font-size:15px;">

                <div class="portlet-body form">

                    <div class="alert alert-info">
                        <div class="row">
                            <div class="col-md-4">
                                <p><b>Name:</b> <?php echo $detail["employeeName"]; ?></p>
                                <p><b>Address:</b> <?php echo $detail["companyAddress"]; ?></p>
                                <p><b>Company Name:</b> <?php echo ucfirst($detail["company"]); ?></p>
                                <p><b>Period:</b> <?php
                                        echo AppTimeSheet::formatSheetPeriod($detail["entry"]);
                                        $duration = $detail["entry"]->start_time;
                                        ?></p>
                            </div>
                            <div class="col-md-4">
                                <p><b>Staff ID:</b> <?php // echo $detail["staff_id"];      ?></p>
                                <p><b>Registration Number:</b> <?php echo $detail["reg_number"]; ?></p>
                                <p><b>Contract:</b><?php echo AppContract::getContractCustomName($parent->contract); ?></p>
                                <p><b>Approver Email:</b> <?php echo $detail["approverEmail"]; ?></p>
                            </div>
                            <div class="col-md-4">
                                <p><b>NI Number:</b> <?php echo $detail["ni_number"]; ?></p>
                                <p><b>Approved By:</b> <?php echo $detail["approver"]; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <table class="table table-responsive table-bordered table-hover table-striped" aria-describedby="example2_info" id="datatable<?php echo $parent->id;?>">
            <!--<thead class="row alert" style="font-weight:bold;margin-bottom:0;margin-right:0;margin-left:0;">-->
                        <thead>
                            <tr>        
                                <th rowspan="1" colspan="1">Day</th>
                                <th rowspan="1" colspan="1">Date</th>
                                <th rowspan="1" colspan="1">Start</th>
                                <th rowspan="1" colspan="1">Finish</th>
                                <th rowspan="1" colspan="1">Breaks</th>
                                <th rowspan="1" colspan="1">Work</th>
                                <th rowspan="1" colspan="1">Total</th>
                                <th rowspan="1" colspan="1">Extra Charges</th>
                                <th rowspan="1" colspan="1">Notes</th>  
                                <th rowspan="1" colspan="1">Action</th>
                            </tr>
                        </thead>
                        <?php
                        $l_monday = AppInterface::last_monday($duration);
                        if (date('d/m/Y', $parent->start_time) != date(AppInterface::getdateformat(), $l_monday)) {
                            $duration = $l_monday;
                            for ($i = 0; $i < 7; $i++) {
                                $criteria = new CDbCriteria();
//                                $criteria->addBetweenCondition('created_at', $duration, $duration+86399);
                                $criteria->condition = 'contract_id=' . $parent->contract_id . ' AND start_time >= ' . $duration . ' AND end_time <= ' . ($duration + 86400);
                                $timesheet = Timesheet::model()->find($criteria);
                                if (date('d/m/Y', $parent->start_time) == date(AppInterface::getdateformat(), $duration)) {
                                    break;
                                }
                                ?>
                                <tr style="margin-bottom:0;margin-left:0;margin-right:0;"> 
                                    <td colspan="1"><?php echo date('l', $duration); ?></td>
                                    <td colspan="1"><?php echo date('d/m/Y', $duration); ?></td>
                                    <?php if ($timesheet != null) { ?>
                                        <td colspan="1"><?php echo date('h:i A', $timesheet->start_time); ?></td>
                                        <td colspan="1"><?php
                                            echo date('h:i A', $timesheet->end_time);
                                            $_breaks = UserLunch::model()->findAllByAttributes(array("user_timing_id" => $timesheet->id));
                                            $_notes = TimesheetNotes::model()->findAllByAttributes(array("user_timing_id" => $timesheet->id));
                                            ?></td>
                                        <td colspan="1"><?php
                                            if (count($_breaks) > 0) {
                                                foreach ($_breaks as $break) {
                                                    if ($break->user_timing_id == $entry->id) {
                                                        $break_hours += ($break->lunch_out - $break->lunch_in);
                                                        ?>                       
                                                        <?php
                                                    }
                                                } echo date('h', $break_hours) . " <b> H</b>" . date('i', $break_hours) . " <b>m</b>";
                                            } else {
                                                ?>
                                                -
                                            <?php } ?>
                                        </td>
                                        <?php
                                        if (count($timesheet->payrates) > 0) {
                                            foreach ($timesheet->payrates as $p) {
                                                if ($p->is_item == 0) {
                                                    ?>
                                                    <td colspan="1"><?php echo number_format($p->hours) . " H"; ?></td>
                                                    <td colspan="1"><?php echo ($p->hours - $break_hours) . " H"; ?></td>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <td colspan="1">-</td>
                                                    <td colspan="1">-</td>
                                                    <?php
                                                }
                                            }
                                        } else {
                                            ?>
                                            <td colspan="1">-</td>
                                            <td colspan="1">-</td>   
                                            <?php
                                        }
                                        foreach ($timesheet->payrates as $key => $p) {
                                            if ($p->is_item == 0) {
//                            if (!isset($payhourstotal[$key])) {
//                                $payhourstotal[$key] = 0;
//                            }
//                            $payhourstotal[$key] += $p->hours;
                                            } else {
                                                $expence += $p->value * $p->item_value;
                                            }
                                        }
                                        $expence += $timesheet->expense;
                                        ?>
                                        <td colspan="1">&pound;<?php echo number_format($expence); ?></td>
                                        <td colspan="1"><?php
                                            if (count($_notes) > 0) {
                                                foreach ($_notes as $note) {
                                                    if ($note->user_timing_id == $entry->id) {
                                                        echo $note->note;
                                                        ?>                       
                                                        <?php
                                                    }
                                                }
                                            } else {
                                                ?>
                                                -
                                            <?php } ?>
                                        </td>
                                        <td colspan="1"><?php if (( $timesheet->type == "daily" && $timesheet->parent0->status == "pending" ) || AppUser::isUserAdmin()) { ?><a class="btn btn-danger" href="<?php echo Yii::app()->createUrl("timesheet/main/edit", array("entry" => $timesheet->id)); ?>" >Edit</a><?php } ?></td>
                                    <?php } else { ?>
                                        <td colspan="1"></td>
                                        <td colspan="1"></td>
                                        <td colspan="1"></td>
                                        <td colspan="1"></td>
                                        <td colspan="1"></td>
                                        <td colspan="1"></td>
                                        <td colspan="1"></td>
                                        <td colspan="1"></td>
                                    <?php } ?>
                                </tr>
                                <?php
                                $duration += 86400;
                            }
                        }
                        ?>
                        <?php if (count($inChild['dataSet']) > 0) { ?>

                            <?php
                            $payhourstotal = array();
                            foreach ($inChild['dataSet'] as $entry) {
                                $regularTime = AppTimeSheet::calHrsDiff($entry->start_time, $entry->end_time);
                                $regSum += $regularTime;
                                $addSum += $entry->overtime;
                                $expence = 0;

                                foreach ($entry->payrates as $key => $p) {
                                    if ($p->is_item == 0) {
                                        if (!isset($payhourstotal[$key])) {
                                            $payhourstotal[$key] = 0;
                                        }
                                        $payhourstotal[$key] += $p->hours;
                                    } else {
                                        $expence += $p->value * $p->item_value;
                                    }
                                }
                                $expence += $entry->expense;
                                $expSum += $expence;
                                ?>
                                <?php
                                if (date('d/m/Y', $entry->start_time) != date(AppInterface::getdateformat(), $duration)) {
                                    for ($i = 0; $i < 7; $i++) {
                                        if (date('d/m/Y', $entry->start_time) == date(AppInterface::getdateformat(), $duration)) {
                                            break;
                                        }
                                        ?>
                                        <tr> 
                                            <td colspan="1"><?php echo date('l', $duration); ?></td>
                                            <td colspan="1"><?php echo date('d/m/Y', $duration); ?></td>
                                            <td colspan="1"></td>
                                            <td colspan="1"></td>
                                            <td colspan="1"></td>
                                            <td colspan="1"></td>
                                            <td colspan="1"></td>
                                            <td colspan="1"></td>
                                            <td colspan="1"></td>
                                            <td colspan="1"></td>
                                        </tr>
                                        <?php
                                        $duration += 86400;
                                    }
                                }
                                ?>

                                <tr>
                                    <?php if (date('d/m/Y', $entry->start_time) == date(AppInterface::getdateformat(), $duration)) { ?>
                                        <td><?php echo date('l', $entry->start_time); ?></td>
                                        <td><?php echo date('d/m/Y', $entry->start_time); ?></td>
                                        <td><?php echo date('h:i A', $entry->start_time); ?></td>
                                        <td><?php echo date('h:i A', $entry->end_time); ?></td>
                                        <td><?php
                                            if (count($breaks) > 0) {
                                                foreach ($breaks as $break) {
                                                    if ($break->user_timing_id == $entry->id) {
                                                        $break_hours += ($break->lunch_out - $break->lunch_in);
                                                        ?>                       
                                                        <?php
                                                    }
                                                } echo date('h', $break_hours) . " <b> H</b>" . date('i', $break_hours) . " <b>m</b>";
                                            } else {
                                                ?>
                                                -
                                            <?php } ?>
                                        </td>
                                        <?php
                                        if (count($entry->payrates) > 0) {
                                            foreach ($entry->payrates as $p) {
                                                if ($p->is_item == 0) {
                                                    ?>
                                                    <td><?php echo number_format($p->hours) . " H"; ?></td>
                                                    <td><?php echo ($p->hours - $break_hours) . " H"; ?></td>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <?php
                                                }
                                            }
                                        } else {
                                            ?>
                                            <td>-</td>
                                            <td>-</td>   
                                        <?php } ?>
                                        <td>&pound;<?php echo number_format($expence); ?></td>
                                        <td><?php
                                            if (count($notes) > 0) {
                                                foreach ($notes as $note) {
                                                    if ($note->user_timing_id == $entry->id) {
                                                        echo $note->note;
                                                        ?>                       
                                                        <?php
                                                    }
                                                }
                                            } else {
                                                ?>
                                                -
                                            <?php } ?>
                                        </td>
                                        <td><?php if (( $entry->type == "daily" && $entry->parent0->status == "pending" ) || AppUser::isUserAdmin()) { ?><a class="btn btn-danger" href="<?php echo Yii::app()->createUrl("timesheet/main/edit", array("entry" => $entry->id)); ?>" >Edit</a><?php } ?></td>
                                    <?php } else { ?>
                                        <td colspan="1"><?php echo date('l', $duration); ?></td>
                                        <td colspan="1"><?php echo date('d/m/Y', $duration); ?></td>
                                        <td colspan="1"></td>
                                        <td colspan="1"></td>
                                        <td colspan="1"></td>
                                        <td colspan="1"></td>
                                        <td colspan="1"></td>
                                        <td colspan="1"></td>
                                        <td colspan="1"></td>
                                        <td colspan="1"></td>
                                    <?php } ?>
                                </tr> 
                                <?php
                                if ($entry == $last_element && date('d/m/Y', $entry->start_time) == date(AppInterface::getdateformat(), $duration) && date('d/m/Y', $detail["entry"]->end_time) != date(AppInterface::getdateformat(), $duration)) {
                                    $duration += 86400;
                                    ?>
                                    <?php for ($i = 0; $i < 7; $i++) { ?>
                                        <tr> 
                                            <td colspan="1"><?php echo date('l', $duration); ?></td>
                                            <td colspan="1"><?php echo date('d/m/Y', $duration); ?></td>
                                            <td colspan="1"></td>
                                            <td colspan="1"></td>
                                            <td colspan="1"></td>
                                            <td colspan="1"></td>
                                            <td colspan="1"></td>
                                            <td colspan="1"></td>
                                            <td colspan="1"></td>
                                            <td colspan="1"></td>
                                        </tr>
                                        <?php
                                        if (date('d/m/Y', $detail["entry"]->end_time) == date(AppInterface::getdateformat(), $duration)) {
                                            break;
                                        }
                                        $duration += 86400;
                                    }
                                }
                                $duration += 86400;
                                ?>
                            <?php } ?>

                        <?php } ?>   
                                        <tfoot><tr style="border:none; text-align:left;">
<!--                            <td colspan="1"></td>
                            <td colspan="1"></td>
                            <td colspan="1"></td>
                            <td colspan="1"></td>
                            <td colspan="1"></td>-->
                            <th colspan="10">Total : <?php foreach ($payhourstotal as $pt) { 
                                echo number_format($pt) . " H"; } ?></th>
                            
<!--                            <td colspan="1"></td>
                            <td colspan="1"></td>
                            <td colspan="1"></td>-->
                        </tr></tfoot>
                    </table>

                </div>

                <?php
                if (count($merChild['dataSet']) > 0) {
                    $regSum = 0;
                    $addSum = 0;
                    $expSum = 0;
                    $ch1 = $merChild['dataSet'][0];
                    $oldParent = $ch1->oldParent;
                    ?>
                    <div class="box-header">
                        <h3 class="box-title">Time Card Of <?php echo $oldParent->contract->company->name . ' ' . strtoupper($oldParent->contract->type); ?> , <?php
                            echo AppTimeSheet::formatSheetPeriod($oldparent);
                            ?></h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-responsive table-hover table-bordered table-striped" aria-describedby="example2_info" id="datatable<?php echo $parent->id;?>">
                            <thead>
                                <tr>
                                    <th colspan="1" rowspan="1">Day</th>
                                    <th colspan="1" rowspan="1">Date</th>
                                    <th colspan="1" rowspan="1">Start</th>
                                    <th colspan="1" rowspan="1">Finish</th>
                                    <th colspan="1" rowspan="1">Breaks</th>
                                    <th colspan="1" rowspan="1">Work</th>
                                    <th colspan="1" rowspan="1">Total</th>
                                    <th colspan="1" rowspan="1">Extra Charges</th>
                                    <th colspan="1" rowspan="1">Notes</th>  
                                    <th colspan="1" rowspan="1">Action</th>
                                </tr>
                            </thead>
                            <?php
                            $duration = $detail["entry"]->start_time;
                            foreach ($merChild['dataSet'] as $entry) {
                                $regularTime = AppTimeSheet::calHrsDiff($entry->start_time, $entry->end_time);
                                $regSum += $regularTime;
                                $addSum += $entry->overtime;
                                $expSum += $entry->expense;
                                ?>
                                <?php
                                if (date('d/m/Y', $entry->start_time) != date(AppInterface::getdateformat(), $duration)) {
                                    for ($i = 0; $i < 7; $i++) {
                                        if (date('d/m/Y', $entry->start_time) == date(AppInterface::getdateformat(), $duration)) {
                                            break;
                                        }
                                        ?>
                                        <tr> 
                                            <td colspan="1"><?php echo date('l', $duration); ?></td>
                                            <td colspan="1"><?php echo date('d/m/Y', $duration); ?></td>
                                            <td colspan="1"></td>
                                            <td colspan="1"></td>
                                            <td colspan="1"></td>
                                            <td colspan="1"></td>
                                            <td colspan="1"></td>
                                            <td colspan="1"></td>
                                            <td colspan="1"></td>
                                            <td colspan="1"></td>
                                        </tr>
                                        <?php
                                        $duration += 86400;
                                    }
                                }
                                ?>
                                <tr>
                                    <?php if (date('d/m/Y', $entry->start_time) == date(AppInterface::getdateformat(), $duration)) { ?>
                                        <td colspan="1"><?php echo date('l', $entry->start_time); ?></td>
                                        <td colspan="1"><?php echo date('d/m/Y', $entry->start_time); ?></td>
                                        <td colspan="1"><?php echo date('h:i A', $entry->start_time); ?></td>
                                        <td colspan="1"><?php echo date('h:i A', $entry->end_time); ?></td>
                                        <td colspan="1"><?php
                                            if (count($breaks) > 0) {
                                                foreach ($breaks as $break) {
                                                    if ($break->user_timing_id == $entry->id) {
                                                        $break_hours += ($break->lunch_out - $break->lunch_in);
                                                        ?>                       
                                                        <?php
                                                    }
                                                } echo date('h', $break_hours) . " <b> H</b>" . date('i', $break_hours) . " <b>m</b>";
                                            } else {
                                                ?>
                                                -
                                            <?php } ?>
                                        </td>
                                        <?php
                                        if (count($entry->payrates) > 0) {
                                            foreach ($entry->payrates as $p) {
                                                if ($p->is_item == 0) {
                                                    ?>
                                                    <td colspan="1"><?php echo number_format($p->hours) . " H"; ?></td>
                                                    <td colspan="1"><?php echo ($p->hours - $break_hours) . " H"; ?></td>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <td colspan="1">-</td>
                                                    <td colspan="1">-</td>
                                                    <?php
                                                }
                                            }
                                        } else {
                                            ?>
                                            <td colspan="1">-</td>
                                            <td colspan="1">-</td>   
                                        <?php } ?>
                                        <td colspan="1">&pound;<?php echo number_format($expence); ?></td>
                                        <td colspan="1"><?php
                                            if (count($notes) > 0) {
                                                foreach ($notes as $note) {
                                                    if ($note->user_timing_id == $entry->id) {
                                                        echo $note->note;
                                                        ?>                       
                                                        <?php
                                                    }
                                                }
                                            } else {
                                                ?>
                                                -
                                            <?php } ?>
                                        </td>
                                        <td colspan="1"><?php if (( $entry->type == "daily" && $entry->parent0->status == "pending" ) || AppUser::isUserAdmin()) { ?><a class="btn btn-danger" href="<?php echo Yii::app()->createUrl("timesheet/main/edit", array("entry" => $entry->id)); ?>" >Edit3</a><?php } ?></td>
                                    <?php } else { ?>
                                        <td colspan="1"><?php echo date('l', $duration); ?></td>
                                        <td colspan="1"><?php echo date('d/m/Y', $duration); ?></td>
                                        <td colspan="1"></td>
                                    <?php } $duration += 86400; ?>
                                </tr>
                                <?php if ($entry == $last_element) {
                                    ?>
                                    <?php for ($i = 0; $i < 7; $i++) { ?>
                                        <tr> <td colspan="1"><?php echo date('l', $duration); ?></td>
                                            <td colspan="1"><?php echo date('d/m/Y', $duration); ?></td>
                                            <td colspan="1"></td>
                                            <td colspan="1"></td>
                                            <td colspan="1"></td>
                                            <td colspan="1"></td>
                                            <td colspan="1"></td>
                                            <td colspan="1"></td>
                                            <td colspan="1"></td>
                                            <td colspan="1"></td>
                                            <td colspan="1"></td>
                                        </tr>
                                        <?php
                                        if (date('d/m/Y', $detail["entry"]->end_time) == date(AppInterface::getdateformat(), $duration)) {
                                            break;
                                        }
                                        $duration += 86400;
                                    }
                                }
                                ?>
                            <?php } ?>

                            <tr>
                                <td colspan="1"></td>
                                <td colspan="1"></td>
                                <td colspan="1"></td>
                                <td colspan="1"></td>
                                <td colspan="1">Total</td>
                                <td colspan="1"><?php echo number_format($regSum); ?> Hrs.</td>
                                <td colspan="1"><?php echo number_format($addSum); ?> Hrs.</td>
                                <td colspan="1">&pound;<?php echo number_format($expSum); ?></td>
                                <td colspan="1"></td>
                                <td colspan="1"></td>
                            </tr>

                        </table>

                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>