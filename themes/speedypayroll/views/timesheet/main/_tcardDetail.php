<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="row">
    <div class="col-md-6">
        <p>Name: <?php echo $employeeName; ?></p>
        <p>Staff ID: <?php echo $employeeId; ?></p>
        <p>Company: <?php echo ucfirst($company); ?></p>
        <p>Location: <?php echo $companyCity . ',' . $companyCountry; ?></p>
        <br>
        <!-- Your first column here -->
    </div>
    <div class="col-md-6">
        <p>Date Submitted: <?php echo date(AppInterface::getdateformat(), $date); ?></p>
        <p></p>
        <p>PaySlip Period: <?php echo AppTimeSheet::formatSheetPeriod($entry); ?></p>
        <p>Employment Type: <?php echo ucfirst($employeeType); ?></p>
        <!-- Your second column here -->
    </div>
    <div class="clearfix"></div>
    <div class="col-md-6">
        <?php if (isset($payRates2) && isset($itemRates2)) { ?>
            <h3>Cost</h3>
        <?php } ?>
        <?php if (isset($payHours) && count($payHours) > 0) { ?>
            <?php foreach ($payHours as $key => $payRate) { ?>
                <p><?php echo ucfirst($key) . ' hours: ' . $payRate . ' x £' . ($payRates ? $payRates[$key] : ''); ?> </p>
            <?php } ?>
        <?php } ?>
        <br>
        <?php if (isset($itemCount) && count($itemCount) > 0) { ?>
            <?php foreach ($itemCount as $key => $item) { ?>
                <p><?php echo ucfirst($key) . ': ' . $item . ' x £' . ($itemRates ? $itemRates[$key] : ''); ?> </p>
            <?php } ?>
        <?php } ?>
        <br>
        <p>Expense: <?php echo '£' . $expense; ?></p>
    </div>
    <?php if (isset($payRates2) && isset($itemRates2)) { ?>
        <div class="col-md-6">
            <h3>Bill</h3>
            <?php if (isset($payHours) && count($payHours) > 0) { ?>
                <?php foreach ($payHours as $key => $payRate) { ?>
                    <p><?php echo ucfirst($key) . ' hours: ' . $payRate . ' x £' . ($payRates2 ? $payRates2[$key] : ''); ?> </p>
                <?php } ?>
            <?php } ?>
            <br>
            <?php if (isset($itemCount) && count($itemCount) > 0) { ?>
                <?php foreach ($itemCount as $key => $item) { ?>
                    <p><?php echo ucfirst($key) . ': ' . $item . ' x £' . ($itemRates2 ? $itemRates2[$key] : ''); ?> </p>
                <?php } ?>
            <?php } ?>
        </div>
    <?php } ?>
</div>    

