<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="exampleModalLabel">Timesheet Details</h4>
</div>
<div class="modal-body" id="popup">
    <?php $this->renderPartial('_tcardDetail', $tdetails); ?>
    <?php if ($tdetails['canSendInvoice']) { ?>
        <?php $this->renderPartial('_sendinvoice', array('timesheetId' => $tdetails['timesheetId'])); ?>
    <?php } ?>
    <div class="clearfix"></div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-primary" onclick="javascript:printDiv('popup');" >Print</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

</div>
