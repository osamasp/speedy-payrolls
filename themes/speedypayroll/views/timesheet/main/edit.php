<?php
/* @var $this MainController */
/* @var $model Timesheet */

$this->breadcrumbs = array(
    'Timesheets' => array('index'),
    'Edit',
);
$this->menu = array(
    array('label' => 'List Contract', 'url' => array('index')),
    array('label' => 'Manage Contract', 'url' => array('admin')),
);
?>
<input type="hidden" id="id" value="<?php echo $model->id; ?>">
<input type="hidden" id="contract_id" name="contract_id" value="<?php echo $model->contract_id; ?>">
<input type="hidden" id="url" name="url" value="<?php echo $_SERVER['REDIRECT_URL']; ?>">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/thirdparty/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/thirdparty/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Edit Timesheet
        </div>

    </div>
    <div class="portlet-body form">
        <div class="horizontal-form">
            <div class="form-body">

                <div class="portlet-body form">
                    <div class="box-header">
                        <h3 class="box-title">Edit Timesheet</h3>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <?php
                            switch ($selectedType) {

                                case 'daily':
                                    $timelist = array();
                                    for ($i = 0; $i <= 24; $i += 0.5)
                                        $timelist["$i"] = $i . " hours";

                                    $this->renderPartial('_dailyentry', array('model' => $model, 'timelist' => $timelist, 'contracts' => $contracts, 'tSheetTypes' => $tSheetTypes, 'selectedType' => $selectedType, 'tempStartTime' => $tempStartTime, 'payrateSettings' => $payrateSettings, 'return_url' => $return_url));
                                    break;
                                default:
                                    $timelist = array();
                                    if ($selectedType == 'weekly') {
                                        for ($i = 0; $i <= 167; $i += 0.5)
                                            $timelist["$i"] = $i . " hours";
                                    } else {
                                        for ($i = 0; $i <= 743; $i += 0.50)
                                            $timelist["$i"] = $i . " hours";
                                    }
                                    $this->renderPartial('_customentry', array('model' => $model, 'timelist' => $timelist, 'contracts' => $contracts, 'tSheetTypes' => $tSheetTypes, 'selectedType' => $selectedType, 'payrateSettings' => $payrateSettings, 'return_url' => $return_url));
                                    break;
                            }
                            ?>
                        </div>
                        <div class="col-md-4">
                            <div class="todo-content">
                                <div class="portlet light">
                                    <?php // echo $this->renderPartial('application.themes.speedypayroll.views.company.companyshifts.notes', array('notes' => $notes, 'canAddNote' => false)); ?>
                                    <?php echo $this->renderPartial('//company/companyShifts/notes', array('notes' => $notes, 'canAddNote' => true)); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-8">
                                <a href="javascript:void(0);" onclick="submitForm()" class="btn btn-circle green-haze" style="float:right">SAVE</a>
                                <?php // echo CHtml::submitButton('Save', array('class' => 'btn btn-circle green-haze', 'style' => 'float:right;')); ?>
                            </div>
                            <div class="col-md-1">
                                <a href="<?php echo Yii::app()->request->urlReferrer; ?>" class="btn btn-circle btn-sm btn-default "  style="padding-left: 10px;">Cancel</a>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#note').on('click', function () {
//                                           alert("2");
            var recipient = $("#id").val(); // Extract info from data-* attributes
            var modal = $("#portlet-config");
            $("#portlet-config").modal('toggle');
            var url = '<?php echo Yii::app()->baseUrl; ?>';
            url = url.concat('/company/companyShifts/addnote');
            $.ajax({
                type: "GET",
                url: url,
                data: {'id': recipient, 'contract_id': $("#contract").val()},
                beforeSend: function (jqXHR, settings) {
                    jQuery(modal).find('.modal-title').html('<h2 style="font-family:\'Avenir Next\',\'Avenir Next LT W01 SC\',\'Helvetica Neue\', Helvetica, Arial, sans-serif;font-weight: bold;font-size:24px;color:#7b8cb8;">ADD NOTE</h2>')
                    jQuery(modal).find('.modal-body').text("Loading..........");

                },
                success: function (response) {
                    if (response.status == true) {
                        jQuery(modal).find('.modal-body').html(response.data);
                    } else {
                        jQuery(modal).modal('hide');
                        alert(response.reason);

                    }
                },
                dataType: 'json'
            });
        $("#saveModel").on('click', function () {
            $("#redirect_url").val($("#url").val());
            $("#contract").val($("#contract_id").val());
            $("#add-note-form").submit();
        });
        }); 
    });
    function submitForm(){
        $("#timesheet-form").submit();
    }
</script>