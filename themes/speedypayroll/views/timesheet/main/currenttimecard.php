<?php
/* @var $this MainController */
/* @var $entry Timesheet */
//dd($entries[0]->id);
$this->breadcrumbs = array(
    'Timesheets' => array('index'),
);
?>
<?php
/* @var $this MainController */
/* @var $entry Timesheet */
$id = 0;
$this->breadcrumbs = array(
    'Timesheets' => array('index'),
);
?>
<form id="entryForm" method="post" >
    <!-- BEGIN CONTENT -->
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Modal title</h4>
                </div>
                <div class="modal-body">
                    Widget settings form goes here
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn blue">Save changes</button>
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE HEAD -->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <!--            <div class="page-title">
        <?php // if($ids==1 && $current == 1){?>
                        <h1>Current - 2 Way Timecard</h1>
        <?php // } else if($ids==1 && $current==0){?>
                        <h1>Archive - 2 Way Timecard</h1>
        <?php // } else if($ids==2 && $current==1){?>
                        <h1>Current - 3 Way Timecard</h1>
        <?php // } else if($ids==2 && $current==0){?>
                        <h1>Archive - 3 Way Timecard</h1>
        <?php // } else if($ids==3){ ?>
                        <h1>Timesheet Approval Archive</h1>
        <?php // } else if(isset($contract)){ ?>
                        <h1>Timesheets</h1>
        <?php // } else{ ?>
                        <h1>Non Billable Timesheet</h1>
        <?php // } ?>
                    </div>-->
        <!-- END PAGE TITLE -->

    </div>
    <!-- END PAGE HEAD -->

    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->

    <!-- data table starts -->
    <!--<div class="box-tools">
            <a class="btn btn-default pull-right" href="<?php // echo Yii::app()->createUrl('/timesheet/main/addWizard');  ?>">Add Timesheet</a>
        </div>-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box grey-cascade">
                <div class="portlet-title">
                    <div class="caption">
                        <?php if ($ids == 1 && $current == 1) { ?>
                            <i class="icon-arrow-right"></i>Current - 2 Way Timesheet
                        <?php } else if ($ids == 1 && $current == 0) { ?>
                            <i class="fa fa-check-circle"></i>Archive - 2 Way Timesheet
                        <?php } else if ($ids == 2 && $current == 1) { ?>
                            <i class="icon-arrow-right"></i>Current - 3 Way Timesheet
                        <?php } else if ($ids == 2 && $current == 0) { ?>
                            <i class="fa fa-check-circle"></i>Archive - 3 Way Timesheet
                        <?php } else if ($ids == 3) { ?>
                            <i class="fa fa-globe"></i>Timesheet Approval Archive
                        <?php } else if($ids == 4) { ?>
                            <i class="fa fa-globe"></i>Current Timecard
                        <?php } else { ?>
                            <i class="icon-arrow-right"></i>Non Billable Timesheet
                        <?php } ?>
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>

                    </div>
                </div>
                <div class="portlet-body table-responsive">
                    <div class="table-toolbar">
                        <div class="row">

                            <div class="col-md-12">

                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-bordered table-hover" id="example1">
                        <thead>
                            <tr>
                                <th class="table-checkbox">
                                    <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes"/>
                                </th>
                                <th>
                                    Contract
                                </th>
                                <th>
                                    Employee
                                </th>
                                <th>
                                    Approver
                                </th>
                                <th>
                                    Period
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>
                                    PaySlip
                                </th>
<!--                                    <th>
                                    Action
                                </th>-->

                            </tr>
                        </thead>
                        <?php if (count($entries) > 0) { ?>
                            <tbody role="alert" aria-live="polite" aria-relevant="all">
                                <?php
                                foreach ($entries as $entry) {
                                    $paySlipStatus = AppPayslip::isIssued($entry);
                                    if (AppUser::isUserAdmin()) {
                                        if (isset($entry->contract->parent)) {
                                            if ($entry->contract->type == 'out_b' && $entry->contract->company_id != AppUser::getUserCompany()->id) {
                                                $etype = ucwords(str_replace('_', ' ', $entry->contract->parent->CustomEmploymentType));
                                            }
                                        }
                                    } else {
                                        $etype = $entry->contract->CustomName;
                                    }
                                    ?>
                                    <tr class="odd gradeX" style="cursor: pointer;" onclick="toggler('<?php echo $entry->id; ?>')">
                                        <td><input type="checkbox" class="checkboxes" value="<?php echo $entry->id; ?>"/></td>
                                        <td>
                                            <?php
                                            if (isset($etype) && $etype == '') {
                                                echo AppContract::getContractCustomName($entry->contract);
                                            } else if (isset($etype) && $etype != '') {
                                                echo $etype;
                                            } else {
                                                echo ucwords(str_replace('_', ' ', AppUser::isUserAdmin() ? $entry->contract->CustomEmploymentType : $entry->contract->CustomName));
                                            }

                                            // echo isset($etype) ? $etype : ucwords(str_replace('_', ' ', AppUser::isUserAdmin() ? $entry->contract->CustomEmploymentType : $entry->contract->CustomName)); 
                                            ?>
                                        </td>
                                        <td><?php echo $entry->contract->user->full_name; ?></td>
                                        <td><?php echo $entry->contract->approver->full_name; ?></td>
                                        <td>
                                            <?php echo AppTimeSheet::formatSheetPeriod($entry); ?>
                                        </td>
                                        <td><?php echo $entry->status; ?></td>
                                        <td><?php echo ($paySlipStatus) ? 'issued' : 'N/A'; ?></td>
            <!--                            <td><div class="btn-group">
                                                    <button class="btn green dropdown-toggle" data-toggle="dropdown">Actions <i class="fa fa-angle-down"></i>
                                                    </button>
                                                    <ul class="dropdown-menu pull-right">
                                        <?php //  if ($entry->type == "daily") { ?>
                                                        <li><a href="#childEntryContainer" class="<?php //  echo ($entry->type == "daily") ? "wklyLnkPrnt" : "";  ?>" tSheetId="<?php //  echo $entry->id;  ?>">Show TimeCard</a></li>
                                        <?php //  } ?>
                                                </ul>
                                                </div>
                                        </td>                            -->
                                    </tr>
                                <?php } ?>
                            </tbody>
                        <?php } ?>
                    </table>
                    <?php foreach ($entries as $entry) { ?>
                        <script>
                            $(function () {
    //                               $('#datatable<?php echo $entry->id; ?>').dataTable({
    //            "bPaginate": false,
    //            "bLengthChange": false,
    //            "bFilter": false,
    //            "bSort": false,
    //            "bInfo": false,
    //            "bAutoWidth": false,
    //            responsive: true,
    //        paging:         false,
    //        }); 
                            });
                        </script>
                        <div id="<?php echo $entry->id; ?>" style="display:none;"> 
                            <?php
                            if (isset($timecard[$entry->id])) {
                                echo $timecard[$entry->id];
                            } else {
                                echo "No Timecard Found";
                            }
                            ?>
                        </div>
<?php } ?>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>      <!-- /data table ends -->
    <!-- END CONTENT -->
</form>
<script>
    function toggler(id) {
        $('#' + id).slideToggle("slow", function () {
            if (!$.fn.DataTable.isDataTable('#datatable' + id)) {
                $('#datatable' + id).dataTable({
                    "bPaginate": false,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": false,
                    "bInfo": false,
                    "bAutoWidth": false,
                    responsive: true,
                    paging: false,
                });
            }

            $('html, body').animate({
                scrollTop: $("#" + id).offset().top - $('.page-header').height()
            }, 2000);
        });
    }
</script>