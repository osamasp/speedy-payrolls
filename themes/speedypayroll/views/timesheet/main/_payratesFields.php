<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if ($model != null && $model->payrates != null && $model->isNewRecord == false && AppUser::isUserAdmin()) {
    ?>
    <hr/>

    <div class="box-body">
        <h3 class="heading">Pay Rates</h3>

        <?php
        foreach ($model->custompayrates as $rates) {



            $fieldName = "rates[" . $rates->id . "]";
            $postVal = Yii::app()->getRequest()->getPost($fieldName);
            ?>
            <div class="form-group">
                <label class="control-label col-md-3"><?php echo CHtml::label($rates->contractSetting->key . ' Rates', $rates->id); ?></label>
                <div class="col-md-8">
                    <div class="input-group">
                        <?php echo CHtml::textField($fieldName, ($postVal != null) ? $postVal : $rates->value, array('size' => 60, 'maxlength' => 255, 'class' => 'form-control', 'placeholder' => '0')); ?>
                        <div class="input-group-btn">
                            <button class="btn default" type="button"><i class="fa fa-gbp"></i></button>
                        </div>
                    </div>
                </div> 


            </div>

        <?php } if(count($model->customitems)>0){ ?>
        <h3 class="heading">Items</h3>

        <?php }
        foreach ($model->customitems as $rates) {



            $fieldName = "item_value[" . $rates->id . "]";
            $postVal = Yii::app()->getRequest()->getPost($fieldName);
            ?>
            <div class="form-group">
                <label class="control-label col-md-3"><?php echo CHtml::label($rates->contractSetting->key . ' Rates', $rates->id); ?></label>
                <div class="col-md-8">
                    <div class="input-group">
                        <?php echo CHtml::textField($fieldName, ($postVal != null) ? $postVal : $rates->value, array('size' => 60, 'maxlength' => 255, 'class' => 'form-control', 'placeholder' => '0')); ?>
                        <div class="input-group-btn">
                            <button class="btn default" type="button"><i class="fa fa-gbp"></i></button>
                        </div>
                    </div>

                </div> 


            </div>

        <?php } ?>
    </div>
    <?php
}
?>

