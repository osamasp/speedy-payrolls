<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if ($model != null && $model->payrates != null && $model->isNewRecord == false) {
    foreach ($model->custompayrates as $rates) {

        if ($model->type == "daily" && AppTimeSheet::isRegularPayRates($rates)) {
            
        } else {
            $fieldName = "hours[" . $rates->id . "]";
            $postVal = Yii::app()->getRequest()->getPost($fieldName);
            ?>
            <div class='form-group'>
                <label class="control-label col-md-3"><?php echo CHtml::label(ucwords($rates->contractSetting->key), $rates->id); ?></label>
                <div class="col-md-8">

                    <div class="input-group">

                        <?php echo CHtml::dropDownList($fieldName, ($postVal != null) ? $postVal : round($rates->hours, 1), $timelist, array('class' => 'form-control chosen-select', 'placeholder' => 'Minutes/Hours')); ?>  
                        <div class="input-group-btn">
                            <button class="btn default" type="button"><i class="fa fa-clock-o"></i></button>
                        </div>
                    </div>
                </div>
            </div>

        <?php }
    } ?>
    <h3 style="padding-left: 20px;">Items</h3>
    <?php
    foreach ($model->customitems as $rates) {
        $fieldName = "items[" . $rates->id . "]";
        $postVal = Yii::app()->getRequest()->getPost($fieldName);
        ?>
        <div class='form-group'>
            <label class="control-label col-md-3"><?php echo CHtml::label(ucwords($rates->contractSetting->key), $rates->id); ?></label>
            <div class="col-md-8">

                <div class="input-group">

        <?php echo CHtml::textField($fieldName, ($postVal != null) ? $postVal : round($rates->item_value, 1), array('class' => 'form-control', 'placeholder' => 'Minutes/Hours')); ?>  
                    <div class="input-group-btn">
                        <button class="btn default" type="button"><i class="fa fa-list"></i></button>
                    </div>
                </div>
            </div>
        </div>

        <?php
    }
} else {


    foreach ($payrateSettings as $cSetting) {

        if ($selectedType == "daily" && AppTimeSheet::isRegularPayRates($cSetting))
            continue;

        $fieldName = "hours[" . $cSetting->id . "]";
        $postVal = Yii::app()->getRequest()->getPost($fieldName);
        ?>
        <div class='form-group'>
            <label class="control-label col-md-3"><?php echo CHtml::label(ucwords($cSetting->key), $cSetting->id); ?></label>
            <div class="col-md-8">
                <div class="input-group">

                    <?php echo CHtml::dropDownList($fieldName, ($postVal != null) ? $postVal : 0, $timelist, array('class' => 'form-control chosen-select', 'placeholder' => 'Minutes/Hours'));
                    ?>
                    <div class="input-group-btn">
                        <button class="btn default" type="button"><i class="fa fa-clock-o"></i></button>
                    </div>
                </div> 

            </div>
        </div>

        <?php
    }
    ?>
    <?php if (count($itemSettings) > 0) { ?>
        <h3 style="padding-left: 20px;">Items</h3>
    <?php
    }
    foreach ($itemSettings as $cSetting) {

        if ($selectedType == "daily" && AppTimeSheet::isRegularPayRates($cSetting))
            continue;

        $fieldName = "items[" . $cSetting->id . "]";
        $postVal = Yii::app()->getRequest()->getPost($fieldName);
        ?>

        <?php
    }
}
?>