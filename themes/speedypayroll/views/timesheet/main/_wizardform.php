<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/timesheet-main-wizardform.js" type="text/javascript"></script>

<form method="post" action="" class="form-horizontal form-body">
    <div class="box-body">
        <div class="form-group">
            <?php
            if ($contracts != null && count($contracts) > 0) { ?>
                <label class="control-label col-md-3">Select Contract :</label>
                <div class="col-md-4">
                <?php
                // used + operator instead of array_merge because of int id 
                echo CHtml::dropDownList('contract', Yii::app()->getRequest()->getPost('contract'), CHtml::listData($contracts, 'id', 'CustomName'), array('class' => 'contractDD form-control'));
                ?> </div>

            <?php }
            ?>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">Timesheet Type</label>
            <div class="col-md-4">
            <?php echo CHtml::dropDownList('type', Yii::app()->getRequest()->getPost('type'), array_merge(array('0' => "Please Select Type"), $tSheetType), array('class' => 'tsheetDD form-control')); ?>
            </div>

        </div>
<div class="form-actions right">
        <div class="row">
            <div class="col-md-offset-3 col-md-9">
                <?php echo CHtml::submitButton('Add', array('class' => 'btn btn-circle green-haze')); ?>
            </div>
        </div>
    </div>
    </div>
    
</form>