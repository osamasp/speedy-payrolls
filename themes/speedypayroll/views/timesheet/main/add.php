<?php
/* @var $this MainController */
/* @var $model Timesheet */

$this->breadcrumbs = array(
    'Timesheets' => array('index'),
    'Add - Step 1' => array('addWizard'),
    'Add',
);

$this->menu = array(
    array('label' => 'List Contract', 'url' => array('index')),
    array('label' => 'Manage Contract', 'url' => array('admin')),
);
?>
<input type="hidden" id="url" value="">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/thirdparty/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/thirdparty/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i>Add New Daily Timesheet
        </div>
    </div>
    <div class="portlet-body form">
        <div class="row">
            <div class="col-md-8">
                <?php
                if ($contracts == null || count($contracts) == 0) {
                    die();
                }

                switch ($selectedType) {

                    case 'daily':
                        $timelist = array();
                        for ($i = 0; $i <= 24; $i += 0.5)
                            $timelist["$i"] = $i . " hours";



                        $this->renderPartial('_dailyentry', array('model' => $model, 'timelist' => $timelist, 'contracts' => $contracts, 'tSheetTypes' => $tSheetTypes, 'selectedType' => $selectedType, 'payrateSettings' => $payrateSettings, 'itemSettings' => $itemSettings,));

                        break;

                    default:
                        $timelist = array();
                        if ($selectedType == 'weekly') {
                            for ($i = 0; $i <= 167; $i += 0.5)
                                $timelist["$i"] = $i . " hours";
                        } else {
                            for ($i = 0; $i <= 743; $i += 0.5)
                                $timelist["$i"] = $i . " hours";
                        }
                        $this->renderPartial('_customentry', array('model' => $model, 'contracts' => $contracts, 'timelist' => $timelist, 'tSheetTypes' => $tSheetTypes, 'selectedType' => $selectedType, 'payrateSettings' => $payrateSettings, 'itemSettings' => $itemSettings,));
                        break;
                }
                ?>
            </div>
            <div class="col-md-4">
                <div class="todo-content">
                    <div class="portlet light">
                        <?php // echo $this->renderPartial('application.themes.speedypayroll.views.company.companyshifts.notes', array('notes' => $notes, 'canAddNote' => false)); ?>
                        <?php echo $this->renderPartial('//company/companyShifts/notes', array('notes' => $notes, 'canAddNote' => false)); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-8">
                    <a href="javascript:void(0);" onclick="submitForm()" class="btn btn-circle green-haze" style="float:right">SAVE</a>
                    <?php // echo CHtml::submitButton('Save', array('class' => 'btn btn-circle green-haze', 'style' => 'float:right;')); ?>
                </div>
                <div class="col-md-1">
                    <a href="<?php echo Yii::app()->request->urlReferrer; ?>" class="btn btn-circle btn-sm btn-default "  style="padding-left: 10px;">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function submitForm(){
        if($("#timesheet-form").h5Validate('allValid')){
            $.ajax({
            type: 'POST',
            url: $("#timesheet-form").attr('action'),
            data: $("#timesheet-form").serialize(),
            dataType: 'json',
            success: function(json) {
                if(json.status == true) {
                    $("#url").val(json.data);
                    if(confirm('Timesheet successfully added.\nAre you going to add notes?')){
                        submitNoteForm(json.id, json.contract_id);
                    } else{
                        window.location = json.data;
                    }
                } else{
                    window.location.reload();
                }
            }
        });
        } else{
            alert("* Please Select Start & End Date");
            $("#Timesheet_start_time").focus();
        }
    }
    
    function submitNoteForm(id, contract_id){
         var recipient = id; // Extract info from data-* attributes
            var modal = $("#portlet-config");
            $("#portlet-config").modal('toggle');
            var url = '<?php echo Yii::app()->baseUrl; ?>';
            url = url.concat('/company/companyShifts/addnote');
            $.ajax({
                type: "GET",
                url: url,
                data: {'id': recipient, 'contract_id': contract_id},
                beforeSend: function (jqXHR, settings) {
                    jQuery(modal).find('.modal-title').html('<h2 style="font-family:\'Avenir Next\',\'Avenir Next LT W01 SC\',\'Helvetica Neue\', Helvetica, Arial, sans-serif;font-weight: bold;font-size:24px;color:#7b8cb8;">ADD NOTE</h2>')
                    jQuery(modal).find('.modal-body').text("Loading..........");

                },
                success: function (response) {
                    if (response.status == true) {
                        jQuery(modal).find('.modal-body').html(response.data);
                    } else {
                        jQuery(modal).modal('hide');
                        alert(response.reason);

                    }
                },
                dataType: 'json'
            });
        $("#saveModel").on('click', function () {
            $("#redirect_url").val($("#url").val());
            $("#add-note-form").submit();
        });
    }
</script>