<?php
/* @var $this MainController */
/* @var $entry Timesheet */

$this->breadcrumbs = array(
    'Timesheets' => array('index'),
);
?>
<?php
/* @var $this MainController */
/* @var $entry Timesheet */
$id = 0;
$this->breadcrumbs = array(
    'Timesheets' => array('index'),
);
$not_included_fields = array('id', 'type', 'start_time', 'end_time', 'time', 'lunchtime', 'overtime', 'dispatch_status', 'dispatch_flag', 'created_by', 'modified_at', 'modified_by', 'parent', 'merge_status', 'old_parent');
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/timesheet-main.js"></script>
<input type="hidden" id="card_url" value="<?php echo $this->createUrl("/timesheet/main/GetSubEntries"); ?>"/>
<input type="hidden" id="sheet_url" value="<?php echo $this->createUrl("/timesheet/main/GetSheetCard"); ?>"/>
<input type="hidden" id="timecard_export_url" value="<?php echo $this->createUrl('timecardexport', array("id" => 3)); ?>">
<input type="hidden" id="select_msg" value="<?php echo ConstantMessages::$req_select; ?>">
<form id="myForm" method="post" >
    <input type="hidden" name="timecard_id" id="timecard_id" value="0"> 
    <input type="hidden" name="is_timecard" id="is_timecard" value="0">
    <!-- BEGIN CONTENT -->
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Modal title</h4>
                </div>
                <div class="modal-body">
                    Widget settings form goes here
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn blue">Save changes</button>
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE HEAD -->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <!--            <div class="page-title">
        <?php // if($ids==1 && $current == 1){?>
                        <h1>Current - 2 Way Timesheet</h1>
        <?php // } else if($ids==1 && $current==0){?>
                        <h1>Archive - 2 Way Timesheet</h1>
        <?php // } else if($ids==2 && $current==1){?>
                        <h1>Current - 3 Way Timesheet</h1>
        <?php // } else if($ids==2 && $current==0){?>
                        <h1>Archive - 3 Way Timesheet</h1>
        <?php // } else if($ids==3){ ?>
                        <h1>Timesheet Approval Archive</h1>
        <?php // } else if(isset($contract)){ ?>
                        <h1>Timesheets</h1>
        <?php // } else{ ?>
                        <h1>Non Billable Timesheet</h1>
        <?php // } ?>
                    </div>-->
        <!-- END PAGE TITLE -->

    </div>
    <!-- END PAGE HEAD -->

    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->

    <!-- data table starts -->
    <!--<div class="box-tools">
            <a class="btn btn-default pull-right" href="<?php // echo Yii::app()->createUrl('/timesheet/main/addWizard');      ?>">Add Timesheet</a>
        </div>-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box grey-cascade">
                <div class="portlet-title">
                    <div class="caption">
                        <?php if ($ids == 1 && $current == 1) { ?>
                            <i class="icon-arrow-right"></i>Current - 2 Way Timesheet
                        <?php } else if ($ids == 1 && $current == 0) { ?>
                            <i class="fa fa-check-circle"></i>Archive - 2 Way Timesheet
                        <?php } else if ($ids == 2 && $current == 1) { ?>
                            <i class="icon-arrow-right"></i>Current - 3 Way Timesheet
                        <?php } else if ($ids == 2 && $current == 0) { ?>
                            <i class="fa fa-check-circle"></i>Archive - 3 Way Timesheet
                        <?php } else if ($ids == 3) { ?>
                            <i class="fa fa-globe"></i>Timesheet Approval Archives
                            <input type="hidden" id="export_url" value="<?php echo $this->createUrl('export', array("id" => 3)); ?>">
                            <?php
                        } else if ($ids == 4) {
                            if ($current == 1) {
                                ?>
                                <input type="hidden" id="export_url" value="<?php echo $this->createUrl('export', array("id" => 4, "current" => 1)); ?>"> 
                            <?php } else { ?>
                                <input type="hidden" id="export_url" value="<?php echo $this->createUrl('export', array("id" => 4, "current" => 0)); ?>">
                            <?php } ?>
                            <i class="fa fa-globe"></i>Current Timesheet
                        <?php } else { ?>
                            <i class="icon-arrow-right"></i>Non Billable Timesheet
                            <input type="hidden" id="export_url" value="<?php echo $this->createUrl('export'); ?>">
                        <?php } ?>
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>

                    </div>
                </div>
                <!--<script src="<?php // echo Yii::app()->theme->baseUrl;     ?>/js/user-employee-index.js" type="text/javascript"></script>-->
                <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/dropdowns-enhancement.css" rel="stylesheet">
                <div class="portlet-body table-responsive">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="btn-group pull-right" style="padding-left:10px;">
                                    <!--<a class="btn btn-success pull-right btn-circle" href="<?php // echo $this->createUrl('add');     ?>">Add Employee</a>-->
                                </div>
                                <?php if (AppUser::isUserAdmin()) { ?>
                                    <div class="btn-group pull-right">
                                        <a href='javascript:void(0)' class="btn green dropdown-toggle" id='openButton'>Export <i class="fa fa-angle-down"></i>
                                        </a>
                                        <ul id="myDropdown" class="dropdown-menu pull-right">				
                                            <li><a id="export" href="javascript:void(0);" onclick='exportData("export")'>Export Selected Record</a></li>
                                            <li class="divider"></li>
                                            <li><a  id="export_all" href="javascript:void(0);" onclick='exportData("export_all")'>Export All Record</a></li>
                                            <?php if (isset($entries[0])) { ?>
                                                <li><input type="checkbox" id="All" value="1"> <label id='selectAll' for="All">Select All</label></li>
                                                <li><input type="checkbox" class="select" name="export_fields[]" id="employee" value="employee"><label for="employee">Employee</label></li>
                                                <li><input type="checkbox" class="select" name="export_fields[]" id="approver" value="approver"><label for="approver">Approver</label></li>
                                                <?php
                                                foreach ($entries[0] as $key => $item) {
                                                    if (!in_array($key, $not_included_fields)) {
                                                        ?>
                                                        <li><input class='select' type="checkbox" name="export_fields[]" id="<?php echo $key; ?>" value="<?php echo $key; ?>"><label for="<?php echo $key; ?>"><?php echo Timesheet::model()->getAttributeLabel($key); ?></label></li>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                                <li><input class="select" type="checkbox" name="export_fields[]" id="staff_id" value="staff_id"><label for="staff_id">Staff ID</label></li>

                                            <?php } ?>
                                        </ul>

                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover" id="example1">
                            <thead>
                                <tr>
                                    <th class="table-checkbox">
                                        <input type="checkbox" data-checkbox="true" class="group-checkable" id="checkAll" onchange="" data-set="#sample_1 .checkboxes"/>
                                    </th>
                                    <th>
                                        Contract
                                    </th>
                                    <th>
                                        Employee
                                    </th>
                                    <th>
                                        Approver
                                    </th>
                                    <th>
                                        Period
                                    </th>
                                    <th>
                                        Status
                                    </th>
                                    <th>
                                        PaySlip
                                    </th>
                                    <th>
                                        Action
                                    </th>

                                </tr>
                            </thead>
                            <?php if (count($entries) > 0) { ?>
                                <tbody role="alert" aria-live="polite" aria-relevant="all">
                                    <?php
                                    foreach ($entries as $entry) {
                                        $paySlipStatus = AppPayslip::isIssued($entry);
                                        if (AppUser::isUserAdmin()) {
                                            if (isset($entry->contract->parent)) {
                                                if ($entry->contract->type == 'out_b' && $entry->contract->company_id != AppUser::getUserCompany()->id) {
                                                    $etype = ucwords(str_replace('_', ' ', $entry->contract->parent->CustomEmploymentType));
                                                }
                                            } else{
                                                $etype = $entry->contract->CustomName;
                                            }
                                        } else {
                                            $etype = $entry->contract->CustomName;
                                        }
                                        ?>
                                        <tr>
                                            <td><input type="checkbox" name="entry[]" class="check" value="<?php echo $entry->id; ?>"/></td>
                                            <td><?php
                                                if (isset($etype) && $etype == '') {
                                                    echo AppContract::getContractCustomName($entry->contract);
                                                } else if (isset($etype) && $etype != '') {
                                                    echo $etype;
                                                } else {
                                                    echo ucwords(str_replace('_', ' ', AppUser::isUserAdmin() ? $entry->contract->CustomEmploymentType : $entry->contract->CustomName));
                                                }

//                            echo (isset($etype) && $etype != '') ? $etype : ucwords(str_replace('_', ' ', AppUser::isUserAdmin() ? $entry->contract->CustomEmploymentType : $entry->contract->CustomName)); 
                                                ?>
                                            </td>
                                            <td><?php echo isset($entry->contract->user->full_name) ? $entry->contract->user->full_name : ""; ?></td>
                                            <td><?php echo isset($entry->contract->approver->full_name) ? $entry->contract->approver->full_name : ""; ?></td>
                                            <td>
                                                <?php echo AppTimeSheet::formatSheetPeriod($entry); ?>
                                            </td>
                                            <td><?php echo $entry->status; ?></td>
                                            <td><?php echo ($paySlipStatus) ? 'issued' : 'N/A'; ?></td>
                                            <td><div class="btn-group">
                                                    <button class="btn green dropdown-toggle" data-toggle="dropdown">Actions <i class="fa fa-angle-down"></i>
                                                    </button>
                                                    <ul class="dropdown-menu pull-right">
                                                        <li><a data-toggle="modal" data-target=".ajaxModel" data-whatever="<?php echo $entry->id; ?>">View</a></li>
                                                        <li class="divider"></li>
                                                        <?php if (( $entry->type != "daily" && ( AppUser::isUserAdmin() || $entry->status == "pending"))) { ?>
                                                            <li><a href="<?php echo Yii::app()->createUrl("timesheet/main/edit", array("entry" => $entry->id)); ?>">Update</a></li>
                                                            <li class="divider"></li>
                                                        <?php } ?>
                                                        <?php if (AppUser::isUserAdmin()) { ?>
                                                            <li><a href="<?php echo Yii::app()->createUrl("invoice/main/sendinvoice", array("timesheet" => $entry->id)); ?>">Invoice</a></li><li class="divider"></li>
                                                            <?php if ($entry->status == 'pending' && AppContract::getInternalContract($entry->contract->user->id)->company_id == AppUser::getUserCompany()->id) { ?>
                                                                <li><a href="javascript:void(0);" onclick="if (confirm('<?php echo $entry->type == 'daily' ? 'The TimeCard for this user will be locked for the current week and the user will not be able to submit more timesheets until the selected timesheet cycle runs out. ' : '' ?>Are you sure you want to approve this timesheet?')) {
                                                                            window.location = '<?php echo Yii::app()->createUrl("timesheet/main/instantapprove", array("tid" => $entry->id)); ?>';
                                                                        }">Instant Approve</a></li><li class="divider"></li>
                                                                   <?php } ?>
                                                               <?php } ?>
                                                               <?php if ($entry->type == "daily") { ?>
                                                            <li><a href="javascript:void(0);" onclick="showTimeCard(<?php echo $entry->id; ?>)" class="<?php echo ($entry->type == "daily") ? "wklyLnkPrnt" : ""; ?>" tSheetId="<?php echo $entry->id; ?>">Show TimeCard</a></li>
                                                            <li class="divider"></li>
                                                        <?php } ?>
                                                        <?php if ($entry->status == "approved" && $entry->dispatch_flag == 0 && AppUser::canPayroll()) { ?>
                                                            <li><a href="<?php echo Yii::app()->createUrl("timesheet/main/DispatchCopy", array("entry" => $entry->id)); ?>" >Copy to Payroll Folder</a></li>
                                                            <li class="divider"></li>
                                                        <?php } ?>
                                                        <?php if (isset($entry->payrolls[0])) { ?>
                                                            <?php if ($entry->payrolls[0]->status == 'completed') { ?>
                                                                <?php $payslipEntry = PayslipEntry::model()->findByAttributes(array('payroll_id' => $entry->payrolls[0]->id)); ?>
                                                                <?php if (AppTimeSheet::canDownloadPayslip($payslipEntry)) { ?>
                                                                    <?php if ($payslipEntry->type == 'csv') { ?>
                                                                        <li> <a href="#" data-toggle="modal" data-target=".ajaxModel" data-url="<?php echo Yii::app()->createUrl("/payroller/payslip/showInTemplate"); ?>" data-whatever="<?php // echo  $payslipEntry->id;     ?>" >View Issued Payslip</a></li>
                                                                    <?php } else { ?>
                                                                        <li><a href="<?php echo Yii::app()->createUrl('payroller/payslip/download', array('id' => $payslipEntry->id)); ?>">Download Payslip</a></li>
                                                                    <?php } ?>
                                                                    <li class="divider"></li>
                                                                <?php } ?>
                                                            <?php } ?> 
                                                        <?php } ?>
                                                        <?php if ($entry->type == "daily") { ?>
                                                            <li><a href="javascript:void(0);" onclick="exportTimecard('<?php echo $entry->id; ?>')">Export TimeCard</a></li>
                                                        <?php } ?>
                                                    </ul>
                                                </div>
                                            </td>                            
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            <?php } ?>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>      <!-- /data table ends -->

        <!-- END CONTENT -->
</form>
<div class="childEntryContainer" id="childEntryContainer" style="display:none">

</div>
