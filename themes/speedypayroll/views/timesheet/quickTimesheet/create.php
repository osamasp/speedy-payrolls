
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content1">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Modal title</h4>
                    </div>
                    <div class="modal-body">
                        Widget settings form goes here
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue">Save changes</button>
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
<!--            <div class="page-title">
                <h1>Create new 1 way timesheet</small></h1>
            </div>-->
            
        </div>
        <!-- END PAGE HEAD -->
        <!-- BEGIN BREADCRUMB -->
        <?php echo $this->renderPartial('//layouts/includes/flash-messages'); ?>

        <?php

$this->breadcrumbs=array(
	'Oneway Timesheets'=>array('index'),
	'Create',
);
?>
        <?php $this->renderPartial('_form', array('model' => $model, 'contacts' => $contacts, 'files' => FALSE, 'company' => null, 'contact_details' => null, 'expenses' => null, 'currencies' => $currencies)); ?>

    </div>
</div>