<?php
/* @var $this QuickTimesheetController */
/* @var $model QuickTimesheet */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

        <div class="form-group">
        <?php echo $form->label($model,'id'); ?>
        <?php echo $form->textField($model,'id',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'hours'); ?>
        <?php echo $form->textField($model,'hours', array('class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'rate'); ?>
        <?php echo $form->textField($model,'rate', array('class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'contact_id'); ?>
        <?php echo $form->textField($model,'contact_id',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'date'); ?>
        <?php echo $form->textField($model,'date', array('class' => 'form-control')); ?>
    </div>

<div class="form-group">
    <?php echo CHtml::submitButton('Search'); ?>
</div>

<?php $this->endWidget(); ?>
<!-- search-form -->