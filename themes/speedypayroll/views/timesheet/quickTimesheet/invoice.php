<?php
/* @var $this QuickTimesheetController */
/* @var $model QuickTimesheet */

$this->breadcrumbs=array(
	'Quick Timesheets'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Invoice',
);
?>

<div class="box-header">
    <h3 class="box-title">Invoice Timesheet</h3>
</div>

<?php $this->renderPartial('_form', array('model'=>$model,'contacts' => $contacts,'currencies' => $currencies,'contact_details' => $contact_details,'expenses' => $expenses, 'company' => $company)); ?>
