<?php
/* @var $this QuickTimesheetController */
/* @var $model QuickTimesheet */

$this->breadcrumbs = array(
    'Quick Timesheets' => array('index'),
    $model->id,
);
?>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/quicktimesheet-cemail.js" type="text/javascript"></script>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Send Oneway Timesheet Email
        </div>
    </div>
    <div class="portlet-body form">
        <div class="box-body" style='padding-left:20px;'>
            <form method="post" id="quick-ts-form" action="<?php echo Yii::app()->getBaseUrl(TRUE) . '/timesheet/quickTimesheet/completedEmail/id/' . $model->id ?>">
                <input type="hidden" id="contact_details" value="<?php echo Yii::app()->baseUrl; ?>/timesheet/quickTimesheet/getContactDetails"/>

                <div class="table-responsive">
                    <div class="box-body">
                        <div class="col-md-12" style="padding-left:0;"><h4>Contact Details</h4></div>
                        <?php if ($contacts) { ?>
                            <div class="form-group">
                                <select class="form-control" name="contacts" id="contacts" onchange="check()">
                                    <option value="out">Select Contact</option>
                                    <?php foreach ($contacts as $contact) {
                                        if ($contact->id == $model->contact_id) { ?>
                                            <option value="<?php echo $contact->id; ?>" selected="selected"><?php echo $contact->name; ?></option><?php } else { ?>
                                            <option value="<?php echo $contact->id; ?>"><?php echo $contact->name; ?></option>
        <?php }
    } ?>
                                </select> 
                            </div>
<?php } else { ?>
                            <div class="form-group">
                                <select class="form-control" name="contacts" id="contacts" onchange="check()">
                                    <option value="out">Select Contact</option>
                                </select> 
                            </div>
<?php } ?>
<?php if ($contact_details) { ?>

                            <div class="col-md-3"  style="padding-left: 0;padding-top: 15px;">
                                <b>Client Email*:</b>
                                <input type="text" name="contact-email" value="<?php if ($contact_details) {
        echo $contact_details->email;
    } ?>" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="E.g:example@abc.com" class="form-control" id="email" placeholder="Enter Email" required>
                            </div>
                            <div class="col-md-3"  style="padding-left: 0;padding-top: 15px;">
                                <b>Client Name*:</b>
                                <input type="text" name="contact-name" value="<?php if ($contact_details) {
        echo $contact_details->name;
    } ?>" class="form-control" id="name" placeholder="Enter Name" required>
                            </div>
                            <div class="col-md-3"  style="padding-left: 0;padding-top: 15px;">
                                <b>Address*:</b>
                                <input type="text" name="client_add" class="form-control" id="cl_add" value="<?php if ($contact_details) {
        echo $contact_details->address;
    } ?>" placeholder="Enter Address" required>
                            </div>
                            <div class="col-md-3" style="padding-left: 0;padding-top: 15px;">
                                <b>City*:</b>
                                <input type="text" name="client_city" class="form-control" id="cl_city" value="<?php if ($contact_details) {
        echo $contact_details->city;
    } ?>" placeholder="Enter City" required>
                            </div>
                            <div class="col-md-3" style="padding-left: 0;padding-top: 15px;">
                                <b>Country*</b>
                                <select id="cl_country" name="client_country" id="cl_country" class="form-control">
    <?php foreach (AppInterface::getCountries() as $item) { ?>
                                        <option><?php echo $item ?></option>
    <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-3" style="padding-left: 0;padding-top: 15px;">
                                <b>Post Code*:</b>
                                <input type="text" name="client_pc" class="form-control" id="cl_pc" placeholder="Enter Post Code" value="<?php if ($contact_details) {
        echo $contact_details->post_code;
    } ?>" required>
                            </div>
<?php } else { ?>
                            <div class="col-md-3" style="padding-left: 0;padding-top: 15px;">
                                <b>Client Email*:</b>
                                <input type="text" name="contact-email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="E.g:example@abc.com" class="form-control" id="email" placeholder="Enter Email" required>
                            </div>
                            <div class="col-md-3" style="padding-left: 0;padding-top: 15px;">
                                <b>Client Name*:</b>
                                <input type="text" name="contact-name" class="form-control" id="name" placeholder="Enter Name" required>
                            </div>
                            <div class="col-md-3" style="padding-left: 0;padding-top: 15px;">
                                <b>Address*:</b>
                                <input type="text" name="client_add" class="form-control" id="cl_add" placeholder="Enter Address" required>
                            </div>
                            <div class="col-md-3" style="padding-left: 0;padding-top: 15px;">
                                <b>City*:</b>
                                <input type="text" name="client_city" class="form-control" id="cl_city" placeholder="Enter City" required>
                            </div>
                            <div class="col-md-3" style="padding-left: 0;padding-top: 15px;">
                                <b>Country*</b>
                                <select id="cl_country" name="client_country" id="cl_country" class="form-control">
                            <?php foreach (AppInterface::getCountries() as $item) { ?>
                                        <option><?php echo $item ?></option>
                            <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-3" style="padding-left: 0;padding-top: 15px;">
                                <b>Post Code*:</b>
                                <input type="text" name="client_pc" class="form-control" id="cl_pc" placeholder="Enter Post Code" required>
                            </div>
                        <?php
                        }
                        $expenses = QuickTsExpenses::model()->findAllByAttributes(array('quick_time_id' => $model->id));
                        $expense_total = 0;
                        foreach ($expenses as $expense)
                            $expense_total += $expense->expense_value;
                        ?>
                        <?php
                        $this->widget('zii.widgets.CDetailView', array(
                        'data' => $model,
                        'attributes' => array(
                        'date',
                        'status',
                        array('name' => 'Currency', 'value' => $currency->name),
                        array('name' => 'Hours', 'value' => date('H:i', $model->hours)),
                        'rate',
                        array('name' => 'Expense', 'value' => $expense_total),
                        'total',
                        ),
                        ));
                        ?>
                        <table class="table"><tr class="even"><td><b>Receipts: </b></td><td><?php if (count($files) > 0) {
                            echo AppInterface::getUploadedReceipts($files, 'Receipts', 'reciept_file', 'receipts', TRUE);
                        } else { ?> <label>-</label> <?php } ?></td></tr>
                            <tr>
                                <td colspan="2">
                                    <div>
                                        <input type="text" name="QuickTimesheet" value="send" style="display:none;">
                                        <a href="javascript:void(0);" onclick="validate()" class="btn btn-primary">SEND INVOICE</a>
                                        <a id="sendinvoice" class="btn btn-primary" data-toggle="modal" name="QuickTimesheet" data-target=".Model" data-whatever="<?php echo $this->createUrl('/timesheet/quickTimesheet/completedemail/id/' . $model->id); ?>" style="display:none;">SEND EMAIL</a>
                                        <!--<input type="submit" name="QuickTimesheet" value="Send Email" class="btn btn-primary" style="text-align: right;">-->
                                    </div>
                                </td>
                            </tr>
                        </table>       


                    </div>
                </div>
            </form>
        </div>
    </div>
</div>