<?php
/* @var $this QuickTimesheetController */
/* @var $model QuickTimesheet */
/* @var $form CActiveForm */
?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'quick-timesheet-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => false,
    'htmlOptions' => array('enctype' => 'multipart/form-data')
        ));
?>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/thirdparty/js/bootstrap-datepaginator/bootstrap-datepaginator.min.js" type="text/javascript"></script>
<div class="row">
    <div class="col-md-6">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-plus-square"></i> Add New 1 Way Timesheet
                </div>
                <div class="tools">
                    <a href="" class="collapse">
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <!--<button type="button" class="" data-dismiss="alert"></button>-->
                <h4>Client Details</h4>
                <div class="form-inline">
                    <div class="form-group">
                        <button type="button" onclick="javascript:$('#clients').show();
                                $('#clientMsg').hide();" class="btn blue btn-circle">Existing Client</button>
                    </div>
                    <div class="form-group">
                        <button type="button" onclick="javascript:newClient();
                                $('#clients').hide();
                                $('#clientMsg').show();" class="btn green btn-circle">New Client</button>
                    </div>
                </div>
                <hr>

                <h4>Client Details</h4>
                <h5 id="clientMsg" class="block">Please complete the fields below:</h5>
                <div id="clients" style="display:none;">
                    <?php if ($contacts) { ?>
                        <div class="form-group">
                            <select class="form-control" name="contacts" id="contacts" onchange="check()">
                                <option value="out">Select Contact</option>
                                <?php
                                foreach ($contacts as $contact) {
                                    if ($contact->id == $model->contact_id) {
                                        ?>
                                        <option value="<?php echo $contact->id; ?>" selected="selected"><?php echo $contact->name; ?></option><?php } else { ?>
                                        <option value="<?php echo $contact->id; ?>"><?php echo $contact->name; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select> 
                        </div>
                    <?php } else { ?>
                        <div class="form-group">
                            <select class="form-control" name="contacts" id="contacts" onchange="check()">
                                <option value="out">Select Contact</option>
                            </select> 
                        </div>
                    <?php } ?>
                </div>
                <?php if ($contact_details) { ?>

                    <div class="form-group">
                        <label class="sr-only" for="exampleInputEmail2">Client Name*</label>
                        <input type="text" name="contact-name" class="form-control" id="name" value="<?php
                        if ($contact_details) {
                            echo $contact_details->name;
                        }
                        ?>" placeholder="Enter Name" required>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="exampleInputPassword2">Client Email*</label>
                        <input type="text" name="contact-email" value="<?php
                        if ($contact_details) {
                            echo $contact_details->email;
                        }
                        ?>" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="E.g:example@abc.com" class="form-control" id="email" placeholder="Enter Email" required>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="exampleInputEmail2">Address*</label>
                        <input type="text" name="client_add" class="form-control" id="cl_add" value="<?php
                        if ($contact_details) {
                            echo $contact_details->address;
                        }
                        ?>" placeholder="Enter Address" required>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="exampleInputPassword2">City*</label>
                        <input type="text" name="client_city" class="form-control" id="cl_city" value="<?php
                        if ($contact_details) {
                            echo $contact_details->city;
                        }
                        ?>" placeholder="Enter City" required>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="exampleInputPassword2">Country*</label>
                        <select id="cl_country" name="client_country" class="form-control">
                            <?php foreach (AppInterface::getCountries() as $item) { ?>
                                <option><?php echo $item ?></option>
                            <?php } ?>
                        </select> 
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="exampleInputEmail2">Post Code*</label>
                        <input type="text" name="client_pc" class="form-control" id="cl_pc" placeholder="Enter Post Code" value="<?php
                        if ($contact_details) {
                            echo $contact_details->post_code;
                        }
                        ?>" required>
                    </div>
                <?php } else { ?>
                    <div class="form-group">
                        <label class="sr-only" for="exampleInputEmail2">Client Name*</label>
                        <input type="text" name="contact-name" class="form-control" id="name" placeholder="Enter Name" required>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="exampleInputPassword2">Client Email*</label>
                        <input type="text" name="contact-email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="E.g:example@abc.com" class="form-control" id="email" placeholder="Enter Email" required>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="exampleInputEmail2">Address*</label>
                        <input type="text" name="client_add" class="form-control" id="cl_add" placeholder="Enter Address" required>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="exampleInputPassword2">City*</label>
                        <input type="text" name="client_city" class="form-control" id="cl_city" placeholder="Enter City" required>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="exampleInputPassword2">Country*</label>
                        <select id="cl_country" name="client_country" class="form-control" placeholder="Country">
                            <?php
                            foreach (AppInterface::getCountries() as $item) {
                                if ($item == "United Kingdom") {
                                    ?>
                                    <option value="<?php echo $item; ?>" selected><?php echo $item; ?></option>
                                <?php } else { ?>
                                    <option value="<?php echo $item; ?>" ><?php echo $item; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="exampleInputEmail2">Post Code*</label>
                        <input type="text" name="client_pc" class="form-control" id="cl_pc" placeholder="Enter Post Code" required>
                    </div>


                <?php } ?>
                <hr>
            </div><!-- /portlet body end -->
        </div><!-- /portlet end -->

<!--        <div class="portlet box blue hidden-xs">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-plus-square"></i> Clock-in Clock-out Internal Timesheets
                </div>
                <div class="tools">
                    <a href="" class="collapse">
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <button type="button" class="" data-dismiss="alert"></button>
                <h4>Simple Clock Entry System</h4>

                <hr>
                <p> Use this system for simple timesheet entries with clock in and clock out time stamp feature.
                    The clock feature will only record the time for your current running day.

                    clock in out 

                <div class="swrapper">
                    <h4>Clock-in Clock-out System</h4>
                    <p><span id="seconds">00</span>:<span id="tens">00</span></p>

                    <button id="button-start" type="submit" class="btn blue">Clock In</button>
                    <button id="button-stop" type="submit" class="btn green">Clock Out</button>
                    <button id="button-reset" type="submit" class="btn red">Create Timesheet</button>
                </div> 

                /clock in out 

            </div> /portlet body end 
        </div> /portlet end -->

    </div><!-- col-md-6 Ends -->
    <div class="col-md-6">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-plus-square"></i> Add New 1 Way Timesheet
                </div>
                <div class="tools">
                    <a href="" class="collapse">
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <!--<button type="button" class="" data-dismiss="alert"></button>-->
                <h4>Timesheet Details</h4>
                <div class="form-group">
                    <div>
                        <div class="input-group">            
                            <?php echo $form->textField($model, 'date', array('class' => 'form-control pull-left datePicker disableEle', 'placeholder' => 'Click To Select the Date', 'required' => 'required', 'style' => 'cursor:context-menu')); ?>
                            <div class="input-group-btn">
                                <button class="btn default btn-circle" type="button"><i class="fa fa-calendar"></i></button>
                            </div>
                        </div>
                    </div> 
                </div>
                <!--   <div class="form-group">
                       <h5>Timesheet Submission Date</h5>
                <?php // echo $form->hiddenField($model, 'date', array('id' => 'date', 'value' => date('d/m/Y')));   ?>
                       <div class="col-md-12" style="padding-bottom: 12px;">
                           <div id="datepaginator_sample_2"></div>
                       </div>
            </div>-->
            <div class="form-group">
                <label class="sr-only" for="exampleInputEmail2">Matter</label>
                <?php echo $form->textField($model, 'job_title', array('class' => 'form-control', 'placeholder' => 'Matter')); ?>
                <?php echo $form->error($model, 'job_title', array('class' => 'text-red')); ?>
            </div>
            <div class="form-group">
                <label class="sr-only" for="exampleInputPassword2">Activity</label>
                <?php echo $form->textField($model, 'job_reference', array('class' => 'form-control', 'placeholder' => 'Activity')); ?>
                <?php echo $form->error($model, 'job_reference', array('class' => 'text-red')); ?>
            </div>
            <div class="form-group">
                <label class="sr-only" for="exampleInputPassword2">Comment</label>
                <?php echo $form->textField($model, 'job_description', array('class' => 'form-control', 'placeholder' => 'Comment')); ?>
                <?php echo $form->error($model, 'job_description', array('class' => 'text-red')); ?>
            </div>
            <hr>
            <h4>Record Hours</h4>
            <form class="form-inline" role="form">
                <div class="bootstrap-timepicker"><div class="bootstrap-timepicker-widget dropdown-menu"><table><tbody><tr><td><a href="#" data-action="incrementHour"><i class="glyphicon glyphicon-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="incrementMinute"><i class="glyphicon glyphicon-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td class="meridian-column"><a href="#" data-action="toggleMeridian"><i class="glyphicon glyphicon-chevron-up"></i></a></td></tr><tr><td><span class="bootstrap-timepicker-hour">10</span></td> <td class="separator">:</td><td><span class="bootstrap-timepicker-minute">45</span></td> <td class="separator">&nbsp;</td><td><span class="bootstrap-timepicker-meridian">PM</span></td></tr><tr><td><a href="#" data-action="decrementHour"><i class="glyphicon glyphicon-chevron-down"></i></a></td><td class="separator"></td><td><a href="#" data-action="decrementMinute"><i class="glyphicon glyphicon-chevron-down"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="toggleMeridian"><i class="glyphicon glyphicon-chevron-down"></i></a></td></tr></tbody></table></div>
                    <div class="form-group">
                        <label class="sr-only" for="exampleInputEmail2">Hours worked</label>
                        <div class="input-group">
                            <?php if (isset($model->hours)) { ?>
                                <input type="text" name="QuickTimesheet[hours]"  id="QuickTimesheet_hours" class="form-control timepickerIn disableEle" value="<?php echo date('H:i', $model->hours); ?>">
                            <?php } else { ?>
                                <?php echo $form->textField($model, 'hours', array('class' => 'form-control timepickerIn disableEle', 'onkeypress' => 'return isNumberKey(event);','style'=>'cursor:context-menu')); ?>
                            <?php } ?>
                            <?php echo $form->error($model, 'hours', array('class' => 'text-red')); ?>
                            <div class="input-group-btn">
                                <button class="btn default" type="button"><i class="fa fa-clock-o"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="sr-only" for="exampleInputPassword2">Hourly Rate</label>
                    <?php echo $form->textField($model, 'rate', array('class' => 'form-control', 'placeholder' => 'Hourly rate', 'onkeypress' => 'return isNumberKey(event);')); ?>
                    <?php echo $form->error($model, 'rate', array('class' => 'text-red')); ?>
                </div>
                <div class="form-group">
                    <select class="form-control" name="currency" value="<?php echo $model->currency_id; ?>" id="currency">
                        <?php
                        foreach ($currencies as $currency) {
                            if ($currency->id == $model->currency_id) {
                                ?>
                                <option value="<?php echo $currency->id; ?>" selected="selected"><?php echo $currency->name; ?></option><?php } else { ?>
                                <option value="<?php echo $currency->id; ?>"><?php echo $currency->name; ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select> 
                </div>
                <div class="form-group">
                    <label>Recorded Hours Total</label>
                </div>
                <div class="form-group">
                    <input type="text" name="hourly_total" placeholder="Auto Total" id="hourly_total" class="form-control" readonly>
                </div>		
            </form>
            <hr>
            <h4>Record Expense</h4>
            <?php
            $counter = 0;
            if ($expenses) {
                ?>
                <table id="myTable" class="table-responsive">
                    <?php
                    foreach ($expenses as $item) {
                        $counter++;
                        ?>
                        <tr>
                            <?php if ($counter == 1) { ?>
                                <td class="col-md-3" style="padding:10px 15px 0 0;">
                                    <b>Name:</b><input type="text" value="<?php echo $item->expense_name; ?>" id="expense_name<?php echo $counter; ?>" name="expense_name<?php echo $counter; ?>" class="form-control"></td>
                                <td class="col-md-3" style="padding:10px 15px 0 0;">
                                    <b>Value:</b><input type="text" value="<?php echo $item->expense_value; ?>" id="expense_value<?php echo $counter; ?>" name="expense_value<?php echo $counter; ?>" class="form-control" onchange="getTotal()" onkeypress="return isNumberKey(event);">
                                </td>
                                <td class="col-md-3" style="padding:10px 15px 0 0;">
                                    <b>Comment:</b><input type="text" id="comment<?php echo $counter; ?>" name="comment<?php echo $counter; ?>" value="<?php echo $item->comment; ?>" class="form-control">
                                </td>
                            <?php } else { ?>
                                <td class="col-md-3" style="padding:10px 15px 0 0;">
                                    <input type="text" name="expense_name<?php echo $counter; ?>" id="expense_name<?php echo $counter; ?>" value="<?php echo $item->expense_name; ?>" class="form-control"></td>
                                <td class="col-md-3" style="padding:10px 15px 0 0;">
                                    <input type="text" name="expense_value<?php echo $counter; ?>" id="expense_value<?php echo $counter; ?>" class="form-control" onchange="getTotal()" onfocusout="getTotal()" value="<?php echo $item->expense_value; ?>" onkeypress="return isNumberKey(event);">
                                </td>
                                <td class="col-md-3" style="padding:10px 15px 0 0;">
                                    <input type="text" name="comment<?php echo $counter; ?>" id="comment<?php echo $counter; ?>" value="<?php echo $item->comment; ?>" class="form-control">
                                </td>

                            <?php } ?>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td colspan="6" class="col-md-6" style=" text-align:right;padding-left:0;  ">
                            <a href="javascript:void(0)" onclick="addItem()" class="btn btn-default btn-circle">Add More</a>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-3" style="padding-left: 0;">
                            <?php echo $form->labelEx($model, 'total'); ?>
                            <?php echo $form->textField($model, 'total', array('class' => 'form-control', 'onfocus' => 'javascript:getTotal()', 'readonly' => TRUE)); ?>
                            <?php // echo $form->error($model, 'total', array('class' => 'text-red'));   ?>
                        </td>
                    </tr>
                    <input type ='hidden' name='count' id="count" value="<?php echo $counter; ?>">
                </table>
            <?php } else { ?>
                <table id="myTable" class="table-responsive">
                    <tr>
                        <td class="col-md-3" style="padding-left:0;">
                            <b>Name:</b><input type="text" id="expense_name1" name="expense_name1" onfocusout="getTotal()" class="form-control">
                        </td>
            <!--            <td>&nbsp;</td>-->
                        <td class="col-md-3" style="padding-left:0;">
                            <b>Value:</b><input type="text" id="expense_value1" name="expense_value1"  onchange="getTotal()" onfocusout="getTotal()" id="expense_value1" class="form-control" onkeypress="return isNumberKey(event);">
                        </td>
                        <td class="col-md-3" style="padding-left:0;">
                            <b>Comment:</b><input type="text" name="comment1" id="comment1" onfocusout="getTotal()" class="form-control">
                        </td>
                    <input type ='hidden' name='count' id="count" value='1'>
                    </tr>
                    <tr>
                        <td colspan="6" class="col-md-6" style="text-align:right;padding-left:0; padding-top:10px;">
                            <a href="javascript:void(0)" onclick="addItem()" class="btn btn-default btn-circle">Add More </a>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-3" style="padding-left: 0;">
                            <b>Total*:</b>
                            <?php echo $form->textField($model, 'total', array('class' => 'form-control', 'readonly' => TRUE)); ?>
                            <?php // echo $form->error($model, 'total', array('class' => 'text-red'));   ?>
                        </td>
                    </tr>
                </table>
            <?php } ?>

            <hr>
            <h4>Upload Receipts</h4>
            <h5>Keep original receipts for your records.
            </h5>
            <div id="file_upload">
                <?php echo $form->fileField($model, 'reciept_file[]', array("id" => "files", "class" => "btn green btn-lg dropdown-toggle btn-circle", "multiple" => true, "style" => "margin-top:5px;")); ?>

                <?php if ($files) { ?>
                    <div class="form-group" style="padding-left:0;padding-top:15px;">
                        <?php echo AppInterface::getUploadedReceipts($files, 'Receipts', 'reciept_file', 'receipts', FALSE) ?>
                    </div><?php } ?>
            </div>
            <div class="form-group" style="text-align: left; padding-top: 10px;"><a href="javascript:void(0)" class="btn btn-default btn-circle" onclick="addFile()">Add more files</a></div>
            <hr>
            <div class="btn-group">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'CREATE TIMESHEET' : 'Save', array('class' => 'btn btn-circle green-haze btn-circle')); ?>

            </div>
        </div>



    </div> <!--/portlet ends-->




</div>
</div>
</div><!-- row Ends -->
<script type="text/javascript">

    jQuery(document).ready(function () {

        jQuery('.disableEle').attr('readonly', 'readonly');
        jQuery('.numPicker').bootstrapNumber({
            upClass: 'danger',
            downClass: 'success'

        });



        $(".timepickerIn").timepicker({
            showInputs: false,
            showMeridian: false,
            defaultTime: '00:00',
            minuteStep: 1
        });


//        var options = {
//            selectedDateFormat: 'DD-MM-YYYY',
//            selectedDate: moment(),
//        }
//        $('#datepaginator_sample_2').datepaginator(options);
//        $('#datepaginator_sample_2').on('selectedDateChanged', function (event, date) {
//            var fullDate = new Date(date);
//            //convert month to 2 digits
//            var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '0' + (fullDate.getMonth() + 1);
//
//            var currentDate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
//            $("#date").val(currentDate);
//        });
        $("#QuickTimesheet_date").datepicker({format: 'dd/mm/yyyy',autoclose: true,orientation: "left bottom",todayHighlight:true});
        jQuery('#date').datepicker({format: 'dd/mm/yyyy'});
        check();
        jQuery('#quick-timesheet-form').h5Validate({errorClass: 'errorClass'});
        getvalue();
        getTotal();
        jQuery("#QuickTimesheet_rate").focusout(function () {
            var hours = jQuery("#QuickTimesheet_hours").val();
            hours = hours.split(':');
//            hours = hours.replace(":", ".");
            var rate = jQuery("#QuickTimesheet_rate").val();
            if (hours == null || rate == null)
                jQuery("#hourly_total").val(0);
            else {
                var val = parseFloat(hours[0] * rate) + (parseFloat(hours[1]/60)*rate);
                jQuery("#hourly_total").val(val.toFixed(2));
            }
            getTotal();
        });
        jQuery("#QuickTimesheet_hours").focusout(function () {
            var hours = jQuery("#QuickTimesheet_hours").val();
             hours = hours.split(':');
//            hours = hours.replace(":", ".");
            var rate = jQuery("#QuickTimesheet_rate").val();
            if (hours == null || rate == null)
                jQuery("#hourly_total").val(0);
            else {
                var val = parseFloat(hours[0] * rate) + (parseFloat(hours[1]/60)*rate);
                jQuery("#hourly_total").val(val.toFixed(2));
            }
            getTotal();
        });
        jQuery("#QuickTimesheet_hours").change(function () {
            var hours = jQuery("#QuickTimesheet_hours").val();
             hours = hours.split(':');
//            hours = hours.replace(":", ".");
            var rate = jQuery("#QuickTimesheet_rate").val();
            if (hours == null || rate == null)
                jQuery("#hourly_total").val(0);
            else {
                var val = parseFloat(hours[0] * rate) + (parseFloat(hours[1]/60)*rate);
                jQuery("#hourly_total").val(val.toFixed(2));
            }
            getTotal();
        });
    });

    function getvalue() {
        var hours = jQuery("#QuickTimesheet_hours").val();
         hours = hours.split(':');
//            hours = hours.replace(":", ".");
            var rate = jQuery("#QuickTimesheet_rate").val();
            if (hours == null || rate == null)
                jQuery("#hourly_total").val(0);
            else {
                var val = parseFloat(hours[0] * rate) + (parseFloat(hours[1]/60)*rate);
                jQuery("#hourly_total").val(val.toFixed(2));
            }
    }

    function getTotal()
    {
        var hour_total = jQuery("#hourly_total").val();
        var count = jQuery("#count").val();
        var fname = "expense_value";
        var expense = 0;
        for (i = 1; i <= count; i++) {
            var name = fname.concat(i);
            var val = document.getElementById(name).value;
            if (val == "") {
                val = 0;
            }
            if (val != null) {
                expense = expense + parseFloat(val);
            }
        }
        if (hour_total == "") {
            hour_total = 0;
        }
        var result = 0;
        result = parseFloat(hour_total) + parseFloat(expense);
        if (result != null)
            jQuery("#QuickTimesheet_total").val(result.toFixed(2));
    }

    function newClient()
    {
        document.getElementById("email").disabled = false;
        document.getElementById("name").disabled = false;
        document.getElementById("cl_add").disabled = false;
        document.getElementById("cl_country").disabled = false;
        document.getElementById("cl_pc").disabled = false;
        document.getElementById("cl_city").disabled = false;
        document.getElementById("email").value = "";
        document.getElementById("name").value = "";
        document.getElementById("cl_add").value = "";
        document.getElementById("cl_country").value = "United Kingdom";
        document.getElementById("cl_pc").value = "";
        document.getElementById("cl_city").value = "";
        $("#contacts").val("out");
        $("#clients").hide();
    }

    function check() {
        if (document.getElementById("contacts").value != 'out') {
            document.getElementById("email").disabled = true;
            document.getElementById("name").disabled = true;
            document.getElementById("cl_add").disabled = true;
            document.getElementById("cl_country").disabled = true;
            document.getElementById("cl_pc").disabled = true;
            document.getElementById("cl_city").disabled = true;
        }
        else {
            document.getElementById("email").disabled = false;
            document.getElementById("name").disabled = false;
            document.getElementById("cl_add").disabled = false;
            document.getElementById("cl_country").disabled = false;
            document.getElementById("cl_pc").disabled = false;
            document.getElementById("cl_city").disabled = false;
        }
        var id = jQuery("#contacts").val();
        var url = "<?php echo Yii::app()->baseUrl; ?>/timesheet/quickTimesheet/getContactDetails";
        $.post(url, {contact_id: id},
        function (result) {
            var data = JSON.parse(result);
            if (data !== null)
            {
                document.getElementById("email").value = data.email;
                document.getElementById("name").value = data.name;
                document.getElementById("cl_add").value = data.address;
                document.getElementById("cl_country").value = data.country;
                document.getElementById("cl_pc").value = data.post_code;
                document.getElementById("cl_city").value = data.city;
            }
            else
            {
                document.getElementById("email").value = "";
                document.getElementById("name").value = "";
                document.getElementById("cl_add").value = "";
                document.getElementById("cl_country").value = "United Kingdom";
                document.getElementById("cl_pc").value = "";
                document.getElementById("cl_city").value = "";
            }
        });
    }


</script>
<script>
    function addFile() {
        var html = '<?php echo $form->fileField($model, 'reciept_file[]', array("id" => "files", "multiple" => true, "class" => "btn green btn-lg dropdown-toggle", "style" => "margin-top:5px;")); ?>';
        jQuery("#file_upload").append(html);
    }
    function addItem() {
        var count = document.getElementById("count");
        var countVal = count.value;
        countVal++;
        var table = document.getElementById("myTable");

        // Create an empty <tr> element and add it to the 1st position of the table:
        var row = table.insertRow(count.value);
        // Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        var item = document.createElement("INPUT");
        var i = 'expense_name';
        var itemName = i.concat(countVal);
        item.setAttribute("type", "text");
        item.setAttribute("name", itemName);
        item.setAttribute("class", "form-control");
        item.setAttribute("onfocusout", "getTotal()")
        var itemValue = document.createElement("INPUT");
        i = 'expense_value';
        itemName = i.concat(countVal);
        itemValue.setAttribute("type", "text");
        itemValue.setAttribute("name", itemName);
        itemValue.setAttribute("id", itemName);
        itemValue.setAttribute("class", "form-control");
        itemValue.setAttribute("onfocusout", "getTotal()");
        itemValue.setAttribute("onchange", "getTotal()");
        itemValue.setAttribute("onkeypress", "return isNumberKey(event);")

        var rate = document.createElement("INPUT");
        i = 'comment';
        itemName = i.concat(countVal);
        rate.setAttribute("type", "text");
        rate.setAttribute("name", itemName);
        rate.setAttribute("class", "form-control");
        rate.setAttribute("onfocusout", "getTotal()");
        cell1.appendChild(item);
        cell2.appendChild(itemValue);
        cell3.appendChild(rate);
        var i = document.createElement("i");
        i.setAttribute("class", "fa fa-times");
        var anchor = document.createElement("a");
        var anchorName = 'anchor'.concat(countVal);
        anchor.setAttribute("name", anchorName);
        anchor.setAttribute("href", "javascript:return false;");
        anchor.setAttribute("onclick", "$(this).closest('tr').remove(); $('#count').val($('#count').val()-1);");
        anchor.appendChild(i);
        anchor.setAttribute("id", "close");
        cell4.appendChild(anchor);
        cell1.className = 'col-md-2';
        cell2.className = 'col-md-2';
        cell3.className = 'col-md-2';
        cell1.style.padding = '10px 15px 0 0';
        cell2.style.padding = '10px 15px 0 0';
        cell3.style.padding = '10px 15px 0 0';
        count.value = countVal;
    }
</script>
<?php $this->endWidget(); ?>

<script type="text/javascript">
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
</script>