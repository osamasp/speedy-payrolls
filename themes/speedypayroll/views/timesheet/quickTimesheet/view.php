<?php
/* @var $this QuickTimesheetController */
/* @var $model QuickTimesheet */

$this->breadcrumbs=array(
	'Quick Timesheets'=>array('index'),
	$model->id,
);
?>

<div class="box-header">
    <h3 class="box-title">View Oneway Timesheet</h3>
</div>
<div class="table-responsive">
    <div class="box-body">
        <?php
        $this->widget('zii.widgets.CDetailView', array(
            'data' => $model,
            'attributes' => array(
                'job_title',
                'job_reference',
                'job_description',
                array('name' => 'Hours', 'value' => date('H:i',$model->hours)),
                array('name' => 'Date', 'value' => date('d/m/Y',$model->date)),
                'status',
                array('name'=>'Client','value'=>$contact->name),
                array('name'=>'Currency' , 'value'=>$currency->name),
                'total',
            ),
        ));
        ?>
        <table class="table"><tr class="even"><td><b>Receipts: </b></td><td><?php if(count($files)>0){ echo AppInterface::getUploadedReceipts($files, 'Receipts' , 'reciept_file' , 'receipts',TRUE);}?></td></tr></table>
    </div>
</div>