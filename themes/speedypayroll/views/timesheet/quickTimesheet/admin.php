<?php
/* @var $this QuickTimesheetController */
/* @var $model QuickTimesheet */

$this->breadcrumbs=array(
	'Quick Timesheets'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List QuickTimesheet', 'url'=>array('index')),
	array('label'=>'Create QuickTimesheet', 'url'=>array('create')),
);
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/timesheet-quicktimesheet-admin.js"></script>
<div class="box-header">
    <h3 class="box-title">Manage Quick Timesheets</h3>
</div>
<div class="box-body">
<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'quick-timesheet-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'hours',
		'rate',
		'contact_id',
		'date',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
</div>