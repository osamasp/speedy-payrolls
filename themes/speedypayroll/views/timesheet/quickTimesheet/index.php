<?php
/* @var $this QuickTimesheetController */
/* @var $dataProvider CActiveDataProvider */
$id = 0;
$this->breadcrumbs = array(
    'Quick Timesheets',
);
$not_included_fields = array('id', 'password', 'first_login', 'notif_seen_at', 'token', 'validity', 'reason', 'modified_by', 'created_by', 'modified_at', 'contract_id', 'created_at', 'status', 'contact_id', 'comment', 'currency_id', 'company_id');
?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'myForm',
    'enableAjaxValidation' => false,
        ));
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/quicktimesheet-main.js"></script>
<input type="hidden" id="select_msg" value="<?php echo ConstantMessages::$req_select; ?>">
<input type="hidden" id="card_url" value="<?php echo $this->createUrl("/timesheet/quickTimesheet/getView"); ?>"/>
<input type="hidden" id="url" value="<?php echo $this->createUrl("/timesheet/quickTimesheet/getView"); ?>"/>
<input type="hidden" id="select_msg" value="<?php echo ConstantMessages::$req_select; ?>">
<input type="hidden" name="status" id="status" value="Open"/>

<!-- BEGIN CONTENT -->
<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                Widget settings form goes here
            </div>
            <div class="modal-footer">
                <button type="button" class="btn blue">Save changes</button>
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
<!-- BEGIN PAGE HEADER-->
<!-- BEGIN PAGE HEAD -->
<!--<div class="page-head">
     BEGIN PAGE TITLE 
    <div class="page-title">
        <h1>Current 1 Way Timesheet</h1>
    </div>
     END PAGE TITLE 
</div>-->
<!-- data table starts -->
<input type="hidden" id="export_url" value="<?php echo $this->createUrl('export'); ?>">
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/dropdowns-enhancement.css" rel="stylesheet">
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box grey-cascade">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-arrow-right"></i>Current 1 Way Timesheets
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                    <script>
                        $(document).ready(function () {
//                            $('.dropdown-toggle').dropdown();
                        });
                    </script>
                </div>
            </div>
            <div class="portlet-body table-responsive">
                <div class="table-toolbar">
                    <div class="row">

                        <div class="col-md-12">

                            <div class="btn-group pull-right" style="padding-left:10px;">
                                <!--<a class="btn btn-success pull-right btn-circle" href="<?php // echo $this->createUrl('add');   ?>">Add Employee</a>-->
                            </div>
                            <?php if (AppUser::isUserAdmin()) { ?>
                                <div class="btn-group pull-right">
                                    <a href="javascript:void(0);" class="btn green dropdown-toggle" id="openButton">Export <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul id="myDropdown" class="dropdown-menu pull-right">				
                                        <li><a id="export" href="javascript:void(0);" onclick='exportData("export")'>Export Selected Record</a></li>
                                        <li class="divider"></li>

                                        <li><a  id="export_all" href="javascript:void(0);" onclick='exportData("export_all")'>Export All Record</a></li>
                                        <?php if (isset($dataProvider[0])) { ?>
                                        <li><input type="checkbox" id="All" value="1"> <label id='selectAll' for="All">Select All</label></li>

                                        <?php
                                        foreach ($dataProvider[0] as $key => $item) {
                                            if (!in_array($key, $not_included_fields)) {
                                                ?>
                                                <li><input class="select" type="checkbox" name="export_fields[]" id="<?php echo $key; ?>" value="<?php echo $key; ?>"><label for="<?php echo $key; ?>"><?php echo QuickTimesheet::model()->getAttributeLabel($key); ?></label></li>
                                            <?php
                                            } 
                                        } ?>
                                                <li><input class="select" type="checkbox" name="export_fields[]" id="name" value="name"><label for="name">Client Name</label></li>
                                                <li><input class="select" type="checkbox" name="export_fields[]" id="email" value="email"><label for="email">Client Email</label></li>
                                                <li><input class="select" type="checkbox" name="export_fields[]" id="hourly_total" value="hourly_total"><label for="hourly_total">Hourly Total</label></li>
                                                <li><input class="select" type="checkbox" name="export_fields[]" id="expense_details" value="expense_details"><label for="expense_details">Expense Details</label></li>
                                       <?php }
                                        ?>
                                    </ul>
                                </div>
<?php } ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="btn-group pull-right">
                            <!--<a class="btn btn-primary btn-circle" href="<?php echo $this->createUrl('export', array('status' => 'Open')); ?>" >Export</a>-->
                        </div>
                    </div>
                    <table class="table table-striped table-bordered table-hover" id="example1">
                        <thead>
                            <tr>
                                <th class="table-checkbox">
                                    <input type="checkbox" data-checkbox="true" class="group-checkable" id="checkAll" onchange="" data-set="#sample_1 .checkboxes"/>
                                </th>
                                <th>
                                    Client
                                </th>
                                <th>
                                    Date
                                </th>
                                <th>
                                    Matter
                                </th>
                                <th>
                                    Activity
                                </th>
<?php // if(AppUser::isUserAdmin()){   ?>
    <!--                                    <th>
                                    Employer
                                </th>-->
<?php // }    ?>
                                <th>
                                    Hours
                                </th>
                                <th>
                                    Rate
                                </th>
                                <th>
                                    Expense
                                </th>
                                <th>
                                    Total
                                </th>
                                <th>
                                    Action
                                </th>

                            </tr>
                        </thead>
                        <tbody>
<?php foreach ($dataProvider as $item) { ?>
                                <tr>
                                    <td><input type="checkbox" name="entry[]" class="check" value="<?php echo $item->id; ?>"/></td>
                                    <td><?php echo (isset(Contact::model()->findByPk($item->contact_id)->name)) ? Contact::model()->findByPk($item->contact_id)->name : "-"; ?></td>
                                    <td><?php echo date('d/m/Y', $item->date); ?></td>
                                    <?php // if(AppUser::isUserAdmin()){   ?>
                                    <!--<td><?php // echo User::model()->findByPk($item->created_by)->first_name; ?> </td>-->
    <?php // }    ?>
                                    <td><?php echo $item->job_title; ?></td>
                                    <td><?php echo $item->job_reference; ?></td>
                                    <td><?php echo date('H:i', $item->hours); ?></td>
                                    <td><?php echo $item->rate; ?></td>
                                    <td>
                                        <?php
                                        $expenses = QuickTsExpenses::model()->findAllByAttributes(array('quick_time_id' => $item->id));
                                        $expense_total = 0;
                                        foreach ($expenses as $expense)
                                            $expense_total += $expense->expense_value;
                                        echo $expense_total;
                                        ?>
                                    </td>
                                    <td><?php echo $item->total; ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <button class="btn green dropdown-toggle" data-toggle="dropdown">Actions <i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a id="oneway" data-toggle="modal" data-target=".ajaxModel" data-whatever="<?php echo $item->id; ?>" >View</a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo $this->createUrl('/timesheet/quickTimesheet/update/id/' . $item->id); ?>">
                                                        Update</a>
                                                </li>
                                                <li>
                                                    <a id="invoice" data-toggle="modal" data-target=".Model" data-whatever="<?php echo $this->createUrl('/timesheet/quickTimesheet/invoice/id/' . $item->id); ?>">       
                                                        Invoice</a>
                                                </li>

                                            </ul></div></td>
                                </tr>
<?php } ?>
                        </tbody>
                    </table>

                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>      <!-- /data table ends -->
    </div>
</div>
<!-- END PAGE CONTENT-->
<!-- END CONTENT -->
<?php $this->endWidget(); ?>
<script>
    $('#selectAll').click(function(event) {  //on click 
        if ($("#All").is(":checked")) { // check select status
            $('.select').each(function() { //loop through each checkbox
                this.checked = false;  //select all checkboxes with class "checkbox1"               
            });
        } else {

            $('.select').each(function() { //loop through each checkbox
                this.checked = true; //deselect all checkboxes with class "checkbox1"                       
            });
        }
    });
    $('#openButton').on('click', function(event) {
        $(this).parent().toggleClass('open');
    });
</script>
