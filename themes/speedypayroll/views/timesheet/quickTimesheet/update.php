<?php
/* @var $this QuickTimesheetController */
/* @var $model QuickTimesheet */

$this->breadcrumbs=array(
	'Oneway Timesheets'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
?>
<?php $this->renderPartial('_form', array('model'=>$model,'contacts' => $contacts,'files'=>$files,'currencies' => $currencies, 'company' => null,'contact_details' => $contact_details,'expenses' => $expenses)); ?>