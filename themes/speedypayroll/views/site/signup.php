<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle = Yii::app()->name . ' - Signup';
$this->breadcrumbs = array(
    'Signup',
);
?>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/site-signup.js" type="text/javascript"></script>
<?php if (Yii::app()->user->hasFlash('message'))  ?>
<div class="row">
    <div class="regwizard">
        <div class="portlet box blue" id="form_wizard_1">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-sign-in"></i> Speedy Registration - <span class="step-title">
                        Step 1 of 4 </span>
                </div>
            </div>
            <div class="portlet-body form">
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'login-form',
                    'enableClientValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                    ),
                ));
                ?>
                <div class="form-wizard">
                    <div class="form-body">
                        <ul class="nav nav-pills nav-justified steps">
                            <li class="active">
                                <a href="#tab1" data-toggle="tab" class="step">
                                    <span class="number">
                                        1 </span>
                                    <span class="desc">
                                        <i class="fa fa-check"></i> Account Type </span>
                                </a>
                            </li>
                            <li>
                                <a href="#tab2" data-toggle="tab" class="step">
                                    <span class="number">
                                        2 </span>
                                    <span class="desc">
                                        <i class="fa fa-check"></i> Your Details </span>
                                </a>
                            </li>
                            <li>
                                <a href="#tab3" data-toggle="tab" class="step active">
                                    <span class="number">
                                        3 </span>
                                    <span class="desc">
                                        <i class="fa fa-check"></i> Company Details </span>
                                </a>
                            </li>
                            <li>
                                <a href="#tab4" data-toggle="tab" class="step">
                                    <span class="number">
                                        4 </span>
                                    <span class="desc">
                                        <i class="fa fa-check"></i> Set Payrates </span>
                                </a>
                            </li>
                        </ul>
                        <div id="bar" class="progress progress-striped" role="progressbar">
                            <div class="progress-bar progress-bar-success" style="width:25%">
                            </div>
                        </div>
                        <div class="tab-pane active" id="tab1">
                            <!--row-->	
                            <div class="row">
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="well">
                                        <!--<button type="button" class="" data-dismiss="alert"></button>-->
                                        <h4 class="alert-heading">Option 1</h4>
                                        <p>
                                            Register as a company here to use timesheet and staff managment solution. 
                                        </p>
                                        <p>
                                            <a class="btn green btn-lg button-next" href="javascript:selection(0);">
                                                <input type="radio" value="c" name="account_type" id="companySelect" checked="checked"/>    Register as a Company </a>
                                        </p>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="well">
                                        <!--<button type="button" class="" data-dismiss="alert"></button>-->
                                        <h4 class="alert-heading">Option 2</h4>
                                        <p>
                                            Internal payroll departments complete your registration as payroller first, then register as a company.
                                        </p>
                                        <p>
                                            <a class="btn blue btn-lg button-next" href="javascript:selection(1);">
                                                <input type="radio" name="account_type" value="p" id="payrollerSelect"/>    Register as Payroller</a>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group has-feedback">
                                        <input type="text" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="E.g:example@abc.com" class="form-control" placeholder="Email" required="required">
                                        <span class="glyphicon glyphicon-envelope form-control-feedback" style="top:12px;"></span>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <input type="password" name="password" pattern=".{6,}" title="Six or more characters" class="form-control" placeholder="Password" required="required">
                                        <span class="glyphicon glyphicon-lock form-control-feedback" style="top:12px;"></span>
                                    </div>
                                </div>
                                <div class="col-xs-8"> 
                                    <label class="text-danger"><?php echo Yii::app()->user->getFlash('message'); ?></label>
                                </div>
                            </div>
                            <!--/row-->	
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-5 col-md-12">
                            <input type="submit" name="submit" value="Continue" class="btn blue button-next"/>
                                <i class="m-icon-swapright m-icon-white"></i>
                        </div>
                    </div>
                </div>

                <?php $this->endWidget(); ?>           
            </div>
        </div>
    </div>
</div>
