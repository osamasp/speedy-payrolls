<?php
/* @var $this SiteController */
/* @var $error array */
$message = Yii::app()->user->getFlash('error');
$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);
?>
<!--<div class="form" style="margin: 0 auto;width: 50%;">
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-exclamation-triangle"></i>Page Not Found
        </div>
    </div>
    <div class="portlet-body form">-->
<!--        <div class="row" style="padding-top:10px;">
        </div>
        <div style="padding-left:20px;padding-right:20px;">
            <div class="form-group" style="text-align: center;">
                <span class="title" style="font-size: 22px;font-family: 'Open Sans',sans-serif">Oops!!! Something went wrong</span>
            </div>
            <div class="form-group" style='text-align: center;'>
                <h1 style="font-size: 30px;font-family: 'Open Sans',sans-serif; font-weight: bolder">404</h1>
            </div>
            <div class="form-group" style="text-align:center;">
<div class="error">
    <span style="font-size: 22px;font-family: 'Open Sans',sans-serif">Page Not Found</span>
</div>
            </div>
        </div>-->
<div style="width:97%">
    <div class="page-inner">
            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/earth.jpg" class="img-responsive" alt=""> 
    </div>
<div class="container error-404">
            <h1>404</h1>
            <h2>Houston, we have a problem.</h2>
            <p> Actually, the page you are looking for does not exist. </p>
        </div>
    <div class="form-actions">
            <div class="row" style="text-align:center;">
                    <a href="<?php echo Yii::app()->createUrl('/site/index');?>" class="btn btn-primary">Return home</a>
                    <a href="javascript:parent.history.back();" class="btn btn-default" style="margin-left:20px;">Go back</a>
            </div>
    </div>
</div>
        
<!--    </div>
</div>
</div>-->