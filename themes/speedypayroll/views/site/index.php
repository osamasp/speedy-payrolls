<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name;
?>
<!-- Chart Plugins -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/amcharts/3.13.0/amcharts.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/amcharts/3.13.0/serial.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/amcharts/3.13.0/pie.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/amcharts/3.13.0/radar.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/amcharts/3.13.0/themes/light.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/amcharts/3.13.0/themes/patterns.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/amcharts/3.13.0/themes/chalk.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ammaps/3.13.0/ammap.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ammaps/3.13.0/maps/js/worldLow.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/amstockchart/3.13.0/amstock.js" type="text/javascript"></script>
<!-- End Chart Plugins -->
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/css/pages/scripts/charts-amcharts.js"></script>
<script>
        jQuery(document).ready(function() {
        ChartsAmcharts.init();
    });
</script>
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Modal title</h4>
                        </div>
                        <div class="modal-body">
                            Widget settings form goes here
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn blue">Save changes</button>
                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE HEAD -->
<!--            <div class="page-head">
                 BEGIN PAGE TITLE 
                <div class="page-title">
                    <h1>Speedy Dashboard<small></small></h1>
                </div>
                 END PAGE TITLE 
                 BEGIN PAGE TOOLBAR 
                
                 END PAGE TOOLBAR 
            </div>
             END PAGE HEAD 
             BEGIN PAGE BREADCRUMB 
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="javascript:;">Home</a>
                    <i class="fa fa-circle"></i>
                </li>

            </ul>-->
            <!-- END PAGE BREADCRUMB -->
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->

            <!-- BEGIN PAGE CONTENT INNER -->
            <div class="row margin-top-10">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="dashboard-stat2">
                        <div class="display">
                            <div class="number">
                                <h3 class="font-green-sharp"><?php if($total_hours==0){ echo "0";} else{ echo date('h', $total_hours);} ?></h3>
                                <small>HOURS THIS WEEK</small>
                            </div>
                            <div class="icon">
                                <i class="icon-pie-chart"></i>
                            </div>
                        </div>
                        <div class="progress-info">
                            <div class="progress">
                                <span style="width: <?php if($total_hours==0){ echo "0";} else{ echo date('h', $total_hours); } ?>%;" class="progress-bar progress-bar-success green-sharp">
                                    <span class="sr-only">76% APPROVED</span>
                                </span>
                            </div>
                            <div class="status">
                                <div class="status-title">
                                    progress
                                </div>
                                <div class="status-number">
                                    <?php if($total_hours==0){ echo "0";} else{ echo date('h', $total_hours);} ?>%
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="dashboard-stat2">
                        <div class="display">
                            <div class="number">
                                <h3 class="font-red-haze"><?php if($timesheets==0){ echo "0";} else{ echo count($timesheets);} ?></h3>
                                <small>TIMESHEETS</small>
                            </div>
                            <div class="icon">
                                <i class="icon-like"></i>
                            </div>
                        </div>
                        <div class="progress-info">
                            <div class="progress">
                                <span style="width: <?php if($timesheets==0){ echo "0";} else{ echo count($timesheets);} ?>%;" class="progress-bar progress-bar-success red-haze">
                                    <span class="sr-only">10% change</span>
                                </span>
                            </div>
                            <div class="status">
                                <div class="status-title">
                                    change
                                </div>
                                <div class="status-number">
                                    <?php if($timesheets==0){ echo "0";} else{ echo count($timesheets);} ?>%
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="dashboard-stat2">
                        <div class="display">
                            <div class="number">
                                <h3 class="font-blue-sharp"><?php if($epayslips==0){ echo "0";} else{ echo count($epayslips);} ?></h3>
                                <small>epayslips</small>
                            </div>
                            <div class="icon">
                                <i class="icon-basket"></i>
                            </div>
                        </div>
                        <div class="progress-info">
                            <div class="progress">
                                <span style="width: <?php if($epayslips==0){ echo "0";} else{ echo count($epayslips);} ?>%;" class="progress-bar progress-bar-success blue-sharp">
                                    <span class="sr-only">15% change</span>
                                </span>
                            </div>
                            <div class="status">
                                <div class="status-title">
                                    change
                                </div>
                                <div class="status-number">
                                    <?php if($epayslips==0){ echo "0";} else{ echo count($epayslips);} ?>%
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="dashboard-stat2">
                        <div class="display">
                            <div class="number">
                                <h3 class="font-purple-soft"><?php if($users==0){ echo "0";} else{ echo count($users);} ?></h3>
                                <small>USERS</small>
                            </div>
                            <div class="icon">
                                <i class="icon-user"></i>
                            </div>
                        </div>
                        <div class="progress-info">
                            <div class="progress">
                                <span style="width: <?php if($users==0){ echo "0";} else{ echo count($users);} ?>%;" class="progress-bar progress-bar-success purple-soft">
                                    <span class="sr-only"></span>
                                </span>
                            </div>
                            <div class="status">
                                <div class="status-title">
                                    users
                                </div>
                                <div class="status-number">
                                    <?php if($users==0){ echo "0";} else{ echo count($users);} ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <div class="row">
                <div class="col-md-12">


                    <!-- BEGIN ROW -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN CHART PORTLET-->
                            <div class="portlet light">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-bar-chart font-green-haze"></i>
                                        <span class="caption-subject bold uppercase font-green-haze"> Hours Worked</span>
                                        <span class="caption-helper">with day breaks</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="chart_3" class="chart" style="height: 400px;">
                                    </div>
                                </div>
                            </div>
                            <!-- END CHART PORTLET-->
                        </div>
                    </div>
                    <!-- END ROW -->


                    <!-- BEGIN ROW -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN CHART PORTLET-->
                            <div class="portlet light">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-bar-chart font-green-haze"></i>
                                        <span class="caption-subject bold uppercase font-green-haze"> GPS Tracking (coming soon...)</span>
                                        <span class="caption-helper">showing staff timesheet input locations</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="chart_11" class="chart" style="height: 500px;">
                                    </div>
                                </div>
                            </div>
                            <!-- END CHART PORTLET-->
                        </div>
                    </div>
                    <!-- END ROW -->

                </div>
            </div>
            <!-- END PAGE CONTENT-->