<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);
?>
<!--<div class="form" style="margin: 0 auto;width: 50%;">
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Access Denied
        </div>
    </div>
    <div class="portlet-body form">
        <div class="row" style="padding-top:10px;">
        </div>
        <div style="padding-left:20px;padding-right:20px;">
            <div class="form-group" style="text-align:center;">
                <?php // if(config_settings::$environment == "development"){ ?>
<?php if($code==403){
    $message = Yii::app()->user->getFlash('error');
    ?>
                <img src="<?php // echo Yii::app()->theme->baseUrl;?>/img/lock-icon.png">
<div class="error"><?php // echo $message ?>
</div>
<?php } 
//else{ ?>
<div class="error">
<?php // echo CHtml::encode($message); ?>
</div>
                <?php // } } else { ?>
                    <img src="<?php // echo Yii::app()->theme->baseUrl;?>/img/too-far.png" style="width:100%; height:auto;">
                <?php // } ?>
            </div>
        </div>
        <div class="form-actions">
            <div class="row" style="text-align:center;">
                    <a href="<?php // echo Yii::app()->createUrl('/site/index');?>" class="btn btn-primary">Return home</a>
                    <a href="javascript:parent.history.back();" class="btn btn-default" style="margin-left:20px;">Go back</a>
            </div>
        </div>
    </div>
</div>
</div>-->

<div style="width:97%">
    <div class="page-inner">
            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/earth.jpg" class="img-responsive" alt=""> 
    </div>
<div class="container error-404">
            <h1>403</h1>
            <h2>Houston, we have a problem.</h2>
            <p> <?php echo $message; ?> </p>
        </div>
    <div class="form-actions">
            <div class="row" style="text-align:center;">
                    <a href="<?php echo Yii::app()->createUrl('/site/index');?>" class="btn btn-primary">Return home</a>
                    <a href="javascript:parent.history.back();" class="btn btn-default" style="margin-left:20px;">Go back</a>
            </div>
    </div>
</div>