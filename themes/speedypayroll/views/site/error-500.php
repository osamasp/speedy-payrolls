<?php
/* @var $this SiteController */
/* @var $error array */
$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);
?>
<div class="form" style="margin: 0 auto;width: 50%;">
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-exclamation-triangle"></i>Internal Server Error
        </div>
    </div>
    <div class="portlet-body form">
        <div class="row" style="padding-top:10px;">
        </div>
        <div style="padding-left:20px;padding-right:20px;">
            <div class="form-group" style="text-align: center;">
                <span class="tile" style="font-size: 22px;font-family: 'Open Sans',sans-serif">Oops!!! Something went wrong</span>
            </div>
            <div class="form-group" style='text-align: center;'>
                <h1 style="font-size: 30px;font-family: 'Open Sans',sans-serif; font-weight: bolder">500</h1>
            </div>
            <div class="form-group" style="text-align:center;">
<div class="error">
    <span style="font-size: 22px;font-family: 'Open Sans',sans-serif">Internal Server Error</span>
</div>
        </div>
    </div>
        <div class="form-actions">
            <div class="row" style="text-align:center;">
                    <a href="<?php echo Yii::app()->createUrl('/site/index');?>" class="btn btn-primary">Return home</a>
                    <a href="javascript:parent.history.back();" class="btn btn-default" style="margin-left:20px;">Go back</a>
            </div>
        </div>
</div>
</div>
</div>