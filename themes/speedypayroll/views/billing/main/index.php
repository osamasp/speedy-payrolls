<?php
/* @var $this MainController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Companies',
);

$this->menu = array(
    array('label' => 'Create Company', 'url' => array('create')),
    array('label' => 'Manage Company', 'url' => array('admin')),
);
?>
<div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa  fa-dollar "></i>Billing
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>

                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">

                        <div class="col-md-12">
                            
                        </div>
                    </div>
                </div>
<div class="box-header">
    <h3 class="box-title">Companies</h3>
</div>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'billing-form',
    'enableAjaxValidation' => false,
        ));
?>

<div class="box-body table-responsive">
    <table id="example1" class="table table-responsive table-striped table-bordered" id="example1" >
        <thead>
            <tr>
                <th></th>
                <th>Company Name</th>
                <th>Location</th>
                <th>No. of Employees</th>
                <th>TS</th>
                <th>PS</th>
                <th>Billing Date</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th colspan="8">
        <div class="btn-group">
            <input type="submit" value="Reset TS Count" name="timesheet" class="btn btn-primary" />
        </div>
        <div class="btn-group">
            <input type="submit" value="Reset PS Count" name="payslip" class="btn btn-primary" />
        </div>
        </th>
        </tr>
        </tfoot>
        <tbody>
            <?php foreach ($companies as $item) { ?>
                <tr>
                    <td><input type="checkbox" name="reset[<?php echo $item->id; ?>]" class="form-control" ></td>
                    <td><?php echo $item->name; ?></td>
                    <td><?php echo $item->country; ?></td>
                    <td><?php echo AppCompany::getCompanyInteralUserCount($item->id); ?></td>
                    <td><?php echo AppCompany::getTimesheetCount($item); ?></td>
                    <td><?php echo AppCompany::getPayslipCount($item); ?></td>
                    <td><?php echo get_placing_string($item->billing_date); ?></td>
                    <td>
                        <div class="btn-group">
                                <button class="btn green dropdown-toggle" data-toggle="dropdown">Actions <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                <li><a href="<?php echo $this->createUrl('edit', array('id' => $item->id)); ?>">Edit</a></li>
                            </ul>
                        </div>
                        </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<?php $this->endWidget(); ?>
            </div>
</div>