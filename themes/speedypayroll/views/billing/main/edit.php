<?php
/* @var $this MainController */
/* @var $company Company */

$this->breadcrumbs = array(
    'Companies' => array('index'),
    $company->name,
);
?>
<div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-globe"></i>Edit Billing
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>

                </div>
            </div>
            <div class="portlet-body form" style="padding-left:20px !important;padding-right:20px !important;">
                <div class="table-toolbar">
                    <div class="row">

                        <div class="col-md-12">
                            
                        </div>
                    </div>
                </div>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'billing-form',
    'enableAjaxValidation' => false,
        ));
?>

<div class="box-header">
    <h3 class="box-title">Edit Billing Details of "<?php echo $company->name; ?>"</h3>
</div>
<div class="box-body" style="padding-left:10px;">
    <div class="row">
        <div class="col-lg-4">
            <label>Monthly Billing Date</label>
            <select class="form-control" name="Company[billing_date]" required="">
                <option value="">Select Day</option>
                <?php for ($i = 1; $i <= 28; $i++) { ?>
                    <option value="<?php echo $i; ?>" <?php echo $company->billing_date == $i ? 'selected' : ''; ?>><?php echo get_placing_string($i) ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="col-lg-8">
            <label>Contract Note</label>
            <textarea name="Company[contract_note]" class="form-control" rows="5" ><?php echo $company->contract_note; ?></textarea>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <input type="submit" name="save" value="save" class="btn btn-primary">
            <hr>
        </div>
    </div>
</div>
<div class="box-header">
    <h3 class="box-title">Details of "<?php echo $company->name; ?>"</h3>
</div>
<div class="box-body" style="padding-left:10px;">
    <?php
    $this->widget('zii.widgets.CDetailView', array(
        'data' => $company,
        'attributes' => array(
            'name',
            'email',
            'phone',
            'sector',
            'registration_number',
            'vat_number',
            'address',
            'street',
            'city',
            'country',
            'post_code',
        ),
    ));
    ?>    
</div>

<?php $this->endWidget(); ?>
            </div>
</div>