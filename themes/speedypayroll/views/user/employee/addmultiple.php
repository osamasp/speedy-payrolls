<div class="portlet box blue" id="form_wizard_1">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-users"></i> Add Multiple Employee </span>
        </div>
   </div>
    <div class="portlet-body form">


        <?php
        /*
         * To change this license header, choose License Headers in Project Properties.
         * To change this template file, choose Tools | Templates
         * and open the template in the editor.
         */


        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'user-form',
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
        ?>
<!--        <div class="box-header">
            <h3 class="box-title">Add Multiple Employees</h3>
        </div>-->
<div class="box-body note note-info" style="padding-bottom: 0px; margin-bottom: 0px;">
<!--            <div class ="col-sm-12">
                <p style="font-family: Helvetica, Geneva, Arial,SunSans-Regular, sans-serif">
                    <br></br>
                    <b>
                        Please read the following guidelines: <br>
                        1. Employment Type should be 'full_time' or 'part_time'.<br>
                        2. Start and End Date should be in 'dd/mm/yyyy' format.<br>
                        3. Collection Frequency should be 'weekly' or 'monthly'. For 'monthly' selection first or last of the specific day should be mentioned as 'first' or 'last', for 'weekly' left blank.<br>
                        4. Email address should be unique.
                    </b>
                </p>

            </div>-->
            <div>
                <div class="alert alert-block alert-info fade in">
                    <p style="font-family: Helvetica, Geneva, Arial,SunSans-Regular, sans-serif">
                    <b>
                        Please read the following guidelines: <br>
                        1. Employment Type should be 'full_time' or 'part_time'.<br>
                        2. Start and End Date should be in 'dd/mm/yyyy' format.<br>
                        3. Collection Frequency should be 'weekly' or 'monthly'. For 'monthly' selection first or last of the specific day should be mentioned as 'first' or 'last', for 'weekly' left blank.<br>
                        4. Email address should be unique.
                    </b>
                    </p>
                <div class="form-group col-sm-12 ">

                    <a class="btn blue hidden-xs btn-circle" href="<?php echo Yii::app()->createAbsoluteUrl('user/employee/downloadtemplate'); ?>" >Download Template</a>   
             
                </div>
            <div class="clearfix"></div>  
                </div>
                <div class="form-group col-sm-3">
                Upload Employees through .csv file
                <input type="file" name="file_up" id="file_up"/>
            </div>
             <div class="clearfix"></div>   
                
                
            </div>

            
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-9">
                    <input type="submit" value="Submit" class="btn btn-circle green-haze" style="float: right;" >
                </div>
            </div></div>

        <div class="clearfix"></div>
        <?php $this->endWidget(); ?>
    </div>
</div>