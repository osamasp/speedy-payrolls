<?php
/* @var $this MainController */
/* @var $item User */

$this->breadcrumbs = array(
    'Users',
);

$this->menu = array(
    array('label' => 'Create User', 'url' => array('create')),
    array('label' => 'Manage User', 'url' => array('admin')),
);
$id = 0;
$not_included_fields = array('id', 'password', 'created_at', 'created_by', 'modified_at', 'modified_by', 'is_verified', 'verification_code', 'first_login', 'notif_seen_at', 'token', 'validity', 'plan');
//dd($approvers);
?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'myForm',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => false,
        ));
?>
<input type="hidden" id="select_msg" value="<?php echo ConstantMessages::$req_select; ?>">
<input type="hidden" id="url" value="<?php echo $this->createUrl('export'); ?>">
<input type="hidden" id="shift_url" value="<?php echo $this->createUrl('assignshift'); ?>">
<input type="hidden" id="supervisor_url" value="<?php echo $this->createUrl('assignsupervisor'); ?>">
<input type="hidden" id="payrate_url" value="<?php echo $this->createUrl('changepayrate'); ?>">
<input type="hidden" id="notification_url" value="<?php echo $this->createAbsoluteUrl('/notification/main/companynotification'); ?>">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/user-employee-index.js" type="text/javascript"></script>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/dropdowns-enhancement.css" rel="stylesheet">

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box grey-cascade">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-globe"></i>View Staff
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>

                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">

                        <div class="col-md-12">

                            <div class="btn-group pull-right" style="padding-left:10px;">
                                <!--<a class="btn btn-success pull-right btn-circle" href="<?php // echo $this->createUrl('add');   ?>">Add Employee</a>-->
                            </div>
                            <?php if (AppUser::isUserAdmin()) { ?>
                                <div class="btn-group pull-right">
    <!--                                    <a href="javascript:void(0);" class="btn green dropdown-toggle" id="openButton">Export <i class="fa fa-angle-down"></i>
                                    </a>-->
                                    <button class="btn green dropdown-toggle" id="openButton">Export <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul id="myDropdown" class="dropdown-menu pull-right">				
                                        <li><a href="javascript:void(0);"  onclick='exportData("export")'>Export Selected Record</a></li>
                                        <li class="divider"></li>
                                        <li><a href="javascript:void(0);" onclick='exportData("export_all")'>Export All Record</a></li>
                                        <?php if (isset($dataProvider[0])) { ?>
                                            <li><input type="checkbox" name="export_fields[]" id="All" value="1"> <label id="selectAll" for="All">Select All</label></li>
                                            <?php
                                            foreach ($dataProvider[0] as $key => $item) {
                                                if (!in_array($key, $not_included_fields)) {
                                                    ?>
                                                    <li><input class="select" type="checkbox" name="export_fields[]" id="<?php echo $key; ?>" 
                                                               value="<?php echo $key; ?>">
                                                        <label for="<?php echo $key; ?>"><?php echo User::model()->getAttributeLabel($key); ?></label></li>
                                                    <?php
                                                }
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
<?php } ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-actions">
                            <input type="button" id="change" value="Add New Employee + " class="btn green">
                            <!--<input type="button" value="Add New Employee + " id="hide" class="btn green">-->
                        </div>
                    </div>
                    <div class="spacer col-md-12">
                    </div>
                    <div class="col-md-8 hidden-xs hidden-sm">
                        <div class="form-actions">
                            <a href="<?php echo AppInterface::getAbsoluteUrl('user/employee/addmultiple'); ?>"><button type="button" class=" col-xs-hidden col-sm-hidden btn blue">Add Employees (bulk csv)</button></a>
                            <a style="text-decoration: none; color: white;" data-toggle="modal" href="#Assign_shift"><button type="submit" class="btn blue">Assign Shift</button></a>
                            <a style="text-decoration: none; color: #666;" data-toggle="modal" href="#Assign_supervisor"><button type="submit" class="btn default">Assign Supervisor</button></a>
                            <a style="text-decoration: none; color: white;" data-toggle="modal" href="#Payrates"><button id="add_cart" type="submit" class="btn blue">Change Payrates</button></a>
                            <button type="button" class="btn default" onclick="bulkNotification()">Bulk Notifications</button>
                        </div>
                    </div>
                </div>
                <ul id="output"></ul>
                <div class="box-body table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="example1">
                        <thead>
                            <tr>
                                <th class="table-checkbox">
                                    <input type="checkbox" data-checkbox="true" class="group-checkable" id="checkAll" onchange="" data-set="#sample_1 .checkboxes"/>
                                </th>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Email
                                </th>
                                <th>
                                    Staff ID
                                </th>
                                <th>
<?php echo User::model()->getAttributeLabel('ni_number') ?>
                                </th>
                                <th>
                                    Start Date
                                </th>
                                <th>
                                    Action
                                </th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr class="hide">
                                <?php
                                $form = $this->beginWidget('CActiveForm', array(
                                    'id' => 'user-form',
                                    'enableAjaxValidation' => false,
                                    'htmlOptions' => array('enctype' => 'multipart/form-data', 'class' => 'form-horizontal'),
                                ));
                                ?>
                                <td><input type="checkbox" class="check" style="width: 35px;"></td>
                                <td style="width: 152px;">
                                    <input name="formData[User][first_name]" required="" placeholder="First" type="text" style="width: 120px;">
                                    &nbsp;&nbsp; <br>
                                    <input name="formData[User][last_name]" required="" placeholder="Last" type="text" style="width: 120px;"></td>
                                <td style="width: 249px;"><input name="formData[User][email]" required="" type="Email" placeholder="email" style="width: 239px;"></td>
                                <td style="width: 120px;"><input name="formData[User][staff_id]" required="" placeholder="Staff ID" type="text" style="width: 120px;"></td>
                                <td  style="width: 120px;"><input name="formData[User][ni_number]" required="" placeholder="NI Number"type="text" style="width: 120px;"></td>
                                <td><input name="formData[Contract][start_time]" required="" placeholder="Start Time" type="text" id="start_date" style="width: 120px;"></td>
                        <input name="formData[Contract][collection_type]" hidden="" type="text" value="1">
                        <input name="formData[Settings][collection_day_first]" hidden="" type="text" value="1">
                        <input name="formData[User][password]" hidden="" type="text" value="<?php echo AppInterface::generateToken(); ?> ">
                        <td style="width: 100px;"><button name="submit" type="submit" class="btn blue dropdown-toggle">Save</button></td>
                        <?php $this->endWidget(); ?>
                        </tr>
                        <?php
                        foreach ($dataProvider as $item) {
                            ?>
                            <tr>
                                <td>
                                    <input type="checkbox" name="entry[]" class="check" value="<?php echo $item->id; ?>"  style="width: 35px;"/>
                                </td>
                                <td style="width: 152px;"><?php echo $item->full_name; ?></td>
                                <td style="width: 249px;"><?php echo $item->email ?></td>
                                <td style="width: 120px;"><?php echo $item->staff_id; ?></td>
                                <td style="width: 120px;"><?php echo $item->ni_number ? $item->ni_number : "-"; ?></td>
                                <td style="width: 120px;"><?php echo date(AppInterface::getdateformat(), $item->current_in_contract->start_time); ?></td>
                                <td>
                                    <div class="btn-group">
                                        <button class="btn green dropdown-toggle" data-toggle="dropdown">Actions <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-right">				
                                            <li><a href="<?php echo $this->createUrl('/user/main/update', array('id' => $item->id)); ?>">Edit Profile</a></li>
                                            <li class="divider"></li>
                                            <li><a href="<?php echo $this->createUrl('/contract/main/grid', array('id' => $item->id)); ?>">View Contract</a></li>                               
                                            <li class="divider"></li>
                                            <li><a href="<?php echo $this->createUrl('/user/main/userpassword', array('id' => $item->id)); ?>">Change Password</a></li>                               
                                        </ul>
                                    </div>
                                </td>
                            </tr>
<?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>      <!-- /data table ends -->
<?php $this->endWidget(); ?>
<script>
    $(document).ready(function() {

        $("#change").click(function() {
            $("tr").toggleClass("show");

        });

    });
</script>
<!--Shifts-->
<div class="modal fade" id="Assign_shift" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <div class="portlet-title">
                    <div class="caption">
                        <h3>Assign Shift</h3>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Please Select Any Shift</label>
                        <div class="col-md-6">
                            <div class="input-icon right">
                                <select name="Contract[collection_type]" id="assign_shift" class="form-control" id="collection_type">
                                    <option value="0">Select Option</option>
                                    <?php foreach ($shifts as $shift) {
                                        ?>
                                        <option value="<?php echo $shift->id; ?>">
                                        <?php echo $shift->title; ?></option>
<?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <a class="btn green btn-circle" onclick="assignShift()" style="width: 130px;">Submit</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<!--approvers-->
<div class="modal fade" id="Assign_supervisor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <div class="portlet-title">
                    <div class="caption">
                        <h3>Assign Supervisor</h3>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Please Select Any Supervisor</label>
                        <div class="col-md-6">
                            <div class="input-icon right">
                                <select name="Contract[collection_type]" id="assign_supervisor" class="form-control" id="collection_type">
                                    <option value="0">Select Option</option>
                                    <?php foreach ($approvers as $approver) {
                                        ?>
                                        <option value="<?php echo $approver->id; ?>">
                                        <?php echo isset($approver->first_name) ? $approver->first_name : ''; ?> <?php echo isset($approver->last_name) ? $approver->last_name : ''; ?></option>
<?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <button type="button" onclick="assignSupervisor()" class="btn green btn-circle" style="width: 130px;">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<!--Payrates-->
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'payrateForm',
    'enableAjaxValidation' => false,
        ));
?>
<div id="Payrates" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Offer Payrates to Staff</h4>
            </div>
            <div class="modal-body">
                <div class="scroller" style="height:350px" data-always-visible="1" data-rail-visible1="1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-sm-4">
                                <?php if ($payrates = AppCompany::getPayrateNames()) { ?>
                                    <h3>Payrates</h3>
                                    <?php foreach ($payrates as $payratename) { ?>
                                        <?php echo ucwords(str_replace('_', ' ', $payratename->name)); ?>
                                        <input type="text" id="Payrates" name="Payrate[<?php echo $payratename->name; ?>]" onkeypress="return isNumberKey(event);" class="form-control" placeholder="£/hr" required="required">
                                    <?php } ?>
                                <?php } ?>
                                <?php if ($items = AppCompany::getItems()) { ?>
                                    <h3>Items</h3>
                                    <?php foreach ($items as $payratename) { ?>
                                        <?php echo ucwords(str_replace('_', ' ', $payratename->name)); ?>
                                        <input type="text" id="Items" name="Item[<?php echo $payratename->name; ?>]" value='<?php echo $payratename->item_value; ?>' onkeypress="return isNumberKey(event);" class="form-control" placeholder="£" required="required">
                                    <?php } ?>
<?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn green btn-circle" style="width: 130px;" onclick="changePayrate()">Set Rates</button>
            </div>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>

<script>
    $('#selectAll').click(function(event) {  //on click 
        if ($("#All").is(":checked")) { // check select status
            $('.select').each(function() { //loop through each checkbox
                this.checked = false;  //select all checkboxes with class "checkbox1"               
            });
        } else {

            $('.select').each(function() { //loop through each checkbox
                this.checked = true; //deselect all checkboxes with class "checkbox1"                       
            });
        }
    });
    $('#openButton').on('click', function(event) {
        $(this).parent().toggleClass('open');
    });
</script>