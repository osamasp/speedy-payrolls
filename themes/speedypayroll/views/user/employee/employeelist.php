<?php

$this->breadcrumbs = array(
    'Users',
);

$this->menu = array(
    array('label' => 'Create User', 'url' => array('create')),
    array('label' => 'Manage User', 'url' => array('admin')),
);
?>

<div class="box-header">
    <h3 class="box-title">Users</h3>
    <div class="box-tools">
        <a class="btn btn-default pull-right" href="<?php echo $this->createUrl('add'); ?>">Add Employee</a>
      
    </div>
</div>

<div class="box-body table-responsive">
    <table id="example1" class="table table-responsive table-striped">
        <thead>
            <tr>
                <th>Name</th>
                <th class="hidden-xs hidden-sm"><?php echo User::model()->getAttributeLabel('ni_number') ?></th>
                <th class="hidden-xs hidden-sm">Employee Type</th>
                <th class="hidden-xs hidden-sm">Date Joined</th>
                <th class="hidden-xs hidden-sm">Date Ending</th>
                <th class="hidden-xs hidden-sm">Location</th>
                <th>Approver</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            foreach ($dataProvider as $item) { ?>
                <tr>
                    
                </tr>
            <?php } ?>
        </tbody>
    </table>
  </div>