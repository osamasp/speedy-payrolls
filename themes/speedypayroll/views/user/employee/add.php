
<?php
/* @var $this EmployeeController */

$this->breadcrumbs = array(
    'Employee',
);
?>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/user-employee-add.js" type="text/javascript"></script>
<input type="hidden" id="date_format" value="<?php echo AppInterface::getdateformat(true); ?>"/>
<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
<div class="modal fade" id="portlet-config" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                Widget settings form goes here
            </div>
            <div class="modal-footer">
                <button type="button" class="btn blue">Save changes</button>
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
<!-- BEGIN PAGE HEADER-->
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>Add New <small>staff management</small></h1>
    </div>
    <!-- END PAGE TITLE -->
</div>
<!-- END PAGE HEAD -->

<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->




<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue" id="form_wizard_1">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user-plus"></i> Add New Staff - <span class="step-title">
                        Step 1 of 4 </span>
                </div>
            </div>
            <div class="portlet-body form">
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'user-form',
                    // Please note: When you enable ajax validation, make sure the corresponding
                    // controller action is handling ajax validation correctly.
                    // There is a call to performAjaxValidation() commented in generated controller code.
                    // See class documentation of CActiveForm for details on this.
                    'enableAjaxValidation' => false,
                    'htmlOptions' => array('enctype' => 'multipart/form-data', 'class' => 'form-horizontal'),
                ));
                ?>
                <div class="form-wizard">
                    <div class="form-body">
                        <ul class="nav nav-pills nav-justified steps">
                            <li id="menuTab1" class="active">
                                <a href="#tab1" data-toggle="tab" class="step">
                                    <span class="number">
                                        1 </span>
                                    <span class="desc">
                                        <i class="fa fa-check"></i>Basic Info</span>
                                </a>
                            </li>
                            <li id="menuTab2">
                                <a href="#tab2" data-toggle="tab" class="step">
                                    <span class="number">
                                        2 </span>
                                    <span class="desc">
                                        <i class="fa fa-check"></i>Contract</span>
                                </a>
                            </li>
                            <li id="menuTab3">
                                <a href="#tab3" data-toggle="tab" class="step active">
                                    <span class="number">
                                        3 </span>
                                    <span class="desc">
                                        <i class="fa fa-check"></i>Configure</span>
                                </a>
                            </li>
                            <li id="menuTab4">
                                <a href="#tab4" data-toggle="tab" class="step active">
                                    <span class="number">
                                        4 </span>
                                    <span class="desc">
                                        <i class="fa fa-check"></i>Confirm</span>
                                </a>
                            </li>
                        </ul>
                        <div id="bar" class="progress progress-striped" role="progressbar">
                            <div class="progress-bar progress-bar-success" id="progress" style="width:25%">
                            </div>
                        </div>
                        <div class="tab-content">
                            <div id="errorDiv" class="alert alert-danger display-none">
                                <button class="close" data-dismiss="alert"></button>
                                <span>You have some form errors. Please check below.</span>
                            </div>
                            <div id="successDiv" class="alert alert-success display-none">
                                <button class="close" data-dismiss="alert"></button>
                                <span>Your form validation is successful!</span>
                            </div>




                            <!-- tab1 -->


                            <div class="tab-pane active" id="tab1">
                                <div class="row">
                                    <div class="col-md-12">


                                        <!-- BEGIN 1st tab FORM-->
                                        <div class="form-body">

                                            <?php if ($error) { ?>
                                                <div class="alert alert-danger display-none">
                                                    <button class="close" data-dismiss="alert"></button>
                                                    <?php echo $error; ?>
                                                </div>
                                            <?php } ?>   
                                            <div class="form-group">
                                                <div class="col-md-4">
                                                    Employee First Name
                                                    <?php echo $form->textField($user, 'first_name', array('placeholder' => 'First Name', 'class' => 'form-control', 'required' => 'required')); ?>
                                                </div>
                                                <div class="col-md-4">
                                                    Employee Last Name
                                                    <?php echo $form->textField($user, 'last_name', array('placeholder' => 'Last Name', 'class' => 'form-control', 'required' => 'required')); ?>
                                                </div>

                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-4">
                                                    Employee Email
                                                    <?php echo $form->textField($user, 'email', array('placeholder' => 'Email', 'class' => 'form-control', 'required' => 'required')); ?>
                                                </div>
                                                <div class="col-md-4">
                                                    Staff ID
                                                    <?php echo $form->textField($user, 'staff_id', array('placeholder' => 'Staff ID', 'class' => 'form-control', 'required' => 'required')); ?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-8">
                                                    <?php echo $form->labelEx($user, 'shift_id'); ?>
                                                    <?php echo CHtml::activeDropDownList($user, 'shift_id', $shifts, array('class' => 'form-control')); ?>
                                                    <?php echo $form->error($user, 'shift_id', array('class' => 'text-red')); ?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-4">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            Password
                                                            <?php echo $form->passwordField($user, 'password', array('placeholder' => 'New Password', 'class' => 'form-control', 'required' => 'required')); ?>
                                                        </div>
                                                        <div class="col-md-12">
                                                            Retype Password
                                                            <input type="password" id="retype" name="retype" class="form-control" placeholder="Retype Password">
                                                        </div>

                                                    </div>


                                                </div>
                                                <div class="col-md-4">
                                                    Additional Company Policies
                                                    <textarea name="Contract[policy]" class="form-control" rows="3"></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-4">
                                                    Employment Type
                                                    <?php echo CHtml::activeDropDownList($contract, 'employee_type', array('full_time' => 'Full Time Employment', 'part_time' => 'Part Time Employment'), array('class' => 'form-control')); ?>
                                                </div>
                                                <div class="col-md-4" >
                                                    NI Number
                                                    <?php echo $form->textField($user, 'ni_number', array('placeholder' => 'NI Number', 'class' => 'form-control', 'required' => 'required')); ?>
                                                </div>

                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-4">
                                                    Contract Start Date
                                                    <?php echo $form->textField($contract, 'start_time', array('placeholder' => 'Start Date', 'class' => 'form-control', 'id' => 'start_date')); ?>
                                                    <!--<input type="text" id="start_date" name="Contract[start_time]" class="form-control" placeholder="Start Date" required="required">-->
                                                </div>
                                                <!--</div>-->

                                                <!--<div class="col-sm-12 form-group">-->
                                                <div class="col-md-4">
                                                    Contract End Date
                                                    <?php echo $form->textField($contract, 'end_time', array('placeholder' => 'End Date', 'class' => 'form-control', 'id' => 'end_date','value' => '')); ?>
                                                    <!--<input type="text" id="end_date" name="Contract[end_time]" class="form-control" placeholder="End Date">-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- /tab1 -->
                            <!-- tab2 -->

                            <div class="tab-pane" id="tab2">
                                <div class="form-body" <div class="col-sm-4">
                                        Upload Contract
                                        <?php echo CHtml::activeFileField($contract, 'file_name', array('class' => 'form-control', 'style' => 'padding-bottom: 40px;')); ?>        
                                    </div></div>

                                <!-- tab 3 -->


                                <div class="tab-pane" id="tab3">


                                    <!-- BEGIN 1st tab FORM-->
                                    <div class="form-body">



                                        <div class="form-group">
                                            <label class="control-label col-md-3">Timesheet Collection</label>
                                            <div class="col-md-4">
                                                <div class="input-icon right">
                                                    <select name="Contract[collection_type]" id="collection_type" class="form-control" id="collection_type">
                                                        <option value="2">Select Option</option>
                                                        <option value="0">Weekly</option>
                                                        <option value="1">Monthly</option>
                                                    </select>
                                                </div>
                                                <div class="help-block">
                                                    Select when to send the accumulated hours to the assigned approver.
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div id="collection_period" style="display: none;">
                                                    <input type="radio" name="Settings[collection_day_first]" value="1" checked>First
                                                    <input type="radio" name="Settings[collection_day_first]" value="0">Last
                                                </div>
                                                <div id="collection_day" style="display: none;">
                                                    <!--                                                <label class="control-label col-md-3">
                                                                                                        Every
                                                                                                    </label>-->
                                                    <select name="Contract[collection_day]" id="collection_days" class="form-control" required="required">
                                                        <option value="">Select Day</option>
                                                        <option value="sunday">Sunday</option>
                                                        <option value="monday">Monday</option>
                                                        <option value="tuesday">Tuesday</option>
                                                        <option value="wednesday">Wednesday</option>
                                                        <option value="thursday">Thursday</option>
                                                        <option value="friday">Friday</option>
                                                        <option value="saturday">Saturday</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3"> Select Timesheet Approver</label>
                                            <div class="col-md-4">
                                                <select  name="Contract[approver_id]" id="approver_id" class="form-control" required="required">
                                                    <option value="0">Select Approver</option>
                                                    <?php foreach ($approvers as $approver) { ?>
                                                        <option value="<?php echo $approver->id; ?>"><?php echo $approver->full_name; ?></option>
                                                    <?php } ?>
                                                </select></div>
                                            <div class="col-md-4" style="display: none;">

                                                Timesheet Template
                                                <select  name="Contract[template]" class="form-control">
                                                    <option value="0">Options</option>
                                                    <option value="1">Standard</option>
                                                </select>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Set Payrates</label>
                                            <div class="col-md-9">
                                                <a class="btn default btn-circle" data-toggle="modal" href="#responsive2">Apply Rates</a>
                                            </div>
                                        </div>

                                        <!-- set contractor rates form -->
                                        <!-- /.modal custom format builder-->
                                        <div id="responsive2" class="modal fade" tabindex="-1" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                        <h4 class="modal-title">Offer Payrates to Staff</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="scroller" style="height:350px" data-always-visible="1" data-rail-visible1="1">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="col-sm-4">
                                                                        <?php if ($payrates = AppCompany::getPayrateNames()) { ?>
                                                                            <h3>Payrates</h3>
                                                                            <?php foreach ($payrates as $payratename) { ?>
                                                                                <?php echo ucwords(str_replace('_', ' ', $payratename->name)); ?>
                                                                                <input type="text" name="Payrate[<?php echo $payratename->name; ?>]" onkeypress="return isNumberKey(event);" class="form-control" placeholder="£/hr" required="required">
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                        <?php if ($items = AppCompany::getItems()) { ?>
                                                                            <h3>Items</h3>
                                                                            <?php foreach ($items as $payratename) { ?>
                                                                                <?php echo ucwords(str_replace('_', ' ', $payratename->name)); ?>
                                                                                <input type="text" name="Item[<?php echo $payratename->name; ?>]" value='<?php echo $payratename->item_value; ?>' onkeypress="return isNumberKey(event);" class="form-control" placeholder="£" required="required">
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" data-dismiss="modal" class="btn green btn-circle" onclick="checkPayrates()">Set Rates</button>
                                                        <button type="button" data-dismiss="modal" class="btn default btn-circle">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- /modal custom format builder-->


                                    </div>

                                    </form>	<!-- END 1st tab FORM-->



                                </div>


                                <!-- /tab3 -->

                                <!-- tab4 -->


                                <div class="tab-pane" id="tab4">
                                    <h3 class="block">Confirm</h3>
                                    <!--/row-->
                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-12" style="text-align:left;"> 
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="tnc" id="chBox" onchange="enable()" checked/> I agree to the <a class="" data-toggle="modal" href="#terms">Terms of Service</a>
                                                    </div>
                                                </label>
                                                <div id="register_tnc_error">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                </div>


                                <!-- /tab4 -->
                            </div>


                            <div class="form-actions right">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <a href="javascript:;" class="btn btn-circle btn-sm btn-default">
                                            <i class="m-icon-swapleft"></i> Back </a>
                                        <a id="continue" href="javascript:show();" class="btn btn-circle btn-sm green-haze">
                                            Continue <i class="m-icon-swapright m-icon-white"></i>
                                        </a>
                                        <input id="submit" type="submit" value="Submit" class='btn btn-circle btn-sm green-haze' style="display:none;">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->endWidget(); ?>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>