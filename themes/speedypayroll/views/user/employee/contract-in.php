<?php
/* @var $this EmployeeController */

$this->breadcrumbs=array(
	'Employee',
);
?>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/user-employee-contractin.js" type="text/javascript"></script>
<h1>Contract In</h1>

<div class="col-sm-10 form-group">
    <div class="col-sm-4">
        Contract Type<select value="0" name="contract_type" class="form-control" disabled="disabled" style="width: 345px;">
            <option>Outside Contractor To Work Inside Your Company</option>
             </select> 
    </div>
    <div class="col-sm-2">
    </div>
        <div class="col-sm-4">
            Upload Contract<select value="0" name="upload_contract" class="form-control" required="required">
                <option>Options</option>
             </select>
            </div>
    
</div>
<div class="col-sm-10 form-group">
    <div class="col-sm-4">
        Contractor Name<input type="text" name="contractor_name" class="form-control" placeholder="Name" required="required">
    </div>
    <div class="col-sm-2">
    </div>
        <div class="col-sm-4">
            Regular Payrate <input type="text" onkeypress="return isNumberKey(event);" name="regular_payrate" class="form-control" placeholder="€/hr" required="required">
            </div>
    
</div>
<div class="col-sm-10 form-group">
    <div class="col-sm-4">
       Contractor Company Name<input type="text" name="contractor_company_name" class="form-control"placeholder="Company" required="required">
    </div>
    <div class="col-sm-2">
    </div>
        <div class="col-sm-4">
            Overtime Payrate <input type="text" onkeypress="return isNumberKey(event);" name="overtime_payrate" class="form-control" placeholder="€/hr" required="required">
            </div>
    
</div>

<div class="col-sm-10 form-group">
    <div class="col-sm-4">
        Timesheet Approver<select id="timesheet_approver" onchange="run()" name="timesheet_approver" class="form-control" required="required">
                <option value="in">Inside Your Company</option>
                <option value="out">Client Company</option>
                
             </select>
    </div>
    <div class="col-sm-2">
    </div>
        <div class="col-sm-4">
            Other Payrate <input type="text" onkeypress="return isNumberKey(event);" name="other_payrate" class="form-control" placeholder="€/hr" required="required">
       <br>
            Addition Company Policies <textarea name="company_policy"></textarea>
        </div> 
         
    
    
</div>

<div class="col-sm-6 form-group">
    <div class="col-sm-4" id="inside" style="display: block;" >
     
        <select  name="inside_company_employees" class="form-control" required="required">
            <option value="0">Select Approver</option>
                <option value="">Employee 1</option>
                <option value="">Employee 2</option>
                
        </select></div>
        
        <div class="col-sm-4" id="outside" style="display: none" >
        Approver Name <input type="text" name="approver_name" class="form-control" placeholder="Name" required="required">
        <br>
        Approver Email <input type="text" name="approver_email" class="form-control" placeholder="Email" required="required">
        <br>
        Approver Phone <input type="text" name="approver_phone" class="form-control" placeholder="Phone" required="required">
           
        </div>
       
        
    </div>
    
    <div class="col-sm-2">
    </div>
       
    
</div>

<div class="col-sm-10">
    <div class="col-sm-4">
        Timesheet Frequency<select id="timesheet_frequency" name="timesheet_frequency" class="form-control" required="required">
            <option value="0">Select Frequency</option>
                <option value="in">Every week</option>
                <option value="out">Every month</option>
                
        </select> <br><p style="display: none;">
         Timesheet Template <select name="timesheet_template" class="form-control">
                <option value="0">Options</option>
                
            </select>
         </p>
         <br>
    </div>
    
</div>
<div class="col-sm-10">
    <input type="submit" value="Done" class="btn btn-primary">
</div>
<div class="clearfix"></div>