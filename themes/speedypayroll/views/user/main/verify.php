<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle = Yii::app()->name . ' - Login';
$this->breadcrumbs = array(
    'Login',
);
?>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'login-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
        ));
?>
<div class="login-box-body">
    <p class="login-box-msg">Verify your email address</p>
    <div class="form-group">
        <label><?php echo $error ? $error : ''; ?></label>
    </div>
    <div class="form-group">
        <input type="text" name="email" class="form-control" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="E.g:example@abc.com" placeholder="Email" required="required" value="<?php echo isset($_POST['email']) ? $_POST['email'] : ''; ?>">
    </div>
</div>
<div class="footer">
    <?php echo CHtml::submitButton('Continue',array('class'=>'btn bg-olive btn-block')); ?>
</div>

<?php $this->endWidget(); ?>
<script>
    jQuery(document).ready(function(){
       var visitortime = new Date();
            var visitortimezone = "GMT " + -visitortime.getTimezoneOffset()/60;
            var url = '<?php echo Yii::app()->baseUrl; ?>';
            url = url.concat('/site/clienttimezone');
            $.ajax({
                type: "GET",
                url: url,
                data: 'time='+ visitortimezone,
                success: function(response){
//                    alert(response);
//                    location.reload();
                }
            }); 
    });
</script>