<?php
/* @var $this MainController */
/* @var $model User */

$this->breadcrumbs = array(
    'Users' => array('index'),
    $model->first_name.' '.$model->last_name,
);

$this->menu = array(
    array('label' => 'List User', 'url' => array('index')),
    array('label' => 'Create User', 'url' => array('create')),
    array('label' => 'Update User', 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Delete User', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage User', 'url' => array('admin')),
);
?>

<!-- BEGIN SAMPLE FORM PORTLET-->
<div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user"></i>User Details
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>

                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">

                        <div class="col-md-12">
                            
                        </div>
                    </div>
                </div>


<div class="box-header">
    <h3 class="box-title"><?php echo ucwords($model->full_name); ?></h3>
    <div class="box-tools">
        <a class="btn btn-circle btn-sm green-haze pull-right"  href="<?php echo $this->createUrl('/user/main/update', array('id' => $model->id)); ?>">Edit</a>
    </div>
</div>
<div class="table-responsive">
    <div class="box-body">
        <?php
        $this->widget('zii.widgets.CDetailView', array(
            'data' => $model,
            'attributes' => array(
                'first_name',
                'last_name',
                'email',
                array('name'=>'NI Number',
                    'value'=>$model->ni_number != NULL ?$model->ni_number : '-'),
                array('name'=>'Date Of Birth' ,
                    'value'=>date('d M Y',$model->dob)) ,
                array('name'=>'Phone',
                    'value'=>$model->phone != NULL ?$model->phone : '-'),
                array('name' => 'User Shift','value' => $shift),
//                'addressline_1',
                array('name'=>'Address Line',
                    'value'=>$model->addressline_1 != NULL ?$model->addressline_1 : '-'),
                array('name'=>'Street',
                    'value'=>$model->street != NULL ?$model->street : '-'),
                array('name'=>'City',
                    'value'=>$model->city != NULL ?$model->city : '-'),
                array('name'=>'Country',
                    'value'=>$model->country != NULL ?$model->country : '-'),
            ),
        ));
        ?>
    </div>
</div>
<div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                        <a class="btn btn-circle btn-sm green-haze" style="float:right;" href="<?php echo $this->createUrl('/user/employee/index'); ?>">Done</a>
</div>
                    </div></div>
    </div>
</div>