<?php
/* @var $this MainController */
/* @var $model User */

$this->breadcrumbs = array(
    'Users' => array('index'),
);
?>




<div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-globe"></i>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
<div class="box-header">
    <h3 class="box-title"><?php echo ucwords($model->full_name); ?></h3>
    <a class="btn btn-default pull-right" style="margin-right: 10px;" href="<?php echo Yii::app()->createUrl('user/main/editprofile'); ?>">Edit Profile</a>
    <a class="btn btn-default pull-right" href="<?php echo Yii::app()->createUrl('user/main/changepassword'); ?>">Change Password</a>
</div>
<div class="table-responsive">
    <div class="box-body">
        <?php
        $this->widget('zii.widgets.CDetailView', array(
            'data' => $model,
            'attributes' => array(
                'first_name',
                'last_name',
                'email',
                array('name'=>'NI Number' , 'value'=>$model->ni_number ? $model->ni_number : '-'),
                array('name'=>'Date of Birth' , 'value'=>$model->dob ? date('d M Y',$model->dob) : '-') ,
                array('name'=>'Phone' , 'value'=>$model->phone ? $model->phone : '-'),
                array('name'=>'Address' , 'value'=>$model->addressline_1 ? $model->addressline_1 : '-'),
                array('name'=>'Street' , 'value'=>$model->street ? $model->street : '-'),
                array('name'=>'City' , 'value'=>$model->city ? $model->city : '-'),
                array('name'=>'Country' , 'value'=>$model->country ? $model->country : '-'),
            ),
        ));
        ?>
    </div>
</div>
<div class="box-footer">
</div>
                    </div>
</div>