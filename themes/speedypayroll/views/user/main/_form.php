<?php
/* @var $this MainController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'user-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => false,
        ));
?>
<div class="box-body">
    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php if ($model->hasErrors()) { ?>
        <div class='callout callout-danger'>    <?php echo $form->errorSummary($model); ?>
        </div>
    <?php } ?>    

    <div class="form-group">
        <?php echo $form->labelEx($model, 'first_name'); ?>
        <?php echo $form->textField($model, 'first_name', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
        <?php echo $form->error($model, 'first_name', array('class' => 'text-red')); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'last_name'); ?>
        <?php echo $form->textField($model, 'last_name', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
        <?php echo $form->error($model, 'last_name', array('class' => 'text-red')); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'email'); ?>
        <?php echo $form->textField($model, 'email', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
        <?php echo $form->error($model, 'email', array('class' => 'text-red')); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'password'); ?>
        <?php echo $form->passwordField($model, 'password', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
        <?php echo $form->error($model, 'password', array('class' => 'text-red')); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'ni_number'); ?>
        <?php echo $form->textField($model, 'ni_number', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
        <?php echo $form->error($model, 'ni_number', array('class' => 'text-red')); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'dob'); ?>
        <?php echo $form->textField($model, 'dob', array('size' => 20, 'maxlength' => 20, 'class' => 'form-control')); ?>
        <?php echo $form->error($model, 'dob', array('class' => 'text-red')); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'phone'); ?>
        <?php echo $form->textField($model, 'phone', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
        <?php echo $form->error($model, 'phone', array('class' => 'text-red')); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'addressline_1'); ?>
        <?php echo $form->textField($model, 'addressline_1', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
        <?php echo $form->error($model, 'addressline_1', array('class' => 'text-red')); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'street'); ?>
        <?php echo $form->textField($model, 'street', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
        <?php echo $form->error($model, 'street', array('class' => 'text-red')); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'city'); ?>
        <?php echo $form->textField($model, 'city', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
        <?php echo $form->error($model, 'city', array('class' => 'text-red')); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'country'); ?>
        <?php echo $form->textField($model, 'country', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
        <?php echo $form->error($model, 'country', array('class' => 'text-red')); ?>
    </div>

</div>
<div class="box-footer">
    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary')); ?>
</div>

<?php $this->endWidget(); ?>
