<?php

$this->pageTitle = Yii::app()->name . ' - Login';
$this->breadcrumbs = array(
    'Login',
);

     

?>
<div class="login-box-body">
    <p class="login-box-msg">Activate Account</p>
    <div class="form-group">
        <p>Please contact admin to activate your account.</p>
    </div>
</div>