<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle = Yii::app()->name . ' - Login';
$this->breadcrumbs = array(
    'Login',
);
?>

<div class="content">
    <?php echo $this->renderPartial('//layouts/includes/flash-messages'); ?>

    <?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'login-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array(
      'class'=>'login-form',  
    ),
        ));
?>
    <h3 class="form-title">Sign In</h3>
    <div class="form-group has-feedback">
        <input type="text" name="email" class="form-control form-control-solid placeholder-no-fix" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="E.g:example@abc.com" placeholder="Email" required="required" value="<?php echo isset($_POST['email']) ? $_POST['email'] : ''; ?>">
        <span class="glyphicon glyphicon-envelope form-control-feedback" style="top:14px;"></span>
    </div>
    <div class="form-group has-feedback">
        <input type="password" placeholder="Password" required="required" name="password" class="form-control form-control-solid placeholder-no-fix">
        <span class="glyphicon glyphicon-lock form-control-feedback" style="top:14px;"></span>
    </div>
    <div class="form-actions">
                        <?php echo CHtml::submitButton('Login', array('class' => 'btn btn-success uppercase')); ?>
			<label class="rememberme check pull-right">
                        <a href="<?php echo Yii::app()->createUrl('/user/main/forgot'); ?>" class="forget-password">Forgot Password?</a>
		</div>
    <div class="row">
        <div class="col-xs-8"> 
            <?php if ($error) { ?>
                <label class="text-danger"><?php echo $error; ?></label>
            <?php } ?>
        </div>
    </div>
    <div class="create-account">
			<p>
                                <a href="<?php echo Yii::app()->createUrl('/site/signup'); ?>" class="uppercase">Create an account</a>
			</p>
		</div>
    
<?php $this->endWidget(); ?>
</div>

<script>
    jQuery(document).ready(function(){
       var visitortime = new Date();
            var visitortimezone = "GMT " + -visitortime.getTimezoneOffset()/60;
            var url = '<?php echo Yii::app()->baseUrl; ?>';
            url = url.concat('/site/clienttimezone');
            $.ajax({
                type: "GET",
                url: url,
                data: 'time='+ visitortimezone,
                success: function(response){
//                    alert(response);
//                    location.reload();
                }
            }); 
    });
</script>