<?php
/* @var $this MainController */
/* @var $model User */

$this->breadcrumbs = array(
    'Users' => array('index'),
    'Create',
);

$this->menu = array(
    array('label' => 'List User', 'url' => array('index')),
    array('label' => 'Manage User', 'url' => array('admin')),
);
?>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/user-main-invite.js" type="text/javascript"></script>
<div class="portlet box blue" id="form_wizard_1">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i> Invite Employee </span>
        </div>
  </div>
    <div class="portlet-body form">

        <?php if (Yii::app()->user->hasFlash('message')) { ?>

            <div style="background-color: beige;" >
                <h4 style="margin-left:10px"><b><?php echo Yii::app()->user->getFlash('message'); ?></b></h4>
                <b style="margin-left:10px"> <?php echo Yii::app()->user->getFlash('message1'); ?></b>
            </div>
        <?php } ?>
        <div class="box-header">
            <h3 class="box-title">Invite Employee</h3>
        </div>
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'user-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation' => false,
        ));
        ?>
        <div class="box-body">
            <input type="hidden" id="addmore" name="addmore" value="0">
            <?php if ($success) { ?>
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>An invitation email has been sent to the user.</b>
                </div>
            <?php } ?>

            <div class="col-sm-12">
                <div class="form-group col-sm-4">
                    First Name*
                    <?php echo $form->textField($user, 'first_name', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control', 'placeholder' => 'First Name', 'required' => 'required')); ?>
                    <?php echo $form->error($user, 'first_name', array('class' => 'text-red')); ?>
                </div>
                <div class="col-sm-1">
                </div>

                <div class=" col-sm-4 form-group">
                    Last Name*
                    <?php echo $form->textField($user, 'last_name', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control', 'placeholder' => 'Last Name', 'required' => 'required')); ?>
                    <?php echo $form->error($user, 'last_name', array('class' => 'text-red')); ?>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="col-sm-4 form-group">
                    Email*
                    <?php echo $form->textField($user, 'email', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control', 'placeholder' => 'Email', 'required' => 'required')); ?>
                    <?php echo $form->error($user, 'email', array('class' => 'text-red')); ?>

                </div>
                <div class="col-sm-1">
                </div>
                <div class="col-sm-4">
                    Timesheet Approver*
                    <select  name="Contract[approver_id]" class="form-control" required="required">
                        <option value="0">Select Approver</option>
                        <?php foreach ($approvers as $approver) { ?>
                            <option value="<?php echo $approver->id; ?>"><?php echo $approver->full_name; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-12 form-group">
                <div class="col-sm-4">
                    Employment Type*
                    <select  name="Contract[employee_type]" class="form-control" required="required">
                        <option value="full_time">Full Time Employment</option>
                        <option value="part_time">Part Time Employment</option>
                    </select>
                </div>
                <div class="col-sm-1">
                </div>
                <div class="col-sm-4" >
                    NI Number*
                    <?php echo $form->textField($user, 'ni_number', array('placeholder' => 'NI Number', 'class' => 'form-control', 'required' => 'required')); ?>
                </div>

            </div>
            <div class="col-sm-12 form-group">
                <div class="col-sm-4">
                    Contract Start Date*
                    <input type="text" id="start_date" name="Contract[start_time]" class="form-control" placeholder="Start Date" required="required">
                </div>
                <div class="col-sm-1">
                </div>
                <div class="col-sm-4">
                    Contract End Date
                    <input type="text" id="end_date" name="Contract[end_time]" class="form-control" placeholder="End Date">
                </div>

            </div>


            <div class="col-sm-12 form-group">
                <div class="col-sm-4">
                    <p>
                        Timesheet Collection Frequency*
                        <select name="Contract[collection_type]" class="form-control" id="collection_type">
                            <option value="2">Select Option</option>
                            <option value="0">Weekly</option>
                            <option value="1">Monthly</option>
                        </select>
                    </p>

                </div>
                <div class="col-sm-1">
                </div>

                <div class="col-sm-4">
                    <div id="collection_period" style="display: none;">
                        <input type="radio" name="Settings[collection_day_first]" value="1">First
                        <input type="radio" name="Settings[collection_day_first]" value="0">Last
                    </div>
                    <div id="collection_day" style="display: none;">
                        <p>
                            Every*
                            <select  name="Contract[collection_day]" class="form-control" required="" >
                                <option value="">Select Day</option>
                                <option value="sunday">Sunday</option>
                                <option value="monday">Monday</option>
                                <option value="tuesday">Tuesday</option>
                                <option value="wednesday">Wednesday</option>
                                <option value="thursday">Thursday</option>
                                <option value="friday">Friday</option>
                                <option value="saturday">Saturday</option>
                            </select>
                        </p>
                    </div>

                </div> 
            </div>
            <div class="col-sm-12 form-group">
                <div class="col-sm-4 form-group">

                    <?php if ($payrates = AppCompany::getPayrateNames()) { ?>
                        <h3>Payrates</h3>
                        <?php foreach ($payrates as $payratename) { ?>
                            <?php echo ucwords(str_replace('_', ' ', $payratename->name)); ?>
                            <input type="text" name="Payrate[<?php echo $payratename->name; ?>]" onkeypress="return isNumberKey(event);" class="form-control" placeholder="£/hr" required="required">
                        <?php } ?>
                    <?php } ?>
                    <?php if ($items = AppCompany::getItems()) { ?>
                        <h3>Items</h3>
                        <?php foreach ($items as $payratename) { ?>
                            <?php echo ucwords(str_replace('_', ' ', $payratename->name)); ?>
                            <input type="text" name="Item[<?php echo $payratename->name; ?>]" value='<?php echo $payratename->item_value; ?>' onkeypress="return isNumberKey(event);" class="form-control" placeholder="£" required="required">
                        <?php } ?>
                    <?php } ?>
                </div>

                <div class="col-sm-1">
                </div>
                <div class="col-sm-4" style="display:none;">
                    Timesheet Template*
                    <select  name="Contract[template]" class="form-control">
                        <option value="0">Options</option>
                        <option value="1">Standard</option>
                    </select>
                </div>

            </div>

<input type="hidden" id="date_format" value="<?php echo AppInterface::getdateformat(true); ?>"/>
            <div class="clearfix"></div>

            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                        <a class="btn btn-primary" data-toggle="modal" data-target="#compose-modal">Send Invite</a>
                        <a class="btn btn-primary" href="<?php echo Yii::app()->createAbsoluteUrl('/user/employee/'); ?>">Skip</a>
                        <input type="submit" id="submit" name="submit" value="submit" style="display: none">
                    </div>
                </div>
            </div>

            <?php $this->endWidget(); ?>

            <div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Invite More Employees</h4>
                        </div>
                        <div class="modal-body">
                            <p>Select "Invite More" if you want to invite more employees.</p>
                        </div>
                        <div class="modal-footer clearfix">
                            <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="sendinvite(1);">Invite More</button>
                            <button type="submit" class="btn btn-primary" data-dismiss="modal" onclick="sendinvite(0);">Done</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

        </div>
    </div>
</div>