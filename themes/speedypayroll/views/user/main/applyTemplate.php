<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle = Yii::app()->name . ' - Login';
$this->breadcrumbs = array(
    'Login',
);
?>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/user-main-applytemplate.js" type="text/javascript"></script>
<!-- Begin Wizard-->
<div class="row">
    <div class="regwizard">
        <div class="portlet box blue" id="form_wizard_1">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-sign-in"></i> Speedy Registration - <span class="step-title">
                        Step 4 of 4 </span>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="form-wizard">
                    <div class="form-body">
                        <ul class="nav nav-pills nav-justified steps">
                            <li class="done">
                                <a href="#tab1" data-toggle="tab" class="step">
                                    <span class="number">
                                        1 </span>
                                    <span class="desc">
                                        <i class="fa fa-check"></i> Account Type </span>
                                </a>
                            </li>
                            <li class="done">
                                <a href="#tab2" data-toggle="tab" class="step">
                                    <span class="number">
                                        2 </span>
                                    <span class="desc">
                                        <i class="fa fa-check"></i> Your Details </span>
                                </a>
                            </li>
                            <li class="done">
                                <a href="#tab3" data-toggle="tab" class="step active">
                                    <span class="number">
                                        3 </span>
                                    <span class="desc">
                                        <i class="fa fa-check"></i> Company Details </span>
                                </a>
                            </li>
                            <li class="active">
                                <a href="#tab4" data-toggle="tab" class="step">
                                    <span class="number">
                                        4 </span>
                                    <span class="desc">
                                        <i class="fa fa-check"></i> Set Payrates </span>
                                </a>
                            </li>
                        </ul>

                        <div id="bar" class="progress progress-striped" role="progressbar">
                            <div class="progress-bar progress-bar-success" style="width:100%">
                            </div>
                        </div>
                        <div class="tab-content">
                            <div id="errorDiv" class="alert alert-danger display-none">
                                <button class="close" data-dismiss="alert"></button>
                                <span>You have some form errors. Please check below.</span>
                            </div>
                            <div id="successDiv" class="alert alert-success display-none">
                                <button class="close" data-dismiss="alert"></button>
                                <span>Your form validation is successful!</span>
                            </div>

                            <!-- tab4 -->


                            <div class="tab-pane active" id="tab4">
                                <h3 class="block">Set Payrates Template</h3>

                                <!--row-->	
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="portlet light">
                                            <!--<button type="button" class="" data-dismiss="alert"></button>-->
                                            <h4 class="alert-heading">Option 1</h4>
                                            <p>
                                                Use the default timesheet format. You will instantly gain access to all three types of Timesheets by selecting this option.
                                            </p>
                                            <p>
                                                <a id="show-default" href="#" class="btn popovers" data-toggle="modal" data-target="#myModal">See Default Format</a>
                                            </p>
                                            <p>
                                                Click the link above to preview.
                                            </p>
                                            <p>
<!--                                                <a href="<?php //echo Yii::app()->createUrl("/user/main/assignTemplate"); ?>" class="btn green">Accept & Apply Format</a>-->
                                                <a href="javascript:;" class="btn green" onclick="applyDefaultTemplate()">Accept & Apply Format</a>
                                            </p>
                                            <p class="text-success">
                                                Default timesheet format is FREE.
                                            </p>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="portlet light">
                                            <!--<button type="button" class="" data-dismiss="alert"></button>-->
                                            <h4 class="alert-heading">Option 2</h4>
                                            <p>
                                                Request a custom timesheet format. 
                                            </p>
                                            <p>
                                                Custom timesheet formats are subject to approval by Speedy Admin (typically 24 hours). You will not be able to use
                                                Two-way and Three-way timesheets until approval. However, you can use One-Way Timesheets instantly.
                                            </p>
                                            <p>
                                                <a class="btn default" data-toggle="modal" href="#responsive">Format Builder</a>
                                            </p>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->	

                                <form id="customTempForm" class="form-horizontal" action="<?php echo $this->createUrl('/user/main/customTemplate'); ?>" method="post">
                                    <!-- /.modal custom format builder-->
                                    <div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Custom Timesheet Format Builder</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="scroller" style="height:350px" data-always-visible="1" data-rail-visible1="1">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <h4>Hours</h4>
                                                                <p>Enter titles for required custom fields such as; visiting hours, night shift, phone consultation etc.</p> 
                                                                <div class="row form-group item-row">
                                                                    <div class="col-md-11">
                                                                        <input type="text" name="payrate[]" value="Regular" class="form-control" readonly="">
                                                                    </div>
                                                                </div>
                                                                <div class="row" id="payrate-before">
                                                                    <div class="col-md-12 text-center">
                                                                        <a href="javascript:void(0);" id="add-more-payrates"><i class="fa fa-fw fa-plus-square"></i> Add More</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <h4>Expenses</h4>
                                                                <p>Enter titles for required expense fields such as; parking charges, fuel expense, travel expense etc.</p>
                                                                <div class="row form-group item-row">
                                                                    <div class="col-md-11">
                                                                        <input type="text" name="item[]" value="Expenses" class="form-control" readonly>
                                                                    </div>
                                                                    <a href="javascript:void(0);" class="remove-closest-item-row" ><i class="fa fa-fw fa-times-circle"></i></a>

                                                                </div>

                                                                <div class="row" id="item-before">
                                                                    <div class="col-md-12 text-center">
                                                                        <a href="javascript:void(0);" id="add-more-items"><i class="fa fa-fw fa-plus-square"></i> Add More</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <input type="button" data-dismiss="modal" onclick="applyCustomTemplate()" class="btn green" value="Save & Request Format"/>
                                                    <button type="button" data-dismiss="modal" class="btn default">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- /modal custom format builder-->
                                </form>


                                <!-- /.modal custom format builder-->
                                <div id="terms" class="modal fade" tabindex="-1" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                <h4 class="modal-title">Terms of Service</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="scroller" style="height:350px" data-always-visible="1" data-rail-visible1="1">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <h4>Usage</h4>
                                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p> 
                                                            <h4>Service</h4>
                                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p> 
                                                            <h4>Offer</h4>
                                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p> 
                                                            <h4>Limitations</h4>
                                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p> 
                                                            <h4>Scope</h4>
                                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p> 
                                                        </div>
                                                        <div class="col-md-6">
                                                            <h4>Usage</h4>
                                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p> 
                                                            <h4>Service</h4>
                                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p> 
                                                            <h4>Offer</h4>
                                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p> 
                                                            <h4>Limitations</h4>
                                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p> 
                                                            <h4>Scope</h4>
                                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" data-dismiss="modal" class="btn default">Print</button>
                                                <button type="button" data-dismiss="modal" class="btn default">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- /modal custom format builder-->



                                <!--/row-->
                                <div class="row">

                                    <div class="col-md-6">
                                            <label> 
                                                <input type="checkbox" name="tnc" required="required" id="chBox" onchange="enable()"/> I agree to the <a class="" data-toggle="modal" href="#terms">Terms of Service</a>
                                                
                                            </label>
                                            <div id="register_tnc_error">
                                            </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">

                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                            </div>


                            <!-- /tab4 -->

                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <a href="javascript:void(0);" onclick="submitForm()" id="sendBtn" class="btn green button-submit" disabled>
                                    Submit <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>


    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Default Template</h4>
                </div>
                <div class="modal-body">
                    <p> 
                    <section>
                        <h4>Fields available to record working hours : </h4>
                        <ul>
                            <?php
                            $template = Template::model()->findByAttributes(array('name' => 'Default'));
                            $payrates = AppTemplate::getPayrates($template->id);
                            $items = AppTemplate::getItems($template->id);
                            foreach ($payrates as $payrate) {
                                echo '<li>' . $payrate->name . ' (Input Hours)</li>';
                            }
                            ?>
                        </ul>
                    </section>
                    <section>
                        <h4>Fields available to record expenses : </h4>
                        <ul>

                            <?php
                            foreach ($items as $item) {
                                echo '<li>' . $item->name . ' (Amount)</li>';
                            }
                            ?>
                        </ul>
                    </section></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <input type="hidden" id="assign" value="<?php echo Yii::app()->createUrl("/user/main/assignTemplate"); ?>"/>
    <input type="hidden" id="verify" value="<?php echo Yii::app()->createUrl("/user/main/verifymessage"); ?>"/>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/select2/select2.min.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/metronic.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/css/layout4/scripts/layout.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/css/layout4/scripts/demo.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/css/pages/scripts/form-wizard.js"></script>
    <!-- END PAGE LEVEL SCRIPTS -->