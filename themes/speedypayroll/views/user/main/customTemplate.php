<?php
/* @var $this MainController */
/* @var $model Template */
/* @var $form CActiveForm */
?>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/user-main-customtemplate.js" type="text/javascript"></script>
<form action="<?php echo $this->createUrl('/user/main/customTemplate'); ?>" method="post">
<div class="box-body" style="background-color: white;">
    <div class="form-group" style="text-align: center;">
            <h3>Custom Timesheet Format Builder</h3>
    <div class="row" style="margin-right:0px;margin-left:0px;">
        <div class="col-md-6">
            <h4>Hours</h4>
            <div class="row form-group item-row" style="padding-left: 15px;">
                <p>Enter titles for required custom fields such as Visiting Hours, Night Shift, Phone Consultation etc.</p>
            </div>
                <div class="row form-group item-row">
                    <div class="col-md-11">
                        <input type="text" name="payrate[]" value="Regular" class="form-control" readonly="">
                    </div>
                </div>           
            <div class="row" id="payrate-before">
                <div class="col-md-12 text-center">
                    <a href="javascript:void(0);" id="add-more-payrates"><i class="fa fa-fw fa-plus-square"></i> Add More</a>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <h4>Expenses</h4>
            <div class="row form-group item-row" style="padding-left: 15px;">
                <p>Enter titles for required custom expense such as Parking, Food, Travel etc.</p>
            </div>
                <div class="row form-group item-row">
                    <div class="col-md-11">
                        <input type="text" name="item[]" value="Expenses" class="form-control" readonly>
                    </div>
                            <a href="javascript:void(0);" class="remove-closest-item-row" ><i class="fa fa-fw fa-times-circle"></i></a>
                    
                </div>

            <div class="row" id="item-before">
                <div class="col-md-12 text-center">
                    <a href="javascript:void(0);" id="add-more-items"><i class="fa fa-fw fa-plus-square"></i> Add More</a>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
<div class="box-footer">
    <input type="submit" class = "btn btn-primary" value="Save and Request Format Activation">
    <p>*Custom format submissions are subject to approval by Speedy Admin and incurr a setup charge of £99.00+VAT.</p>
</div>
    </form>
