<!-- <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Alert!</b>The email address you typed does not match.
    </div>-->
<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle = Yii::app()->name . ' - Change Password';
$this->breadcrumbs = array(
    'Change Password',
);
?>

<div class="portlet box green-meadow">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-globe"></i> Change Password
        </div>
      <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>

        </div>
    </div>
    <div class="portlet-body form">

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'change-form',
    'enableAjaxValidation' => false,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
        'validateOnType' => false,
    ),
        ));
?>

<input type="hidden" value="step2" name="request_type">

<div class="row">
    <?php //if ($password) { ?>
        <!--<div class="sign-up heading">Create new password</div>-->
                <h2 style="padding-left:40px;">Change your password</h2>
            <div class="col-md-6" style="padding-left:70px;">
                <div class="form-group">
                    <input type="password" id="opwd" name="old_password" class="form-control signin-p" placeholder="Old Password" required="required">
                </div>
                <div class="form-group">
                    <input type="password" id="pwd" name="user_password" class="form-control signin-p" placeholder="New Password" required="required">
                </div>
                <div class="form-group">
                    <input type="password" id="cpwd" name="user_password1" class="form-control signin-p" placeholder="Retype Password" required="required">
                    <div id="error" style="display:none;"></div>
                </div>
                
                <script>
                    function validatepassword(){
                        if($('#pw').val() !== $('#cpw').val()){
//                            $('#cpw').css('border-color','red');
                        alert('Password Must Match');   
                        return false;
                        }
                        return true;
                    }
                </script>
            </div><!--col-sm-5-->
    <?php //} else { ?>
        
</div><!--col-lg-12-->
<div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                    <?php echo CHtml::submitButton('Continue', array('class' => 'btn btn-primary signin-submit1','onclick'=>'return validatepassword();')); ?>
                </div><!--col-sm-5 signin-submit-->
                </div>
                </div>
<?php $this->endWidget(); ?>
<div class="clearfix"></div>
    </div>
</div>