<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle = Yii::app()->name . ' - Login';
$this->breadcrumbs = array(
    'Login',
);
?>
<div class="content">
    
<div style="text-align: center;"><h2>Seminant Solutions</h2> </div>
<div class="header">Forgot Password</div>
            <?php echo $this->renderPartial('//layouts/includes/flash-messages'); ?>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'forget-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array(
      'class' => 'login-form',  
    ),
        ));
?>

<?php if(Yii::app()->user->hasFlash('message')) ?>

<div class="body bg-gray">
    
    <div class="form-group">
        <input type="text" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="E.g:example@abc.com" class="form-control" placeholder="Email" required="required">
    </div>
    <div class="form-group">
        <div class="clearfix"></div>
    </div>
</div>
<div class="footer">
    <?php echo CHtml::submitButton('Reset Password', array('class' => 'btn btn-success uppercase')); ?>
    <?php echo Yii::app()->user->getFlash('message'); ?>
</div>
<?php $this->endWidget(); ?>

<!--<h2>Reset your password</h2>
<input type="hidden" value="step2" name="request_type">
<div class="body bg-gray">
<div class="col-lg-12 sign-up">
    <?php //if ($password) { ?>
        <div class="sign-up heading">Create new password</div>
        <div class="col-md-10 col-md-offset-1">
            <div class="col-sm-7 col-sm-offset-3">
                <div class="form-group">
                    <input type="password" id="pw" name="user_password" class="form-control signin-p" placeholder="Password" required="required">
                </div>
                <div class="form-group">
                    <input type="password" id="cpw" name="user_password" class="form-control signin-p" placeholder="Confirm Password" required="required">
                    <div id="error" style="display:none;"></div>
                </div>
                <div class="col-sm-5 signin-submit col-sm-offset-2">
                </div>
                <script>
                    function validatpassword(){
                        if($('#pw').val() !== $('#cpw').val()){
                            $('#cpw').css('border-color','red');
                            return false;
                        }
                        return true;
                    }
                </script>
            </div>
        </div>
        <h4 class="form-group signin-p" style="margin-left: 380px;">Provide your email address below to reset password</h4>
        <div class="col-md-10 col-md-offset-1">
            <div class="col-sm-7 col-sm-offset-3	">
                <div class="form-group">
                    <input type="text" name="user_email" class="form-control signin-p" placeholder="Email Address">
                </div>
                    <div class="col-sm-5 signin-submit col-sm-offset-2" style="margin-left: 170px;">
                        <br>
                    </div>
            </div>
        </div>
        <br>
</div>
</div>
<div class="clearfix"></div>-->
</div>
