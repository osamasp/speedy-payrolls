<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle = Yii::app()->name . ' - Login';
$this->breadcrumbs = array(
    'Login',
);
?>
<div class="login-box-body">
    <div class="form-group">
        
        <p> Default Template with following payrates and items will be assigned. 
        <section>
            <h4>Payrates</h4>
        <ul>
            <?php
            $template = Template::model()->findByAttributes(array('name'=>'Default'));
            $payrates = AppTemplate::getPayrates($template->id);
            $items = AppTemplate::getItems($template->id);
            foreach ($payrates as $payrate) {
                echo '<li>'.$payrate->name.'</li>';
            }
            ?>
        </ul>
        </section>
        <section>
            <h4>Items</h4>
        <ul>
        
        <?php
            foreach ($items as $item) {
                echo '<li>'.$item->name.'</li>';
            }

            ?>
        </ul>
        </section>
          </div>    
</div>
