<?php
/* @var $this MainController */
/* @var $model User */

$this->breadcrumbs = array(
    'Users' => array('index'),
    $model->getfull_name() => array('view', 'id' => $model->id),
    'Update',
);

$this->menu = array(
    array('label' => 'List User', 'url' => array('index')),
    array('label' => 'Create User', 'url' => array('create')),
    array('label' => 'View User', 'url' => array('view', 'id' => $model->id)),
    array('label' => 'Manage User', 'url' => array('admin')),
);
?>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/user-main-update.js" type="text/javascript"></script>
<!-- BEGIN SAMPLE FORM PORTLET-->
<div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-globe"></i>Edit Profile
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>

                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">

                        <div class="col-md-12">
                            
                        </div>
                    </div>
                </div>


<div class="box-header">
    <h3 class="box-title">Update <?php echo $model->getfull_name(); ?></h3>
   
</div>

<?php
/* @var $this MainController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'user-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => false,
    'htmlOptions' => array('enctype' => 'multipart/form-data')
        ));
?>
    
    <?php if ($model->hasErrors()) { ?>
        <div class='callout callout-danger'>    <?php echo $form->errorSummary($model); ?>
        </div>
    <?php } ?>    

    
    <div class="row">
    <div class="col-md-6" style="padding-left:30px;">
    <div class="form-group">
        <?php echo $form->labelEx($model, 'first_name'); ?>
        <?php echo $form->textField($model, 'first_name', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
        <?php echo $form->error($model, 'first_name', array('class' => 'text-red')); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'last_name'); ?>
        <?php echo $form->textField($model, 'last_name', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
        <?php echo $form->error($model, 'last_name', array('class' => 'text-red')); ?>
    </div>
    </div>
        <div class="col-md-4">
        <div id="preview" class="img-preview" style="margin-top: 15px;width:40%;" >
        </div>
        <div class="form-group">
            <span class="btn btn-primary btn-file">
                Browse <?php echo $form->fileField($model, 'photo'); ?>
            </span>
        </div>
    </div>
    </div>

        <div class="row">
            <div class="col-md-6" style="padding-left:30px;">
    <div class="form-group">
        <?php echo $form->labelEx($model, 'email'); ?>
        <?php echo $form->textField($model, 'email', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
        <?php echo $form->error($model, 'email', array('class' => 'text-red')); ?>
    </div>
    
    <?php if ($model->id == AppUser::getUserId()) { ?>
        <div class="form-group">
            <?php echo $form->labelEx($model, 'password'); ?>
            <a href="<?php echo $this->createUrl('changepassword') ?>">Change password</a>
        </div>
    <?php } ?>
            </div>
        </div>
                <div class="row">
                    <div class="col-md-6" style="padding-left:30px;">
                        <div class="form-group">
                            <label>User Shift</label>
                        <select name="User[shift]" class="form-control">
                            <option value="0">Please select shift</option>
                            <?php foreach($shifts as $shift){ ?>
                            <?php if($shift->id == $model->shift_id){ ?>
                            <option value="<?php echo $shift->id;?>" selected><?php echo $shift->title;?></option>
                            <?php } else{ ?>
                            <option value="<?php echo $shift->id;?>"><?php echo $shift->title;?></option>
                            <?php } } ?>
                        </select>
                        </div>
                    </div>
                </div>
        <div class="row">
            <div class="col-md-6" style="padding-left:30px;">
    <div class="form-group">
        <?php echo $form->labelEx($model, 'ni_number'); ?>
        <?php echo $form->textField($model, 'ni_number', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
        <?php echo $form->error($model, 'ni_number', array('class' => 'text-red')); ?>
    </div>

    <div class="form-group">
            <label>Date Of Birth*</label>
            <div class="row">
                <div class="col-lg-4">
                    <select id="form_dob_day" name="dob_day" class="form-control" >
                        <option value="<?php echo date('d' , $model->dob); ?>" selected><?php echo date('d' , $model->dob); ?></option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">14</option>
                        <option value="15">15</option>
                        <option value="16">16</option>
                        <option value="17">17</option>
                        <option value="18">18</option>
                        <option value="19">19</option>
                        <option value="20">20</option>
                        <option value="21">21</option>
                        <option value="22">22</option>
                        <option value="23">23</option>
                        <option value="24">24</option>
                        <option value="25">25</option>
                        <option value="26">26</option>
                        <option value="27">27</option>
                        <option value="28">28</option>
                        <option value="29">29</option>
                        <option value="30">30</option>
                        <option value="31">31</option>
                    </select>
                </div><div class="col-lg-4" style="padding-left:30px;">
                    <select id="form_dob_month" name="dob_month" class="form-control" style="width: 100px;margin-left: -15px;">                        
                        <option value="<?php echo date('M' , $model->dob); ?>" selected><?php echo date('M' , $model->dob); ?></option>
                        <option value="1">January</option>
                        <option value="2">Febuary</option>
                        <option value="3">March</option>
                        <option value="4">April</option>
                        <option value="5">May</option>
                        <option value="6">June</option>
                        <option value="7">July</option>
                        <option value="8">August</option>
                        <option value="9">September</option>
                        <option value="10">October</option>
                        <option value="11">November</option>
                        <option value="12">December</option>
                    </select>
                </div><div class="col-lg-4" style="padding-left:20px;">
                    <select id="form_dob_year" name="dob_year" class="form-control" style="margin-left: -5px;width: 78px;">
                        <option value="<?php echo date('Y' , $model->dob); ?>" selected><?php echo date('Y' , $model->dob); ?></option>
                        <option value="2014">2014</option>
                        <option value="2013">2013</option>
                        <option value="2012">2012</option>
                        <option value="2011">2011</option>
                        <option value="2010">2010</option>
                        <option value="2009">2009</option>
                        <option value="2008">2008</option>
                        <option value="2007">2007</option>
                        <option value="2006">2006</option>
                        <option value="2005">2005</option>
                        <option value="2004">2004</option>
                        <option value="2003">2003</option>
                        <option value="2002">2002</option>
                        <option value="2001">2001</option>
                        <option value="2000">2000</option>
                        <option value="1999">1999</option>
                        <option value="1998">1998</option>
                        <option value="1997">1997</option>
                        <option value="1996">1996</option>
                        <option value="1995">1995</option>
                        <option value="1994">1994</option>
                        <option value="1993">1993</option>
                        <option value="1992">1992</option>
                        <option value="1991">1991</option>
                        <option value="1990">1990</option>
                        <option value="1989">1989</option>
                        <option value="1988">1988</option>
                        <option value="1987">1987</option>
                        <option value="1986">1986</option>
                        <option value="1985">1985</option>
                        <option value="1984">1984</option>
                        <option value="1983">1983</option>
                        <option value="1982">1982</option>
                        <option value="1981">1981</option>
                        <option value="1980">1980</option>
                        <option value="1979">1979</option>
                        <option value="1978">1978</option>
                        <option value="1977">1977</option>
                        <option value="1976">1976</option>
                        <option value="1975">1975</option>
                        <option value="1974">1974</option>
                        <option value="1973">1973</option>
                        <option value="1972">1972</option>
                        <option value="1971">1971</option>
                        <option value="1970">1970</option>
                        <option value="1969">1969</option>
                        <option value="1968">1968</option>
                        <option value="1967">1967</option>
                        <option value="1966">1966</option>
                        <option value="1965">1965</option>
                        <option value="1964">1964</option>
                        <option value="1963">1963</option>
                        <option value="1962">1962</option>
                        <option value="1961">1961</option>
                        <option value="1960">1960</option>
                        <option value="1959">1959</option>
                        <option value="1958">1958</option>
                        <option value="1957">1957</option>
                        <option value="1956">1956</option>
                        <option value="1955">1955</option>
                        <option value="1954">1954</option>
                        <option value="1953">1953</option>
                        <option value="1952">1952</option>
                        <option value="1951">1951</option>
                        <option value="1950">1950</option>
                        <option value="1949">1949</option>
                        <option value="1948">1948</option>
                        </select> 
                </div>
            </div>
        </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" style="padding-left:30px;">
<div class="form-group">
     <?php echo CHtml::activeDropDownList($contract, 'employee_type', array('full_time' => 'Full Time Employment', 'part_time' => 'Part Time Employment'), array('class' => 'form-control')); ?>
</div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'phone'); ?>
        <?php echo $form->textField($model, 'phone', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
        <?php echo $form->error($model, 'phone', array('class' => 'text-red')); ?>
    </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" style="padding-left:30px;">
    <div class="form-group">
        <?php echo $form->labelEx($model, 'addressline_1'); ?>
        <?php echo $form->textField($model, 'addressline_1', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
        <?php echo $form->error($model, 'addressline_1', array('class' => 'text-red')); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'street'); ?>
        <?php echo $form->textField($model, 'street', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
        <?php echo $form->error($model, 'street', array('class' => 'text-red')); ?>
    </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" style="padding-left:30px;">
    <div class="form-group">
        <?php echo $form->labelEx($model, 'city'); ?>
        <?php echo $form->textField($model, 'city', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
        <?php echo $form->error($model, 'city', array('class' => 'text-red')); ?>
    </div>

    <div class="form-group">
         <label>Country</label>
            <?php echo $form->dropDownList($model, 'country',  AppInterface::getCountries(), array('class' => 'form-control')); ?>
       
    </div>
            </div>
        </div>


<div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-9">
    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary')); ?>
</div>
                </div>
</div>
<?php $this->endWidget(); ?>
    </div>
</div>