<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle = Yii::app()->name . ' - Change Password';
$this->breadcrumbs = array(
    'Change Password',
);
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'change-form',
    'enableAjaxValidation' => false,
        ));
?>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Change Password
        </div>
    </div>
    <div class="portlet-body form">
        <div class="row" style="padding-top:10px;">
        </div>
        <div style="padding-left:20px;padding-right:20px;">
<!--<h2>Change <?php // echo $model->full_name?>'s password</h2>-->
            <div class="form-group" style="text-align:center;">


            
                <div class="form-group">
                    <input type="password" id="User_password" name="User[password]" class="form-control signin-p" placeholder="Password" required="required">
                </div>
<div class="clearfix"></div>

            </div>
        </div>                
                <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                    <?php echo CHtml::submitButton('Continue', array('class' => 'btn btn-primary signin-submit1')); ?>
                </div><!--col-sm-5 signin-submit-->
                </div>
                </div>
    </div>
</div>
<?php $this->endWidget(); ?>
