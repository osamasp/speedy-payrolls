<?php
$this->pageTitle = Yii::app()->name . ' - Login';
$this->breadcrumbs = array(
    'Login',
);
?>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/user-main-verifyaccount.js" type="text/javascript"></script>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pages/css/coming-soon.css" rel="stylesheet" type="text/css"/>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/thirdparty/js/countdown/jquery.countdown.min.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/thirdparty/js/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/css/pages/scripts/coming-soon.js" type="text/javascript"></script>
<div class="container">
	<div class="row"><input type="hidden" id="base" value="<?php echo Yii::app()->theme->baseUrl; ?>"/>
	</div>
    
	<div class="row">
		<div class="col-md-6 coming-soon-content">
			<h1><?php echo $title; ?></h1>
			<p>
			<?php echo $message; ?>
			</p>
		</div>
		<div class="col-md-6 coming-soon-countdown">
			<div id="defaultCountdown">
			</div>
		</div>
	</div>
	<!--/end row-->
	<div class="row">
		<div class="col-md-12 coming-soon-footer">
			 2019 &copy; Seminant Solutions Ltd.
		</div>
	</div>
</div>
