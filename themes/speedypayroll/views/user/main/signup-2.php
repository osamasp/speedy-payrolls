<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle = Yii::app()->name . ' - Register';
$this->breadcrumbs = array(
    'Login',
);
?>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'login-form',
    'enableClientValidation' => true,
    'htmlOptions' => array('enctype' => 'multipart/form-data')
        ));
?>
<div class="login-box-body">
    <p class="login-box-msg">Complete Your Profile</p>

    <div class="col-lg-4">
        <div id="preview" class="img-preview">
        </div>
        <div class="form-group has-feedback">
            <span class="btn btn-primary btn-file">
                Browse <?php echo $form->fileField($model, 'photo'); ?>
            </span>
        </div>
    </div>
    <p class="note" style="margin-left: 167px;">Fields with <span class="required">*</span> are required.</p>
    <div class="col-lg-8">

        <div class="form-group has-feedback">
            <label><?php echo $error ? $error : ''; ?></label>
        </div>      
        <div class="form-group has-feedback">
            <label>First Name*</label>
            <?php echo $form->textField($model, 'first_name', array('placeholder' => 'First Name *', 'class' => 'form-control', 'required' => 'required')); ?>
        </div>
        <div class="form-group has-feedback">
            <label>Last Name*</label>
            <?php echo $form->textField($model, 'last_name', array('placeholder' => 'Last Name *', 'class' => 'form-control', 'required' => 'required')); ?>
        </div>
        <?php if ($invite) { ?>
            <div class="form-group has-feedback">
                <?php echo $form->passwordField($model, 'password', array('placeholder' => 'New Password', 'class' => 'form-control', 'required' => 'required')); ?>
            </div>
            <div class="form-group has-feedback">
                <?php echo $form->passwordField($model, 'password', array('placeholder' => 'Retype Password', 'class' => 'form-control', 'required' => 'required')); ?>
            </div>
        <?php } ?>

        <div class="form-group">
            <label>Date Of Birth*</label>
            <div class="row">
                <div class="col-lg-4">
                    <select id="form_dob_day" name="dob_day" class="form-control" required="">
                        <!--<option></option>-->
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">14</option>
                        <option value="15">15</option>
                        <option value="16">16</option>
                        <option value="17">17</option>
                        <option value="18">18</option>
                        <option value="19">19</option>
                        <option value="20">20</option>
                        <option value="21">21</option>
                        <option value="22">22</option>
                        <option value="23">23</option>
                        <option value="24">24</option>
                        <option value="25">25</option>
                        <option value="26">26</option>
                        <option value="27">27</option>
                        <option value="28">28</option>
                        <option value="29">29</option>
                        <option value="30">30</option>
                        <option value="31">31</option>
                    </select>
                </div><div class="col-lg-4">
                    <select id="form_dob_month" name="dob_month" class="form-control" required="" style="width: 100px;margin-left: -25px;">                        
                        <!--<option></option>-->
                        <option value="1">January</option>
                        <option value="2">Febuary</option>
                        <option value="3">March</option>
                        <option value="4">April</option>
                        <option value="5">May</option>
                        <option value="6">June</option>
                        <option value="7">July</option>
                        <option value="8">August</option>
                        <option value="9">September</option>
                        <option value="10">October</option>
                        <option value="11">November</option>
                        <option value="12">December</option>
                    </select>
                </div><div class="col-lg-4">
                    <select id="form_dob_year" name="dob_year" class="form-control" required="" style="margin-left: -15px;width: 78px;">
                        <!--<option></option>-->
                        <option value="2014">2014</option>
                        <option value="2013">2013</option>
                        <option value="2012">2012</option>
                        <option value="2011">2011</option>
                        <option value="2010">2010</option>
                        <option value="2009">2009</option>
                        <option value="2008">2008</option>
                        <option value="2007">2007</option>
                        <option value="2006">2006</option>
                        <option value="2005">2005</option>
                        <option value="2004">2004</option>
                        <option value="2003">2003</option>
                        <option value="2002">2002</option>
                        <option value="2001">2001</option>
                        <option value="2000">2000</option>
                        <option value="1999">1999</option>
                        <option value="1998">1998</option>
                        <option value="1997">1997</option>
                        <option value="1996">1996</option>
                        <option value="1995">1995</option>
                        <option value="1994">1994</option>
                        <option value="1993">1993</option>
                        <option value="1992">1992</option>
                        <option value="1991">1991</option>
                        <option value="1990">1990</option>
                        <option value="1989">1989</option>
                        <option value="1988">1988</option>
                        <option value="1987">1987</option>
                        <option value="1986">1986</option>
                        <option value="1985">1985</option>
                        <option value="1984">1984</option>
                        <option value="1983">1983</option>
                        <option value="1982">1982</option>
                        <option value="1981">1981</option>
                        <option value="1980">1980</option>
                        <option value="1979">1979</option>
                        <option value="1978">1978</option>
                        <option value="1977">1977</option>
                        <option value="1976">1976</option>
                        <option value="1975">1975</option>
                        <option value="1974">1974</option>
                        <option value="1973">1973</option>
                        <option value="1972">1972</option>
                        <option value="1971">1971</option>
                        <option value="1970">1970</option>
                        <option value="1969">1969</option>
                        <option value="1968">1968</option>
                        <option value="1967">1967</option>
                        <option value="1966">1966</option>
                        <option value="1965">1965</option>
                        <option value="1964">1964</option>
                        <option value="1963">1963</option>
                        <option value="1962">1962</option>
                        <option value="1961">1961</option>
                        <option value="1960">1960</option>
                        <option value="1959">1959</option>
                        <option value="1958">1958</option>
                        <option value="1957">1957</option>
                        <option value="1956">1956</option>
                        <option value="1955">1955</option>
                        <option value="1954">1954</option>
                        <option value="1953">1953</option>
                        <option value="1952">1952</option>
                        <option value="1951">1951</option>
                        <option value="1950">1950</option>
                        <option value="1949">1949</option>
                        <option value="1948">1948</option>
                        </select> 
                </div>
            </div>
        </div>


        <div class="form-group has-feedback">
            <label>Address*</label>
            <?php echo $form->textField($model, 'addressline_1', array('placeholder' => 'Address Line 1', 'class' => 'form-control', 'required' => 'required')); ?>

        </div>
        <div class="form-group has-feedback">
            <label>Street*</label>
            <?php echo $form->textField($model, 'street', array('placeholder' => 'Street', 'class' => 'form-control', 'required' => 'required')); ?>
        </div>
        <div class="form-group has-feedback">
            <label>Country*</label>
            <select name="User[country]" class="form-control" required="">
                <option value="Afghanistan">Afghanistan</option>
                <option value="Albania">Albania</option>
                <option value="Algeria">Algeria</option>
                <option value="American Samoa">American Samoa</option>
                <option value="Andorra">Andorra</option>
                <option value="Angola">Angola</option>
                <option value="Anguilla">Anguilla</option>
                <option value="Antartica">Antarctica</option>
                <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                <option value="Argentina">Argentina</option>
                <option value="Armenia">Armenia</option>
                <option value="Aruba">Aruba</option>
                <option value="Australia">Australia</option>
                <option value="Austria">Austria</option>
                <option value="Azerbaijan">Azerbaijan</option>
                <option value="Bahamas">Bahamas</option>
                <option value="Bahrain">Bahrain</option>
                <option value="Bangladesh">Bangladesh</option>
                <option value="Barbados">Barbados</option>
                <option value="Belarus">Belarus</option>
                <option value="Belgium">Belgium</option>
                <option value="Belize">Belize</option>
                <option value="Benin">Benin</option>
                <option value="Bermuda">Bermuda</option>
                <option value="Bhutan">Bhutan</option>
                <option value="Bolivia">Bolivia</option>
                <option value="Bosnia and Herzegowina">Bosnia and Herzegowina</option>
                <option value="Botswana">Botswana</option>
                <option value="Bouvet Island">Bouvet Island</option>
                <option value="Brazil">Brazil</option>
                <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                <option value="Brunei Darussalam">Brunei Darussalam</option>
                <option value="Bulgaria">Bulgaria</option>
                <option value="Burkina Faso">Burkina Faso</option>
                <option value="Burundi">Burundi</option>
                <option value="Cambodia">Cambodia</option>
                <option value="Cameroon">Cameroon</option>
                <option value="Canada">Canada</option>
                <option value="Cape Verde">Cape Verde</option>
                <option value="Cayman Islands">Cayman Islands</option>
                <option value="Central African Republic">Central African Republic</option>
                <option value="Chad">Chad</option>
                <option value="Chile">Chile</option>
                <option value="China">China</option>
                <option value="Christmas Island">Christmas Island</option>
                <option value="Cocos Islands">Cocos (Keeling) Islands</option>
                <option value="Colombia">Colombia</option>
                <option value="Comoros">Comoros</option>
                <option value="Congo">Congo</option>
                <option value="Congo">Congo, the Democratic Republic of the</option>
                <option value="Cook Islands">Cook Islands</option>
                <option value="Costa Rica">Costa Rica</option>
                <option value="Cota D'Ivoire">Cote d'Ivoire</option>
                <option value="Croatia">Croatia (Hrvatska)</option>
                <option value="Cuba">Cuba</option>
                <option value="Cyprus">Cyprus</option>
                <option value="Czech Republic">Czech Republic</option>
                <option value="Denmark">Denmark</option>
                <option value="Djibouti">Djibouti</option>
                <option value="Dominica">Dominica</option>
                <option value="Dominican Republic">Dominican Republic</option>
                <option value="East Timor">East Timor</option>
                <option value="Ecuador">Ecuador</option>
                <option value="Egypt">Egypt</option>
                <option value="El Salvador">El Salvador</option>
                <option value="Equatorial Guinea">Equatorial Guinea</option>
                <option value="Eritrea">Eritrea</option>
                <option value="Estonia">Estonia</option>
                <option value="Ethiopia">Ethiopia</option>
                <option value="Falkland Islands">Falkland Islands (Malvinas)</option>
                <option value="Faroe Islands">Faroe Islands</option>
                <option value="Fiji">Fiji</option>
                <option value="Finland">Finland</option>
                <option value="France">France</option>
                <option value="France Metropolitan">France, Metropolitan</option>
                <option value="French Guiana">French Guiana</option>
                <option value="French Polynesia">French Polynesia</option>
                <option value="French Southern Territories">French Southern Territories</option>
                <option value="Gabon">Gabon</option>
                <option value="Gambia">Gambia</option>
                <option value="Georgia">Georgia</option>
                <option value="Germany">Germany</option>
                <option value="Ghana">Ghana</option>
                <option value="Gibraltar">Gibraltar</option>
                <option value="Greece">Greece</option>
                <option value="Greenland">Greenland</option>
                <option value="Grenada">Grenada</option>
                <option value="Guadeloupe">Guadeloupe</option>
                <option value="Guam">Guam</option>
                <option value="Guatemala">Guatemala</option>
                <option value="Guinea">Guinea</option>
                <option value="Guinea-Bissau">Guinea-Bissau</option>
                <option value="Guyana">Guyana</option>
                <option value="Haiti">Haiti</option>
                <option value="Heard and McDonald Islands">Heard and Mc Donald Islands</option>
                <option value="Holy See">Holy See (Vatican City State)</option>
                <option value="Honduras">Honduras</option>
                <option value="Hong Kong">Hong Kong</option>
                <option value="Hungary">Hungary</option>
                <option value="Iceland">Iceland</option>
                <option value="India">India</option>
                <option value="Indonesia">Indonesia</option>
                <option value="Iran">Iran (Islamic Republic of)</option>
                <option value="Iraq">Iraq</option>
                <option value="Ireland">Ireland</option>
                <option value="Israel">Israel</option>
                <option value="Italy">Italy</option>
                <option value="Jamaica">Jamaica</option>
                <option value="Japan">Japan</option>
                <option value="Jordan">Jordan</option>
                <option value="Kazakhstan">Kazakhstan</option>
                <option value="Kenya">Kenya</option>
                <option value="Kiribati">Kiribati</option>
                <option value="Democratic People's Republic of Korea">Korea, Democratic People's Republic of</option>
                <option value="Korea">Korea, Republic of</option>
                <option value="Kuwait">Kuwait</option>
                <option value="Kyrgyzstan">Kyrgyzstan</option>
                <option value="Lao">Lao People's Democratic Republic</option>
                <option value="Latvia">Latvia</option>
                <option value="Lebanon">Lebanon</option>
                <option value="Lesotho">Lesotho</option>
                <option value="Liberia">Liberia</option>
                <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                <option value="Liechtenstein">Liechtenstein</option>
                <option value="Lithuania">Lithuania</option>
                <option value="Luxembourg">Luxembourg</option>
                <option value="Macau">Macau</option>
                <option value="Macedonia">Macedonia, The Former Yugoslav Republic of</option>
                <option value="Madagascar">Madagascar</option>
                <option value="Malawi">Malawi</option>
                <option value="Malaysia">Malaysia</option>
                <option value="Maldives">Maldives</option>
                <option value="Mali">Mali</option>
                <option value="Malta">Malta</option>
                <option value="Marshall Islands">Marshall Islands</option>
                <option value="Martinique">Martinique</option>
                <option value="Mauritania">Mauritania</option>
                <option value="Mauritius">Mauritius</option>
                <option value="Mayotte">Mayotte</option>
                <option value="Mexico">Mexico</option>
                <option value="Micronesia">Micronesia, Federated States of</option>
                <option value="Moldova">Moldova, Republic of</option>
                <option value="Monaco">Monaco</option>
                <option value="Mongolia">Mongolia</option>
                <option value="Montserrat">Montserrat</option>
                <option value="Morocco">Morocco</option>
                <option value="Mozambique">Mozambique</option>
                <option value="Myanmar">Myanmar</option>
                <option value="Namibia">Namibia</option>
                <option value="Nauru">Nauru</option>
                <option value="Nepal">Nepal</option>
                <option value="Netherlands">Netherlands</option>
                <option value="Netherlands Antilles">Netherlands Antilles</option>
                <option value="New Caledonia">New Caledonia</option>
                <option value="New Zealand">New Zealand</option>
                <option value="Nicaragua">Nicaragua</option>
                <option value="Niger">Niger</option>
                <option value="Nigeria">Nigeria</option>
                <option value="Niue">Niue</option>
                <option value="Norfolk Island">Norfolk Island</option>
                <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                <option value="Norway">Norway</option>
                <option value="Oman">Oman</option>
                <option value="Pakistan">Pakistan</option>
                <option value="Palau">Palau</option>
                <option value="Panama">Panama</option>
                <option value="Papua New Guinea">Papua New Guinea</option>
                <option value="Paraguay">Paraguay</option>
                <option value="Peru">Peru</option>
                <option value="Philippines">Philippines</option>
                <option value="Pitcairn">Pitcairn</option>
                <option value="Poland">Poland</option>
                <option value="Portugal">Portugal</option>
                <option value="Puerto Rico">Puerto Rico</option>
                <option value="Qatar">Qatar</option>
                <option value="Reunion">Reunion</option>
                <option value="Romania">Romania</option>
                <option value="Russia">Russian Federation</option>
                <option value="Rwanda">Rwanda</option>
                <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                <option value="Saint LUCIA">Saint LUCIA</option>
                <option value="Saint Vincent">Saint Vincent and the Grenadines</option>
                <option value="Samoa">Samoa</option>
                <option value="San Marino">San Marino</option>
                <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                <option value="Saudi Arabia">Saudi Arabia</option>
                <option value="Senegal">Senegal</option>
                <option value="Seychelles">Seychelles</option>
                <option value="Sierra">Sierra Leone</option>
                <option value="Singapore">Singapore</option>
                <option value="Slovakia">Slovakia (Slovak Republic)</option>
                <option value="Slovenia">Slovenia</option>
                <option value="Solomon Islands">Solomon Islands</option>
                <option value="Somalia">Somalia</option>
                <option value="South Africa">South Africa</option>
                <option value="South Georgia">South Georgia and the South Sandwich Islands</option>
                <option value="Span">Spain</option>
                <option value="SriLanka">Sri Lanka</option>
                <option value="St. Helena">St. Helena</option>
                <option value="St. Pierre and Miguelon">St. Pierre and Miquelon</option>
                <option value="Sudan">Sudan</option>
                <option value="Suriname">Suriname</option>
                <option value="Svalbard">Svalbard and Jan Mayen Islands</option>
                <option value="Swaziland">Swaziland</option>
                <option value="Sweden">Sweden</option>
                <option value="Switzerland">Switzerland</option>
                <option value="Syria">Syrian Arab Republic</option>
                <option value="Taiwan">Taiwan, Province of China</option>
                <option value="Tajikistan">Tajikistan</option>
                <option value="Tanzania">Tanzania, United Republic of</option>
                <option value="Thailand">Thailand</option>
                <option value="Togo">Togo</option>
                <option value="Tokelau">Tokelau</option>
                <option value="Tonga">Tonga</option>
                <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                <option value="Tunisia">Tunisia</option>
                <option value="Turkey">Turkey</option>
                <option value="Turkmenistan">Turkmenistan</option>
                <option value="Turks and Caicos">Turks and Caicos Islands</option>
                <option value="Tuvalu">Tuvalu</option>
                <option value="Uganda">Uganda</option>
                <option value="Ukraine">Ukraine</option>
                <option value="United Arab Emirates">United Arab Emirates</option>
                <option value="United Kingdom" selected>United Kingdom</option>
                <option value="United States">United States</option>
                <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                <option value="Uruguay">Uruguay</option>
                <option value="Uzbekistan">Uzbekistan</option>
                <option value="Vanuatu">Vanuatu</option>
                <option value="Venezuela">Venezuela</option>
                <option value="Vietnam">Viet Nam</option>
                <option value="Virgin Islands (British)">Virgin Islands (British)</option>
                <option value="Virgin Islands (U.S)">Virgin Islands (U.S.)</option>
                <option value="Wallis and Futana Islands">Wallis and Futuna Islands</option>
                <option value="Western Sahara">Western Sahara</option>
                <option value="Yemen">Yemen</option>
                <option value="Yugoslavia">Yugoslavia</option>
                <option value="Zambia">Zambia</option>
                <option value="Zimbabwe">Zimbabwe</option>
            </select>
        </div>
        <div class="form-group has-feedback">
            <div class="col-xs-6" style="padding-left: 0;"><label>City*</label><?php echo $form->textField($model, 'city', array('placeholder' => 'City', 'class' => 'form-control')); ?></div>
            <div class="col-xs-6" style="padding-right: 0;"><label>Postcode*</label><?php echo $form->textField($model, 'postcode', array('placeholder' => 'Postcode', 'class' => 'form-control')); ?></div>
            <div class="clearfix"></div>
        </div>
        <script type="text/javascript">
            function isNumberKey(evt)
            {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode != 46 && charCode > 31
                        && (charCode < 48 || charCode > 57))
                    return false;

                return true;
            }
        </script>

        <div class="form-group has-feedback">
            <label>Phone Number*</label>
            <?php echo $form->textField($model, 'phone', array('placeholder' => 'Phone Number', 'class' => 'form-control', 'required' => 'required')); ?>
        </div>
        <div class="form-group has-feedback">
            <label>NI Number*</label>
            <?php echo $form->textField($model, 'ni_number', array('placeholder' => 'NI Number', 'class' => 'form-control', 'required' => 'required')); ?>
        </div>
        <?php if (!$invite && $invitation_contract == NULL) { ?>
            <div class="form-group has-feedback">
                <label>Employment Type*</label>
                <select id="etype" name="Settings[type]" class="form-control" required="">
                    <option value="">Select Employment Type</option>
                    <option value="1">Employed by a Company</option>
                    <option value="2">Self Employed or Sole Trader</option>
                    <option value="3">Employed by Agency</option>
                </select>
            </div>

            <div id="company" style="display: none;">
                <div class="form-group has-feedback">
                    <label>Invitation Email Address*</label>
                    <input type="text" name="Settings[invite_email]" class="form-control" placeholder="Invitation Email Address">
                </div>
            </div>
            <div id="self" style="display: none;">
                <div class="form-group has-feedback">
                    <label>Select Role*</label>
                    <select name="Settings[self_role]" class="form-control" id="self_role">
                        <option value="">Select Your Role</option>
                        <option value="Director">Director/Owner</option>
                        <option value="Senior Manager">Senior Manager</option>
                        <option value="1">Other</option>
                        <!--                        <option value="Other">Other</option>-->
                    </select>

                </div>
                <div id="show-input" style="display: none;" class="form-group">
                    <input type="text" class="form-control" placeholder="Describe" name="describe_position" id="describe_position">

                </div>
                <script type="text/javascript">
                    $(function () {
                        $('#self_role').on('change', function () {
                            if ($(this).val() == 1) {
                                document.getElementById("show-input").style.display = 'block';
                            }
                            else {
                                document.getElementById("show-input").style.display = 'none';
                            }
                        });
                    });
                </script>
                <h4>Company Details</h4>
                <div class="form-group has-feedback">
                    <label>Company Name*</label>
                    <input type="text" name="Company[name]" class="form-control" placeholder="Name" value="<?php echo isset($invitation_contract) ? $invitation_contract->company->name : ''; ?>">
                </div>
                <div class="form-group has-feedback">
                    <label>Company Registration No.*</label>
                    <input type="text" name="Company[registration_number]" class="form-control" placeholder="Company Registration Number">
                </div>
                <div class="form-group has-feedback">
                    <label>Company VAT No.*</label>
                    <input type="text" name="Company[vat_number]" class="form-control" placeholder="VAT Number">
                </div>
                <div class="form-group has-feedback">
                    <label>Company Address*</label>
                    <input type="text" name="Company[address]" class="form-control" placeholder="Address Line 1">
                </div>
                <div class="form-group has-feedback">
                    <label>Company Street*</label>
                    <input type="text" name="Company[street]" class="form-control" placeholder="Street">
                </div>
                <div class="form-group has-feedback">
                    <label>Country*</label>
                    <select name="Company[country]" class="form-control">
                        <option value="Afghanistan">Afghanistan</option>
                        <option value="Albania">Albania</option>
                        <option value="Algeria">Algeria</option>
                        <option value="American Samoa">American Samoa</option>
                        <option value="Andorra">Andorra</option>
                        <option value="Angola">Angola</option>
                        <option value="Anguilla">Anguilla</option>
                        <option value="Antartica">Antarctica</option>
                        <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                        <option value="Argentina">Argentina</option>
                        <option value="Armenia">Armenia</option>
                        <option value="Aruba">Aruba</option>
                        <option value="Australia">Australia</option>
                        <option value="Austria">Austria</option>
                        <option value="Azerbaijan">Azerbaijan</option>
                        <option value="Bahamas">Bahamas</option>
                        <option value="Bahrain">Bahrain</option>
                        <option value="Bangladesh">Bangladesh</option>
                        <option value="Barbados">Barbados</option>
                        <option value="Belarus">Belarus</option>
                        <option value="Belgium">Belgium</option>
                        <option value="Belize">Belize</option>
                        <option value="Benin">Benin</option>
                        <option value="Bermuda">Bermuda</option>
                        <option value="Bhutan">Bhutan</option>
                        <option value="Bolivia">Bolivia</option>
                        <option value="Bosnia and Herzegowina">Bosnia and Herzegowina</option>
                        <option value="Botswana">Botswana</option>
                        <option value="Bouvet Island">Bouvet Island</option>
                        <option value="Brazil">Brazil</option>
                        <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                        <option value="Brunei Darussalam">Brunei Darussalam</option>
                        <option value="Bulgaria">Bulgaria</option>
                        <option value="Burkina Faso">Burkina Faso</option>
                        <option value="Burundi">Burundi</option>
                        <option value="Cambodia">Cambodia</option>
                        <option value="Cameroon">Cameroon</option>
                        <option value="Canada">Canada</option>
                        <option value="Cape Verde">Cape Verde</option>
                        <option value="Cayman Islands">Cayman Islands</option>
                        <option value="Central African Republic">Central African Republic</option>
                        <option value="Chad">Chad</option>
                        <option value="Chile">Chile</option>
                        <option value="China">China</option>
                        <option value="Christmas Island">Christmas Island</option>
                        <option value="Cocos Islands">Cocos (Keeling) Islands</option>
                        <option value="Colombia">Colombia</option>
                        <option value="Comoros">Comoros</option>
                        <option value="Congo">Congo</option>
                        <option value="Congo">Congo, the Democratic Republic of the</option>
                        <option value="Cook Islands">Cook Islands</option>
                        <option value="Costa Rica">Costa Rica</option>
                        <option value="Cota D'Ivoire">Cote d'Ivoire</option>
                        <option value="Croatia">Croatia (Hrvatska)</option>
                        <option value="Cuba">Cuba</option>
                        <option value="Cyprus">Cyprus</option>
                        <option value="Czech Republic">Czech Republic</option>
                        <option value="Denmark">Denmark</option>
                        <option value="Djibouti">Djibouti</option>
                        <option value="Dominica">Dominica</option>
                        <option value="Dominican Republic">Dominican Republic</option>
                        <option value="East Timor">East Timor</option>
                        <option value="Ecuador">Ecuador</option>
                        <option value="Egypt">Egypt</option>
                        <option value="El Salvador">El Salvador</option>
                        <option value="Equatorial Guinea">Equatorial Guinea</option>
                        <option value="Eritrea">Eritrea</option>
                        <option value="Estonia">Estonia</option>
                        <option value="Ethiopia">Ethiopia</option>
                        <option value="Falkland Islands">Falkland Islands (Malvinas)</option>
                        <option value="Faroe Islands">Faroe Islands</option>
                        <option value="Fiji">Fiji</option>
                        <option value="Finland">Finland</option>
                        <option value="France">France</option>
                        <option value="France Metropolitan">France, Metropolitan</option>
                        <option value="French Guiana">French Guiana</option>
                        <option value="French Polynesia">French Polynesia</option>
                        <option value="French Southern Territories">French Southern Territories</option>
                        <option value="Gabon">Gabon</option>
                        <option value="Gambia">Gambia</option>
                        <option value="Georgia">Georgia</option>
                        <option value="Germany">Germany</option>
                        <option value="Ghana">Ghana</option>
                        <option value="Gibraltar">Gibraltar</option>
                        <option value="Greece">Greece</option>
                        <option value="Greenland">Greenland</option>
                        <option value="Grenada">Grenada</option>
                        <option value="Guadeloupe">Guadeloupe</option>
                        <option value="Guam">Guam</option>
                        <option value="Guatemala">Guatemala</option>
                        <option value="Guinea">Guinea</option>
                        <option value="Guinea-Bissau">Guinea-Bissau</option>
                        <option value="Guyana">Guyana</option>
                        <option value="Haiti">Haiti</option>
                        <option value="Heard and McDonald Islands">Heard and Mc Donald Islands</option>
                        <option value="Holy See">Holy See (Vatican City State)</option>
                        <option value="Honduras">Honduras</option>
                        <option value="Hong Kong">Hong Kong</option>
                        <option value="Hungary">Hungary</option>
                        <option value="Iceland">Iceland</option>
                        <option value="India">India</option>
                        <option value="Indonesia">Indonesia</option>
                        <option value="Iran">Iran (Islamic Republic of)</option>
                        <option value="Iraq">Iraq</option>
                        <option value="Ireland">Ireland</option>
                        <option value="Israel">Israel</option>
                        <option value="Italy">Italy</option>
                        <option value="Jamaica">Jamaica</option>
                        <option value="Japan">Japan</option>
                        <option value="Jordan">Jordan</option>
                        <option value="Kazakhstan">Kazakhstan</option>
                        <option value="Kenya">Kenya</option>
                        <option value="Kiribati">Kiribati</option>
                        <option value="Democratic People's Republic of Korea">Korea, Democratic People's Republic of</option>
                        <option value="Korea">Korea, Republic of</option>
                        <option value="Kuwait">Kuwait</option>
                        <option value="Kyrgyzstan">Kyrgyzstan</option>
                        <option value="Lao">Lao People's Democratic Republic</option>
                        <option value="Latvia">Latvia</option>
                        <option value="Lebanon">Lebanon</option>
                        <option value="Lesotho">Lesotho</option>
                        <option value="Liberia">Liberia</option>
                        <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                        <option value="Liechtenstein">Liechtenstein</option>
                        <option value="Lithuania">Lithuania</option>
                        <option value="Luxembourg">Luxembourg</option>
                        <option value="Macau">Macau</option>
                        <option value="Macedonia">Macedonia, The Former Yugoslav Republic of</option>
                        <option value="Madagascar">Madagascar</option>
                        <option value="Malawi">Malawi</option>
                        <option value="Malaysia">Malaysia</option>
                        <option value="Maldives">Maldives</option>
                        <option value="Mali">Mali</option>
                        <option value="Malta">Malta</option>
                        <option value="Marshall Islands">Marshall Islands</option>
                        <option value="Martinique">Martinique</option>
                        <option value="Mauritania">Mauritania</option>
                        <option value="Mauritius">Mauritius</option>
                        <option value="Mayotte">Mayotte</option>
                        <option value="Mexico">Mexico</option>
                        <option value="Micronesia">Micronesia, Federated States of</option>
                        <option value="Moldova">Moldova, Republic of</option>
                        <option value="Monaco">Monaco</option>
                        <option value="Mongolia">Mongolia</option>
                        <option value="Montserrat">Montserrat</option>
                        <option value="Morocco">Morocco</option>
                        <option value="Mozambique">Mozambique</option>
                        <option value="Myanmar">Myanmar</option>
                        <option value="Namibia">Namibia</option>
                        <option value="Nauru">Nauru</option>
                        <option value="Nepal">Nepal</option>
                        <option value="Netherlands">Netherlands</option>
                        <option value="Netherlands Antilles">Netherlands Antilles</option>
                        <option value="New Caledonia">New Caledonia</option>
                        <option value="New Zealand">New Zealand</option>
                        <option value="Nicaragua">Nicaragua</option>
                        <option value="Niger">Niger</option>
                        <option value="Nigeria">Nigeria</option>
                        <option value="Niue">Niue</option>
                        <option value="Norfolk Island">Norfolk Island</option>
                        <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                        <option value="Norway">Norway</option>
                        <option value="Oman">Oman</option>
                        <option value="Pakistan">Pakistan</option>
                        <option value="Palau">Palau</option>
                        <option value="Panama">Panama</option>
                        <option value="Papua New Guinea">Papua New Guinea</option>
                        <option value="Paraguay">Paraguay</option>
                        <option value="Peru">Peru</option>
                        <option value="Philippines">Philippines</option>
                        <option value="Pitcairn">Pitcairn</option>
                        <option value="Poland">Poland</option>
                        <option value="Portugal">Portugal</option>
                        <option value="Puerto Rico">Puerto Rico</option>
                        <option value="Qatar">Qatar</option>
                        <option value="Reunion">Reunion</option>
                        <option value="Romania">Romania</option>
                        <option value="Russia">Russian Federation</option>
                        <option value="Rwanda">Rwanda</option>
                        <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                        <option value="Saint LUCIA">Saint LUCIA</option>
                        <option value="Saint Vincent">Saint Vincent and the Grenadines</option>
                        <option value="Samoa">Samoa</option>
                        <option value="San Marino">San Marino</option>
                        <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                        <option value="Saudi Arabia">Saudi Arabia</option>
                        <option value="Senegal">Senegal</option>
                        <option value="Seychelles">Seychelles</option>
                        <option value="Sierra">Sierra Leone</option>
                        <option value="Singapore">Singapore</option>
                        <option value="Slovakia">Slovakia (Slovak Republic)</option>
                        <option value="Slovenia">Slovenia</option>
                        <option value="Solomon Islands">Solomon Islands</option>
                        <option value="Somalia">Somalia</option>
                        <option value="South Africa">South Africa</option>
                        <option value="South Georgia">South Georgia and the South Sandwich Islands</option>
                        <option value="Span">Spain</option>
                        <option value="SriLanka">Sri Lanka</option>
                        <option value="St. Helena">St. Helena</option>
                        <option value="St. Pierre and Miguelon">St. Pierre and Miquelon</option>
                        <option value="Sudan">Sudan</option>
                        <option value="Suriname">Suriname</option>
                        <option value="Svalbard">Svalbard and Jan Mayen Islands</option>
                        <option value="Swaziland">Swaziland</option>
                        <option value="Sweden">Sweden</option>
                        <option value="Switzerland">Switzerland</option>
                        <option value="Syria">Syrian Arab Republic</option>
                        <option value="Taiwan">Taiwan, Province of China</option>
                        <option value="Tajikistan">Tajikistan</option>
                        <option value="Tanzania">Tanzania, United Republic of</option>
                        <option value="Thailand">Thailand</option>
                        <option value="Togo">Togo</option>
                        <option value="Tokelau">Tokelau</option>
                        <option value="Tonga">Tonga</option>
                        <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                        <option value="Tunisia">Tunisia</option>
                        <option value="Turkey">Turkey</option>
                        <option value="Turkmenistan">Turkmenistan</option>
                        <option value="Turks and Caicos">Turks and Caicos Islands</option>
                        <option value="Tuvalu">Tuvalu</option>
                        <option value="Uganda">Uganda</option>
                        <option value="Ukraine">Ukraine</option>
                        <option value="United Arab Emirates">United Arab Emirates</option>
                        <option value="United Kingdom" selected>United Kingdom</option>
                        <option value="United States">United States</option>
                        <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                        <option value="Uruguay">Uruguay</option>
                        <option value="Uzbekistan">Uzbekistan</option>
                        <option value="Vanuatu">Vanuatu</option>
                        <option value="Venezuela">Venezuela</option>
                        <option value="Vietnam">Viet Nam</option>
                        <option value="Virgin Islands (British)">Virgin Islands (British)</option>
                        <option value="Virgin Islands (U.S)">Virgin Islands (U.S.)</option>
                        <option value="Wallis and Futana Islands">Wallis and Futuna Islands</option>
                        <option value="Western Sahara">Western Sahara</option>
                        <option value="Yemen">Yemen</option>
                        <option value="Yugoslavia">Yugoslavia</option>
                        <option value="Zambia">Zambia</option>
                        <option value="Zimbabwe">Zimbabwe</option>
                    </select>



                </div>
                <div class="form-group has-feedback">
                    <div class="col-xs-6" style="padding-left: 0;"><label>City*</label><input type="text" name="Company[city]" class="form-control" placeholder="City"></div>
                    <div class="col-xs-6" style="padding-right: 0;"><label>Postcode*</label><input type="text" name="Company[post_code]" class="form-control" placeholder="Postcode"></div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group has-feedback">
                    <label>Company Phone*</label>
                    <input type="text" name="Company[phone]" class="form-control" placeholder="Phone Number" >
                </div>
                <div class="form-group has-feedback">
                    <label>Company Sector*</label>
                    <?php
                    echo CHtml::dropDownList('Company[sector]', 'none', array('none' => 'Select Sector',
                        ' Agriculture ' => ' Agriculture ',
                        ' Accounting ' => ' Accounting ',
                        ' Advertising ' => ' Advertising ',
                        ' Aerospace ' => ' Aerospace ',
                        ' Aircraft ' => ' Aircraft ',
                        ' Airline ' => ' Airline ',
                        ' Apparel & Accessories ' => ' Apparel & Accessories ',
                        ' Automotive ' => ' Automotive ',
                        ' Banking ' => ' Banking ',
                        ' Broadcasting ' => ' Broadcasting ',
                        ' Brokerage ' => ' Brokerage ',
                        ' Biotechnology ' => ' Biotechnology ',
                        ' Call Centers ' => ' Call Centers ',
                        ' Cargo Handling ' => ' Cargo Handling ',
                        ' Chemical ' => ' Chemical ',
                        ' Computer ' => ' Computer ',
                        ' Consulting ' => ' Consulting ',
                        ' Consumer Products ' => ' Consumer Products ',
                        ' Cosmetics ' => ' Cosmetics ',
                        ' Defense ' => ' Defense ',
                        ' Department Stores ' => ' Department Stores ',
                        ' Education ' => ' Education ',
                        ' Electronics ' => ' Electronics ',
                        ' Energy ' => ' Energy ',
                        ' Entertainment & Leisure ' => ' Entertainment & Leisure ',
                        ' Executive Search ' => ' Executive Search ',
                        ' Financial Services ' => ' Financial Services ',
                        ' Food, Beverage & Tobacco ' => ' Food, Beverage & Tobacco ',
                        ' Grocery ' => ' Grocery ',
                        ' Health Care ' => ' Health Care ',
                        ' Internet Publishing ' => ' Internet Publishing ',
                        ' Investment Banking ' => ' Investment Banking ',
                        ' Legal ' => ' Legal ',
                        ' Manufacturing ' => ' Manufacturing ',
                        ' Motion Picture & Video ' => ' Motion Picture & Video ',
                        ' Music ' => ' Music ',
                        ' Newspaper Publishers ' => ' Newspaper Publishers ',
                        ' Online Auctions ' => ' Online Auctions ',
                        ' Pension Funds ' => ' Pension Funds ',
                        ' Pharmaceuticals ' => ' Pharmaceuticals ',
                        ' Private Equity ' => ' Private Equity ',
                        ' Publishing ' => ' Publishing ',
                        ' Real Estate ' => ' Real Estate ',
                        ' Retail & Wholesale ' => ' Retail & Wholesale ',
                        ' Securities & Commodity Exchanges ' => ' Securities & Commodity Exchanges ',
                        ' Service ' => ' Service ',
                        ' Soap & Detergent ' => ' Soap & Detergent ',
                        ' Software ' => ' Software ',
                        ' Sports ' => ' Sports ',
                        ' Technology ' => ' Technology ',
                        ' Telecommunications ' => ' Telecommunications ',
                        ' Television ' => ' Television ',
                        ' Transportation ' => ' Transportation ',
                        ' Trucking ' => ' Trucking ',
                        ' Venture Capital ' => ' Venture Capital '), array('placeholder' => 'Company Sector', 'class' => 'form-control'));
                    ?>
                </div>
                
                <div class="form-group has-feedback">
                    <label>Usage*</label>
                    <select name="Company[usage]" id="company_usage" class="form-control" >
                        <option value="0">Speedy Timesheets</option>
                        <option value="1" selected="">Speedy Timesheets & Payrolls</option>
                    </select>
                </div>
                <?php if (isset($payrollers)) { ?>
                    <div class="form-group has-feedback" id='payroller-div'>
                        <label>Pay roller*</label>
                        <select name="Company[payroller_id]" id='payroller' class="form-control">
                            <option value="">Select Pay Roller</option>
                            <?php foreach ($payrollers as $payroller) { ?>
                                <option value=<?php echo $payroller->id; ?> > <?php echo $payroller->first_name . ' ' . $payroller->last_name; ?> </option>
                            <?php } ?>
                        </select>
                    </div>
                <?php } ?>
            </div>
            <div id="agency" style="display: none;">
                <div class="form-group has-feedback">
                    <label>Select Agency*</label>
                    <select name="Settings[agency]" class="form-control">
                        <option value="0">Select Agency</option>
                    </select>
                </div>
                <div class='form-group has-feedback'>
                    <div><label><input type="radio" name="Settings[agency_role]" value="u" checked=""> Full Time</label></div>
                    <div><label><input type="radio" name="Settings[agency_role]" value="c"> Contract Based</label></div>
                </div>
            </div>
        <?php } ?>
    </div>
    <div class="row">
        <div class="col-xs-8"> 

        </div><!-- /.col -->
        <div class="col-xs-4">
            <?php echo CHtml::submitButton('Sign Up', array('class' => 'btn bg-olive btn-block')); ?>
        </div><!-- /.col -->
    </div>
    <div class="clearfix"></div>
</div>


<script type="text/javascript">
    $(function () {
        $('#company_usage').on('change', function () {
            if ($(this).val() == 0) {
                $('#payroller-div').hide(800);
                $('#payroller').removeAttr('required');
            } else {
                $('#payroller-div').show(800)
                $('#payroller').attr('required', 'required');
            }
        });
        $("#User_photo").on("change", function ()
        {
            var files = !!this.files ? this.files : [];
            if (!files.length || !window.FileReader)
                return; // no file selected, or no FileReader support

            if (/^image/.test(files[0].type)) { // only image file
                var reader = new FileReader(); // instance of the FileReader
                reader.readAsDataURL(files[0]); // read the local file

                reader.onloadend = function () { // set image data as background of div
                    $("#preview").css("background-image", "url(" + this.result + ")");
                }
            }
        });
        $('#etype').on('change', function () {
            if ($(this).val() == 1) {

                $("[name='Settings[agency]']").removeAttr('required');

                $("[name='Settings[self_role]']").removeAttr('required');
                $("[name='Company[name]']").removeAttr('required');
                $("[name='Company[registration_number]']").removeAttr('required');
                $("[name='Company[vat_number]']").removeAttr('required');
                $("[name='Company[address]']").removeAttr('required');
                $("[name='Company[street]']").removeAttr('required');
                $("[name='Company[country]']").removeAttr('required');
                $("[name='Company[city]']").removeAttr('required');
                $("[name='Company[post_code]']").removeAttr('required');
                $("[name='Company[phone]']").removeAttr('required');
                $("[name='Company[sector]']").removeAttr('required');
                $("[name='Company[payroller_id]']").removeAttr('required');
                $("[name='Company[usage]']").removeAttr('required');

                $('#self').hide(800);
                $('#agency').hide(800);
                $('#company').show(800);

                $("[name='Settings[invite_email]']").attr('required', '');

            } else if ($(this).val() == 2) {

                $("[name='Settings[invite_email]']").removeAttr('required');

                $("[name='Settings[agency]']").removeAttr('required');

                $('#self').show(800);
                $('#company').hide(800);
                $('#agency').hide(800);

                $("[name='Settings[self_role]']").attr('required', '');
                $("[name='Company[name]']").attr('required', '');
                $("[name='Company[registration_number]']").attr('required', '');
                $("[name='Company[vat_number]']").attr('required', '');
                $("[name='Company[address]']").removeAttr('required');
                $("[name='Company[street]']").attr('required', '');
                $("[name='Company[country]']").attr('required', '');
                $("[name='Company[city]']").attr('required', '');
                $("[name='Company[post_code]']").attr('required', '');
                $("[name='Company[phone]']").attr('required', '');
                $("[name='Company[sector]']").attr('required', '');
                $("[name='Company[payroller_id]']").attr('required', '');
                $("[name='Company[usage]']").attr('required', '');

            } else if ($(this).val() == 3) {

                $("[name='Settings[invite_email]']").removeAttr('required');

                $("[name='Settings[self_role]']").removeAttr('required');
                $("[name='Company[name]']").removeAttr('required');
                $("[name='Company[registration_number]']").removeAttr('required');
                $("[name='Company[vat_number]']").removeAttr('required');
                $("[name='Company[address]']").removeAttr('required');
                $("[name='Company[street]']").removeAttr('required');
                $("[name='Company[country]']").removeAttr('required');
                $("[name='Company[city]']").removeAttr('required');
                $("[name='Company[post_code]']").removeAttr('required');
                $("[name='Company[phone]']").removeAttr('required');
                $("[name='Company[sector]']").removeAttr('required');
                $("[name='Company[payroller_id]']").removeAttr('required');
                $("[name='Company[usage]']").removeAttr('required');

                $('#company').hide(800);
                $('#agency').show(800);
                $('#self').hide(800);

                $("[name='Settings[agency]']").attr('required', '');

            } else {
                $('#company').hide(800);
                $('#agency').hide(800);
                $('#self').hide(800);
            }
        });
    });



</script>

<?php $this->endWidget(); ?>