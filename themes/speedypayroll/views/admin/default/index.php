<?php
/* @var $this DefaultController */

$this->breadcrumbs=array(
	$this->module->id,
);

$this->breadcrumbs = array(
    'Users',
);

$this->menu = array(
    array('label' => 'Create User', 'url' => array('create')),
    array('label' => 'Manage User', 'url' => array('admin')),
);
?>
<div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-users"></i>Users Management
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>

                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">

                        <div class="col-md-12">
                            
                        </div>
                    </div>
                </div>

<div class="box-header">
    <h3 class="box-title">Users</h3>
</div>
<!--<div class="box-tools">
        <a class="btn btn-primary pull-right" style="margin-left:20px;" href="<?php // echo $this->createUrl('add'); ?>">Add Employee</a>
        <p> </p>  <a class="btn btn-primary pull-right" href="<?php // echo $this->createUrl('/contract/main/new'); ?>">Add SpeedyTimesheet account</a>
    </div>-->
                <br><br>
<div class="box-body table-responsive">
    <table id="example1" class="table table-striped table-bordered table-hover dataTable">
        <thead>
            <tr>
                <th><input type="checkbox" name="check" clas="form-control" /></th>
                <th>Name</th>
                <th><?php echo User::model()->getAttributeLabel('ni_number') ?></th>
                <th>Email</th>
                <th>Employee Type</th>
                <th>Company</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            foreach ($dataProvider as $item) { ?>
                <tr class="gradeX">
                    <td><input type="checkbox" name="check" value="<?php echo $item->id; ?>" clas="form-control" /></td>
                    <td><?php echo $item->full_name; ?></td>
                    <td><?php echo $item->ni_number ? $item->ni_number : "-"; ?></td>
                    <td><?php echo $item->email; ?></td>
                   <td><?php if ( $item->type== '0') echo 'Admin'; 
                    else if ($item->type== '1')
                        echo 'User';
                    else if ($item->type== '2')
                        echo 'PayRoller';
                        ?></td>
                   <td><?php 
                   if($icontract = AppContract::getInternalContract($item->id)){
                       echo $icontract->company->name;
                   } else {
                       echo '-';
                   }
                   ?></td>
                    <td>
                        
                        <div class="btn-group">
                                <button class="btn green dropdown-toggle" data-toggle="dropdown">Actions <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="<?php echo $this->createUrl('/user/main/view', array('id' => $item->id)); ?>">View</a></li>
                                    <li><a href="<?php echo $this->createUrl('/user/main/update', array('id' => $item->id)); ?>">Update</a></li>
                                    <li class="divider"></li>
                                    <li><a href="<?php echo $this->createUrl('/contract/main/grid', array('id' => $item->id)); ?>">View Contract</a></li>
                                    <?php if (AppUser::isUserSuperAdmin()) {
                                        ?>
                                        <li class="divider"></li>
                                        <li><a href="<?php echo $this->createUrl('/user/main/loginasuser', array('id' => $item->id)); ?>">Login As</a></li>
                                    <?php } ?>
                                </ul>
                            </div>

                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
            </div>
</div>