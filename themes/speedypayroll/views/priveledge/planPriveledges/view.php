<?php
/* @var $this PlanPriveledgesController */
/* @var $model PlanPriveledges */

$this->breadcrumbs=array(
	'Plan Priveledges'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List PlanPriveledges', 'url'=>array('index')),
array('label'=>'Create PlanPriveledges', 'url'=>array('create')),
array('label'=>'Update PlanPriveledges', 'url'=>array('update', 'id'=>$model->id)),
array('label'=>'Delete PlanPriveledges', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage PlanPriveledges', 'url'=>array('admin')),
);
?>

<div class="box-header">
    <h3 class="box-title">View PlanPriveledges #<?php echo $model->id; ?></h3>
</div>
<div class="table-responsive">
    <div class="box-body">
        <?php $this->widget('zii.widgets.CDetailView', array(
        'data'=>$model,
        'attributes'=>array(
        		'id',
		'plan_id',
		'priveledge_id',
		'created_at',
		'modified_at',
		'created_by',
		'modified_by',
        ),
        )); ?>
    </div>
</div>