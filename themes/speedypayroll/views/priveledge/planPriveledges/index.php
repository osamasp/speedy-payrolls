<?php
/* @var $this PlanPriveledgesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Plan Priveledges',
);

$this->menu=array(
array('label'=>'Create PlanPriveledges', 'url'=>array('create')),
array('label'=>'Manage PlanPriveledges', 'url'=>array('admin')),
);
?>

<div class="box-header">
    <h3 class="box-title">Plan Priveledges</h3>
    <div class="box-tools">
        <a class="btn btn-default pull-right" href="<?php echo $this->createUrl('create'); ?>">Create PlanPriveledges</a>
    </div>
</div>

<div class="box-body table-responsive">
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                                                        <th><?php echo PlanPriveledges::model()->getAttributeLabel('id') ?></th>
                                                        <th><?php echo PlanPriveledges::model()->getAttributeLabel('plan_id') ?></th>
                                                        <th><?php echo PlanPriveledges::model()->getAttributeLabel('priveledge_id') ?></th>
                                                        <th><?php echo PlanPriveledges::model()->getAttributeLabel('created_at') ?></th>
                                                        <th><?php echo PlanPriveledges::model()->getAttributeLabel('modified_at') ?></th>
                                                        <th><?php echo PlanPriveledges::model()->getAttributeLabel('created_by') ?></th>
                                                        <th><?php echo PlanPriveledges::model()->getAttributeLabel('modified_by') ?></th>
                                    <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($dataProvider as $item) { ?>
            <tr>
                                                        <td><?php echo $item->id; ?></td>
                                                        <td><?php echo $item->plan_id; ?></td>
                                                        <td><?php echo $item->priveledge_id; ?></td>
                                                        <td><?php echo $item->created_at; ?></td>
                                                        <td><?php echo $item->modified_at; ?></td>
                                                        <td><?php echo $item->created_by; ?></td>
                                                        <td><?php echo $item->modified_by; ?></td>
                                    <td>
                        
                        <div class="btn-group">
                            <a class="btn btn-primary" style="padding-top: 0px;padding-bottom: 0px;" href="<?php echo $this->createUrl('view', array('id' => $item->id)); ?>">View</a>
                            <button type="button" class="btn btn-primary dropdown-toggle" style="padding-top: 0px;padding-bottom: 0px;" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php echo $this->createUrl('update', array('id' => $item->id)); ?>">Update</a></li>
                                <li class="divider"></li>
                                <li><a href="<?php echo $this->createUrl('delete', array('id' => $item->id)); ?>">Delete</a></li>
                            </ul>
                        </div>
                        
                    </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>