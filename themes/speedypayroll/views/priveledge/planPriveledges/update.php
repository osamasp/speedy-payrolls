<?php
/* @var $this PlanPriveledgesController */
/* @var $model PlanPriveledges */

$this->breadcrumbs=array(
	'Plan Priveledges'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PlanPriveledges', 'url'=>array('index')),
	array('label'=>'Create PlanPriveledges', 'url'=>array('create')),
	array('label'=>'View PlanPriveledges', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PlanPriveledges', 'url'=>array('admin')),
);
?>

<div class="box-header">
    <h3 class="box-title">Update PlanPriveledges <?php echo $model->id; ?></h3>
</div>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>