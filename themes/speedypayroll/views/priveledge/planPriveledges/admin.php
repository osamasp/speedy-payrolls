<?php
/* @var $this PlanPriveledgesController */
/* @var $model PlanPriveledges */

$this->breadcrumbs=array(
	'Plan Priveledges'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List PlanPriveledges', 'url'=>array('index')),
	array('label'=>'Create PlanPriveledges', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#plan-priveledges-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="box-header">
    <h3 class="box-title">Manage Plan Priveledges</h3>
</div>
<div class="box-body">
<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'plan-priveledges-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'plan_id',
		'priveledge_id',
		'created_at',
		'modified_at',
		'created_by',
		/*
		'modified_by',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
</div>