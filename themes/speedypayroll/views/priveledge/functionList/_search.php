<?php
/* @var $this FunctionListController */
/* @var $model FunctionList */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

        <div class="form-group">
        <?php echo $form->label($model,'id'); ?>
        <?php echo $form->textField($model,'id',array('size'=>20,'maxlength'=>20,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'title'); ?>
        <?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'module'); ?>
        <?php echo $form->textField($model,'module',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'controller'); ?>
        <?php echo $form->textField($model,'controller',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'action'); ?>
        <?php echo $form->textField($model,'action',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'priveledge_id'); ?>
        <?php echo $form->textField($model,'priveledge_id',array('size'=>20,'maxlength'=>20,'class' => 'form-control')); ?>
    </div>

<div class="form-group">
    <?php echo CHtml::submitButton('Search'); ?>
</div>

<?php $this->endWidget(); ?>
<!-- search-form -->