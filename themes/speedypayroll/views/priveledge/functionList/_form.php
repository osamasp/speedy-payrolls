<?php
/* @var $this FunctionListController */
/* @var $model FunctionList */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'function-list-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
<div class="box-body" style="padding-left:20px;">
    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php if ($model->hasErrors()) { ?>
<div class='callout callout-danger'>    <?php echo $form->errorSummary($model); ?>
</div>
<?php } ?>    
                <div class="form-group">
            <?php echo $form->labelEx($model,'title'); ?>
            <?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
            <?php echo $form->error($model,'title', array('class' => 'text-red')); ?>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'module'); ?>
            <?php
            echo $form->dropDownList($model, 'module', $modules, array('class' => 'form-control',
                'ajax' => array(
                    'method' => 'POST',
                    'url' => Yii::app()->createUrl('priveledge/functionList/loadcontrollers'), //or $this->createUrl('loadcities') if '$this' extends CController
                    'success' => 'function(data){$("#FunctionList_controller").empty();$("#FunctionList_controller").append(data);$("#FunctionList_controller").trigger("chosen:updated");}',
                    'data' => array('module' => 'js:this.value'),)));
            ?>
            <?php echo $form->error($model,'module', array('class' => 'text-red')); ?>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'controller'); ?>
            <?php
            echo CHtml::activeDropDownList($model, 'controller', $controllers, array('class' => 'form-control',
                'ajax' => array(
                    'method' => 'POST',
                    'url' => Yii::app()->createUrl('priveledge/functionList/loadactions'), //or $this->createUrl('loadcities') if '$this' extends CController
                    'success' => 'function(data){$("#FunctionList_action").empty();$("#FunctionList_action").append(data);$("#FunctionList_action").trigger("chosen:updated");}',
                    'data' => array('controller' => 'js:this.value','module'=>'js:$("#FunctionList_module").val()'),)));
            ?>
            <?php echo $form->error($model,'controller', array('class' => 'text-red')); ?>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'action'); ?>
            <?php echo CHtml::activeDropDownList($model, 'action', $actions, array('class' => 'form-control')); ?>
            <?php echo $form->error($model,'action', array('class' => 'text-red')); ?>
        </div>

        </div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-9">
    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary')); ?>
</div>
    </div>
</div>

<?php $this->endWidget(); ?>