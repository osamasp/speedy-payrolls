<?php
/* @var $this FunctionListController */
/* @var $model FunctionList */

$this->breadcrumbs=array(
	'Function Lists'=>array('index'),
	$model->title,
);

$this->menu=array(
array('label'=>'List FunctionList', 'url'=>array('index')),
array('label'=>'Create FunctionList', 'url'=>array('create')),
array('label'=>'Update FunctionList', 'url'=>array('update', 'id'=>$model->id)),
array('label'=>'Delete FunctionList', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage FunctionList', 'url'=>array('admin')),
);
?>

<div class="box-header">
    <h3 class="box-title">View FunctionList #<?php echo $model->id; ?></h3>
</div>
<div class="table-responsive">
    <div class="box-body">
        <?php $this->widget('zii.widgets.CDetailView', array(
        'data'=>$model,
        'attributes'=>array(
        		'id',
		'title',
		'module',
		'controller',
		'action',
		'priveledge_id',
        ),
        )); ?>
    </div>
</div>