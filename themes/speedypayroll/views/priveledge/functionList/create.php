<?php
/* @var $this FunctionListController */
/* @var $model FunctionList */

$this->breadcrumbs=array(
	'Function Lists'=>array('index'),
	'Create',
);

//$this->menu=array(
//	array('label'=>'List FunctionList', 'url'=>array('index')),
//	array('label'=>'Manage FunctionList', 'url'=>array('admin')),
//);
//?>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Add New Action
        </div>
    </div>
    <div class="portlet-body form">
<?php $this->renderPartial('_form', array('model'=>$model,'modules'=>$modules,
                        'controllers'=>$controllers,
                        'actions'=>$actions,)); ?>
    </div>
</div>