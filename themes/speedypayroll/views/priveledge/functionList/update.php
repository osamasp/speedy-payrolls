<?php
/* @var $this FunctionListController */
/* @var $model FunctionList */

$this->breadcrumbs=array(
	'Function Lists'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List FunctionList', 'url'=>array('index')),
	array('label'=>'Create FunctionList', 'url'=>array('create')),
	array('label'=>'View FunctionList', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage FunctionList', 'url'=>array('admin')),
);
?>

<div class="box-header">
    <h3 class="box-title">Update Function List <?php echo $model->id; ?></h3>
</div>

<?php $this->renderPartial('_form', array('model'=>$model,'modules'=>$modules,'controllers'=>$controllers,'actions'=>$actions)); ?>