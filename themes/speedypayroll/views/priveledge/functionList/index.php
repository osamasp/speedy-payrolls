<?php
/* @var $this FunctionListController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Function Lists',
);

//$this->menu=array(
//array('label'=>'Create FunctionList', 'url'=>array('create')),
//array('label'=>'Manage FunctionList', 'url'=>array('admin')),
//);
//?>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Update Privilege
        </div>
    </div>
    <div class="portlet-body form">
<div class="box-body">
<div class="box-header">
    <h3 class="box-title">Function Lists</h3>
    <div class="box-tools">
        <a class="btn btn-primary pull-right" href="<?php echo $this->createUrl('create'); ?>">Create Function List</a>
    </div>
</div>

<div class="box-body table-responsive">
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                                                        <th><?php echo FunctionList::model()->getAttributeLabel('id') ?></th>
                                                        <th><?php echo FunctionList::model()->getAttributeLabel('title') ?></th>
                                                        <th><?php echo FunctionList::model()->getAttributeLabel('module') ?></th>
                                                        <th><?php echo FunctionList::model()->getAttributeLabel('controller') ?></th>
                                                        <th><?php echo FunctionList::model()->getAttributeLabel('action') ?></th>
                                    <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($dataProvider as $item) { ?>
            <tr>
                                                        <td><?php echo $item->id; ?></td>
                                                        <td><?php echo $item->title; ?></td>
                                                        <td><?php echo $item->module; ?></td>
                                                        <td><?php echo $item->controller; ?></td>
                                                        <td><?php echo $item->action; ?></td>
                                    <td>
                        
                        <div class="btn-group">
                            <a class="btn btn-primary" style="padding-top: 0px;padding-bottom: 0px;" href="<?php echo $this->createUrl('view', array('id' => $item->id)); ?>">View</a>
                            <button type="button" class="btn btn-primary dropdown-toggle" style="padding-top: 0px;padding-bottom: 0px;" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php echo $this->createUrl('update', array('id' => $item->id)); ?>">Update</a></li>
                                <li class="divider"></li>
                                <li><a href="<?php echo $this->createUrl('delete', array('id' => $item->id)); ?>">Delete</a></li>
                            </ul>
                        </div>
                        
                    </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
</div>
    </div>
</div>