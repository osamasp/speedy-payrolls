<?php
/* @var $this PriveledgeController */
/* @var $model Priveledge */

$this->breadcrumbs=array(
	'Priveledges'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Priveledge', 'url'=>array('index')),
	array('label'=>'Create Priveledge', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#priveledge-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Privileges
        </div>
    </div>
    <div class="portlet-body form">
<div class="box-header" style="padding-left:10px;">
    <h3 class="box-title">Manage Privileges</h3>
</div>
<div class="box-body table-responsive" style="padding-left:20px;">

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'priveledge-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
//		'id',
		'name',
		'key',
		'function',
		'group',
//		'created_at',
//		'modified_at',
//		'created_by',
//		'modified_by',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
</div>
    </div>
</div>