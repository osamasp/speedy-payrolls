<?php 
/* @var $this PriveledgeController */
/* @var $model Priveledge */

$this->breadcrumbs=array(
	'Priveledges'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Priveledge', 'url'=>array('index')),
	array('label'=>'Manage Priveledge', 'url'=>array('admin')),
);
?>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Add New Privelege
        </div>
    </div>
    <div class="portlet-body form">

<?php $this->renderPartial('_form', array('model'=>$model,'actions'=>$actions, 'groups' =>$groups,'functions'=>$functions)); ?>
    </div>
</div>