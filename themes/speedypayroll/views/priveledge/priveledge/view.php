<?php
/* @var $this PriveledgeController */
/* @var $model Priveledge */

$this->breadcrumbs=array(
	'Privileges'=>array('index'),
	$model->name,
);

$this->menu=array(
array('label'=>'List Priveledge', 'url'=>array('index')),
array('label'=>'Create Priveledge', 'url'=>array('create')),
array('label'=>'Update Priveledge', 'url'=>array('update', 'id'=>$model->id)),
array('label'=>'Delete Priveledge', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Priveledge', 'url'=>array('admin')),
);
?>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>View Privilege
        </div>
    </div>
    <div class="portlet-body form">
<div class="table-responsive">
    <div class="box-body">
        <?php $this->widget('zii.widgets.CDetailView', array(
        'data'=>$model,
        'attributes'=>array(
        		'id',
		'name',
		'key',
		'function',
		'controller',
		'action',
		'group',
		'created_at',
		'modified_at',
		'created_by',
		'modified_by',
        ),
        )); ?>
    </div>
</div>
    </div>
</div>