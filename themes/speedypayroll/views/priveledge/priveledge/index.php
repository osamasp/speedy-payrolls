<?php
/* @var $this PriveledgeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Privileges',
);
//$this->menu = array(
//    array('label' => 'Create Priveledge', 'url' => array('create')),
//    array('label' => 'Manage Priveledge', 'url' => array('admin')),
//);
?>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-copy"></i>Privileges
        </div>
    </div>
    <div class="portlet-body table-responsive">
        <div class="row" style="padding-top:10px;">
        </div>
        <div> 


            <div class="box-body table-responsive">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th><?php echo Priveledge::model()->getAttributeLabel('id') ?></th>
                            <th><?php echo Priveledge::model()->getAttributeLabel('name') ?></th>
                            <th><?php echo Priveledge::model()->getAttributeLabel('key') ?></th>
                            <th><?php echo Priveledge::model()->getAttributeLabel('function') ?></th>
                            <th><?php echo Priveledge::model()->getAttributeLabel('function_list_id') ?></th>
                            <th><?php echo Priveledge::model()->getAttributeLabel('group') ?></th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($dataProvider as $item) { ?>
                            <tr>
                                <td><?php echo $item->id; ?></td>
                                <td><?php echo $item->name; ?></td>
                                <td><?php echo $item->key; ?></td>
                                <td><?php echo $item->function; ?></td>
                                <td><?php echo FunctionList::model()->findByPk($item->function_list_id)->title; ?></td>
                                <td><?php echo $item->group; ?></td>
                                <td>

                                    <div class="btn-group">
                                        <button class="btn green dropdown-toggle" data-toggle="dropdown">Actions <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a href="<?php echo $this->createUrl('view', array('id' => $item->id)); ?>">View</a></li>
<!--                                            <li><a href="<?php // echo $this->createUrl('update', array('id' => $item->id)); ?>">Update</a></li>
                                            <li class="divider"></li>
                                            <li><a href="<?php // echo $this->createUrl('delete', array('id' => $item->id)); ?>">Delete</a></li>-->
                                        </ul>
                                    </div>

                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>