<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'companyprivileges-form',
    'htmlOptions' => array('onSubmit' => 'return Chk();'),
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => false,
        ));
?>
<div id="message" style="display:none;">

</div>

<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-check-square-o"></i>Assign Company Privileges
        </div>
    </div>
    <div class="portlet-body form">
        <div class="box-body" style="padding-left:20px;padding-right:20px;">
            <p class="note">Fields with <span class="required">*</span> are required.</p>
            <?php if ($model->hasErrors()) { ?>
                <div class='callout callout-danger'>    <?php echo $form->errorSummary($model); ?>
                </div>
            <?php } ?> 

            <div class="form-group">
                <?php // echo $form->labelEx($model,'company_id'); ?>
                <?php
                echo CHtml::activeDropDownList($model, 'company_id', $companies, array('class' => 'form-control'));
                ?>
                <?php // echo $form->error($model,'companies', array('class' => 'text-red')); ?>
            </div>
            <div id="search" style="display:none;">
                <div class="row">
                    <div class="col-md-4">Group: 
                        <select name="group" class="form-control" id="group">
                            <option value="">All groups</option>
                            <?php foreach ($groups as $group) {
                                ?>
                                <option value="<?php echo $group; ?>"><?php echo $group; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-4">
                        Title: <input type="text" id="title" name="title" class="form-control">
                    </div>
                </div>
                <div class="row" style="margin-top:10px;">
                    <div class="col-md-4">
                        Start Date: <input type="text" id="start_date" name="start_date" class="form-control datePicker">
                    </div>
                    <div class="col-md-4">
                        End Date: <input type="text" id="end_date" name="end_date" class="form-control datePicker">
                    </div>
                    <div class="col-md-4" style="padding-top:20px;">
                        <button type="button" class="btn btn-primary" onclick="search()">Search</button>
                    </div>
                </div>
                <div class="row" style="padding-top:20px;">
                    <div class="col-md-12">
                        <b>Note:</b> In-order to assign unlimited quota for any action please use -1.
                    </div>
                </div>
            </div>
            <div id="privilege">

            </div>

        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-9">
                    <?php // echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Save', array('class' => 'btn btn-primary','onclick'=>'submitForm()')); ?>
                    <input type="button" id="save" name="save" class="btn btn-primary" value="Save">
                </div>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
    <div id="overlay" style="position:absolute;top:50%;left:40%;display: none;"><div id='PleaseWait' style=''><img src="<?php echo Yii::app()->theme->baseUrl . '/img/ajax-loader.gif'; ?>" style="width:100px;"/></div></div>
    <script type="text/javascript">
        jQuery(function ($) {
            jQuery('body').on('change', '#CompanyPriveledge_company_id', function () {
                $("#overlay").show();
                var url = '<?php echo Yii::app()->baseUrl; ?>';
                url = url.concat('/priveledge/priveledge/loadprivileges');

                jQuery.ajax({'type': 'POST', 'url': url,
                    'success': function (data) {
                        $("#group").val("");
                        $("#title").val("");
                        $("#start_date").val("");
                        $("#end_date").val("");
//                        $("#search").show();
                        $("#privilege").empty();
                        $("#privilege").append(data);
                        $("#privilege").trigger("chosen:updated");
                        $("#overlay").hide();
                    },
                    'data': {'company_id': this.value}, 'cache': false});
                return false;
            });
        });
        
        function Chk(flag){
        return flag;
        }

        $("#save").click(function ()
        {
            var status = false;
            $(".privilege_checks").each(function () {
                if ($(this).is(":checked")) {
                    var privilege_id = $(this).attr('id');
                    var quota = '#q_' + privilege_id;
                    var type = '#t_' + privilege_id;
                    if ($(quota).val() == "")
                    {
                        alert("Quota is required");
                        $(quota).focus();
                        chk(false);
                        return false;
                    }
                    status = true;
                }
            });
            if (status)
            {
                $("#status").val(status);
                $("#companyprivileges-form").submit();
            }
        });
    </script>   