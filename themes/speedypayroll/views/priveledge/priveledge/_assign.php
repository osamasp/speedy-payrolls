<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$company_privilege_ids = array();
?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'priveledge-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => false,
        ));
?>

<input type="hidden" id="ajax_loader" value="<?php echo "<img src='" . Yii::app()->theme->baseUrl . "/img/ajax-loader.gif' />"; ?>"/>

<div class="box-body table-responsive" style="margin-top:20px;">
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th class="table-checkbox">
                    <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes"/>
                </th>
                <th>
                    Privilege
                </th>
                <th>
                    Start Time
                </th>
                <th>
                    End Time
                </th>
                <th>
                    Recursive
                </th>
                <th>
                    Type
                </th>
                <th>
                    Quota
                </th>
                <th>
                    Action
                </th>

            </tr>
        </thead>
        <tbody role="alert" aria-live="polite" aria-relevant="all">
            <?php foreach ($company_privilege as $privilege) {
                if(count($privilege->spCompanyPriveledges)>0){
                ?>
                <tr class="gradeX">
                    <td>
                        <input type="checkbox" name="check[]" class="checker" id="check<?php echo $privilege["spCompanyPriveledges"][0]->id; ?>" checked/>
                    </td>
                    <td>
                        <?php // array_push($company_privilege_ids, $privilege->id);
                        echo $privilege->name
                        ?>
                    </td>
                    <td>
                        <input type="text" name="start_time" id="CompanyPriveledge_start_time<?php echo $privilege["spCompanyPriveledges"][0]->id; ?>" value="<?php echo date('d/m/Y', $privilege["spCompanyPriveledges"][0]->start_time); ?>" class="datePicker">
                        <?php // echo $form->textField($model,'start_time', $privilege->start_time, array('class' => 'form-control pull-left datePicker disableEle')); ?>
    <?php echo $form->error($model, 'start_time', array('class' => 'text-red')); ?>
                    </td>
                    <td>
                        <input type="text" name="end_time" id="CompanyPriveledge_end_time<?php echo $privilege["spCompanyPriveledges"][0]->id; ?>" value="<?php echo date('d/m/Y', $privilege["spCompanyPriveledges"][0]->end_time); ?>" class="datePicker">
                        <?php // echo $form->textField($model,'end_time', $privilege->end_time,array('class' => 'form-control pull-left datePicker disableEle')); ?>
    <?php echo $form->error($model, 'end_time', array('class' => 'text-red')); ?>
                    </td>
                    <td>
                        <select name='CompanyPriveledge[recursive]' class='form-control' id='CompanyPriveledge_recursive<?php echo $privilege["spCompanyPriveledges"][0]->id; ?>' required>
                            <option value='Recursive' <?php if ($privilege["spCompanyPriveledges"][0]->recursive == "Recursive") { ?> selected='selected'<?php } ?>>Recursive</option>
                            <option value='Non-Recursive' <?php if ($privilege["spCompanyPriveledges"][0]->recursive == "Non-Recursive") { ?> selected='selected'<?php } ?>>Non-Recursive</option>
                        </select>
                        <?php // echo CHtml::activeDropDownList($model, 'recursive',array('Recursive'=>'Recursive','Non-Recursive'=>'Non-Recursive'), array('required'=>'required','class' => 'form-control')); ?>
    <?php // echo $form->error($model,'recursive', array('class' => 'text-red'));   ?>
                    </td>
                    <td>
                        <select name='CompanyPriveledge[recursive_type]' class='form-control' id='CompanyPriveledge_recursive_type<?php echo $privilege["spCompanyPriveledges"][0]->id; ?>' required>
                            <option value='None' <?php if ($privilege["spCompanyPriveledges"][0]->recursive_type == "None") { ?> selected='selected'<?php } ?>>None</option>
                            <option value='Daily' <?php if ($privilege["spCompanyPriveledges"][0]->recursive_type == "Daily") { ?> selected='selected'<?php } ?>>Daily</option>
                            <option value='Weekly' <?php if ($privilege["spCompanyPriveledges"][0]->recursive_type == "Weekly") { ?> selected='selected'<?php } ?>>Weekly</option>
                            <option value='Monthly' <?php if ($privilege["spCompanyPriveledges"][0]->recursive_type == "Monthly") { ?> selected='selected'<?php } ?>>Monthly</option>
                        </select>
                        <?php // echo CHtml::activeDropDownList($model, 'recursive_type',array('None'=>'None','Daily'=>'Daily','Weekly'=>'Weekly','Monthly'=>'Monthly'), array('required'=>'required','class' => 'form-control')); ?>
    <?php // echo $form->error($model,'recursive_type', array('class' => 'text-red'));   ?>
                    </td>
                    <td>
                        <input type="text" name="quota" id="CompanyPriveledge_quota<?php echo $privilege["spCompanyPriveledges"][0]->id; ?>"  value="<?php echo $privilege["spCompanyPriveledges"][0]->quota; ?>" class="form-control" required>
                        <?php // echo $form->textField($model,'quota', $privilege->quota,array('class' => 'form-control')); ?>
    <?php echo $form->error($model, 'quota', array('class' => 'text-red')); ?>
                    </td>
                    <td>
                        <button type="button" onclick="save(<?php echo $privilege["spCompanyPriveledges"][0]->priveledge_id; ?>,<?php echo $privilege["spCompanyPriveledges"][0]->id; ?>)" class="btn-primary">Save</button>
                    </td>
                </tr>
            <?php } 
            else{
            ?>
           <tr class="gradeX">
                        <td>
                            <input type="checkbox" name="check[]" id="check<?php echo $privilege->id; ?>" class="checker"/>
                        </td>
                        <td>
        <?php echo $privilege->name; ?>
                        </td>
                        <td>
                            <input type="text" name="start_time" id="CompanyPriveledge_start_time<?php echo $privilege->id; ?>" class="datePicker">
        <?php // echo $form->textField($model,'start_time', $privilege->start_time, array('class' => 'form-control pull-left datePicker disableEle'));  ?>
        <?php echo $form->error($model, 'start_time', array('class' => 'text-red')); ?>
                        </td>
                        <td>
                            <input type="text" name="end_time" id="CompanyPriveledge_end_time<?php echo $privilege->id; ?>" class="datePicker">
        <?php // echo $form->textField($model,'end_time', $privilege->end_time,array('class' => 'form-control pull-left datePicker disableEle'));  ?>
        <?php echo $form->error($model, 'end_time', array('class' => 'text-red')); ?>
                        </td>
                        <td>
                            <select name='CompanyPriveledge[recursive]' class='form-control' id='CompanyPriveledge_recursive<?php echo $privilege->id; ?>' required>
                                <option value='Recursive'>Recursive</option>
                                <option value='Non-Recursive'>Non-Recursive</option>
                            </select>
        <?php // echo CHtml::activeDropDownList($model, 'recursive',array('Recursive'=>'Recursive','Non-Recursive'=>'Non-Recursive'), array('required'=>'required','class' => 'form-control'));  ?>
        <?php // echo $form->error($model,'recursive', array('class' => 'text-red'));   ?>
                        </td>
                        <td>
                            <select name='CompanyPriveledge[recursive_type]' class='form-control' id='CompanyPriveledge_recursive_type<?php echo $privilege->id; ?>' required>
                                <option value='None'>None</option>
                                <option value='Daily'>Daily</option>
                                <option value='Weekly'>Weekly</option>
                                <option value='Monthly'>Monthly</option>
                            </select>
        <?php // echo CHtml::activeDropDownList($model, 'recursive_type',array('None'=>'None','Daily'=>'Daily','Weekly'=>'Weekly','Monthly'=>'Monthly'), array('required'=>'required','class' => 'form-control'));  ?>
        <?php // echo $form->error($model,'recursive_type', array('class' => 'text-red'));   ?>
                        </td>
                        <td>
                            <input type="text" name="quota" id="CompanyPriveledge_quota<?php echo $privilege->id; ?>" class="form-control" required>
        <?php // echo $form->textField($model,'quota', $privilege->quota,array('class' => 'form-control'));  ?>
        <?php echo $form->error($model, 'quota', array('class' => 'text-red')); ?>
                        </td>
                        <td>
                            <button type="button" onclick="save(<?php echo $privilege->id; ?>, 0)" class="btn-primary">Save</button>
                        </td>
                    </tr>
            <?php }
            } ?>
        </tbody>
    </table>
</div>

<?php $this->endWidget(); ?>
<table>
        <?php $this->widget('CLinkPager', 
                array('pages' => $pages,
                    'header'=>'<center>',
                    'footer'=>'</center>',
                    'id'=>'link_pager',
                    'maxButtonCount'=>3,
                    'nextPageLabel' => 'Next',
                    'prevPageLabel' => 'Prev',
                    'selectedPageCssClass' => 'active',
                    'hiddenPageCssClass' => 'disabled',
                    'htmlOptions' => array(
                        'class'=>'pagination pull-right',
                    )
                    )); ?>
</table>
<script type="text/javascript">
    jQuery(document).ready(function () {
           $('.datePicker').datepicker({format: 'dd/mm/yyyy'});
      });
</script>