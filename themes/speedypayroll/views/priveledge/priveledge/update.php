<?php
/* @var $this PriveledgeController */
/* @var $model Priveledge */

$this->breadcrumbs=array(
	'Priveledges'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

?>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Update Privilege
        </div>
    </div>
    <div class="portlet-body form">
<?php $this->renderPartial('_form', array('model'=>$model,'functions'=>$functions,'actions'=>$actions, 'groups'=>$groups)); ?>
    </div>
</div>