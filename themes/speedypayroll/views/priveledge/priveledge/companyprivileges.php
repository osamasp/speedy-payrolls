<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$parent = array();
$count=0;
$last_element = end($model);
$saved_ids = array();
//dd($model);
?>

<div class="page-container" style="width:100%;margin-top:10px;padding-left:0;padding-right:0;">
<div class="page-sidebar-wrapper">
    <div class="page-sidebar md-shadow-z-2-i  navbar-collapse collapse" style="width:100%;">
        <div class="row" style="padding-left:20px;">
            <div class="col-md-1"><input type="checkbox" id="main_check"></div>
            <div class="col-md-2"><label><b>Privilege</b></label></div>
            <div class="col-md-2"><label><b>Start Time</b></label></div>
            <div class="col-md-2"><label><b>End Time</b></label></div>
            <div class="col-md-2"><label><b>Recursive</b></label></div>
            <div class="col-md-2"><label><b>Type</b></label></div>
            <div class="col-md-1"><label><b>Quota</b></label></div>
        </div>
<ul class="page-sidebar-menu " data-keep-expanded="true" data-auto-scroll="true" data-slide-speed="200" style="padding-right:20px;padding-left:20px;">
    <?php foreach($model as $item){ $count++; if(!in_array($item["group_id"], $parent)){     array_push($parent, $item["group_id"]);?>
    <li class="treeview"><div class="row">
            <div class="col-md-1"><?php if($item["pp_id"]){ ?> 
                <input type="checkbox" value="<?php echo $item["id"];?>" id="<?php echo 'c_'.$item["group_id"];?>" onclick="checkAll('<?php echo '.c_'.$item["group_id"];?>','<?php echo '#c_'.$item["group_id"];?>')" checked="checked"> 
                    <?php } 
                    else{ ?>
                        <input type="checkbox" value="<?php echo $item["id"];?>" id="<?php echo 'c_'.$item["group_id"];?>" onclick="checkAll('<?php echo '.c_'.$item["group_id"];?>','<?php echo '#c_'.$item["group_id"];?>')">
                  <?php } ?>
            </div>
            <div class="col-md-2"><a href="javascript:void(0);" onclick="toggler('<?php echo '#'.$item['group_id']; ?>')"><?php echo $item["group"]; ?></a></div>
            <?php if($item["start_time"]){ ?><div class="col-md-2"><input type="text" id="<?php echo 's_'.$item["group_id"];?>" onchange="setQuota('<?php echo "s_".$item["group_id"];?>')" value="<?php echo date('d/m/Y', $item["start_time"]); ?>" class="datePicker"></div>
            <?php } else{ ?><div class="col-md-2"><input type="text" id="<?php echo 's_'.$item["group_id"];?>" onchange="setQuota('<?php echo "s_".$item["group_id"];?>')" class="datePicker"></div><?php }
            if($item["end_time"]){ ?>
            <div class="col-md-2"><input type="text" id="<?php echo 'e_'.$item["group_id"];?>" onchange="setQuota('<?php echo "e_".$item["group_id"];?>')" value="<?php echo date('d/m/Y', $item["end_time"]); ?>" class="datePicker"></div><?php } else{ ?>
            <div class="col-md-2"><input type="text" id="<?php echo 'e_'.$item["group_id"];?>" onchange="setQuota('<?php echo "e_".$item["group_id"];?>')" class="datePicker"></div> <?php } ?>
            <div class="col-md-2">
                <select class='form-control' id="<?php echo 'r_'.$item["group_id"];?>" onchange="setQuota('<?php echo "r_".$item["group_id"];?>')" required>
                    <option value='Recursive' <?php if ($item["recursive"] == "Recursive") { ?> selected='selected'<?php } ?>>Recursive</option>
                    <option value='Non-Recursive' <?php if ($item["recursive"] == "Non-Recursive") { ?> selected='selected'<?php } ?>>Non-Recursive</option>
                </select>
            </div>
            <div class="col-md-2"><select id="<?php echo 't_'.$item["group_id"];?>" onchange="setQuota('<?php echo "t_".$item["group_id"];?>')" class="form-control <?php echo 't_'.$item["group_id"];?>">
            <?php if($item["recursive_type"] == "None"){ ?> <option value="None" selected="selected">None</option> <?php } else{ ?><option value="None">None</option><?php } ?>
            <?php if($item["recursive_type"] == "Daily"){ ?><option value="Daily" selected="selected">Daily</option> <?php } else{ ?><option value="Daily">Daily</option><?php } ?>
            <?php if($item["recursive_type"] == "Weekly"){ ?><option value="Weekly" selected="selected">Weekly</option> <?php } else{ ?><option value="Weekly">Weekly</option><?php } ?>
            <?php if($item["recursive_type"] == "Monthly"){ ?><option value="Monthly" selected="selected">Monthly</option> <?php } else{ ?><option value="Monthly">Monthly</option><?php } ?>
            </select></div>
            <div class="col-md-1"><input type="text" id="<?php echo 'q_'.$item["group_id"];?>" onchange="setQuota('<?php echo "q_".$item["group_id"];?>')" class="form-control" value="<?php echo $item['quota'];?>"></div>
    <ul class="sub-menu" id="<?php echo $item['group_id'];?>">
    <?php }
    if($item["pp_id"]){ array_push($saved_ids, $item["pp_id"]); ?><input type="hidden" name="saved_privileges[]" value="<?php echo $item['id'];?>">
        <li class="treeview"><div class="row">
            <div class="col-md-1" style="text-align:right;">
                <input type="checkbox" name="privilege[<?php echo $item['id'];?>]" class="<?php echo 'c_'.$item['group_id'];?> privilege_checks" id="<?php echo $item["id"];?>" checked="checked">
            </div>
            <div class="col-md-2"><a href="javascript:void(0);"><?php echo $item["name"]; ?></a></div>
            <div class="col-md-2"><input type="text" id="<?php echo 's_'.$item["id"];?>" name="start_time[<?php echo $item['id'];?>]" value="<?php echo date('d/m/Y', $item["start_time"]); ?>" class="datePicker <?php echo 's_'.$item["group_id"];?>"></div>
            <?php if($item["end_time"]){ ?>
                <div class="col-md-2"><input type="text" id="<?php echo 'e_'.$item["id"];?>" name="end_time[<?php echo $item['id'];?>]"  value="<?php echo date('d/m/Y', $item["end_time"]); ?>" class="datePicker <?php echo 'e_'.$item["group_id"];?>"></div><?php } else{ ?>
            <div class="col-md-2"><input type="text" id="<?php echo 'e_'.$item["id"];?>" name="end_time[<?php echo $item['id'];?>]" class="datePicker <?php echo 'e_'.$item["group_id"];?>"></div><?php } ?>
            <div class="col-md-2">
                <select class='form-control <?php echo 'r_'.$item["group_id"];?>' id="<?php echo 'r_'.$item["id"];?>" onchange="setType('<?php echo 'r_'.$item['id'];?>')" name="recursive[<?php echo $item['id'];?>]" required>
                    <option value='Recursive' <?php if ($item["recursive"] == "Recursive") { ?> selected='selected'<?php } ?>>Recursive</option>
                    <option value='Non-Recursive' <?php if ($item["recursive"] == "Non-Recursive") { ?> selected='selected'<?php } ?>>Non-Recursive</option>
                </select>
            </div>
            <div class="col-md-2"><select id="<?php echo 't_'.$item["id"];?>" name="type[<?php echo $item['id'];?>]" class="form-control <?php echo 't_'.$item["group_id"];?>">
            <?php if($item["recursive_type"] == "None"){ ?> <option value="None" selected="selected">None</option> <?php } else{ ?><option value="None">None</option><?php } ?>
            <?php if($item["recursive_type"] == "Daily"){ ?><option value="Daily" selected="selected">Daily</option> <?php } else{ ?><option value="Daily">Daily</option><?php } ?>
            <?php if($item["recursive_type"] == "Weekly"){ ?><option value="Weekly" selected="selected">Weekly</option> <?php } else{ ?><option value="Weekly">Weekly</option><?php } ?>
            <?php if($item["recursive_type"] == "Monthly"){ ?><option value="Monthly" selected="selected">Monthly</option> <?php } else{ ?><option value="Monthly">Monthly</option><?php } ?>
            </select></div>
            <div class="col-md-1"><input type="text" name="quota[<?php echo $item['id'];?>]" id="<?php echo 'q_'.$item["id"];?>" class="form-control <?php echo 'q_'.$item["group_id"];?>" value="<?php echo $item['quota'];?>"></div>
            </div>
        </li>
    <?php } else{ ?>
        <li class="treeview"><div class="row">
            <div class="col-md-1" style="text-align:right;">
                <input type="checkbox" name="privilege[<?php echo $item['id'];?>]" class="<?php echo 'c_'.$item['group_id'];?> privilege_checks" id="<?php echo $item["id"];?>">
            </div>
            <div class="col-md-2"><a href="javascript:void(0);"><?php echo $item["name"]; ?></a></div>
            <div class="col-md-2"><input type="text" id="<?php echo 's_'.$item["id"];?>" name="start_time[<?php echo $item['id'];?>]" class="datePicker <?php echo 's_'.$item["group_id"];?>"></div>
            <div class="col-md-2"><input type="text" id="<?php echo 'e_'.$item["id"];?>" name="end_time[<?php echo $item['id'];?>]" class="datePicker <?php echo 'e_'.$item["group_id"];?>"></div>
            <div class="col-md-2">
                <select class='form-control <?php echo 'r_'.$item["group_id"];?>' id="<?php echo 'r_'.$item["id"];?>" onchange="setType('<?php echo 'r_'.$item['id'];?>')" name="recursive[<?php echo $item['id'];?>]" required>
                    <option value='Recursive'>Recursive</option>
                    <option value='Non-Recursive'>Non-Recursive</option>
                </select>
            </div>
            <div class="col-md-2"><select id="<?php echo 't_'.$item["id"];?>" name="type[<?php echo $item['id'];?>]" class="form-control <?php echo 't_'.$item["group_id"];?>">
            <option value="None">None</option>
            <option value="Daily">Daily</option>
            <option value="Weekly">Weekly</option>
            <option value="Monthly">Monthly</option>
            </select></div>
            <div class="col-md-1"><input type="text" name="quota[<?php echo $item['id'];?>]" id="<?php echo 'q_'.$item["id"];?>" class="form-control <?php echo 'q_'.$item["group_id"];?>"></div>
            </div>
        </li>
    <?php } ?>
 <?php if($last_element != $item){ if($item["group_id"] != $model[$count]["group_id"]){ ?>
    </ul></div></li>
 <?php } } else{ ?> </ul><?php } } ?>
</ul>
</div>
</div>
</div>
    <script>
        $(document).ready(function(){
           $("#status").val('false');
           $("#main_check").click(function () {
    $("input:checkbox").prop('checked', $(this).prop('checked'));
});
           $('.datePicker').datepicker({format: 'dd/mm/yyyy'});
           $("#save_privileges").val('<?php $saved_ids ?>');
        });
        function setQuota(id)
    {
        $('.'+id).each(function(){
           $(this).val($('#'+id).val()); 
        });
        if(id.indexOf("r_")>=0)
        {
            if($("#"+id).val()=="Non-Recursive")
            {
                var elem = id.replace("r_","t_");
                $('.'+elem).each(function(){
                   $(this).val("None");
                   $(this).attr("readonly",true);
                });
            }
            else
            {
                var elem = id.replace("r_","t_");
                $('.'+elem).each(function(){
                   $(this).val("None");
                   $(this).attr("readonly",false);
                });
            }
        }
    }
    
    function setType(id)
    {
        if($("#"+id).val()=="Non-Recursive")
            {
                var elem = id.replace("r_","t_");
                $("#"+elem).val("None");
                $("#"+elem).attr("readonly",true);
            }
            else
            {
                var elem = id.replace("r_","t_");
                $("#"+elem).val("None");
                $("#"+elem).attr("readonly",false);
            }
    }
    
    function toggler(id) {
        $(id).slideToggle("slow", function () {

        });
    }
    
    function checkAll(classes, id)
    {
        if ($(id).is(":checked")) {
            $(classes).attr('checked', true);
        }
        else {
            $(classes).attr('checked', false);
        }
    }
    </script>