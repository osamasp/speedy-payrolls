<?php
/* @var $this PriveledgeController */
/* @var $model Priveledge */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'priveledge-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

<div class="box-body" style="padding-left:20px;">
    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php if ($model->hasErrors()) { ?>
<div class='callout callout-danger'>    <?php echo $form->errorSummary($model); ?>
</div>
<?php } ?>    
            
                <div class="form-group">
            <?php echo $form->labelEx($model,'name'); ?>
            <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
            <?php echo $form->error($model,'name', array('class' => 'text-red')); ?>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'key'); ?>
            <?php echo $form->textField($model,'key',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
            <?php echo $form->error($model,'key', array('class' => 'text-red')); ?>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'function'); ?>
            <?php echo CHtml::activeDropDownList($model, 'function', $functions, array('class' => 'form-control')); ?>
            <?php echo $form->error($model,'function', array('class' => 'text-red')); ?>
        </div>
            
                <div class="form-group">
            <?php echo $form->labelEx($model,'function_list_id'); ?>
            <?php echo CHtml::activeDropDownList($model, 'function_list_id', $actions, array('class' => 'form-control')); ?>
            <?php echo $form->error($model,'function_list_id', array('class' => 'text-red')); ?>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'group'); ?>
            <?php echo CHtml::activeDropDownList($model, 'group', $groups, array('class' => 'form-control')); ?>
            <?php echo $form->error($model,'group', array('class' => 'text-red')); ?>
        </div>
    
        </div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-9">
    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary')); ?>
</div>
    </div>
</div>
<?php $this->endWidget(); ?>
