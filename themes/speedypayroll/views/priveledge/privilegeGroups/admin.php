<?php
/* @var $this PrivilegeGroupsController */
/* @var $model PrivilegeGroups */

$this->breadcrumbs=array(
	'Privilege Groups'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List PrivilegeGroups', 'url'=>array('index')),
	array('label'=>'Create PrivilegeGroups', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#privilege-groups-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Privilege Group
        </div>
    </div>
    <div class="portlet-body form">
<div class="box-body">
<div class="box-body">

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'privilege-groups-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'group',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
</div>
</div>
    </div>
</div>