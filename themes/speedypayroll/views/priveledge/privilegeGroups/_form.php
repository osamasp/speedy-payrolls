<?php
/* @var $this PrivilegeGroupsController */
/* @var $model PrivilegeGroups */
/* @var $form CActiveForm */
?>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Add Privilege Group
        </div>
    </div>
    <div class="portlet-body form">
<div class="box-body">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'privilege-groups-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
<div class="box-body">
    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php if ($model->hasErrors()) { ?>
<div class='callout callout-danger'>    <?php echo $form->errorSummary($model); ?>
</div>
<?php } ?>    
                <div class="form-group">
            <?php echo $form->labelEx($model,'group'); ?>
            <?php echo $form->textField($model,'group',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
            <?php echo $form->error($model,'group', array('class' => 'text-red')); ?>
        </div>

        </div>
<div class="box-footer">
    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary')); ?>
</div>

<?php $this->endWidget(); ?>
</div>
    </div>
</div>