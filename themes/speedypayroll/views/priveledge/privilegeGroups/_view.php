<?php
/* @var $this PrivilegeGroupsController */
/* @var $data PrivilegeGroups */
?>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Privilege Group
        </div>
    </div>
    <div class="portlet-body form">
<div class="box-body">
<div class="box-body">

	<b><?php echo CHtml::encode($data->getAttributeLabel('group')); ?>:</b>
	<?php echo CHtml::encode($data->group); ?>
	<br />


</div>
</div>
    </div>
</div>