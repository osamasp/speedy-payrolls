<?php
/* @var $this PrivilegeGroupsController */
/* @var $model PrivilegeGroups */

$this->breadcrumbs=array(
	'Privilege Groups'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List PrivilegeGroups', 'url'=>array('index')),
array('label'=>'Create PrivilegeGroups', 'url'=>array('create')),
array('label'=>'Update PrivilegeGroups', 'url'=>array('update', 'id'=>$model->id)),
array('label'=>'Delete PrivilegeGroups', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage PrivilegeGroups', 'url'=>array('admin')),
);
?>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Privilege Group
        </div>
    </div>
    <div class="portlet-body form">
<div class="box-body">
<div class="table-responsive">
    <div class="box-body">
        <?php $this->widget('zii.widgets.CDetailView', array(
        'data'=>$model,
        'attributes'=>array(
        		'id',
		'group',
        ),
        )); ?>
    </div>
</div>
</div>
    </div>
</div>
