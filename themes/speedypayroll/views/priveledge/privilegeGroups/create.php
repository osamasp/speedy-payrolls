<?php
/* @var $this PrivilegeGroupsController */
/* @var $model PrivilegeGroups */

$this->breadcrumbs=array(
	'Privilege Groups'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PrivilegeGroups', 'url'=>array('index')),
	array('label'=>'Manage PrivilegeGroups', 'url'=>array('admin')),
);
?>

<div class="box-header">
    <h3 class="box-title">Create Privilege Groups</h3>
</div>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>