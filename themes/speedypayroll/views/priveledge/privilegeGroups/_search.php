<?php
/* @var $this PrivilegeGroupsController */
/* @var $model PrivilegeGroups */
/* @var $form CActiveForm */
?>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Privilege Group
        </div>
    </div>
    <div class="portlet-body form">
<div class="box-body">
<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

        <div class="form-group">
        <?php echo $form->label($model,'group'); ?>
        <?php echo $form->textField($model,'group',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
    </div>

<div class="form-group">
    <?php echo CHtml::submitButton('Search'); ?>
</div>

<?php $this->endWidget(); ?>
<!-- search-form -->
</div>
    </div>
</div>