<input type="hidden" id="ajax_loader" value="<?php echo "<img src='" . Yii::app()->theme->baseUrl . "/img/ajax-loader.gif' />"; ?>"/>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'add-privilege-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => false,
        ));
?>
<input type="hidden" name="status" id="status" value="false">
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-check-square-o"></i>Plan & Privileges
        </div>
    </div>
    <div class="portlet-body form">
        <div class="row" style="padding-top:10px;">
        </div>
        <div style="padding-left:20px;padding-right:20px;">
            <div class="form-group">
                <label><b>Plan: </b></label>
                <?php if($created_plan!=""){
                echo $form->dropDownList($model, 'plan_type', $plans, array('class' => 'form-control','options' => array($created_plan=>array('selected'=>true))));} else{
                  echo $form->dropDownList($model, 'plan_type', $plans, array('class' => 'form-control'));  
                }
                ?>
                <?php echo $form->error($model, 'plan_type', array('class' => 'text-red')); ?>
            </div>
            <div id="privileges">

            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-9">
                    <?php // echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Save', array('class' => 'btn btn-primary','onclick'=>'submitForm()')); ?>
                    <input type="button" id="save" name="save" class="btn btn-primary" value="Save">
                </div>
            </div>
        </div>
    </div>
</div>
<div id="overlay" style="position:absolute;top:50%;left:40%;display: none;"><div id='PleaseWait' style=''><img src="<?php echo Yii::app()->theme->baseUrl . '/img/ajax-loader.gif'; ?>" style="width:100px;"/></div></div>
<?php $this->endWidget(); ?>
<script>
    $(document).ready(function () {
        $("#status").val('false');
        if($("#Plan_plan_type").val() != ""){
            var url = '<?php echo Yii::app()->baseUrl; ?>';
            url = url.concat('/priveledge/plan/loadprivileges');
            jQuery.ajax({'type': 'POST', 'url': url,
                'success': function (data) {
                    $("#overlay").hide();
                    $("#privileges").html(data);
                },
                'data': {'plan_id': $("#Plan_plan_type").val()},
                'cache': false});
        }
        $("#Plan_plan_type").on('change', function () {
            $("#overlay").show();
            var url = '<?php echo Yii::app()->baseUrl; ?>';
            url = url.concat('/priveledge/plan/loadprivileges');
            jQuery.ajax({'type': 'POST', 'url': url,
                'success': function (data) {
                    $("#overlay").hide();
                    $("#privileges").html(data);
                },
                'data': {'plan_id': $("#Plan_plan_type").val()},
                'cache': false});
        });
    });
</script>
<script>
    function toggler(id) {
        $(id).slideToggle("slow", function () {

        });
    }
    
    function setQuota(id)
    {
        $('.'+id).each(function(){
           $(this).val($('#'+id).val()); 
        });
    }
    
    function checkAll(classes, id)
    {
        if ($(id).is(":checked")) {
            $(classes).attr('checked', true);
        }
        else {
            $(classes).attr('checked', false);
        }
    }
    
    function formSubmit(group_id)
    {
        var quota = "#q_"+group_id;
        var check = "#c_"+group_id;
        var type = "#t_"+group_id;
        if($(check).is(":checked")){
        var url = '<?php // echo Yii::app()->baseUrl; ?>';
            url = url.concat('/priveledge/plan/addprivilege');
            jQuery.ajax({'type': 'POST', 'url': url,
                'success': function (data) {
                    
                },
                'data': {'plan_id': $("#Plan_plan_type").val(),'group_id':group_id,'quota':$(quota).val(),'type':$(type).val()},
                'cache': false});
    }
    else{
        alert("Please check the group you want to save.");
    }
    }

$("#save").click(function()
{
    var status = true;
    $(".privilege_checks").each(function(){
       
       
        if($(this).is(":checked")){
         var privilege_id = $(this).attr('id');
         var groupId = $(this).attr('privGroup');
         var quota = '#q_'+privilege_id;
         var type = '#t_'+privilege_id;
         if($(quota).val()=="")
         {
             
             if(!$("#"+groupId).is(':visible')){
                 toggler("#"+groupId)
             }
             alert("Quota is required");
             $(quota).focus();
             status = false;
             return false;
         }
        }
    });
    if(status==true)
    {
        $("#status").val(status);
        $("#add-privilege-form").submit();
    }
});
</script>
