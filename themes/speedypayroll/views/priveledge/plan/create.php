<?php
/* @var $this PlanController */
/* @var $model Plan */

$this->breadcrumbs=array(
	'Plans'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Plan', 'url'=>array('index')),
	array('label'=>'Manage Plan', 'url'=>array('admin')),
);
?>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i>Plan
        </div>
    </div>
    <div class="portlet-body form">
        <div class="row" style="padding-top:10px;">
        </div>
<div class="box-header">
    <h3 class="box-title">Create Plan</h3>
</div>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
    </div>
</div>