<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'add-privilege-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => false,
        ));
?>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Update Company Plan
        </div>
    </div>
    <div class="portlet-body form">
        <div class="box-body" style="padding-left:20px;padding-right:20px;">
            <p class="note">Fields with <span class="required">*</span> are required.</p>
            <?php if (isset($error)) { ?>
            <div class='form-control' style="color:red;margin-bottom: 20px;"><span>   <?php echo $error; ?></span>
                </div>
            <?php } ?> 
            <div class="form-group">
                <select name="company_id" class="form-control" id="company">
                    <?php foreach($companies as $id=>$company){?>
                    <option value="<?php echo $id;?>"><?php echo $company?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <select name="plan_id" class="form-control" id="plan">
                    <?php foreach($plans as $id=>$plan){?>
                    <option value="<?php echo $id;?>"><?php echo $plan?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-9">
                    <input type="submit" name="submit" value="Save" class="btn btn-primary">
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->endWidget();?>
<script>
    $(document).ready(function(){
       $("#company").on("change",function(){
          var url = '<?php echo Yii::app()->baseUrl; ?>';
            url = url.concat('/priveledge/plan/getcompanyplan');
            jQuery.ajax({'type': 'POST', 'url': url,
                'success': function (data) {
                    if(data > 10){
                        $("#plan").val(data);
                    }
                    else{
                        $("#plan").val("");
                    }
                },
                'data': {'company_id': $("#company").val()},
                'cache': false}); 
       }); 
    });
</script>