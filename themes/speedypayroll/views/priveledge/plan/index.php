<?php
/* @var $this PlanController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Plans',
);

$this->menu=array(
array('label'=>'Create Plan', 'url'=>array('create')),
array('label'=>'Manage Plan', 'url'=>array('admin')),
);
?>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Plan
        </div>
    </div>
    <div class="portlet-body form">
        <div class="row" style="padding-top:30px;padding-right:30px;"><a class="btn btn-primary pull-right" style="margin-bottom:20px;" href="<?php echo $this->createUrl('create'); ?>">Create Plan</a></div>
            <div class="box-header">
            </div>
    <div class="box-tools">
    </div>
        

<div class="box-body table-responsive" style="padding-left:20px;padding-right:20px;">
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                                                        <th><?php echo Plan::model()->getAttributeLabel('plan_type') ?></th>
                                    <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($dataProvider as $item) { ?>
            <tr>
                                                        <td><?php echo $item->plan_type; ?></td>
                                    <td>
                        <div class="btn-group">
                                        <button class="btn green dropdown-toggle" data-toggle="dropdown">Actions <i class="fa fa-angle-down"></i>
                                        </button>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="<?php echo $this->createUrl('view', array('id' => $item->id)); ?>">View</a></li>
                                <li class="divider"></li>
                                <li><a href="<?php echo $this->createUrl('update', array('id' => $item->id)); ?>">Update</a></li>
                                <li class="divider"></li>
                                <li><a href="<?php echo $this->createUrl('addprivilege',array('id'=>$item->id));?>"</li>
                            </ul>
                        </div>
                        
                    </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
    </div>
</div>