<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$parent = array();
$count=0;
$last_element = end($model);
$saved_ids = array();
//dd($model);
?>

<div class="page-container" style="width:100%;margin-top:10px;">
<div class="page-sidebar-wrapper">
    <div class="page-sidebar md-shadow-z-2-i  navbar-collapse collapse" style="width:100%;">
        <div class="row" style="padding-left:20px;">
            <div class="col-md-1"><input type="checkbox" id="main_check"></div>
            <div class="col-md-4"><label><b>Privilege</b></label></div>
            <div class="col-md-3"><label><b>Quota</b></label></div>
            <div class="col-md-4"><label><b>Type</b></label></div>
        </div>
<ul class="page-sidebar-menu " data-keep-expanded="true" data-auto-scroll="true" data-slide-speed="200" style="padding-right:20px;padding-left:20px;">
    <?php foreach($model as $item){ $count++; if(!in_array($item["group_id"], $parent)){     array_push($parent, $item["group_id"]);?>
    <li class="treeview"><div class="row"><div class="col-md-1"><?php if($item["pp_id"]){ ?>
                <input type="checkbox" class="sub_check" value="<?php echo $item["id"];?>" id="<?php echo 'c_'.$item["group_id"];?>" onchange="checkAll('<?php echo '.c_'.$item["group_id"];?>','<?php echo '#c_'.$item["group_id"];?>')" checked="checked"> 
                    <?php } else{ ?><input type="checkbox" class="sub_check" value="<?php echo $item["id"];?>" id="<?php echo 'c_'.$item["group_id"];?>" onchange="checkAll('<?php echo '.c_'.$item["group_id"];?>','<?php echo '#c_'.$item["group_id"];?>')"><?php } ?></div><div class="col-md-4"><a href="javascript:void(0);" onclick="toggler('<?php echo '#'.$item['group_id']; ?>')"><?php echo $item["group"]; ?></a></div>
            <div class="col-md-3"><input type="text" id="<?php echo 'q_'.$item["group_id"];?>" onchange="setQuota('<?php echo "q_".$item["group_id"];?>')" class="form-control" value="<?php echo $item['quota'];?>"></div>
        <div class="col-md-4"><select id="<?php echo 't_'.$item["group_id"];?>" onchange="setQuota('<?php echo "t_".$item["group_id"];?>')" class="form-control">
            <?php if($item["type"] == "None"){ ?> <option value="None" selected="selected">None</option> <?php } else{ ?><option value="None">None</option><?php } ?>
            <?php if($item["type"] == "Daily"){ ?><option value="Daily" selected="selected">Daily</option> <?php } else{ ?><option value="Daily">Daily</option><?php } ?>
            <?php if($item["type"] == "Weekly"){ ?><option value="Weekly" selected="selected">Weekly</option> <?php } else{ ?><option value="Weekly">Weekly</option><?php } ?>
            <?php if($item["type"] == "Monthly"){ ?><option value="Monthly" selected="selected">Monthly</option> <?php } else{ ?><option value="Monthly">Monthly</option><?php } ?>
            </select></div>
    <ul class="sub-menu" id="<?php echo $item['group_id'];?>">
    <?php }
    if($item["pp_id"]){ array_push($saved_ids, $item["pp_id"]); ?>
        <input type="hidden" name="saved_privileges[]" value="<?php echo $item['id'];?>">
        <li class="treeview"><div class="row"><div class="col-md-1" style="text-align:right;">
                <input type="checkbox" name="privilege[<?php echo $item['id'];?>]" privGroup="<?php echo $item['group_id'];?>" class="<?php echo 'c_'.$item['group_id'];?> privilege_checks" id="<?php echo $item["id"];?>" checked="checked"></div><div class="col-md-4"><a href="#"><?php echo $item["name"]; ?></a></div>
                <div class="col-md-3"><input type="text" name="quota[<?php echo $item['id'];?>]" value="<?php echo $item['quota'];?>" id="<?php echo 'q_'.$item["id"];?>" class="form-control <?php echo 'q_'.$item["group_id"];?>"></div>
                <div class="col-md-4"><select name="type[<?php echo $item['id'];?>]" id="<?php echo 't_'.$item["id"];?>" class="form-control <?php echo 't_'.$item["group_id"];?>">
            <?php if($item["type"] == "None"){ ?> <option value="None" selected="selected">None</option> <?php } else{ ?><option value="None">None</option><?php } ?>
            <?php if($item["type"] == "Daily"){ ?><option value="Daily" selected="selected">Daily</option> <?php } else{ ?><option value="Daily">Daily</option><?php } ?>
            <?php if($item["type"] == "Weekly"){ ?><option value="Weekly" selected="selected">Weekly</option> <?php } else{ ?><option value="Weekly">Weekly</option><?php } ?>
            <?php if($item["type"] == "Monthly"){ ?><option value="Monthly" selected="selected">Monthly</option> <?php } else{ ?><option value="Monthly">Monthly</option><?php } ?>
            </select></div>
        </li>
    <?php } else{ ?>
        <li class="treeview"><div class="row"><div class="col-md-1" style="text-align:right;">
                <input type="checkbox" name="privilege[<?php echo $item['id'];?>]" privGroup="<?php echo $item['group_id'];?>" class="<?php echo 'c_'.$item['group_id'];?> privilege_checks" id="<?php echo $item["id"];?>"></div><div class="col-md-4"><a href="#"><?php echo $item["name"]; ?></a></div>
                <div class="col-md-3"><input type="text" name="quota[<?php echo $item['id'];?>]" id="<?php echo 'q_'.$item["id"];?>" class="form-control <?php echo 'q_'.$item["group_id"];?>"></div>
                <div class="col-md-4"><select name="type[<?php echo $item['id'];?>]" id="<?php echo 't_'.$item["id"];?>" class="form-control <?php echo 't_'.$item["group_id"];?>">
            <option value="None">None</option>
            <option value="Daily">Daily</option>
            <option value="Weekly">Weekly</option>
            <option value="Monthly">Monthly</option>
            </select></div>
        </li>
    <?php } ?>
 <?php if($last_element != $item){ if($item["group_id"] != $model[$count]["group_id"]){ ?>
    </ul></div></li>
 <?php } } else{ ?> </ul><?php } } ?>
</ul>
</div>
</div>
</div>
    <script>
        $(document).ready(function(){
           $("#status").val('false');
           $("#save_privileges").val('<?php $saved_ids ?>');
           $("#main_check").click(function () {
    $("input:checkbox").prop('checked', $(this).prop('checked'));
});
        });
    </script>