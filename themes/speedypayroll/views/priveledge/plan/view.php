<?php
/* @var $this PlanController */
/* @var $model Plan */

$this->breadcrumbs=array(
	'Plans'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Plan', 'url'=>array('index')),
array('label'=>'Create Plan', 'url'=>array('create')),
array('label'=>'Update Plan', 'url'=>array('update', 'id'=>$model->id)),
array('label'=>'Delete Plan', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Plan', 'url'=>array('admin')),
);
?>

<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Plan
        </div>
    </div>
    <div class="portlet-body form">
        <div class="row" style="padding-top:10px;">
        </div>
<div class="table-responsive">
    <div class="box-body">
        <?php $this->widget('zii.widgets.CDetailView', array(
        'data'=>$model,
        'attributes'=>array(
        		'id',
		'plan_type',
		'created_at',
		'modified_at',
		'created_by',
		'modified_by',
        ),
        )); ?>
    </div>
</div>
        </div>
    </div>