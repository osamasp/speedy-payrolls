<?php
/* @var $this TemplateItemController */
/* @var $model TemplateItem */

$this->breadcrumbs=array(
	'Template Items'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TemplateItem', 'url'=>array('index')),
	array('label'=>'Create TemplateItem', 'url'=>array('create')),
	array('label'=>'View TemplateItem', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TemplateItem', 'url'=>array('admin')),
);
?>

<div class="box-header">
    <h3 class="box-title">Update TemplateItem <?php echo $model->id; ?></h3>
</div>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>