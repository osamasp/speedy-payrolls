<?php
/* @var $this TemplateItemController */
/* @var $model TemplateItem */

$this->breadcrumbs=array(
	'Template Items'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List TemplateItem', 'url'=>array('index')),
	array('label'=>'Create TemplateItem', 'url'=>array('create')),
);
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/template-templateitem-admin.js"></script>
<div class="box-header">
    <h3 class="box-title">Manage Template Items</h3>
</div>
<div class="box-body">
<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'template-item-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'template_id',
		'name',
		'is_item',
		'created_by',
		'created_at',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
</div>