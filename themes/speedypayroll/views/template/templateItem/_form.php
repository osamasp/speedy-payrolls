<?php
/* @var $this TemplateItemController */
/* @var $model TemplateItem */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'template-item-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
<div class="box-body">
    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php if ($model->hasErrors()) { ?>
<div class='callout callout-danger'>    <?php echo $form->errorSummary($model); ?>
</div>
<?php } ?>    
            <div class="form-group">
            <?php echo $form->labelEx($model,'id'); ?>
            <?php echo $form->textField($model,'id',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
            <?php echo $form->error($model,'id', array('class' => 'text-red')); ?>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'template_id'); ?>
            <?php echo $form->textField($model,'template_id',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
            <?php echo $form->error($model,'template_id', array('class' => 'text-red')); ?>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'name'); ?>
            <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
            <?php echo $form->error($model,'name', array('class' => 'text-red')); ?>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'is_item'); ?>
            <?php echo $form->textField($model,'is_item', array('class' => 'form-control')); ?>
            <?php echo $form->error($model,'is_item', array('class' => 'text-red')); ?>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'created_by'); ?>
            <?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
            <?php echo $form->error($model,'created_by', array('class' => 'text-red')); ?>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'created_at'); ?>
            <?php echo $form->textField($model,'created_at',array('size'=>20,'maxlength'=>20,'class' => 'form-control')); ?>
            <?php echo $form->error($model,'created_at', array('class' => 'text-red')); ?>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'modified_by'); ?>
            <?php echo $form->textField($model,'modified_by',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
            <?php echo $form->error($model,'modified_by', array('class' => 'text-red')); ?>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'modified_at'); ?>
            <?php echo $form->textField($model,'modified_at',array('size'=>20,'maxlength'=>20,'class' => 'form-control')); ?>
            <?php echo $form->error($model,'modified_at', array('class' => 'text-red')); ?>
        </div>

        </div>
<div class="box-footer">
    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary')); ?>
</div>

<?php $this->endWidget(); ?>
