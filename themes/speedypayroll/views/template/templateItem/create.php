<?php
/* @var $this TemplateItemController */
/* @var $model TemplateItem */

$this->breadcrumbs=array(
	'Template Items'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TemplateItem', 'url'=>array('index')),
	array('label'=>'Manage TemplateItem', 'url'=>array('admin')),
);
?>

<div class="box-header">
    <h3 class="box-title">Create TemplateItem</h3>
</div>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>