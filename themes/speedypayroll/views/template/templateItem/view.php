<?php
/* @var $this TemplateItemController */
/* @var $model TemplateItem */

$this->breadcrumbs=array(
	'Template Items'=>array('index'),
	$model->name,
);

$this->menu=array(
array('label'=>'List TemplateItem', 'url'=>array('index')),
array('label'=>'Create TemplateItem', 'url'=>array('create')),
array('label'=>'Update TemplateItem', 'url'=>array('update', 'id'=>$model->id)),
array('label'=>'Delete TemplateItem', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage TemplateItem', 'url'=>array('admin')),
);
?>

<div class="box-header">
    <h3 class="box-title">View TemplateItem #<?php echo $model->id; ?></h3>
</div>
<div class="table-responsive">
    <div class="box-body">
        <?php $this->widget('zii.widgets.CDetailView', array(
        'data'=>$model,
        'attributes'=>array(
        		'id',
		'template_id',
		'name',
		'is_item',
		'created_by',
		'created_at',
		'modified_by',
		'modified_at',
        ),
        )); ?>
    </div>
</div>