<?php
/* @var $this TemplateItemController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Template Items',
);

$this->menu=array(
array('label'=>'Create TemplateItem', 'url'=>array('create')),
array('label'=>'Manage TemplateItem', 'url'=>array('admin')),
);
?>

<div class="box-header">
    <h3 class="box-title">Template Items</h3>
    <div class="box-tools">
        <a class="btn btn-default pull-right" href="<?php echo $this->createUrl('create'); ?>">Create TemplateItem</a>
    </div>
</div>

<div class="box-body table-responsive">
    <table id="example1" class="table table-responsive table-striped">
        <thead>
            <tr>
                                                        <th><?php echo TemplateItem::model()->getAttributeLabel('id') ?></th>
                                                        <th><?php echo TemplateItem::model()->getAttributeLabel('template_id') ?></th>
                                                        <th><?php echo TemplateItem::model()->getAttributeLabel('name') ?></th>
                                                        <th><?php echo TemplateItem::model()->getAttributeLabel('is_item') ?></th>
                                                        <th><?php echo TemplateItem::model()->getAttributeLabel('created_by') ?></th>
                                                        <th><?php echo TemplateItem::model()->getAttributeLabel('created_at') ?></th>
                                                        <th><?php echo TemplateItem::model()->getAttributeLabel('modified_by') ?></th>
                                                        <th><?php echo TemplateItem::model()->getAttributeLabel('modified_at') ?></th>
                                    <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($dataProvider as $item) { ?>
            <tr>
                                                        <td><?php echo $item->id; ?></td>
                                                        <td><?php echo $item->template_id; ?></td>
                                                        <td><?php echo $item->name; ?></td>
                                                        <td><?php echo $item->is_item; ?></td>
                                                        <td><?php echo $item->created_by; ?></td>
                                                        <td><?php echo $item->created_at; ?></td>
                                                        <td><?php echo $item->modified_by; ?></td>
                                                        <td><?php echo $item->modified_at; ?></td>
                                    <td>
                        
                        <div class="btn-group">
                            <a class="btn btn-primary" style="padding-top: 0px;padding-bottom: 0px;" href="<?php echo $this->createUrl('view', array('id' => $item->id)); ?>">View</a>
                            <button type="button" class="btn btn-primary dropdown-toggle" style="padding-top: 0px;padding-bottom: 0px;" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php echo $this->createUrl('update', array('id' => $item->id)); ?>">Update</a></li>
                                <li class="divider"></li>
                                <li><a href="<?php echo $this->createUrl('delete', array('id' => $item->id)); ?>">Delete</a></li>
                            </ul>
                        </div>
                        
                    </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>