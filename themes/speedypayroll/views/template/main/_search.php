<?php
/* @var $this MainController */
/* @var $model Template */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

        <div class="form-group">
        <?php echo $form->label($model,'id'); ?>
        <?php echo $form->textField($model,'id',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'name'); ?>
        <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'created_by'); ?>
        <?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'created_at'); ?>
        <?php echo $form->textField($model,'created_at',array('size'=>20,'maxlength'=>20,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'modified_by'); ?>
        <?php echo $form->textField($model,'modified_by',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
    </div>

        <div class="form-group">
        <?php echo $form->label($model,'modified_at'); ?>
        <?php echo $form->textField($model,'modified_at',array('size'=>20,'maxlength'=>20,'class' => 'form-control')); ?>
    </div>

<div class="form-group">
    <?php echo CHtml::submitButton('Search'); ?>
</div>

<?php $this->endWidget(); ?>
<!-- search-form -->