<?php
/* @var $this MainController */
/* @var $model Template */

$this->breadcrumbs=array(
	'Templates'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Template', 'url'=>array('index')),
	array('label'=>'Manage Template', 'url'=>array('admin')),
);
?>

<!--<div class="box-header">
    <h3 class="box-title">Create Template</h3>
</div>-->

<?php $this->renderPartial('_form', array('model'=>$model)); ?>