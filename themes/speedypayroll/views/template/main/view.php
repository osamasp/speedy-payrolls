<?php
/* @var $this MainController */
/* @var $model Template */

$this->breadcrumbs = array(
    'Templates' => array('index'),
    $model->name,
);

$this->menu = array(
    array('label' => 'List Template', 'url' => array('index')),
    array('label' => 'Create Template', 'url' => array('create')),
    array('label' => 'Update Template', 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Delete Template', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage Template', 'url' => array('admin')),
);
?>
<div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-globe"></i>View Template
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>

                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">

                        <div class="col-md-12">
                            
                        </div>
                    </div>
                </div>
<div class="box-header">
    <h3 class="box-title">View Template <b><?php echo $model->name; ?></b></h3>
</div>
<div class="table-responsive">
    <div class="box-body">
        <?php
        $this->widget('zii.widgets.CDetailView', array(
            'data' => $model,
            'attributes' => array(
                'name',
                array(
                    'name' => 'Creation Date',
                    'value' => date(AppInterface::getdateformat(),$model->created_at),
                ),
                array(
                    'name' => 'Payrates',
                    'value' => AppTemplate::getPayratesForView($model->id)
                ),
                array(
                    'name' => 'Items',
                    'value' => AppTemplate::getItemsForView($model->id)
                ),
            ),
        ));
        ?>
    </div>
</div>
            </div>
</div>