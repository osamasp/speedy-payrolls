<?php
/* @var $this MainController */
/* @var $model Template */

$this->breadcrumbs=array(
	'Templates'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Template', 'url'=>array('index')),
	array('label'=>'Create Template', 'url'=>array('create')),
	array('label'=>'View Template', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Template', 'url'=>array('admin')),
);
?>

<div class="box-header">
    <h3 class="box-title">Update Template <b><?php echo $model->name; ?></b></h3>
</div>

<?php $this->renderPartial('_form', array(
    'model'=>$model,
    'payrates' => $payrates,
            'items' => $items,
    'templates' => AppTemplate::getAllTemplates(),
        )); ?>