<?php
/* @var $this MainController */
/* @var $model Template */
/* @var $form CActiveForm */
?>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/template-main-form.js" type="text/javascript"></script>
<div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-plus"></i>Create New Template
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>

                </div>
            </div>
            <div class="portlet-body form">
                <div class="table-toolbar">
                    <div class="row">

                        <div class="col-md-12">
                            
                        </div>
                    </div>
                </div>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'template-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => false,
        ));
?>
<div class="box-body" style="padding-left:20px;padding-right:20px;">
    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php if ($model->hasErrors()) { ?>
        <div class='callout callout-danger'>    <?php echo $form->errorSummary($model); ?>
        </div>
    <?php } ?>    

    <?php if ($model->isNewRecord) { ?>
        <div class="form-group">
            <?php echo $form->labelEx($model, 'name'); ?>
            <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'name', array('class' => 'text-red')); ?>
        </div>
    <?php } else { ?>
        <div class="form-group">
            <label>Templates</label>
            <select class="form-control" id="templates">
                <?php foreach ($templates as $template) { ?>
                    <option value="<?php echo $template->id; ?>" <?php echo $template->id == $model->id ? 'selected' : ''; ?>><?php echo $template->name; ?></option>
                <?php } ?>
            </select>
        </div>
        <script>
            $('#templates').on('change', function () {
                window.location = '<?php echo Yii::app()->createUrl('/template/main/update') ?>/id/' + $(this).val();
            });
        </script>
    <?php } ?>
    <div class="row">
        <div class="col-md-6">
            <h4>Payrates</h4>
            <?php if ($model->isNewRecord) { ?>
                <div class="row form-group item-row">
                    <div class="col-md-11">
                        <input type="text" name="payrate[]" value="Regular" class="form-control" readonly="">
                    </div>
                </div>
                <div class="row form-group item-row">
                    <div class="col-md-11">
                        <input type="text" name="payrate[]" value="" class="form-control">
                    </div>
                    <div class="col-md-1">
                        <div class="row">
                            <a href="javascript:void(0);" class="remove-closest-item-row" ><i class="fa fa-fw fa-times-circle"></i></a>
                        </div>
                    </div>
                </div>
            <?php } else { ?>
                <div class="row form-group item-row">
                    <div class="col-md-11">
                        <input type="text" name="payrate[]" value="Regular" class="form-control" readonly="">
                    </div>
                </div>
                <?php foreach ($payrates as $payrate) { ?>
                    <?php
                    if ($payrate->name == 'Regular') {
                        continue;
                    } else {
                        ?>
                        <div class="row form-group item-row">
                            <div class="col-md-11">
                                <input type="text" name="payrate[]" value="<?php echo $payrate->name; ?>" class="form-control">
                            </div>
                            <div class="col-md-1">
                                <div class="row">
                                    <a href="javascript:void(0);" class="remove-closest-item-row" ><i class="fa fa-fw fa-times-circle"></i></a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
<?php } ?>
            <div class="row" id="payrate-before">
                <div class="col-md-12 text-center">
                    <a href="javascript:void(0);" id="add-more-payrates"><i class="fa fa-fw fa-plus-square"></i> Add More</a>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <h4>Items</h4>
<?php if ($model->isNewRecord) { ?>
                <div class="row form-group item-row">
                    <div class="col-md-11">
                        <input type="text" name="item[]" value="" class="form-control">
                    </div>
                    <div class="col-md-1">
                        <div class="row">
                            <a href="javascript:void(0);" class="remove-closest-item-row" ><i class="fa fa-fw fa-times-circle"></i></a>
                        </div>
                    </div>
                </div>
            <?php } else { ?>
    <?php foreach ($items as $item) { ?>
                    <div class="row form-group item-row">
                        <div class="col-md-11">
                            <input type="text" name="item[]" value="<?php echo $item->name; ?>" class="form-control">
                        </div>
                        <div class="col-md-1">
                            <div class="row">
                                <a href="javascript:void(0);" class="remove-closest-item-row" ><i class="fa fa-fw fa-times-circle"></i></a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
<?php } ?>
            <div class="row" id="item-before">
                <div class="col-md-12 text-center">
                    <a href="javascript:void(0);" id="add-more-items"><i class="fa fa-fw fa-plus-square"></i> Add More</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="form-actions" style="margin-top:10px;">
        <div class="row">
            <div class="col-md-offset-3 col-md-9">
<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary')); ?>
</div>
        </div>
</div>

<?php $this->endWidget(); ?>
            </div>
</div>