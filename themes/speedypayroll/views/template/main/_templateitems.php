<?php
$payrate_count = 1;
$item_count = 1;
?>
<div class="col-md-5">
    <h4>Payrates</h4>
    <?php foreach ($payrates as $payrate) { ?>
        <div class="row form-group item-row">
            <div class="col-md-3">
                <label>Payrate <?php echo $payrate_count; ?></label>
            </div>
            <div class="col-md-6">
                <input type="text" name="payrate[<?php echo $payrate_count; ?>][name]" value="<?php echo $payrate->name; ?>" class="form-control">
            </div>
            <div class="col-md-3">
                <input type="checkbox" name="payrate[<?php echo $payrate_count; ?>][enable]" checked=""> Enable
            </div>
        </div>
        <?php $payrate_count++; ?>
    <?php } ?>
</div>
<div class="col-md-7">
    <h4>Items</h4>
    <?php foreach ($items as $item) { ?>
        <div class="row form-group item-row">
            <div class="col-md-2">
                <label>Item <?php echo $item_count; ?></label>
            </div>
            <div class="col-md-4">
                <input type="text" name="item[<?php echo $item_count; ?>][name]" value="<?php echo $item->name; ?>" class="form-control">
            </div>
            <div class="col-md-3">
                <input type="text" name="item[<?php echo $item_count; ?>][value]" value="" class="form-control">
            </div>
            <div class="col-md-3">
                <input type="checkbox" name="item[<?php echo $item_count; ?>][enable]" checked=""> Enable
            </div>
        </div>
        <?php $item_count++; ?>
    <?php } ?>
</div>