<?php
/* @var $this MainController */
/* @var $model Template */

$this->breadcrumbs = array(
    'Templates' => array('index'),
    'Create',
);

$this->menu = array(
    array('label' => 'List Template', 'url' => array('index')),
    array('label' => 'Manage Template', 'url' => array('admin')),
);
?>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/template-main-apply.js" type="text/javascript"></script>
<input type="hidden" id="ajax_loader" value="<?php echo "<img src='" . Yii::app()->theme->baseUrl . "/img/ajax-loader.gif' />"; ?>"/>
<input type="hidden" id="template" value="<?php echo Yii::app()->createUrl('/template/main/gettemplateitems'); ?>"/>
<div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-arrow-circle-right"></i>Apply Timesheet Template
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>

                </div>
            </div>
            <div class="portlet-body form">
                <div class="table-toolbar">
                    <div class="row">

                        <div class="col-md-12">
                            
                        </div>
                    </div>
                </div>
<div class="box-header" style="padding-left:20px;">
    <h3 class="box-title">Apply	Timesheet Template</h3>
</div>

<?php
/* @var $this MainController */
/* @var $model Template */
/* @var $form CActiveForm */
?>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'template-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal form-bordered',
    ),
        ));
?>
<div class="form-body" style="padding-left:30px;padding-right:20px;">
    <div class="row">
        <div class="col-md-12">
    <div class="form-group">
        <label>Select Company</label>
        <select class="form-control" name='company_id' required="">
            <option value="">Select</option>
            <?php foreach ($companies as $company) { ?>
                <option value="<?php echo $company->id; ?>"><?php echo $company->name; ?></option>
            <?php } ?>
        </select>
    </div>
        </div>
    </div>
    <div class="row" style="margin-top:10px;">
        <div class="col-md-12">
    <div class="form-group">
        <label>Select Template</label>
        <select class="form-control" id="templates" required="">
            <option value="">Select</option>
            <?php foreach ($templates as $template) { ?>
                <option value="<?php echo $template->id; ?>"><?php echo $template->name; ?></option>
            <?php } ?>
        </select>
    </div>
        </div>
    </div>
    <div class="row" id="template-data">

    </div>
</div>
                <div class="form-actions" style="margin-top:10px;">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" value="Apply" class="btn btn-primary">
                        </div>
                    </div>
                </div>
<?php $this->endWidget(); ?>

            </div></div>