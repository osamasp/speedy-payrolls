<?php
/* @var $this MainController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List User', 'url'=>array('index')),
array('label'=>'Create User', 'url'=>array('create')),
array('label'=>'Update User', 'url'=>array('update', 'id'=>$model->id)),
array('label'=>'Delete User', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage User', 'url'=>array('admin')),
);
?>

<div class="box-header">
    <h3 class="box-title">View User #<?php echo $model->id; ?></h3>
</div>
<div class="table-responsive">
    <div class="box-body">
        <?php $this->widget('zii.widgets.CDetailView', array(
        'data'=>$model,
        'attributes'=>array(
        		'id',
		'first_name',
		'last_name',
		'email',
		'password',
		'ni_number',
		'dob',
		'phone',
		'addressline_1',
		'street',
		'city',
		'country',
		'postcode',
		'photo',
		'type',
		'created_at',
		'created_by',
		'modified_at',
		'modified_by',
        ),
        )); ?>
    </div>
</div>