<?php
/* @var $this MainController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Users',
);

$this->menu=array(
array('label'=>'Create User', 'url'=>array('create')),
array('label'=>'Manage User', 'url'=>array('admin')),
);
?>

<div class="box-header">
    <h3 class="box-title">Users</h3>
    <div class="box-tools">
        <a class="btn btn-default pull-right" href="<?php echo $this->createUrl('create'); ?>">Create User</a>
    </div>
</div>

<div class="box-body table-responsive">
    <table id="example1" class="table table-responsive table-striped">
        <thead>
            <tr>
                                                        <th><?php echo User::model()->getAttributeLabel('id') ?></th>
                                                        <th><?php echo User::model()->getAttributeLabel('first_name') ?></th>
                                                        <th><?php echo User::model()->getAttributeLabel('last_name') ?></th>
                                                        <th><?php echo User::model()->getAttributeLabel('email') ?></th>
                                                        <th><?php echo User::model()->getAttributeLabel('password') ?></th>
                                                        <th><?php echo User::model()->getAttributeLabel('ni_number') ?></th>
                                                        <th><?php echo User::model()->getAttributeLabel('dob') ?></th>
                                                        <th><?php echo User::model()->getAttributeLabel('phone') ?></th>
                                                        <th><?php echo User::model()->getAttributeLabel('addressline_1') ?></th>
                                                        <th><?php echo User::model()->getAttributeLabel('street') ?></th>
                                                        <th><?php echo User::model()->getAttributeLabel('city') ?></th>
                                                        <th><?php echo User::model()->getAttributeLabel('country') ?></th>
                                                        <th><?php echo User::model()->getAttributeLabel('postcode') ?></th>
                                                        <th><?php echo User::model()->getAttributeLabel('photo') ?></th>
                                                        <th><?php echo User::model()->getAttributeLabel('type') ?></th>
                                                        <th><?php echo User::model()->getAttributeLabel('created_at') ?></th>
                                                        <th><?php echo User::model()->getAttributeLabel('created_by') ?></th>
                                                        <th><?php echo User::model()->getAttributeLabel('modified_at') ?></th>
                                                        <th><?php echo User::model()->getAttributeLabel('modified_by') ?></th>
                                    <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($dataProvider as $item) { ?>
            <tr>
                                                        <td><?php echo $item->id; ?></td>
                                                        <td><?php echo $item->first_name; ?></td>
                                                        <td><?php echo $item->last_name; ?></td>
                                                        <td><?php echo $item->email; ?></td>
                                                        <td><?php echo $item->password; ?></td>
                                                        <td><?php echo $item->ni_number; ?></td>
                                                        <td><?php echo $item->dob; ?></td>
                                                        <td><?php echo $item->phone; ?></td>
                                                        <td><?php echo $item->addressline_1; ?></td>
                                                        <td><?php echo $item->street; ?></td>
                                                        <td><?php echo $item->city; ?></td>
                                                        <td><?php echo $item->country; ?></td>
                                                        <td><?php echo $item->postcode; ?></td>
                                                        <td><?php echo $item->photo; ?></td>
                                                        <td><?php echo $item->type; ?></td>
                                                        <td><?php echo $item->created_at; ?></td>
                                                        <td><?php echo $item->created_by; ?></td>
                                                        <td><?php echo $item->modified_at; ?></td>
                                                        <td><?php echo $item->modified_by; ?></td>
                                    <td>
                        
                        <div class="btn-group">
                            <a class="btn btn-primary" style="padding-top: 0px;padding-bottom: 0px;" href="<?php echo $this->createUrl('view', array('id' => $item->id)); ?>">View</a>
                            <button type="button" class="btn btn-primary dropdown-toggle" style="padding-top: 0px;padding-bottom: 0px;" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php echo $this->createUrl('update', array('id' => $item->id)); ?>">Update</a></li>
                                <li class="divider"></li>
                                <li><a href="<?php echo $this->createUrl('delete', array('id' => $item->id)); ?>">Delete</a></li>
                            </ul>
                        </div>
                        
                    </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>