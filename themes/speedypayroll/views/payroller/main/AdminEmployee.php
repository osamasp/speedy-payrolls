<?php
/* @var $this MainController */
/* @var $model Timesheet */

$this->breadcrumbs = array(
    'Timesheets' => array('index'),
);
$not_included_fields = array('id', 'company_id', 'parent_id', 'role', 'employee_type', 'approver_id', 'payroller_id');
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'myForm',
    'enableAjaxValidation' => false,
        ));
?>
<input type="hidden" id="select_msg" value="<?php echo ConstantMessages::$req_select; ?>">
<input type="hidden" id="export_url" value="<?php echo $this->createUrl('export'); ?>">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/company-companyshifts-approvallist.js" type="text/javascript"></script>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/dropdowns-enhancement.css" rel="stylesheet">
<!-- data table starts -->

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box grey-cascade">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-globe"></i>Payroll Inbox for Employee
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>

                </div>
            </div>
            <div class="portlet-body table-responsive">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-12">
                            <?php if (AppUser::isUserAdmin()) { ?>
                                <div class="btn-group pull-right"style="margin-top:12px; margin-right: 25px;">
                                    <a href="javascript:void(0);" class="btn green dropdown-toggle" id="openButton">Export <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">				
                                        <li><a id="export" href="javascript:void(0);" onclick="exportData('export')">Export Selected Record</a></li>
                                        <li class="divider"></li>

                                        <li><a href="javascript:void(0);" id="export_all" onclick="exportData('export_all')">Export All Record</a></li>
                                        <?php if (isset($entries[0])) { ?>
                                            <li><input type="checkbox" id="All" value="1"> <label for="All">Select All</label></li>

                                            <?php
                                            foreach ($entries[0] as $key => $item) {
                                                if (!in_array($key, $not_included_fields)) {
                                                    ?>
                                                    <li><input class="select" type="checkbox" name="export_fields[]" id="<?php echo $key; ?>" value="<?php echo $key; ?>"><label for="<?php echo $key; ?>"><?php echo UserTimings::model()->getAttributeLabel($key); ?></label></li>
                                                    <?php
                                                }
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                    <input type="hidden" name='action'  id='action' value="merge" />
                    <table class="table table-striped table-bordered table-hover dataTable" id="example1" aria-describedby="example2_info">
                        <thead>
                            <tr>
                                <th rowspan="1" colspan="1"><input type="checkbox" data-checkbox="true" class="group-checkable" id="checkAll" onchange="" data-set="#sample_1 .checkboxes"/></th>
                                <th rowspan="1" colspan="1">Name</th>
                                <th rowspan="1" colspan="1">ID</th>
                                <th rowspan="1" colspan="1">Employment Type</th>
                                <th rowspan="1" colspan="1">Date Joined</th>
                                <th rowspan="1" colspan="1">Date Ending</th>
                                <th rowspan="1" colspan="1">Location</th>
                                <th rowspan="1" colspan="1">Supervisor</th>
                                <th rowspan="1" colspan="1">Action</th>
                            </tr>
                        </thead>
                        <?php if (count($entries) > 0) { ?>
                            <tbody role="alert" aria-live="polite" aria-relevant="all">
                                <?php
                                foreach ($entries as $entry) {
                                    ?>
                                    <tr class="gradeX">
                                        <td><input type="checkbox" name="entry[]" class="check" value="<?php echo $entry->id; ?>"/></td>
                                        <td><?php echo $entry->user->full_name; ?></td>
                                        <td><?php echo AppInterface::formatID($entry->user->id); ?></td>
                                        <td><?php echo AppContract::formatEmployeType($entry->employee_type); ?></td>
                                        <td><?php echo date(AppInterface::getdateformat(), $entry->start_time); ?></td>
                                        <td><?php echo ($entry->end_time != -1) ? date(AppInterface::getdateformat(), $entry->end_time) : '-'; ?></td>
                                        <td><?php echo $entry->company ? $entry->company->country : '-'; ?></td>
                                        <td><?php echo $entry->approver ? $entry->approver->full_name : '-'; ?></td>
                                        <td><a class="label label-sm label-success" href="<?php echo Yii::app()->createUrl('/payroller/main/AdminTimesheets', array('user' => $entry->user->id)); ?>">View Timesheets</a></td>

                                    </tr>
                                <?php } ?>
                            </tbody>
                        <?php } ?>
                    </table>
                    <?php
//                    $this->widget('CLinkPager', array(
//                        'pages' => $pages,
//                        'header' => '',
//                        'footer' => '',
//                        'maxButtonCount' => 3,
//                        'nextPageLabel' => 'Next',
//                        'prevPageLabel' => 'Prev',
//                        'selectedPageCssClass' => 'active',
//                        'hiddenPageCssClass' => 'disabled',
//                        'htmlOptions' => array(
//                            'class' => 'pagination pull-right',
//                        )
//                    ));
//                    ?>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>      <!-- /data table ends -->

<?php $this->endWidget(); ?>
<br/>
<div class="childEntryContainer" id="childEntryContainer" style="display:none">

</div>
