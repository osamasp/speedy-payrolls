<?php
/* @var $this MainController */
/* @var $payroll Payroll */

$this->breadcrumbs = array(
    'Timesheets' => array('index'),
);
$not_included_fields = array('id');
?>
<form id="myForm" method="post" >

<input type="hidden" id="select_msg" value="<?php echo ConstantMessages::$req_select; ?>">
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/dropdowns-enhancement.css" rel="stylesheet">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/payroller-main.js"></script>
<input type="hidden" id="url" value="<?php echo $this->createUrl("/timesheet/main/GetSubEntries"); ?>"/>
<input type="hidden" id="export_url" value="<?php echo $this->createUrl("export"); ?>"/>
<input type="hidden" name="status" value="<?php echo $status; ?>"/>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <?php if ($type == '1') { ?>
                <i class="fa fa-file-o"></i>ePayslips Pending
            <?php } else if ($type == '2') { ?>
                <i class="fa fa-file-o"></i>ePayslips Archives
            <?php } ?>
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>

        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-12">
                    <?php if (AppUser::isUserAdmin()) { ?>
                        <div class="btn-group pull-right">
                            <button class="btn green dropdown-toggle" data-toggle="dropdown">Export <i class="fa fa-angle-down"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                                <?php if ($status == 'requested') { ?>
                                    <input type="hidden" name="requested" value="requested"/>
                                    <li><a id="export" href="javascript:void(0);" onclick="exportData('export')">Export Selected Record</a></li>
                                    <li class="divider"></li>
                                    <li><a  id="export_all" href="javascript:void(0);"  onclick="exportData('export_all')">Export All Record</a></li>
                                <?php } else { ?>
                                    <input type="hidden" name="completed" value="completed"/>
                                    <li><a id="export" href="javascript:void(0);" onclick="exportData('export', 'completed')">Export Selected Record</a></li>
                                    <li class="divider"></li>
                                    <li><a  id="export_all" href="javascript:void(0);"  onclick="exportData('export_all', 'completed')">Export All Record</a></li>
                                <?php } ?>                
                                <li><input type="checkbox" name="export_fields[]" id="All" value="1"> <label id="selectAll" for="All">Select All</label></li>
                                <?php
                                if (isset($entries['payrolls'][0])) { ?>
                                    <li><input class="select" type="checkbox" name="export_fields[]" id="contract" value="contract"><label for="contract">Contract</label></li>
                                    <li><input class="select" type="checkbox" name="export_fields[]" id="employee" value="employee"><label for="employee">Employee</label></li>
                                    <li><input class="select" type="checkbox" name="export_fields[]" id="approver" value="approver"><label for="approver">Approver</label></li>
                                    <li><input class="select" type="checkbox" name="export_fields[]" id="period" value="period"><label for="period">Period</label></li>
                                    <li><input class="select" type="checkbox" name="export_fields[]" id="status" value="status"><label for="status">Status</label></li>
                               <?php }
                                ?>
                            </ul>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <!--<div class="box-header">
            <h3 class="box-title">Seminant Solutions</h3>
        </div>-->
        <div class="box-body table-responsive">
            <form id="entryForm" method="post" >
                <input type="hidden" name='action'  id='action' value="merge" />
                <table class="table table-striped table-bordered table-hover dataTable" id="example1" aria-describedby="example2_info">

                    <thead>
                        <tr>
                            <th><input type="checkbox" data-checkbox="true" class="group-checkable" id="checkAll" onchange="" data-set="#sample_1 .checkboxes"/></th>
                            <th rowspan="1" colspan="1">Employee</th>
                            <th rowspan="1" colspan="1">Contract</th>
                            <th rowspan="1" colspan="1">Approver</th>
                            <th rowspan="1" colspan="1">Period</th>
                            <th rowspan="1" colspan="1">Status</th>
                        </tr>
                    </thead>
                    <?php if (count($entries['payrolls']) > 0) { ?>
                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                            <?php foreach ($entries['payrolls'] as $payroll) { ?>
                                <tr class="gradeX">
                                    <td><input type="checkbox" name="entry[]" class="check" value="<?php echo $payroll->id; ?>"/></td>
                                    <td><?php echo $payroll->timesheet->contract->user->full_name; ?></td>
                                    <td><?php echo AppContract::getContractCustomName($payroll->timesheet->contract) . AppTimeSheet::printMergeLogName($payroll->timesheet); ?></td>
                                    <td><?php echo $payroll->timesheet->contract->approver->full_name; ?></td>
                                    <td><?php echo AppTimeSheet::formatSheetPeriod($payroll->timesheet); ?></td>
                                    <td><?php
                                        if ($payroll->status == 'completed') {
                                            $payslipEntry = PayslipEntry::model()->findByAttributes(array('payroll_id' => $payroll->id));
                                            if ($payslipEntry->type == 'csv') {
                                                ?>
                                                <a href="#" data-toggle="modal" data-target=".ajaxModel" data-url="<?php echo Yii::app()->createUrl("/payroller/payslip/showInTemplate"); ?>" data-whatever="<?php echo $payslipEntry->payroll_id; ?>" >View Issued Payslip</a>
                                                <?php
                                            } else {
                                                ?>
                                                <a href="<?php echo Yii::app()->createUrl('payroller/payslip/download', array('id' => $payslipEntry->id)); ?>">Download Payslip</a>
                                                <?php
                                            }
                                        } else
                                            echo $payroll->status;
                                        ?> </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    <?php } ?>

                </table>
                <?php
                $this->widget('CLinkPager', array(
                    'pages' => $entries['pages'],
                    'header' => '',
                    'footer' => '',
                    'maxButtonCount' => 3,
                    'nextPageLabel' => 'Next',
                    'prevPageLabel' => 'Prev',
                    'selectedPageCssClass' => 'active',
                    'hiddenPageCssClass' => 'disabled',
                    'htmlOptions' => array(
                        'class' => 'pagination pull-right',
                    )
                ));
                ?>
            </form>
        </div>

        <br/>
        <div class="childEntryContainer" id="childEntryContainer" style="display:none">

        </div>
    </div>
</div>
</form>
