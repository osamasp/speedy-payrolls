<?php
/* @var $this MainController */
/* @var $model Timesheet */

$this->breadcrumbs = array(
    'Timesheets' => array('index'),
);
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/payroller-main.js"></script>
<input type="hidden" id="url" value="<?php echo $this->createUrl("/timesheet/main/GetSubEntries");?>"/>
<!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box grey-cascade">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-globe"></i>Dispatch Payroll
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>

                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="btn-group pull-right">
                                <a class="btn btn-primary btn-circle" href="<?php echo $this->createUrl('exportdispatch'); ?>" >Export</a>
                            </div>
                        </div>
                    </div>
                </div>

<!--<div class="box-header">
    <h3 class="box-title">Dispatch Payroll Folder</h3>
</div>-->
<div class="box-body table-responsive">
    <form id="entryForm" method="post" >
        <!--<input type="hidden" name='action'  id='action' value="merge" />-->
        <table class="table table-striped table-bordered table-hover dataTable" aria-describedby="example2_info" id="example1">

            <thead>
                <tr>
                    <th rowspan="1" colspan="1"></th>
                    <th rowspan="1" colspan="1">Employee</th>
                    <th rowspan="1" colspan="1">Contract</th>
                    <th rowspan="1" colspan="1">Period</th>
                    <th rowspan="1" colspan="1">Status</th>
                    <th rowspan="1" colspan="1">Dispatch</th>
                    <th rowspan="1" colspan="1">Action</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th rowspan="1" colspan="7" align="right">
          
                 <?php if (AppUser::isUserAdmin() && count($entries)>0) { ?>

                        <button type="submit" class="btn btn-circle btn-sm green-haze" name="action[]" value="merge">Merge Timesheets</button>
                        <button type="submit" class="btn btn-circle btn-sm green-haze" name="action[]" value="payroll" />Request Payrolls</button>
            <?php } ?>
            </div>
            </th>

            </tr>
            </tfoot>
            <?php if (count($entries) > 0) { ?>
                <tbody role="alert" aria-live="polite" aria-relevant="all">
                    <?php
                    foreach ($entries as $entry) {
                        $paySlipStatus = AppPayslip::isIssued($entry);
                        ?>
                        <tr class="gradeX">
                            <td>
                                <input type="checkbox" name="entry[]" value="<?php echo $entry->id; ?>" clas="form-control" />
                            </td>
                            <td><?php echo $entry->contract->user->full_name; ?></td>
                            <td><?php echo AppContract::getContractCustomName($entry->contract).AppTimeSheet::printMergeLogName($entry); ?></td>
                            
                            <td>
                                <?php echo AppTimeSheet::formatSheetPeriod($entry); ?>
                            </td>
                            <td><?php echo $entry->dispatch_status; ?></td>
                            <td><?php echo ($entry->dispatch_status=="copied") ? 'Ready' : ''; ?></td>
                            <td>
                                <div class="btn-group">
                                            <button class="btn green dropdown-toggle" data-toggle="dropdown">Actions <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a data-toggle="modal" data-target=".ajaxModel" data-url="<?php echo Yii::app()->createUrl('/timesheet/main/GetSheetCard'); ?>" data-whatever="<?php echo $entry->id; ?>" >View</a></li>
                                        <?php if (( $entry->type != "daily" && ( AppUser::isUserAdmin() || $entry->status == "pending"))) { ?>
                                            <li><a href="<?php echo Yii::app()->createUrl("timesheet/main/edit", array("entry" => $entry->id)); ?>">Update</a></li>
                                        <?php } ?>
                                       
                                        <?php if ($entry->type == "daily") { ?>
                                            <li><a href="#childEntryContainer" class="<?php echo ($entry->type == "daily") ? "wklyLnkPrnt" : ""; ?>" tSheetId="<?php echo $entry->id; ?>">Show TimeCard</a></li>
                                        <?php } ?>
                                       
                                    </ul>
                                </div>

                            </td>
                            
                        </tr>
                    <?php } ?>
                </tbody>
            <?php } ?>
        </table>
        <?php
                    $this->widget('CLinkPager', array(
                        'pages' => $pages,
                        'header' => '',
                        'footer' => '',
                        'maxButtonCount'=>3,
                        'nextPageLabel' => 'Next',
                        'prevPageLabel' => 'Prev',
                        'selectedPageCssClass' => 'active',
                        'hiddenPageCssClass' => 'disabled',
                        'htmlOptions' => array(
                            'class'=>'pagination pull-right',
                        )
                    ));

        ?>
    </form>
</div>

<br/>
<div class="childEntryContainer" id="childEntryContainer" style="display:none">

</div>
        </div>
</div>