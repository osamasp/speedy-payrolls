<?php
/* @var $this ApproveController */
/* @var $model Timesheet */

$this->breadcrumbs = array(
    'Approve Timesheets' => array('index'),
);
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/payroller-main.js"></script>
<input type="hidden" id="url" value="<?php echo $this->createUrl("/timesheet/main/GetSubEntries");?>"/>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <?php if (!isset($status)) { ?>
                <i class="fa fa-globe"></i>Payroll Inbox for Employee
            <?php } else { ?>
                <i class="fa fa-globe"></i>ePayslip Inbox Accounts
            <?php } ?>
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>

        </div>
    </div>
    <div class="portlet-body">


        <div class="box-body table-responsive">

            <?php
            if ($user != null) {
                echo '<p>You are watching dispatch payroll timesheets of ' . $user->full_name . '</p>';
            }
            ?>




            <table class="table table-striped table-bordered table-hover dataTable" id="example1" aria-describedby="example2_info">

                <thead>
                    <tr>
                        <th rowspan="1" colspan="1"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes"></th>
                        <th rowspan="1" colspan="1">Contractor/Staff</th>
                        <th rowspan="1" colspan="1">Supervisor</th>
                        <th rowspan="1" colspan="1">Period</th>
                        <th rowspan="1" colspan="1">Status</th>
                        <th rowspan="1" colspan="1">Payslip</th>
                        <th rowspan="1" colspan="1">Action</th>
                    </tr>
                </thead>
                
                <?php if (count($entries) > 0) { ?>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                        <?php
                        foreach ($entries as $entry) {
                            $paySlipStatus = AppPayslip::isIssued($entry);
                            $type = '';
                            $payroll = null;
                            if (count($entry->payrolls) > 0) {
                                $payroll = $entry->payrolls[0];
                            }
                            if ($paySlipStatus) {
                                $sEntry = $payroll->payslipEntries[0];
                                $type = $sEntry->type;
                            }
                            $payrolls = $entry->payrolls;
                            ?>
                            <tr class="gradeX">
                                <td><input type="checkbox" class="group-checkable" value="<?php echo $entry->id;?>" data-set="#sample_1 .checkboxes"></td>
                                <td class=" "><?php echo $entry->contract->user->full_name; ?></td>
                                <td class=""><?php echo $entry->contract->approver->full_name; ?></td>
                                <td class=" ">
                                    <?php echo AppTimeSheet::formatSheetPeriod($entry); ?>
                                </td>

                                <td class=" "><?php echo ($paySlipStatus == false) ? 'pending' : 'completed'; ?></td>
                                <td class=" "><?php echo ($paySlipStatus == false) ? 'N/A' : 'issued'; ?></td>
                                <td class=" ">
                                    <div class="btn-group">
                                            <button class="btn green dropdown-toggle" data-toggle="dropdown">Actions <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-right">

                                            <?php if ($paySlipStatus == true && $type == 'csv') { ?>
                                                <li><a data-toggle="modal" data-target=".ajaxModel" data-url="<?php echo Yii::app()->createUrl("/payroller/payslip/showInTemplate"); ?>" data-whatever="<?php echo $sEntry->id; ?>" >View Issued Payslip</a></li>
                                                <li class="divider"></li>
                                            <?php } elseif ($paySlipStatus == true && $type == "other") { ?>
                                                <li><a href="<?php echo Yii::app()->createUrl('/payroller/payslip/download/', array('id' => $sEntry->id)); ?>" >Download PaySlip</a></li>
                                                <li class="divider"></li>
                                            <?php } ?>

                                            <li><a data-toggle="modal" data-target=".ajaxModel" data-url="<?php echo Yii::app()->createUrl("/timesheet/main/GetSheetCard"); ?>" data-whatever="<?php echo $entry->id; ?>" >View TimeCard</a></li>
                                            <li class="divider"></li>
                                            <li><a href="<?php echo Yii::app()->createUrl("payroller/payslip/issue", array("id" => $payroll->id)); ?>">Update</a></li>

                                            <?php if ($entry->type == "daily") { ?>
                                                <li class="divider"></li>
                                                <li><a href="#childEntryContainer" class="<?php echo ($entry->type == "daily") ? "wklyLnkPrnt" : ""; ?>" tSheetId="<?php echo $entry->id; ?>">Show TimeCard</a></li>
                                            <?php } ?>
                                            <?php if ($paySlipStatus == false) { ?>
                                                <li class="divider"></li>
                                                <li><a href="<?php echo Yii::app()->createUrl('payroller/payslip/issue', array("id" => $payroll->id)); ?>" >Issue Payslip</a></li>
                                            <?php } ?>
                                        </ul>
                                    </div>

                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                <?php } ?>

            </table>
            <?php
                    $this->widget('CLinkPager', array(
                        'pages' => $pages,
                        'header' => '',
                        'footer' => '',
                        'maxButtonCount'=>3,
                        'nextPageLabel' => 'Next',
                        'prevPageLabel' => 'Prev',
                        'selectedPageCssClass' => 'active',
                        'hiddenPageCssClass' => 'disabled',
                        'htmlOptions' => array(
                            'class'=>'pagination pull-right',
                        )
                    ));

            ?>
        </div>

        <br/>
        <div class="childEntryContainer" style="display:none">

        </div>
    </div>
</div>