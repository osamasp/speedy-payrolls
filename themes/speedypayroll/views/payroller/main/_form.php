<?php
/* @var $this MainController */
/* @var $model User */
/* @var $form CActiveForm */
?>
<!-- Begin Wizard-->
<div class="row">
    <div class="regwizard">
        <div class="portlet box blue" id="form_wizard_1">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i> Speedy Registration - <span class="step-title">
                        Step 2 of 4 </span>
                </div>
            </div>
            <div class="portlet-body form">
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'submit_form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => false,
        ));
?>
                <div class="form-wizard">
                        <div class="form-body">
                            <ul class="nav nav-pills nav-justified steps">
                                <li class="done">
                                    <a href="javascript:;" data-toggle="tab" class="step">
                                        <span class="number">
                                            1 </span>
                                        <span class="desc">
                                            <i class="fa fa-check"></i> Account Type </span>
                                    </a>
                                </li>
                                <li class="active" id="menuTab2">
                                    <a href="#tab2" data-toggle="tab" onclick="setActions('back')" class="step">
                                        <span class="number">
                                            2 </span>
                                        <span class="desc">
                                            <i class="fa fa-check"></i> Your Details </span>
                                    </a>
                                </li>
                                <li id="menuTab3">
                                    <a href="#tab3" data-toggle="tab" onclick="setActions('continue')" class="step active">
                                        <span class="number">
                                            3 </span>
                                        <span class="desc">
                                            <i class="fa fa-check"></i> Company Details </span>
                                    </a>
                                </li>
                                <li id="menuTab4">
                                    <a href="#tab4" data-toggle="tab" onclick="setActions('continue')" class="step active">
                                        <span class="number">
                                            4 </span>
                                        <span class="desc">
                                            <i class="fa fa-check"></i> Finish </span>
                                    </a>
                                </li>
                            </ul>

                            <div id="bar" class="progress progress-striped" role="progressbar">
                                <div class="progress-bar progress-bar-success" id="progress" style="width:50%;">
                                </div>
                            </div>
<div class="tab-content">
    <?php if ($model->hasErrors()) { ?>
        <div class='callout callout-danger'>    <?php echo $form->errorSummary($model); ?>
        </div>
    <?php } ?>
     <!-- tab2 -->

                                <div class="tab-pane active" id="tab2">
                                    <h3 class="block">Your Details</h3>

                                    <div class="row">
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Company Name</label>
                                                <div class="col-md-9">
                                                    <input type="text" placeholder="Company Name" class="form-control" required="required" name="Company[name]" id="payroller_company">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">First Name</label>
                                                <div class="col-md-9">
                                                    <?php echo $form->textField($model, 'first_name', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control', 'required' => 'required', 'placeholder ' => 'First Name')); ?>
                                                    <?php echo $form->error($model, 'first_name', array('class' => 'text-red')); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Last Name</label>
                                                <div class="col-md-9">
                                                    <?php echo $form->textField($model, 'last_name', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control', 'required' => 'required', 'placeholder ' => 'Last Name')); ?>
        <?php echo $form->error($model, 'last_name', array('class' => 'text-red')); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Phone Number</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="Phone" name="Company[phone]" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Staff ID</label>
                                                <div class="col-md-9">
                                                    <?php echo $form->textField($model, 'staff_id', array('placeholder' => 'Staff ID', 'class' => 'form-control', 'required' => 'required')); ?>
                                                </div>
                                            </div>
                                        </div>
                                       </div>
                                </div>


                                <!-- /tab2 -->
                                <!-- tab3 -->
                                <div class="tab-pane" id="tab3">
                                    <h3 class="block">Company Details</h3>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Sector</label>
                                                <div class="col-md-9">
                                                    <?php
        echo CHtml::dropDownList('Company[sector]', 'none', array('none' => 'Select Sector',
            ' Agriculture ' => ' Agriculture ',
            ' Accounting ' => ' Accounting ',
            ' Advertising ' => ' Advertising ',
            ' Aerospace ' => ' Aerospace ',
            ' Aircraft ' => ' Aircraft ',
            ' Airline ' => ' Airline ',
            ' Apparel & Accessories ' => ' Apparel & Accessories ',
            ' Automotive ' => ' Automotive ',
            ' Banking ' => ' Banking ',
            ' Broadcasting ' => ' Broadcasting ',
            ' Brokerage ' => ' Brokerage ',
            ' Biotechnology ' => ' Biotechnology ',
            ' Call Centers ' => ' Call Centers ',
            ' Cargo Handling ' => ' Cargo Handling ',
            ' Chemical ' => ' Chemical ',
            ' Computer ' => ' Computer ',
            ' Consulting ' => ' Consulting ',
            ' Consumer Products ' => ' Consumer Products ',
            ' Cosmetics ' => ' Cosmetics ',
            ' Defense ' => ' Defense ',
            ' Department Stores ' => ' Department Stores ',
            ' Education ' => ' Education ',
            ' Electronics ' => ' Electronics ',
            ' Energy ' => ' Energy ',
            ' Entertainment & Leisure ' => ' Entertainment & Leisure ',
            ' Executive Search ' => ' Executive Search ',
            ' Financial Services ' => ' Financial Services ',
            ' Food, Beverage & Tobacco ' => ' Food, Beverage & Tobacco ',
            ' Grocery ' => ' Grocery ',
            ' Health Care ' => ' Health Care ',
            ' Internet Publishing ' => ' Internet Publishing ',
            ' Investment Banking ' => ' Investment Banking ',
            ' Legal ' => ' Legal ',
            ' Manufacturing ' => ' Manufacturing ',
            ' Motion Picture & Video ' => ' Motion Picture & Video ',
            ' Music ' => ' Music ',
            ' Newspaper Publishers ' => ' Newspaper Publishers ',
            ' Online Auctions ' => ' Online Auctions ',
            ' Pension Funds ' => ' Pension Funds ',
            ' Pharmaceuticals ' => ' Pharmaceuticals ',
            ' Private Equity ' => ' Private Equity ',
            ' Publishing ' => ' Publishing ',
            ' Real Estate ' => ' Real Estate ',
            ' Retail & Wholesale ' => ' Retail & Wholesale ',
            ' Securities & Commodity Exchanges ' => ' Securities & Commodity Exchanges ',
            ' Service ' => ' Service ',
            ' Soap & Detergent ' => ' Soap & Detergent ',
            ' Software ' => ' Software ',
            ' Sports ' => ' Sports ',
            ' Technology ' => ' Technology ',
            ' Telecommunications ' => ' Telecommunications ',
            ' Television ' => ' Television ',
            ' Transportation ' => ' Transportation ',
            ' Trucking ' => ' Trucking ',
            ' Venture Capital ' => ' Venture Capital '), array('placeholder' => 'Company Sector', 'class' => 'form-control'));
        ?>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    
                                    <h3 class="form-section">Company Address</h3>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Address</label>
                                                <div class="col-md-9">
                                                    <input type="text" placeholder="Company Address" class="form-control" required="required" name="Company[address]" id="payroller_company">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Street</label>
                                                <div class="col-md-9">
                                                    <input type="text" placeholder="Company Street" class="form-control" required="required" name="Company[street]" id="payroller_company">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">City</label>
                                                <div class="col-md-9">
                                                    <input type="text" placeholder="Company City" class="form-control" required="required" name="Company[city]" >
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Country</label>
                                                <div class="col-md-9">
<select name="Company[country]" class="form-control" required="required">
            <option value="Afghanistan">Afghanistan</option>
            <option value="Albania">Albania</option>
            <option value="Algeria">Algeria</option>
            <option value="American Samoa">American Samoa</option>
            <option value="Andorra">Andorra</option>
            <option value="Angola">Angola</option>
            <option value="Anguilla">Anguilla</option>
            <option value="Antartica">Antarctica</option>
            <option value="Antigua and Barbuda">Antigua and Barbuda</option>
            <option value="Argentina">Argentina</option>
            <option value="Armenia">Armenia</option>
            <option value="Aruba">Aruba</option>
            <option value="Australia">Australia</option>
            <option value="Austria">Austria</option>
            <option value="Azerbaijan">Azerbaijan</option>
            <option value="Bahamas">Bahamas</option>
            <option value="Bahrain">Bahrain</option>
            <option value="Bangladesh">Bangladesh</option>
            <option value="Barbados">Barbados</option>
            <option value="Belarus">Belarus</option>
            <option value="Belgium">Belgium</option>
            <option value="Belize">Belize</option>
            <option value="Benin">Benin</option>
            <option value="Bermuda">Bermuda</option>
            <option value="Bhutan">Bhutan</option>
            <option value="Bolivia">Bolivia</option>
            <option value="Bosnia and Herzegowina">Bosnia and Herzegowina</option>
            <option value="Botswana">Botswana</option>
            <option value="Bouvet Island">Bouvet Island</option>
            <option value="Brazil">Brazil</option>
            <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
            <option value="Brunei Darussalam">Brunei Darussalam</option>
            <option value="Bulgaria">Bulgaria</option>
            <option value="Burkina Faso">Burkina Faso</option>
            <option value="Burundi">Burundi</option>
            <option value="Cambodia">Cambodia</option>
            <option value="Cameroon">Cameroon</option>
            <option value="Canada">Canada</option>
            <option value="Cape Verde">Cape Verde</option>
            <option value="Cayman Islands">Cayman Islands</option>
            <option value="Central African Republic">Central African Republic</option>
            <option value="Chad">Chad</option>
            <option value="Chile">Chile</option>
            <option value="China">China</option>
            <option value="Christmas Island">Christmas Island</option>
            <option value="Cocos Islands">Cocos (Keeling) Islands</option>
            <option value="Colombia">Colombia</option>
            <option value="Comoros">Comoros</option>
            <option value="Congo">Congo</option>
            <option value="Congo">Congo, the Democratic Republic of the</option>
            <option value="Cook Islands">Cook Islands</option>
            <option value="Costa Rica">Costa Rica</option>
            <option value="Cota D'Ivoire">Cote d'Ivoire</option>
            <option value="Croatia">Croatia (Hrvatska)</option>
            <option value="Cuba">Cuba</option>
            <option value="Cyprus">Cyprus</option>
            <option value="Czech Republic">Czech Republic</option>
            <option value="Denmark">Denmark</option>
            <option value="Djibouti">Djibouti</option>
            <option value="Dominica">Dominica</option>
            <option value="Dominican Republic">Dominican Republic</option>
            <option value="East Timor">East Timor</option>
            <option value="Ecuador">Ecuador</option>
            <option value="Egypt">Egypt</option>
            <option value="El Salvador">El Salvador</option>
            <option value="Equatorial Guinea">Equatorial Guinea</option>
            <option value="Eritrea">Eritrea</option>
            <option value="Estonia">Estonia</option>
            <option value="Ethiopia">Ethiopia</option>
            <option value="Falkland Islands">Falkland Islands (Malvinas)</option>
            <option value="Faroe Islands">Faroe Islands</option>
            <option value="Fiji">Fiji</option>
            <option value="Finland">Finland</option>
            <option value="France">France</option>
            <option value="France Metropolitan">France, Metropolitan</option>
            <option value="French Guiana">French Guiana</option>
            <option value="French Polynesia">French Polynesia</option>
            <option value="French Southern Territories">French Southern Territories</option>
            <option value="Gabon">Gabon</option>
            <option value="Gambia">Gambia</option>
            <option value="Georgia">Georgia</option>
            <option value="Germany">Germany</option>
            <option value="Ghana">Ghana</option>
            <option value="Gibraltar">Gibraltar</option>
            <option value="Greece">Greece</option>
            <option value="Greenland">Greenland</option>
            <option value="Grenada">Grenada</option>
            <option value="Guadeloupe">Guadeloupe</option>
            <option value="Guam">Guam</option>
            <option value="Guatemala">Guatemala</option>
            <option value="Guinea">Guinea</option>
            <option value="Guinea-Bissau">Guinea-Bissau</option>
            <option value="Guyana">Guyana</option>
            <option value="Haiti">Haiti</option>
            <option value="Heard and McDonald Islands">Heard and Mc Donald Islands</option>
            <option value="Holy See">Holy See (Vatican City State)</option>
            <option value="Honduras">Honduras</option>
            <option value="Hong Kong">Hong Kong</option>
            <option value="Hungary">Hungary</option>
            <option value="Iceland">Iceland</option>
            <option value="India">India</option>
            <option value="Indonesia">Indonesia</option>
            <option value="Iran">Iran (Islamic Republic of)</option>
            <option value="Iraq">Iraq</option>
            <option value="Ireland">Ireland</option>
            <option value="Israel">Israel</option>
            <option value="Italy">Italy</option>
            <option value="Jamaica">Jamaica</option>
            <option value="Japan">Japan</option>
            <option value="Jordan">Jordan</option>
            <option value="Kazakhstan">Kazakhstan</option>
            <option value="Kenya">Kenya</option>
            <option value="Kiribati">Kiribati</option>
            <option value="Democratic People's Republic of Korea">Korea, Democratic People's Republic of</option>
            <option value="Korea">Korea, Republic of</option>
            <option value="Kuwait">Kuwait</option>
            <option value="Kyrgyzstan">Kyrgyzstan</option>
            <option value="Lao">Lao People's Democratic Republic</option>
            <option value="Latvia">Latvia</option>
            <option value="Lebanon">Lebanon</option>
            <option value="Lesotho">Lesotho</option>
            <option value="Liberia">Liberia</option>
            <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
            <option value="Liechtenstein">Liechtenstein</option>
            <option value="Lithuania">Lithuania</option>
            <option value="Luxembourg">Luxembourg</option>
            <option value="Macau">Macau</option>
            <option value="Macedonia">Macedonia, The Former Yugoslav Republic of</option>
            <option value="Madagascar">Madagascar</option>
            <option value="Malawi">Malawi</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Maldives">Maldives</option>
            <option value="Mali">Mali</option>
            <option value="Malta">Malta</option>
            <option value="Marshall Islands">Marshall Islands</option>
            <option value="Martinique">Martinique</option>
            <option value="Mauritania">Mauritania</option>
            <option value="Mauritius">Mauritius</option>
            <option value="Mayotte">Mayotte</option>
            <option value="Mexico">Mexico</option>
            <option value="Micronesia">Micronesia, Federated States of</option>
            <option value="Moldova">Moldova, Republic of</option>
            <option value="Monaco">Monaco</option>
            <option value="Mongolia">Mongolia</option>
            <option value="Montserrat">Montserrat</option>
            <option value="Morocco">Morocco</option>
            <option value="Mozambique">Mozambique</option>
            <option value="Myanmar">Myanmar</option>
            <option value="Namibia">Namibia</option>
            <option value="Nauru">Nauru</option>
            <option value="Nepal">Nepal</option>
            <option value="Netherlands">Netherlands</option>
            <option value="Netherlands Antilles">Netherlands Antilles</option>
            <option value="New Caledonia">New Caledonia</option>
            <option value="New Zealand">New Zealand</option>
            <option value="Nicaragua">Nicaragua</option>
            <option value="Niger">Niger</option>
            <option value="Nigeria">Nigeria</option>
            <option value="Niue">Niue</option>
            <option value="Norfolk Island">Norfolk Island</option>
            <option value="Northern Mariana Islands">Northern Mariana Islands</option>
            <option value="Norway">Norway</option>
            <option value="Oman">Oman</option>
            <option value="Pakistan">Pakistan</option>
            <option value="Palau">Palau</option>
            <option value="Panama">Panama</option>
            <option value="Papua New Guinea">Papua New Guinea</option>
            <option value="Paraguay">Paraguay</option>
            <option value="Peru">Peru</option>
            <option value="Philippines">Philippines</option>
            <option value="Pitcairn">Pitcairn</option>
            <option value="Poland">Poland</option>
            <option value="Portugal">Portugal</option>
            <option value="Puerto Rico">Puerto Rico</option>
            <option value="Qatar">Qatar</option>
            <option value="Reunion">Reunion</option>
            <option value="Romania">Romania</option>
            <option value="Russia">Russian Federation</option>
            <option value="Rwanda">Rwanda</option>
            <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
            <option value="Saint LUCIA">Saint LUCIA</option>
            <option value="Saint Vincent">Saint Vincent and the Grenadines</option>
            <option value="Samoa">Samoa</option>
            <option value="San Marino">San Marino</option>
            <option value="Sao Tome and Principe">Sao Tome and Principe</option>
            <option value="Saudi Arabia">Saudi Arabia</option>
            <option value="Senegal">Senegal</option>
            <option value="Seychelles">Seychelles</option>
            <option value="Sierra">Sierra Leone</option>
            <option value="Singapore">Singapore</option>
            <option value="Slovakia">Slovakia (Slovak Republic)</option>
            <option value="Slovenia">Slovenia</option>
            <option value="Solomon Islands">Solomon Islands</option>
            <option value="Somalia">Somalia</option>
            <option value="South Africa">South Africa</option>
            <option value="South Georgia">South Georgia and the South Sandwich Islands</option>
            <option value="Span">Spain</option>
            <option value="SriLanka">Sri Lanka</option>
            <option value="St. Helena">St. Helena</option>
            <option value="St. Pierre and Miguelon">St. Pierre and Miquelon</option>
            <option value="Sudan">Sudan</option>
            <option value="Suriname">Suriname</option>
            <option value="Svalbard">Svalbard and Jan Mayen Islands</option>
            <option value="Swaziland">Swaziland</option>
            <option value="Sweden">Sweden</option>
            <option value="Switzerland">Switzerland</option>
            <option value="Syria">Syrian Arab Republic</option>
            <option value="Taiwan">Taiwan, Province of China</option>
            <option value="Tajikistan">Tajikistan</option>
            <option value="Tanzania">Tanzania, United Republic of</option>
            <option value="Thailand">Thailand</option>
            <option value="Togo">Togo</option>
            <option value="Tokelau">Tokelau</option>
            <option value="Tonga">Tonga</option>
            <option value="Trinidad and Tobago">Trinidad and Tobago</option>
            <option value="Tunisia">Tunisia</option>
            <option value="Turkey">Turkey</option>
            <option value="Turkmenistan">Turkmenistan</option>
            <option value="Turks and Caicos">Turks and Caicos Islands</option>
            <option value="Tuvalu">Tuvalu</option>
            <option value="Uganda">Uganda</option>
            <option value="Ukraine">Ukraine</option>
            <option value="United Arab Emirates">United Arab Emirates</option>
            <option value="United Kingdom" selected>United Kingdom</option>
            <option value="United States">United States</option>
            <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
            <option value="Uruguay">Uruguay</option>
            <option value="Uzbekistan">Uzbekistan</option>
            <option value="Vanuatu">Vanuatu</option>
            <option value="Venezuela">Venezuela</option>
            <option value="Vietnam">Viet Nam</option>
            <option value="Virgin Islands (British)">Virgin Islands (British)</option>
            <option value="Virgin Islands (U.S)">Virgin Islands (U.S.)</option>
            <option value="Wallis and Futana Islands">Wallis and Futuna Islands</option>
            <option value="Western Sahara">Western Sahara</option>
            <option value="Yemen">Yemen</option>
            <option value="Yugoslavia">Yugoslavia</option>
            <option value="Zambia">Zambia</option>
            <option value="Zimbabwe">Zimbabwe</option>
        </select>    
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Post Code</label>
                                                <div class="col-md-9">
                                                    <input type="text" placeholder="Company Postcode" class="form-control" required="required" name="Company[postcode]">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>

                                </div>
                                
                                <!-- Tab 4 -->
                                <div class="tab-pane" id="tab4">
                                    <h3>FINISH</h3>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="checker"><span class="checked"><input type="checkbox" id="chBox" name="tnc" checked onchange="enable()"></span></div> I agree to the <a class="" data-toggle="modal" href="#terms">Terms of Service</a>
                                                
                                        </div>
                                    </div>
                                </div>
                                <!-- Tab 4 End -->
    
    
</div> <!-- tab-content end -->
                        </div> <!-- form-body ends -->
                </div> <!-- form-wizard ends -->
            
<script type="text/javascript">
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
            function enable()
        {
            if($("#chBox").is(":checked"))
            {
                $("#chBox").closest("span").addClass("checked");
                $("#Submit").attr("disabled",false);
            }
            else
            {
                if($("#chBox").closest("span").hasClass("checked"))
                    $("#chBox").closest("span").removeClass("checked");
                $("#Submit").attr("disabled",true);
            }
        }
</script>

<div class="form-actions">
                        <input type="hidden" name="currTab" id="currTab" value="1"/>
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <a href="javascript:setActions('back');"  class="btn default button-previous" style="display:none;" >
                                    <i class="m-icon-swapleft"></i> Back </a>
                                <input type="button" id="compSubmit" onclick="setActions('continue');" value="Continue" class="btn blue button-next"/>
                                <input type="submit" id="Submit" value="Submit" style="display: none;" class="btn blue button-submit"/>
                             </div>
                        </div>
                    </div>
    <?php $this->endWidget(); ?>
</div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-hover-dropdown/2.0.10/bootstrap-hover-dropdown.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.2/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/js/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap-wizard/1.2/jquery.bootstrap.wizard.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/3.5.1/select2.min.js"></script>
<script>
   function setActions(action)
    {
       if($("#currTab").val() == "1"){
           if(action==='continue')
           {
                $(".button-previous").show();
                $("#currTab").val("2");
            }
            else
            {
                $(".button-previous").hide();
                $("#currTab").val("1");
            }
           return false;
       }
       else if($("#currTab").val()=="2")
       {
           if(action==='continue')
           {
                $("#currTab").val("3");
                $("#Submit").show();
            }
            else
            {
               $(".button-previous").hide();
                $("#currTab").val("1");
            }
            return false;
       }
       else
       {
           if(action==='back')
           {
               $("#currTab").val("2");
               return false;
           }
           else{
               $('#submit_form').submit();
                return true;
            }
        }
    }
</script>