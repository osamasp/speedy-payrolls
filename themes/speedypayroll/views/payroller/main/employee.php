<?php
/* @var $this MainController */
/* @var $model Timesheet */

$this->breadcrumbs = array(
    'Timesheets' => array('index'),
);
?>

<div class="portlet box green ">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i> Payroller Employees
        </div>
        <div class="tools">
            <a href="" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body">
<div class="box-header">
    <h3 class="box-title">Payroller Employees</h3>
</div>
<div class="box-body">
    <form id="entryForm" method="post" >
        <input type="hidden" name='action'  id='action' value="merge" />
        <div class="box-body table-responsive">
        <table class="table table-striped table-bordered table-hover dataTable" id="example1" >

            <thead>
                <tr>
                    <th><input type="checkbox" name="check" clas="form-control" /></th>
                    <th rowspan="1" colspan="1">Name</th>
                    <th rowspan="1" colspan="1">ID</th>
                    <th rowspan="1" colspan="1">Employment Type</th>
                    <th rowspan="1" colspan="1">Date Joined</th>
                    <th rowspan="1" colspan="1">Date Ending</th>
                    <th rowspan="1" colspan="1">Location</th>
                    <th rowspan="1" colspan="1">Supervisor</th>
                    <th rowspan="1" colspan="1">Action</th>
                </tr>
            </thead>
           
            <?php if (count($entries) > 0) { ?>
                <tbody role="alert" aria-live="polite" aria-relevant="all">
                    <?php
                    foreach ($entries as $entry) {
                        
                        ?>
                        <tr class="gradeX">
                            <td><input type="checkbox" name="check" clas="form-control" /></td>
                            <td class=" "><?php echo $entry->user->full_name; ?></td>
                            <td class=""><?php echo AppInterface::formatID($entry->user->id); ?></td>
                            <td class=""><?php echo AppContract::formatEmployeType($entry->employee_type); ?></td>
                            <td class=""><?php echo date(AppInterface::getdateformat(),$entry->start_time); ?></td>
                            <td class=""><?php echo ($entry->end_time!=-1)?date(AppInterface::getdateformat(),$entry->end_time):'-'; ?></td>
                            <td class=""><?php echo $entry->company ? $entry->company->country : '-' ; ?></td>
                            <td class=""><?php echo $entry->approver ? $entry->approver->full_name : '-' ; ?></td>
                            <td class=" "><a class="label label-sm label-success" href="<?php echo Yii::app()->createUrl('/payroller/main/timesheets',array('user'=>$entry->user->id)); ?>">View Timesheets</a></td>
                            
                        </tr>
                    <?php } ?>
                </tbody>
            <?php } else { ?>
                <tbody>
                    <tr>
                        <td colspan="7">No Entry Found</td>

                    </tr>
                </tbody>
            <?php } ?>

        </table>
</div>
    </form>
</div>

<br/>
<div class="childEntryContainer" id="childEntryContainer" style="display:none">

</div>
    </div>
</div>