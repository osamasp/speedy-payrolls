<?php
/* @var $this MainController */
/* @var $model Timesheet */

$this->breadcrumbs = array(
    'Timesheets' => array('index'),
);
?>
<!-- data table starts -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/payroller-main.js"></script>
<input type="hidden" id="url" value="<?php echo $this->createUrl("/timesheet/main/GetSubEntries");?>"/>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box grey-cascade">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-globe"></i>TImesheets
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>

                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">

                        <div class="col-md-12">

                        </div>
                    </div>
                </div>
                <form id="entryForm" method="post" class="form-horizontal" >
                    <input type="hidden" name='action'  id='action' value="merge" />
                    <table class="table table-responsive table-hover" aria-describedby="example2_info">

                        <thead>
                            <tr>
                                <th rowspan="1" colspan="1">Contract</th>
                                <th rowspan="1" colspan="1">Employee</th>
                                <th rowspan="1" colspan="1">Approver</th>
                                <th rowspan="1" colspan="1">Period</th>
                                <th rowspan="1" colspan="1">Status</th>
                                <th rowspan="1" colspan="1">PaySlip</th>
                                <th rowspan="1" colspan="1">Action</th>
                                <th rowspan="1" colspan="1"></th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th rowspan="1" colspan="1">Contract</th>
                                <th rowspan="1" colspan="1">Employee</th>
                                <th rowspan="1" colspan="1">Approver</th>
                                <th rowspan="1" colspan="1">Period</th>
                                <th rowspan="1" colspan="1">Status</th>
                                <th rowspan="1" colspan="1">PaySlip</th>

                                <th rowspan="1" colspan="2" align="right">

                                    <?php if (AppUser::isUserAdmin()) { ?>

                                        <input type="submit" class="btn btn-primary changeAction" action="merge" value="Merge" />
                                        <input type="submit" class="btn btn-primary changeAction" action="dispatch" value="Copy to Payrolls Dispatch" />
                                    <?php } ?>
                                    </div>
                                </th>

                            </tr>
                        </tfoot>
                        <?php if (count($entries) > 0) { ?>
                            <tbody role="alert" aria-live="polite" aria-relevant="all">
                                <?php
                                foreach ($entries as $entry) {
                                    $paySlipStatus = AppPayslip::isIssued($entry);
                                    ?>
                                    <tr class="odd">
                                        <td class=""><?php echo AppContract::getContractCustomName($entry->contract) . AppTimeSheet::printMergeLogName($entry); ?></td>
                                        <td class=" "><?php echo $entry->contract->user->full_name; ?></td>
                                        <td class=" "><?php echo $entry->contract->approver->full_name; ?></td>
                                        <td class=" ">
                                            <?php echo AppTimeSheet::formatSheetPeriod($entry); ?>
                                        </td>
                                        <td class=" "><?php echo $entry->status; ?></td>
                                        <td class=" "><?php echo ($paySlipStatus) ? 'issued' : 'N/A'; ?></td>
                                        <td class=" ">
                                            <div class="btn-group">
                                                <a class="btn btn-primary" style="padding-top: 0px;padding-bottom: 0px;" data-toggle="modal" data-target=".ajaxModel" data-url="<?php echo Yii::app()->createUrl('/timesheet/main/GetSheetCard'); ?>" data-whatever="<?php echo $entry->id; ?>" >View</a>
                                                <button type="button" class="btn btn-primary dropdown-toggle" style="padding-top: 0px;padding-bottom: 0px;" data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <?php if (( $entry->type != "daily" && ( AppUser::isUserAdmin() || $entry->status == "pending"))) { ?>
                                                        <li><a href="<?php echo Yii::app()->createUrl("timesheet/main/edit", array("entry" => $entry->id)); ?>">Update</a></li>
                                                    <?php } ?>
                                                    <?php if (AppUser::isUserAdmin()) { ?>
                                                        <li><a href="<?php echo Yii::app()->createUrl("invoice/main/sendinvoice", array("timesheet" => $entry->id)); ?>">Invoice</a></li>
                                                    <?php } ?>
                                                    <?php if ($entry->type == "daily") { ?>
                                                        <li class="divider"></li>
                                                        <li><a href="#childEntryContainer" class="<?php echo ($entry->type == "daily") ? "wklyLnkPrnt" : ""; ?>" tSheetId="<?php echo $entry->id; ?>">Show TimeCard</a></li>
                                                    <?php } ?>
                                                    <?php if ($entry->status == "approved" && $entry->dispatch_flag == 0) { ?>
                                                        <li class="divider"></li>
                                                        <li><a href="<?php echo Yii::app()->createUrl("timesheet/main/DispatchCopy", array("entry" => $entry->id)); ?>" >Copy to Payroll Folder</a></li>
                                                    <?php } ?>
                                                </ul>
                                            </div>

                                        </td>
                                        <td class="">
                                            <input type="checkbox" name="entry[]" value="<?php echo $entry->id; ?>" clas="form-control" />
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        <?php } else { ?>
                            <tbody>
                                <tr>
                                    <td colspan="7">No Entry Found</td>

                                </tr>
                            </tbody>
                        <?php } ?>

                    </table>
                    <?php
                    $this->widget('CLinkPager', array(
                        'pages' => $pages,
                        'header' => '',
                        'footer' => '',
                        'maxButtonCount'=>3,
                        'nextPageLabel' => 'Next',
                        'prevPageLabel' => 'Prev',
                        'selectedPageCssClass' => 'active',
                        'hiddenPageCssClass' => 'disabled',
                        'htmlOptions' => array(
                            'class'=>'pagination pull-right',
                        )
                    ));

                    ?>
                </form>
                <?php if (AppUser::isUserAdmin()) { ?> 
                    <br/>
                    <button type="button" class="btn btn-danger link-btn" confirm="false" href-link="<?php echo Yii::app()->createUrl('/payroller/main/DispatchPayroll'); ?>">Payroll Dispatch Folder</button>
                <?php } ?>

                <div class="childEntryContainer" id="childEntryContainer" style="display:none">

                </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>      <!-- /data table ends -->
