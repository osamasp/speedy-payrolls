<?php
/* @var $this MainController */
/* @var $model Timesheet */

$this->breadcrumbs = array(
    'Timesheets' => array('index'),
);
?>

<div class="portlet box green ">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i> Payroller Companies
        </div>
        <div class="tools">
            <a href="" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body">
<div class="box-header">
    <h3 class="box-title">Payroller Companies</h3>
</div>
<div class="box-body table-responsive">
    <form id="entryForm" method="post" class="form-horizontal">
        <input type="hidden" name='action'  id='action' value="merge" />
        <div class="box-body table-responsive">
        <table class="table table-striped table-bordered table-hover dataTable" id="example1">

            <thead>
                <tr>
                    <th><input type="checkbox" name="check" clas="form-control" /></th>
                    <th rowspan="1" colspan="1">Company Name</th>
                    <th rowspan="1" colspan="1">Location</th>
                    <th rowspan="1" colspan="1">Number of Staff/Contractors</th>
                    <!--<th rowspan="1" colspan="1">Status</th>-->
                    <th rowspan="1" colspan="1">Action</th>
                </tr>
            </thead>
            <?php if (count($entries) > 0) { ?>
                <tbody role="alert" aria-live="polite" aria-relevant="all">
                    <?php
                    foreach ($entries as $entry) {
                        
                        ?>
                        <tr class="gradeX">
                            <td><input type="checkbox" name="check" clas="form-control" /></td>
                            <td class=" "><?php echo $entry->name; ?></td>
                            <td class=""><?php echo ucwords($entry->country); ?></td>
                            <td class=""><?php echo AppCompany::getCompanyInteralUserCount($entry->id); ?></td>
                            <!--<td class=""><?php // echo ($entry->status==0)?'pending':'status' ; ?></td>-->
                            <td class="">
                                <div class="btn-group pull-right">
                                        <button class="btn green dropdown-toggle" data-toggle="dropdown">Actions <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-right">
                                        <li><a href="<?php echo Yii::app()->createUrl('/company/main/view/',array('id'=>$entry->id)); ?>">View Details</a> </li>
                                        <li class="divider"></li>
                                        <li><a href="<?php echo Yii::app()->createUrl('/payroller/main/employee/',array('company'=>$entry->id)); ?>">View Employees</a> </li>
                                       
                                    </ul>
                                </div>
                                
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            <?php } else { ?>
                <tbody>
                    <tr>
                        <td colspan="7">No Entry Found</td>

                    </tr>
                </tbody>
            <?php } ?>

        </table>
        </div>
    </form>
</div>

<br/>
<div class="childEntryContainer" id="childEntryContainer" style="display:none">

</div>
</div>
</div>