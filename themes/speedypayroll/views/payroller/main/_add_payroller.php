<?php
/* @var $this EmployeeController */

$this->breadcrumbs = array(
    'Employee',
);
?>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/payroller-main-addpayroller.js" type="text/javascript"></script>
<input type="hidden" id="date_format" value="<?php echo AppInterface::getdateformat(true); ?>"/>
<div class="box-header">
    <h3 class="box-title">Complete your company profile.</h3>
</div>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'payroller-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => false,
        ));
?>

<div class="box-body">

    <div class="col-sm-12 form-group">
        <div class="col-sm-4">
            Payroller First Name
            <input type="text" name="Payroller[first_name]" class="form-control"placeholder="First Name" required="required">
        </div>
        <div class="col-sm-1">
        </div>
        <div class="col-sm-4">
            Payroller Email
            <input type="text" name="Payroller[email]" class="form-control" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="E.g:example@abc.com" placeholder="Email" required="required">
       
        </div>

    </div>
    <div class="col-sm-12 form-group">
        <div class="col-sm-4">
            Payroller Last Name
            <input type="text" name="Payroller[last_name]" class="form-control"placeholder="Last Name" required="required">
        </div>
        <div class="col-sm-1">
        </div>
        <div class="col-sm-4">
           Company Email
            <input type="text" name="Payroller[company_email]" class="form-control" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="E.g:example@abc.com" placeholder="Email" required="required">
        </div>

    </div>
    <div class="col-sm-12 form-group">
        <div class="col-sm-4">
            Company Name
            <input type="text" name="Payroller[company_name]" class="form-control"placeholder="Company Name" required="required">
        </div>
        <div class="col-sm-1">
        </div>
        <div class="col-sm-4">
            Payroller Company Number
            <input type="text" name="Payroller[company_number]" onkeypress="return isNumberKey(event);" class="form-control" placeholder="Company Number" required="required">
        </div>

    </div>

    <div class="col-sm-12 form-group">
        <div class="col-sm-4">
            Company Address
            <input type="text" name="Payroller[address]" class="form-control" placeholder="Address" required="required">
        </div>
        <div class="col-sm-1">
        </div>
        <div class="col-sm-4">
            VAT Number
            <input type="text" name="Payroller[vat_number]" onkeypress="return isNumberKey(event);" class="form-control" placeholder="VAT Number" required="required">
      </div>
    </div>

    <div class="col-sm-12 form-group">
        <div class="col-sm-4">
           <input type="text" name="Payroller[street]" class="form-control" placeholder="Street">
        </div>
        <div class="col-sm-1">
        </div>
      <div class="col-sm-4">
          </div>

    </div>
    
    <div class="col-sm-12 form-group">
        <div class="col-sm-4">
           <input type="text" name="Payroller[country]" class="form-control" placeholder="Country">
        </div>
        <div class="col-sm-1">
        </div>
      <div class="col-sm-4">
          </div>

    </div>
    
    <div class="col-sm-12 form-group">
        <div class="col-sm-6">
        <div class="col-sm-3">
           <input type="text" name="Payroller[city]" class="form-control" placeholder="City">
        </div>
            <div class="col-sm-3">
           <input type="text" name="Payroller[postcode]" class="form-control" placeholder="Postcode">
        </div>
        </div>
        <div class="col-sm-1">
        </div>
      <div class="col-sm-4">
          </div>

    </div>
    
    <div class="col-sm-12 form-group">
        <div class="col-sm-4">
          Company Telephone <input type="text" name="Payroller[country]" class="form-control" placeholder="Phone">
        </div>
        <div class="col-sm-1">
        </div>
      <div class="col-sm-4">
          </div>

    </div>
    
    <div class="col-sm-12 form-group">
        <div class="col-sm-4">
            Company Sector<select>
                <option value="0">Options</option>                   
            </select> 
        </div>
        <div class="col-sm-1">
        </div>
      <div class="col-sm-4">
          </div>

    </div>
    
    <div class="clearfix"></div>
</div>
<div class="box-footer">
    <input type="submit" class="btn btn-primary" name="submit" value="Done">
</div>

<?php $this->endWidget(); ?>

