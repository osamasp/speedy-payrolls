<?php
/* @var $this MainController */
/* @var $model User */
$this->breadcrumbs=array(
	'Payroller Permissions',
);

?>
<div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-check-square-o"></i>Payroller Permission
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>

                </div>
            </div>
            <div class="portlet-body form">
                <div class="table-toolbar">
                    <div class="row">

                        <div class="col-md-12">
                            
                        </div>
                    </div>
                </div>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'login-form',
    'enableClientValidation' => true,
    'htmlOptions' => array(
        'class' => 'form-horizontal form-bordered',
    )
        ));
?>
<div class="box-header" style="padding-left:20px;">
    <h3 class="box-title">Allow Bulk Payslip Upload</h3>
</div>
<div class="table-responsive">
    <div class="box-body">
        <select id="payroller" name="Payroller" class="form-control" style="width: 30%;margin-left:30px;">
             <option value="0">Select Payroller</option>
            <?php foreach ($payrollers as $payroller ) {
            $user = User::model()->findByPk($payroller->payroller_id); ?>
            <option value ="<?php echo $payroller->id; ?>"><?php echo Payroller::model()->findByAttributes(array('payroller_id'=> $payroller->id))->company_name; ?></option>
            <?php } ?>
        </select><br>
         <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-3 col-md-9">
    <?php echo CHtml::submitButton('Done', array('class' => 'btn btn-primary btn-file')); ?>
</div>
        </div>
         </div>
</div>
</div> 
<?php $this->endWidget(); ?>
            </div>
</div>