<?php
/* @var $this MainController */
/* @var $model User */

$this->breadcrumbs = array(
    'Users' => array('index'),
);
?>
<div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-globe"></i>Company Profile
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
<div class="box-header">
    <h3 class="box-title"><?php echo ucwords($model->full_name); ?></h3>
     <a class="btn btn-primary pull-right" style="margin-right: 10px;margin-left:10px;" href="<?php echo Yii::app()->createUrl('payroller/main/editprofile'); ?>">Edit Profile</a> 
     <a class="btn btn-primary pull-right" href="<?php echo Yii::app()->createUrl('user/main/changepassword'); ?>">Change Password</a>
</div>
<div class="table-responsive">
    <div class="box-body">
        <?php
        $this->widget('zii.widgets.CDetailView', array(
            'data' => $model,
            'attributes' => array(
                'first_name',
                'last_name',
                'email',
                array('name'=>'Company Name' , 'value'=>$payroller ? $payroller->company_name : '-') ,
                array('name'=>'Phone Number' , 'value'=>$payroller ? $payroller->company_phone : '-') ,
                array('name'=>'Address' , 'value'=>$payroller ? $payroller->company_address : '-') ,
                array('name'=>'Street' , 'value'=>$payroller ? $payroller->company_street : '-') ,
                array('name'=>'City' , 'value'=>$payroller ? $payroller->company_city : '-') ,
                array('name'=>'Country' , 'value'=>$payroller ? $payroller->company_country : '-') ,
            ),
        ));
        ?>
    </div>
</div>
<div class="box-footer">
</div>
                    </div>
</div>