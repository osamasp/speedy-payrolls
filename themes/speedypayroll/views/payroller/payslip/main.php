<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!--row-->	
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat blue">
            <div class="visual">
                <i class="fa fa-comments"></i>
            </div>
            <div class="details">
                <div class="number">
                    ePayslips Inbox <span class="badge badge-danger"> 1 new</span>
                </div>
                <div class="desc">
                    see issued payslips
                </div>
            </div>
            <a class="more" href="<?php echo Yii::app()->createUrl('/payroller/main/timesheetinbox');?>">
                Click to enter<i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat green">
            <div class="visual">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <div class="details">
                <div class="number">
                    ePayslip Requests
                </div>
                <div class="desc">
                    create new payslip requests
                </div>
            </div>
            <a class="more" href="<?php echo Yii::app()->createUrl('/payroller/main/adminemployee');?>">
                Click to enter<i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat red">
            <div class="visual">
                <i class="fa fa-globe"></i>
            </div>
            <div class="details">
                <div class="number">
                    Pending ePayslips
                </div>
                <div class="desc">
                    see the requests you made
                </div>
            </div>
            <a class="more" href="<?php echo Yii::app()->createUrl('/payroller/main/timesheets');?>">
                Click to enter<i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>

</div>