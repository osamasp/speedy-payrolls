<?php
/* @var $this ApproveController */
/* @var $model Timesheet */

$this->breadcrumbs = array(
    'Approve Timesheets' => array('index'),
);
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/payslip-main.js"></script>
<input type="hidden" id="url" value="<?php echo $this->createUrl("/timesheet/main/GetSubEntries");?>"/>

<div class="portlet box green ">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i> Payslip Requests
        </div>
        <div class="tools">
            <a href="" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body">

<div class="box-header">
    <h3 class="box-title">PaySlip Requests</h3>
</div>
<div class="box-body">
    <table class="table table-responsive table-hover table-bordered" aria-describedby="example2_info" id="example1">

        <thead>
            <tr>
                <th rowspan="1" colspan="1">Payroll ID</th>
                <th rowspan="1" colspan="1">Contractor/Staff</th>
                <th rowspan="1" colspan="1">Company</th>
                <th rowspan="1" colspan="1">Supervisor</th>
                <th rowspan="1" colspan="1">Period</th>

                <th rowspan="1" colspan="1">Status</th>
                <th rowspan="1" colspan="1">Payslip</th>
                <th rowspan="1" colspan="1">Actions</th>
                <th rowspan="1" colspan="1">Submission Date</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th rowspan="1" colspan="1">Payroll ID</th>
                <th rowspan="1" colspan="1">Contractor/Staff</th>
                <th rowspan="1" colspan="1">Company</th>
                <th rowspan="1" colspan="1">Supervisor</th>
                <th rowspan="1" colspan="1">Period</th>
                <th rowspan="1" colspan="1">Status</th>
                <th rowspan="1" colspan="1">Payslip</th>
                <th rowspan="1" colspan="1">Actions</th>
                <th rowspan="1" colspan="1">Submission Date</th>
            </tr>
        </tfoot>
        <?php if (count($entries) > 0) { ?>
            <tbody role="alert" aria-live="polite" aria-relevant="all">
                <?php
                foreach ($entries as $request) {

                    $entry = $request->timesheet;
                    $paySlipStatus = AppPayslip::isIssued($request);
                    $type='';
                    if($paySlipStatus)
                    {
                        $sEntry = $request->payslipEntries[0];
                        $type = $sEntry->type;
                        
                    }
                    ?>
                    <tr class="odd">
                        <td class=" "><?php echo $request->id; ?></td>
                        <td class=" "><?php echo $entry->contract->user->full_name; ?></td>
                        <td class=""><?php echo $entry->contract->company->name; ?></td>
                        <td class=" "><?php echo $entry->contract->approver->full_name; ?></td>
                        <td class=" ">
                            <?php echo AppTimeSheet::formatSheetPeriod($entry); ?></td>


                        <td class=" "><?php echo $request->status; ?></td>
                        <td clas="">
                            <?php echo ($paySlipStatus) ? 'issued' : 'N/A'; ?>


                        </td>
                        <td class="">
                            <div class="btn-group pull-right">
                                        <button class="btn green dropdown-toggle" data-toggle="dropdown">Actions <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-right">
                                    <?php if ($paySlipStatus == true && $type=='csv') { ?>
                                        <li><a data-toggle="modal" data-target=".ajaxModel" data-url="<?php echo Yii::app()->createUrl("/payroller/payslip/showInTemplate"); ?>" data-whatever="<?php echo $request->id; ?>" >View Issued Payslip</a></li>
                                        <li class="divider"></li>
                                    <?php }elseif($paySlipStatus==true && $type=="other"){ ?>
                                        <li><a href="<?php echo Yii::app()->createUrl('/payroller/payslip/download/',array('id'=>$sEntry->id)); ?>" >Download PaySlip</a></li>
                                        <li class="divider"></li>
                                    <?php } ?>

                                    <li><a data-toggle="modal" data-target=".ajaxModel" data-url="<?php echo Yii::app()->createUrl("/timesheet/main/GetSheetCard"); ?>" data-whatever="<?php echo $entry->id; ?>" >View Summary</a></li>
                                    <li class="divider"></li>
                                    <li><a href="<?php echo Yii::app()->createUrl("payroller/payslip/issue", array("id" => $request->id)); ?>">Update</a></li>

                                    <?php if ($entry->type == "daily") { ?>
                                        <li class="divider"></li>
                                        <li><a href="#childEntryContainer" class="<?php echo ($entry->type == "daily") ? "wklyLnkPrnt" : ""; ?>" tSheetId="<?php echo $entry->id; ?>">Show TimeCard</a></li>
                                    <?php } ?>
                                    <?php if ($paySlipStatus == false) { ?>
                                        <li class="divider"></li>
                                        <li><a href="<?php echo Yii::app()->createUrl('payroller/payslip/issue', array("id" => $request->id)); ?>" >Issue Payslip</a></li>
                                    <?php } ?>
                                </ul>
                            </div>


                        </td>

                        <td class=" "><?php echo date(AppInterface::getdateformat(), $request->created_at); ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        <?php } else { ?>

            <tbody>
                <tr>
                    <td colspan="7">No Entry Found</td>

                </tr>
            </tbody>
        <?php } ?>

    </table>
    <?php
                    $this->widget('CLinkPager', array(
                        'pages' => $pages,
                        'header' => '',
                        'footer' => '',
                        'maxButtonCount'=>3,
                        'nextPageLabel' => 'Next',
                        'prevPageLabel' => 'Prev',
                        'selectedPageCssClass' => 'active',
                        'hiddenPageCssClass' => 'disabled',
                        'htmlOptions' => array(
                            'class'=>'pagination pull-right',
                        )
                    ));

    ?>
</div>

<br/>
<div class="childEntryContainer" style="display:none">

</div>
</div>
</div>