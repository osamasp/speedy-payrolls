<?php ?>
<div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-globe"></i>Payslips
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>

                </div>
            </div>
            <div class="portlet-body form">
                <div class="table-toolbar">
                    <div class="row">

                        <div class="col-md-12">
                            
                        </div>
                    </div>
                </div>
<div class="box-header" style="padding-left:20px;">
    <h2 class="box-title">Import Payslips</h2>
</div>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'payslip-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => false,
    'htmlOptions' => array('enctype' => 'multipart/form-data')
        ));
?>

<div class="box-body" style="padding-left:30px;">
    <div class="row">
        <div class="col-md-12">
            Upload File
            <?php echo CHtml::fileField('csvUpload'); ?>
        </div>

    </div>
    <br/>
    <div class="row">
        <div class="col-md-11">Accepted Formats: CSV ( Comma Separated Values)</div>

    </div>
    <p></p>
    <div class="row">
        <div class="col-md-11"><a class="btn btn-primary" href="<?php echo Yii::app()->createUrl('/payroller/payslip/formatDownload'); ?>">Click here</a> to download the csv format, required for bulk upload.</div>
    </div>
    <br>
</div>
<div class="form-actions">
        <div class="row">
            <div class="col-md-offset-3 col-md-9">
    <?php echo CHtml::submitButton('Import & Issue Payslips', array('class' => 'btn btn-primary')); ?>
    <?php echo CHtml::submitButton('Generate PDF', array('class' => 'btn btn-primary')); ?>
</div>
        </div>
</div>
<?php $this->endWidget(); ?>



                    </div></div>