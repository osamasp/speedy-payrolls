<?php
$entry = $model;
if (AppPayslip::isIssued($model)) {
    $entry = $model->payslipEntries[0];
}
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/payslip-main.js"></script>
<input type="hidden" id="url" value="<?php echo $this->createUrl("/timesheet/main/GetSubEntries");?>"/>
<div class="portlet box green ">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i> Payslip Requests
        </div>
        <div class="tools">
            <a href="" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body">

<div clas="row">
    <div class="col-md-11">
        <h3 style="font-size:28px;margin-bottom:0;">Issue Payslip via Request Archives</h3>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="box box-primary">

            <div class="box-body" style="padding-left:20px;">

                <?php $this->renderPartial('//timesheet/main/tcardDetailFull', $tdetails); ?>
                <div class="row">
                    <div class="col-md-11">
                        <?php if (AppPayslip::isIssued($model) && $entry->type == "other") { ?>
                            <button type="button" class="btn btn-primary link-btn" href-link="<?php echo Yii::app()->createUrl('/payroller/payslip/download', array('id' => $entry->id)); ?>">Download Payslip</button>
                            <button type="button" class="btn btn-primary">Print Payslip</button>
                        <?php } elseif (AppPayslip::isIssued($model) && $entry->type == "csv") { ?>
                            <button type="button" class="btn btn-primary link-btn" data-toggle="modal" data-target=".ajaxModel" data-url="<?php echo Yii::app()->createUrl("/payroller/payslip/showInTemplate"); ?>" data-whatever="<?php echo $model->id; ?>" >View Payslip</button>
                            <button type="button" class="btn btn-primary">Print Payslip</button>
                        <?php } ?>

                    </div>

                </div>
                <p></p>
                <div class="row">
                    <div class="col-md-11"><a class="btn btn-primary" href="<?php echo Yii::app()->createUrl('/payroller/payslip/formatDownload'); ?>">Click here</a> to download the csv format, required for bulk upload.</div>

                </div>

            </div>

        </div>
    </div><!-- content -->


    <div class="col-md-4">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title" style="font-size:22px;">Upload Payslip:</h3>
            </div>

            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'payslip-form',
                // Please note: When you enable ajax validation, make sure the corresponding
                // controller action is handling ajax validation correctly.
                // There is a call to performAjaxValidation() commented in generated controller code.
                // See class documentation of CActiveForm for details on this.
                'enableAjaxValidation' => false,
                'htmlOptions' => array('enctype' => 'multipart/form-data')
            ));
            ?>
            <div class="box-body" style="padding-left:10px;">
                <div class="row">
                    <div class="col-md-4">
                        Upload File
                    </div>
                    <div class="col-md-6">
                        <?php echo CHtml::fileField('otherFile') ?>
                    </div>

                </div>
                <br/>
                <div class="row">
                    <div class="col-md-11">Accepted Formats: PDF, Excel WorkSheet</div>

                </div>
                <div class="row">
                    <div class="col-md-4">
                        <?php echo CHtml::hiddenField('action', 'upload'); ?>
                        <?php echo CHtml::submitButton('Upload & Issue Payslip', array('class' => 'btn btn-primary')); ?>
                    </div>

                </div>
            </div>
            <?php $this->endWidget(); ?>





        </div>
        <?php if (AppPayroller::checkBulkPermission()) { ?>
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title" style="font-size:22px;">Upload CSV:</h3>
                </div>

                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'payslip-form2',
                    // Please note: When you enable ajax validation, make sure the corresponding
                    // controller action is handling ajax validation correctly.
                    // There is a call to performAjaxValidation() commented in generated controller code.
                    // See class documentation of CActiveForm for details on this.
                    'enableAjaxValidation' => false,
                    'htmlOptions' => array('enctype' => 'multipart/form-data')
                ));
                ?>
                <div class="box-body" style="padding-left:10px;">
                    <div class="row">
                        <div class="col-md-4">
                            Upload File
                        </div>
                        <div class="col-md-6">
                            <?php echo CHtml::fileField('csvFile') ?>
                            <?php // echo CHtml::activeFileField($entry, 'file');  ?>
                        </div>

                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-md-11">Accepted Formats: CSV(Comma-Separated Values)</div>

                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <?php echo CHtml::hiddenField('action', 'csvUpload'); ?>
                            <?php echo CHtml::submitButton('Import & Issue Payslip', array('class' => 'btn btn-primary')); ?>
                        </div>

                    </div>
                </div>
                <?php $this->endWidget(); ?>





            </div>

        <?php } ?>
    </div><!-- sidebar -->

</div>
    </div>
</div></div>


