<?php
/* @var $this MainController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Users',
);

$this->menu=array(
array('label'=>'Create User', 'url'=>array('create')),
array('label'=>'Manage User', 'url'=>array('admin')),
);
?>

<div class="box-header">
    <h3 class="box-title">Upload Payslip</h3>
    
</div>

<div class="box-body table-responsive">

    <form action="" method="POST" enctype="multipart/form-data">
        <div class="form-group">
            <input type="file" name="payslip" value=""/>
            <input type="submit" value="Upload"/>
        </div>
        
    </form>

</div>