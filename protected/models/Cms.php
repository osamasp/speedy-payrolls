<?php

/**
 * This is the model class for table "di_cms".
 *
 * The followings are the available columns in table 'di_cms':
 * @property string $id
 * @property string $title
 * @property string $slug
 * @property string $content
 * @property string $type
 * @property string $sort
 * @property string $is_created
 * @property string $is_modified
 * @property string $user_modified
 */
class Cms extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Cms the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'sp_cms';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, slug', 'required'),
            array('title, slug', 'length', 'max' => 255),
            array('type', 'length', 'max' => 12),
            array('sort, user_modified', 'length', 'max' => 30),
            array('content, is_created, is_modified', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, title, slug, content, type, sort, is_created, is_modified, user_modified', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'title' => 'Title',
            'slug' => 'Slug',
            'content' => 'Content',
            'type' => 'Type',
            'sort' => 'Sort',
            'is_created' => 'Is Created',
            'is_modified' => 'Is Modified',
            'user_modified' => 'User Modified',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('slug', $this->slug, true);
        $criteria->compare('content', $this->content, true);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('sort', $this->sort, true);
        $criteria->compare('is_created', $this->is_created, true);
        $criteria->compare('is_modified', $this->is_modified, true);
        $criteria->compare('user_modified', $this->user_modified, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getAllCMS($select = '', $from = '', $whereSql = '', $orderBy = '', $orderDirection = '', $offSet = 0, $size = 10000) {

        $command = Yii::app()->db->createCommand('CALL getAllCMS("' . $select . '","' . $from . '","' . $whereSql . '","' . $orderBy . '","' . $orderDirection . '",' . (int) $offSet . ',' . (int) $size . ')');
        $result = $command->queryAll();
        return $result;
    }

    public function getCMSContentBySlug($slug) {

//            $command = Yii::app()->db->createCommand('CALL getCMSContentBySlug("'.$slug.'" )');
//            $result = $command->queryRow();

        $model = Cms::model()->findByAttributes(array('slug' => $slug));
        return $model->attributes;

//            return $result;  
    }

    public function getCMSContentById($id) {

//            $command = Yii::app()->db->createCommand('CALL getCMSContentById('.$id.' )');
//            $result = $command->queryRow();
//               
//            return $result;
        $model = Cms::model()->findByPk($id);
        return $model->attributes;
    }

}
