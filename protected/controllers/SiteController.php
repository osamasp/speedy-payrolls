<?php

use \ElephantIO\Client,
  \ElephantIO\Engine\SocketIO\Version1X;

class SiteController extends Controller {

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

//    public function actionTestNotify(){
//        
//        include_once Yii::getPathOfAlias('application.modules.notification.extensions') . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, array(
//			'elephant.io',
//			'vendor',
//			'autoload.php'
//		));
//        
//        $host = "www.speedypayrolls.com";
//        $port = "1443";
//        
//       $client = new Client(new ElephantIO\Engine\SocketIO\Version1X(sprintf('https://%s:%s', $host, $port)));
//        
//       $client->initialize();
//       $client->emit('company_message',array('bingo','hello'));
//       $client->close();
//       dd('Push Checking Done...');
//       
//    }
    
    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        $total_hours = 0;
        $last_week = 0;
        $epayslips["payrolls"] = 0;
        $users = 0;
        if(AppUser::isUserAdmin() && !AppUser::isUserSuperAdmin()){
//            dd(date('d/m/Y h:i a', strtotime('last monday')));
        $start_date = DateTime::createFromFormat('d/m/Y h:i a', date('d/m/Y h:i a', strtotime('last monday')));
        $end_date = DateTime::createFromFormat('d/m/Y h:i a', date('d/m/Y h:i a'));
        $start_date = $start_date->getTimestamp();
        $end_date = $end_date->getTimestamp();
        $total_hours = 0;
        $last_week = Yii::app()->db->createCommand("select * from sp_user_timings where user_id=".AppUser::getUserId()." AND (created_at between ".$start_date." AND ".$end_date.")")->queryAll();
        foreach($last_week as $item)
        {
            $total_hours += $item["total_time"];
        }
        $epayslips = AppPayroller::getPayrolls("requested", (AppUser::isUserAdmin() && !isset($_GET['id']))?'':(isset($_GET['id'])?$_GET['id']:AppUser::getUserId()));
//        dd($epayslips);
        $company = AppUser::getUserCompany();
        if ($company != null)
            $users = AppCompany::getCompanyEmployee($company->id);
        }
        $this->render('index',array("total_hours"=>$total_hours,"timesheets"=>$last_week,"epayslips"=>$epayslips["payrolls"],"users"=>$users));
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            $this->layout = "//layouts/blank_head";
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else{
                if(config_settings::$environment == "development"){
                    $this->render('error', $error);
                }
                else{
                    $this->layout = "//layouts/blank_head";
                    if($error["code"] == "404"){
                    $this->render('error-404', $error);}
                    else if($error["code"] == "403"){
                    $this->render('error',$error);}
                    else{
                    $this->render('error-500', $error);}
                }
            }
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact() {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                        "Reply-To: {$model->email}\r\n" .
                        "MIME-Version: 1.0\r\n" .
                        "Content-Type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }
    
    public function actionClientTimezone() {
        $client_timezone = $_GET["time"];
        Yii::app()->session["client_timezone"] = $client_timezone;
        echo Yii::app()->session["client_timezone"];
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {
        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login())
                $this->redirect(Yii::app()->user->returnUrl);
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }

    public function actionSignup() {
        $this->layout = '//layouts/login';
        if (isset($_POST['email']) && isset($_POST['password']) && isset($_POST['account_type'])) {
           if( !$user_email = User::model()->prepareUserForAuthorisation($_POST['email'])) {
               
            Yii::app()->user->setState('signup_email', $_POST['email']);
            Yii::app()->user->setState('signup_password', md5($_POST['password']));
            Yii::app()->user->setState('plan_type',0);
            if(isset($_GET['plan_id']) && $_GET['plan_id'] != ''){
            Yii::app()->user->setState('plan_id',$_GET['plan_id']);            
            } else {
//                free plan id 50231709
            Yii::app()->user->setState('plan_id',664560002);   
            }
            AppLogging::addLog('Sign up', 'success', 'application.controller.site');
            if ($_POST['account_type'] == 'u') { 
                AppLogging::addLog('User Signup', 'success', 'application.user.controller.main');
                $this->redirect(array('/user/main/signup'));
            } else if ($_POST['account_type'] == 'c') {
                AppLogging::addLog('Company Signup', 'success', 'application.company.controller.main');
                $this->redirect(array('/company/main/signup'));
            }
             else if ($_POST['account_type'] == 'p') {
                 AppLogging::addLog('Payroller Signup', 'success', 'application.payroller.controller.main');
                $this->redirect(array('/payroller/main/create'));            }
        }
        else {
            if($_POST['account_type'] == 'u'){
            $error = Yii::app()->user->setFlash('message', "User already registered.");
            } else {
                $error = Yii::app()->user->setFlash('message', "Company already registered.");
            }
            AppLogging::addLog('Signup', 'error', 'application.controller.site');
            $this->redirect(array('/site/signup'));
        }}
        $this->render('signup');
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        AppLogging::addLog('Logout', 'success', 'application.controller.site');
        $this->redirect(Yii::app()->homeUrl);
    }

}
