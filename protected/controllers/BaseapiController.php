<?php

class BaseapiController extends Controller {

    protected $responseCode = HTTPCODE_SUCCESS;
    protected $results = array();

    public function statusCode($status) {
        $codes = Array(
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Rquired',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Invalid Token',
        );
        return (isset($codes[$status])) ? $codes[$status] : '';
    }
    
    public function authenticateUserToken($token){
        $user = User::getUserFromToken($token);
        if($user instanceof User){
            return $user;
        }
        $this->responseCode = HTTPCODE_UNAUTHORIZED;
        $this->results['Message'] = ConstantMessages::$not_valid_token;
        return $user;
    }
    
    public function headersMissing(){
        $this->responseCode = HTTPCODE_FAILURE;
        $this->results['Message'] = ConstantMessages::$headers_data_missing;
        return true;
    }

    public function generateResponse( $content_type = RESPONSE_TYPE_JSON ) {
        
        
         /** This will set the http code in response header **/
        http_response_code($this->responseCode);
        /**/

         switch($content_type){
               case RESPONSE_TYPE_JSON: 
                                        echo CJSON::encode($this->results);
                                        break;
               case RESPONSE_TYPE_XML: 
                                      break;
           }     

// we need to create the body if none is passed
       
    }
    
    public function getHTTPMethod(){
        return Yii::app()->request->getRequestType();
    }
    
    public function getHeadersData($key){
        if(isset($_SERVER['HTTP_'.strtoupper($key)])){
        return $_SERVER['HTTP_'.strtoupper($key)];
        }
        return null;
    }
    
    public function getPutRequestData(){
        parse_str(file_get_contents("php://input"), $put_vars);
        return $put_vars;
    }
    
    public function invalidRequest(){
        $this->responseCode = HTTPCODE_FAILURE;
        $this->results = array('Message'=>'Invalid Request Method'); ///*** Take if from ConstantMessages
     }
    

}

/* 
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

