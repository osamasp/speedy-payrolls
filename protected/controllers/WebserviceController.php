<?php

///*** Create a base class which will extent Controller and WebserviceController extend that base class
///*** Base Class will consist of following items
/**
 * responseCode - Property
 * sendResponse - method
 * statusCode - method
 * invalidReq - method
 * getHTTPMethod - method
 * saveStats - method ( maintain W/S log ) 
 */
class WebserviceController extends BaseapiController {

    public function viewCompanyShift($id, $page, $size) {

        ///*** Add Pagination here
        $model = AppCompany::getUserCompanyShifts($id, FROM_API, $page, $size);
        if (isset($model) && count($model) > 0) {
            $this->responseCode = HTTPCODE_SUCCESS;
            $this->results['Message'] = ConstantMessages::$company_shifts;
        } else {
            $this->responseCode = HTTPCODE_FAILURE;
            $this->results['Message'] = ConstantMessages::$fields_missing;
        }
        $this->results['Result'] = $model;
    }

    public function actionCompanyShift() {
        $method = $this->getHTTPMethod();
        $token = AppInterface::getHeadersData('TOKEN');
        if (!isset($token)) {
            $this->headersMissing();
        } else {
            ///*** Add Header Missing Constraints
            $user = $this->authenticateUserToken($token);
            if ($user instanceof User) {
                ///*** Use Proper Constants also handle if company not found
                if (!AppUser::isUserAdmin($user->id)) {
                    $company = AppUser::getUserCompany($user->id, false);
                    if (isset($company)) {
                        $company_id = $company->id;
                    }
                } else {
                    $company = AppUser::getUserCompany($user->id, true);
                    if (isset($company)) {
                        $company_id = $company->id;
                    }
                }
                ///***

                switch ($method) {
                    case HTTPMETHOD_GET:
                        $this->viewCompanyShift($user->id, Yii::app()->request->getParam('page'), Yii::app()->request->getParam('size'));
                        break;
                    case HTTPMETHOD_POST:
                        $this->createCompanyShift($_POST['CompanyShifts'], $user->id, $company_id);
                        break;
                    case HTTPMETHOD_PUT:
                        $put_vars = AppInterface::getPutRequestData();
                        $this->updateCompanyShift($put_vars['CompanyShifts'], $_GET['id']);
                        break;
                    case HTTPMETHOD_DELETE:
                        $this->deleteCompanyShift($_GET['id']);
                        break;
                    default:
                        $this->invalidRequest();
                        break;
                }
            }
        }
        $this->generateResponse();
    }

    public function updateCompanyShift($formData, $id) {
        if (isset($formData['title']) && isset($formData['start_time']) && isset($formData['end_time'])) {

            $response = AppCompany::updateCompanyShift($formData, $id);
            ///*** Also cater Start Time greater than End Time Error
            if ($response instanceof CompanyShifts) {
                $this->results['Message'] = ConstantMessages::$company_shift_updated;
                $this->responseCode = HTTPCODE_SUCCESS;
            } else if ($response == false) {
                $this->responseCode = HTTPCODE_FAILURE;
                $this->results['Message'] = ConstantMessages::$start_time_greater_end_time;
            } else {
                $this->results['Message'] = ConstantMessages::$cannot_update_shift;
                $this->responseCode = HTTPCODE_FAILURE;
            }
        } else {
            $this->responseCode = HTTPCODE_FAILURE;
            $this->results['Message'] = ConstantMessages::$fields_missing;
        }
        $this->results['Result'] = $response;
    }

    public function deleteCompanyShift($id) {
        $is_assigned = AppCompany::companyShiftAssigned($id);
        if ($is_assigned) {
            $this->results['Message'] = ConstantMessages::$not_delete_shift_user_exist;
            $this->responseCode = HTTPCODE_FAILURE;
        } else {
            $model = AppCompany::deleteCompanyShift($id);
            if ($model instanceof CompanyShifts) {
                $this->results['Message'] = ConstantMessages::$shift_deleted;
                $this->responseCode = HTTPCODE_SUCCESS;
            } else {
                $this->results['Message'] = ConstantMessages::$cannot_delete_shift;
                $this->results['Result'] = $model;
                $this->responseCode = HTTPCODE_FAILURE;
            }
        }
    }

    public function createCompanyShift($formData, $user_id, $company_id) {
        if (isset($formData['title']) && isset($formData['start_time']) && isset($formData['end_time'])) {

            $start_time = DateTime::createFromFormat('h:i a', $formData['start_time'])->getTimestamp();
            $end_time = DateTime::createFromFormat('h:i a', $formData['end_time'])->getTimestamp();
            $title = $formData['title'];
            $exist = AppCompany::getCompanyShiftsByAttributes($start_time, $end_time, $title);
            if (isset($exist)) {
                $this->responseCode = HTTPCODE_FAILURE;
                $this->results['Message'] = ConstantMessages::$shift_already_exist;
            } else {
                $model = AppCompany::addCompanyShift($formData, $user_id, $company_id);
                if ($model instanceof CompanyShifts) {
                    $this->responseCode = HTTPCODE_CREATED;
                    $this->results['Message'] = ConstantMessages::$shift_created;
                } elseif ($model == false) {
                    $this->responseCode = HTTPCODE_FAILURE;
                    $this->results['Message'] = ConstantMessages::$start_time_greater_end_time;
                } else {
                    $this->responseCode = HTTPCODE_FAILURE;
                    $this->results['Message'] = ConstantMessages::$cannot_create_shift;
                }
                $this->results['Result'] = $model;
            }
        } else {
            $this->responseCode = HTTPCODE_FAILURE;
            $this->results['Message'] = ConstantMessages::$fields_missing;
        }
    }

    public function listTimesheet($type = '', $id, $contract_id, $page = 0, $pageSize = 10) {
        $contract = AppContract::getContract($contract_id);
        $args = array('user' => $id, 'contract_id' => $contract_id, 'page' => $page, 'size' => $pageSize);

        if (isset($contract)) {

            if ($type == TIMESHEET_TYPE_CURRENT) {
                $args['status'] = 'pending';
                if ($contract->type == TIMESHEET_TYPE_OUT) {
                    $model = AppTimeSheet::getTimesheetOfTwoWayContract($args, FROM_API);
                    $msg = TWO_WAY;
                } else if ($contract->type == TIMESHEET_TYPE_IN_OUT || $contract->type == TIMESHEET_TYPE_OUTB) {
                    $model = AppTimeSheet::getTimesheetOfThreeWayContract($args, FROM_API);
                    $msg = THREE_WAY;
                } else if ($contract->type == TIMESHEET_TYPE_IN) {
                    $args['contractType'] = TIMESHEET_TYPE_INTERNAL;
                    $company = AppUser::getUserCompany($id);
                    if (isset($company)) {
                        $args['company'] = $company->id;
                    }
                    $model = AppTimeSheet::getTimeSheets($args, FROM_API);
                    $msg = NON_BILLABLE;
                }
                $this->responseCode = HTTPCODE_SUCCESS;
                $this->results['Message'] = sprintf("%s" . ConstantMessages::$current_timesheets, $msg);
                $this->results['Result'] = $model;
            } elseif ($type == TIMESHEET_TYPE_COMPLETED) {
                $args['status'] = array("approved", "in-approval", "closed", "payrolled", "rejected");
                if ($contract->type == TIMESHEET_TYPE_OUT) {
                    $model = AppTimeSheet::getTimesheetOfTwoWayContract($args, FROM_API);
                    $msg = TWO_WAY;
                } else if ($contract->type == TIMESHEET_TYPE_IN_OUT || $contract->type == TIMESHEET_TYPE_OUTB) {
                    $model = AppTimeSheet::getTimesheetOfThreeWayContract($args, FROM_API);
                    $msg = THREE_WAY;
                } else if ($contract->type == TIMESHEET_TYPE_IN) {
                    $args['contractType'] = TIMESHEET_TYPE_INTERNAL;
                    $company = AppUser::getUserCompany($id);
                    if (isset($company)) {
                        $args['company'] = $company->id;
                    }
                    $model = AppTimeSheet::getTimeSheets($args, FROM_API);
                    $msg = NON_BILLABLE;
                }
                $this->responseCode = HTTPCODE_SUCCESS;
                $this->results['Message'] = sprintf("%s" . ConstantMessages::$completed_timesheets, $msg);
                $this->results['Result'] = $model;
            } else {
                if ($contract->type == TIMESHEET_TYPE_OUT) {
                    $model = AppTimeSheet::getTimesheetOfTwoWayContract($args, FROM_API);
                    $msg = TWO_WAY;
                } else if ($contract->type == TIMESHEET_TYPE_IN_OUT || $contract->type == TIMESHEET_TYPE_OUTB) {
                    $model = AppTimeSheet::getTimesheetOfThreeWayContract($args, FROM_API);
                    $msg = THREE_WAY;
                } else if ($contract->type == TIMESHEET_TYPE_IN) {
                    $args['contractType'] = TIMESHEET_TYPE_INTERNAL;
                    $company = AppUser::getUserCompany($id);
                    if (isset($company)) {
                        $args['company'] = $company->id;
                    }
                    $model = AppTimeSheet::getTimeSheets($args, FROM_API);
                    $msg = NON_BILLABLE;
                }
                $this->responseCode = HTTPCODE_SUCCESS;
                $this->results['Message'] = sprintf("%s" . ConstantMessages::$current_timesheets, $msg);
                $this->results['Result'] = $model;
            }
        } else {
            $this->responseCode = HTTPCODE_FAILURE;
            $this->results['Message'] = ConstantMessages::$fields_missing;
        }
    }

    public function actionGetSubEntries() {

        $method = $this->getHTTPMethod();
        $token = AppInterface::getHeadersData('TOKEN');
        $header_missing = false;
        if (!isset($token)) {
            $header_missing = $this->headersMissing();
        } else {
            $user = $this->authenticateUserToken($token);
            if ($user instanceof User) {
                switch ($method) {
                    case HTTPMETHOD_GET:
                        $this->getSubEntries($_GET['timesheet_id']);
                        break;
                    default:
                        $this->invalidRequest();
                        break;
                }
            }
        }
        $this->generateResponse();
    }

    public function getSubEntries($parent_id) {

        $data = AppTimeSheet::getTimecard($parent_id);
        if (isset($data)) {
            $childs = $data['childs'];
            $mergeChilds = $data['mergeChilds'];
            $parent = $data['parent'];
            $breaks = $data['breaks'];
            $notes = $data['notes'];
            $details = $data['details'];
            if (isset($childs['dataSet']) && count($childs['dataSet']) > 0) {

                $response['status'] = true;
                $response['reason'] = '';
                $response['data'] = array_merge(array('parent' => $parent), array('inChild' => $childs), array('breaks' => $breaks), array('notes' => $notes), array('merChild' => $mergeChilds), array('detail' => $details));
            } else {
                $response['status'] = false;
                $response['reason'] = ConstantMessages::$no_record_found;
            }

            $this->results['Result'] = AppSetting::generateAjaxResponse($response);
            $this->responseCode = HTTPCODE_SUCCESS;
        } else {
            $this->responseCode = HTTPCODE_FAILURE;
            $this->results['Message'] = ConstantMessages::$fields_missing;
        }
    }

    public function actionStaff() {

        $method = $this->getHTTPMethod();
        $token = AppInterface::getHeadersData('TOKEN');
        $header_missing = false;
        if (!isset($token)) {
            $header_missing = $this->headersMissing();
        } else {
            $user = $this->authenticateUserToken($token);

            if ($user instanceof User) {
                switch ($method) {
                    case HTTPMETHOD_POST :
                        $this->addStaff($_POST, $user->id);
                        break;
                    case HTTPMETHOD_GET :
                        $this->getStaff($user->id);
                        break;
                    default:
                        $this->invalidRequest();
                        break;
                }
            }
        }
        $this->generateResponse();
    }

    public function getStaff($user_id) {
        ///*** Add Pagination here
        $company = AppUser::getUserCompany($user_id);
        if ($company != null) {
            $users = AppCompany::getCompanyEmployee($company->id, FLAG_ON, FROM_API, Yii::app()->request->getParam('page'), Yii::app()->request->getParam('size'));  ///*** Add Pagination here
            $this->results['Result'] = $users;
            $this->responseCode = HTTPCODE_SUCCESS;
        } else {
            $users = array();
            $this->results['Message'] = ConstantMessages::$no_record_found;
            $this->results['Result'] = $users;
            $this->responseCode = HTTPCODE_SUCCESS;
        }
    }

    public function addStaff($formData, $user_id) {
        if (isset($formData['User']['first_name']) && isset($formData['User']['email']) &&
                isset($formData['User']['password']) && isset($formData['User']['last_name']) &&
                isset($formData['User']['staff_id']) && isset($formData['Contract']['start_time']) &&
                isset($formData['Contract']['approver_id'])) {
            $user_exist = User::prepareUserForAuthorisation($formData['User']['email']);
            if (!$user_exist) {
                ///*** Test this scenario
                $user = AppUser::addStaff($formData, $user_id);
                if ($user instanceof User) {
                    $this->results['Message'] = ConstantMessages::$staff_added;
                    $this->responseCode = HTTPCODE_CREATED;
                } else {
                    $this->results['Result'] = $user;
                    $this->responseCode = HTTPCODE_FAILURE;
                }
            } else {
                $this->results['Message'] = ConstantMessages::$user_exist;
                $this->responseCode = HTTPCODE_FAILURE;
            }
        } else {
            $this->results['Message'] = ConstantMessages::$fields_missing;
            $this->responseCode = HTTPCODE_FAILURE;
        }
        $this->generateResponse();
    }

    ///*** Need to know what this W/S is doing ?
    //Extra added
//    public function actiongetShifts() {
//        $method = $this->getHTTPMethod();
//        $token = AppInterface::getHeadersData('TOKEN');
//        if (!isset($token)) {
//            $this->headersMissing();
//        }
//        $user = User::getUserFromToken($token);
//        $user = $this->authenticateUserToken($token);
////      dd($user_id);
//        switch ($method) {
//            case HTTPMETHOD_GET :
//                $this->getShifts();
//                break;
//            default:
//                $this->invalidRequest();
//                break;
//        }
//    }
//    public function getShifts() {
//        $shifts = AppCompany::getCompanyShifts(AppUser::getUserCompany()->id);
//        if (count($shifts) > 0) {
//            $this->results['Result'] = $shifts;
//            $this->responseCode = HTTPCODE_SUCCESS;
//        } else {
//            $this->responseCode = HTTPCODE_FAILURE;
//            $this->results['Message'] = ConstantMessages::$shift_not_found;
//        }
//        $this->generateResponse();
//    }

    public function actionTimesheet() {
        $method = $this->getHTTPMethod();
        $token = AppInterface::getHeadersData('TOKEN');
        $type = AppInterface::getHeadersData('TYPE');
        $contract_id = AppInterface::getHeadersData('CONTRACT');

        if (!isset($token) || !isset($contract_id)) {
            $this->headersMissing();
        } else {
            $user = $this->authenticateUserToken($token);

            if ($user instanceof User) {
                switch ($method) {
                    case HTTPMETHOD_GET:
                        $page = Yii::app()->request->getParam('page');
                        $size = Yii::app()->request->getParam('size');
                        ///*** Page size should be catered here in this W/S

                        $this->listTimesheet($type, $user->id, $contract_id, $page, $size);
                        break;
                    case HTTPMETHOD_PUT:
                        $put_vars = AppInterface::getPutRequestData();

                        $this->addTimesheet($put_vars, $type, $contract_id, $user->id, true);
                        break;
                    case HTTPMETHOD_POST:
                        $this->addTimesheet($_POST, $type, $contract_id, $user->id);
                        break;
                    default:
                        $this->invalidRequest();
                        break;
                }
            }
        }
        $this->generateResponse();
    }
    
    public function actionNote(){
        $method = $this->getHTTPMethod();
        $token = AppInterface::getHeadersData('TOKEN');
        $type = AppInterface::getHeadersData('TYPE');
        $timesheet_id = AppInterface::getHeadersData('TIMESHEET');

        if (!isset($token) || !isset($type)) {
            $this->headersMissing();
        } else {
            $user = $this->authenticateUserToken($token);

            if ($user instanceof User) {
                switch ($method) {
                    case HTTPMETHOD_GET:
                        if(!isset($timesheet_id)){
                            $this->headersMissing();
                        } else{
                            $model = AppCompany::getNotesById($timesheet_id);
                            if(count($model)>0){
                                $this->results['Result'] = $model;
                                $this->results['Message'] = ConstantMessages::$notes_found;
                                $this->responseCode = HTTPCODE_SUCCESS;
                            } else{
                                $this->results['Result'] = $model;
                                $this->results['Message'] = ConstantMessages::$notes_not_found;
                                $this->responseCode = HTTPCODE_NOT_FOUND;
                            }
                        }
                        break;
                    case HTTPMETHOD_POST:
                        $model = AppCompany::addNote($_POST, $type);
                        if($model instanceof TimesheetNotes){
                            $this->results['Message'] = ConstantMessages::$note_added;
                            $this->results['Result'] = $model;
                            $this->responseCode = HTTPCODE_CREATED;
                        } else{
                            $this->results['Message'] = ConstantMessages::$cannot_create_note;
                            $this->results['Result'] = $model;
                            $this->responseCode = HTTPCODE_FAILURE;
                        }
                        break;
                    default:
                        $this->invalidRequest();
                        break;
                }
            }
        }
        $this->generateResponse();
    }

    public function actionTimesheetpayrates() {
        $method = $this->getHTTPMethod();
        $token = AppInterface::getHeadersData('TOKEN');
        $timesheet_id = AppInterface::getHeadersData('TIMESHEET');
        $user = $this->authenticateUserToken($token);
        if (!isset($token) || !isset($timesheet_id)) {
            $this->headersMissing();
        } else {
            if ($user instanceof User) {
                switch ($method) {
                    case HTTPMETHOD_GET:
                        $model = AppTimeSheet::getTimesheetPayrates($timesheet_id);
                        $this->results["Result"] = $model;
                        if (count($model) > 0) {
                            $this->results["Message"] = ConstantMessages::$payrates_found;
                            $this->responseCode = HTTPCODE_SUCCESS;
                        } else {
                            $this->results["Message"] = ConstantMessages::$payrates_not_found;
                            $this->responseCode = HTTPCODE_NOT_FOUND;
                        }
                        break;
                    default:
                        $this->invalidRequest();
                        break;
                }
            }
        }

        $this->generateResponse();
    }

    public function addTimesheet($formData, $type, $contract_id, $id, $is_update = false) {

        if (isset($formData['Timesheet']['date']) && isset($type) && isset($contract_id) && isset($formData['timeIn']) && isset($formData['timeOut'])) {

            $tempTimeEntry = $formData['Timesheet']['date'];
            $contract = AppContract::getContract($contract_id);

            $timein = $formData['timeIn'];
            $timeout = $formData['timeOut'];
            if ($type == "") {
                $type = $contract->timesheetTypeSel;
            }
            if ($type != TIMESHEET_TYPE_CUSTOM) {
                $checkExist = AppTimeSheet::checkAlreadyExist($tempTimeEntry, $tempTimeEntry, $contract, TIMESHEET_TYPE_DAILY);
            } else {
                $timeInput = explode(" to ", $tempTimeEntry);
                $checkExist = AppTimeSheet::checkAlreadyExist($timeInput[0], $timeInput[1], $contract, TIMESHEET_TYPE_CUSTOM);
            }

            if ($checkExist != null && isset($checkExist['status']) && $checkExist['status'] == true && $is_update == false) {
                $this->responseCode = HTTPCODE_FAILURE;
                $this->results['Message'] = ConstantMessages::$timesheet_exist;
                $this->results['Result'] = $checkExist;
            } else {
//dd($contract);

                $response = AppTimeSheet::addTimesheet($formData, $type, $contract, $tempTimeEntry, $timein, $timeout, $id);
                if ($response instanceof Timesheet) {
                    $this->responseCode = HTTPCODE_CREATED;
                    $this->results['Message'] = ConstantMessages::$prompt_for_note;
                } else if ($response == false) {
                    $this->responseCode = HTTPCODE_FAILURE;
                    $this->results['Message'] = ConstantMessages::$start_time_greater_end_time;
                } else {
                    $this->responseCode = HTTPCODE_FAILURE;
                    $this->results['Message'] = ConstantMessages::$timesheet_cannot_created;
                }
                if ($response !== false)
                    $this->results['Result'] = $response;
                else
                    $this->results['Result'] = array();
            }
        } else {
            $this->responseCode = HTTPCODE_FAILURE;
            $this->results['Message'] = ConstantMessages::$fields_missing;
        }
    }

    public function actionForgetPassword() {
        $method = $this->getHTTPMethod();
        switch ($method) {
            case HTTPMETHOD_PUT:
                $put_vars = AppInterface::getPutRequestData();
                if (isset($put_vars['email'])) {
                    $user = AppUser::getUserByEmail($put_vars['email']);
                    if (isset($user)) {
                        $response = AppUser::updateUserPassword($user);

                        if ($response) {
                            $notification = Yii::app()->getModule('notification');
                            $notification->send(array('email' => array(
                                    'template' => 'ResetPassword',
                                    'params' => array('NAME' => $user->first_name . ' ' . $user->last_name,
                                        'PASSWORD' => $response, 'EMAIL' => $user->email),
                                    'to' => array(array('email' => $user->email, 'name' => $user->first_name . ' '
                                            . '' . $user->last_name)),
                                    'from' => array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'),
                                        'name' => SYSTEM_NAME),
                                    'cc' => array(),
                                    'bcc' => array(),
                            )));

                            $this->responseCode = HTTPCODE_SUCCESS;
                            $this->results['Message'] = ConstantMessages::$password_reset;
                        } else {

                            $this->responseCode = HTTPCODE_FAILURE;
                            $this->results['Message'] = ConstantMessages::$password_cannot_update;
                            $this->results['Result'] = $user->errors;
                        }
                    } else {
                        $this->responseCode = HTTPCODE_FAILURE;
                        $this->results['Message'] = ConstantMessages::$email_not_exist;
                    }
                } else {
                    $this->responseCode = HTTPCODE_FAILURE;
                    $this->results['Message'] = ConstantMessages::$fields_missing;
                }
                break;
            default:
                $this->invalidRequest();
                break;
        }
        $this->generateResponse();
    }

    public function actionCompanySignup() {
        $method = $this->getHTTPMethod();

        switch ($method) {
            case HTTPMETHOD_POST:

                if (isset($_POST['Company']['user_email']) && isset($_POST['Company']['company_address']) &&
                        isset($_POST['Company']['city']) && isset($_POST['Company']['street']) &&
                        isset($_POST['Company']['country']) && isset($_POST['Company']['postcode']) &&
                        isset($_POST['Company']['registration_number']) && isset($_POST['Company']['vat_number']) &&
                        isset($_POST['Company']['company_name']) && isset($_POST['Company']['sector']) &&
                        isset($_POST['Company']['company_phone']) && isset($_POST['User']['email']) &&
                        isset($_POST['User']['password']) && isset($_POST['User']['password']) && isset($_POST['User']['staff_id']) &&
                        isset($_POST['User']['phone']) && isset($_POST['User']['first_name']) && isset($_POST['User']['last_name'])) {
                    $company_email_exist = AppCompany::isEmailExists($_POST['Company']['user_email']);
                    $user_email_exist = AppUser::isEmailExists($_POST['User']['email']);

                    ///*** This should be done once company will be created.
                    /** First Company Create
                     * Logo Upload
                     * If logo successfully upload then update company model with logo name
                     * */
                    if (!$company_email_exist && !$user_email_exist) {
                        $company = AppCompany::validateCompany($_POST['Company'], COMPANY_TYPE, COMPANY_STATUS);
                        $user = AppUser::validateUser($_POST['User']);
                        if ($company instanceof Company && $user instanceof User) {
                            $company = AppCompany::addCompanyWithAddress($_POST['Company'], COMPANY_TYPE, COMPANY_STATUS);
                            $user = AppUser::addNewUser($_POST['User']);
                            if ($_FILES) {
                                $company->logo = CUploadedFile::getInstance($company, 'logo');
                                $images_path = realpath(Yii::app()->basePath . '/../uploads/photos');
                                $filename = explode('.', $company->logo);
                                $filename[0] = AppInterface::getFilename();
                                $newfilename = $filename[0] . '.' . $filename[1];
                                $company->logo->saveAs($images_path . '/' . $newfilename);
                                $company->logo = $newfilename;
                                $company->scenario = Company::SCENARIO_SAVE_COMPANY_LOGO;
                                $company->save();
                            }

//     contract for director
//                       ///*** Default values should come from a Constant proper Flags

                            $director_contract = AppCompany::addContract(array("user_id" => $user->id,
                                        "company_id" => $company->id, "approver_id" => $user->id,
                                        "role" => EMPLOYEE_TYPE_DIRECTOR,
                                        "status" => FLAG_ON, "start_time" => time(),
                                        "end_time" => -1, "type" => TIMESHEET_TYPE_IN,
                                        "employee_type" => EMPLOYEE_TYPE_FULL_TIME,
                                        "created_by" => FLAG_OFF, "created_at" => time(),
                                        "modified_by" => FLAG_OFF, "modified_at" => time(),
                                        "is_internal" => FLAG_ON, "owner_id" => $user->id));

//     contract for admin
                            ///*** Default values should come from a Constant proper Flags

                            $user_contract = AppCompany::addContract(array("user_id" => $user->id,
                                        "company_id" => $company->id, "approver_id" => $user->id,
                                        "role" => EMPLOYEE_TYPE_ADMIN,
                                        "status" => FLAG_ON, "start_time" => time(),
                                        "end_time" => -1, "type" => TIMESHEET_TYPE_IN,
                                        "employee_type" => EMPLOYEE_TYPE_FULL_TIME,
                                        "created_by" => FLAG_OFF, "created_at" => time(),
                                        "modified_by" => FLAG_OFF, "modified_at" => time(),
                                        "is_internal" => FLAG_ON, "owner_id" => $user->id));

                            ///**** Check $director_contract->errors & $user_contract->errors and if error comes then initmiate it
                            if ($director_contract instanceof Contract && $user_contract instanceof Contract) {
                                ///*** Use method from AppInterfaces::getAbsoluteUrl($path,$params);
                                $url = AppInterface::getAbsoluteUrl('/user/main/verifyemail', array('vcode' => $user->verification_code));
                                ///*** Use Notificaiton Module here
                                $notification = Yii::app()->getModule('notification');
                                $notification->send(array('email' => array(
                                        'template' => 'ConfirmEmail',
                                        'params' => array('NAME' => $user->full_name, 'CONFIRMLINK' => $url, 'EMAIL' => $user->email),
                                        'to' => array(array('email' => $user->email, 'name' => $user->full_name)),
                                        'from' => array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => SYSTEM_NAME),
                                        'cc' => array(),
                                        'bcc' => array(),
                                )));
                                //Successfully Signup
                                $this->responseCode = HTTPCODE_CREATED; ///*** It should be HTTPCODE_CREATED
                                $this->results['Message'] = ConstantMessages::$user_registered_success;
                            } else {
                                // Errors occurred
                                $this->responseCode = HTTPCODE_FAILURE;   ///*** Display Error reason in response with key 'Results'
                                $this->results['Message'] = ConstantMessages::$cannot_create_contract;
                                $this->results['Result'] = array_merge($director_contract, $user_contract);
                            }
                        } else {
                            // Errors occurred
                            $this->responseCode = HTTPCODE_FAILURE;
                            $this->results['Message'] = ConstantMessages::$cannot_create_company;
                            $this->results['Result'] = $user;
                            $this->results['Results'] = $company;
                        }
                    } else {
                        //Email already exist
                        $this->responseCode = HTTPCODE_FAILURE;
                        $this->results['Message'] = ConstantMessages::$email_already_exist;
                    }
                } else {
                    //Input fields are missing
                    $this->responseCode = HTTPCODE_FAILURE;
                    $this->results['Message'] = ConstantMessages::$fields_missing;
                }
                break;
            default:
                $this->invalidRequest();
                break;
        }
        $this->generateResponse();
    }

    public function actionPayrollerSignup() {
        $method = $this->getHTTPMethod();

        switch ($method) {
            case HTTPMETHOD_POST:
                if (isset($_POST['User']['email']) && isset($_POST['User']['password']) &&
                        isset($_POST['User']['staff_id']) && isset($_POST['User']['first_name']) && isset($_POST['User']['last_name']) && isset($_POST['Company']['address']) &&
                        isset($_POST['Company']['city']) && isset($_POST['Company']['street']) &&
                        isset($_POST['Company']['country']) && isset($_POST['Company']['postcode']) &&
                        isset($_POST['Company']['name']) && isset($_POST['Company']['sector']) && isset($_POST['Company']['phone'])) {
                    $emailStatus = AppUser::isEmailExists($_POST['User']['email']);
                    if (!$emailStatus) {
                        $user = AppUser::addNewUser($_POST['User']);
                        if ($user instanceof User) {

                            $payroller = AppPayroller::addPayroller($_POST, $user->id);
                            if ($payroller instanceof Payroller) {

                                $url = AppInterface::getAbsoluteUrl('/user/main/verifyemail', array('vcode' => $user->verification_code)); ///*** ///*** call AppInteraface isLocalServer() method which will return bolean value

                                $notification = Yii::app()->getModule('notification');

                                ///**** Following string should come from Constants
                                ///**** New CMS Template
                                AppLogging::addLog(ConstantMessages::$payroller_created, 'success', 'webserviceController');
                                $msg = sprintf(PAYROLLER_SIGNUP_MESSAGE, $user->first_name, $user->email);
                                $notification->send(array(
                                    'email' => array(
                                        'template' => 'ConfirmEmail',
                                        'params' => array('NAME' => $user->full_name, 'CONFIRMLINK' => $url, 'EMAIL' => $user->email),
                                        'to' => array(array('email' => $user->email, 'name' => $user->full_name)),
                                        'from' => array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => SYSTEM_NAME),
                                        'cc' => array(),
                                        'bcc' => array(),
                                    ), 'email' => array(
                                        'template' => 'Email',
                                        'params' => array('BODY' => $msg),
                                        'to' => array(array('email' => AppUser::get_super_user()->email, 'name' => 'Super Admin')),
                                        'from' => array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => SYSTEM_NAME),
                                        'cc' => array(),
                                        'bcc' => array(),
                                )));

                                $this->responseCode = HTTPCODE_CREATED;
                                $this->results['Message'] = ConstantMessages::$user_registered_success;
                            } else {
                                $this->responseCode = HTTPCODE_FAILURE;
                                $this->results['Message'] = ConstantMessages::$cannot_create_payroller;
                                $this->results['Result'] = $payroller;
                            }
                        } else {
                            $this->responseCode = HTTPCODE_FAILURE;
                            $this->results['Message'] = ConstantMessages::$cannot_create_payroller;
                            $this->results['Result'] = $user;
                        }
                    } else {
                        //Email already exist
                        $this->responseCode = HTTPCODE_FAILURE;
                        $this->results['Message'] = ConstantMessages::$email_already_exist;
                    }
                } else {
                    //Input fields are missing
                    $this->responseCode = HTTPCODE_FAILURE;
                    $this->results['Message'] = ConstantMessages::$fields_missing;
                }
                break;
            default:
                $this->invalidRequest();
                break;
        }
        $this->generateResponse();
    }

    public function actionLogin() {
        $method = $this->getHTTPMethod();

        switch ($method) {
            case HTTPMETHOD_POST:
                
                if (isset($_POST['User']['email']) && isset($_POST['User']['password']) && isset($_POST['User']['device_id']) && isset($_POST['User']['device_type'])) {
                    $user = AppUser::getUserByEmailPassword($_POST['User']['email'], AppInterface::getHash($_POST['User']['password']));
                    if (isset($user)) {                        
                        $user_device = UserDevices::addOrUpdateUserDevice(array("user_id" => $user->id,
                                    "device_id" => $_POST['User']['device_id'],
                                    "device_type" => $_POST['User']['device_type']));
                        if ($user->is_verified == FLAG_USER_UNVERIFIED) {
                            $this->results['Message'] = ConstantMessages::$account_not_verified;
                            $this->responseCode = HTTPCODE_FAILURE;
                        } else {
                            $response = AppUser::updateUserToken($user);
                            if ($response && $user_device) {
                                $this->results['Message'] = ConstantMessages::$login_success;
                                $this->results['Result'] = $user;
                                $this->results['Timezone'] = date_default_timezone_get();
                                $this->responseCode = HTTPCODE_SUCCESS;
                            } else {
                                $this->results['Message'] = ConstantMessages::$login_failed;
                                $this->results['Result'] = $user->errors;
                                $this->responseCode = HTTPCODE_FAILURE;
                            }
                        }
                    } else {
                        $this->responseCode = HTTPCODE_UNAUTHORIZED;
                        $this->results['Message'] = ConstantMessages::$user_invalid;
                    }
                } else if (isset($_POST['User']['token']) && isset($_POST['User']['device_id']) && isset($_POST['User']['device_type'])) {

                    $vResponse = AppUser::checkUpdateTokenValidity($_POST['User']['token']);
                    $user_device = false;
                    if ($vResponse instanceof User) {
                        $user_device = UserDevices::addOrUpdateUserDevice(array("user_id" => $vResponse->id,
                                    "device_id" => $_POST['User']['device_id'],
                                    "device_type" => $_POST['User']['device_type']));
                    }
                    if ($vResponse != null && $vResponse instanceof User && $user_device) {
                        $this->responseCode = HTTPCODE_SUCCESS;
                        $this->results['Message'] = ConstantMessages::$login_success;
                    } else if ($vResponse != null && !$user_device) {
                        $this->responseCode = HTTPCODE_FAILURE;
                        $this->results['Message'] = ConstantMessages::$cannot_update_token;
                    } else if (count($vResponse) > 0) {
                        $this->results['Message'] = ConstantMessages::$account_not_verified;
                        $this->responseCode = HTTPCODE_FAILURE;
                    } else {
                        $this->responseCode = HTTPCODE_UNAUTHORIZED;
                        $this->results['Message'] = ConstantMessages::$session_expired;
                    }
                    $this->results['Result'] = $vResponse;
                } else {
                    $this->responseCode = HTTPCODE_FAILURE;
                    $this->results['Message'] = ConstantMessages::$fields_missing;
                }
                break;
            default:
                $this->invalidRequest();
                break;
        }
        $this->generateResponse();
    }

    public function actionLogout() {
        $method = $this->getHTTPMethod();
        $token = AppInterface::getHeadersData('TOKEN');
        // dd($token);
        if (!isset($token)) {
            $this->headersMissing();
        } else {
            switch ($method) {
                case HTTPMETHOD_POST:
                    $id = AppUser::getIdFromToken($token);
                    if (!isset($id)) {
                        $this->responseCode = HTTPCODE_FAILURE;
                        $this->results['Message'] = ConstantMessages::$not_valid_token;
                    }
                    if (isset($_POST['device_id'])) {
                        UserDevices::model()->deleteAllByAttributes(array("device_id" => $_POST['device_id']));
                        $this->responseCode = HTTPCODE_SUCCESS;
                        $this->results['Message'] = ConstantMessages::$logout_success;
                    }
                    break;
                default:
                    $this->invalidRequest();
                    break;
            }
        }
        $this->generateResponse();
    }

    public function actionContact() {

        $method = $this->getHTTPMethod();
        $header_missing = false;

        $token = AppInterface::getHeadersData('TOKEN');
        if (!isset($token)) {
            $header_missing = $this->headersMissing();
        } else {
            $user = $this->authenticateUserToken($token);

            if ($user instanceof User) {
                switch ($method) {
                    case HTTPMETHOD_GET:
                        $model = AppCompany::getCompanyContacts(FROM_API, Yii::app()->request->getParam('page'), Yii::app()->request->getParam('size'));
                        if (isset($model)) {
                            $this->results['Result'] = $model;
                            $this->responseCode = HTTPCODE_SUCCESS;
                            $this->results['Message'] = ConstantMessages::$contact_list;
                        } else {
                            $this->responseCode = HTTPCODE_FAILURE;
                            $this->results['Message'] = ConstantMessages::$no_contact_list_found;
                        }
                        break;

                    case HTTPMETHOD_POST:

                        $model = AppCompany::saveContact($user->id, $_POST['Contact']);
                        if ($model instanceof Contact) {
                            $this->responseCode = HTTPCODE_CREATED;
                            $this->results['Message'] = ConstantMessages::$contact_added;
                        } else {
                            $this->responseCode = HTTPCODE_FAILURE;
                            $this->results['Message'] = ConstantMessages::$cannot_create_contact;
                        }
                        $this->results['Result'] = $model;
                        break;

                    default:
                        $this->invalidRequest();
                        break;
                }
            }
        }
        $this->generateResponse();
    }

    public function actionEmployees() {
        $method = $this->getHTTPMethod();
        $header_missing = false;

        $token = AppInterface::getHeadersData('TOKEN');
        if (!isset($token)) {
            $header_missing = $this->headersMissing();
        } else {
            $user = $this->authenticateUserToken($token);
            if ($user instanceof User) {
                switch ($method) {
                    case HTTPMETHOD_GET:
                        $model = AppCompany::getCompanyEmployee(AppUser::getUserCompany($user->id)->id);
                        $this->results['Result'] = $model;
                        if (count($model) > 0) {
                            $this->responseCode = HTTPCODE_SUCCESS;
                            $this->results['Message'] = ConstantMessages::$employees_found;
                        } else {
                            $this->responseCode = HTTPCODE_NOT_FOUND;
                            $this->results['Message'] = ConstantMessages::$employees_not_found;
                        }
                        break;
                    default:
                        $this->invalidRequest();
                        break;
                }
            }
        }
        $this->generateResponse();
    }

    public function actionNotification() {
        $method = $this->getHTTPMethod();
        $header_missing = false;

        $token = AppInterface::getHeadersData('TOKEN');
        if (!isset($token)) {
            $header_missing = $this->headersMissing();
        } else {
            $user = $this->authenticateUserToken($token);
            if ($user instanceof User) {
                switch ($method) {
                    case HTTPMETHOD_GET:
                        $notifications = AppUser::getUserNotifications($user->id);
                        $this->results['Result'] = $notifications;
                        if (count($notifications['model']) > 0) {
                            $this->responseCode = HTTPCODE_SUCCESS;
                            $this->results['Message'] = ConstantMessages::$notifications_found;
                        } else {
                            $this->responseCode = HTTPCODE_NOT_FOUND;
                            $this->results['Message'] = ConstantMessages::$notifications_not_found;
                        }
                        break;
                    case HTTPMETHOD_POST:
                        $t_id = Yii::app()->request->getParam('id');
                        $title = Yii::app()->request->getParam('title');
                        $message = Yii::app()->request->getParam('message');
                        $notification = Yii::app()->getModule('notification');
                        $not_result = AppInterface::notification($t_id, 15, 0, 0, AppUser::getUserId(), $title, $message);
                        $notifier = new Notification();
                        $notification_count = $notifier->getUserNotificationUnseenCount($t_id, true);
                        $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
                        $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;

                        $notification->send(array(
                            'push' => array('company_message',
                                array('id' => $t_id, 'msg' => $message,
                                    'url' => $url, 'logo' => $logo, 'count' => $notification_count)
                            ))
                        );
                        $this->responseCode = HTTPCODE_SUCCESS;
                        break;
                    default:
                        $this->invalidRequest();
                        break;
                }
            }
        }
        $this->generateResponse();
    }

    public function actionActivity() {
        $method = $this->getHTTPMethod();
        $token = AppInterface::getHeadersData("TOKEN");

        if (!isset($token)) {
            $this->headersMissing();
        } else {
            $user = AppUser::getUserByToken($token);
            if (isset($user)) {
                switch ($method) {
                    case HTTPMETHOD_GET:
                        $model = AppUser::getUserActivities($user->id, true);
                        $this->responseCode = HTTPCODE_SUCCESS;
                        $this->results['Result'] = $model;
                        $this->results['Message'] = ConstantMessages::$activities_found;
                        break;
                    default :
                        $this->invalidRequest();
                        break;
                }
            } else {
                $this->responseCode = HTTPCODE_FAILURE;
                $this->results['Message'] = ConstantMessages::$not_valid_token;
            }
        }
        $this->generateResponse();
    }

    public function actionClock() {

        $method = $this->getHTTPMethod();
        $token = AppInterface::getHeadersData('Token');
        // $token = 'vLDQOzHmAlpYhS2C';
        // dd($token);

        if (!isset($token)) {
            $this->headersMissing();
        } else {
            $t_id = Yii::app()->request->getParam('id');
            // dd($t_id);
            $type = Yii::app()->request->getparam('type');
            $date = Yii::app()->request->getParam('date');
            $contract_id = Yii::app()->request->getParam('contract_id');

            $user = $this->authenticateUserToken($token);

            if ($user instanceof User) {
                switch ($method) {
                    case HTTPMETHOD_PUT:
                        if ($type == TIMESHEET_STATUS_APPROVED || $type == TIMESHEET_STATUS_DISAPPROVED) {
                            $put_vars = AppInterface::getPutRequestData();
                            //$t_id is user_timing_id
                            //status and reason for put_vars
                            $response = AppCompany::updateApprovalListStatus($put_vars, $t_id);

                            if ($response instanceof UserTimings) {
                                $this->responseCode = HTTPCODE_SUCCESS;
                                $this->results['Message'] = ConstantMessages::$timesheet_status_updated;
                            } else {
                                $this->responseCode = HTTPCODE_FAILURE;
                                $this->result['Message'] = ConstantMessages::$cannot_update_timesheet;
                            }
                            $this->results['Result'] = $response;
                        }
                        break;

                    case HTTPMETHOD_GET:
                        if (isset($t_id) || isset($date) || isset($contract_id)) {
                            //$t_id is user_timing_id
                            //This case is for View Details of a particular Clock Entry
                            if (isset($date) || isset($contract_id)) {
                                $model = AppCompany::getClockEntryDetails(0, $user->id, strtotime(date('m/d/Y')), $contract_id);
                            } else {
                                $model = AppCompany::getClockEntryDetails($t_id);
                            }
                            $this->results['Message'] = ConstantMessages::$timesheet_details;
                        } else if ($type == CLOCK_ENTRY_LIST) {
                            //This case is for List of Clock Entries
                            $model = AppCompany::getClockEntries($user->id, FROM_API, Yii::app()->request->getParam('date'), Yii::app()->request->getParam('page'), Yii::app()->request->getParam('size'));
                            $this->results['Message'] = ConstantMessages::$clock_entries;
                        } else if ($type == CLOCK_APPROVAL_LIST) {
                            //This case is for Approval List

                            $model = AppCompany::getApprovalList(FROM_API, Yii::app()->request->getParam('page'), Yii::app()->request->getParam('size'));
                            $this->results['Message'] = ConstantMessages::$approval_list;
                        }
                        $this->responseCode = HTTPCODE_SUCCESS;
                        $this->results['Result'] = $model;
                        break;
                    case HTTPMETHOD_POST:
                        if (isset($t_id) && isset($type)) {
                            $this->clockLunchInOut($t_id, $type, $user);
                        } else if ($type == REPORT) {
                            $this->getReport($_POST);
                        } else {
                            $this->responseCode = HTTPCODE_FAILURE;
                            $this->results['Message'] = ConstantMessages::$fields_missing;
                        }
                        break;
                    default:
                        $this->invalidRequest();
                        break;
                }
            }
        }
        $this->generateResponse();
    }

    public function clockLunchInOut($t_id, $type, $user) {
        if (AppCompany::isUserInBranch(Yii::app()->request->getparam('latitude'), Yii::app()->request->getparam('longitude'))) {
            if ($type == CLOCK_IN) {   
                if ($t_id == 1) {                    
                    $t_id = AppContract::getInternalContract($user->id)->id;
                }
                //This case is for time in and $t_id is contract_id
                $model = AppCompany::timeIn($user->id, $t_id);
                if ($model instanceof UserTimings) {
                    $model = AppCompany::getClockEntryDetails($model->id);
                    $this->responseCode = HTTPCODE_CREATED;
                    $this->results['Message'] = ConstantMessages::$clock_in;
                } else {
                    $this->responseCode = HTTPCODE_FAILURE;
                    $this->results['Message'] = ConstantMessages::$failed_to_clock_in;
                }
            } else if ($type == LUNCH_IN) {
                //This case is for lunch in and $t_id is user_timing_id
                $response = AppCompany::lunchIn($user->id, $t_id);
                if ($response instanceof UserLunch) {
                    $model = AppCompany::getClockEntryDetails($t_id);
                    $this->results['Message'] = ConstantMessages::$lunch_in;
                    $this->responseCode = HTTPCODE_CREATED;
                } else {
                    $model = $response;
                    $this->results['Message'] = ConstantMessages::$failed_to_lunch_in;
                    $this->responseCode = HTTPCODE_FAILURE;
                }
            } else if ($type == LUNCH_OUT) {
                //This case is for lunch out and $t_id is user_timing_id
                $response = AppCompany::lunchOut(Yii::app()->request->getParam('lunch_id'));
                if ($response instanceof UserLunch) {
                    $model = AppCompany::getClockEntryDetails($t_id);
                    $this->results['Message'] = ConstantMessages::$lunch_out;
                    $this->responseCode = HTTPCODE_SUCCESS;
                } else {
                    $model = $response;
                    $this->results['Message'] = ConstantMessages::$failed_to_lunch_out;
                    $this->responseCode = HTTPCODE_FAILURE;
                }
            } else if ($type == CLOCK_OUT) {
                
                //This case is for time out and $t_id is user_timing_id
                $response = AppCompany::timeOut($t_id);
                if ($response instanceof UserTimings) {

                    $model = AppCompany::getClockEntryDetails($t_id);
                    $this->results['Message'] = ConstantMessages::$clock_out;
                    $this->responseCode = HTTPCODE_SUCCESS;
                } else {
                    $model = $response;
                    $this->results['Message'] = ConstantMessages::$failed_to_clock_out;
                    $this->responseCode = HTTPCODE_FAILURE;
                }
            } else if ($type == NOTE) {
                $response = AppCompany::addNote($_POST, ConstantMessages::$note_type_user_timing);
                if ($response instanceof TimesheetNotes) {
                    $model = AppCompany::getClockEntryDetails($t_id);
                    $this->results['Message'] = ConstantMessages::$note_added;
                    $this->responseCode = HTTPCODE_CREATED;
                } else {
                    $model = $response;
                    $this->results['Message'] = ConstantMessages::$failed_to_add_note;
                    $this->responseCode = HTTPCODE_FAILURE;
                }
            }
            $this->results['Result'] = $model;
        } else {
            $this->results['Message'] = ConstantMessages::$out_of_branch;
            $this->responseCode = HTTPCODE_FAILURE;
        }
    }

    public function getReport($formData) {
        $isAdmin = AppUser::isUserAdmin();
        $employees = null;
        $date_group = false;
        $is_html = false;
        $is_general = Yii::app()->getRequest()->queryString;
//        if ($isAdmin && $is_general != '') {
//            $employees = AppUser::get_all_users_by_company();
//        }
        $formData['type'] = $formData['report_type'];
        $data = AppCompany::getTimesheetsForReport($formData, $is_general);
        $timesheets = $data["timesheets"];
        $date_group = $data["date_group"];
        $this->layout = '//layouts/blank_head';

        ///*** should return rendered file in response of this W/S
        ///*** should return rendered file in response of this W/S

        if ($_POST["export"] == FLAG_ON) {
            $this->layout = '//layouts/column1';
            //If export data in html
            $is_html = true;
            $model = array('model' => $timesheets,
                'employees' => $employees, 'date_group' => $date_group,
                'is_html' => $is_html);
        } else {
            //if export data in pdf
            $is_html = false;
            $mPDF1 = new mpdf();
            $mPDF1->WriteHTML($this->render('/company/companyShifts/generatereport', array('model' => $timesheets,
                        'employees' => $employees, 'date_group' => $date_group, 'is_html' => $is_html), true));
//                    dd($html);
            if (!file_exists(Yii::app()->baseUrl . '/uploads/clock_reports')) {
                mkdir(Yii::app()->baseUrl . '/uploads/clock_reports', 0777, true);
            }
            $path = Yii::app()->baseUrl . '/uploads/clock_reports' . time() . '_clock_report.pdf';
            $mPDF1->Output($path);
            $model = $path;
        }
        $this->responseCode = HTTPCODE_SUCCESS;
        $this->results['Result'] = $model;

        ///***
    }

    public function actionPayrates() {
        $method = $this->getHTTPMethod();
        $token = AppInterface::getHeadersData("TOKEN");
        if (!isset($token)) {
            $this->headersMissing();
        } else {
            $user = $this->authenticateUserToken($token);
            if ($user instanceof User) {
                switch ($method) {
                    case HTTPMETHOD_GET:
                        $contract_id = AppContract::getInternalContract(AppUser::getCurrentUser()->id)->id;
                        $payrates = AppContract::getContractPayrates($contract_id);
                        $items = AppContract::getContractItems($contract_id);
                        $this->results['Result'] = array('payrates' => $payrates, "items" => $items);
                        $this->results['Message'] = ConstantMessages::$payrates_found;
                        break;
                    default:
                        $this->invalidRequest();
                        break;
                }
            }
        }
        $this->generateResponse();
    }

    public function actionContract() {
        $method = $this->getHTTPMethod();
        $token = AppInterface::getHeadersData('TOKEN');
        if (!isset($token)) {
            $this->headersMissing();
        } else {
            $user = $this->authenticateUserToken($token);
            if ($user instanceof User) {
                $type = Yii::app()->request->getParam('type');
                switch ($method) {
                    case HTTPMETHOD_GET:
                        $contracts = AppContract::getAllContracts($user->id);
                        $_contracts = array();
                        $temp = array();
                        foreach ($contracts as $item) {
                            $temp['company_name'] = $item->company->name;
                            $already_timeIn = AppCompany::getClockEntryDetails(0, $user->id, strtotime(date('m/d/Y')), $item->id);
                            if (isset($already_timeIn['model'])) {
                                $temp['status'] = true;
                            } else {
                                $temp['status'] = false;
                            }
                            $temp['contract_name'] = $item->type;
                            $temp['user_name'] = $item->user->first_name;
                            $temp['id'] = $item->id;
                            array_push($_contracts, $temp);
                        }
                        if (count($_contracts) > 0) {
                            $this->results['Message'] = "Contracts Found";
                            $this->responseCode = HTTPCODE_SUCCESS;
                        } else {
                            $this->results['Message'] = ConstantMessages::$no_record_found;
                            $this->responseCode = HTTPCODE_NOT_FOUND;
                        }
                        $this->results['Result'] = $_contracts;
                        break;

                    case HTTPMETHOD_POST:
                        if ($type == TIMESHEET_TYPE_IN) {
                            $this->addInContract($_POST);
                        } else if ($type == TIMESHEET_TYPE_OUT) {
                            $this->addOutContract($_POST);
                        } else if ($type == TIMESHEET_TYPE_IN_OUT) {
                            $this->addInOutContract($_POST);
                        }
                        break;
                    default:
                        $this->invalidRequest();
                        break;
                }
            }
        }
        $this->generateResponse();
    }

    public function actionPayslip() {
        $method = $this->getHTTPMethod();
        $token = AppInterface::getHeadersData('TOKEN');

        ///*** Use same pattern here
        if (!isset($token)) {
            $this->headersMissing();
        } else {
            $p_id = Yii::app()->request->getParam('id');
            $status = Yii::app()->request->getParam('status');
            $user = $this->authenticateUserToken($token);
            ///***
            if ($user instanceof User) {
                switch ($method) {
                    case HTTPMETHOD_GET:
                        if (isset($p_id)) {
                            ///*** This should be called from some component??
                            $entry = AppPayslip::getPayslip($p_id);
                            if (isset($entry)) {
                                ///*** This should be called from some component??
//                                
                                if (AppTimeSheet::canDownloadPayslip($entry)) {
                                    $model = AppPayslip::getPayslip($p_id);
                                    if ($model != null) {
                                        $data = AppPayslip::generateMarkup($model->id);
                                        $mPDF1 = new mpdf('c', 'A4-L');
                                        $mPDF1->WriteHTML($data);
                                        $webroot = Yii::getPathOfAlias('webroot');
                                        if (!is_dir($webroot . '/uploads/payslips')) {
                                            mkdir($webroot . '/uploads/payslips');
                                            chmod(($webroot . '/uploads/payslips'), 0755);
                                            // the default implementation makes it under 777 permission, which you could possibly change recursively before deployment, but here's less of a headache in case you don't
                                        }
                                        $file_name = AppInterface::getFilename().'.pdf';
                                        $file = $webroot . '/uploads/payslips/'.$file_name;
                                        $mPDF1->Output($file,EYiiPdf::OUTPUT_TO_FILE);
                                    }
//                                    dd($file);
                                    AppLogging::addLog('Download File', 'success', 'webserviceController');
                                    ///*** What is happening here
//                                    Yii::app()->getRequest()->sendFile(AppSetting::genAutoFileName($entry->file), file_get_contents(Yii::app()->params['uploads']['payslip'] . $entry->file));
                                    $this->results['Result'] = AppInterface::getAbsoluteUrl('/uploads/payslips/' . $file_name);
                                    $this->responseCode = HTTPCODE_SUCCESS;
                                    $this->results['Message'] = ConstantMessages::$payslips;
                                } else {
                                    $this->responseCode = HTTPCODE_FAILURE;
                                    $this->results['Message'] = ConstantMessages::$payslip_not_found;
                                }
                            } else {
                                $this->responseCode = HTTPCODE_FAILURE;
                                $this->results['Message'] = ConstantMessages::$payslip_not_found;
                            }
                        } else {
                            ///*** Add Pagination here
                            if (isset($status)) {
                                $model = AppPayroller::getPayrolls($status, (AppUser::isUserAdmin() && !isset($_GET['id'])) ? '' : (isset($_GET['id']) ? $_GET['id'] : AppUser::getUserId()), FROM_API, Yii::app()->request->getParam('page'), Yii::app()->request->getParam('size'));
                            } else {
                                $model = AppPayroller::getPayrolls('', (AppUser::isUserAdmin() && !isset($_GET['id'])) ? '' : (isset($_GET['id']) ? $_GET['id'] : AppUser::getUserId()), FROM_API, Yii::app()->request->getParam('page'), Yii::app()->request->getParam('size'));
                            }
//                            $result = array();
//                            $i = 0;
//                            foreach ($model as $item) {
//                                $result[$i]['model'] = $item;
//                                $entry = AppPayslip::getPayslip($p_id);
//                                if (isset($entry)) {
//                                    if (AppTimeSheet::canDownloadPayslip($entry)) {
//                                        $result[$i]['download'] = PAYSLIP_PATH . $entry->file;
//                                    }
//                                }
//                                $i++;
//                            }
                            $this->responseCode = HTTPCODE_SUCCESS;
                            $this->results['Result'] = $model;
                            $this->results['Message'] = ConstantMessages::$payslip_list;
                        }
                        break;
                    default:
                        $this->invalidRequest();
                        break;
                }
            }
        }
        $this->generateResponse();
    }

    public function actionQuickTimesheetExpense() {
        $method = $this->getHTTPMethod();
        $token = AppInterface::getHeadersData('TOKEN');
        $timesheet_id = AppInterface::getHeadersData('TIMESHEET');

        ///*** Use Same Pattern
        if (!isset($token) && !isset($timesheet_id)) {
            $this->headersMissing();
        } else {
            $user = $this->authenticateUserToken($token);
            if ($user instanceof User) {
                switch ($method) {
                    case HTTPMETHOD_GET:
                        $model = QuickTsExpenses::model()->findAllByAttributes(array("quick_time_id" => $timesheet_id));
                        if (count($model) > 0) {
                            $this->results['Message'] = ConstantMessages::$timesheet_expenses;
                            $this->responseCode = HTTPCODE_SUCCESS;
                        } else {
                            $this->results['Message'] = ConstantMessages::$timseeheet_expense_not_found;
                            $this->responseCode = HTTPCODE_NOT_FOUND;
                        }
                        $this->results['Result'] = $model;
                        break;
                    default:
                        $this->invalidRequest();
                        break;
                }
            }
        }
        $this->generateResponse();
    }

    public function actionQuickTimesheet() {
        $method = $this->getHTTPMethod();
        $token = AppInterface::getHeadersData('TOKEN');
        $status = AppInterface::getHeadersData('STATUS');
        $contact = AppInterface::getHeadersData('CONTACT');

        ///*** Use Same Pattern
        if (!isset($token) && !isset($status)) {
            $this->headersMissing();
        } else {
            $q_id = Yii::app()->request->getParam('id');
            $user = $this->authenticateUserToken($token);
            if ($user instanceof User) {
                switch ($method) {
                    case HTTPMETHOD_PUT:
                        $put_vars = AppInterface::getPutRequestData();
//dd($q_id);
                        ///*** Following method need refactoring
                        $this->saveQuickTimesheet($put_vars, $q_id, $_FILES);
                        ///***
                        break;

                    case HTTPMETHOD_DELETE:
                        $this->deleteQuickTimesheet($q_id);
                        break;

                    case HTTPMETHOD_GET:

                        if (isset($status)) {
                            if ($status == TIMESHEET_TYPE_CURRENT) { ///*** These constants should come from CONSTANTS
                                ///**** This should cater pagination , status filter etc. $args    
                                if (isset($contact)) {
                                    $model = AppCompany::getNonbillableTimesheets(array('status' => 'Open', 'contact_id' => $contact), FROM_API, Yii::app()->request->getParam('page'), Yii::app()->request->getParam('size'));
                                } else {
                                    $model = AppCompany::getNonbillableTimesheets(array('status' => 'Open'), FROM_API, Yii::app()->request->getParam('page'), Yii::app()->request->getParam('size'));
                                }
                                $this->results['Message'] = ConstantMessages::$oneWay_current;
                            } else if ($status == TIMESHEET_TYPE_COMPLETED) {
                                if (isset($contact)) {
                                    $model = AppCompany::getNonbillableTimesheets(array('status' => 'Emailed', 'contact_id' => $contact), FROM_API, Yii::app()->request->getParam('page'), Yii::app()->request->getParam('size'));
                                } else {
                                    $model = AppCompany::getNonbillableTimesheets(array('status' => 'Emailed'), FROM_API, Yii::app()->request->getParam('page'), Yii::app()->request->getParam('size'));
                                }
                                $this->results['Message'] = ConstantMessages::$oneWay_completed;
                            } else {
                                $model = $this->invoiceTimesheet($q_id);
                                $this->results['Message'] = ConstantMessages::$oneWayTimesheetInvoice;
                            }
                        } else {
                            if (isset($contact)) {
                                $model = AppCompany::getNonbillableTimesheets(array('contact_id' => $contact), FROM_API, Yii::app()->request->getParam('page'), Yii::app()->request->getParam('size'));
                            } else {
                                $model = AppCompany::getNonbillableTimesheets(array(), FROM_API, Yii::app()->request->getParam('page'), Yii::app()->request->getParam('size'));
                            }
                        }
                        $this->responseCode = HTTPCODE_SUCCESS;
                        $this->results['Result'] = $model;
                        break;
                    case HTTPMETHOD_POST:
                        $this->saveQuickTimesheet($_POST, ConstantMessages::$flag_off, $_FILES);
                        break;

                    default:
                        $this->invalidRequest();
                        break;
                }
            }
        }
        $this->generateResponse();
    }

    public function actionInvoice() {
        $method = $this->getHTTPMethod();
        $token = AppInterface::getHeadersData('TOKEN');
        $contact_id = AppInterface::getHeadersData('CONTACT');

        if (!isset($token)) {
            $this->headersMissing();
        } else {
            $user = $this->authenticateUserToken($token);
            if ($user instanceof User) {
                switch ($method) {
                    case HTTPMETHOD_GET:
                        $invoices = AppUser::getUserInvoices(array("contact_id" => $contact_id));
                        AppLogging::addLog('Contact list for invoice ', 'success', 'application.invoice.controller.main');
                        if (count($invoices) > 0) {
                            $this->results['Message'] = ConstantMessages::$invoices_found;
                            $this->responseCode = HTTPCODE_SUCCESS;
                        } else {
                            $this->results['Message'] = ConstantMessage::$invoices_not_found;
                            $this->responseCode = HTTPCODE_NOT_FOUND;
                        }
                        $this->results['Result'] = $invoices;
                        break;
                    case HTTPMETHOD_POST:
                        $company = AppUser::getUserCompany($user->id);
                        $contacts = AppCompany::getCompanyContacts();
                        $details = array();
                        $invoice_items = array();
                        $contact_id = "";

                        if (count($details) < 1) {
                            $details['company'] = $company['name'];
                            $details['companyAddress'] = $company['address'];
                            $details['post_code'] = $company['post_code'];
                            $details['companyCity'] = $company['city'];
                            $details['companyCountry'] = $company['country'];
                            $details['logo'] = $company['logo'];
                        }
                        if (Yii::app()->getRequest()->isPostRequest) {
                            if (!empty($_POST['timesheet_id'])) {
                                $details = AppTimeSheet::compileCardDetail($_POST['timesheet_id']);
                            } elseif (!empty($_POST['invoice_id'])) {
                                $temp = Invoice::model()->findByPk($_POST['invoice_id']);
                                $details = AppTimeSheet::compileCardDetail($temp->timesheet_id);
                            }

                            $details['logo'] = $company['logo'];
                            $details['date'] = $_POST['date'];
                            if (empty($details['logo'])) {
                                $details['logo'] = "";
                            }
                            if (isset($_POST['contacts']) && $_POST['contacts'] != 'out') {
                                $contact = Contact::model()->findByAttributes(array('id' => $_POST['contacts']));
                                if (isset($contact)) {
                                    $select_email = $contact->email;
                                    $select_name = $contact->name;
                                    $details['client_info'] = Contact::model()->findByAttributes(array('id' => $_POST['contacts']));
                                    $contact_id = $_POST['contacts'];
                                }
                            } else {
                                $select_email = $_POST['contact-email'];
                                $select_name = $_POST['contact-name'];
                                $contact = AppCompany::addContact(array('name' => $select_name, 'email' => $select_email, 'type' => 'Individual', 'company_id' => AppUser::getUserCompany()->id, 'created_by' => AppUser::getUserId(), 'modified_by' => AppUser::getUserId(), 'created_at' => time(), 'modified_at' => time(), 'status' => 1, 'address' => $_POST['client_add'], 'country' => $_POST['client_country'], 'post_code' => $_POST['client_pc'], 'city' => $_POST['client_city']));


                                if (isset($contact)) {
                                    $contact_id = $contact->id;
                                    $details['client_info'] = Contact::model()->findByAttributes(array('id' => $contact->id));
                                } else {
                                    Yii::app()->user->setFlash('error', 'An error occured while creating invoice');
                                    $this->redirect(array('index'));
                                }
                            }

                            if (!empty($details['employeeName'])) {
                                $html = $this->renderPartial('timesheet', $details, true);
                            } else {
                                $html = "";
                            }
                            if (empty($company['logo'])) {
                                $logo = "";
                            } else {
                                $logo = $company['logo'];
                            }
//dd(DateTime::createFromFormat('d/m/Y H:i:s', $details['date'] . " 00:00:00")->getTimestamp());
                            $invoice = AppCompany::addInvoice(array('html' => $html, 'contact_id' => $contact_id,
                                        'status' => 1, 'created_by' => AppUser::getUserId(),
                                        'modified_by' => AppUser::getUserId(), 'created_at' => time(),
                                        'modified_at' => time(), 'logo' => $logo, 'invoice_ref' => $_POST['invoice_ref'],
                                        'sub_total' => $_POST['subtotal'], 'vat' => $_POST['vat'],
                                        'total' => $_POST['total'], 'company_id' => $company->id,
                                        'contractor_name' => $company['name'], 'contractor_address' => $company['address'],
                                        'vat_number' => $company['vat_number'], 'timesheet_id' => NULL, 'date' => $details['date']));

                            $details['invoice_ref'] = $_POST['invoice_ref'];
                            $details['subtotal'] = $_POST['subtotal'];
                            $details['vat'] = $_POST['vat'];
                            $details['total'] = $_POST['total'];
                            $details['contractor_name'] = $company['name'];
                            $details['contractor_address'] = $company['address'];
                            $details['vat_number'] = $company['vat_number'];
                            $details['invoice_to'] = $select_name;
                        }
                        if ($invoice instanceof Invoice) {
                            for ($x = 0; $x < $_POST['count']; $x++) {
                                $invoiceItems = new InvoiceItems();
                                $invoiceItems->item = $_POST['item' . $x];
                                $invoiceItems->rate = $_POST['rate' . $x];
                                $invoiceItems->cost = $_POST['cost' . $x];
                                $invoiceItems->unit = $_POST['unit' . $x];
                                $invoiceItems->id = AppInterface::getUniqueId();
                                $invoiceItems->invoice_id = $invoice->id;
                                $id = $invoiceItems->model()->create($invoiceItems->id, $invoiceItems->item, $invoiceItems->unit, $invoiceItems->rate, $invoiceItems->cost, $invoice->id);
                                $details['invoice_items'][$x] = $invoiceItems;
                            }
                            $user = AppUser::getCurrentUser();
                            $details['user'] = $user;
                            $temp = $this->actionCreatepdf($details);
                            $attachement = Yii::getPathOfAlias('webroot') . '/uploads/invoicespdf/' . $details['company'] . '-' . $details['invoice_ref'] . '.pdf';
                            $file_name = $details['company'] . '-' . $details['invoice_ref'] . '.pdf';
//            $invoice_pdf = $this->createUrl('/uploads/invoicespdf/'.$details['company'].'-'.$details['invoice_ref'].'.pdf');
                            $mailer = new AppMailer();
                            $msg = "Invoice sent from " . $user['first_name'] . " " . $user['last_name'];
                            $mailer->prepareBody('invoice', array('INVOICE' => $msg));

                            $file_name = $details['company'] . '-' . $details['invoice_ref'] . '.pdf';
                            $response = $mailer->sendSesMailWithAttach(array('email' => $select_email, 'name' => $select_name), array('email' => 'support@speedypayrolls.com', 'name' => 'Support'), "Invoice Email", "Hello", $attachement, $file_name);
                            Yii::app()->user->setFlash('success', 'Invoice sent to selected user.');
//        AppSetting::generateAjaxResponse($response);
//            return;
                            $this->responseCode = HTTPCODE_SUCCESS;
                            $this->results['Message'] = ConstantMessages::$invoice_sent;
                        } else {
                            $this->responseCode = HTTPCODE_FAILURE;
                            $this->results['Result'] = $invoice;
                            $this->results['Message'] = ConstantMessages::$invoice_not_send;
                        }
                        break;
                    default:
                        $this->invalidRequest();
                        break;
                }
            }
            $this->generateResponse();
        }
    }

    public function actionCreatepdf($details) {
        $pdf = new SPPDF();
        $pdf->details = $details;
        $pdf->AddPage();

        $pdf->SetTextColor(0, 0, 0);
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->SetFont("Arial", "B", "13");
        $pdf->Write("4", "Client:");
        $pdf->SetFont("Arial", "", "12");
        $pdf->Cell('0', '0', $details['date'], '', '0', 'R');
        $pdf->Cell('0', '7', '', '', '1');
        $pdf->Write("4", $details['client_info']['name']);
        $pdf->Cell("0", "5", "", "", "1");
        $pdf->Write("4", $details['client_info']['address']);
        $pdf->Cell("0", "5", "", "", "1");
        $pdf->Write("4", $details['client_info']['city'] . " " . $details['client_info']['post_code']);
        $pdf->Cell("0", "5", "", "", "1");
        $pdf->Write("4", $details['client_info']['country']);
        $pdf->Cell("0", "5", "", "", "1");
        $pdf->Write("4", "VAT NO. :" . $details['vat_number']);
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->Write("4", "Invoice No. :" . $details['invoice_ref']);
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->SetFont("Arial", "B", "12");
        $pdf->Cell('0', '10', "", "", "1");
        $pdf->Cell('80', '10', 'Services', '', '0', 'L');
        $pdf->Cell('30', '10', 'Unit Price', '', '0', 'C');
        $pdf->Cell('30', '10', 'Quantity', '', '0', 'C');
        $pdf->Cell('40', '10', 'Total (excl. VAT)', '', '0', 'R');
        $pdf->Cell('0', '10', "", "", "1");
        $pdf->SetFont("Arial", "", "12");
        $pdf->SetLineWidth(0.6);
        $pdf->Line(11, 145, 190, 145);
        $ylength = 145;
        foreach ($details['invoice_items'] as $item) {
            $pdf->Cell('80', '10', $item->item, "", "0", 'L');
            $pdf->Cell('30', '10', $item->unit, "", "0", 'C');
            $pdf->Cell('30', '10', $item->rate, "", "0", 'C');
            $pdf->Cell('40', '10', $item->cost, "", "0", 'R');
            $pdf->Cell('0', '10', "", "", "1");
            $ylength += 10;
        }
//loop
        $pdf->SetLineWidth(0.6);
        $pdf->Line(11, $ylength + 2, 190, $ylength + 2);
        $pdf->Cell('80', '10', '', '', '0', 'L');
        $pdf->Cell('30', '10', '', '', '0', 'C');
        $pdf->Cell('30', '10', 'Sub Total', '', '0', 'C');
        $pdf->Cell('40', '10', $details['subtotal'], '', '0', 'R');
        $pdf->Cell('0', '10', "", "", "1");
        $pdf->Cell('80', '10', '', '', '0', 'L');
        $pdf->Cell('30', '10', '', '', '0', 'C');
        $pdf->Cell('30', '10', 'VAT', '', '0', 'C');
        $pdf->Cell('40', '10', $details['vat'], '', '0', 'R');
        $pdf->Cell('0', '10', "", "", "1");
        $pdf->Cell('80', '10', '', '', '0', 'L');
        $pdf->Cell('30', '10', '', '', '0', 'C');
        $pdf->Cell('30', '10', 'Order Total', '', '0', 'C');
        $pdf->Cell('40', '10', $details['total'], '', '0', 'R');

        $pdf->SetFont("Arial", "", "11");
        $pdf->Cell('0', '10', "", "", "1");
        $pdf->Cell('0', '10', "", "", "1");

        // make the directory to store the pic:
        if (!is_dir(Yii::getPathOfAlias('webroot') . '/uploads/invoicespdf')) {
            mkdir(Yii::getPathOfAlias('webroot') . '/uploads/invoicespdf');
            chmod((Yii::getPathOfAlias('webroot') . '/uploads/invoicespdf'), 0755);
            // the default implementation makes it under 777 permission, which you could possibly change recursively before deployment, but here's less of a headache in case you don't
        }
        return $pdf->Output(Yii::getPathOfAlias('webroot') . '/uploads/invoicespdf/' . $details['company'] . '-' . $details['invoice_ref'] . '.pdf');
    }

    public function invoiceTimesheet($id) {
        $model = QuickTimesheet::model()->findByPk($id);
        $model->status = "Emailed";
        $model->save();
        $contact_details = Contact::model()->findByPk($model->contact_id);
        $company = AppUser::getUserCompany();
        $details = AppTimeSheet::setInvoiceData($id, $model, $contact_details);
        $temp = AppTimeSheet::actionCreatepdf($details);
        $attachement = Yii::getPathOfAlias('webroot') . '/uploads/invoicespdf/' . $details ['company'] . '-' . $id . '.pdf';
//        $invoice = $this->createAbsoluteUrl('/uploads/invoicespdf/' . $details['company'] . '-' . $id . '.pdf');
        $msg = "Invoice";
        $mailer = new AppMailer();
        $mailer->prepareBody('invoice', array('INVOICE' => $msg));
        $response = $mailer->sendSesMailWithAttach(array('email' => $contact_details->email, 'name' => $contact_details->name), array('email' => 'support@speedypayrolls.com', 'name' => 'Support'), "Invoice Email", "Hello, <br/> Please find attachment.", $attachement, $company->name . '-' . $id . '.pdf');
        return

                $attachement;
    }

    public function deleteQuickTimesheet($id) {
        if (AppTimeSheet::deleteQuickTimesheet($id)) {
            $this->responseCode = HTTPCODE_SUCCESS;
            $this->results['Message'] = ConstantMessages::$timesheet_deleted;
        } else {
            $this->responseCode = HTTPCODE_FAILURE;
            $this->results['Message'] = ConstantMessages::$cannot_delete_timesheet;
        }
    }

    public function saveQuickTimesheet($formData, $q_id, $files) {
        $id = $q_id;
        if ($id != FLAG_OFF) { ///*** Should come from ???
            $model = QuickTimesheet::model()->findByPk($id);
        } else {
            $model = new QuickTimesheet();
        }
        if (isset($formData['QuickTimesheet'] ['date']) && isset($formData['QuickTimesheet'] ['job_title']) && isset($formData['QuickTimesheet']['job_reference']) && isset($formData['QuickTimesheet'] ['rate']) && isset($formData ['currency']) && isset($formData['hourly_total']) && isset($formData['QuickTimesheet']['total'])) {
            $contact = "";

            ///*** Refactor this method need serious attention

            if (isset($formData ['contacts']) && $formData ['contacts'] != 'out') {
                $contact = Contact::model()->findByAttributes(array('id' => $formData['contacts']));
                $select_email = $contact->email;
                $select_name = $contact->name;
                $contact_id = $formData['contacts'];
            } else {
                if (isset($formData['contact_email']) && isset($formData['contact_name'])) {
                    $select_email = $formData['contact_email'];
                    $select_name = $formData['contact_name'];

                    ///*** Handle Errors if coming from addContact
                    $contact = AppCompany::addContact(array('name' => $select_name, 'email' => $select_email,
                                'type' => EMPLOYEE_TYPE_INDIVIDUAL, 'company_id' => AppUser::getUserCompany()->id,
                                'created_by' => AppUser ::getUserId(), 'modified_by' => AppUser::getUserId(),
                                'created_at' => time(), 'modified_at' => time(), 'status' => FLAG_ON,
                                'address' => $formData['client_add'], 'country' => $formData['client_country'],
                                'post_code' => $formData['client_pc'], 'city' => $formData['client_city']));
                    ///***
                }
            }

            if ($contact instanceof Contact) {
                $contact_id = $contact->id;
//                if (AppCompany::getQuickTimesheetByDate(DateTime::createFromFormat('d/m/Y H:i:s', $formData['QuickTimesheet']['date'] . " 00:00:00")->getTimestamp())) {
//                    $this->responseCode = HTTPCODE_FAILURE;
//                    $this->results['Message'] = ConstantMessages::$timesheet_already_exist;
//                } else {
                $model = AppCompany::addQuickTimesheet($formData, $contact_id, $model);
                if ($model instanceof QuickTimesheet) {
                    if (isset($files['QuickTimesheet'])) {
                        if ($files['QuickTimesheet']['name']['reciept_file'][0]) {
                            AppCompany::saveQuickTimesheetReciepts($id, $model, $files);
                        }
                    }
                    AppCompany::saveQuickTimesheetExpense($id, $formData, $model);
                    $this->responseCode = HTTPCODE_CREATED;
                    $this->results['Result'] = $model;
                    $this->results['Message'] = ConstantMessages::$timesheet_saved;
                } else {
                    $this->responseCode = HTTPCODE_FAILURE;
                    $this->results['Message'] = ConstantMessages::$cannot_save_timesheet;
                    $this->results['Result'] = $model;
                }
//                }
            } else {
                $this->responseCode = HTTPCODE_FAILURE;
                $this->results['Message'] = ConstantMessages::$fields_missing;
            }
        } else {
            $this->responseCode = HTTPCODE_FAILURE;
            $this->results['Message'] = ConstantMessages::$fields_missing;
        }
    }

    public function addOutContract($formData) {
        $c = new Contract();
        $company_created = true;
        $current_user = AppUser::getCurrentUser();
        $super_user = AppUser::get_super_user();
        $c->attributes = $formData['Contract'];
        $company_id = 0;
        if (!empty($formData['cc']['company_name']) && !empty($formData['cc'] ['user_name']) && !empty($formData['cc']['user_email'])) {
            $pwd = User::model()->createRandomPassword();
            $formData['cc']['password'] = AppInterface::getHash($pwd);
            $formData['cc']['pwd'] = $pwd;
            $company = AppCompany::addDummyCompany($formData['cc']);
            if ($_POST['timesheet_approver'] == ConstantMessages::$timesheet_type_out) {
                $_POST['timesheet_approver'] = 'in_company';
                $approver_id = $company['user_id'];
            }
            if ($company != null && $company['company'] instanceof Company) {
                $company_id = $company['company']->id;
//Assign Plan and Privileges to the company
                $set_company_priviledges = AppCompany::setCompanyPlan($company_id, ConstantMessages::$default_privilege_id);
            } else {
                $this->results['Result'] = $company;
                AppLogging::addLog('Error in creating new company', 'error', 'webserviceController');
                $this->responseCode = HTTPCODE_FAILURE;
                $company_created = false;
            }
        }
        $payroller_id = 0;
        if (AppUser::canPayroll()) {
            $payroller_id = AppUser::getUserCompany($formData['Contract']['user_id'], false)->payroller_id;
        }
        if ($company_created) {
            $file_name = null;
            if (count($_FILES) > 0) {
                if ($_FILES['Contract']['name']['file_name']) {
                    $file_name = CUploadedFile:: getInstance($c, 'file_name');
                    $images_path = realpath(Yii::app()->basePath . '/../uploads/contracts');
                    $filename = explode('.', $_FILES['Contract']['name']['file_name']);
                    $filename[0] = AppInterface::getFilename();
                    $newfilename = $filename [0] . '.' . $filename[1];
                    $file_name->saveAs($images_path . '/' . $newfilename);
                    $file_name = $newfilename;
                }
            }
            $parent_id = 0;
            if ($parentcontract = AppContract::getInternalContract($c->user_id)) {
                $parent_id = $parentcontract->id;
            }

            if ($formData['timesheet_approver'] == 'out') {
                if (isset($formData['approver_email'])) {
                    $outapprover = User::prepareUserForAuthorisation($formData['approver_email']);
                    if ($outapprover) {
                        $approver_id = $outapprover->id;
                    } else {


                        $args = array(
                            'company_name' => $formData['approver_name'] . "'s Company",
                            'user_name' => $formData['approver_name'],
                            'user_email' => $formData['approver_email']
                        );

                        $dummyCompany = AppCompany::addDummyCompany($args);

                        if ($dummyCompany != null) {
                            $outapprover = User::prepareUserForAuthorisation($formData['approver_email']);
//Assign Plan and Privileges to the company
                            $set_company_priviledges = AppCompany::setCompanyPlan($dummyCompany->id, ConstantMessages::$default_privilege_id);
                            if ($outapprover != null)
                                $approver_id = $outapprover->id;
                        } else {
                            $this->results['Message'] = ConstantMessages::$approver_not_found;
                            AppLogging::addLog('No Approver Found ', 'error', 'webserviceController');
                            $this->responseCode = HTTPCODE_FAILURE;
                            $this->generateResponse();
                            die();
                        }
                    }
                } else {
                    $this->results['Message'] = ConstantMessages::$approver_not_found;
                    AppLogging::addLog('No Approver Found ', 'error', 'webserviceController');
                    $this->responseCode = HTTPCODE_FAILURE;
                    $this->generateResponse();
                    die();
                }
            } else {
                $approver_id = $formData['Contract']['approver_id'];
            }

            $contract1 = AppCompany::addContract(array('attributes' => $formData ['Contract'], 'user_id' => $c->user_id,
                        'company_id' => $company_id, 'role' => ConstantMessages:: $employee_type_employee, 'status' => ConstantMessages::$flag_on, 'start_time' => time(),
                        'end_time' => -1, 'type' => ConstantMessages:: $timesheet_type_out, 'created_by' => ConstantMessages::$flag_off, 'created_at' => time(), 'file_name' => $file_name,
                        'payroller_id' => $payroller_id,
                        'collection_type' => $formData['Contract']['collection_type'] == 1 ? ($formData['Settings']['collection_day_first'] == 1 ? 1 : 2) : 0,
                        'collection_day' => $formData['Contract']['collection_day'], 'modified_by' => ConstantMessages::$flag_off, 'modified_at' => time(),
                        'parent_id' => $parent_id, 'owner_id' => AppUser ::getUserId(), 'approver_id' => $approver_id));

            if (isset($approver_id)) {
                if (isset($contract1)) {
                    if (isset($formData['Payrate'])) {
                        foreach ($formData['Payrate'] as $name => $rate) {
                            AppCompany::addContractSetting(array('contract_id' => $contract1->id, 'key' => $name, 'value' => $rate, 'category' => 'payrate', 'created_by' => AppUser ::getUserId(), 'modified_by' => AppUser ::getUserId(), 'created_at' => time(), 'modified_at' => time()));
                        }
                    }

                    if (isset($formData['Item'])) {
                        foreach ($_POST['Item'] as $name => $rate) {
                            AppCompany::addContractSetting(array('contract_id' => $contract1->id, 'key' => $name, 'value' => $rate, 'category' => 'item', 'created_by' => AppUser ::getUserId(), 'modified_by' => AppUser ::getUserId(), 'created_at' => time(), 'modified_at' => time()));
                        }
                    }
                    $this->results['Message'] = 'Contract Out has been created, you can view contract of ' . $contract1->user->first_name;
                    AppLogging::addLog('Contract Out Created ', 'success', 'webserviceController');
                    $approver = User::model()->findByPk($contract1->approver_id);
                    $msg = 'You have been nominated by ' . $current_user->first_name . ' to become their Timesheet approver on SpeedyPayrolls.';

                    if (isset($dummyCompany)) {
                        $msg .= "<br/><br/><a href='" . Yii::app()->createAbsoluteUrl("/contract/main/inviteview", array('id' => $dummyCompany['company']->id)) . "'>View Contract";
                    }
                    $notification = Yii::app()->getModule('notification');

                    $not_result = AppInterface::notification($contract1->user_id, 9, 0, $contract1->id, $contract1->user_id);
                    $notifier = new Notification();
                    $notification_count = $notifier->getUserNotificationUnseenCount($contract1->user_id);
                    $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
                    $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;

                    $notification->send(array(
                        'email' => array(
                            'template' => 'Email',
                            'params' => array('BODY' => $msg),
                            'to' => array(array('email' => $approver->email, 'name' => $approver->first_name)),
                            'from' => array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => SYSTEM_NAME),
                            'cc' => array(),
                            'bcc' => array(),
                        ),
                        'push' => array('server_notification',
                            array('id' => $contract1->user_id,
                                'msg' => sprintf($not_result->notificationType->notice, $contract1->user->first_name),
                                'url' => $url, 'logo' => $logo, 'count' => $notification_count)
                        ))
                    );

                    $not_result = AppInterface::notification($contract1->company->admin->id, 13, 0, $contract1->id, $contract1->user_id);
                    $notification_count = $notifier->getUserNotificationUnseenCount($contract1->company->admin->id);
                    $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
                    $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;

                    $msg = 'Dear ' . $contract1->company->admin->first_name . ' ,<br/><br/>' . $contract1->user->first_name . ' has been added to your company.';
                    $notification->send(array(
                        'push' => array('server_notification',
                            array('id' => $contract1->company->admin->id,
                                'msg' => sprintf($not_result->notificationType->notice, $contract1->user->first_name, $contract1->company->name),
                                'url' => $url, 'logo' => $logo, 'count' => $notification_count)),
                        'email' => array(
                            'template' => 'Email',
                            'params' => array('BODY' => $msg),
                            'to' => array(array('email' => $contract1->company->admin->email, 'name' => $contract1->company->admin->first_name)),
                            'from' => array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => SYSTEM_NAME),
                            'cc' => array(),
                            'bcc' => array(),
                    )));

                    $msg = ConstantMessages::contractApprover();
                    $not_result = AppInterface::notification($approver->id, 3, 0, $contract1->id, $contract1->approver_id);
                    $notification_count = $notifier->getUserNotificationUnseenCount($approver->id);
                    $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
                    $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;

//email to employee
                    $employee = User::model()->findByPk($formData['Contract']['user_id']);
                    $msg = 'Dear ' . $employee->first_name . ' ,<br/><br/>Your new contract has been added.';
                    $notification->send(array(
                        'push' => array('server_notification',
                            array('id' => $approver_id,
                                'msg' => sprintf($not_result->notificationType->notice, $current_user->first_name),
                                'url' => $url,
                                'logo' => $logo,
                                'count' => $notification_count
                            )
                        ),
                        'email' => array(
                            'template' => 'Email',
                            'params' => array('BODY' => $msg),
                            'to' => array(array('email' => $employee->email, 'name' => $employee->first_name)),
                            'from' => array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls'),
                            'cc' => array(),
                            'bcc' => array(),
                    )));
                    $msg2 = $approver->first_name . ' has been nominated by ' . $current_user->full_name . ' to become timesheet approver for ' . $contract1->user->first_name . '.';

                    if (isset($dummyCompany)) {
                        $msg2 .= "<br/><br/><a href='" . Yii::app()->createAbsoluteUrl("/contract/main/inviteview", array('id' => $dummyCompany['company']->id)) . "'>View Contract";
                    }

                    $not_result = AppInterface::notification($super_user->id, 10, 0, $contract1->id, $contract1->user_id);
                    $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
                    $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;

                    $notification->send(array(
                        'email' => array(
                            'template' => 'Email',
                            'params' => array('BODY' => $msg2),
                            'to' => array(array('email' => $super_user->email, 'name' => $super_user->email)),
                            'from' => array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls'),
                            'cc' => array(),
                            'bcc' => array(),
                        ),
                        'push' => array('server_notification',
                            array('id' => $super_user->id,
                                'msg' => sprintf($not_result->notificationType->notice, $approver->first_name, $current_user->full_name, $contract1->user->first_name),
                                'url' => $url, 'logo' => $logo
                            )
                        )
                            )
                    );

//                    email to super admin
                    $msg3 = User::model()->findByPk($contract1->user_id)->first_name . ' has a new Speedy Timesheet account created by ' . $current_user->full_name;

                    $not_result = AppInterface::notification($super_user->id, 6, 0, $contract1->id, $contract1->user_id);
                    $notifier = new Notification();
                    $notification_count = $notifier->getUserNotificationUnseenCount($super_user->id);
                    $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
                    $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;

                    $notification->send(array(
                        'push' => array('server_notification',
                            array('id' => $super_user->id,
                                'msg' => sprintf($not_result->notificationType->notice, $contract1->user->first_name, $current_user->full_name),
                                'url' => $url, 'logo' => $logo, 'count' => $notification_count
                            )
                        ),
                        'email' => array(
                            'template' => 'Email',
                            'params' => array('BODY' => $msg3),
                            'to' => array(array('email' => $super_user->email, 'name' => $super_user->email)),
                            'from' => array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls'),
                            'cc' => array(),
                            'bcc' => array(),
                        )
                            )
                    );
                    $this->responseCode = HTTPCODE_SUCCESS;
                    $this->generateResponse();
                } else {
                    $this->responseCode = HTTPCODE_FAILURE;
                    $this->results['Message'] = ConstantMessages::$cannot_create_contract;
                    $this->generateResponse();
                }
            }
        }
    }

    public function addInContract($formData) {
//        dd($formData);
        $parent_id = '';
        $user_id = '';
//        dd($user_id);
        $contract1 = new Contract();
        $company_created = true;
        if (!empty($formData['cc']['company_name']) && !empty($formData['cc'] ['user_name']) &&
                !empty($formData['cc']['user_email'])) {

            $dummycompany = AppCompany::addCompanyWithAddress($_POST['cc'], FLAG_ON, FLAG_ON);

            if ($dummycompany instanceof Company) {
                AppUser::assignDefaultTemplate($dummycompany->id);
                $pwd = User::model()->createRandomPassword();
                $dummyuser = AppUser::addUser($formData['cc']['user_name'], $formData['cc'] ['user_email'], AppInterface :: getHash($pwd), '', '', FLAG_ON, FLAG_OFF, DEFAULT_PRIVILEGE_ID);

//Assign Plan and Privileges to the company
                $set_company_priviledges = AppCompany::setCompanyPlan($dummycompany->id, DEFAULT_PRIVILEGE_ID);

                if (isset($dummyuser)) {
                    $user_id = $dummyuser->id;
                    if ($_POST['timesheet_approver'] == TIMESHEET_TYPE_OUT) {
                        $_POST['timesheet_approver'] = APPROVER_IN_COMPANY;
                        $approver_id = $user_id;
                    }
//Add New Employee Contract
                    $contract = AppCompany::addContract(array('user_id' => $user_id,
                                'approver_id' => $dummyuser->id, 'company_id' => $dummycompany->id,
                                'created_at' => time(), 'created_by' => '0', 'modified_at' => time(),
                                'modified_by' => '0', 'start_time' => time(), 'end_time' => -1,
                                'status' => FLAG_OFF, 'type' => TIMESHEET_TYPE_IN,
                                'collection_type' => $_POST['Contract']['collection_type'] == 1 ? ($_POST['Settings']['collection_day_first'] == 1 ? 1 : 2) : 0, 'collection_day' => $_POST['Contract']['collection_day'], 'role' => ConstantMessages:: $employee_type_employee, 'is_internal' => ConstantMessages::$flag_on, 'owner_id' => $dummyuser->id));

                    $internalcontract = $contract;
                    $parent_id = $contract->id;
//Add New admin contract
                    $admincontract = AppCompany::addContract(array('user_id' => $user_id, 'approver_id' => $user_id, 'company_id' => $dummycompany->id, 'created_at' => time(), 'created_by' => '0', 'modified_at' => time(), 'modified_by' => '0', 'start_time' => time(), 'end_time' => -1, 'status' => ConstantMessages::$flag_on, 'type' => ConstantMessages:: $timesheet_type_in, 'collection_type' => $_POST['Contract']['collection_type'] == 1 ? ($_POST['Settings']['collection_day_first'] == 1 ? 1 : 2) : 0, 'collection_day' => $_POST['Contract']['collection_day'], 'role' => ConstantMessages:: $employee_type_admin, 'is_internal' => ConstantMessages::$flag_on, 'invitation_code' => User::createRandomPassword(20)));

                    $mailer = Yii::app()->getModule('notification');
                    $mailer->send(array('email' => array(
                            'template' => 'invite_admin',
                            'params' => array('NAME' => $dummyuser->first_name, 'INVITED_BY' => AppUser::getCurrentUser()->full_name, 'USERNAME' => $dummyuser->email, 'PASSWORD' => $pwd, 'LINK' => Yii::app()->createAbsoluteUrl('/user/main/login')),
                            'to' => array(array('email' => $dummyuser->email, 'name' => $dummyuser->first_name)),
                            'from' => array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls'),
                            'cc' => array(),
                            'bcc' => array(),
                    )));
                } else {
                    $this->results['Result'] = $dummyuser;
                    AppLogging::addLog('Error in creating new user', 'error', 'webserviceController');
                    $this->responseCode = HTTPCODE_FAILURE;
                    $company_created = false;
                }
            } else {
                $dummycompany = array('company' => $dummycompany);
                $this->results['Result'] = $dummycompany;
                AppLogging::addLog('Error in creating new company', 'error', 'webserviceController');
                $this->responseCode = HTTPCODE_FAILURE;
                $company_created = false;
            }
        } else {

            $internalcontract = AppContract::getInternalContract($formData['Contract']['user_id']);
            $user_id = $formData['Contract']['user_id'];
        }
        if ($company_created) {
            $file_name = null;
            if (count($_FILES) > 0) {
                if ($_FILES['Contract']['name']['file_name']) {
                    $file_name = CUploadedFile:: getInstance($contract1, 'file_name');
                    $images_path = realpath(Yii::app()->basePath . '/../uploads/contracts');
                    $filename = explode('.', $_FILES['Contract']['name']['file_name']);
                    $filename[0] = AppInterface::getFilename();
                    $newfilename = $filename [0] . '.' . $filename[1];
                    $file_name->saveAs($images_path . '/' . $newfilename);
                    $file_name = $newfilename;
                }
            }
            $payroller_id = 0;
            if (AppUser::canPayroll()) {
                $payroller_id = AppUser::getUserCompany($user_id, false)->payroller_id;
            }
            if ($formData['timesheet_approver'] == TIMESHEET_TYPE_OUT) {
                if (isset($formData['approver_email'])) {
                    $outapprover = User::prepareUserForAuthorisation($formData['approver_email']);
                    if ($outapprover) {
                        $approver_id = $outapprover->id;
                    } else {

                        $args = array(
                            'company_name' => $formData['approver_name'] . "'s Company",
                            'user_name' => $formData['approver_name'],
                            'user_email' => $formData['approver_email']
                        );

                        $dummyCompany = AppCompany::addDummyCompany($args);

                        if ($dummyCompany['company'] instanceof Company) {

//Assign Plan and Privileges to the company
                            $set_company_priviledges = AppCompany::setCompanyPlan($dummyCompany["company"]->id, DEFAULT_PRIVILEGE_ID);
                            $outapprover = User::prepareUserForAuthorisation($formData['approver_email']);
                            if ($outapprover != null)
                                $approver_id = $outapprover->id;
                        } else {
                            AppLogging::addLog('No Approver Found ', 'error', 'webserviceController');
                            $this->responseCode = HTTPCODE_FAILURE;
                            $this->results['Result'] = $dummyCompany;
                            $company_created = false;
                        }
                    }
                } else {
                    AppLogging::addLog('No Approver Found ', 'error', 'webserviceController');
                    $this->responseCode = HTTPCODE_FAILURE;
                    $company_created = false;
                }
            } else {
                $approver_id = $formData['Contract']['approver_id'];
            }

            if (isset($approver_id) && $company_created) {
                $contract1 = AppCompany::addContract(array('attributes' => $formData ['Contract'], 'user_id' => $user_id,
                            'company_id' => AppUser::getUserCompany()->id,
                            'role' => EMPLOYEE_TYPE_EMPLOYEE, 'status' => FLAG_ON, 'start_time' => time(),
                            'end_time' => -1, 'type' => TIMESHEET_TYPE_OUT, 'created_by' => FLAG_OFF,
                            'created_at' => time(), 'file_name' => $file_name,
                            'payroller_id' => $payroller_id,
                            'collection_type' => $formData['Contract']['collection_type'] == 1 ? ($formData['Settings']['collection_day_first'] == 1 ? 1 : 2) : 0,
                            'collection_day' => $formData['Contract']['collection_day'],
                            'modified_by' => FLAG_OFF, 'modified_at' => time(), 'parent_id' => $internalcontract->id,
                            'owner_id' => AppUser ::getUserId(), 'approver_id' => $approver_id));
                if (isset($contract1)) {
                    if (isset($_POST['Payrate'])) {
                        foreach ($_POST['Payrate'] as $name => $rate) {
                            AppCompany::addContractSetting(array('contract_id' => $contract1->id,
                                'key' => $name, 'value' => $rate,
                                'category' => 'payrate', 'created_by' => AppUser::getUserId(),
                                'modified_by' => AppUser ::getUserId(), 'created_at' => time(),
                                'modified_at' => time()));
                        }
                    }
                    if (isset($_POST['Item'])) {
                        foreach ($_POST['Item'] as $name => $rate) {
                            AppCompany::addContractSetting(array('contract_id' => $contract1->id, 'key' => $name,
                                'value' => $rate, 'category' => 'item',
                                'created_by' => AppUser ::getUserId(), 'modified_by' => AppUser::getUserId(),
                                'created_at' => time(), 'modified_at' => time()));
                        }
                    }

                    AppLogging::addLog('Contract In Created ', 'success', 'application.contract.controller.main');
                    $this->results['Message'] = 'Contract In has been created, you can view contract of ' . $contract1->user->first_name;
                    $approver = User::model()->findByPk($contract1->approver_id);
//                    email to timesheet approver
                    $msg = 'You have been nominated by ' . AppUser::getCurrentUser()->first_name . ' to become their Timesheet approver on SpeedyPayrolls.';

                    if (isset($dummyCompany)) {
                        $msg .= "<br/><br/><a href='" . Yii::app()->createAbsoluteUrl("/contract/main/inviteview", array('id' => $dummyCompany['company']->id)) . "'>View Contract";
                    }
                    $not_result = AppInterface::notification($contract1->user_id, 11, 0, $contract1->id, $contract1->user_id);
                    $notifier = new Notification();
                    $notification_count = $notifier->getUserNotificationUnseenCount();
                    $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
                    $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;

                    $notification = Yii::app()->getModule('notification');
                    $notification->send(array(
                        'email' => array(
                            'template' => 'Email',
                            'params' => array('BODY' => $msg),
                            'to' => array(array('email' => $approver->email, 'name' => $approver->first_name)),
                            'from' => array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => SYSTEM_NAME),
                            'cc' => array(),
                            'bcc' => array(),
                        ),
                        'push' => array('server_notification',
                            array('id' => $contract1->user_id,
                                'msg' => sprintf($not_result->notificationType->notice, $contract1->user->first_name),
                                'url' => $url, 'logo' => $logo, 'count' => $notification_count)
                        )
                            )
                    );

                    $not_result = AppInterface::notification(AppUser:: getUserId(), 14, 0, $contract1->id, AppUser::getUserId());
                    $notifier = new Notification();
                    $notification_count = $notifier->getUserNotificationUnseenCount();
                    $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
                    $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;

                    $notification->send(array(
                        'push' => array('server_notification',
                            array('id' => AppUser::getUserId(),
                                'msg' => sprintf($not_result->notificationType->notice, $contract1->user->first_name, $contract1->company->name),
                                'url' => $url, 'logo' => $logo, 'count' => $notification_count))));

                    $msg = ConstantMessages::contractApprover();
                    $not_result = AppInterface:: notification($approver_id, 3, 0, $contract1->id, $contract1->user_id);
                    $notifier = new Notification();
                    $notification_count = $notifier->getUserNotificationUnseenCount($approver_id);
                    $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
                    $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;

//                    email to super admin
                    $msg2 = $approver->first_name . ' has been nominated by ' . AppUser::getCurrentUser()->getfull_name() . ' to become timesheet approver for ' . $contract1->user->first_name . '.';
                    if (isset($dummyCompany)) {
                        $msg2 .= "<br/><br/><a href='" . Yii::app()->createAbsoluteUrl("/contract/main/inviteview", array('id' => $dummyCompany['company']->id)) . "'>View Contract";
                    }
                    $notification->send(array(
                        'email' => array(
                            'template' => 'Email',
                            'params' => array('BODY' => $msg2),
                            'to' => array(array('email' => AppUser:: get_super_user()->email, 'name' => AppUser::get_super_user()->email)),
                            'from' => array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => SYSTEM_NAME),
                            'cc' => array(),
                            'bcc' => array(),
                        ),
                        'push' => array('server_notification',
                            array('id' => $approver_id,
                                'msg' => sprintf($not_result->notificationType->notice, AppUser::getCurrentUser()->first_name),
                                'url' => $url, 'logo' => $logo, 'count' => $notification_count))
                    ));

                    $not_result = AppInterface::notification(AppUser:: get_super_user()->id, 10, 0, $contract1->id, $contract1->user_id);
                    $notifier = new Notification();
                    $notification_count = $notifier->getUserNotificationUnseenCount(AppUser::get_super_user()->id);
                    $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
                    $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;

                    $notification->send(array(
                        'push' => array('server_notification',
                            array('id' => AppUser::get_super_user()->id,
                                'msg' => sprintf($not_result->notificationType->notice, $approver->first_name, AppUser::getCurrentUser()->getfull_name(), $contract1->user->first_name),
                                'url' => $url, 'logo' => $logo, 'count' => $notification_count
                            )
                        )
                            )
                    );

                    $this->responseCode = HTTPCODE_SUCCESS;
                } else {
                    $this->results['Message'] = ConstantMessages::$fields_missing;
                    $this->responseCode = HTTPCODE_FAILURE;
                }
            }
        }
    }

    public function addInOutContract($formData) {
        $result = array();
        $company_created = true;
        if (!empty($formData['cc2']['company_name']) && !empty($formData['cc2'] ['user_name']) && !empty($formData['cc2']['user_email'])) {
            $pwd = User::model()->createRandomPassword();
            $formData['cc2']['password'] = AppInterface::getHash($pwd);
            $formData['cc2']['pwd'] = $pwd;
            $lookgCompany = AppCompany::addDummyCompany($formData['cc2']);
            if ($lookgCompany != null && $lookgCompany['company'] instanceof Company) {
//Assign Plan and Privileges to the company
                $set_company_priviledges = AppCompany::setCompanyPlan($lookgCompany->id, DEFAULT_PRIVILEGE_ID);

                if ($formData['approver_id'] == '') {
                    $formData['approver_id'] = $lookgCompany['user_id'];
                }
            } else {
                $lookgCompany = array('company' => $lookgCompany);
                $this->results['Result'] = $lookgCompany;
                AppLogging::addLog('Error in creating new company', 'error', 'webserviceController');
                $this->responseCode = HTTPCODE_FAILURE;
                $company_created = false;
            }
        }
        $contract = new Contract ();
        if ($company_created) {
            if (!empty($formData['cc']['company_name']) && !empty($formData['cc'] ['user_name']) && !empty($formData['cc']['user_email'])) {
                $dummycompany = AppCompany::addCompanyWithAddress($formData['cc'], FLAG_ON, FLAG_OFF);

                if (isset($dummycompany) && $dummycompany instanceof Company) {
                    AppUser::assignDefaultTemplate($dummycompany->id);

//Assign Plan and Privileges to the company
                    $set_company_priviledges = AppCompany::setCompanyPlan($dummycompany->id, DEFAULT_PRIVILEGE_ID);

                    $pwd = User::model()->createRandomPassword();
                    $dummyuser = AppUser::addUser($formData['cc']['user_name'], $formData['cc'] ['user_email'], AppInterface :: getHash($pwd), '', '', FLAG_ON, FLAG_OFF, DEFAULT_PRIVILEGE_ID);

                    if (isset($dummyuser)) {
                        $user_id = $dummyuser->id;
//Add Employee Contract
                        $contract = AppCompany::addContract(array('user_id' => $user_id, 'approver_id' => $user_id,
                                    'company_id' => $dummycompany->id, 'created_at' => time(), 'created_by' => '0',
                                    'modified_at' => time(), 'modified_by' => '0', 'start_time' => time(), 'end_time' => -1,
                                    'status' => FLAG_OFF, 'type' => TIMESHEET_TYPE_IN,
                                    'collection_type' => $formData['Contract2']['collection_type'] == 1 ? ($formData['Settings']['collection_day_first'] == 1 ? 1 : 2) : 0, 'collection_day' => $formData['Contract2']['collection_day'], 'role' => ConstantMessages:: $employee_type_employee, 'is_internal' => ConstantMessages::$flag_on, 'owner_id' => $dummyuser->id));

                        $internalcontract = $contract;
                        $parent_id = $contract->id;

//Add Admin Contract
                        $admincontract = AppCompany::addContract(array('user_id' => $user_id, 'approver_id' => $user_id, 'company_id' => $dummycompany->id, 'created_at' => time(), 'created_by' => '0', 'modified_at' => time(), 'modified_by' => '0', 'start_time' => time(), 'end_time' => -1, 'status' => ConstantMessages::$flag_off, 'type' => ConstantMessages:: $timesheet_type_in, 'collection_type' => $formData['Contract2']['collection_type'] == 1 ? ($formData['Settings']['collection_day_first'] == 1 ? 1 : 2) : 0, 'collection_day' => $formData['Contract2']['collection_day'], 'role' => ConstantMessages:: $employee_type_admin, 'is_internal' => ConstantMessages::$flag_on, 'invitation_code' => User::createRandomPassword(20)));
                        $file_name = null;
                        if (count($_FILES) > 0) {
                            if ($_FILES['Contract']['name']['file_name']) {
                                $file_name = CUploadedFile:: getInstance($contract, 'file_name');
                                $images_path = realpath(Yii::app()->basePath . '/../uploads/contracts');
                                $filename = explode('.', $_FILES['Contract']['name']['file_name']);
                                $filename[0] = AppInterface::getFilename();
                                $newfilename = $filename [0] . '.' . $filename[1];
                                $file_name->saveAs($images_path . '/' . $newfilename);
                                $file_name = $newfilename;
                            }
                        }
                        $url = $this->createAbsoluteUrl('/user/main/signup') . '/invite/' . $admincontract->invitation_code . '/contract/' . $admincontract->id;
                        $mailer = Yii::app()->getModule('notification');
                        $mailer->send(array('email' => array(
                                'template' => 'invite_admin',
                                'params' => array('NAME' => $dummyuser->first_name, 'INVITED_BY' => AppUser::getCurrentUser()->full_name, 'USERNAME' => $dummyuser->email, 'PASSWORD' => $pwd, 'LINK' => Yii::app()->createAbsoluteUrl('/user/main/login')),
                                'to' => array(array('email' => $dummyuser->email, 'name' => $dummyuser->first_name)),
                                'from' => array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => SYSTEM_NAME),
                                'cc' => array(),
                                'bcc' => array(),
                        )));
                    } else {
                        $this->results['Result'] = $dummyuser;
                        AppLogging::addLog('Error in creating new user', 'error', 'webserviceController');
                        $this->responseCode = HTTPCODE_FAILURE;
                        $company_created = false;
                    }
                } else {
                    $dummycompany = array('company' => $dummycompany);
                    $this->results['Result'] = $dummycompany;
                    AppLogging::addLog('Error in creating new company', 'error', 'webserviceController');
                    $this->responseCode = HTTPCODE_FAILURE;
                    $company_created = false;
                }
            } else {
                $internalcontract = AppContract::getInternalContract($formData['Contract']['user_id']);
                $user_id = $formData['Contract']['user_id'];
            }
            if ($company_created) {
                $payroller_id = 0;
                if (AppUser::canPayroll()) {
                    $payroller_id = AppUser::getUserCompany($user_id, false)->payroller_id;
                }

                if (count($_FILES) > 0) {
                    if ($_FILES['Contract']['name']['file_name']) {
                        $file_name = CUploadedFile:: getInstance($contract, 'file_name');
                        $images_path = realpath(Yii::app()->basePath . '/../uploads/contracts');
                        $filename = explode('.', $_FILES['Contract']['name']['file_name']);
                        $filename[0] = AppInterface::getFilename();
                        $newfilename = $filename [0] . '.' . $filename[1];
                        $file_name->saveAs($images_path . '/' . $newfilename);
                        $file_name = $newfilename;
                    }
                }

//Add Employee Contract
                $contract2 = AppCompany::addContract(array('user_id' => $user_id, 'policy' => $formData['Contract']['policy'], 'file_name' => (isset($file_name)) ? $file_name : "", 'approver_id' => $formData['Contract']['approver_id'], 'company_id' => AppUser:: getUserCompany()->id, 'created_at' => time(), 'created_by' => '0', 'modified_at' => time(), 'modified_by' => '0', 'start_time' => time(), 'end_time' => -1, 'status' => 'active', 'type' => 'in_out', 'role' => 'Employee', 'is_internal' => 1, 'owner_id' => AppUser ::getUserId(), 'parent_id' => $internalcontract->id, 'payroller_id' => $payroller_id));

                if (isset($lookgCompany) && $lookgCompany != null)
                    $company_id = $lookgCompany['company']->id;
                else
                    $company_id = $formData['Contract2']['company_id'];
                $payroller_id2 = 0;
                if (AppUser::canPayroll()) {
                    $payroller_id2 = AppUser::getUserCompany($user_id, false)->payroller_id;
                }
                $contract3 = AppCompany::addContract(array('user_id' => $user_id, 'approver_id' => $formData['Contract']['approver_id'], 'company_id' => $company_id, 'created_at' => time(), 'created_by' => '0', 'modified_at' => time(), 'modified_by' => '0', 'start_time' => time(), 'end_time' => -1, 'status' => 'pending', 'type' => 'out_b', 'role' => 'Employee', 'is_internal' => 0, 'owner_id' => AppUser ::getUserId(), 'parent_id' => $contract2->id, 'payroller_id' => $payroller_id2, 'collection_type' => $formData['Contract2']['collection_type'] == 1 ? ($formData['Settings']['collection_day_first'] == 1 ? 1 : 2) : 0, 'collection_day' => $formData['Contract2']['collection_day']));

                if (isset($contract2) && isset($contract3)) {

                    if (isset($formData['Payrate1'])) {
                        foreach ($formData['Payrate1'] as $name => $rate) {
                            AppCompany::addContractSetting(array('contract_id' => $contract2->id, 'key' => $name, 'value' => $rate, 'category' => 'payrate', 'created_by' => AppUser ::getUserId(), 'modified_by' => AppUser ::getUserId(), 'created_at' => time(), 'modified_at' => time()));
                        }
                    }
                    if (isset($formData['Item1'])) {
                        foreach ($formData['Item1'] as $name => $rate) {
                            AppCompany::addContractSetting(array('contract_id' => $contract2->id, 'key' => $name, 'value' => $rate, 'category' => 'item', 'created_by' => AppUser ::getUserId(), 'modified_by' => AppUser ::getUserId(), 'created_at' => time(), 'modified_at' => time()));
                        }
                    }

                    if (isset($formData['Payrate2'])) {
                        foreach ($formData['Payrate2'] as $name => $rate) {
                            AppCompany::addContractSetting(array('contract_id' => $contract3->id, 'key' => $name, 'value' => $rate, 'category' => 'payrate', 'created_by' => AppUser ::getUserId(), 'modified_by' => AppUser ::getUserId(), 'created_at' => time(), 'modified_at' => time()));
                        }
                    }
                    if (isset($formData['Item2'])) {
                        foreach ($formData['Item2'] as $name => $rate) {
                            AppCompany::addContractSetting(array('contract_id' => $contract3->id, 'key' => $name, 'value' => $rate, 'category' => 'item', 'created_by' => AppUser ::getUserId(), 'modified_by' => AppUser ::getUserId(), 'created_at' => time(), 'modified_at' => time()));
                        }
                    }
                    $approver = User::model()->findByPk($contract2->approver_id);
//                email to approver
                    $msg = 'You have been nominated by ' . AppUser::getCurrentUser()->first_name . ' to become their Timesheet approver on SpeedyPayrolls.';

                    $not_result = AppInterface::notification($contract2->user_id, 8, 0, $contract2->id, $contract2->user_id);
                    $notifier = new Notification();
                    $notification_count = $notifier->getUserNotificationUnseenCount();
                    $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
                    $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;

                    $notification = Yii::app()->getModule('notification');
                    $notification->send(array(
                        'email' => array(
                            'template' => 'Email',
                            'params' => array('BODY' => $msg),
                            'to' => array(array('email' => $approver->email, 'name' => $approver->first_name)),
                            'from' => array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => SYSTEM_NAME),
                            'cc' => array(),
                            'bcc' => array(),
                        ),
                        'push' => array('server_notification',
                            array('id' => $contract2->user_id,
                                'msg' => sprintf($not_result->notificationType->notice, $contract2->user->first_name),
                                'url' => $url, 'logo' => $logo, 'count' => $notification_count
                            )
                        )
                            )
                    );

                    $not_result = AppInterface::notification($approver->id, 3, 0, $contract2->id, $contract2->user_id);
                    $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
                    $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;
                    $notifier = new Notification();
                    $notification_count = $notifier->getUserNotificationUnseenCount($contract2->approver_id);
                    $msg = ConstantMessages::contractApprover();

//                email to super admin
                    $msg2 = $approver->first_name . ' has been nominated by ' . User::model()->getfull_name() . ' to become timesheet approver for ' . User::model()->findByPk($contract2->user_id)->first_name . '.';
                    $notification->send(array(
                        'email' => array(
                            'template' => 'Email',
                            'params' => array('BODY' => $msg2),
                            'to' => array(array('email' => AppUser:: get_super_user()->email, 'name' => AppUser::get_super_user()->email)),
                            'from' => array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => SYSTEM_NAME),
                            'cc' => array(),
                            'bcc' => array(),
                        ),
                        'push' => array('server_notification',
                            array('id' => $contract2->approver_id,
                                'msg' => sprintf($not_result->notificationType->notice, AppUser::getCurrentUser()->first_name),
                                'url' => $url, 'logo' => $logo)
                        )
                            )
                    );

                    $not_result = AppInterface::notification(AppUser:: get_super_user()->id, 10, 0, $contract2->id, $contract2->user_id);
                    $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
                    $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;
                    $notifier = new Notification();
                    $notification_count = $notifier->getUserNotificationUnseenCount(AppUser::get_super_user()->id);

//                email to user
                    $msg3 = User::model()->findByPk($contract2->user_id)->first_name . ' has a new Speedy Timesheet account created by ' . AppUser::getCurrentUser()->full_name . 'Please login to your SpeedyPayrolls account for further information.';
                    $notification->send(array(
                        'email' => array(
                            'template' => 'Email',
                            'params' => array('BODY' => $msg3),
                            'to' => array(array('email' => User::model()->findByPk($contract2->user_id)->email, 'name' => User::model()->findByPk($contract2->user_id)->first_name)),
                            'from' => array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => SYSTEM_NAME),
                            'cc' => array(),
                            'bcc' => array(),
                        ),
                        'push' => array('server_notification',
                            array('id' => AppUser::get_super_user()->id,
                                'msg' => sprintf($not_result->notificationType->notice, $approver->first_name, AppUser:: getCurrentUser()->full_name, $contract2->user->first_name),
                                'url' => $url, 'logo' => $logo, 'count' => $notification_count
                            )
                        )
                            )
                    );

                    $this->results['Message'] = 'Contract Out has been created, you can view contract of ' . $contract2->user->first_name;
                    AppLogging::addLog('Contract In-Out Created ', 'success', 'webserviceController');
                    $this->responseCode = HTTPCODE_SUCCESS;
                    $this->generateResponse();
                } else {
                    $this->results['Message'] = ConstantMessages::$fields_missing;
                    $this->responseCode = HTTPCODE_FAILURE;
                    $this->generateResponse();
                }
            }
        }
    }

}
