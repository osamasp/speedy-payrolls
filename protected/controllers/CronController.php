<?php

class CronController extends Controller {

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function actionMissingEntries() {
        $user = AppUser::getCurrentUser();
        $model = UserTimings::model()->findAllByAttributes(array("user_id" => $user->id, "created_at" => strtotime(date('d/m/Y')), "time_out" => 0));
        if (count($model) > 0) {
            $msg = "Dear " . $user->first_name . ",<br>It is to notify you that you forgot to time out for yesterday timesheet.<br>You are requested to update your timesheet.<br><br>Thanks";
            Yii::app()->user->setFlash('danger', $msg);
            $not_result = AppInterface::notification($user->id, 2, $model[0]->id, $model[0]->contract_id, $model[0]->user_id);
            $notifier = new Notification();
            $notification_count = $notifier->getUserNotificationUnseenCount();
            $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
            $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;
            $notification = Yii::app()->getModule('notification');
            $notification->send(
                    array(
                        'email' =>
                        array('template' => 'Email', 'params' => array('BODY' => $msg),
                            'to' => array(array('email' => $user->email, 'name' => $user->first_name)),
                            'from' => array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls')
                        ),
                        'push' => array('server_notification',
                            array('id' => $user->id,
                                'msg' => sprintf($not_result->notificationType->notice, $user->first_name), 'url' => $url, 'logo' => $logo, 'count' => $notification_count)
                        )
            ));
        }
        $this->redirect(Yii::app()->createAbsoluteUrl('site/index'));
    }

    public function sendEmailToUsersNotClockIn() {
        $result = Yii::app()->db->createCommand("SELECT DISTINCT u.* FROM sp_user u 
                                                INNER JOIN sp_contract c ON u.id=c.user_id 
                                                INNER JOIN sp_company_shifts s ON s.id = u.shift_id
                                                WHERE c.is_internal=1
                                                AND FROM_UNIXTIME(s.start_time, '%H:%i:%s')<FROM_UNIXTIME(CURTIME(),'%H:%i:%s') AND u.id 
                                                NOT IN(SELECT sp_user_timings.user_id FROM sp_user_timings 
                                                WHERE created_at=" . strtotime(date('m/d/Y')) . ")")->queryAll();
        foreach ($result as $item) {
            dd($item);
            $mailer = new AppMailer();
            $msg = "Dear " . $item['first_name'] . ",<br>It is to notify you that you forgot to time in for today's timesheet.<br>You are requested to update your timesheet.<br><br>Thanks";
            $mailer->prepareBody('Email', array('BODY' => $msg));
            $mailer->sendMail(array(array('email' => $item['email'], 'name' => $item['first_name'])), array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls'));
        }
    }

    public function actionTimesheetMap() {
//        dd(strtotime(date('m/d/Y'))." ".strtotime("last monday"));
        $clocks = UserTimings::model()->findAllBySql("SELECT * FROM sp_user_timings WHERE created_at BETWEEN " . strtotime('last monday') . " AND " . strtotime(date('m/d/Y')));
//        dd($clocks);
        foreach ($clocks as $item) {
            $model = new Timesheet();
            $model->id = $item->id;
            $model->contract_id = $item->contract_id;
            $model->type = 'daily';
            $model->start_time = $item->time_in;
            $model->end_time = $item->time_out;
            $model->time = $item->total_time;
            $model->lunchtime = $item->total_lunch;
            if ($item->status == 'approved')
                $model->status = 'approved';
            else if ($item->status == 'pending')
                $model->status = 'pending';
            else
                $model->status = 'in-approval';
            $model->created_at = time();
            $model->created_by = 1;
            $model->modified_by = 1;
            $model->modified_at = time();
            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'Timesheet inserted successfully');
                AppLogging::addLog("Timesheet added", 'success', 'application.timesheet.main');
            } else {
                dd($model->errors);
            }
            $this->redirect(Yii::app()->baseUrl . "/site/index");
        }
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionSendTimeCard() {

        $criteria = new CDbCriteria();
        $criteria->addCondition("t.STATUS IN (:status,:status2)");
        $criteria->params = array(":status" => "active", ":status2" => "pending");
        $contracts = Contract::model()->findAll($criteria);

        foreach ($contracts as $contract) {
            $currentTime = strtotime('today');
            $currentDate = date('m/d/Y');
            $time = null;

            switch ($contract->collection_type) {

                case 0:    // Weekly Type
                    $time = AppTimeSheet::getStEnDateWeekly($contract->collection_day, $currentDate);

                    break;
                default: // First & Last Week Of Every Month 
                    $time = AppTimeSheet::getStEnDateMonthly($contract->collection_type, $contract->collection_day, $currentDate);

                    break;
            }




            /*             * ** Check Contract Completion & Start ** */
            if ($currentTime >= $contract->start_time && $currentTime < $contract->end_time && $contract->status = "pending") {
                $contract->status = "active";
                $contract->update();
            } else if ($currentTime > $contract->end_time && $contract->status = "active") {

                $contract->status = "completed";
                $contract->update();
            }
            /*             * ***** */



            $criteria = new CDbCriteria();
            $criteria->addCondition(' t.contract_id = :contractId AND parent is NULL');
            $criteria->addCondition(' t.status IN (:status,:status2)');
            $criteria->params = array(":contractId" => $contract->id, ":status" => "pending", ":status2" => "current");

            $tSheets = Timesheet::model()->findAll($criteria);

//                           mail('usama.ayaz@live.com','Speedy TimeSheet Found for processing',"Total TSHEETS = ".count($tSheets));

            foreach ($tSheets as $sheets) {

//                                    if($sheets!=null && $currentTime>$sheets->end_time && (isset($time['end']) && $currentDate>=date('m/d/Y',strtotime($time['end']))) ){
                if ($sheets != null && $currentTime > $sheets->end_time) {
                    AppLogging::addLog("Timesheet has been sent for Approval", "success", "application.cron.sendTimeCard");
                    /*                     * * Email Notification ** */
                    AppTimeSheet::notifyUsersInApproval($sheets);

                    $sheets->status = 'in-approval';
                    $sheets->modified_at = $currentTime;
                    $sheets->update();
                }
            }
        }
    }

    public function actionUpdateContract() {

        $criteria = new CDbCriteria();
        $criteria->addCondition("t.next_cf_date > 0 AND t.next_cf_date <= :currentTime");
        $criteria->params = array(':currentTime' => time());
        $contracts = Contract::model()->findAll($criteria);

        foreach ($contracts as $contract) {

            $contract->collection_day = $contract->next_cf_day;
            $contract->collection_type = $contract->next_cf_type;
            $contract->next_cf_day = "";
            $contract->next_cf_day = "";
            $contract->next_cf_date = 0;
            $contract->update();
        }
    }

}
