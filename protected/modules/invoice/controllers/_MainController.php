<?php
            @set_magic_quotes_runtime(false);
            ini_set('magic_quotes_runtime', 0);
class MainController extends Controller {

    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'send', 'create', 'createpdf', 'delete', 'getdata', 'new', 'ses','sendInvoice'),
                'roles' => array('admin', 'super_admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $c = new CDbCriteria();
        $c->join = " LEFT JOIN sp_contact c on t.contact_id = c.id";
        //$c->join .= " LEFT JOIN sp_contract con on ts.contract_id = con.id";
        $c->addCondition('c.company_id = :cid');
        $c->params = array(
            ':cid' => AppUser::getUserCompany()->id,
        );
        $invoices = Invoice::model()->findAll($c);
        $this->renderHomePageJs();
        AppLogging::addLog('Contact list for invoice ', 'success', 'application.invoice.controller.main');
        $this->render('list', array(
            'dataProvider' => $invoices,
        ));
    }
    
    public function actionSes()
    {
            $attachement = Yii::getPathOfAlias('webroot').'/uploads/invoicespdf/Some Company Ltd-112122ab.pdf';
            $mailer = new AppMailer();
            
            //dd($attachement);
            $mailer->sendSesMailWithAttach(array('email' => 'usama8890@hotmail.com', 'name' => 'Usama'), array('email' => 'usama8890@hotmail.com', 'name' => 'Usama'), "Invoice Email","Hello" , $attachement);
            
    }

    public function actionCreate() {
        if (isset($_POST['Invoice'])) {
            $invoice = new Invoice;
            $invoice->id = AppInterface::getUniqueId();
            $invoice->attributes = $_POST['Invoice'];
            $details = AppTimeSheet::compileCardDetail($_POST['Invoice']['timesheet_id']);
            $details['canSendInvoice'] = false;
            $invoice->html = $this->renderPartial('timesheet', $details, true);
            $invoice->status = 1;
            $invoice->created_by = AppUser::getUserId();
            $invoice->modified_by = AppUser::getUserId();
            $invoice->created_at = time();
            $invoice->modified_at = time();
            $invoice->timesheet_id = $_POST['Invoice']['timesheet_id'];
            if($_POST['Invoice']['contact_id']!=null)
            {
                $invoice->contact_id = $_POST['Invoice']['contact_id'];
            }
            else
            {
                $invoice->contact_id = '0';
            }
            
//            if (isset($details['payHours']) && count($details['payHours']) > 0) {
//                foreach ($details['payHours'] as $key => $payRate) {
//                    $details['payHours'] = $payRate;
//                    $details['payRates'] = (float)($details['payRates'] ? $details['payRates'][$key] : '');
//                }
//            }
//            
//            if (isset($details['itemCount']) && count($details['itemCount']) > 0) {
//                foreach ($details['itemCount'] as $key => $item) {
//                    $details['itemCount'] = $item;
//                    $details['itemRates'] = (float)($details['itemRates'] ? $details['itemRates'][$key] : '');
//                }
//            }
//            
//            if (isset($details['payRates2']) && isset($details['itemRates2'])) {
//                if (isset($payHours) && count($payHours) > 0) {
//                foreach ($payHours as $key => $payRate) { 
//                    $details[$key.' hours'] = $payRate;
//                    $details['payRates2'] = ($payRates2 ? $payRates2[$key] : '');
//                    }
//                }
//                if (isset($itemCount) && count($itemCount) > 0) {
//                 foreach ($itemCount as $key => $item) { 
//                    $details[$key] = $item;
//                    $details['itemRates2'] = ($itemRates2 ? $itemRates2[$key] : '');
//                    }
//                }
//            }
//            
//            $details['entry'] = 0;
//            foreach($details as $key => $value)
//            {   
//                $invoice_att = new InvoiceAttributes();
//                $invoice_att->id = AppInterface::getUniqueId();
//                $invoice_att->attributes = $details;
//                $invoice_att->attribute_key = $key;
//                $invoice_att->attribute_value = $value;
//                $invoice_att->timesheet_id = $invoice->timesheet_id;
//                $invoice_att->is_basic = 1;
//                $invoice_att->item_value = 0;
//                $invoice_att->pay_rate = 0;
//                $invoice_att->save();
//                
//            }
            
            if ($invoice->save()) {
                $this->redirect(array('index'));
            } else {
                Yii::app()->user->setFlash('error', 'An error occured while creating invoice');
                $this->redirect(array('index'));
            }
        }
    }
    
    public function actionSendInvoice($timesheet=null,$id=null)
    {
        $company = AppUser::getUserCompany();
        $contacts = AppCompany::getCompanyContacts();
        $details = array();
        $invoice_items = array();
        $contact_id = "";
        if(!empty($timesheet))
        {
            $details = AppTimeSheet::compileCardDetail($timesheet);
            $details['logo'] = $company['logo'];
        }
        elseif(!empty($id))
        {
            $invoice_items = InvoiceItems::model()->findByAttributes(array('invoice_id'=>$id));
            $temp = Invoice::model()->findByPk($id);
            $details = AppTimeSheet::compileCardDetail($temp->timesheet_id);
            $details['invoice_att'] = InvoiceItems::model()->findAll('invoice_id = '.$id);
            $timesheet = $temp->timesheet_id;
            $details['logo'] = $company['logo'];
        }
        
        if(count($details)<1)
        {
            $details['company'] = $company['name'];
            $details['companyAddress'] = $company['address'];
            $details['post_code'] = $company['post_code'];
            $details['companyCity'] = $company['city'];
            $details['companyCountry'] = $company['country'];
            $details['logo'] = $company['logo'];
        }
        if(Yii::app()->getRequest()->isPostRequest)
        {
            if(!empty($_POST['timesheet_id']))
            {
                $details = AppTimeSheet::compileCardDetail($_POST['timesheet_id']);
            }
            elseif(!empty($_POST['invoice_id']))
            {
                $temp = Invoice::model()->findByPk($_POST['invoice_id']);
                $details = AppTimeSheet::compileCardDetail($temp->timesheet_id);
            }
            
            $details['logo'] = $company['logo'];
            $details['date'] = $_POST['date'];
            if(empty($details['logo']))
            {
                $details['logo'] = "";
            }
            if (isset($_POST['contacts']) && $_POST['contacts'] != 'out') {
                $select_email = Contact::model()->findByAttributes(array('id' => $_POST['contacts']))->email;
                $select_name = Contact::model()->findByAttributes(array('id' => $_POST['contacts']))->name;
                $details['client_info'] = Contact::model()->findByAttributes(array('id' => $_POST['contacts']));
//                dd($select_email);
                $contact_id = $_POST['contacts'];
            } else {
                $select_email = $_POST['contact-email'];
                $select_name = $_POST['contact-name'];
                $contact = new Contact;
                $contact->id = AppInterface::getUniqueId();
                $contact->name = $select_name;
                $contact->email = $select_email;
                $contact->type = 'Individual';
                $contact->company_id = AppUser::getUserCompany()->id;
                $contact->created_by = AppUser::getUserId();
                $contact->created_at = time();
                $contact->modified_by = AppUser::getUserId();
                $contact->modified_at = time();
                $contact->status = 1;
                $contact->address = $_POST['client_add'];
                $contact->country = $_POST['client_country'];
                $contact->post_code = $_POST['client_pc'];
                
                if ($contact->save()) {
                    $contact_id = $contact->id;
                    $details['client_info'] = Contact::model()->findByAttributes(array('id' => $contact->id));
                } else {
                    Yii::app()->user->setFlash('error', 'An error occured while creating invoice');
                    $this->redirect(array('index'));
                }
            }
            
            
            $invoice = new Invoice();
                $invoice->id = AppInterface::getUniqueId();
            if(!empty($details['employeeName']))
            {
                $invoice->html = $this->renderPartial('timesheet', $details, true);
            }
            else
            {
                $invoice->html = "";
            }
            $invoice->contact_id = $contact_id;
            $invoice->status = 1;
            $invoice->created_by = AppUser::getUserId();
            $invoice->modified_by = AppUser::getUserId();
            $invoice->created_at = time();
            $invoice->modified_at = time();
            if(empty($company['logo'])){
                $invoice->logo_url = "";
            }
            else{
                $invoice->logo_url = $company['logo'];
            }
            $invoice->invoice_reference = $_POST['invoice_ref'];
            $invoice->sub_total = $_POST['subtotal'];
            $invoice->vat = $_POST['vat'];
            $invoice->total = $_POST['total'];
            $invoice->company_id = $company->id;
            $invoice->contractor_name = $_POST['contractor_name'];
            $invoice->contractor_address = $_POST['contractor_address'];
            $invoice->vat_number = $_POST['vat_number'];
            $details['invoice_ref'] = $_POST['invoice_ref'];
            $details['subtotal'] = $_POST['subtotal'];
            $details['vat'] = $_POST['vat'];
            $details['total'] = $_POST['total'];
            $details['contractor_name'] = $_POST['contractor_name'];
            $details['contractor_address'] = $_POST['contractor_address'];
            $details['vat_number'] = $_POST['vat_number'];
            $details['invoice_to'] = $select_name;
            if($_POST['timesheet_id']!=null)
            {
                $invoice->timesheet_id = $_POST['timesheet_id'];
            }
            else
            {
                $invoice->timesheet_id = null;
            }
            
            
        if($invoice->save())
        {
            for($x=0;$x<$_POST['count'];$x++) 
            {
            $invoiceItems = new InvoiceItems();
            $invoiceItems->item = $_POST['item'.$x];
            $invoiceItems->rate = $_POST['rate'.$x];
            $invoiceItems->cost = $_POST['cost'.$x];
            $invoiceItems->unit = $_POST['unit'.$x];
            $invoiceItems->id = AppInterface::getUniqueId();
            $invoiceItems->invoice_id = $invoice->id;
            $invoiceItems->save();
            $details['invoice_items'][$x] = $invoiceItems;
            }
            $user = AppUser::getCurrentUser();
            $details['user'] = $user;
            $temp = $this->actionCreatepdf($details);
            $attachement = Yii::getPathOfAlias('webroot').'/uploads/invoicespdf/'.$details['company'].'-'.$details['invoice_ref'].'.pdf';
            $mailer = new AppMailer();
            $msg = "Invoice sent from ". $user['first_name'] ." ". $user['last_name'];
            $mailer->prepareBody('invoice', array('INVOICE' => $msg));
            //dd($attachement);
            $mailer->sendMailWithAttach(array('email' => $select_email, 'name' => $select_name), array('email' => $user['email'], 'name' => $user['first_name'] ." ". $user['last_name']), $attachement);
            Yii::app()->user->setFlash('success', 'Invoice sent to selected user.');
            $this->redirect(array('index'));
        }
        else 
        {
            Yii::app()->user->setFlash('error', 'An error occured while creating invoice');
            $this->redirect(array('index'));
        }
        }
        $this->render('sendinvoice',array('details' => $details,'company' => $company,'contacts' => $contacts,'invoice_items'=>$invoice_items,'timesheet_id'=>$timesheet,'invoice_id'=>$id));
        
    }
    
        public function actionCreatepdf($details) {
        $pdf = new SPPDF();
        $pdf->details = $details;
        $pdf->AddPage();
        
        $pdf->SetTextColor(0, 0, 0);
        
//        $pdf->SetDrawColor(26,26,26);
        $pdf->SetLineWidth(0.5);
        $pdf->Line(11,45,200,45);
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->SetFont("Arial","B","13");
        $pdf->Write("4", "Client:");
        $pdf->SetFont("Arial","","12");
        $pdf->Cell('0', '0', $details['date'], '', '0', 'R');
        $pdf->Cell('0','7','','','1');
	$pdf->Write("4",$details['client_info']['name']);
        $pdf->Cell("0", "5", "", "", "1");
        $pdf->Write("4", $details['client_info']['address']);
        $pdf->Cell("0", "5", "", "", "1");
        $pdf->Write("4", $details['client_info']['country']);
        $pdf->Cell("0", "5", "", "", "1");
        $pdf->Write("4", $details['client_info']['post_code']);
         $pdf->Cell("0", "5", "", "", "1");
        $pdf->Write("4","VAT NO. :". $details['vat_number']);
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->Write("4","Invoice No. :". $details['invoice_ref']);
        $pdf->Cell("0", "10", "", "", "1");
        //$pdf->Write("4", "Order No: ");
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->SetFont("Arial", "B", "12");
        $pdf->Cell('0', '10', "", "", "1");
        $pdf->Cell('80', '10', 'Services','', '0', 'L');
        $pdf->Cell('30', '10', 'Unit Price','', '0', 'C');
        $pdf->Cell('30', '10', 'Quantity','', '0', 'C');
        $pdf->Cell('40', '10', 'Total (excl. VAT)', '', '0', 'R');
        $pdf->Cell('0', '10', "", "", "1");
        $pdf->SetFont("Arial", "", "12");
        $pdf->SetLineWidth(0.6);
        $pdf->Line(11,145,190,145);
        $ylength = 145;
        foreach ($details['invoice_items'] as $item)
        {
            $pdf->Cell('80', '10', $item->item, "", "0",'L');
            $pdf->Cell('30', '10', $item->unit, "", "0",'C');
            $pdf->Cell('30', '10', $item->rate, "", "0",'C');
            $pdf->Cell('40', '10', $item->cost, "", "0",'R');
            $pdf->Cell('0', '10', "", "", "1");
            $ylength += 10;
        }
//loop
        $pdf->SetLineWidth(0.6);
        $pdf->Line(11,$ylength+2,190,$ylength+2);
        $pdf->Cell('80', '10', '','', '0', 'L');
        $pdf->Cell('30', '10', '','', '0', 'C');
        $pdf->Cell('30','10','Sub Total','','0','C');
        $pdf->Cell('40','10',$details['subtotal'],'','0','R');
        $pdf->Cell('0', '10', "", "", "1");
        $pdf->Cell('80', '10', '','', '0', 'L');
        $pdf->Cell('30', '10', '','', '0', 'C');
        $pdf->Cell('30','10','VAT','','0','C');
        $pdf->Cell('40','10',$details['vat'],'','0','R');
        $pdf->Cell('0', '10', "", "", "1");
        $pdf->Cell('80', '10', '','', '0', 'L');
        $pdf->Cell('30', '10', '','', '0', 'C');
        $pdf->Cell('30', '10', 'Order Total','', '0', 'C');
        $pdf->Cell('40', '10', $details['total'], '', '0', 'R');
        
        $pdf->SetFont("Arial","", "11");
        $pdf->Cell('0', '10', "", "", "1");
        $pdf->Cell('0', '10', "", "", "1");
        $pdf->Write("10", "Thanks For Your Purchase!");
        $pdf->Cell('0', '10', "", "", "1");
        $pdf->Write("10", "For any other qustions do not hesitate to contact us!");
        $pdf->Cell('0', '10', "", "", "1");
        
        // make the directory to store the pic:
        if(!is_dir(Yii::getPathOfAlias('webroot').'/uploads/invoicespdf')) {
            mkdir(Yii::getPathOfAlias('webroot').'/uploads/invoicespdf');
            chmod((Yii::getPathOfAlias('webroot').'/uploads/invoicespdf'), 0755); 
            // the default implementation makes it under 777 permission, which you could possibly change recursively before deployment, but here's less of a headache in case you don't
        }
        //dd($pdf->Output(Yii::getPathOfAlias('webroot').'/uploads/invoicespdf/'.$details['company'].'-'.$details['invoice_ref'].'.pdf'));
  //      dd(Yii::getPathOfAlias('webroot').'/uploads/invoicespdf/'.$details['company'].'-'.$details['invoice_ref'].'.pdf');
        return $pdf->Output(Yii::getPathOfAlias('webroot').'/uploads/invoicespdf/'.$details['company'].'-'.$details['invoice_ref'].'.pdf');
        }

    public function actionNew($timesheet = '', $id = '') {
//        dd($id);
         $company = AppUser::getUserCompany();
        $contacts = AppCompany::getCompanyContacts();
        if (isset($_POST)) {
            $count = 0;
            $obj = array();
            $payHoursArr = array();
            $payRatesArr = array();
//        dd($_POST['city']);
            if (isset($_POST['payrate_name'])) {
                foreach ($_POST['payrate_name'] as $data) {
                    $payHoursArr[$data] = $_POST['payrate_set_value'][$count];
                    $payRatesArr[$data] = $_POST['payrate_value'][$count];

                    //$obj = array('Units' => $_POST['payrate_set_value'][$count]);
                    //$obj += array('Rates' => $_POST['payrate_value'][$count]);
                    //$obj += array('Cost' => $_POST['total'][$count]);
                    //$obs[$data] = $obj;

                    $count++;
                }
                $obs['payHours'] = $payHoursArr;
                $obs['payRates'] = $payRatesArr;

                $obs1 = array('companyCity' => $_POST['city']);
                $obs1 += array('companyCountry' => $_POST['country']);
                $obs1 += array('post_code' => $_POST['post_code']);
                $obs1 += array('Invoice' => $_POST['invoice_ref']);

                $obs2 = array_merge($obs1, $obs);

                //  dd(json_encode($obs2));
//                dd(json_encode($obs1));
                if (isset($_POST['contacts']) && $_POST['contacts'] != 'out') {
                    $select_email = Contact::model()->findByAttributes(array('id' => $_POST['contacts']))->email;
                    $select_name = Contact::model()->findByAttributes(array('id' => $_POST['contacts']))->name;
//                dd($select_email);
                    $contact_id = $_POST['contacts'];
                } else {
                    $select_email = $_POST['contact-email'];
                    $select_name = $_POST['contact-name'];
                    $contact = new Contact;
                    $contact->id = AppInterface::getUniqueId();
                    $contact->name = $select_name;
                    $contact->email = $select_email;
                    $contact->type = 'Individual';
                    $contact->company_id = AppUser::getUserCompany()->id;
                    $contact->created_by = AppUser::getUserId();
                    $contact->created_at = time();
                    $contact->modified_by = AppUser::getUserId();
                    $contact->modified_at = time();
                    $contact->status = 1;
                    if ($contact->save()) {
                        $contact_id = $contact->id;
                    } else {
                        Yii::app()->user->setFlash('error', 'An error occured while creating invoice');
                        $this->redirect(array('index'));
                    }
                }

//        dd(json_encode($obs));
                $invoice = new Invoice();
                $invoice->id = AppInterface::getUniqueId();
                $invoice->timesheet_id = $timesheet;
                $invoice->contact_id = $contact_id;
                $invoice->html = json_encode($obs2);
                $invoice->status = 1;
                $invoice->created_at = time();
                $invoice->created_by = AppUser::getUserId();
                $invoice->modified_by = AppUser::getUserId();
                $invoice->modified_at = time();
                $invoice->save();
//         dd($obs);
                if ($invoice->save()) {
                    $user = AppUser::getCurrentUser();
//                $mailer = new AppMailer();
//                $mailer->prepareBody('invoice', array('INVOICE' => $msg));
//                $mailer->sendMail(array($select_email, $user->email, 'name' => 'SpeedyPayrolls'));
//                $mailer->sendMail(array(array('email' => $select_email, 'name' => $select_name)), array('email' => $user->email, 'name' => $user->full_name));
                    Yii::app()->user->setFlash('success', 'Invoice sent to selected user.');
                    $this->redirect(array('index'));
                } else {
                    Yii::app()->user->setFlash('error', 'An error occured while creating invoice');
                    $this->redirect(array('index'));
                }
            }
        }

        $details = array();
        $timeSheetData = '';
        $timesheet_id = null;
        if ($timesheet) {
            $details = AppTimeSheet::compileCardDetail($timesheet);
//            dd(json_encode($details));
//            dd($details->logo);
//            $timeSheetData = $this->renderPartial('new', $details);
            $timesheet_id = $timesheet;
//        dd($details);
        } else if (isset($id)) {
//            dd($id);
            if ($temp = Invoice::model()->findByPk($id)) {
//                $timeSheetData = json_decode($temp->html);
//                 dd($id);                 
                $timesheet_id = $temp->timesheet_id;
                $details = get_object_vars(json_decode($temp->html));
//                dd($details);
                $this->render('new1', array('details' => $details,
                    'contacts' => $contacts));
                die();
//                dd($details['entry']->payHours);
//                $this->render('new' ,array( 'detail'=> $timeSheetData ));
            }
        }

        $this->render('new', array('details' => $details,
            'company' => $company,
            'contacts' => $contacts
        ));
    }

    public function actionSend($timesheet = '', $id = '') {
        $timeSheetData = '';
        $timesheet_id = null;
        if ($timesheet) {
            $details = AppTimeSheet::compileCardDetail($timesheet);
//            dd($details);
            $timeSheetData = $this->renderPartial('timesheet', $details, true);
            $timesheet_id = $timesheet;
        } else if ($id) {
            if ($temp = Invoice::model()->findByPk($id)) {
                $timeSheetData = $temp->html;
                $timesheet_id = $temp->timesheet_id;
            }
        }

        if (isset($_POST['email-text'])) {

            $msg = $_POST['email-text'];

            if (isset($_POST['contacts']) && $_POST['contacts'] != 'out') {
                $select_email = Contact::model()->findByAttributes(array('id' => $_POST['contacts']))->email;
                $select_name = Contact::model()->findByAttributes(array('id' => $_POST['contacts']))->name;
//                dd($select_email);
                $contact_id = $_POST['contacts'];
            } else {
                $select_email = $_POST['contact-email'];
                $select_name = $_POST['contact-name'];
                $contact = new Contact;
                $contact->id = AppInterface::getUniqueId();
                $contact->name = $select_name;
                $contact->email = $select_email;
                $contact->type = 'Individual';
                $contact->company_id = AppUser::getUserCompany()->id;
                $contact->created_by = AppUser::getUserId();
                $contact->created_at = time();
                $contact->modified_by = AppUser::getUserId();
                $contact->modified_at = time();
                $contact->status = 1;
                if ($contact->save()) {
                    $contact_id = $contact->id;
                } else {
                    Yii::app()->user->setFlash('error', 'An error occured while creating invoice');
                    $this->redirect(array('index'));
                }
            }
//            dd($_POST);
            $invoice = new Invoice;
            $invoice->id = AppInterface::getUniqueId();
            $invoice->html = $msg;
            $invoice->contact_id = $contact_id;
            $invoice->timesheet_id = $timesheet_id;
            $invoice->status = 1;
            $invoice->created_by = AppUser::getUserId();
            $invoice->modified_by = AppUser::getUserId();
            $invoice->created_at = time();
            $invoice->modified_at = time();
            if ($invoice->save()) {
                $user = AppUser::getCurrentUser();
                $mailer = new AppMailer();
                $mailer->prepareBody('invoice', array('INVOICE' => $msg));
//                $mailer->sendMail(array($select_email, $user->email, 'name' => 'SpeedyPayrolls'));
                $mailer->sendMail(array(array('email' => $select_email, 'name' => $select_name)), array('email' => $user->email, 'name' => $user->full_name));
                $this->redirect(array('index'));
            } else {
                //dd($invoice->errors);
                Yii::app()->user->setFlash('error', 'An error occured while creating invoice');
                $this->redirect(array('index'));
            }

            AppLogging::addLog('Invoice Email', 'success', 'application.invoice.controller.main');
            Yii::app()->user->setFlash('success', 'Invoice sent to the recipient.');
        }
        $contacts = AppCompany::getCompanyContacts();
//               dd($contacts);
        $this->render('index', array(
            'contacts' => $contacts,
            'timeSheetData' => $timeSheetData,
        ));
    }

    public function actionGetData() {
        $this->layout = "//layouts/blank";
        $response = array('status' => false, 'data' => '', 'reason' => 'Not a Valid Ajax Request or Post Data is Missing');
        if (Yii::app()->getRequest()->isAjaxRequest && Yii::app()->getRequest()->getPost("entryId") != "") {
            $invoice = Invoice::model()->findByPk(Yii::app()->getRequest()->getPost("entryId"));
            //$details = AppTimeSheet::compileCardDetail(Yii::app()->getRequest()->getPost("entryId"));
            if ($invoice) {
                $response['status'] = true;
                $response['reason'] = '';
                $response['data'] = $this->renderPartial('_view', array('data' => $invoice->html), true);
            } else {
                $response['status'] = false;
                $response['reason'] = "No Record Found";
            }
        }

        AppSetting::generateAjaxResponse($response);
    }

    public function renderHomePageJs() {

        //$getChildEntriesUrl = $this->createUrl("/timesheet/main/GetSubEntries");
        $getSheetDetailUrl = $this->createUrl("/invoice/main/GetData");
        //$startTime = (Yii::app()->getRequest()->getPost('timeIn'))?Yii::app()->getRequest()->getPost('timeIn'):'09:00 AM';
        //$endTime = (Yii::app()->getRequest()->getPost('timeOut'))?Yii::app()->getRequest()->getPost('timeOut'):'06:00 PM';

        $script = <<<EOL
            jQuery(document).ready(function(){
                        
                                    
                        $('.ajaxModel').on('show.bs.modal', function (event) {
                                    
                                    
                            var button = $(event.relatedTarget) // Button that triggered the modal
                            var recipient = button.data('whatever') // Extract info from data-* attributes
                            var modal = $(this)
                              $.ajax({
                                    type: "POST",
                                    url: '{$getSheetDetailUrl}',
                                    data: {'entryId':recipient},
                                    beforeSend:function(jqXHR,settings){
                                            jQuery(modal).find('.modal-content').text("Loading..........");
                                            
                                    },
                                    success: function(response){
                                        if(response.status==true){
                                            jQuery(modal).find('.modal-content').html(response.data);
                                        }else{
                                             alert(response.reason);
                                    
                                        }
                                    },
                                    dataType: 'json'
                                  });        
                                                             
                          })            
            });
           
           
EOL;

        Yii::app()->clientScript->registerScript('timeSheetHome-js', $script, CClientScript::POS_END);
    }

    public function actionDelete($id) {
        if ($invoice = Invoice::model()->findByPk($id)) {
            $invoice->delete();
            $this->redirect(array('index'));
        } else {
            Yii::app()->user->setFlash('error', 'Unable to delete the invoice');
            $this->redirect(array('index'));
        }
    }

}