<?php
if (version_compare(PHP_VERSION, '5.3.0', '<')) {
            @set_magic_quotes_runtime(false);
            ini_set('magic_quotes_runtime', 0);
}
class MainController extends Controller {

    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'send', 'create', 'invoice', 'createpdf', 'delete', 'invoicedelete', 'sesattach', 'ses', 'getdata', 'new','sendInvoice'),
                'roles' => array('admin', 'super_admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    
    public function actionSesAttach($toId='usama8890@hotmail.com')
    {
        
            $attachement = Yii::getPathOfAlias('webroot').'/uploads/invoicespdf/usama ltd-1121.pdf';
            $mailer = new AppMailer();
            
            $response = $mailer->sendSesMailWithAttach(array('email' => $toId, 'name' => 'Usama'), array('email' => 'support@speedypayrolls.com', 'name' => 'Support'), "Invoice Email","Hello" , $attachement, "usama ltd-1121.pdf");
        
            print_r($response);
            print_r(Yii::app()->ses->getSendQuota());
            print_r(Yii::app()->ses->getSendStatistics());
    }
    
    public function actionSes($toId='szohaibnajam@gmail.com')
    {
         $response = Yii::app()->ses->email()
		->addTo($toId)
		->setFrom('support@speedypayrolls.com')
		->setSubject('Hello, world!')
		->setMessageFromString("", "Test Message") //Pass a string body and html body
		->setSubjectCharset('ISO-8859-1')
		->setMessageCharset('ISO-8859-1')
	->send();
	print_r($response);
        print_r(Yii::app()->ses->getSendQuota());
        print_r(Yii::app()->ses->getSendStatistics());
    }

    public function actionIndex() {
        $c = new CDbCriteria();
        $c->join = " LEFT JOIN sp_contact c on t.contact_id = c.id";
        $c->addCondition('c.company_id = :cid');
        $c->order = "t.created_at DESC";
        $c->params = array(
            ':cid' => AppUser::getUserCompany()->id,
        );
        $invoices = Invoice::model()->findAll($c);
        AppLogging::addLog('Contact list for invoice ', 'success', 'application.invoice.controller.main');
        $this->render('list', array(
            'dataProvider' => $invoices,
        ));
    }
    
    public function actionCreate() {
        if (isset($_POST['Invoice'])) {
            $details = AppTimeSheet::compileCardDetail($_POST['Invoice']['timesheet_id']);
            $details['canSendInvoice'] = false;
            $html = $this->renderPartial('timesheet', $details, true);
            $invoice = AppCompany::addInvoice(array('attributes'=>$_POST['Invoice'],'html'=>$html,
                'status'=>1,'created_by'=>AppUser::getUserId(),'modified_by'=>AppUser::getUserId(),
                'created_at'=>time(),'modified_at'=>time(),'timesheet_id'=>$_POST['Invoice']['timesheet_id'],
                'contact_id'=>$_POST['Invoice']['contact_id'], 'date' => date('d/m/Y')));
            if (isset($invoice)) {
                $this->redirect(array('index'));
            } else {
                Yii::app()->user->setFlash('error', 'An error occured while creating invoice');
                $this->redirect(array('index'));
            }
        }
    }
    
    public function actionSendInvoice($timesheet=null,$id=null)
    {
        $company = AppUser::getUserCompany();
        $contacts = AppCompany::getCompanyContacts();
        $details = array();
        $invoice_items = array();
        $contact_id = "";
        if(!empty($timesheet))
        {
            $details = AppTimeSheet::compileCardDetail($timesheet);
            $details['logo'] = $company['logo'];
        }
        elseif(!empty($id))
        {
            $invoice_items = InvoiceItems::model()->findByAttributes(array('invoice_id'=>$id));
            $temp = Invoice::model()->findByPk($id);
            $details = AppTimeSheet::compileCardDetail($temp->timesheet_id);
            $details['invoice_att'] = InvoiceItems::model()->findAll('invoice_id = '.$id);
            $timesheet = $temp->timesheet_id;
            $details['logo'] = $company['logo'];
        }
        
        if(count($details)<1)
        {
            $details['company'] = $company['name'];
            $details['companyAddress'] = $company['address'];
            $details['post_code'] = $company['post_code'];
            $details['companyCity'] = $company['city'];
            $details['companyCountry'] = $company['country'];
            $details['logo'] = $company['logo'];
        }
        if(Yii::app()->getRequest()->isPostRequest)
        {
            if(!empty($_POST['timesheet_id']))
            {
                $details = AppTimeSheet::compileCardDetail($_POST['timesheet_id']);
            }
            elseif(!empty($_POST['invoice_id']))
            {
                $temp = Invoice::model()->findByPk($_POST['invoice_id']);
                $details = AppTimeSheet::compileCardDetail($temp->timesheet_id);
            }
            
            $details['logo'] = $company['logo'];
            $details['date'] = $_POST['date'];
            if(empty($details['logo']))
            {
                $details['logo'] = "";
            }
            if (isset($_POST['contacts']) && $_POST['contacts'] != 'out') {
                $select_email = Contact::model()->findByAttributes(array('id' => $_POST['contacts']))->email;
                $select_name = Contact::model()->findByAttributes(array('id' => $_POST['contacts']))->name;
                $details['client_info'] = Contact::model()->findByAttributes(array('id' => $_POST['contacts']));
                $contact_id = $_POST['contacts'];
            } else {
                $select_email = $_POST['contact-email'];
                $select_name = $_POST['contact-name'];
                $contact = AppCompany::addContact(array('name'=>$select_name,'email'=>$select_email,'type'=>'Individual','company_id'=>  AppUser::getUserCompany()->id,'created_by'=>  AppUser::getUserId(),'modified_by'=>  AppUser::getUserId(),'created_at'=>time(),'modified_at'=>time(),'status'=>1,'address'=>$_POST['client_add'],'country'=>$_POST['client_country'],'post_code'=>$_POST['client_pc'],'city'=>$_POST['client_city']));
                
                
                if (isset($contact)) {
                    $contact_id = $contact->id;
                    $details['client_info'] = Contact::model()->findByAttributes(array('id' => $contact->id));
                } else {
                    Yii::app()->user->setFlash('error', 'An error occured while creating invoice');
                    $this->redirect(array('index'));
                }
            }
            if($_POST['timesheet_id']!=null)
            {
                $timesheet_id = $_POST['timesheet_id'];
            }
            else
            {
                $timesheet_id = null;
            }
            if(!empty($details['employeeName']))
            {
                $html = $this->renderPartial('timesheet', $details, true);
            }
            else
            {
                $html = "";
            }
            if(empty($company['logo'])){
                $logo = "";
            }
            else{
                $logo = $company['logo'];
            }
            $invoice = AppCompany::addInvoice(array('html'=>$html,'contact_id'=>$contact_id,
                                            'status'=>1,'created_by'=>  AppUser::getUserId(),
                                            'modified_by'=>  AppUser::getUserId(),'created_at'=>time(),
                                            'modified_at'=>time(),'logo'=>$logo,'invoice_ref'=>$_POST['invoice_ref'],
                                            'sub_total'=>$_POST['subtotal'],'vat'=>$_POST['vat'],
                                            'total'=>$_POST['total'],'company_id'=>$company->id,
                                            'contractor_name'=>$_POST['contractor_name'],
                                            'contractor_address'=>$_POST['contractor_address'],'vat_number'=>$_POST['vat_number'],
                                            'timesheet_id'=>$timesheet_id, 'date' => $details['date']));
            
            $details['invoice_ref'] = $_POST['invoice_ref'];
            $details['subtotal'] = $_POST['subtotal'];
            $details['vat'] = $_POST['vat'];
            $details['total'] = $_POST['total'];
            $details['contractor_name'] = $_POST['contractor_name'];
            $details['contractor_address'] = $_POST['contractor_address'];
            $details['vat_number'] = $_POST['vat_number'];
            $details['invoice_to'] = $select_name;
            
        if(isset($invoice))
        {
            for($x=0;$x<$_POST['count'];$x++) 
            {
            $invoiceItems = new InvoiceItems();
            $invoiceItems->item = $_POST['item'.$x];
            $invoiceItems->rate = $_POST['rate'.$x];
            $invoiceItems->cost = $_POST['cost'.$x];
            $invoiceItems->unit = $_POST['unit'.$x];
            $invoiceItems->id = AppInterface::getUniqueId();
            $invoiceItems->invoice_id = $invoice->id;
            $id = $invoiceItems->model()->create($invoiceItems->id, $invoiceItems->item, $invoiceItems->unit, $invoiceItems->rate, $invoiceItems->cost, $invoiceItems->invoice_id);
            $details['invoice_items'][$x] = $invoiceItems;
            
            }
            $user = AppUser::getCurrentUser();
            $details['user'] = $user;
            $temp = $this->actionCreatepdf($details);
            $attachement = Yii::getPathOfAlias('webroot').'/uploads/invoicespdf/'.$details['company'].'-'.$details['invoice_ref'].'.pdf';
            $file_name = $details['company'].'-'.$details['invoice_ref'].'.pdf';
            $invoice_pdf = $this->createUrl('/uploads/invoicespdf/'.$details['company'].'-'.$details['invoice_ref'].'.pdf');
//            $mailer = new AppMailer();
//            $msg = "Invoice sent from ". $user['first_name'] ." ". $user['last_name'];
//            $mailer->prepareBody('invoice', array('INVOICE' => $msg));
//            
//            $file_name = $details['company'].'-'.$details['invoice_ref'].'.pdf';
//            $response = $mailer->sendSesMailWithAttach(array('email' => $select_email, 'name' => $select_name), array('email' => 'support@speedypayrolls.com', 'name' => 'Support'), "Invoice Email","Hello" , $attachement, $file_name);
//            Yii::app()->user->setFlash('success', 'Invoice sent to selected user.');
            $this->layout = "//layouts/blank";
            $response = array('status' => false, 'data' => '', 'reason' => 'Not a Valid Ajax Request or Post Data is Missing');
            $response['status'] = true;
        $response['reason'] = '';

        $response['data'] = $this->render('viewinvoice',array('attachement' => $attachement,'select_email' => $select_email,'select_name' => $select_name,'file_name' => $file_name,'invoice_pdf' => $invoice_pdf),true);
        AppSetting::generateAjaxResponse($response);
            return;
        }
        else 
        {
            Yii::app()->user->setFlash('error', 'An error occured while creating invoice');
            $this->redirect(array('index'));
        }
        }
        $this->render('sendinvoice',array('details' => $details,'company' => $company,'contacts' => $contacts,'invoice_items'=>$invoice_items,'timesheet_id'=>$timesheet,'invoice_id'=>$id));
        
    }
    
    public function actionInvoice($attachement,$select_email,$select_name,$file_name)
    {
        $user = AppUser::getCurrentUser();
        $mailer = new AppMailer();
            $msg = "Invoice sent from ". $user['first_name'] ." ". $user['last_name'];
            $mailer->prepareBody('invoice', array('INVOICE' => $msg));
            
            $response = $mailer->sendSesMailWithAttach(array('email' => $select_email, 'name' => $select_name), array('email' => 'support@speedypayrolls.com', 'name' => 'Support'), "Invoice Email","Hello" , $attachement, $file_name);
            Yii::app()->user->setFlash('success', 'Invoice sent to selected user.');
            $this->redirect(array('index'));
    }
    
        public function actionCreatepdf($details) {
        $pdf = new SPPDF();
        $pdf->details = $details;
        $pdf->AddPage();
        
        $pdf->SetTextColor(0, 0, 0);
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->SetFont("Arial","B","13");
        $pdf->Write("4", "Client:");
        $pdf->SetFont("Arial","","12");
        $pdf->Cell('0', '0', $details['date'], '', '0', 'R');
        $pdf->Cell('0','7','','','1');
	$pdf->Write("4",$details['client_info']['name']);
        $pdf->Cell("0", "5", "", "", "1");
        $pdf->Write("4", $details['client_info']['address']);
        $pdf->Cell("0", "5", "", "", "1");
        $pdf->Write("4",$details['client_info']['city'] ." ". $details['client_info']['post_code']);
        $pdf->Cell("0", "5", "", "", "1");
        $pdf->Write("4", $details['client_info']['country']);
         $pdf->Cell("0", "5", "", "", "1");
        $pdf->Write("4","VAT NO. :". $details['vat_number']);
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->Write("4","Invoice No. :". $details['invoice_ref']);
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->SetFont("Arial", "B", "12");
        $pdf->Cell('0', '10', "", "", "1");
        $pdf->Cell('80', '10', 'Services','', '0', 'L');
        $pdf->Cell('30', '10', 'Unit Price','', '0', 'C');
        $pdf->Cell('30', '10', 'Quantity','', '0', 'C');
        $pdf->Cell('40', '10', 'Total (excl. VAT)', '', '0', 'R');
        $pdf->Cell('0', '10', "", "", "1");
        $pdf->SetFont("Arial", "", "12");
        $pdf->SetLineWidth(0.6);
        $pdf->Line(11,145,190,145);
        $ylength = 145;
        foreach ($details['invoice_items'] as $item)
        {
            $pdf->Cell('80', '10', $item->item, "", "0",'L');
            $pdf->Cell('30', '10', $item->unit, "", "0",'C');
            $pdf->Cell('30', '10', $item->rate, "", "0",'C');
            $pdf->Cell('40', '10', $item->cost, "", "0",'R');
            $pdf->Cell('0', '10', "", "", "1");
            $ylength += 10;
        }
//loop
        $pdf->SetLineWidth(0.6);
        $pdf->Line(11,$ylength+2,190,$ylength+2);
        $pdf->Cell('80', '10', '','', '0', 'L');
        $pdf->Cell('30', '10', '','', '0', 'C');
        $pdf->Cell('30','10','Sub Total','','0','C');
        $pdf->Cell('40','10',$details['subtotal'],'','0','R');
        $pdf->Cell('0', '10', "", "", "1");
        $pdf->Cell('80', '10', '','', '0', 'L');
        $pdf->Cell('30', '10', '','', '0', 'C');
        $pdf->Cell('30','10','VAT','','0','C');
        $pdf->Cell('40','10',$details['vat'],'','0','R');
        $pdf->Cell('0', '10', "", "", "1");
        $pdf->Cell('80', '10', '','', '0', 'L');
        $pdf->Cell('30', '10', '','', '0', 'C');
        $pdf->Cell('30', '10', 'Order Total','', '0', 'C');
        $pdf->Cell('40', '10', $details['total'], '', '0', 'R');
        
        $pdf->SetFont("Arial","", "11");
        $pdf->Cell('0', '10', "", "", "1");
        $pdf->Cell('0', '10', "", "", "1");

        // make the directory to store the pic:
        if(!is_dir(Yii::getPathOfAlias('webroot').'/uploads/invoicespdf')) {
            mkdir(Yii::getPathOfAlias('webroot').'/uploads/invoicespdf');
            chmod((Yii::getPathOfAlias('webroot').'/uploads/invoicespdf'), 0755); 
            // the default implementation makes it under 777 permission, which you could possibly change recursively before deployment, but here's less of a headache in case you don't
        }
        return $pdf->Output(Yii::getPathOfAlias('webroot').'/uploads/invoicespdf/'.$details['company'].'-'.$details['invoice_ref'].'.pdf');
        }

    public function actionNew($timesheet = '', $id = '') {
         $company = AppUser::getUserCompany();
        $contacts = AppCompany::getCompanyContacts();
        if (isset($_POST)) {
            $count = 0;
            $obj = array();
            $payHoursArr = array();
            $payRatesArr = array();
            if (isset($_POST['payrate_name'])) {
                foreach ($_POST['payrate_name'] as $data) {
                    $payHoursArr[$data] = $_POST['payrate_set_value'][$count];
                    $payRatesArr[$data] = $_POST['payrate_value'][$count];

                    $count++;
                }
                $obs['payHours'] = $payHoursArr;
                $obs['payRates'] = $payRatesArr;

                $obs1 = array('companyCity' => $_POST['city']);
                $obs1 += array('companyCountry' => $_POST['country']);
                $obs1 += array('post_code' => $_POST['post_code']);
                $obs1 += array('Invoice' => $_POST['invoice_ref']);

                $obs2 = array_merge($obs1, $obs);

                if (isset($_POST['contacts']) && $_POST['contacts'] != 'out') {
                    $select_email = Contact::model()->findByAttributes(array('id' => $_POST['contacts']))->email;
                    $select_name = Contact::model()->findByAttributes(array('id' => $_POST['contacts']))->name;
                    $contact_id = $_POST['contacts'];
                } else {
                    $select_email = $_POST['contact-email'];
                    $select_name = $_POST['contact-name'];
                    $contact = AppCompany::addContact(array('name'=>$select_name,'email'=>$select_email,'type'=>'Individual','company_id'=>  AppUser::getUserCompany()->id,'created_by'=>  AppUser::getUserId(),'modified_by'=>  AppUser::getUserId(),'created_at'=>time(),'modified_at'=>time(),'status'=>1));
                    
                    if (isset($contact)) {
                        $contact_id = $contact->id;
                    } else {
                        Yii::app()->user->setFlash('error', 'An error occured while creating invoice');
                        $this->redirect(array('index'));
                    }
                }
                $invoice = AppCompany::addInvoice(array('timesheet_id'=>$timesheet,'contact_id'=>$contact_id,'html'=>  json_encode($obs2),'status'=>1,'created_by'=>  AppUser::getUserId(),'modified_by'=>  AppUser::getUserId(),'created_at'=>time(),'modified_at'=>time()));
                
                if (isset($invoice)) {
                    $user = AppUser::getCurrentUser();
                    Yii::app()->user->setFlash('success', 'Invoice sent to selected user.');
                    $this->redirect(array('index'));
                } else {
                    Yii::app()->user->setFlash('error', 'An error occured while creating invoice');
                    $this->redirect(array('index'));
                }
            }
        }

        $details = array();
        $timeSheetData = '';
        $timesheet_id = null;
        if ($timesheet) {
            $details = AppTimeSheet::compileCardDetail($timesheet);
            $timesheet_id = $timesheet;
        } else if (isset($id)) {
            if ($temp = Invoice::model()->findByPk($id)) {
                $timesheet_id = $temp->timesheet_id;
                $details = get_object_vars(json_decode($temp->html));
                $this->render('new1', array('details' => $details,
                    'contacts' => $contacts));
                die();
            }
        }

        $this->render('new', array('details' => $details,
            'company' => $company,
            'contacts' => $contacts
        ));
    }

    public function actionSend($timesheet = '', $id = '') {
        $timeSheetData = '';
        $timesheet_id = null;
        if ($timesheet) {
            $details = AppTimeSheet::compileCardDetail($timesheet);
            $timeSheetData = $this->renderPartial('timesheet', $details, true);
            $timesheet_id = $timesheet;
        } else if ($id) {
            if ($temp = Invoice::model()->findByPk($id)) {
                $timeSheetData = $temp->html;
                $timesheet_id = $temp->timesheet_id;
            }
        }

        if (isset($_POST['email-text'])) {

            $msg = $_POST['email-text'];

            if (isset($_POST['contacts']) && $_POST['contacts'] != 'out') {
                $select_email = Contact::model()->findByAttributes(array('id' => $_POST['contacts']))->email;
                $select_name = Contact::model()->findByAttributes(array('id' => $_POST['contacts']))->name;
                $contact_id = $_POST['contacts'];
            } else {
                $select_email = $_POST['contact-email'];
                $select_name = $_POST['contact-name'];
                $contact = AppCompany::addContact(array('name'=>$select_name,'email'=>$select_email,'type'=>'Individual','company_id'=>  AppUser::getUserCompany()->id,'created_by'=>  AppUser::getUserId(),'modified_by'=>  AppUser::getUserId(),'created_at'=>time(),'modified_at'=>time(),'status'=>1));
                
                if (isset($contact)) {
                    $contact_id = $contact->id;
                } else {
                    Yii::app()->user->setFlash('error', 'An error occured while creating invoice');
                    $this->redirect(array('index'));
                }
            }
            $invoice = AppCompany::addInvoice(array('timesheet_id'=>$timesheet_id,'contact_id'=>$contact_id,'html'=> $msg,'status'=>1,'created_by'=>  AppUser::getUserId(),'modified_by'=>  AppUser::getUserId(),'created_at'=>time(),'modified_at'=>time()));
            
            if (isset($invoice)) {
                $user = AppUser::getCurrentUser();
                $mailer = new AppMailer();
                $mailer->prepareBody('invoice', array('INVOICE' => $msg));
                $mailer->sendMail(array(array('email' => $select_email, 'name' => $select_name)), array('email' => $user->email, 'name' => $user->full_name));
                $this->redirect(array('index'));
            } else {
                Yii::app()->user->setFlash('error', 'An error occured while creating invoice');
                $this->redirect(array('index'));
            }

            AppLogging::addLog('Invoice Email', 'success', 'application.invoice.controller.main');
            Yii::app()->user->setFlash('success', 'Invoice sent to the recipient.');
        }
        $contacts = AppCompany::getCompanyContacts();
        $this->render('index', array(
            'contacts' => $contacts,
            'timeSheetData' => $timeSheetData,
        ));
    }

    public function actionGetData() {
        $this->layout = "//layouts/blank";
        $response = array('status' => false, 'data' => '', 'reason' => 'Not a Valid Ajax Request or Post Data is Missing');
        if (Yii::app()->getRequest()->isAjaxRequest && Yii::app()->getRequest()->getPost("entryId") != "") {
            $invoice = Invoice::model()->findByPk(Yii::app()->getRequest()->getPost("entryId"));
            if ($invoice) {
                $response['status'] = true;
                $response['reason'] = '';
                $response['data'] = $this->renderPartial('_view', array('data' => $invoice->html), true);
            } else {
                $response['status'] = false;
                $response['reason'] = "No Record Found";
            }
        }

        AppSetting::generateAjaxResponse($response);
    }

    public function actionInvoiceDelete($id)
    {
        if ($invoice = Invoice::model()->findByPk($id)) {
            Yii::app()->user->setFlash('success', 'Invoice has been deleted successfully.');
            $this->redirect(array('index'));
        } else {
            Yii::app()->user->setFlash('error', 'Unable to delete the invoice');
            $this->redirect(array('index'));
        }
    }
    public function actionDelete($id) {
        dd($id);
        if ($invoice = Invoice::model()->findByPk($id)) {
            $invoice->delete();
            $this->redirect(array('index'));
        } else {
            Yii::app()->user->setFlash('error', 'Unable to delete the invoice');
            $this->redirect(array('index'));
        }
    }

}