<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

function dd($obj) {
    echo "<pre>";
    print_r($obj);
    echo "<pre>";
    die();
}

function d($obj) {
    echo "<pre>";
    print_r($obj);
    echo "<pre>";
}

function get_placing_string($placing) {
    $i = intval($placing % 10);
    $place = substr($placing, -2); //For 11,12,13 places

    if ($i == 1 && $place != '11') {
        return $placing . 'st';
    } else if ($i == 2 && $place != '12') {
        return $placing . 'nd';
    } else if ($i == 3 && $place != '13') {
        return $placing . 'rd';
    }
    return $placing . 'th';
}

if ($_SERVER['SERVER_NAME'] == "localhost") {
    $db_password = '';
    $db_user = 'root';
    $db_name = "speedy_payroll";
} else if (strpos($_SERVER['REQUEST_URI'], 'speedy-stage')) {
    $db_password = 'k~pbpw!uU[p!';
    $db_user = 'vistecho_speedy';
    $db_name = "vistecho_speedyStage";

    ini_set('display_errors', 0);
    error_reporting(~E_ALL & ~E_NOTICE);
} else if (strpos($_SERVER['REQUEST_URI'], 'speedyalpha')) {
    $db_password = 'k~pbpw!uU[p!';
    $db_user = 'vistecho_speedy';
    $db_name = "vistecho_speedy_alpha";

 //   ini_set('display_errors', 0);
 //   error_reporting(~E_ALL & ~E_NOTICE);
}else if (strpos($_SERVER['REQUEST_URI'], 'testserver/speedpayroll')) {
    $db_password = 'k~pbpw!uU[p!';
    $db_user = 'vistecho_speedy';
    $db_name = "vistecho_speedy_payroll";
}
else if (strpos($_SERVER['REQUEST_URI'], 'sptest')) {
    $db_password = 'bitnami';
    $db_user = 'root';
    $db_name = "speedy_payrolltest";
    ini_set('display_errors', 0);
    error_reporting(~E_ALL & ~E_NOTICE);
}
else {
    $db_password = 'bitnami';
    $db_user = 'root';
    $db_name = "speedy_payroll";
    ini_set('display_errors', 0);
    error_reporting(~E_ALL & ~E_NOTICE);
}

$docRoot = str_replace('index.php', '', $_SERVER['SCRIPT_FILENAME']);
date_default_timezone_set("Europe/London");

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Seminant Solutions',
    // preloading 'log' component
    'preload' => array('log'),
    'theme' => 'speedypayroll',
    'timeZone' => 'Europe/London',
    // autoloading model and component classes
    'import' => array(
        'ext.ESES.*',
        'application.models.*',
        'application.components.*',
        'application.extensions.fpdf.*',
        'application.modules.user.models.*',
        'application.modules.user.components.*',
        'application.modules.contract.models.*',
        'application.modules.contract.components.*',
        'application.modules.invoice.models.*',
        'application.modules.invoice.components.*',
        'application.modules.importcsv.models.*',
        'application.modules.importcsv.components.*',
        'application.modules.payroller.models.*',
        'application.modules.payroller.components.*',
        'application.modules.company.models.*',
        'application.modules.company.components.*',
        'application.modules.timesheet.models.*',
        'application.modules.timesheet.components.*',
        'application.modules.template.models.*',
        'application.modules.template.components.*',
        'application.modules.billing.models.*',
        'application.modules.billing.components.*',
        'application.modules.help.models.*',
        'application.modules.help.components.*',
    ),
    'modules' => array(
        'user',
        'company',
        'contract',
        'timesheet',
        'invoice',
        'payroller',
        'importcsv',
        'admin',
        'template',
        'billing',
        'help',
        // uncomment the following to enable the Gii tool
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'test12',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
        ),
    ),
    // application components
    'components' => array(
        'ses'=>array(
        'class'=>'ext.ESES.ESES',
        'access_key'=>'AKIAJU76VTJTA2I6L36A',
        'secret_key'=>'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',
        'host'=>'email.us-east-1.amazonaws.com',
        ),
        'user' => array(
            // enable cookie-based authentication
            'allowAutoLogin' => false,
            'loginUrl' => array('/user/main/login'),
            'class' => 'application.components.WebUser',
        ),
        // uncomment the following to enable URLs in path-format
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
//		'db'=>array(
//			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
//		),
        // uncomment the following to use a MySQL database
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=' . $db_name,
            'emulatePrepare' => true,
            'username' => $db_user,
            'password' => $db_password,
            'charset' => 'utf8',
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CDbLogRoute',
                    'connectionID' => 'db',
//                    'autoCreateLogTable'=>true,
                    'levels' => 'error, warning, info',
                    'filter' => 'CLogFilter',
                    'logTableName' => 'sp_log'
                ),
            // uncomment the following to show log messages on web pages
            /*
              array(
              'class'=>'CWebLogRoute',
              ),
             */
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'webmaster@example.com',
        'pagination' => array(
            'tsheetSize' => 15,
            'payrollSize' => 15
        ),
        'uploads' => array(
            'payslip' => $docRoot . 'uploads/payslips/',
            'defaultPaySlip' => array('path' => $docRoot . 'uploads/', 'name' => 'default-payslip.csv')
        )
    ),
);
