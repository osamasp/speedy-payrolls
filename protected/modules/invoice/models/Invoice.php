<?php

/**
 * This is the model class for table "sp_invoice".
 *
 * The followings are the available columns in table 'sp_invoice':
 * @property string $id
 * @property string $timesheet_id
 * @property string $contact_id
 * @property string $html
 * @property integer $status
 * @property string $created_at
 * @property string $created_by
 * @property string $modified_at
 * @property string $modified_by
 * @property string $logo_url
 * @property string $invoice_reference
 * @property double $sub_total
 * @property double $vat
 * @property double $total
 * @property string $company_id
 * @property string $contractor_name
 * @property string $contractor_address
 * @property string $vat_number
 * @property string $date
 *
 * The followings are the available model relations:
 * @property Timesheet $timesheet
 * @property Contact $contact
 * @property User $createdBy
 * @property User $modifiedBy
 * @property InvoiceItems[] $invoiceItems
 */
class Invoice extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sp_invoice';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, contact_id, company_id, contractor_name, date, contractor_address, vat_number', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('sub_total, vat, total', 'numerical'),
			array('id, timesheet_id, contact_id, created_at, created_by, modified_at, modified_by, company_id, vat_number', 'length', 'max'=>20),
			array('logo_url', 'length', 'max'=>100),
			array('invoice_reference, contractor_name, contractor_address', 'length', 'max'=>255),
			array('html', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, timesheet_id, contact_id, html, status, created_at, created_by, modified_at, modified_by, logo_url, invoice_reference, sub_total, vat, total, company_id, contractor_name, contractor_address, vat_number', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'timesheet' => array(self::BELONGS_TO, 'Timesheet', 'timesheet_id'),
			'contact' => array(self::BELONGS_TO, 'Contact', 'contact_id'),
			'createdBy' => array(self::BELONGS_TO, 'User', 'created_by'),
			'modifiedBy' => array(self::BELONGS_TO, 'User', 'modified_by'),
			'invoiceItems' => array(self::HAS_MANY, 'InvoiceItems', 'invoice_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'timesheet_id' => 'Timesheet',
			'contact_id' => 'Contact',
			'html' => 'Html',
			'status' => 'Status',
			'created_at' => 'Created At',
			'created_by' => 'Created By',
			'modified_at' => 'Modified At',
			'modified_by' => 'Modified By',
			'logo_url' => 'Logo Url',
			'invoice_reference' => 'Invoice Reference',
			'sub_total' => 'Sub Total',
			'vat' => 'Vat',
			'total' => 'Total',
			'company_id' => 'Company',
			'contractor_name' => 'Contractor Name',
			'contractor_address' => 'Contractor Address',
			'vat_number' => 'Vat Number',
                        'date' => 'Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('timesheet_id',$this->timesheet_id,true);
		$criteria->compare('contact_id',$this->contact_id,true);
		$criteria->compare('html',$this->html,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('modified_at',$this->modified_at,true);
		$criteria->compare('modified_by',$this->modified_by,true);
		$criteria->compare('logo_url',$this->logo_url,true);
		$criteria->compare('invoice_reference',$this->invoice_reference,true);
		$criteria->compare('sub_total',$this->sub_total);
		$criteria->compare('vat',$this->vat);
		$criteria->compare('total',$this->total);
		$criteria->compare('company_id',$this->company_id,true);
		$criteria->compare('contractor_name',$this->contractor_name,true);
		$criteria->compare('contractor_address',$this->contractor_address,true);
		$criteria->compare('vat_number',$this->vat_number,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Invoice the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
