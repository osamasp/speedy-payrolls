<?php

/**
 * This is the model class for table "sp_invoice_items".
 *
 * The followings are the available columns in table 'sp_invoice_items':
 * @property integer $id
 * @property string $item
 * @property double $unit
 * @property double $rate
 * @property double $cost
 * @property string $invoice_id
 *
 * The followings are the available model relations:
 * @property Invoice $invoice
 */
class InvoiceItems extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sp_invoice_items';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, item, unit, rate, cost, invoice_id', 'required'),
			array('id', 'numerical', 'integerOnly'=>true),
			array('unit, rate, cost', 'numerical'),
			array('item, invoice_id', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, item, unit, rate, cost, invoice_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'invoice' => array(self::BELONGS_TO, 'Invoice', 'invoice_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'item' => 'Item',
			'unit' => 'Unit',
			'rate' => 'Rate',
			'cost' => 'Cost',
			'invoice_id' => 'Invoice',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('item',$this->item,true);
		$criteria->compare('unit',$this->unit);
		$criteria->compare('rate',$this->rate);
		$criteria->compare('cost',$this->cost);
		$criteria->compare('invoice_id',$this->invoice_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public static function create($id,$item,$unit,$rate,$cost,$invoice_id)
        {
            $model = new InvoiceItems();
                        $model->id = $id;
                        $model->item = $item;
                        $model->unit = $unit;
                        $model->rate = $rate;
                        $model->cost = $cost;
                        $model->invoice_id = $invoice_id;
                        if($model->save()){
                return true;
            } else {
                return $model->errors;
            }
        }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return InvoiceItems the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
