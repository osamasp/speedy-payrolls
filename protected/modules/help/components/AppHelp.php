<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AppHelp
 *
 * @author usman
 */
class AppHelp extends CComponent {

    public static $help;

    public static function hasHelp() {
        if (isset(self::$help)) {
            return true;
        }
        return self::findHelp();
    }

    private static function findHelp() {
        $curpage = Yii::app()->controller->module?Yii::app()->controller->module->id : '';
        $curpage .= ($curpage==''?'':'/'). Yii::app()->getController()->getAction()->controller->id;
        $curpage .= '/'.Yii::app()->getController()->getAction()->controller->action->id;
        $helps = Help::model()->findAll();
        foreach ($helps as $help) {            
            if(($curpage == trim($help->route, '/')) || ($curpage == trim($help->route, '/').'/index')){
                self::$help = $help;
                return true;
            }
        }
        return false;
    }

    public static function getHelpHTML() {
        if (isset(self::$help)) {
            return self::$help->html;
        } else if (self::findHelp()) {
            return self::$help->html;
        }
        return Null;
    }
    
    public static function getHelpTitle() {
        if (isset(self::$help)) {
            return self::$help->title;
        } else if (self::findHelp()) {
            return self::$help->title;
        }
        return Null;
    }

}
