<?php

class MainController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
                //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'create', 'update', 'admin', 'delete'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array(),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array(),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Help;

        if (isset($_POST['Help'])) {
            $model->id = AppInterface::getUniqueId();
            $model->attributes = $_POST['Help'];
            $model->route = trim(str_replace('\\', '/', $_POST['Help']['route']),'/ ');
            $model->created_at = time();
            $model->modified_at = time();
            $model->created_by = AppUser::getUserId();
            $model->modified_by = AppUser::getUserId();
            if ($model->save()) {
                Yii::app()->user->setFlash('success','Help created successfully');
                if(isset($_POST['save'])){
                    $this->redirect(array('index'));
                } else {
                    $this->redirect(array('create'));
                }
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        if (isset($_POST['Help'])) {
            $model->attributes = $_POST['Help'];
            $model->modified_at = time();
            $model->modified_by = AppUser::getUserId();
            if ($model->save())
                $this->redirect(array('index'));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if ($help = Help::model()->findByPk($id)) {
            $help->status = 0;
            $help->modified_by = AppUser::getUserId();
            $help->modified_at = time();
            if ($help->save()) {
                Yii::app()->user->setFlash('success', 'Deleted successfully.');
            } else {
                Yii::app()->user->setFlash('error', 'An error occured while deleting');
            }
            $this->redirect(array('index'));
        } else {
            throw new CHttpException(400, 'Invalid Request');
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = Help::model()->findAllByAttributes(array('status'=>'1'));
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Help('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Help']))
            $model->attributes = $_GET['Help'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Help the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Help::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Help $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'help-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
