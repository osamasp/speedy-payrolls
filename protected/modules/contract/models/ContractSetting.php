<?php

/**
 * This is the model class for table "sp_contract_setting".
 *
 * The followings are the available columns in table 'sp_contract_setting':
 * @property string $id
 * @property string $contract_id
 * @property string $key
 * @property string $value
 * @property string $category
 * @property string $created_by
 * @property string $created_at
 * @property string $modified_by
 * @property string $modified_at
 *
 * The followings are the available model relations:
 * @property Contract $contract
 * @property Payrate[] $payrates
 */
class ContractSetting extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'sp_contract_setting';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id, key, value, category', 'required'),
            array('id, contract_id, key, value, created_by, modified_by', 'length', 'max' => 255),
            array('category', 'length', 'max' => 7),
            array('created_at, modified_at', 'length', 'max' => 20),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, contract_id, key, value, category, created_by, created_at, modified_by, modified_at', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'contract' => array(self::BELONGS_TO, 'Contract', 'contract_id'),
            'payrates' => array(self::HAS_MANY, 'Payrate', 'contract_setting_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'contract_id' => 'Contract',
            'key' => 'Key',
            'value' => 'Value',
            'category' => 'Category',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'modified_by' => 'Modified By',
            'modified_at' => 'Modified At',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('contract_id', $this->contract_id, true);
        $criteria->compare('key', $this->key, true);
        $criteria->compare('value', $this->value, true);
        $criteria->compare('category', $this->category, true);
        $criteria->compare('created_by', $this->created_by, true);
        $criteria->compare('created_at', $this->created_at, true);
        $criteria->compare('modified_by', $this->modified_by, true);
        $criteria->compare('modified_at', $this->modified_at, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ContractSetting the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function create($contractid, $name, $value, $is_item = 0) {
        $contractsettings = new ContractSetting;
        $contractsettings->id = AppInterface::getUniqueId();
        $contractsettings->contract_id = $contractid;
        $contractsettings->key = $name;
        $contractsettings->value = $value;
        if ($is_item) {
            $contractsettings->category = 'item';
        } else {
            $contractsettings->category = 'payrate';
        }
        $contractsettings->created_by = AppUser::getUserId();
        $contractsettings->modified_by = AppUser::getUserId();
        $contractsettings->created_at = time();
        $contractsettings->modified_at = time();
        if ($contractsettings->save()) {
            return true;
        }
        return false;
    }

}
