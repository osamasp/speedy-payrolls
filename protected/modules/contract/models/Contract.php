<?php

/**
 * This is the model class for table "sp_contract".
 *
 * The followings are the available columns in table 'sp_contract':
 * @property string $id
 * @property string $user_id
 * @property string $company_id
 * @property string $parent_id
 * @property string $role
 * @property string $employee_type
 * @property string $approver_id
 * @property string $payroller_id
 * @property string $type
 * @property string $policy
 * @property string $start_time
 * @property string $end_time
 * @property string $collection_day
 * @property integer $collection_type
 * @property string $status
 * @property string $invitation_code
 * @property string $created_at
 * @property string $created_by
 * @property string $modified_at
 * @property string $modified_by
 * @property string $timesheetTypeSel
 * @property string $owner_id
 * @property string $file_name
 * @property integer $is_internal
 * @property string $next_cf_date
 * @property string $next_cf_day
 * @property integer $next_cf_type
 *
 * The followings are the available model relations:
 * @property User $user
 * @property Company $company
 * @property User $approver
 * @property User $payroller
 * @property User $createdBy
 * @property User $modifiedBy
 * @property Contract $parent
 * @property Contract[] $contracts
 * @property ContractSetting[] $contractSettings
 * @property Timesheet[] $timesheets
 */
class Contract extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'sp_contract';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id, user_id, company_id, type, start_time, created_at, created_by, modified_at, modified_by', 'required'),
            array('collection_type, is_internal', 'numerical', 'integerOnly' => true),
            array('id, user_id, company_id, parent_id, role, approver_id, payroller_id, invitation_code, created_by, modified_by, owner_id, file_name', 'length', 'max' => 255),
            array('employee_type, collection_day', 'length', 'max' => 9),
            array('type', 'length', 'max' => 6),
            array('start_time, end_time, created_at, modified_at, next_cf_date', 'length', 'max' => 20),
            array('status', 'length', 'max' => 10),
            array('timesheetTypeSel', 'length', 'max' => 7),
            array('policy', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, user_id, company_id, parent_id, role, employee_type, approver_id, payroller_id, type, policy, start_time, end_time, collection_day, collection_type, status, invitation_code, created_at, created_by, modified_at, modified_by, timesheetTypeSel, owner_id, file_name, is_internal, next_cf_date, next_cf_day, next_cf_type', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
            'approver' => array(self::BELONGS_TO, 'User', 'approver_id'),
            'payroller' => array(self::BELONGS_TO, 'User', 'payroller_id'),
            'createdBy' => array(self::BELONGS_TO, 'User', 'created_by'),
            'modifiedBy' => array(self::BELONGS_TO, 'User', 'modified_by'),
            'parent' => array(self::BELONGS_TO, 'Contract', 'parent_id'),
            'contracts' => array(self::HAS_MANY, 'Contract', 'parent_id'),
            'contractSettings' => array(self::HAS_MANY, 'ContractSetting', 'contract_id'),
            'timesheets' => array(self::HAS_MANY, 'Timesheet', 'contract_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'user_id' => 'User',
            'company_id' => 'Company',
            'parent_id' => 'Parent',
            'role' => 'Role',
            'employee_type' => 'Employee Type',
            'approver_id' => 'Approver',
            'payroller_id' => 'Payroller',
            'type' => 'Type',
            'policy' => 'Policy',
            'start_time' => 'Start Time',
            'end_time' => 'End Time',
            'collection_day' => 'Collection Day',
            'collection_type' => 'Collection Type',
            'status' => 'Status',
            'invitation_code' => 'Invitation Code',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'modified_at' => 'Modified At',
            'modified_by' => 'Modified By',
            'timesheetTypeSel' => 'Timesheet Type Sel',
            'owner_id' => 'Owner',
            'file_name' => 'File Name',
            'is_internal' => 'Is Internal',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('user_id', $this->user_id, true);
        $criteria->compare('company_id', $this->company_id, true);
        $criteria->compare('parent_id', $this->parent_id, true);
        $criteria->compare('role', $this->role, true);
        $criteria->compare('employee_type', $this->employee_type, true);
        $criteria->compare('approver_id', $this->approver_id, true);
        $criteria->compare('payroller_id', $this->payroller_id, true);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('policy', $this->policy, true);
        $criteria->compare('start_time', $this->start_time, true);
        $criteria->compare('end_time', $this->end_time, true);
        $criteria->compare('collection_day', $this->collection_day, true);
        $criteria->compare('collection_type', $this->collection_type);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('invitation_code', $this->invitation_code, true);
        $criteria->compare('created_at', $this->created_at, true);
        $criteria->compare('created_by', $this->created_by, true);
        $criteria->compare('modified_at', $this->modified_at, true);
        $criteria->compare('modified_by', $this->modified_by, true);
        $criteria->compare('timesheetTypeSel', $this->timesheetTypeSel, true);
        $criteria->compare('owner_id', $this->owner_id, true);
        $criteria->compare('file_name', $this->file_name, true);
        $criteria->compare('is_internal', $this->is_internal);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Contract the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getCustomName() {

        return AppContract::getContractCustomName($this);
    }

    public static function create($userid, $companyid, $type, $approverid, $role, $employeetype, $is_internal, $starttime, $endtime = -1, $payrollerid = Null, $parent_id = Null, $collection_type = 1, $collection_day = 'friday', $policy = '', $status = 1) {
        $contract = new Contract;
        $contract->id = AppInterface::getUniqueId();
        $contract->user_id = $userid;
        $contract->company_id = $companyid;
        $contract->approver_id = $approverid;
        $contract->payroller_id = $payrollerid;
        $contract->role = $role;
        $contract->status = $status;
        $contract->start_time = $starttime;
        $contract->end_time = $endtime;
        $contract->type = $type;
        $contract->employee_type = $employeetype;
        $contract->created_by = 0;
        $contract->created_at = time();
        $contract->modified_by = 0;
        $contract->modified_at = time();
        $contract->is_internal = $is_internal;
        $contract->owner_id = AppUser::getUserId();
        if ($contract->insert()) {
            return $contract->id;
        } else {
            return $contract->errors;
        }
    }

    public function getCustomType() {
        return AppContract::getContractCustomType($this);
    }

    public function getCustomEmploymentType() {
        if ($this->is_internal && $this->company_id == AppUser::getUserCompany()->id) {
            return $this->employee_type;
        }
        if ($this->type != 'in' && $this->type != 'in_out' && $this->company_id == AppUser::getUserCompany()->id) {
            if ($this->parent)
                return 'in ' . $this->parent->company->name;
        }
        else if ($this->type == 'in_out' && $this->company_id == AppUser::getUserCompany()->id) {
            if ($this->parent && $this->contracts) {
                return 'out ' . $this->parent->company->name . ' in ' . $this->contracts[0]->company->name;
            }
        }
        return 'out ' . $this->company->name;
    }

    public function getcompany_name() {
        if (AppUser::isUserAdmin()) {
            if ($this->type != 'in' && $this->type != 'in_out' && $this->company_id == AppUser::getUserCompany()->id) {
                if ($this->parent)
                    return $this->parent->company->name;
            } else if ($this->type == 'in_out' && $this->company_id == AppUser::getUserCompany()->id) {
                if ($this->parent && $this->contracts) {
                    return $this->parent->company->name . ' ➡ ' . $this->contracts[0]->company->name;
                }
            }
        }
        return $this->company->name;
    }

    public function gettimesheetfrequency() {
        $result = '';
        if ($this->collection_type == 0) {
            $result .= 'Every Week(' . ucfirst($this->collection_day) . ')';
        } else {
            $result .= 'Every Month(';
            $result .= $this->collection_type == 1 ? 'First ' : 'Last ';
            $result .= ucfirst($this->collection_day) . ')';
        }
        return $result;
    }

    public function getnexttimesheetfrequency() {
        $result = '';
        if ($this->next_cf_type == 0) {
            $result .= 'Every Week(' . ucfirst($this->next_cf_day) . ')';
        } else {
            $result .= 'Every Month(';
            $result .= $this->next_cf_day == 1 ? 'First ' : 'Last ';
            $result .= ucfirst($this->collection_day) . ')';
        }
        return $result;
    }
    
    public function getcustomrole(){
        if($this->is_internal){
            return $this->role;
        } else {
            return 'Contractor';
        }
    }

}
