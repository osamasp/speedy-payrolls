<?php

class MainController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'export', 'inviteview', 'contract'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'grid', 'update', 'out', 'in', 'inout', 'outb', 'new', 'admin', 'delete', 'contract'),
                'roles' => array('admin'),
            ),
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('admin', 'grid', 'update', 'contract'),
                'roles' => array('super_admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
//        $notification_id = Yii::app()->request->getParam('notification_id');
//        if (isset($notification_id)) {
//            AppUser::updateNotificationStatus($notification_id);
//        }
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionInviteview($id) {

        $this->layout = "//layouts/login";

        $this->render('inviteview', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Contract;

        if (isset($_POST['Contract'])) {
            $model->attributes = $_POST['Contract'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {

        if ($model = Contract::model()->findByPk($id)) {
            switch ($model->collection_type) {
                case 0:    // Weekly Type
                    $time = AppTimeSheet::getStEnDateWeekly($model->collection_day, date('m/d/Y')); //Do not change date format. it has no effect on ui
                    break;
                default: // First & Last Week Of Every Month 
                    $time = AppTimeSheet::getStEnDateMonthly($model->collection_type, $model->collection_day, date('m/d/Y')); //Do not change date format. it has no effect on ui
                    break;
            }
            $cf_change_time = strtotime($time['end']) + 1;
            if (isset($_POST['Contract'])) {
                if ($_POST['Contract']['approver_id']) {
                    $model->approver_id = $_POST['Contract']['approver_id'];
                }
                if ($_POST['Contract']['end_time']) {
                    $model->end_time = DateTime::createFromFormat(AppInterface::getdateformat(), $_POST['Contract']['end_time'])->getTimestamp(); //strtotime($_POST['Contract']['end_time']);
                }
                if (isset($_POST['Contract']['collection_type']) && $_POST['Contract']['collection_type'] != '2') {
                    $model->next_cf_type = $_POST['Contract']['collection_type'] == 1 ? ($_POST['Settings']['collection_day_first'] == 1 ? 1 : 2) : 0;
                    $model->next_cf_day = $_POST['Contract']['collection_day'];
                    $model->next_cf_date = $cf_change_time;
                }
                if ($_POST['Contract']['policy']) {
                    $model->policy = $_POST['Contract']['policy'];
                }
                $file_name = null;
                if ($_FILES['Contract']['name']['file_name']) {
                    $file_name = CUploadedFile::getInstance($model, 'file_name');
                    $images_path = realpath(Yii::app()->basePath . '/../uploads/contracts');
                    $filename = explode('.', $file_name->name);
                    $filename[0] = AppInterface::getFilename();
                    $newfilename = $filename[0] . '.' . $filename[1];
                    $file_name->saveAs($images_path . '/' . $newfilename);
                    $file_name = $newfilename;
                    $model->file_name = $file_name;
                }

                $model->modified_at = time();
                $model->modified_by = AppUser::getUserId();
                if ($model->save()) {
                    if ($model->type != 'in_out') {
                        if (isset($_POST['Payrate'])) {
                            foreach ($_POST['Payrate'] as $name => $rate) {
                                AppCompany::updateContractSetting($model->id, $name, 'payrate', $rate);
                            }
                        }
                        if (isset($_POST['Item'])) {
                            foreach ($_POST['Item'] as $name => $rate) {
                                AppCompany::updateContractSetting($model->id, $name, 'item', $rate);
                            }
                        }
                    } else {
                        if (isset($_POST['Payrate1'])) {
                            foreach ($_POST['Payrate1'] as $name => $rate) {
                                AppCompany::updateContractSetting($model->id, $name, 'payrate', $rate);
                            }
                        }
                        if (isset($_POST['Item1'])) {
                            foreach ($_POST['Item1'] as $name => $rate) {
                                AppCompany::updateContractSetting($model->id, $name, 'item', $rate);
                            }
                        }
                        if (isset($_POST['Payrate2'])) {
                            foreach ($_POST['Payrate2'] as $name => $rate) {
                                AppCompany::updateContractSetting($model->id, $name, 'payrate', $rate);
                            }
                        }
                        if (isset($_POST['Item2'])) {
                            foreach ($_POST['Item2'] as $name => $rate) {
                                AppCompany::updateContractSetting($model->id, $name, 'item', $rate);
                            }
                        }
                    }
                    Yii::app()->user->setFlash('success', 'Speedy Timesheet account saved successfully');
                    $this->redirect(array('index'));
                }
            }

            $this->render('edit', array(
                'model' => $model,
                'cf_change_time' => $cf_change_time,
                'approvers' => AppContract::get_all_internal_users(),
            ));
        } else {
            throw new CHttpException('403', 'Invalid Request');
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        if (AppUser::isUserAdmin()) {
            $dataProvider = AppContract::get_all_contract_by_company();
        } else {
            $dataProvider = AppContract::getEmployeeContracts(AppUser::getUserId());
        }
        AppLogging::addLog('Employee Contract List', 'success', 'application.contract.controller.main');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionExport() {
        $webroot = Yii::getPathOfAlias('webroot');
        if (!is_dir($webroot . '/uploads/contracts')) {
            mkdir($webroot . '/uploads/contracts');
            chmod(($webroot . '/uploads/contracts'), 0755);
            // the default implementation makes it under 777 permission, which you could possibly change recursively before deployment, but here's less of a headache in case you don't
        }
        $file = $webroot . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'contracts' . DIRECTORY_SEPARATOR . 'contract-' . date('d-m-Y-H-i') . '.csv';
        $handle = fopen($file, 'w');
        if (AppUser::isUserAdmin()) {
            $dataProvider = AppContract::get_all_contract_by_company();
        } else {
            $dataProvider = AppContract::getEmployeeContracts(AppUser::getUserId());
        }
        $data = array();
        $i = 0;
        foreach ($dataProvider as $item) {
            if (AppContract::can_admin_view_out_b($item)) {
                if (in_array($item->id, $_POST['entry'])) {
                    if (isset($_POST['export_fields'])) {
                        foreach ($_POST['export_fields'] as $items) {
                            if ($items == 'name') {
                                $data[$i]['name'] = $item->created_at;
                            } else if ($items == 'type') {
                                $data[$i]['type'] = $item->title;
                            } else if ($items == 'start_date') {
                                $data[$i]['start_date'] = $item->start_time;
                            } else if ($items == 'end_date') {
                                $data[$i]['end_date'] = $item->end_time;
                            } else if ($items == 'approver') {
                                $data[$i]['approver'] = $item->approver ? $item->approver->full_name : '-';
                            }
                        }
                        $i++;
                    } else {
                        $data[$i]['name'] = $item->user->full_name;
                        $data[$i]['type'] = ucwords(str_replace('_', ' ', AppUser::isUserAdmin() ? $item->CustomEmploymentType : $item->CustomName));
                        $data[$i]['start_date'] = date(AppInterface::getdateformat(), $item->start_time);
                        $data[$i]['end_date'] = $item->end_time <= 0 ? '-' : date(AppInterface::getdateformat(), $item->end_time);
                        $data[$i]['approver'] = $item->approver ? $item->approver->full_name : '-';
                        $i++;
                    }
                }
            }
        }

        AppInterface::GenerateCSV($file, array('name', 'type', 'start_date', 'end_date', 'approver'), $data);
        fclose($handle);
        $url = $this->createAbsoluteUrl('/uploads/contracts/' . 'contract-' . date('d-m-Y-H-i') . '.csv');
        $this->layout = "//layouts/blank";
        $response = array('status' => false, 'data' => '', 'reason' => 'Not a Valid Ajax Request or Post Data is Missing');
        $response['status'] = true;
        $response['reason'] = '';
        $response['url'] = $url;
        $response['data'] = $this->render('//user/employee/viewlist', array(
            'file' => $file,
            'name' => 'contract-' . date('d-m-Y-H-i') . '.csv',
            'url' => $url
                ), true);
        AppSetting::generateAjaxResponse($response);
    }

    public function actionGrid($id) {
        $contracts = AppContract::getEmployeeContracts($id);
        AppLogging::addLog('Individual Employee Contract List ', 'success', 'application.contract.controller.main');
        $this->render('grid', array('dataProvider' => $contracts));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Contract('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Contract']))
            $model->attributes = $_GET['Contract'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionOut() {
        $c = new Contract();
        if (isset($_POST['Contract'])) {
            $current_user = AppUser::getCurrentUser();
            $super_user = AppUser::get_super_user();
            $c->attributes = $_POST['Contract'];
            $company_id = 0;
            if (!empty($_POST['cc']['company_name']) && !empty($_POST['cc']['user_name']) && !empty($_POST['cc']['user_email'])) {
                $pwd = User::model()->createRandomPassword();
                $_POST['cc']['password'] = md5($pwd);
                $_POST['cc']['pwd'] = $pwd;
                $company = AppCompany::addDummyCompany($_POST['cc']);
                if ($company['company'] instanceof Company) {
                    if ($_POST['timesheet_approver'] == 'out') {
                        $_POST['timesheet_approver'] = 'in_company';
                        $approver_id = $company['user_id'];
                    }
                    if ($company != null) {
                        $company_id = $company['company']->id;
                        //Assign Plan and Privileges to the company
                        $set_company_priviledges = AppCompany::setCompanyPlan($company_id, 29450400);
                    }
                } else {
                    
                }
            }
            $payroller_id = 0;
            if (AppUser::canPayroll()) {
                $payroller_id = AppUser::getUserCompany($_POST['Contract']['user_id'], false)->payroller_id;
            }
            $file_name = null;
            if ($_FILES['Contract']['name']['file_name']) {
                $file_name = CUploadedFile::getInstance($c, 'file_name');
                $images_path = realpath(Yii::app()->basePath . '/../uploads/contracts');
                $filename = explode('.', $_FILES['Contract']['name']['file_name']);
                $filename[0] = AppInterface::getFilename();
                $newfilename = $filename[0] . '.' . $filename[1];
                $file_name->saveAs($images_path . '/' . $newfilename);
                $file_name = $newfilename;
            }
            $parent_id = 0;
            if ($parentcontract = AppContract::getInternalContract($c->user_id)) {
                $parent_id = $parentcontract->id;
            }

            if ($_POST['timesheet_approver'] == 'out') {
                if (isset($_POST['approver_email'])) {
                    $outapprover = User::prepareUserForAuthorisation($_POST['approver_email']);
                    if ($outapprover) {
                        $approver_id = $outapprover->id;
                    } else {


                        $args = array(
                            'company_name' => $_POST['approver_name'] . "'s Company",
                            'user_name' => $_POST['approver_name'],
                            'user_email' => $_POST['approver_email']
                        );

                        $dummyCompany = AppCompany::addDummyCompany($args);

                        if ($dummyCompany != null) {
                            $outapprover = User::prepareUserForAuthorisation($_POST['approver_email']);
                            //Assign Plan and Privileges to the company
                            $set_company_priviledges = AppCompany::setCompanyPlan($dummyCompany->id, 29450400);
                            if ($outapprover != null)
                                $approver_id = $outapprover->id;
                        }else {
                            Yii::app()->user->setFlash('error', 'Unable to find approver. You can set yourself as approver.');
                            AppLogging::addLog('No Approver Found ', 'error', 'application.contract.controller.main');
                        }
                    }
                } else {
                    Yii::app()->user->setFlash('error', 'Unable to find approver. You can set yourself as approver.');
                    AppLogging::addLog('No Approver Found ', 'error', 'application.contract.controller.main');
                }
            } else {
                $approver_id = $_POST['Contract']['approver_id'];
            }

            $contract1 = AppCompany::addContract(array('attributes' => $_POST['Contract'], 'user_id' => $c->user_id, 'company_id' => $company_id, 'role' => "Employee", 'status' => 1, 'start_time' => time(), 'end_time' => -1, 'type' => 'out', 'created_by' => 0, 'created_at' => time(), 'file_name' => $file_name, 'payroller_id' => $payroller_id, 'collection_type' => $_POST['Contract']['collection_type'] == 1 ? ($_POST['Settings']['collection_day_first'] == 1 ? 1 : 2) : 0, 'collection_day' => $_POST['Contract']['collection_day'], 'modified_by' => 0, 'modified_at' => time(), 'parent_id' => $parent_id, 'owner_id' => AppUser::getUserId(), 'approver_id' => $approver_id));

            if (isset($approver_id)) {
                if (isset($contract1)) {
                    if (isset($_POST['Payrate'])) {
                        foreach ($_POST['Payrate'] as $name => $rate) {
                            AppCompany::addContractSetting(array('contract_id' => $contract1->id, 'key' => $name, 'value' => $rate, 'category' => 'payrate', 'created_by' => AppUser::getUserId(), 'modified_by' => AppUser::getUserId(), 'created_at' => time(), 'modified_at' => time()));
                        }
                    }

                    if (isset($_POST['Item'])) {
                        foreach ($_POST['Item'] as $name => $rate) {
                            AppCompany::addContractSetting(array('contract_id' => $contract1->id, 'key' => $name, 'value' => $rate, 'category' => 'item', 'created_by' => AppUser::getUserId(), 'modified_by' => AppUser::getUserId(), 'created_at' => time(), 'modified_at' => time()));
                        }
                    }
                    Yii::app()->user->setFlash('success', 'Contract Out has been created, you can view contract of ' . $contract1->user->first_name);
                    AppLogging::addLog('Contract Out Created ', 'success', 'application.contract.controller.main');
                    $approver = User::model()->findByPk($contract1->approver_id);

                    $msg = 'You have been nominated by ' . $current_user->first_name . ' to become their Timesheet approver on SpeedyPayrolls.';
                    if (isset($dummyCompany)) {
                        $msg .= "<br/><br/><a href='" . Yii::app()->createAbsoluteUrl("/contract/main/inviteview", array('id' => $dummyCompany['company']->id)) . "'>View Contract";
                    }

                    $not_result = AppInterface::notification($contract1->user_id, 9, 0, $contract1->id, $contract1->user_id);
                    $notifier = new Notification();
                    $notification_count = $notifier->getUserNotificationUnseenCount($contract1->user_id);
                    $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
                    $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;

                    $notification = Yii::app()->getModule('notification');
                    $notification->send(array(
                        'email' =>
                        array('template' => 'Email', 'params' => array('BODY' => $msg),
                            'to' => array(array('email' => $approver->email, 'name' => $approver->first_name)),
                            'from' => array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls')
                        ),
                        'push' =>
                        array('server_notification',
                            array('id' => $contract1->user_id,
                                'msg' => sprintf($not_result->notificationType->notice, $contract1->user->first_name),
                                'url' => $url, 'logo' => $logo, 'count' => $notification_count
                            )
                        )
                            )
                    );

                    $not_result = AppInterface::notification($contract1->company->admin->id, 13, 0, $contract1->id, $contract1->user->id);
                    $notification_count = $notifier->getUserNotificationUnseenCount($contract1->company->admin->id);
                    $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
                    $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;

                    $msg = 'Dear ' . $contract1->company->admin->first_name . ' ,<br/><br/>' . $contract1->user->first_name . ' has been added to your company.';
                    $notification->send(array(
                        'email' =>
                        array('template' => 'Email', 'params' => array('BODY' => $msg),
                            'to' => array(array('email' => $contract1->company->admin->email, 'name' => $contract1->company->admin->first_name)),
                            'from' => array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls')
                        ),
                        'push' =>
                        array('server_notification',
                            array('id' => $contract1->company->admin->id,
                                'msg' => sprintf($not_result->notificationType->notice, $contract1->user->first_name, AppUser::getUserCompany($contract1->user->id)->name),
                                'url' => $url, 'logo' => $logo, 'count' => $notification_count
                            )
                        )
                    ));

                    $msg = ConstantMessages::contractApprover();
                    $not_result = AppInterface::notification($approver->id, 3, 0, $contract1->id, $contract1->approver_id);
                    $notification_count = $notifier->getUserNotificationUnseenCount($approver->id);
                    $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
                    $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;

                    $employee = User::model()->findByPk($_POST['Contract']['user_id']);
                    $msg = 'Dear ' . $employee->first_name . ' ,<br/><br/>Your new contract has been added.';
                    $msg2 = $approver->first_name . ' has been nominated by ' . $current_user->full_name . ' to become timesheet approver for ' . $contract1->user->first_name . '.';

                    if (isset($dummyCompany)) {
                        $msg2 .= "<br/><br/><a href='" . Yii::app()->createAbsoluteUrl("/contract/main/inviteview", array('id' => $dummyCompany['company']->id)) . "'>View Contract";
                    }

                    $notification->send(array(
                        'email' =>
                        array('template' => 'Email', 'params' => array('BODY' => $msg),
                            array('email' => $employee->email, 'name' => $employee->first_name),
                            array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls')
                        ),
                        'push' =>
                        array('server_notification',
                            array('id' => $approver_id,
                                'msg' => sprintf($not_result->notificationType->notice, $current_user->first_name),
                                'url' => $url,
                                'logo' => $logo,
                                'count' => $notification_count
                            )
                        )
                            )
                    );


//                    email to super admin
//                    $mailer2 = new AppMailer();
                    $msg2 = $approver->first_name . ' has been nominated by ' . $current_user->full_name . ' to become timesheet approver for ' . $contract1->user->first_name . '.';

                    if (isset($dummyCompany)) {
                        $msg2 .= "<br/><br/><a href='" . Yii::app()->createAbsoluteUrl("/contract/main/inviteview", array('id' => $dummyCompany['company']->id)) . "'>View Contract";
                    }

                    $not_result = AppInterface::notification($super_user->id, 10, 0, $contract1->id, $contract1->user_id);
                    $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
                    $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;

                    $msg3 = User::model()->findByPk($contract1->user_id)->first_name . ' has a new Speedy Timesheet account created by ' . $current_user->full_name;

                    $notification->send(
                            array(
                                'email' => array('template' => 'Email', 'params' => array('BODY' => $msg2),
                                    'to' => array(array('email' => $super_user->email, 'name' => $super_user->email)),
                                    'from' => array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls')
                                ),
                                'email' => array('template' => 'Email', 'params' => array('BODY' => $msg3),
                                    'to' => array(array('email' => $super_user->email, 'name' => $super_user->email)),
                                    'from' => array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls')
                                ),
                                'push' => array('server_notification',
                                    array('id' => $super_user->id,
                                        'msg' => sprintf($not_result->notificationType->notice, $approver->first_name, $current_user->full_name, $contract1->user->first_name),
                                        'url' => $url, 'logo' => $logo
                                    )
                                )
                    ));

//                    email to super admin
                    $not_result = AppInterface::notification($super_user->id, 6, 0, $contract1->id, $contract1->user_id);
                    $notifier = new Notification();
                    $notification_count = $notifier->getUserNotificationUnseenCount($super_user->id);
                    $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
                    $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;

                    $notification->send(array(
                        'push' => array('server_notification',
                            array('id' => $super_user->id,
                                'msg' => sprintf($not_result->notificationType->notice, $contract1->user->first_name, $current_user->full_name), 'url' => $url, 'logo' => $logo, 'count' => $notification_count))
                            )
                    );
                    $this->redirect(array('/contract/main/index'));
                }
                dd($contract1->errors);
            }
        }
        $this->render('contract-out', array(
            'employees' => AppContract::get_all_internal_users(),
            'approvers' => AppContract::get_all_internal_users(),
            'companies' => AppCompany::getAllCompanies(true),
            'contract' => $c,
        ));
    }

    public function actionIn() {
        $contract1 = new Contract;

        if (isset($_POST['Contract'])) {
            $parent_id = '';
            $user_id = '';

            if (!empty($_POST['cc']['company_name']) && !empty($_POST['cc']['user_name']) && !empty($_POST['cc']['user_email'])) {
                $dummycompany = AppCompany::addCompanyWithAddress($_POST['cc'], 1, 1);

                if (isset($dummycompany)) {
                    AppUser::assignDefaultTemplate($dummycompany->id);
                    $pwd = User::model()->createRandomPassword();
                    $dummyuser = AppUser::addUser($_POST['cc']['user_name'], $_POST['cc']['user_email'], md5($pwd), '', '', 1, 0, 29450400);

                    //Assign Plan and Privileges to the company
                    $set_company_priviledges = AppCompany::setCompanyPlan($dummycompany->id, 29450400);

                    if (isset($dummyuser)) {
                        $user_id = $dummyuser->id;
                        if ($_POST['timesheet_approver'] == 'out') {
                            $_POST['timesheet_approver'] = 'in_company';
                            $approver_id = $user_id;
                        }
                        //Add New Employee Contract
                        $contract = AppCompany::addContract(array('user_id' => $user_id, 'approver_id' => $dummyuser->id, 'company_id' => $dummycompany->id, 'created_at' => time(), 'created_by' => '0', 'modified_at' => time(), 'modified_by' => '0', 'start_time' => time(), 'end_time' => -1, 'status' => 0, 'type' => 'in', 'collection_type' => $_POST['Contract']['collection_type'] == 1 ? ($_POST['Settings']['collection_day_first'] == 1 ? 1 : 2) : 0, 'collection_day' => $_POST['Contract']['collection_day'], 'role' => 'Employee', 'is_internal' => 1, 'owner_id' => $dummyuser->id));

                        $internalcontract = $contract;
                        $parent_id = $contract->id;

                        //Add New admin contract
                        $admincontract = AppCompany::addContract(array('user_id' => $user_id, 'approver_id' => $user_id, 'company_id' => $dummycompany->id, 'created_at' => time(), 'created_by' => '0', 'modified_at' => time(), 'modified_by' => '0', 'start_time' => time(), 'end_time' => -1, 'status' => 0, 'type' => 'in', 'collection_type' => $_POST['Contract']['collection_type'] == 1 ? ($_POST['Settings']['collection_day_first'] == 1 ? 1 : 2) : 0, 'collection_day' => $_POST['Contract']['collection_day'], 'role' => 'admin', 'is_internal' => 1, 'invitation_code' => User::createRandomPassword(20)));
                    } else {
                        dd($dummyuser->errors);
                    }
                } else {
                    dd($dummycompany->errors);
                }

                $mailer11 = new AppMailer();
                $mailer11->prepareBody('invite_admin', array('NAME' => $dummyuser->first_name, 'INVITED_BY' => AppUser::getCurrentUser()->full_name, 'USERNAME' => $dummyuser->email, 'PASSWORD' => $pwd, 'LINK' => Yii::app()->createAbsoluteUrl('/user/main/login')));
                $mailer11->sendMail(array(array('email' => $dummyuser->email, 'name' => $dummyuser->first_name)), array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls'));
            } else {
                $internalcontract = AppContract::getInternalContract($_POST['Contract']['user_id']);
                $user_id = $_POST['Contract']['user_id'];
            }
            $file_name = null;
            if ($_FILES['Contract']['name']['file_name']) {
                $file_name = CUploadedFile::getInstance($contract1, 'file_name');
                $images_path = realpath(Yii::app()->basePath . '/../uploads/contracts');
                $filename = explode('.', $contract1->file_name);
                $filename[0] = AppInterface::getFilename();
                $newfilename = $filename[0] . '.' . $filename[1];
                $file_name->saveAs($images_path . '/' . $newfilename);
                $file_name = $newfilename;
            }
            $payroller_id = 0;
            if (AppUser::canPayroll()) {
                $payroller_id = AppUser::getUserCompany($user_id, false)->payroller_id;
            }
            if ($_POST['timesheet_approver'] == 'out') {
                if (isset($_POST['approver_email'])) {
                    $outapprover = User::prepareUserForAuthorisation($_POST['approver_email']);
                    if ($outapprover) {
                        $approver_id = $outapprover->id;
                    } else {

                        $args = array(
                            'company_name' => $_POST['approver_name'] . "'s Company",
                            'user_name' => $_POST['approver_name'],
                            'user_email' => $_POST['approver_email']
                        );

                        $dummyCompany = AppCompany::addDummyCompany($args);

                        if ($dummyCompany != null) {

                            //Assign Plan and Privileges to the company
                            $set_company_priviledges = AppCompany::setCompanyPlan($dummyCompany["company"]->id, 29450400);
                            $outapprover = User::prepareUserForAuthorisation($_POST['approver_email']);
                            if ($outapprover != null)
                                $approver_id = $outapprover->id;
                        }else {
                            Yii::app()->user->setFlash('error', 'Unable to find approver. You can set yourself as approver.');
                            AppLogging::addLog('No Approver Found ', 'error', 'application.contract.controller.main');
                        }
                    }
                } else {
                    Yii::app()->user->setFlash('error', 'Unable to find approver.');
                    AppLogging::addLog('No Approver Found ', 'error', 'application.contract.controller.main');
                }
            } else {
                $approver_id = $_POST['Contract']['approver_id'];
            }

            $contract1 = AppCompany::addContract(array('attributes' => $_POST['Contract'], 'user_id' => $user_id, 'company_id' => AppUser::getUserCompany()->id, 'role' => "Employee", 'status' => 1, 'start_time' => time(), 'end_time' => -1, 'type' => 'out', 'created_by' => 0, 'created_at' => time(), 'file_name' => $file_name, 'payroller_id' => $payroller_id, 'collection_type' => $_POST['Contract']['collection_type'] == 1 ? ($_POST['Settings']['collection_day_first'] == 1 ? 1 : 2) : 0, 'collection_day' => $_POST['Contract']['collection_day'], 'modified_by' => 0, 'modified_at' => time(), 'parent_id' => $internalcontract->id, 'owner_id' => AppUser::getUserId(), 'approver_id' => $approver_id));

            if (isset($approver_id)) {
                if (isset($contract1)) {
                    if (isset($_POST['Payrate'])) {
                        foreach ($_POST['Payrate'] as $name => $rate) {
                            AppCompany::addContractSetting(array('contract_id' => $contract1->id, 'key' => $name, 'value' => $rate, 'category' => 'payrate', 'created_by' => AppUser::getUserId(), 'modified_by' => AppUser::getUserId(), 'created_at' => time(), 'modified_at' => time()));
                        }
                    }
                    if (isset($_POST['Item'])) {
                        foreach ($_POST['Item'] as $name => $rate) {
                            AppCompany::addContractSetting(array('contract_id' => $contract1->id, 'key' => $name, 'value' => $rate, 'category' => 'item', 'created_by' => AppUser::getUserId(), 'modified_by' => AppUser::getUserId(), 'created_at' => time(), 'modified_at' => time()));
                        }
                    }

                    AppLogging::addLog('Contract In Created ', 'success', 'application.contract.controller.main');
                    Yii::app()->user->setFlash('success', 'Contract Out has been created, you can view contract of ' . $contract1->user->first_name);
                    $approver = User::model()->findByPk($contract1->approver_id);
//                    email to timesheet approver
                    $mailer = new AppMailer();
                    $msg = 'You have been nominated by ' . AppUser::getCurrentUser()->first_name . ' to become their Timesheet approver on SpeedyPayrolls.';

                    if (isset($dummyCompany)) {
                        $msg .= "<br/><br/><a href='" . Yii::app()->createAbsoluteUrl("/contract/main/inviteview", array('id' => $dummyCompany['company']->id)) . "'>View Contract";
                    }


                    $mailer->prepareBody('Email', array('BODY' => $msg));
                    $mailer->sendMail(array(array('email' => $approver->email, 'name' => $approver->first_name)), array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls'));
                    $not_result = AppInterface::notification($contract1->user_id, 11, 0, $contract1->id, $contract1->user_id);
                    $notifier = new Notification();
                    $notification_count = $notifier->getUserNotificationUnseenCount();
                    $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
                    $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;

                    $notification = Yii::app()->getModule('notification');
                    $notification->send(array('server_notification', array('id' => $contract1->user_id, 'msg' => sprintf($not_result->notificationType->notice, $contract1->user->first_name), 'url' => $url, 'logo' => $logo, 'count' => $notification_count)), 'push');

                    $not_result = AppInterface::notification(AppUser::getUserId(), 14, 0, $contract1->id, $contract1->user->id);
                    $notifier = new Notification();
                    $notification_count = $notifier->getUserNotificationUnseenCount();
                    $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
                    $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;

                    $notification->send(array('server_notification', array('id' => AppUser::getUserId(), 'msg' => sprintf($not_result->notificationType->notice, $contract1->user->full_name, AppUser::getUserCompany($contract1->user->id)->name), 'url' => $url, 'logo' => $logo, 'count' => $notification_count)), 'push');

                    $not_result = AppInterface::notification(AppUser::getUserCompany($contract1->user_id)->admin->id, 13, 0, $contract1->id, $contract1->user_id);
                    $notification_count = $notifier->getUserNotificationUnseenCount($contract1->company->admin->id);
                    $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
                    $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;

                    $notification->send(array('server_notification', array('id' => AppUser::getUserCompany($contract1->user_id)->admin->id, 'msg' => sprintf($not_result->notificationType->notice, $contract1->user->first_name, AppUser::getUserCompany($contract1->user_id)->name), 'url' => $url, 'logo' => $logo, 'count' => $notification_count)), 'push');

                    $msg = ConstantMessages::contractApprover();
                    $not_result = AppInterface::notification($approver_id, 3, 0, $contract1->id, $contract1->user_id);
                    $notifier = new Notification();
                    $notification_count = $notifier->getUserNotificationUnseenCount($approver_id);
                    $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
                    $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;

                    $notification = Yii::app()->getModule('notification');
                    $notification->send(array('server_notification', array('id' => $approver_id, 'msg' => sprintf($not_result->notificationType->notice, AppUser::getCurrentUser()->first_name), 'url' => $url, 'logo' => $logo, 'count' => $notification_count)), 'push');

//                    email to super admin
                    $mailer2 = new AppMailer();
                    $msg2 = $approver->first_name . ' has been nominated by ' . AppUser::getCurrentUser()->getfull_name() . ' to become timesheet approver for ' . $contract1->user->first_name . '.';


                    if (isset($dummyCompany)) {
                        $msg2 .= "<br/><br/><a href='" . Yii::app()->createAbsoluteUrl("/contract/main/inviteview", array('id' => $dummyCompany['company']->id)) . "'>View Contract";
                    }


                    $mailer2->prepareBody('Email', array('BODY' => $msg2));
                    $mailer2->sendMail(array(array('email' => AppUser::get_super_user()->email, 'name' => AppUser::get_super_user()->email)), array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls'));
                    $not_result = AppInterface::notification(AppUser::get_super_user()->id, 10, 0, $contract1->id, $contract1->user_id);
                    $notifier = new Notification();
                    $notification_count = $notifier->getUserNotificationUnseenCount(AppUser::get_super_user()->id);
                    $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
                    $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;

                    $notification->send(array('server_notification', array('id' => AppUser::get_super_user()->id, 'msg' => sprintf($not_result->notificationType->notice, $approver->first_name, AppUser::getCurrentUser()->getfull_name(), $contract1->user->first_name), 'url' => $url, 'logo' => $logo, 'count' => $notification_count)), 'push');

                    $this->redirect(array('/contract/main/index'));
                }
            }
        }

        $this->render('contract-in', array(
            'approvers' => AppContract::get_all_internal_users(),
            'contractors' => User::model()->findAll(),
            'contract' => $contract1,
        ));
    }

    public function actionInOut() {
        $contract = new Contract;

        if (isset($_POST['Contract'])) {

            if (!empty($_POST['cc2']['company_name']) && !empty($_POST['cc2']['user_name']) && !empty($_POST['cc2']['user_email'])) {
                $pwd = User::model()->createRandomPassword();
                $_POST['cc2']['password'] = md5($pwd);
                $_POST['cc2']['pwd'] = $pwd;
                $lookgCompany = AppCompany::addDummyCompany($_POST['cc2']);

                //Assign Plan and Privileges to the company
                $set_company_priviledges = AppCompany::setCompanyPlan($lookgCompany->id, 29450400);

                if ($_POST['approver_id'] == '') {
                    $_POST['approver_id'] = $lookgCompany['user_id'];
                }
            }


            if (!empty($_POST['cc']['company_name']) && !empty($_POST['cc']['user_name']) && !empty($_POST['cc']['user_email'])) {
                $dummycompany = AppCompany::addCompanyWithAddress($_POST['cc'], 1, 0);

                if (isset($dummycompany)) {
                    AppUser::assignDefaultTemplate($dummycompany->id);

                    //Assign Plan and Privileges to the company
                    $set_company_priviledges = AppCompany::setCompanyPlan($dummycompany->id, 29450400);

                    $pwd = User::model()->createRandomPassword();
                    $dummyuser = AppUser::addUser($_POST['cc']['user_name'], $_POST['cc']['user_email'], md5($pwd), '', '', 1, 0, 29450400);

                    if (isset($dummyuser)) {
                        $user_id = $dummyuser->id;
                        //Add Employee Contract
                        $contract = AppCompany::addContract(array('user_id' => $user_id, 'approver_id' => $user_id, 'company_id' => $dummycompany->id, 'created_at' => time(), 'created_by' => '0', 'modified_at' => time(), 'modified_by' => '0', 'start_time' => time(), 'end_time' => -1, 'status' => 0, 'type' => 'in', 'collection_type' => $_POST['Contract']['collection_type'] == 1 ? ($_POST['Settings']['collection_day_first'] == 1 ? 1 : 2) : 0, 'collection_day' => $_POST['Contract2']['collection_day'], 'role' => 'Employee', 'is_internal' => 1, 'owner_id' => $dummyuser->id));

                        $internalcontract = $contract;
                        $parent_id = $contract->id;

                        //Add Admin Contract
                        $admincontract = AppCompany::addContract(array('user_id' => $user_id, 'approver_id' => $user_id, 'company_id' => $dummycompany->id, 'created_at' => time(), 'created_by' => '0', 'modified_at' => time(), 'modified_by' => '0', 'start_time' => time(), 'end_time' => -1, 'status' => 0, 'type' => 'in', 'collection_type' => $_POST['Contract']['collection_type'] == 1 ? ($_POST['Settings']['collection_day_first'] == 1 ? 1 : 2) : 0, 'collection_day' => $_POST['Contract2']['collection_day'], 'role' => 'admin', 'is_internal' => 1, 'invitation_code' => User::createRandomPassword(20)));

                        $url = $this->createAbsoluteUrl('/user/main/signup') . '/invite/' . $admincontract->invitation_code . '/contract/' . $admincontract->id;

                        $mailer3 = new AppMailer();
                        $mailer3->prepareBody('invite_admin', array('NAME' => $dummyuser->first_name, 'INVITED_BY' => AppUser::getCurrentUser()->full_name, 'USERNAME' => $dummyuser->email, 'PASSWORD' => $pwd, 'LINK' => Yii::app()->createAbsoluteUrl('/user/main/login')));

                        $mailer3->sendMail(array(array('email' => $dummyuser->email, 'name' => $dummyuser->first_name)), array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls'));
                    } else {
                        dd($dummyuser->errors);
                    }
                }
            } else {
                $internalcontract = AppContract::getInternalContract($_POST['Contract']['user_id']);
                $user_id = $_POST['Contract']['user_id'];
            }
            $file_name = null;
            if ($_FILES['Contract']['name']['file_name']) {
                $file_name = CUploadedFile::getInstance($contract, 'file_name');
                $images_path = realpath(Yii::app()->basePath . '/../uploads/contracts');
                $filename = explode('.', $file_name);
                $filename[0] = AppInterface::getFilename();
                $newfilename = $filename[0] . '.' . $filename[1];
                $file_name->saveAs($images_path . '/' . $newfilename);
                $file_name = $newfilename;
            }
            $payroller_id = 0;
            if (AppUser::canPayroll()) {
                $payroller_id = AppUser::getUserCompany($user_id, false)->payroller_id;
            }

            //Add Employee Contract
            $contract2 = AppCompany::addContract(array('user_id' => $user_id, 'policy' => $_POST['Contract']['policy'], 'file_name' => $file_name, 'approver_id' => $_POST['Contract']['approver_id'], 'company_id' => AppUser::getUserCompany()->id, 'created_at' => time(), 'created_by' => '0', 'modified_at' => time(), 'modified_by' => '0', 'start_time' => time(), 'end_time' => -1, 'status' => 'active', 'type' => 'in_out', 'role' => 'Employee', 'is_internal' => 1, 'owner_id' => AppUser::getUserId(), 'parent_id' => $internalcontract->id, 'payroller_id' => $payroller_id));

            if (isset($lookgCompany) && $lookgCompany != null)
                $company_id = $lookgCompany['company']->id;
            else
                $company_id = $_POST['Contract2']['company_id'];
            $payroller_id2 = 0;
            if (AppUser::canPayroll()) {
                $payroller_id2 = AppUser::getUserCompany($user_id, false)->payroller_id;
            }
            $contract3 = AppCompany::addContract(array('user_id' => $user_id, 'approver_id' => $_POST['Contract']['approver_id'], 'company_id' => $company_id, 'created_at' => time(), 'created_by' => '0', 'modified_at' => time(), 'modified_by' => '0', 'start_time' => time(), 'end_time' => -1, 'status' => 'pending', 'type' => 'out_b', 'role' => 'Employee', 'is_internal' => 0, 'owner_id' => AppUser::getUserId(), 'parent_id' => $contract2->id, 'payroller_id' => $payroller_id2, 'collection_type' => $_POST['Contract']['collection_type'] == 1 ? ($_POST['Settings']['collection_day_first'] == 1 ? 1 : 2) : 0, 'collection_day' => $_POST['Contract2']['collection_day']));

            if (isset($contract2) && isset($contract3)) {

                if (isset($_POST['Payrate1'])) {
                    foreach ($_POST['Payrate1'] as $name => $rate) {
                        AppCompany::addContractSetting(array('contract_id' => $contract2->id, 'key' => $name, 'value' => $rate, 'category' => 'payrate', 'created_by' => AppUser::getUserId(), 'modified_by' => AppUser::getUserId(), 'created_at' => time(), 'modified_at' => time()));
                    }
                }
                if (isset($_POST['Item1'])) {
                    foreach ($_POST['Item1'] as $name => $rate) {
                        AppCompany::addContractSetting(array('contract_id' => $contract2->id, 'key' => $name, 'value' => $rate, 'category' => 'item', 'created_by' => AppUser::getUserId(), 'modified_by' => AppUser::getUserId(), 'created_at' => time(), 'modified_at' => time()));
                    }
                }

                if (isset($_POST['Payrate2'])) {
                    foreach ($_POST['Payrate2'] as $name => $rate) {
                        AppCompany::addContractSetting(array('contract_id' => $contract3->id, 'key' => $name, 'value' => $rate, 'category' => 'payrate', 'created_by' => AppUser::getUserId(), 'modified_by' => AppUser::getUserId(), 'created_at' => time(), 'modified_at' => time()));
                    }
                }
                if (isset($_POST['Item2'])) {
                    foreach ($_POST['Item2'] as $name => $rate) {
                        AppCompany::addContractSetting(array('contract_id' => $contract3->id, 'key' => $name, 'value' => $rate, 'category' => 'item', 'created_by' => AppUser::getUserId(), 'modified_by' => AppUser::getUserId(), 'created_at' => time(), 'modified_at' => time()));
                    }
                }
                $approver = User::model()->findByPk($contract2->approver_id);
//                email to approver
                $mailer = new AppMailer();
                $msg = 'You have been nominated by ' . AppUser::getCurrentUser()->first_name . ' to become their Timesheet approver on SpeedyPayrolls.';
                $mailer->prepareBody('Email', array('BODY' => $msg));
                $mailer->sendMail(array(array('email' => $approver->email, 'name' => $approver->first_name)), array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls'));
                $not_result = AppInterface::notification($contract2->user_id, 8, 0, $contract2->id, $contract2->user_id);
                $notifier = new Notification();
                $notification_count = $notifier->getUserNotificationUnseenCount();
                $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
                $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;

                $notification = Yii::app()->getModule('notification');
                $notification->send(array('server_notification', array('id' => $contract2->user_id, 'msg' => sprintf($not_result->notificationType->notice, $contract2->user->first_name), 'url' => $url, 'logo' => $logo, 'count' => $notification_count)), 'push');

                //Send notification to contractor company
                $not_result = AppInterface::notification($contract2->user_id, 20, 0, $contract2->id, $contract2->user_id);
                $notifier = new Notification();
                $notification_count = $notifier->getUserNotificationUnseenCount($contract2->user_id);
                $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
                $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;

                $notification->send(array('server_notification', array('id' => $contract2->user_id, 'msg' => sprintf($not_result->notificationType->notice, $contract2->user->first_name, AppUser::getUserCompany($contract2->user_id)->name), 'url' => $url, 'logo' => $logo, 'count' => $notification_count)), 'push');

                //Send notification to client company
                $client_company_admin = AppCompany::getCompanyAdmin($company_id);
                $not_result = AppInterface::notification($client_company_admin->id, 21, 0, $contract3->id, $contract2->user_id);
                $notifier = new Notification();
                $notification_count = $notifier->getUserNotificationUnseenCount($client_company_admin->id);
                $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
                $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;

                $notification->send(array('server_notification', array('id' => $client_company_admin->id, 'msg' => sprintf($not_result->notificationType->notice, $contract2->user->first_name, AppUser::getUserCompany($contract2->user_id)->name), 'url' => $url, 'logo' => $logo, 'count' => $notification_count)), 'push');

                $not_result = AppInterface::notification($approver->id, 3, 0, $contract2->id, $contract2->user_id);
                $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
                $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;
                $notifier = new Notification();
                $notification_count = $notifier->getUserNotificationUnseenCount($contract2->approver_id);
                $msg = ConstantMessages::contractApprover();

                $notification->send(array('server_notification', array('id' => $contract2->approver_id, 'msg' => sprintf($not_result->notificationType->notice, AppUser::getCurrentUser()->first_name), 'url' => $url, 'logo' => $logo)), 'push');

//                email to super admin
                $mailer2 = new AppMailer();
                $msg2 = $approver->first_name . ' has been nominated by ' . User::model()->getfull_name() . ' to become timesheet approver for ' . User::model()->findByPk($contract2->user_id)->first_name . '.';
                $mailer2->prepareBody('Email', array('BODY' => $msg2));
                $mailer2->sendMail(array(array('email' => AppUser::get_super_user()->email, 'name' => AppUser::get_super_user()->email)), array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls'));
                $not_result = AppInterface::notification(AppUser::get_super_user()->id, 10, 0, $contract2->id, $contract2->user_id);
                $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
                $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;
                $notifier = new Notification();
                $notification_count = $notifier->getUserNotificationUnseenCount(AppUser::get_super_user()->id);

                $notification->send(array('server_notification', array('id' => AppUser::get_super_user()->id, 'msg' => sprintf($not_result->notificationType->notice, $approver->first_name, AppUser::getCurrentUser()->full_name, $contract2->user->first_name), 'url' => $url, 'logo' => $logo, 'count' => $notification_count)), 'push');

//                email to user
                $mailer3 = new AppMailer();
                $msg3 = User::model()->findByPk($contract2->user_id)->first_name . ' has a new Speedy Timesheet account created by ' . AppUser::getCurrentUser()->full_name . 'Please login to your SpeedyPayrolls account for further information.';
                $mailer3->prepareBody('Email', array('BODY' => $msg3));
                $mailer3->sendMail(array(array('email' => User::model()->findByPk($contract2->user_id)->email, 'name' => User::model()->findByPk($contract2->user_id)->first_name)), array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls'));

                Yii::app()->user->setFlash('success', 'Contract Out has been created, you can view contract of ' . $contract2->user->first_name);
                AppLogging::addLog('Contract In-Out Created ', 'success', 'application.contract.controller.main');

                $this->redirect(array('/contract/main/index'));
            }
        }
        $this->render('contract-in-out', array(
            'employees' => AppContract::get_all_internal_users(),
            'approvers' => AppContract::get_all_internal_users(),
            'companies' => AppCompany::getAllCompanies(true),
            'contractors' => User::model()->findAll(),
            'contract' => $contract,
        ));
    }

    public function actionOutB() {
        $this->render('contract-out-b');
    }

    public function actionNew() {
        $id = $_REQUEST['id'];
        if (isset($_POST['type'])) {
            $type = $_POST['type'];
            $this->redirect(array('/contract/main/' . $type));
        }
        $this->render('new', array('id' => $id));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Contract the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Contract::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionContract() {
        $user = AppUser::getCurrentUser();
        if (isset($user)) {
            $privileges = Yii::app()->session['privileges'];
            $two_way_in = false;
            $two_way_out = false;
            $two_way = false;
            $three_way = false;
//            dd($two_way);
            if ((AppUser::searchCompanyPrivilege("Create_Two_Way_Contract_Out", $privileges)) || (AppUser::searchCompanyPrivilege("Create_Two_Way_Contract", $privileges))) {
                $two_way_out = true;
                $two_way = true;
            }
            if ((AppUser::searchCompanyPrivilege("Create_Two_Way_Contract_In", $privileges)) || (AppUser::searchCompanyPrivilege("Create_Two_Way_Contract", $privileges))) {
                $two_way_in = true;
                $two_way = true;
            }
            if (AppUser::searchCompanyPrivilege("Create_3Way_Contract", $privileges)) {
                $three_way = true;
            }
            $token = AppUser::getCurrentUser()->token;
            $this->render('contract', array(
                'employees' => AppContract::get_all_internal_users(),
                'approvers' => AppContract::get_all_internal_users(),
                'token' => $token,
                'two_way' => $two_way,
                'two_way_in' => $two_way_in,
                'two_way_out' => $two_way_out,
                'three_way' => $three_way,
            ));
        } else {
            $this->redirect(array('/user/main/login'));
        }
    }

    /**
     * Performs the AJAX validation.
     * @param Contract $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'contract-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
