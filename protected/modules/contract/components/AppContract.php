<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AppContract
 *
 * @author usman
 */
class AppContract extends CComponent {

    public static function get_all_in_users() {
        if ($company = AppUser::getUserCompany()) {
            $c = new CDbCriteria();
            $c->join = "LEFT JOIN sp_contract con on con.user_id = t.id";
            $c->addCondition('con.company_id = :company_id '); //AND con.type = :contype');
            $c->params = array(
                ':company_id' => $company->id,
            );
            $users = User::model()->findAll($c);
            if ($users) {
                $result = array();
                foreach ($users as $user) {
                    $result[$user->id] = $user;
                }
                return $result;
            }
        }
        return null;
    }

    public static function get_all_internal_users() {
        if ($company = AppUser::getUserCompany()) {
            $c = new CDbCriteria();
            $c->join = "LEFT JOIN sp_contract con on con.user_id = t.id";
            $c->addCondition('con.company_id = :company_id AND con.is_internal = :isin');
            $c->params = array(
                ':company_id' => $company->id,
                ':isin' => '1',
            );
            $c->order= 't.first_name asc';
            $users = User::model()->findAll($c);
            if ($users) {
                $result = array();
                foreach ($users as $user) {
                    $result[$user->id] = $user;
                }
                return $result;
            }
        }
        return null;
    }

    public static function can_admin_view_out_b(&$contract) {
        if (AppUser::isUserAdmin()) {
            return !($contract->type == 'out_b' && $contract->parent->company_id == AppUser::getUserCompany()->id);
        }
        return true;
    }


    public static function get_all_contract_by_company($cid = '') {
        if ($company = AppUser::getUserCompany()) {

            if ($cid == '' && !empty($company)) {
                $cid = $company->id;
            } else {
                return null;
            }

            $c = new CDbCriteria();
            $c->addCondition('t.role != :role OR t.role IS NULL');
            $c->addCondition('t.company_id = :cid OR parent.company_id = :cid');
            $c->join = 'LEFT JOIN sp_contract parent on parent.id = t.parent_id';
            $c->params = array(
                ':role' => 'admin',
                ':cid' => $cid,
            );
            $c->order = "t.created_at desc";

            $contracts = Contract::model()->findAll($c);
            if ($contracts) {
                return $contracts;
            }
        }
        return null;
    }

    public static function getContractSetting($contract_id) {
       $contract = ContractSetting::model()->findAllByAttributes(array('contract_id' => $contract_id));
        if (isset($contract)) {
            return $contract;
        }
    }
    public static function getInternalContract($employee_id) {
        if ($user = User::model()->findByPk($employee_id)) {
            return Contract::model()->findByAttributes(array('user_id' => $user->id, 'is_internal' => 1), 'role <> "admin"');
        }
    }
    public static function getContract($contract_id) {
            return Contract::model()->findByPk($contract_id);
    }
    public static function getEmployeeContracts($id,$isAddTimesheet = true, $type=null) {
        $c = new CDbCriteria();

        $c->addCondition('t.user_id = :uid');
        $c->addCondition('t.role != :role OR t.role IS NULL');
        $c->params = array(
            ':uid' => $id,
            ':role' => 'admin',
        );

        if (AppUser::isUserAdmin() && $isAddTimesheet) {
            $c->addCondition('t.company_id = :cid OR parent.company_id = :cid');
            $c->params[':cid'] = AppUser::getUserCompany()->id;
            if($type){
                if($type=="out_b"){
                    $c->addCondition('t.type="in_out" OR t.type="out_b"');
                }
                else{
                $c->addCondition('t.type=:cType');
                $c->params[':cType'] = $type;
                }
            }
            $c->join = 'LEFT JOIN sp_contract parent on parent.id = t.parent_id';
        } else {
            if($type){
                if($type=="out_b"){
                    $c->addCondition('t.type="in_out" OR t.type="out_b"');
                }
                else{
                $c->addCondition('t.type=:cType');
                $c->params[':cType'] = $type;
                }
            }
            else{
                $c->addCondition('t.type <> :cType');
                $c->params[':cType'] = "in_out";
            }
        }
        $c->order = "t.created_at desc";
        $contracts = Contract::model()->findAll($c);
        return $contracts;
    }

    public static function getAllContracts($uid = '', $args = null) {


        if ($uid == '' && !empty(Yii::app()->user->id)) {
            $uid = Yii::app()->user->id;
        } elseif ($uid == '') {
            return null;
        }

        $attributes = array('user_id' => $uid);

        if ($args != null) {
            $attributes = array_merge($attributes, $args);
        }

        $c = new CDbCriteria();
        $c->addCondition('t.role != :role OR t.role IS NULL');
        $c->params = array(
            ':role' => 'admin',
        );

        $contracts = Contract::model()->findAllByAttributes($attributes, $c);


        return $contracts;
    }

    public static function formatEmployeType($str) {
        return ucwords(str_replace('_', ' ', $str));
    }

    public static function getContractSettings($contractId, $type = 'payrate') {
        $criteria = new CDbCriteria();
        $criteria->addCondition(' t.contract_id = :contractId AND category= :category');
        $criteria->params = array(":contractId" => $contractId, ":category" => $type);

        $cSettings = ContractSetting::model()->findAll($criteria);
        return $cSettings;
    }

    public static function getContractSettingsByKey($contractId, $key = '', $type = 'payrate') {
        $criteria = new CDbCriteria();
        $criteria->select = "t.*";
        $criteria->addCondition("t.contract_id = :conId");
        $criteria->addCondition("t.category = :category");
        $criteria->addCondition("t.key LIKE ('%" . $key . "%')");
        $criteria->params = array(":conId" => $contractId, ':category' => $type);

        $cSettings = ContractSetting::model()->find($criteria);
        return $cSettings;
    }

    public static function getContractPayrates($cid) {
        $items = ContractSetting::model()->findAllByAttributes(array(
            'contract_id' => $cid,
            'category' => 'payrate',
        ));
        $result = array();
        foreach ($items as $item) {
            $result[$item->key] = $item;
        }
        return $result;
    }

    public static function getContractItems($cid) {
        $items = ContractSetting::model()->findAllByAttributes(array(
            'contract_id' => $cid,
            'category' => 'item',
        ));
        $result = array();
        foreach ($items as $item) {
            $result[$item->key] = $item;
        }
        return $result;
    }

    public static function getContractSettingsByID($Id) {
        return ContractSetting::model()->findByPk($Id);
    }

    public static function getContractCustomName($contract) {
        if ($contract->is_internal == true)
            return $contract->user->first_name . ' (Internal - ' . $contract->company->name . ')';
        else
            return $contract->user->first_name . ' (' . ucwords(str_replace('_', ' ', $contract->type)) . ' - ' . $contract->company->name . ')';
    }

    public static function getContractCustomType(&$contract) {
        if (AppUser::isUserAdmin()) {
            if ($contract->is_internal && $contract->company_id == AppUser::getUserCompany()->id) {
                return 'internal';
            } else if ($contract->type != 'in' && $contract->type != 'in_out' && $contract->company_id == AppUser::getUserCompany()->id) {
                if ($contract->parent)
                    return 'in';
            }
        }
        return $contract->type;
    }

}
