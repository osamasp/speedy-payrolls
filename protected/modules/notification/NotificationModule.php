<?php

class NotificationModule extends CWebModule {

    public $childs = null;
    private $compArray = null;

    public function init() {

        // this method is called when the module is being created
        // you may place code here to customize the module or the application
        // import the module-level models and components
        $this->setImport(array(
            'notification.models.*',
            'notification.components.*',
        ));
//        dd($this->childs);

        /*         * ******* Setup Component Elements **** */
        if ($this->childs != null) {
            foreach ($this->childs as $comp) {

                if (isset($this->$comp) && $this->$comp != null)
                    $this->compArray[] = $this->$comp;
            }
        }
        /*         * ******************************** */
    }

    public function beforeControllerAction($controller, $action) {
        if (parent::beforeControllerAction($controller, $action)) {
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        } else
            return false;
    }

    /*     * ********** */

    //Usage:
    //1) Sending Both Notification Push and Email
    //      $notification = Yii::app()->getModule('notifications');
    //      $notification->send($data);
    //
    //2) Sending Only One Notification
    //      $notification = Yii::app()->getModule('notifications');
    //      $notification->send($data,'push');
    // OR
    //      $notification->send($data,'email');
    //
    //Paramaters:
    //
    //      $data = array( 'email'=>array(
    //                                     'template'=>'SignUpEmail',
    //                                      'params'=>array()
    //                                      'to'=>array(),
    //                                      'from'=>array(),
    //                                       'cc'=>array(),
    //                                       'bcc'=>array(),
    //                                     ),
    //                     'push' => array(
    //                                 'message'=>array('title'=>'Following Request','content'=>'Someone is following you'), /// Used For Message alerts
    //                                  'uid' => array('uayaz@amearld.com'),                                                 /// can be an array
    //                                  'params' => array('sound'=>'cheering.caf')                                                                  /// Extra Params related to IOS and Andriod Sets here such as sound badge etc etc.
    //                                  ),
    //                    );
    //
    //       $type: both, push, email
    //
    ///**************/


    public function send($data) {
        foreach ($data as $key => $value) {
            $component = $this->getChildren($key);
            if (isset($component)) {
                $component->send($value);
            }
        }
    }

    public function getChildren($type) {
        if ($this->compArray != null) {
            foreach ($this->compArray as $component) {
                if ($component->getType() == $type) {
                    return $component;
                }
            }
        }
    }

}
