<?php

/*
 * Description: Parse Component for Notification Module
 * Author: Usama Ayaz
 * Dated:  11-Nov-2014
 */

use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;

class ParseNotify extends CApplicationComponent {

    public $userFollower = "";
    public $appId;
    public $rest_key;
    public $master_key;
    private $type = 'push';

    public function __construct($ip = "") {
        
    }

    public function init() {

        parent::init();
        $this->registerScripts();
    }

    public function registerScripts() {
        Yii::import('application.modules.notification.extensions.ParseSDK.autoload', true);
        ParseClient::initialize($this->appId, $this->rest_key, $this->master_key);
    }
    
    public function getType(){
        return $this->type;
        
    }
    
    
    
    
    

    public function send($input) {
        
        /**** Use Only Push Related Information */
         if(isset($input['push'])){
             
                $usageData = $input['push'];
               
                $query = ParseInstallation::query();

                    $data = array(
                               "alert" => $usageData['message']['content'],
                               "badge" => "Increment",
                               "sound" => $usageData['params']['sound'],
                               "title" => $usageData['message']['title']
                           );
                    
                    
                    if(isset($usageData['params']['callBack']))
                    $data['callBack'] = "".$usageData['params']['callBack'];
                    
                    
                   if(isset($usageData['params']['skey']))
                   $data['skey'] = $usageData['params']['skey']; 
                 

                if(isset($usageData['uid']) && $usageData['uid']!=null){

                    if(is_array($usageData['uid'])){
                       $query->containedIn("UID", $usageData['uid']);  
                    }else{
                       $query->equalTo("UID", $usageData['uid']);
                    }


                }else{  /******** Send it To All **********/
                   // $query->equalTo("appName", 'BCast');
                    $query->containedIn("appName", array('BCast','bcast','BCAST')); 
                }
//                d(ParsePush::send(array(
//                                            "where" => $query,
//                                            "data" => $data
//                                        )));
                  
                return ParsePush::send(array(
                                            "where" => $query,
                                            "data" => $data
                                        ));
                
               
        
        }else{
            
           return null; 
        }
        
    }

}

?>
