<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 *
 *  $mailer = new MailerNotify();
 *  $mailer->prepareBody('SignUpEmail',array('USER'=>'John Doe', 'ACTIVATIONKEY' => '121312{8ASDASQZqqw' )  );
 *  $mailer->sendMail(array('email'=>'john@abc.com' , 'name'=>'john doe') , array('email'=>'support@site.com' name=>'Support'));
 *
 *
 */

class MailerNotify extends AppMailer
{
    public $smtp = null;
    private $type = 'email';
    
    public function init(){
        
    }


    public function getType(){
        return $this->type;
        
    }
    public function send($input){
        
         /**** Use Only Email Related Information */
         if(isset($input['email'])){
             $mail = new AppMailer();
                $usageData = $input['email'];
                $mail->prepareBody($usageData['template'],$usageData['params'] );
                $cc = (isset($usageData['cc']))?$usageData['cc']:array();
                $bcc = (isset($usageData['bcc']))?$usageData['bcc']:array();
                return $mail->sendMail($usageData['to'], $usageData['from'],$cc,$bcc);
                
         } else{
             return null;
         }
        
    }

}


?>
