<?php

use \ElephantIO\Client,
  \ElephantIO\Engine\SocketIO\Version1X;

class PushNotify extends CApplicationComponent {

	/**
	 * @var string path for elephant io library, should be in extensions directory
	 */
	public $elephantIOExtDir = 'elephant.io';
	/**
	 * @var string
	 */
	public $host;
	public $host2;

	/**
	 * @var integer|string
	 */
	public $port;

	/**
	 * @var int in miliseconds
	 */
        
        private $type = 'push';
	public $handshakeTimeout = 3000;
        
	public function init() {
		parent::init();
		include_once Yii::getPathOfAlias('application.modules.notification.extensions') . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, array(
			$this->elephantIOExtDir,
			'vendor',
			'autoload.php'
		));
	}

	/**
	 * @param null $host
	 * @param null $port
	 *
	 * @return \ElephantIO\Client
	 */
	public function createClient($host = null, $port = null) {
		if (!isset($host)) {
			$host = $this->host;
		}
		if (!isset($port)) {
			$port = $this->port;
		}
		return new \ElephantIO\Client(
			sprintf('http://', $host, $port),
			'socket.io',
			1,
			false
		);
	}
        
        
    public function getType(){
        return $this->type;
        
    }

	/**
	 * @param string $path namespace on nodejs socket.io server
	 * @param string $event event name in current namespace
	 * @param mixed  $data event data
	 * @param mixed  $data,...
	 *
	 */
	public function send($data = array()) {
            if(isset($data[1]['id'])){
                $user_devices = UserDevices::model()->findAllByAttributes(array("user_id" => $data[1]['id']));
                foreach($user_devices as $item){
                    if($item->device_type == USER_DEVICE_IOS){
                        self::PushIos($item->device_id, AppUser::getCurrentUser()->first_name, "SPEEDY NOTIFICATION", $data[1]['msg']);
                    } else if($item->device_type == USER_DEVICE_ANDROID){
                        self::PushAndroid($item->device_id, AppUser::getCurrentUser()->first_name, "SPEEDY NOTIFICATION", $data[1]['msg']);
                    }
                }
//            $host = sprintf("%s",$this->host2);
//            $client = new Client(new ElephantIO\Engine\SocketIO\Version1X($host));
//            $client->initialize();
//            $client->emit($data[0],$data[1]);
//            $client->close();
            }
	}
        
    public static function PushIos($device, $Sendername, $title, $msg) {

        $payload['aps'] = array('alert' => $title, 'Sendername' => $Sendername, 'message' => $msg, 'sound' => 'default');
        //$payload = json_encode($payload);
        $payload = json_encode($payload, JSON_UNESCAPED_UNICODE);
        $options = array('ssl' => array(
                'local_cert' => Yii::app()->getBasePath().'/../external_assets/apns_cert/APNSCertificates.pem'
        ));

        $streamContext = stream_context_create();
        stream_context_set_option($streamContext, $options);
        $apns = stream_socket_client('ssl://gateway.push.apple.com:2195', $error, $errorString, 60, STREAM_CLIENT_CONNECT, $streamContext);

        $apnsMessage = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $device)) . chr(0) . chr(strlen($payload)) . $payload;

        fwrite($apns, $apnsMessage);
        fclose($apns);
    }

    public static function PushAndroid($device, $Sendername, $title, $msg) {

        $url = 'https://android.googleapis.com/gcm/send';
        $serverApiKey = "AIzaSyCg-8PXOB57tlBbXv_U4a7BNJ7d4HrnbNk";
        $reg = $device;
        $message = $msg;

        $headers = array(
            'Content-Type:application/json',
            'Authorization:key=' . $serverApiKey
        );

        $data = array(
            'registration_ids' => array($reg),
            'data' => array('alert' => $title,
                'Sendername' => $Sendername,
                'msg' => $message,
                'url' => $url
            )
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        if ($headers)
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        $response = curl_exec($ch);
        curl_close($ch);
    }

}
