<?php

/**
 * This is the model class for table "sp_notification".
 *
 * The followings are the available columns in table 'sp_notification':
 * @property string $id
 * @property string $title
 * @property string $notice
 * @property integer $is_read
 * @property string $sender_id
 * @property string $receiver_id
 * @property string $created_at
 * @property string $modified_at
 * @property string $created_by
 * @property string $modified_by
 * @property string $status
 * @property string $timesheet_id
 * @property string $contract_id
 * @property string $user_id
 * @property string $notification_type_id
 *
 * The followings are the available model relations:
 * @property User $sender
 * @property User $receiver
 * @property NotificationType $notificationType
 */
class Notification extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'sp_notification';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('is_read', 'numerical', 'integerOnly' => true),
            array('title', 'length', 'max' => 255),
            array('sender_id, receiver_id, created_at, modified_at, created_by, modified_by, timesheet_id, contract_id, notification_type_id', 'length', 'max' => 20),
            array('status', 'length', 'max' => 7),
            array('notice', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, title, notice, is_read, sender_id, receiver_id, created_at, modified_at, created_by, modified_by, status, timesheet_id, contract_id, user_id, notification_type_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'sender' => array(self::BELONGS_TO, 'User', 'sender_id'),
            'receiver' => array(self::BELONGS_TO, 'User', 'receiver_id'),
            'notificationType' => array(self::BELONGS_TO, 'NotificationType', 'notification_type_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'title' => 'Title',
            'notice' => 'Notice',
            'is_read' => 'Is Read',
            'sender_id' => 'Sender',
            'receiver_id' => 'Receiver',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
            'created_by' => 'Created By',
            'modified_by' => 'Modified By',
            'status' => 'Status',
            'timesheet_id' => 'Timesheet',
            'contract_id' => 'Contract',
            'user_id' => 'User',
            'notification_type_id' => 'Notification Type',
        );
    }
    
    public function checkUserMessageExist($user_message, $user_id, $message_id)
    {
        foreach ($user_message as $item)
        {
            if($item->receiver_id == $user_id && $item->message_id == $message_id)
            {
                return true;
            }
        }
        return false;
    }

    public function getUserNotifications($id = '') {
        if ($id == '') {
            $id = AppUser::getUserId();
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.receiver_id = :id');

//        if (!(AppUser::isUserSuperAdmin() || AppUser::isUserPayroller())) {
//            $criteria->addCondition('sender_id = ' . AppUser::getUserCompany()->admin->id);
//        }

        $criteria->params = array(':id' => $id);
        $criteria->order = "created_at desc";

        $notifications = self::model()->findAll($criteria);
        return $notifications;
    }

    public function getUserNotificationUnseenCount($id = '',$message=false) {
        if($id == '')
        {
            $id = AppUser::getUserId();
            $user = AppUser::getCurrentUser();
        }
        else{
            $user = AppUser::getUserById($id);
        }
        $user->id = $id;
        if($message)
        {
            $data = self::model()->findAll(array('condition'=>'created_at>:time AND receiver_id=:id','params'=> array(':time' => $user->notif_seen_at, ':id' => $user->id)));
            $company = AppUser::getUserCompany($id);
            if(isset($company->admin)){
            $sender_id = $company->admin->id;
            }
            $count = count($data);
            $result = self::model()->findall(array('condition' => 'created_at>:time AND receiver_id=0 AND sender_id=:sender','params' => array(':time' => $user->notif_seen_at, ':sender' => $sender_id)));
            $count = $count + count($result);
            return $count;
        }
        else{
            $data = self::model()->findAll(array('condition'=>'created_at>:time AND receiver_id=:id','params'=> array(':time' => $user->notif_seen_at, ':id' => $user->id)));
        }
        return count($data);
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('notice', $this->notice, true);
        $criteria->compare('is_read', $this->is_read);
        $criteria->compare('sender_id', $this->sender_id, true);
        $criteria->compare('receiver_id', $this->receiver_id, true);
        $criteria->compare('created_at', $this->created_at, true);
        $criteria->compare('modified_at', $this->modified_at, true);
        $criteria->compare('created_by', $this->created_by, true);
        $criteria->compare('modified_by', $this->modified_by, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('timesheet_id', $this->timesheet_id, true);
        $criteria->compare('contract_id', $this->contract_id, true);
        $criteria->compare('user_id', $this->user_id, true);
        $criteria->compare('notification_type_id', $this->notification_type_id, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Notification the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
