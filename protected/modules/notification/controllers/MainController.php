<?php
use \ElephantIO\Client,
  \ElephantIO\Engine\SocketIO\Version1X;
class MainController extends Controller {

    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'updateunseentime', 'viewmessage', 'notification', 'markread', 'updateread'),
                'users' => array('@'),
            ),
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('companynotification'),
                'roles' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionViewMessage() {
        $notification_id = Yii::app()->request->getParam('notification_id');
        $model = Notification::model()->findByPk($notification_id);
//        $company_message = CompanyMessage::model()->findByAttributes(array('notification_id' => $notification_id));
//        if (isset($notification_id)) {
//            AppUser::updateNotificationStatus($notification_id);
//        }
        $this->render('view_message', array('model' => $model));
    }

    public function actionUpdateUnseenTime() {
        $model = AppUser::getCurrentUser();
        $model->notif_seen_at = time();
        $model->save();
        $count = Notification::model()->getUserNotificationUnseenCount();
        $response = array('status' => 1, 'data' => $count);
        AppSetting::generateAjaxResponse($response);
    }

    public function actionUpdateRead() {
        $id = $_POST['id'];
        $model = Notification::model()->findByPk($id);
        $model->is_read = 1;
        $model->save();
    }

    public function actionMarkRead($id) {
        $model = Notification::model()->findByPk($id);
        $model->is_read = 1;
        $model->save();
        echo $model->id;
    }

    public function actionCompanyNotification() {
//        dd($_GET['users']);
        $selected = null;
        if (isset($_GET['users'])) {
            $selected = $_GET['users'];
        }
        $employees = AppUser::get_all_users_by_company();
        if (isset($_POST["CompanyMessage"])) {
            $notifier = new Notification();
            $notification = Yii::app()->getModule('notification');
            foreach ($_POST["employee"] as $item) {
                $not_result = AppInterface::notification($item, 15, 0, 0, AppUser::getUserId(), $_POST["CompanyMessage"]["title"], $_POST["CompanyMessage"]["message"]);
                $notification_count = $notifier->getUserNotificationUnseenCount($item, true);
                $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
                $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;

                $notification->send(array(
                    'push' => array('company_message',
                        array('id' => $item, 'msg' => $_POST["CompanyMessage"]["message"],
                            'url' => $url, 'logo' => $logo, 'count' => $notification_count)
                    ))
                );
            }
            Yii::app()->user->setFlash('success', 'Message to the selected employees has been sent');
        }
        $this->render('company_message', array('employees' => $employees, 'selected' => $selected));
    }

    public function actionNotification() {
        
        $host = sprintf("%s:%s", config_settings::$node_host, config_settings::$node_port);
        $client = new Client(new ElephantIO\Engine\SocketIO\Version1X($host));
        $client->initialize();
        $client->emit('company_message', array('id' => AppUser::getCurrentUser()->id, 'msg' => "some message",
                            'url' => '', 'logo' => '', 'count' => 3));
        $client->close();
    }

    public function actionIndex() {
        $criteria = new CDbCriteria();
        $criteria->condition = '(receiver_id = ' . AppUser::getUserId() . ') AND status = \'active\'';
        $criteria->order = 'created_at desc';
        $count = Notification::model()->count($criteria);
        $pages = new CPagination($count);
        $pages->pageSize = 10;
        $pages->applyLimit($criteria);

        $notifications = Notification::model()->findAll($criteria);
        $this->render('index', array('notifications' => $notifications, 'pages' => $pages));
    }

    public function actionSending() {
        $data = array('email' => array(
                'template' => 'Email',
                'params' => array('BODY' => 'Testing Email'),
                'to' => array(array('email' => 'fahhd@msn.com', 'name' => 'fahd')),
                'from' => array('email' => 'support@speedypayrolls.com', 'name' => 'Speedy Payrolls'),
                'cc' => array(),
                'bcc' => array(),
            ),
        );
        $module = Yii::app()->getModule('notification');
        $module->send($data, 'email');
    }

    public function actionPush() {
//       $module = Yii::app()->getModule('notification');
//       $module->send('abcd','push');
        $abc = new PushNotify();
        $abc->emit('server_notification', array('id' => AppUser::getUserId()));
        $this->redirect(Yii::app()->createUrl('site/index'));
    }

    public function actionView($id) {
        $this->render('view');
    }

    public function actionDelete($id) {

        $notification = Notification::model()->findByPk($id);
        if ($notification) {
            $notification->status = 'deleted';
            if ($notification->update()) {
                $this->redirect(array('index'));
            }
        }
    }

    // Uncomment the following methods and override them if needed
    /*
      public function filters()
      {
      // return the filter configuration for this controller, e.g.:
      return array(
      'inlineFilterName',
      array(
      'class'=>'path.to.FilterClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }

      public function actions()
      {
      // return external action classes, e.g.:
      return array(
      'action1'=>'path.to.ActionClass',
      'action2'=>array(
      'class'=>'path.to.AnotherActionClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }
     */
}
