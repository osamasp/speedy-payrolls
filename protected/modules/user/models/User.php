<?php

/**
 * This is the model class for table "sp_user".
 *
 * The followings are the available columns in table 'sp_user':
 * @property string $id
 * @property string $first_name
 * @property string $last_name
 * @property string $full_name
 * @property string $email
 * @property string $password
 * @property string $ni_number
 * @property string $dob
 * @property string $phone
 * @property string $addressline_1
 * @property string $street
 * @property string $city
 * @property string $country
 * @property string $postcode
 * @property string $photo
 * @property integer $type
 * @property string $created_at
 * @property string $created_by
 * @property string $modified_at
 * @property string $modified_by
 * @property Contract $current_in_contract
 * @propoerty integer $is_verified
 * @property string $verification_code
 * @property integer $first_login
 * @property integer $plan_type
 * @property string $staff_id
 * @property string $shift_id
 * @property string $notif_seen_at
 * @property string $token
 * @property string $validity
 *
 * The followings are the available model relations:
 * @property Company[] $companies
 * @property Company[] $companies1
 * @property Contact[] $contacts
 * @property Contact[] $contacts1
 * @property Contact[] $contacts2
 * @property Contract[] $contracts
 * @property Contract[] $contracts1
 * @property Contract[] $contracts2
 * @property Contract[] $contracts3
 * @property Contract[] $contracts4
 * @property Logging[] $loggings
 * @property Payrate[] $payrates
 * @property Payrate[] $payrates1
 * @property Timesheet[] $timesheets
 * @property Timesheet[] $timesheets1
 * @property CompanyShifts $companyShift
 */
class User extends CActiveRecord {

    const SCENARIO_TOKEN_UPDATE = "token";
    const SCENARIO_PASSWORD_UPDATE = "password";
    const SCENARIO_TOKEN_UPDATE_VERIFIED = "user_verified";
    const SCENARIO_USER_ADD = 'user_add';
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'sp_user';
    }
    
    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            
            array('id,token,validity','required','on'=>  self::SCENARIO_TOKEN_UPDATE ),
            array('id,password', 'required', 'on' => self::SCENARIO_PASSWORD_UPDATE),
            array('id, token, validity, is_verified', 'required', 'on' => self::SCENARIO_TOKEN_UPDATE_VERIFIED),
            array('id, first_name, email, password, type, created_at, modified_at, plan_type, staff_id', 'required', 'on' => self::SCENARIO_USER_ADD),
            
            array('type, is_verified', 'numerical', 'integerOnly' => true),
            array('id, first_name, last_name, email, password, ni_number, phone, addressline_1, street, city, country, postcode, photo, created_by, modified_by, plan_type , verification_code', 'length', 'max' => 255),
            array('dob, created_at, modified_at', 'length', 'max' => 20),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, first_name, last_name, email, password, ni_number, dob, token, validity, phone, shift_id, addressline_1, street, city, country, postcode, photo, type, created_at, created_by, modified_at, modified_by ,is_verified, verification_code ', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'companies' => array(self::HAS_MANY, 'Company', 'created_by'),
            'companies1' => array(self::HAS_MANY, 'Company', 'modified_by'),
            'contacts' => array(self::HAS_MANY, 'Contact', 'email'),
            'contacts1' => array(self::HAS_MANY, 'Contact', 'created_by'),
            'contacts2' => array(self::HAS_MANY, 'Contact', 'modified_by'),
            'contracts' => array(self::HAS_MANY, 'Contract', 'user_id'),
            'contracts1' => array(self::HAS_MANY, 'Contract', 'approver_id'),
            'contracts2' => array(self::HAS_MANY, 'Contract', 'payroller_id'),
            'contracts3' => array(self::HAS_MANY, 'Contract', 'created_by'),
            'contracts4' => array(self::HAS_MANY, 'Contract', 'modified_by'),
            'loggings' => array(self::HAS_MANY, 'Logging', 'user_id'),
            'payrates' => array(self::HAS_MANY, 'Payrate', 'created_by'),
            'payrates1' => array(self::HAS_MANY, 'Payrate', 'modified_by'),
            'timesheets' => array(self::HAS_MANY, 'Timesheet', 'created_by'),
            'timesheets1' => array(self::HAS_MANY, 'Timesheet', 'modified_by'),
            'companyShift' => array(self::BELONGS_TO,'CompanyShifts','shift_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'password' => 'Password',
            'ni_number' => 'Ni Number',
            'dob' => 'Dob',
            'phone' => 'Phone',
            'addressline_1' => 'Addressline 1',
            'street' => 'Street',
            'city' => 'City',
            'country' => 'Country',
            'photo' => 'Photo',
            'type' => 'Type',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'modified_at' => 'Modified At',
            'modified_by' => 'Modified By',
            'is_verified' => 'Is Verified',
            'verification_code' => 'Verification Code',
            'plan_type' => 'Plan Type',
            'shift_id' => 'Shift',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('first_name', $this->first_name, true);
        $criteria->compare('last_name', $this->last_name, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('ni_number', $this->ni_number, true);
        $criteria->compare('dob', $this->dob, true);
        $criteria->compare('phone', $this->phone, true);
        $criteria->compare('addressline_1', $this->addressline_1, true);
        $criteria->compare('street', $this->street, true);
        $criteria->compare('city', $this->city, true);
        $criteria->compare('country', $this->country, true);
        $criteria->compare('photo', $this->photo, true);
        $criteria->compare('type', $this->type);
        $criteria->compare('created_at', $this->created_at, true);
        $criteria->compare('created_by', $this->created_by, true);
        $criteria->compare('modified_at', $this->modified_at, true);
        $criteria->compare('modified_by', $this->modified_by, true);
        $criteria->compare('is_verified', $this->is_verified);
        $criteria->compare('verification_code', $this->verification_code, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public static function getUserFromToken($token) {
        $user = User::model()->findByAttributes(array('token' => $token));
        return $user;
    }
    
    public static function isLoginValid($username, $pass) {
        ////*** Create a new Hash function in AppInterface::pwdHash($input)
        return (User::model()->countByAttributes(array('email' => $username, 'password' => md5($pass)))) > 0 ? true : false;
    }

    public function getcurrent_in_contract() {
        if ($company = AppUser::getUserCompany()) {
            $contract = Contract::model()->findByAttributes(array('user_id' => $this->id, 'company_id' => $company->id)); //, 'type' => 'in'));
            if ($contract) {
                return $contract;
            }
        }
        return null;
    }

    public function getcurrent_contracts() {
        
    }

    public function getfull_name() {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function setfull_name($value) {
        $value = explode(' ', $value);
        $this->first_name = isset($value[0]) ? $value[0] : '';
        $this->last_name = isset($value[1]) ? $value[1] : '';
    }

    public static function prepareUserForAuthorisation($email) {
        $user = self::model()->findByAttributes(array('email' => $email));

        if ($user instanceof User) {
            return $user;
        }

        return NULL;
    }
    
    public function createUser($fname, $lname, $email, $password, $type, $otherattributes = array()) {
        $this->attributes = $otherattributes;
        $this->id = AppInterface::getUniqueId();
        $this->first_name = $fname;
        $this->last_name = $lname;
        $this->email = $email;
        $this->password = $password;
        $this->type = $type;
        if (AppUser::isUserLogin()) {
            $this->created_by = AppUser::getUserId();
            $this->modified_by = AppUser::getUserId();
        } else {
            $this->created_by = '0';
            $this->modified_by = '0';
        }
        $this->created_at = time();
        $this->modified_at = time();
        $this->first_login = 1;
        if ($this->insert()) {
            return true;
        }
        return false;
    }

    public function updateUser($fname, $lname, $name, $email, $is_created, $is_modified, $status, $last_login, $facebook_id = '', $twitter_id = '', $gender, $dob = '', $city = '', $country = '', $profile_image = '', $profile_thumb = '', $current_lat = '', $current_long = '', $device_token = '') {

        $this->fname = $fname;
        $this->lname = $lname;
        $this->full_name = $name;
        $this->email = $email;
        $this->is_created = $is_created;
        $this->is_modified = $is_modified;
        $this->status = $status;
        $this->last_login = $last_login;
        $this->facebook_id = $facebook_id;
        $this->twitter_id = $twitter_id;
        $this->gender = $gender;
        $this->dob = $dob;
        $this->city = $city;
        $this->country = $country;
        $this->profile_image = $profile_image;
        $this->profile_thumb = $profile_thumb;

        if ($this->update()) {
            return false;
        }

        return false;
    }
    
    public function validatePassword($password) {
        return $this->password == $password;
    }
    
    public function getInternalContract($id){
        $contract = Contract::model()->findByAttributes(array('is_internal' => 1 , 'user_id' => $this->id ));
        return $contract;
    }

    public static function createRandomPassword($lenght = 5) {
        $chars = "QWERTZUIOPASDFGHJKLYXCVBNMasdfghjklqwertzuiopyxcvbnm1234567890";
        $shuffle = str_split($chars);
        shuffle($shuffle);
        $string = implode("", $shuffle);
        return substr($string, 0, $lenght) . time();
    }

}
