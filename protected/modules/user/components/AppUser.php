<?php

/*
 * Description: Component for User Module
 * Author: Usama Ayaz
 * Dated:  2-June-2014
 */

class AppUser extends CComponent {

    public function __construct($ip = "") {
        
    }

    public static function searchCompanyPrivilege($key, $privileges, $_id = 0) {
        if ($privileges) {
            foreach ($privileges as $item) {
                if ($item->priveledge->key == $key || $item->priveledge_id == $_id) {
                    return true;
                }
            }
            return false;
        }
        if (AppUser::isUserSuperAdmin())
            return true;
        else
            return false;
    }
    
    public static function getUserNotifications($user_id, $page=1, $pageSize=10){
        $criteria = new CDbCriteria();
        $criteria->condition = '(receiver_id = ' . AppUser::getUserId() . ') AND status = \'active\' AND is_read=0';
        $criteria->order = 'created_at desc';
        $count = Notification::model()->count($criteria);
        $pages = new CPagination($count);
        $pages->pageSize = $pageSize;
        $pages->applyLimit($criteria);

        $notifications = Notification::model()->findAll($criteria);
        return array("model" => $notifications, "pages" => $pages->getPageCount(), "current_page" => $pages->getCurrentPage());
    }

    public static function updateUserToken(&$user) {
        $user->token = AppInterface::generateToken();
        if (isset($user->validity)) {
            $user->validity = AppUser::extendTokenValidity($user->validity);
        } else {
            $user->validity = AppUser::extendTokenValidity();
        }

        $user->scenario = User::SCENARIO_TOKEN_UPDATE;

        if ($user->save()) {
            return true;
        }
        return false;
    }

    public static function getUserByEmailPassword($email, $password) {
        $user = User::model()->findByAttributes(array("email" => $email, "password" => $password));
        return $user;
    }

    public static function getUserByEmail($email) {
        return User::model()->findByAttributes(array('email' => $email));
    }

    public static function isTokenValid($id, $token) {
        $user = User::model()->findByAttributes(array("id" => $id, "token" => $token));
        if (isset($user) && $user->validity > time()) {
            return true;
        }
        return false;
    }

    public static function getIdFromToken($token) {
        $user = User::model()->findByAttributes(array("token" => $token));
        if ($user) {
            return $user->id;
        } else {
            return 0;
        }
    }

    public static function getUserByToken($token) {
        $user = User::model()->findByAttributes(array("token" => $token));
        if ($user) {
            return $user;
        }
        return null;
    }

    public static function getUserActivities($user_id, $is_api = false, $page = 1, $size = 10) {
        $sql = "SELECT u.*,o.name FROM sp_user_timings u INNER JOIN sp_contract c "
                . "ON u.contract_id=c.id INNER JOIN sp_company o ON c.company_id=o.id "
                . "WHERE u.user_id=".$user_id." AND u.created_at >=".strtotime(date("d-m-Y H:i:s",mktime(0,0,0))).""
                . " ORDER BY u.modified_at DESC";
        $result = Yii::app()->db->createCommand($sql)->queryAll();
        $user_timings = array();
        $i = 0;
        foreach($result as $item){
            $user_timings[$i]['user_timings'] = $item;
            $user_timings[$i]['lunches'] = Yii::app()->db->createCommand("SELECT * FROM sp_user_lunch WHERE user_timing_id=".$item['id'])->queryAll();
            $user_timings[$i]['notes'] = Yii::app()->db->createCommand("SELECT * FROM sp_timesheet_notes WHERE user_timing_id=".$item['id'])->queryAll();
            $i++;
        }
        $sql = "SELECT u.*,o.name FROM sp_timesheet u INNER JOIN sp_contract c "
                . "ON u.contract_id=c.id INNER JOIN sp_company o ON c.company_id=o.id "
                . "WHERE (CASE u.type WHEN 'daily' THEN u.parent IS NOT NULL ELSE TRUE END) AND u.created_by=".$user_id." AND u.created_at >=".strtotime(date("d-m-Y H:i:s",mktime(0,0,0))).""
                . " ORDER BY u.modified_at DESC";
        $timesheets = Yii::app()->db->createCommand($sql)->queryAll();
        $sql = "SELECT u.*,o.name FROM sp_quick_timesheet u INNER JOIN sp_contact o"
                . " ON u.contact_id=o.id WHERE u.created_by=".$user_id." AND u.created_at >=".strtotime(date("d-m-Y H:i:s",mktime(0,0,0))).""
                . " ORDER BY u.modified_at DESC";
                
        $quick_timesheets = Yii::app()->db->createCommand($sql)->queryAll();
        return array("user_timings" => $user_timings, "timesheets" => $timesheets, "quick_timesheets" => $quick_timesheets);
    }

    public static function updateUserPassword(&$user) {
        $password = AppUser::createRandomPassword();
        $user->password = AppInterface::getHash($password);
        $user->scenario = User::SCENARIO_PASSWORD_UPDATE;
        if ($user->save()) {
            return $password;
        }
        return false;
    }

    public static function checkUpdateTokenValidity($token) {
        ///*** Send specific fields in this response 
        ///*** Send specific fields in this response 
//        $user = User::model()->findByAttributes(array("token" => $token));
        $criteria = new CDbCriteria();
        $criteria->select = "*";
        $criteria->condition = "token='" . $token . "'";
        $user = User::model()->find($criteria);
        if ($user instanceof User) {
            if (!$user->is_verified) {
                return array("Verification" => 0, "user" => $user);
            } else {
                if ($user->validity < time()) {
                    $user->scenario = User::SCENARIO_TOKEN_UPDATE;
                    $user->validity = AppUser::extendTokenValidity($user->validity);

                    if ($user->save()) {
                        return $user;
                    } else {
                        return $user->errors;
                    }
                } else {
                    return $user;
                }
            }
        }
        return null;
    }

    public static function checkEmailExist($email) {
        $user = User::model()->findAllByAttributes(array("email" => $email));
        if (count($user) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static function showMainMenu($ids, $privileges) {
        if ($privileges) {
            foreach ($privileges as $item) {
                for ($i = 0; $i < count($ids); $i++) {
                    if ($item->priveledge_id == $ids[$i]) {
                        return true;
                    }
                }
            }
        }
        if (AppUser::isUserSuperAdmin())
            return true;
        else
            return false;
    }

    public static function updateUserFirstLogin() {
        $user = AppUser::getCurrentUser();
        $user->first_login = 0;
        if ($user->update()) {
            return true;
        }
        return false;
    }

    public static function canPayroll() {
        if (AppUser::isUserAdmin()) {
            if ($company = AppUser::getUserCompany()) {
                if ($company->usage == 0) {
                    return false;
                }
            }
        }
        return true;
    }

    public static function extendTokenValidity($time = 0) {
        return ($time == 0) ? (time() + 86400) : ($time + 86400);
    }

    public static function addNewUser($formData) {
        $user = new User;
        $user->attributes = $formData;
        $user->id = AppInterface::getUniqueId();
        $user->password = AppInterface::getHash($formData['password']);
        if (isset($formData['type'])) {
            $user->type = $formData['type'];
        } else {
            $user->type = ConstantMessages::$user_type_employee;   ///*** This should come from CONSTANTS
        }
        $user->created_at = time();
        $user->created_by = ConstantMessages::$flag_off;
        $user->modified_by = ConstantMessages::$flag_off;
        $user->created_at = time();
        $user->modified_at = time();
        $user->validity = self::extendTokenValidity();
        $user->token = AppInterface::generateToken();
        $user->verification_code = User::createRandomPassword(50);
        if ($user->save()) {
            return $user;
        }

        return $user->errors;
    }

    public static function validateUser($formData) {
        $user = new User;
        $user->attributes = $formData;
        $user->id = AppInterface::getUniqueId();
        $user->password = AppInterface::getHash($formData['password']);  ///*** User Hash function that will be defined in AppInterface function
        if (isset($formData['type'])) {
            $user->type = $formData['type'];
        } else {
            $user->type = USER_TYPE_EMPLOYEE;   ///*** This should be a constant
        }
        $user->created_at = time();
        $user->created_by = FLAG_OFF;
        $user->modified_by = FLAG_OFF;
        $user->created_at = time();
        $user->modified_at = time();
        $user->validity = self::extendTokenValidity();  ///*** Create Funciton AppUser:CreateTokenValidty
        $user->token = AppInterface::generateToken();
        $user->verification_code = User::createRandomPassword(50);
        if ($user->validate()) {
            return $user;
        }///*** return Error in response as well if comes.

        return $user->errors;
    }

    public static function isEmailExists($email) {
        return (User::model()->countByAttributes(array('email' => $email))) > 0 ? true : false;
    }

    public static function updateNotificationStatus($notification_id) {
        $model = Notification::model()->findByPk($notification_id);
        $model->is_read = 1;
        $model->save();
    }

    public static function getNotificationColor($notification) {
        switch ($notification->notification_type_id) {
            case 1:
            case 2:
            case 3:
                return "todo-tasklist-item-border-red";
            case 4:
            case 5:
            case 6:
                return "todo-tasklist-item-border-yellow";
            case 7:
            case 8:
            case 9:
                return "todo-tasklist-item-border-blue";
            case 10:
            case 11:
            case 12:
                return "todo-tasklist-item-border-green";
            case 13:
            case 14:
            case 15:
                return "todo-tasklist-item-border-purple";
        }
    }

    public static function getNotificationTime($notification) {
        preg_match_all('!\d+!', Yii::app()->session["client_timezone"], $matches);
        $time = AppInterface::getTimeAgo($notification->created_at + ($matches[0][0] * 3600));
        return $time;
    }

    public static function formatNotificationMessage($notification) {
        $msg = "";
        switch ($notification->notification_type_id) {
            case 0:
                $user = AppUser::getUserById($notification->sender_id);
                $msg = sprintf($notification->notificationType->notice, $user->full_name);
                break;
            case 1:
                $msg = self::getMessageWithUserCreated($notification, $notification->created_by);
                break;
            case 2:
                $msg = sprintf($notification->notificationType->notice, AppUser::getCurrentUser()->first_name);
                break;
            case 3:
                $msg = self::getMessageWithUserCreated($notification, $notification->created_by);
                break;
            case 4:
                $timesheet = Timesheet::model()->findByPk($notification->timesheet_id);
                $name = AppTimeSheet::genTimeSheetName($timesheet);
                $msg = sprintf($notification->notificationType->notice, $name);
                break;
            case 5:
                $timesheet = Timesheet::model()->findByPk($notification->timesheet_id);
                $name = $timesheet->contract->user->first_name . ' ' . $timesheet->contract->user->last_name;
                $msg = sprintf($notification->notificationType->notice, $name);
                break;
            case 6:
                $createdBy = User::model()->findByPk($notification->created_by);
                $createdFor = Contract::model()->findByPk($notification->contract_id)->user->first_name;
                $msg = sprintf($notification->notificationType->notice, $createdFor, $createdBy->first_name);
                break;
            case 7:
                $msg = self::getMessageWithUserCreated($notification, $notification->created_by);
                break;
            case 8:
                $msg = self::getMessageWithUserCreated($notification, $notification->receiver_id);
                break;
            case 9:
                $msg = self::getMessageWithUserCreated($notification, $notification->receiver_id);
                break;
            case 10:
                $contract = Contract::model()->findByPk($notification->contract_id);
                $user = User::model()->findByPk($notification->created_by);
                $msg = sprintf($notification->notificationType->notice, $contract->approver->first_name, $user->full_name, $contract->user->first_name);
                break;
            case 11:
                $msg = self::getMessageWithUserCreated($notification, $notification->receiver_id);
                break;
            case 12:
                $msg = self::getMessageWithUserCreated($notification, $notification->created_by);
                break;
            case 13:
                $user = User::model()->findByPk($notification->user_id);
                $msg = sprintf($notification->notificationType->notice, $user->full_name, AppUser::getUserCompany($user->id)->name);
                break;
            case 14:
                $user = AppUser::getUserById($notification->user_id);
                $company = AppUser::getUserCompany($user->id);
                $msg = sprintf($notification->notificationType->notice, $user->full_name, $company->name);
                break;
            case 15:
                $msg = $notification->notice;
                break;
            case 18:
                $company = AppUser::getUserCompany($notification->sender_id);
                $msg = sprintf($notification->notificationType->notice, $company->name);
                break;
            case 19:
                $payroller = AppUser::getUserById($notification->sender_id);
                $msg = sprintf($notification->notificationType->notice, $payroller->full_name);
                break;
            case 20:
                $msg = sprintf($notification->notificationType->notice, $notification->receiver->full_name, AppUser::getUserCompany($notification->receiver_id)->name);
                break;
            case 21:
                $user = AppUser::getUserById($notification->user_id);
                $msg = sprintf($notification->notificationType->notice, $user->full_name, AppUser::getUserCompany($user->id)->name);
                break;
        }
        return $msg;
    }

    public static function getMessageWithUserCreated($notification, $user_id) {
        $user = User::model()->findByPk($user_id);
        return sprintf($notification->notificationType->notice, $user->first_name);
    }

    public static function getNotificationLink($notification_id, $notification_type_id, $timesheet_id, $contract_id, $user_id) {
        if (!AppUser::isUserSuperAdmin($user_id)) {
            if ($notification_type_id == 0) {
                $url = Yii::app()->createUrl('/payroller/payslip/request?notification_id=' . $notification_id);
                return $url;
            } else if ($notification_type_id == 1) {
                $url = Yii::app()->createUrl('/user/main/view?notification_id=' . $notification_id);
                return $url;
            } else if ($notification_type_id == 2) {
                $url = Yii::app()->createUrl('/company/companyShifts/timeinout?notification_id=' . $notification_id);
                return $url;
            } else if ($notification_type_id == 3) {
                $url = Yii::app()->createUrl('/timesheet/main/index/id/1/current/1?notification_id=' . $notification_id);
                return $url;
            } else if ($notification_type_id == 4) {
                $url = Yii::app()->createUrl('/payroller/main/ppayrolls?notification_id=' . $notification_id);
                return $url;
            } else if ($notification_type_id == 5) {
                $url = Yii::app()->createUrl('/payroller/payslip/request?notification_id=' . $notification_id);
                return $url;
            } else if ($notification_type_id == 6) {
                
            } else if ($notification_type_id == 7) {
                $url = Yii::app()->createUrl('/user/main/view?notification_id=' . $notification_id);
                return $url;
            } else if ($notification_type_id == 8 || $notification_type_id == 9 || $notification_type_id == 11 || $notification_type_id == 13 || $notification_type_id == 14 || $notification_type_id == 20 || $notification_type_id == 21) {
                $url = Yii::app()->createUrl('/contract/main/view/id/' . $contract_id . '?notification_id=' . $notification_id);
                return $url;
            } else if ($notification_type_id == 15) {
                $url = Yii::app()->createUrl('/notification/main/viewmessage?notification_id=' . $notification_id);
                return $url;
            }
        }
        return "javascript:void(0);";
    }

    public static function canAddSpeedyTimesheet($default) {
        if (self::isUserAdmin()) {
            if (AppUser::isTemplateAssigned(AppUser::getUserCompany()->id) < 1 || AppUser::getCurrentUser()->plan_type == 0) {
                return array();
            } else {
                return $default;
            }
        } else {
            return $default;
        }
    }

    public static function canUseTemplateAssignedThings($default) {
        if (self::isUserAdmin()) {
            if (AppUser::isTemplateAssigned(AppUser::getUserCompany()->id) < 1) {
                return array();
            } else {
                return $default;
            }
        } else {
            return $default;
        }
    }

    public static function assignDefaultTemplate($company_id) {
        if (isset($company_id)) {
            $internal_contracts = AppCompany::getAllInternalContracts($company_id);
            $template = Template::model()->findByAttributes(array('name' => 'Default'));
            $payrates = AppTemplate::getPayrates($template->id);
            $items = AppTemplate::getItems($template->id);
            foreach ($internal_contracts as $contract) {
                if (count($items) > 0) {
                    foreach ($items as $item) {
                        ContractSetting::create($contract->id, $item->name, 10, 1);
                    }
                }

                foreach ($payrates as $payrate) {
                    $status = ContractSetting::create($contract->id, $payrate->name, 10, 0);
                }
            }
            if (count($items) > 0) {
                foreach ($items as $item) {
                    PayrateName::create($company_id, $item->name, 1, 10);
                }
            }

            foreach ($payrates as $payrate) {
                $statuse = PayrateName::create($company_id, $payrate->name, 0, 10);
            }
        }
    }

    public static function isTemplateAssigned($companyId = 0) {
        if ($companyId == 0) {
            $user = User::model()->findByAttributes(array('email' => Yii::app()->user->getState('signup_email')));
            $company = AppUser::getUserCompany($user->id);
            $companyId = $company->id;
        }
        $temp_assigned = ContractSetting::model()->findBySql('SELECT C.id,CS.* FROM sp_contract C INNER JOIN sp_contract_setting CS ON CS.contract_id=C.id WHERE C.company_id=' . $companyId);
        return count($temp_assigned);
    }

    public static function getRoleByTitle($title = 'student') {

        $model = Role::model()->findByAttributes(array('title' => $title));
        return $model;
    }

    public static function getCurrentUserContract() {
        $user = AppUser::getCurrentUser();
        $company = AppUser::getUserCompany();
        $contract = Contract::model()->findByAttributes(array('user_id' => $user->id, 'company_id' => $company->id));
        if (count($contract) > 0) {
            return $contract;
        }
    }

    public static function getCurrentUserRole() {
        $user = AppUser::getCurrentUser();
        return $user->role;
    }

    public static function userFirstLogin() {
        $user = AppUser::getCurrentUser();
        return $user->first_login;
    }

    public static function getCurrentUser() {
        return User::model()->findByPk(AppUser::getUserId());
    }

    public static function loginUser($email, $password) {

        $identity = new UserIdentity($email, $password);
        Yii::app()->user->setState('email', $email);
        Yii::app()->user->setState('password', $password);
        Yii::app()->user->setState('identity', $identity);
        $identity->authenticate();
        if ($identity->errorCode === UserIdentity::ERROR_NONE) {
            Yii::app()->user->login($identity, NULL);
            return true;
        } else if ($identity->errorCode === UserIdentity::ERROR_USER_NOT_VERIFIED) {
            Yii::app()->user->setFlash('error-message', 'Please verify your account');
            return false;
        } else {
            Yii::app()->user->setFlash('error-message', 'Invalid Email/Password');
            return false;
        }
    }

    public static function setVerificationCode($id) {
        if ($user = User::model()->findByPk($id)) {
            $user->verification_code = User::createRandomPassword(50);
            if ($user->save()) {
                return true;
            }
        }
        return false;
    }

    public static function uploadUserPhoto(&$model, $name) {
        $model->photo = CUploadedFile::getInstance($model, $name);
        $images_path = realpath(Yii::app()->basePath . '/../uploads/photos');
        $filename = explode('.', $model->photo);
        $filename[0] = AppInterface::getFilename();
        $newfilename = $filename[0] . '.' . $filename[1];
        $model->photo->saveAs($images_path . '/' . $newfilename);
        $model->photo = $newfilename;
        $model->save();
    }

    public static function isUserLogin() {

        if (Yii::app()->user->isGuest)
            return false;
        else {
            return true;
        }
    }

    public static function getUserId() {
        $token = AppInterface::getHeadersData('TOKEN');
        if (isset($token)) {
            return AppUser::getIdFromToken($token);
        }
        if (Yii::app()->user->isGuest) {
            return null;
        } else {
            return Yii::app()->user->id;
        }
    }

    public static function isUserAdmin($id = '') {
        if (!$id) {
            $id = AppUser::getUserId();
        }
        if (Contract::model()->findByAttributes(array('role' => 'admin', 'user_id' => $id))) {

            return true;
        }
        return false;
    }

    public static function getUserById($id) {
        $user = User::model()->findByPk($id);
        return $user;
    }

    public static function getUserCompany($id = '', $isUserAdmin = true) {
        if ($id == '' || !$id) {
            $id = AppUser::getUserId();
        }
        /* Changed by Zohaib
         * For Normal User without changing the old functionality.!!!!
         */
        if ($isUserAdmin) {
            if (self::isUserAdmin($id)) {
                $contract = Contract::model()->findByAttributes(array('role' => 'admin', 'user_id' => $id));
                if (isset($contract)) {
                    return Company::model()->findByPk($contract->company_id); ///*** This should be check and handle if contract not found
                }
                ///*** This should be check and handle if contract not found
            } else if (!AppUser::isUserSuperAdmin($id) && !AppUser::isUserPayroller($id)) {
                $contract = Contract::model()->findByAttributes(array('is_internal' => 1, 'user_id' => $id));
                if (isset($contract)) {
                    return Company::model()->findByPk($contract->company_id);
                }
                ///*** This should be check and handle if contract not found
                ///*** This should be check and handle if contract not found
            }
        } else {
            if (!AppUser::isUserPayroller($id)) {
                $contract = Contract::model()->findByAttributes(array('is_internal' => 1, 'user_id' => $id));
                if (isset($contract)) {
                    return Company::model()->findByPk($contract->company_id);
                }
            }
        }
        return NULL;
    }

    public static function isUserPayroller($id = '') {
        if (!$id) {
            $id = AppUser::getUserId();
        }
        if ($user = User::model()->findByPk($id)) {
            if ($user->type == 2) {
                return true;
            }
        }
        return false;
    }

    public static function getUserType($id = '') {
        if (!$id) {
            $id = AppUser::getUserId();
        }
        if (self::isUserPayroller($id)) {
            return 'payroller';
        } else if (self::isUserSuperAdmin($id)) {
            return 'super_admin';
        } else if (self::isUserAdmin($id)) {
            return 'admin';
        } else
            return 'employee';
    }

    public static function isUserSuperAdmin($id = '') {
        if (!$id) {
            $id = AppUser::getUserId();
        }
        $user = User::model()->findByPk($id);
        if ($user = User::model()->findByPk($id)) {
            if ($user->type == 0) {
                return true;
            }
        }
        return false;
    }

    public static function get_all_users_by_company($cid = '') {
        if ($company = AppUser::getUserCompany()) {

            if ($cid == '' && !empty($company)) {
                $cid = $company->id;
            } else {
                return null;
            }

            $contracts = Contract::model()->findAllByAttributes(array('company_id' => $cid));
            if ($contracts) {
                $users = array();
                foreach ($contracts as $contract) {
                    $users[$contract->user->id] = $contract->user;
                }
                return $users;
            }
        }
        return null;
    }

    public static function formatUserAddres(&$user) {

        if ($user != null) {
            return $user->addressline_1 . ',' . $user->street . ',' . $user->city . ',' . $user->country;
        }
        return '';
    }

    public static function get_super_user() {

        $user = User::model()->findByAttributes(array('type' => 0));
        return $user;
    }

    public static function isCompanyAdmin($companyId, $uid) {

        $model = Contract::model()->findByAttributes(array('company_id' => $companyId, 'user_id' => $uid, 'role' => 'admin'));

        return ($model == null) ? false : true;
    }

    public static function getUserNameofId($user_id) {
        $user = User::model()->findByPk($user_id);
        return $user->first_name . ' ' . $user->last_name;
    }

    public static function goToHome() {
        if (self::isUserLogin()) {
            Yii::app()->request->redirect(Yii::app()->createUrl('/site/index'));
        }
    }

    public function addStaff($formData, $user_id) {
        $contract = new Contract;
        if (isset($formData['Contract']) && isset($formData['User']['first_name']) 
                && isset($formData['User']['email']) && isset($formData['User']['password']) 
                && isset($formData['User']['last_name']) && isset($formData['User']['staff_id'])) {

            $user = self::addUser($formData['User']['first_name'], $formData['User']['email'], 
                    AppInterface::getHash($formData['User']['password']), $formData['User']['first_name'], 
                    $formData['User']['last_name'], FLAG_ON, $formData['User']['staff_id'], FLAG_OFF, 
                    $formData['User']['ni_number']); ///*** Use it from constants

            $contract->attributes = $formData['Contract'];
            $contract->id = AppInterface::getUniqueId();
            $contract->user_id = $user->id;
            $contract->company_id = AppUser::getUserCompany()->id;
            $contract->role = EMPLOYEE_TYPE_EMPLOYEE; ///*** Use it from constants
            $contract->status = FLAG_ON;
            $contract->start_time = DateTime::createFromFormat(AppInterface::getdateformat(), $formData['Contract']['start_time'])->getTimestamp();
            if (isset($formData['Contract']['end_time']))
                $contract->end_time = DateTime::createFromFormat(AppInterface::getdateformat(), $formData['Contract']['end_time'])->getTimestamp();
            else
                $contract->end_time = "";
            if(isset($_POST['Contract']['approver_id'])){
                $contract->approver_id = $_POST['Contract']['approver_id'];
            } else{
                $contract->approver_id = AppUser::getUserId();
            }
            $contract->type = TIMESHEET_TYPE_IN; ///*** Use it from constants
            $contract->is_internal = FLAG_ON; ///*** Use it from constants
            $contract->created_at = time();
            if (AppUser::canPayroll()) {
                $contract->payroller_id = AppUser::getUserCompany()->payroller_id;
            }
            $contract->collection_type = $formData['Contract']['collection_type'] == 1 ? ($formData['Settings']['collection_day_first'] == 1 ? 1 : 2) : 0;
            $contract->created_by = AppUser::getUserId();
            $contract->owner_id = AppUser::getUserId();
            if (count($_FILES) > 0) {
                if ($_FILES['Contract']['name']['file_name']) {
                    $contract->file_name = CUploadedFile::getInstance($contract, 'file_name');
                    $images_path = AppInterface::getRealPath('contracts'); ///*** Use it from constants
                    $filename = explode('.', $contract->file_name);
                    $filename[0] = AppInterface::getFilename();
                    $newfilename = $filename[0] . '.' . $filename[1];
                    $contract->file_name->saveAs($images_path . '/' . $newfilename);
                    $contract->file_name = $newfilename;
                }
            }
            $contract->created_at = time();
            $contract->modified_by = AppUser::getUserId();
            $contract->modified_at = time();

            if (isset($user) && count($user->errors) == 0 && $contract->save()) {

                if (isset($formData['Payrate'])) {
                    foreach ($formData['Payrate'] as $name => $rate) {
                        AppCompany::addContractSetting(array('contract_id' => $contract->id, 'key' => $name, 'value' => $rate, 'category' => 'payrate', 'created_by' => AppUser::getUserId(), 'modified_by' => AppUser::getUserId(), 'created_at' => time(), 'modified_at' => time()));
                    }
                }

                if (isset($formData['Item'])) {
                    foreach ($formData['Item'] as $name => $rate) {
                        AppCompany::addContractSetting(array('contract_id' => $contract->id, 'key' => $name, 'value' => $rate, 'category' => 'item', 'created_by' => AppUser::getUserId(), 'modified_by' => AppUser::getUserId(), 'created_at' => time(), 'modified_at' => time()));
                    }
                }
                return $user;
            } elseif (isset($user) && count($user->errors) == 0) {
                return $contract->errors;
            } else {
                return $user->errors;
            }
        } return 0;
    }

    public static function getUserInvoices($args = array()) {
        $invoice_items = array();
        $c = new CDbCriteria();
        $c->select = "t.*";
        $c->join = " LEFT JOIN sp_contact c on t.contact_id = c.id ";
        $c->addCondition('c.company_id = :cid');
        $c->addCondition('t.contact_id = :id');
        $c->params = array(':id' => $args['contact_id'],
            ':cid' => AppUser::getUserCompany()->id,);
        $count = count(Invoice::model()->findAll($c));
        $pages = new CPagination($count);
        $result = array();
        $pages->applyLimit($c);
        $pages->pageSize = 10;
        $invoices = Invoice::model()->findAll($c);
        $count = 0;
        foreach ($invoices as $item) {
            $result[$count]['Invoice'] = $item;
            $result[$count]['InvoiceItems'] = $item->invoiceItems;
            $count++;
        }
        return array("dataSet" => $result, 'pages' => $pages->getPageCount(), "current_page" => $pages->getCurrentPage());
    }

    public static function addUser($username, $email, $password, $fname = '', $lname = '', $type = 1, $staff_id = 0, $plan_type = 0, $ni_num = '') {
        $user = new User();
        $user->id = AppInterface::getUniqueId();
        $contractor_name = explode(' ', $username);
        $user->first_name = ($fname != '') ? $fname : $contractor_name[0];
        $user->email = $email;
        $user->last_name = ($lname != '') ? $lname : (count($contractor_name) > 1 ? $contractor_name[1] : '');
        $user->type = $type;
        $user->ni_number = $ni_num;
        $user->is_verified = 1;
        $user->password = $password;
        $user->created_at = time();
        $user->created_by = '0';
        $user->modified_at = time();
        $user->modified_by = '0';
        $user->staff_id = $staff_id;
        $user->plan_type = $plan_type;
        $user->token = AppInterface::generateToken();
        $user->validity = AppUser::extendTokenValidity();
        $user->validate();
        if ($user->insert()) {
            return $user;
        }

        return null;
    }

    public static function createRandomPassword($lenght = 5) {
        $chars = "QWERTZUIOPASDFGHJKLYXCVBNMasdfghjklqwertzuiopyxcvbnm1234567890";
        $shuffle = str_split($chars);
        shuffle($shuffle);
        $string = implode("", $shuffle);
        return substr($string, 0, $lenght) . time();
    }

    public static function sendEmail($user) {
        $current_payroller = AppUser::getUserCompany()->payroller_id;
        $payroller_email = User::model()->findByPk($current_payroller);
        $notification = Yii::app()->getModule('notification');
        $msg = ConstantMessages::employeeAdd();
        $owner = AppUser::getCurrentUser()->full_name;
        $notification->send(
                array(
                    'email' =>
                    array('template' => 'addemployee', 'params' => array('CREATOR' => $owner, 'USERNAME' => $user->email, 'PASSWORD' => $_POST['formData']['User']['password'], 'NAME' => $user->first_name),
                        'to' => array(array('email' => $user->email, 'name' => $user->first_name)),
                        'from' => array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls')
                    ),
                    'email' =>
                    array('template' => 'Email', 'params' => array('BODY' => $msg),
                        'to' => array(array('email' => AppUser::get_super_user()->email, 'name' => 'Super Admin')),
                        'from' => array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls')
                    )
        ));

        $msg2 = 'A new employee ' . $user->first_name . ' has nominated you as their payroller via ' . AppUser::getUserCompany()->name . '. Please login to your SpeedyPayroll account and accept this request.';
        if (isset($payroller_email)) {
            $notification->send(array(
                'email' =>
                array('template' => 'Email', 'params' => array('BODY' => $msg2),
                    'to' => array(array('email' => $payroller_email->email,
                            'name' => $payroller_email->full_name)),
                    'from' => array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls')
                )
            ));
        }
    }

}

?>
