<?php

class MainController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('logoutUser', 'signup', 'completeprofile', 'SignupReloaded', 'customTemplate', 'verifymessage', 'defaultTemplate', 'applyTemplate', 'assignTemplate', 'login', 'checkcompanyexist', 'checkuserexist', 'checkuserexistincompany', 'forgot', 'changepassword', 'verifyemail', 'logout', 'verify', 'profile', 'editprofile'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('dashboard'),
                'roles' => array('employee', 'admin', 'super_admin',),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('create', 'update', 'admin', 'delete', 'invite', 'index', 'registrationPending', 'view', 'userpassword'),
                'roles' => array('admin', 'super_admin'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('loginasCompany', 'loginasUser',),
                'roles' => array('super_admin',),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id = '') {
//        $notification_id = Yii::app()->request->getParam('notification_id');
//        if (isset($notification_id)) {
//            AppUser::updateNotificationStatus($notification_id);
//        }
        if (!$id) {
            $id = AppUser::getUserId();
        }
        $model = $this->loadModel($id);
        if (isset($model->shift_id) && $model->shift_id != 0) {
            $shift = CompanyShifts::model()->findByPk($model->shift_id)->title;
        } else {
            $shift = "-";
        }
        $this->render('view', array(
            'model' => $model,
            'shift' => $shift
        ));
    }

    public function actionProfile() {
        $model = User::model()->findByPk(AppUser::getUserId());

        $this->render('profile', array(
            'model' => $model,
        ));
    }

    public function actionEditProfile() {
        if (isset($_POST['User'])) {
            $model = User::model()->findByPk(AppUser::getUserId());
            $model->first_name = $_POST['User']['first_name'];
            $model->last_name = $_POST['User']['last_name'];
            $model->phone = $_POST['User']['phone'];
            $model->ni_number = $_POST['User']['ni_number'];

            $model->dob = strtotime($_POST['dob_year'] . '-' . $_POST['dob_month'] . '-' . $_POST['dob_day']);
            $model->addressline_1 = $_POST['User']['addressline_1'];
            $model->country = $_POST['User']['country'];
            $model->street = $_POST['User']['street'];
            $model->city = $_POST['User']['city'];
            $model->postcode = $_POST['User']['postcode'];
            $model->created_by = 0;
            $model->created_at = time();
            $model->modified_by = 0;
            $model->modified_at = time();
            if ($_FILES['User']['name']['photo']) {
                $model->photo = CUploadedFile::getInstance($model, 'photo');
                $images_path = realpath(Yii::app()->basePath . '/../uploads/photos');
                $filename = explode('.', $model->photo);
                $filename[0] = AppInterface::getFilename();
                $newfilename = $filename[0] . '.' . $filename[1];
                $model->photo->saveAs($images_path . '/' . $newfilename);
                $model->photo = $newfilename;
            }
            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'Profile successfully updated.');
                if ($model->id == AppUser::getUserId()) {
                    $mailer1 = new AppMailer();
                    $mailer2 = new AppMailer();
                    $msg = $model->full_name . ' has updated profile';

//                    Mail to super admin
                    $mailer1->prepareBody('Email', array('BODY' => $msg));
                    $mailer1->sendMail(array(array('email' => AppUser::get_super_user()->email, 'name' => AppUser::get_super_user()->full_name)), array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls'));
//                    Mail to company admin
                    if (!AppUser::isUserSuperAdmin()) {
                        $userCompanyAdmin = AppUser::getUserCompany(AppUser::getUserId(), FALSE)->admin;
                        $mailer1->prepareBody('Email', array('BODY' => $msg));
                        $mailer1->sendMail(array(array('email' => $userCompanyAdmin->email, 'name' => $userCompanyAdmin->full_name)), array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls'));
                    }
                }
                $this->redirect(array('/user/main/profile'));
            }
        }

        $model = User::model()->findByPk(AppUser::getUserId());
        $this->render('editing', array(
            'model' => $model,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new User;

        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id = '') {
        if (!$id) {
            $id = AppUser::getUserId();
        }
//        dd($id);
        $model = $this->loadModel($id);
//        dd($model);
        $contract = Contract::model()->findByAttributes(array('user_id' => $id));
//        dd($contract->company_id);
//        dd(AppUser::getCurrentUser()->id);
        $company = AppUser::getUserCompany();
        if (AppUser::getUserId() == 1) {
            $shifts = CompanyShifts::model()->findAllByAttributes(array("company_id" => $contract->company_id));
        } else {
            $shifts = CompanyShifts::model()->findAllByAttributes(array("company_id" => $company->id));
        }
        $photo = $model->photo;
        if (isset($_POST['User'])) {

//            $_POST['User']['email'] != $model->email
            $user_email = AppUser::checkEmailExist($_POST['User']['email']);
            if ($user_email && $_POST['User']['email'] != $model->email) {
                Yii::app()->user->setFlash('error', 'Email already exist.');
                $this->redirect(array('update'));
            } else {
                $model->attributes = $_POST['User'];
                $model->shift_id = $_POST["User"]["shift"];
                $model->dob = strtotime($_POST['dob_year'] . '-' . $_POST['dob_month'] . '-' . $_POST['dob_day']);
                $contract->employee_type = $_POST['Contract']['employee_type'];

                if ($_FILES['User']['name']['photo']) {
                    $model->photo = CUploadedFile::getInstance($model, 'photo');
                    $images_path = realpath(Yii::app()->basePath . '/../uploads/photos');
                    $filename = explode('.', $model->photo);
                    $filename[0] = AppInterface::getFilename();
                    $newfilename = $filename[0] . '.' . $filename[1];
                    $model->photo->saveAs($images_path . '/' . $newfilename);
                    $model->photo = $newfilename;
                } else {
                    $model->photo = $photo;
                }

                if ($model->update() && $contract->update())
                    $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
            'contract' => $contract,
            'shifts' => $shifts
        ));
    }

    public function actionUserPassword($id) {

        $model = User::model()->findByPk($id);
        if (isset($_POST['User'])) {
            $password = $_POST['User']['password'];
            $model->password = md5($_POST['User']['password']);
            if ($model->update())
                Yii::app()->user->setFlash('success', 'Password change successfully');
//            $msg = 'Your password has been reset by admin of your company. Your new password is: ' . $password;
            $mailer = new AppMailer();
                $mailer->prepareBody('ResetPassword', array('NAME' => $model->first_name . ' ' . $model->last_name, 'PASSWORD' => $password, 'EMAIL' => $model->email));
                $mailer->sendMail(array(array('email' => $model->email, 'name' => $model->first_name . ' ' . $model->last_name)), array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'SpeedyPayrolls'));
//            $mailer->prepareBody('Email', array('NAME' => $model->first_name, "MESSAGE" => $msg));
//            $mailer->sendMail(array(array('email' => $model->email, 'name' => $model->first_name)), array('email' => AppSetting::getSettingByKey("SUPPORT_EMAIL")->value, 'name' => 'Support'));
//            dd($mailer);
            $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('password', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $this->redirect(array('/user/employee'));
    }

    public function actionInvite() {
        $success = false;
        if (!AppUser::isUserAdmin()) {
            throw new CHttpException(403, 'You are not authorized for this action.');
            AppLogging::addLog('Authorization Error', 'error', 'application.user.controller.main');
        } else {
            $company_id = AppUser::getUserCompany()->id;
            $company_name = AppUser::getUserCompany()->name;
            $inviter = AppUser::getCurrentUser()->full_name;
        }
        $user = new User();
        if (isset($_POST['User'])) {
            $user_already = User::prepareUserForAuthorisation($_POST['User']['email']);
            $user->attributes = $_POST['User'];
            if (!$user_already) {
                $user->id = AppInterface::getUniqueId();
                $user->attributes = $_POST['User'];
                $user->created_at = time();
                $user->created_by = '0';
                $user->modified_at = time();
                $user->modified_by = '0';
                $user->type = 1;
                $user->password = '-';
                $user->is_verified = 1;
                $user->save();
                $user_id = $user->id;
            } else {
                $user_id = $user_already->id;
            }
            $contract = new Contract();
            $contract->id = AppInterface::getUniqueId();
            $contract->user_id = $user_id;
            $contract->role = 'Employee';
            $contract->status = 1;
            $contract->company_id = $company_id;
            $contract->start_time = DateTime::createFromFormat(AppInterface::getdateformat(), $_POST['Contract']['start_time'])->getTimestamp(); //strtotime($_POST['Contract']['start_time']);
            if ($_POST['Contract']['end_time'])
                $contract->end_time = DateTime::createFromFormat(AppInterface::getdateformat(), $_POST['Contract']['end_time'])->getTimestamp(); //strtotime($_POST['Contract']['end_time']);
            else
                $contract->end_time = "";
            $contract->approver_id = $_POST['Contract']['approver_id'];
            $contract->collection_type = $_POST['Contract']['collection_type'] == 1 ? ($_POST['Settings']['collection_day_first'] == 1 ? 1 : 2) : 0;
            $contract->type = 'in';
            $contract->employee_type = $_POST['Contract']['employee_type'];
            $contract->invitation_code = User::createRandomPassword(20);
            $contract->created_at = time();
            $contract->is_internal = 1;
            $contract->created_at = time();
            if (AppUser::canPayroll()) {
                $contract->payroller_id = AppUser::getUserCompany()->payroller_id;
            }
            $contract->created_by = '0';
            $contract->modified_at = time();
            $contract->modified_by = '0';
            $contract->owner_id = AppUser::getUserId();
            $contract->save();
            if (isset($_POST['Payrate'])) {
                foreach ($_POST['Payrate'] as $name => $rate) {
                    AppCompany::addContractSetting(array('contract_id' => $contract->id, 'key' => $name, 'value' => $rate, 'category' => 'payrate', 'created_by' => AppUser::getUserId(), 'modified_by' => AppUser::getUserId(), 'created_at' => time(), 'modified_at' => time()));
                }
            }

            if (isset($_POST['Item'])) {
                foreach ($_POST['Item'] as $name => $rate) {
                    AppCompany::addContractSetting(array('contract_id' => $contract->id, 'key' => $name, 'value' => $rate, 'category' => 'item', 'created_by' => AppUser::getUserId(), 'modified_by' => AppUser::getUserId(), 'created_at' => time(), 'modified_at' => time()));
                }
            }

            $url = $this->createAbsoluteUrl('/user/main/completeprofile') . '/invite/' . $contract->invitation_code . '/contract/' . $contract->id;

            $success = true;
            AppLogging::addLog('Invitation Sent', 'success', 'application.user.controller.main');
            $not_result = AppInterface::notification($user->id, 1, 0, $contract->id, $user->id);
            $_url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
            $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;
            $notifier = new Notification();
            $notification_count = $notifier->getUserNotificationUnseenCount();
            $msg = ConstantMessages::employeeAdd();

            $notification = Yii::app()->getModule('notification');
            $notification->send(array(
                'push' => array('server_notification', array('id' => $user->id, 'msg' => sprintf($not_result->notificationType->notice, AppUser::getCurrentUser()->first_name), 'url' => $_url, 'logo' => $logo, 'count' => $notification_count)),
                'email' => array(
                    'template' => 'invitation',
                    'params' => array('NAME' => $user->first_name, 'FULLNAME' => $inviter, 'RESETLINK' => $url, 'EMAIL' => $user->email),
                    'to' => array(array('email' => $user->email, 'name' => $user->first_name)),
                    'from' => array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls'),
            )));

            Yii::app()->user->setFlash('success', 'An invite email has been sent to the user');
            $user = new User;
            if (!$_POST['addmore']) {
                $this->redirect(array('/site/index'));
            }
        }

        $this->render('invite', array(
            'user' => $user, 'success' => $success, 'approvers' => AppContract::get_all_internal_users(),
        ));
    }

    public function actionDashboard() {
        $this->render('dashboard', array());
    }

    public function actionVerify() {
        $this->layout = '//layouts/login';
        $this->render('verifymessage');
    }

    public function actionDefaultTemplate() {
        $this->layout = '//layouts/login';
        $this->render('defaultTemplate');
    }

    public function actionApplyTemplate() {
        $this->layout = '//layouts/login';
        $this->render('applyTemplate');
    }

    public function actionAssignTemplate() {
        $this->layout = '//layouts/login';
        $user = User::model()->findByAttributes(array('email' => Yii::app()->user->getState('signup_email')));
        $company = AppUser::getUserCompany($user->id);
        if ($company) {
            if (AppUser::isTemplateAssigned($company->id) < 1) {
                AppUser::assignDefaultTemplate($company->id);
            }
            echo 'success';
        } else {
            echo 'failure';
        }
    }

    public function actionVerifyemail() {
        $this->layout = '//layouts/login';
        $error = 'Please enter your email address for verification';
        if (isset($_GET['vcode'])) {
            $user = User::model()->findByAttributes(array('verification_code' => $_GET['vcode']));
            if ($user) {
                $user->is_verified = 1;
                $user->save();
                AppLogging::addLog('Email Verified', 'success', 'application.user.controller.main');
                Yii::app()->user->setFlash('success', 'You have successfully verified your account. You can complete your profile and  refer to our documentation section for further help on using SpeedyPayrolls.');
                $superadmin = new AppMailer();
                $superadmin->prepareBody('userverified', array('USER' => $user->email));
                $status = AppUser::loginUser($user->email, $user->password);

                if ($status === false) {
                    $error = Yii::app()->user->getFlash('error-message');
                } else {
                    if (AppUser::isUserSuperAdmin()) {
                        $this->redirect(array('/site/index'));
                    } else if (AppUser::isUserAdmin()) {
                        $company = AppUser::getUserCompany();

                        if (AppUser::userFirstLogin()) {
                            AppUser::updateUserFirstLogin();
                            $this->redirect(array('/contract/main/update/id/' . AppContract::getInternalContract(AppUser::getCurrentUser()->id)->id . '?Staff_Management'));
                        }
                        $this->redirect(array('/site/index'));
                    } else {
                        $this->redirect(array('/site/index'));
                    }
                }
                die();
            }
        } else if (isset($_POST['email'])) {
            $user = User::prepareUserForAuthorisation($_POST['email']);
            if ($user) {
                if (!$user->is_verified) {
                    $user->verification_code = User::createRandomPassword(50);
                    $user->save();
                    $url = Yii::app()->createAbsoluteUrl('/user/main/verifyemail') . '/vcode/' . $user->verification_code;
                    $mailer = new AppMailer();
                    $mailer->prepareBody('ConfirmEmail', array('NAME' => $user->full_name, 'CONFIRMLINK' => $url, 'EMAIL' => $user->email));
                    $mailer->sendMail(array(array('email' => $user->email, 'name' => $user->full_name)), array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'SpeedyPayrolls'));
                    $this->redirect(array('verify'));
                }
            } else {
                $error = 'Sorry, no user found with this email address. You can register as a new user. <a href="' . $this->createUrl('/site/signup') . '" >Sign up</a>';
            }
        }
        $this->render('verify', array(
            'error' => $error,
        ));
    }

    public function actionCompleteProfile($invite, $contract) {
        $this->layout = '//layouts/login';
        $error = ''; //array();
        $payrollers = User::model()->findAllByAttributes(array('type' => 2));
        $invitation_contract = null;

        if ($contract = Contract::model()->findByPk($contract)) {
            if ($invite == $contract->invitation_code) {
                $model = $contract->user;
                $model->password = '';
                if ($contract->role == 'admin') {
                    $invitation_contract = $contract;
                }
            } else {
                AppLogging::addLog('Invalid Request', 'error', 'application.user.controller.main');
                throw new CHttpException(400, 'Invalid Request.');
            }
        } else {
            AppLogging::addLog('Invalid Request', 'error', 'application.user.controller.main');
            throw new CHttpException(400, 'Invalid Request.');
        }

        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            $model->password = md5($_POST['User']['password']);
            $model->save();
            $contract->invitation_code = null;
            $contract->status = 1;
            $contract->save();
            $this->login($model->email, $model->password, "Thank you for completing your profile.");
        }

        $this->render('signup-2', array(
            'model' => $model,
            'error' => $error,
            'invite' => true,
            'invitation_contract' => $invitation_contract,
            'payrollers' => $payrollers,
        ));
    }

    public function login($email, $password, $message = '') {
        $status = AppUser::loginUser($email, $password);
        if ($status === false) {
            $error = Yii::app()->user->setFlash('message', "Form should be filled properly");
            AppLogging::addLog('invalid login', 'login', 'application.user.controller.main');
        } else {
            Yii::app()->user->setFlash('success', "Welcome to SpeedyPayrolls. " . $message);
            AppLogging::addLog('login successfull', 'login', 'application.user.controller.main');
            AppUser::goToHome();
        }
    }

    public function actionSignupReloaded() {
        $this->layout = '//layouts/login';
        $model = new User;
        $error = '';

        $yiiuser = Yii::app()->user;
        if ($yiiuser->hasState('signup_email') && $yiiuser->hasState('signup_password')) {
            $model->email = $yiiuser->getState('signup_email');
            $model->password = $yiiuser->getState('signup_password');
        } else {
            AppLogging::addLog('Invalid Request', 'error', 'application.user.controller.main');
            throw new CHttpException(400, 'Invalid Request.');
        }

        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            if (isset($_POST['Settings']) && $_POST['Settings']['type'] == 1) {
                $model = User::prepareUserForAuthorisation($_POST['Settings']['invite_email']);
                if ($model) {
                    $model->dob = strtotime($_POST['dob_year'] . '-' . $_POST['dob_month'] . '-' . $_POST['dob_day']);
                    $model->password = $yiiuser->getState('signup_password');
                    $model->modified_by = 0;
                    $model->modified_at = time();
                    $model->is_verified = true;
                    $model->type = 1;
                    if ($model->contracts) {
                        foreach ($model->contracts as $c) {
                            if ($c->invitation_code) {
                                $old_contract = $c;
                                break;
                            }
                        }
                        if (isset($old_contract)) {
                            $model->save();
                            $old_contract->invitation_code = null;
                            $old_contract->status = 1;
                            $old_contract->save();
                            AppInterface::notification('User Signup: employed by company', AppUser::get_super_user()->id);
                            $this->login($model->email, $model->password, 'Thank you for completeing your profile.');
                        }
                    }
                } else {
                    $error = 'No invitation found with this email address';
                }
            } else if (isset($_POST['Settings']) && $_POST['Settings']['type'] == 2) {
                $user = User::prepareUserForAuthorisation($model->email);
                if (!$user) {
                    if ($_POST['Settings']) {
                        if ($_POST['Settings']['type'] == 2) {
                            $model->id = AppInterface::getUniqueId();
                            $model->attributes = $_POST['User'];
                            $model->dob = strtotime($_POST['dob_year'] . '-' . $_POST['dob_month'] . '-' . $_POST['dob_day']);
//                            $model->dob = strtotime($_POST['dob']);
                            $model->created_by = 0;
                            $model->created_at = time();
                            $model->modified_by = 0;
                            $model->modified_at = time();
                            $model->type = 1;
                            $model->save();
                            if ($_FILES['User']['name']['photo']) {
                                AppUser::uploadUserPhoto($model, 'photo');
                            }
                            $company = new Company;
                            $company->id = AppInterface::getUniqueId();
                            $company->attributes = $_POST['Company'];
                            $company->created_by = 0;
                            $company->created_at = time();
                            $company->modified_by = 0;
                            $company->modified_at = time();
                            $company->type = 1;
                            $company->status = 1;
                            $company->save();

                            $contractid = Contract::create($model->id, $company->id, 'in', $model->id, $_POST['Settings']['self_role'], "full_time", 1, time(), -1);
                            Contract::create($model->id, $company->id, 'in', $model->id, 'admin', "full_time", 0, time(), -1);

                            if ($model->save()) {
                                $company->save();
                                if ($_POST['PayrateName']) {
                                    foreach ($_POST['PayrateName'] as $name) {
                                        PayrateName::create($company->id, $name);
                                        ContractSetting::create($contractid, $name, 10);
                                    }
                                }
                                AppUser::setVerificationCode($model->id);
                                $url = Yii::app()->createAbsoluteUrl('/user/main/verifyemail') . '/vcode/' . $model->verification_code;
                                $mailer = new AppMailer();
                                $mailer->prepareBody('ConfirmEmail', array('NAME' => $model->full_name, 'CONFIRMLINK' => $url, 'EMAIL' => $model->email));
                                $mailer->sendMail(array(array('email' => $model->email, 'name' => $model->full_name)), array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'SpeedyPayrolls'));

                                $mailer1 = new AppMailer();
                                $msg = 'A new signup of ' . $model->first_name . ' with email address: ' . $model->email . ' is pending.';
                                $mailer1->prepareBody('Email', array('BODY' => $msg));
                                $mailer1->sendMail(array(array('email' => AppUser::get_super_user()->email, 'name' => 'Super Admin')), array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls'));

                                if (isset($_POST['Company']['payroller_id'])) {
                                    $mailer2 = new AppMailer();
                                    $current_payroller = User::model()->findByPk($_POST['Company']['payroller_id']);
                                    $msg2 = 'A new employee ' . $model->first_name . ' has nominated you as their payroller via ' . $_POST['Company']['name'] . '. Please login to your SpeedyPayroll account and accept this request.';
                                    $mailer2->prepareBody('Email', array('BODY' => $msg2));
                                    $mailer2->sendMail(array(array('email' => $current_payroller->email, 'name' => $current_payroller->first_name)), array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls'));
                                }

                                $this->login($model->email, $model->password);
                            }
                        }
                    } else {
                        throw new CHttpException(400, 'Invalid Request.');
                    }
                } else {
                    $error = 'Account with this email address already exist.';
                }
            } else if (isset($_POST['Settings']) && $_POST['Settings']['type'] == 3) {
                
            }
        }

        $this->render('signup-2', array(
            'model' => $model,
            'error' => $error,
            'invite' => false,
            'invitation_contract' => null,
            'payrollers' => User::model()->findAllByAttributes(array('type' => 2)),
        ));
    }

    public function actionSignup($invite = null, $contract = null) {
        $this->layout = '//layouts/login';
        $isinvited = false;
        $error = ''; //array();
        $invitation_contract = null;
        $payrollers = User::model()->findAllByAttributes(array('type' => 2));

        if (isset($invite) && isset($contract)) {
            if ($contract = Contract::model()->findByPk($contract)) {
                if ($invite == $contract->invitation_code) {
                    $model = $contract->user;
                    $model->password = '';
                    $model->plan_type = 0;
                    $isinvited = true;
                    if ($contract->role == 'admin') {
                        $invitation_contract = $contract;
                    }
                } else {
                    AppLogging::addLog('Invalid Request', 'error', 'application.user.controller.main');
                    throw new CHttpException(400, 'Invalid Request.');
                }
            } else {
                AppLogging::addLog('Invalid Request', 'error', 'application.user.controller.main');
                throw new CHttpException(400, 'Invalid Request.');
            }
        } else {
            $model = new User;
            $yiiuser = Yii::app()->user;
            if ($yiiuser->hasState('signup_email') && $yiiuser->hasState('signup_password')) {
                $model->email = $yiiuser->getState('signup_email');
                $model->password = $yiiuser->getState('signup_password');
                $model->plan_type = $yiiuser->getState('plan_type');
            } else {
                AppLogging::addLog('Invalid Request', 'error', 'application.user.controller.main');
                throw new CHttpException(400, 'Invalid Request.');
            }
        }

        if (isset($_POST['User'])) {
            if (isset($_POST['Settings']) && $_POST['Settings']['type'] == 1) {

                $model = User::prepareUserForAuthorisation($_POST['Settings']['invite_email']);
                if ($model) {
                    $model->attributes = $_POST['User'];
                    $model->dob = strtotime($_POST['dob_year'] . '-' . $_POST['dob_month'] . '-' . $_POST['dob_day']);
                    $model->password = $yiiuser->getState('signup_password');
                    $model->modified_by = 0;
                    $model->modified_at = time();
                    $model->is_verified = true;
                    $model->plan_type = $yiiuser->getState('plan_type');
                    $model->type = 1;
                    if ($model->contracts) {
                        foreach ($model->contracts as $c) {
                            if ($c->invitation_code) {
                                $old_contract = $c;
                                break;
                            }
                        }
                        if (isset($old_contract)) {
                            $model->save();
                            $old_contract->invitation_code = null;
                            $old_contract->status = 1;
                            $old_contract->save();

                            $model->verification_code = User::createRandomPassword(50);
                            $model->save();
                            $url = Yii::app()->createAbsoluteUrl('/user/main/verifyemail') . '/vcode/' . $model->verification_code;
                            $mailer = new AppMailer();
                            $mailer->prepareBody('ConfirmEmail', array('NAME' => $model->full_name, 'CONFIRMLINK' => $url, 'EMAIL' => $model->email));
                            $mailer->sendMail(array(array('email' => $model->email, 'name' => $model->full_name)), array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'SpeedyPayrolls'));

                            AppInterface::notification('User Signup', AppUser::get_super_user()->id, 1, $old_contract->id, $model->id);
                            $status = AppUser::loginUser($model->email, $model->password);
                            if ($status === false) {
                                $error = Yii::app()->user->setFlash('message', "Form should be filled properly");
                                AppLogging::addLog($error, 'error', 'application.user.controller.main');
                            } else {
                                $error = Yii::app()->user->setFlash('message', "SignUp Successfully");
                                AppLogging::addLog($error, 'success', 'application.user.controller.main');
                                if (AppUser::isUserAdmin()) {
                                    $this->redirect(array('/site/index'));
                                }
                                $this->redirect(array('/site/index'));
                            }
                        }
                    }
                }
                $model = new User;
                $model->attributes = $_POST['User'];
                $this->render('signup-2', array('model' => $model, 'error' => 'No invitaion found with this email address', 'invite' => $isinvited));
                AppLogging::addLog('Authorization Error', 'error', 'application.user.controller.main');
                die();
            } else if ($model->isNewRecord) {


                $user = User::prepareUserForAuthorisation($model->email);
                if (!$user) {
                    if ($_POST['Settings']) {
                        if ($_POST['Settings']['type'] == 2) {
                            $model->id = AppInterface::getUniqueId();
                            $model->attributes = $_POST['User'];
                            $model->dob = strtotime($_POST['dob_year'] . '-' . $_POST['dob_month'] . '-' . $_POST['dob_day']);
                            $model->created_by = 0;
                            $model->created_at = time();
                            $model->modified_by = 0;
                            $model->modified_at = time();
                            $model->plan_type = $yiiuser->getState('plan_type');
                            $model->type = 1;
                            if ($_FILES['User']['name']['photo']) {
                                $model->photo = CUploadedFile::getInstance($model, 'photo');
                                $images_path = realpath(Yii::app()->basePath . '/../uploads/photos');
                                $filename = explode('.', $model->photo);
                                $filename[0] = AppInterface::getFilename();
                                $newfilename = $filename[0] . '.' . $filename[1];
                                $model->photo->saveAs($images_path . '/' . $newfilename);
                                $model->photo = $newfilename;
                            }

                            if (!$invitation_contract) {
                                $company = new Company;
                                $company->id = AppInterface::getUniqueId();
                                $company->attributes = $_POST['Company'];
                                $company->created_by = 0;
                                $company->created_at = time();
                                $company->modified_by = 0;
                                $company->modified_at = time();
                                $company->type = 1;
                                $company->status = 1;

                                $contract = new Contract;
                                $contract->id = AppInterface::getUniqueId();
                                $contract->user_id = $model->id;
                                $contract->company_id = $company->id;
                                $contract->approver_id = $model->id;
                                $contract->role = $_POST['Settings']['self_role'];
                                $contract->status = 1;
                                $contract->start_time = time();
                                $contract->end_time = -1;
                                $contract->type = "in";
                                $contract->employee_type = "full_time";
                                $contract->created_by = 0;
                                $contract->created_at = time();
                                $contract->modified_by = 0;
                                $contract->modified_at = time();
                                $contract->is_internal = 1;
                                $contract->owner_id = $model->id;


                                $contract2 = new Contract;
                                $contract2->id = AppInterface::getUniqueId();
                                $contract2->user_id = $model->id;
                                $contract2->company_id = $company->id;
                                $contract2->approver_id = $model->id;
                                $contract2->role = 'admin';
                                $contract2->status = 1;
                                $contract2->start_time = time();
                                $contract2->end_time = -1;
                                $contract2->type = "in";
                                $contract2->employee_type = "full_time";
                                $contract2->created_by = 0;
                                $contract2->created_at = time();
                                $contract2->modified_by = 0;
                                $contract2->modified_at = time();
                            } else {
                                $company = $invitation_contract->company;
                                $company->attributes = $_POST['Company'];
                                $company->modified_by = 0;
                                $company->modified_at = time();
                                $company->type = 1;
                                $company->status = 1;
                                $company->save();
                            }

                            if ($model->save()) {
                                if (!$invitation_contract) {
                                    $company->save();
                                    $contract->save();
                                    $contract2->save();
                                    if (isset($_POST['PayrateName'])) {
                                        foreach ($_POST['PayrateName'] as $name) {
                                            $pn = new PayrateName;
                                            $pn->id = AppInterface::getUniqueId();
                                            $pn->company_id = $company->id;
                                            $pn->name = str_replace(' ', '_', strtolower($name));
                                            $pn->save();
                                        }
                                        foreach ($_POST['PayrateName'] as $name) {
                                            $contractsettings = new ContractSetting;
                                            $contractsettings->id = AppInterface::getUniqueId();
                                            $contractsettings->contract_id = $contract->id;
                                            $contractsettings->key = $name;
                                            $contractsettings->value = '10';
                                            $contractsettings->category = 'payrate';
                                            $contractsettings->created_by = AppUser::getUserId();
                                            $contractsettings->modified_by = AppUser::getUserId();
                                            $contractsettings->created_at = time();
                                            $contractsettings->modified_at = time();
                                            $contractsettings->save();
                                        }
                                    }
                                } else {
                                    $invitation_contract->invitation_code = null;
                                    $invitation_contract->status = 1;
                                    $invitation_contract->save();
                                }

                                $model->verification_code = User::createRandomPassword(50);
                                $model->save();
                                $url = Yii::app()->createAbsoluteUrl('/user/main/verifyemail') . '/vcode/' . $model->verification_code;
                                $mailer = new AppMailer();
                                $mailer->prepareBody('ConfirmEmail', array('NAME' => $model->full_name, 'CONFIRMLINK' => $url, 'EMAIL' => $model->email));
                                $mailer->sendMail(array(array('email' => $model->email, 'name' => $model->full_name)), array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'SpeedyPayrolls'));

                                $mailer1 = new AppMailer();
                                $msg = 'A new signup of ' . $model->first_name . ' with email address: ' . $model->email . ' is pending.';
                                $mailer1->prepareBody('Email', array('BODY' => $msg));
                                $mailer1->sendMail(array(array('email' => AppUser::get_super_user()->email, 'name' => 'Super Admin')), array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls'));

                                if (isset($_POST['Company']['payroller_id'])) {

                                    $mailer2 = new AppMailer();
                                    $current_payroller = User::model()->findByPk($_POST['Company']['payroller_id']);
                                    $msg2 = 'A new employee ' . $model->first_name . ' has nominated you as their payroller via ' . $_POST['Company']['name'] . '. Please login to your SpeedyPayroll account and accept this request.';
                                    $mailer2->prepareBody('Email', array('BODY' => $msg2));
                                    $mailer2->sendMail(array(array('email' => $current_payroller->email, 'name' => $current_payroller->first_name)), array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls'));
                                }
                                $status = AppUser::loginUser($model->email, $model->password);
                                if ($status === false) {
                                    $error = Yii::app()->user->setFlash('message', "Form should be filled properly");
                                    AppLogging::addLog($error, 'error', 'application.user.controller.main');
                                } else {
                                    $error = Yii::app()->user->setFlash('message', "SignUp Successfully");
                                    AppLogging::addLog($error, 'success', 'application.user.controller.main');
                                    AppLogging::addLog('User Created', 'success', 'application.user.controller.main');
                                    if (AppUser::isUserAdmin()) {
                                        $this->redirect(array('/site/index'));
                                    }
                                    $this->redirect(array('/site/index'));
                                }
                            }
                        }
                    } else {
                        throw new CHttpException(400, 'Invalid Request.');
                    }
                } else {
                    $error = 'Account with this email address already exist.';
                }
            } else {
                $model->attributes = $_POST['User'];
                $model->password = md5($_POST['User']['password']);
                $model->save();
                $contract->invitation_code = null;
                $contract->status = 1;
                $contract->save();
                $status = AppUser::loginUser($model->email, $model->password);
                if ($status === false) {
                    $error = Yii::app()->user->setFlash('message', "Form should be filled properly");
                    AppLogging::addLog($error, 'error', 'application.user.controller.main');
                } else {
                    $error = Yii::app()->user->setFlash('message', "SignUp Successfully");
                    Yii::app()->user->setFlash('success', "Thank you for completing your profile.");
                    AppLogging::addLog($error, 'success', 'application.user.controller.main');
                    AppLogging::addLog('User Created', 'success', 'application.user.controller.main');
                    if (AppUser::isUserAdmin()) {
                        $this->redirect(array('/site/index'));
                    }
                    $this->redirect(array('/site/index'));
                }
            }
        }

        $this->render('signup-2', array(
            'model' => $model,
            'error' => $error,
            'invite' => $isinvited,
            'invitation_contract' => $invitation_contract,
            'payrollers' => $payrollers,
        ));
    }

    public function actionCustomTemplate() {
        $this->layout = '//layouts/login';
        if (count($_POST) > 0) {
            $user = User::model()->findByAttributes(array('email' => Yii::app()->user->getState('signup_email')));
            $company = AppUser::getUserCompany($user->id);
            $msg = "<table><tr><td>Hello,</td></tr><tr><td>A new user with company " . $company->name . " and admin email " . Yii::app()->user->getState('signup_email') . " has signed up. User requested for custom timesheet.</td></tr><tr><td>Following are the details of custom timesheet:</td></tr><tr><td><b>Payrates</b></td></tr>";
            for ($i = 0; $i < count($_POST["payrate"]); $i++) {
                $msg .= "<tr><td>" . $_POST["payrate"][$i] . "</td></tr>";
            }
            $msg .= "<tr><td><b>Items:</b></td></tr>";
            for ($i = 0; $i < count($_POST["item"]); $i++) {
                $msg .= "<tr><td>" . $_POST["item"][$i] . "</td></tr>";
            }
//            $user = User::model()->findByAttributes(array('email' => Yii::app()->user->getState('signup_email')));
//            Yii::app()->user->setState('plan_type', 1);
//            $user->plan_type = Yii::app()->user->getState('plan_type');
//            $user->save();

            $mailer1 = new AppMailer();
            $mailer1->prepareBody('Email', array('BODY' => $msg));
            $mailer1->sendMail(array(array('email' => 'steven@speedypayrolls.com', 'name' => 'Super Admin')), array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls'));

            $mailer1 = new AppMailer();
            $mailer1->prepareBody('Email', array('BODY' => $msg));
            $mailer1->sendMail(array(array('email' => 'tester@siliconplex.com', 'name' => 'Test')), array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls'));
            echo 'success';
        } else {
            echo 'failure';
        }
    }

    public function actionVerifymessage() {
        $this->layout = '//layouts/login';
        $this->render('verifymessage');
    }

    public function actionLogin() {
        $this->layout = '//layouts/login';
        $model = new User();
        $error = '';
        if (isset($_POST['email']) && isset($_POST['password'])) {
            $status = AppUser::loginUser($_POST['email'], md5($_POST['password']));
            if ($status === false) {
                $error = Yii::app()->user->getFlash('error-message');
            } else {
                $company = AppUser::getUserCompany();
                $user = AppUser::getCurrentUser();
                AppUser::updateUserToken($user);
                Yii::app()->user->setState('token', $user->token);
                if ($company) {
                    Yii::app()->session['privileges'] = CompanyPriveledge::model()->findAllByAttributes(array("company_id" => $company->id, "status" => "active"));
                }
                if (AppUser::isUserSuperAdmin()) {
                    $this->redirect(array('/site/index'));
                } else if (AppUser::isUserAdmin()) {
                    if (!AppUser::isUserPayroller()) {
                        $company = AppUser::getUserCompany();
                    }
                    if (AppUser::userFirstLogin()) {
                        AppUser::updateUserFirstLogin();
                        $this->redirect(array('/contract/main/update/id/' . AppContract::getInternalContract(AppUser::getCurrentUser()->id)->id));
                    }
                    $this->redirect(array('/company/companyShifts/timeinout/'));
                } else {
                    if (!AppUser::isUserPayroller()) {
                        $this->redirect(array('/company/companyShifts/timeinout/'));
                    } else {
                        $this->redirect(array('/site/index'));
                    }
                }
            }
        }
        $this->render('login', array(
            'model' => $model, 'error' => $error
        ));
    }

    public function actionLogout() {
        Yii::app()->session['privileges'] = null;
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionRegistrationPending() {
        $this->layout = '//layouts/login';

        Yii::app()->user->logout();

        $this->render('registrationPending');
    }

    public function actionCheckUserExistInCompany($email, $company1 = '', $company2 = '') {

        if ($user = User::prepareUserForAuthorisation($email)) {
            if ($user instanceof User) {
                if ($user) {
                    $c = new CDBCriteria();
                    $c->addCondition('user_id = :userid');
                    $c->addCondition('company_id = :company1 OR company_id = :company2');
                    $c->addCondition('is_internal = 1');
                    $c->params = array(
                        ':userid' => $user->id,
                        ':company1' => $company1,
                        ':company2' => $company2,
                    );
                    $company = Contract::model()->find($c);
                    if ($company) {
                        $response = array('status' => 1, 'data' => $this->renderpartial("_userDetails", array('user' => $user), true));
                    } else {
                        $response = 0;
                    }
                } else {
                    $response = 0;
                }
            } else {
                $response = 0;
            }
        } else {
            $response = 0;
        }
        AppSetting::generateAjaxResponse($response);
    }

    public function actionCheckCompanyExist($name) {
        $company = Company::model()->findAllByAttributes(array('name' => $name, 'created_by' => AppUser::getUserId()));
        if (count($company) > 0) {
            $response = 1;
        } else {
            $response = 0;
        }
        AppSetting::generateAjaxResponse($response);
    }

    public function actionCheckUserExist($email, $companyId = '', $detail = '') {
        if ($user = User::prepareUserForAuthorisation($email)) {
            if ($user instanceof User) {
                if ($detail) {
                    $company = AppContract::getInternalContract($user->id)->company;
                    $response = array_merge($user->attributes, array('company_id' => $company->id), array('company_name' => $company->name));
                } else {
                    $c = new CDBCriteria();
                    $c->addCondition('user_id = :userid');
                    $c->addCondition('company_id = :companyId');
                    $c->params = array(
                        ':userid' => $user->id,
                        ':companyId' => $companyId,
                    );
                    $company = Contract::model()->find($c);
                    if ($company) {
                        $response = 1;
                    } else {
                        $response = 0;
                    }
                }
            } else {
                $response = 0;
            }
        } else {
            $response = 0;
        }
        AppSetting::generateAjaxResponse($response);
    }

    public function actionChangePassword() {
        if (isset($_POST['user_password'])) {
            if (AppUser::isUserLogin()) {
                $user = AppUser::getCurrentUser();
                if ($user->password != md5($_POST['old_password'])) {
                    Yii::app()->user->setFlash('error', 'Old Password Not Match');
                    AppLogging::addLog('Change Password Unsuccessful', 'error', 'application.user.controller.main');
                    $this->redirect(array('/site/index'));
                } else {
                    $user->password = md5($_POST['user_password']);
                    Yii::app()->user->setFlash('success', 'Password Change Successfully');
                    $user->save();
                    AppLogging::addLog('Change Password', 'success', 'application.user.controller.main');
                }
            }
            $this->redirect(array('/site/index'));
        }
        $this->render('change');
    }

    public function actionForgot() {
        $this->layout = '//layouts/login';
        $error = '';
        if (isset($_POST['email'])) {
            $email = $_POST['email'];
            $user = User::model()->findByAttributes(array('email' => $email));
            if ($user) {
                $password = User::model()->createRandomPassword();
                $user->password = md5($password);
                $user->save();
                $mailer = new AppMailer();
                $mailer->prepareBody('ResetPassword', array('NAME' => $user->first_name . ' ' . $user->last_name, 'PASSWORD' => $password, 'EMAIL' => $user->email));
                $mailer->sendMail(array(array('email' => $user->email, 'name' => $user->first_name . ' ' . $user->last_name)), array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'SpeedyPayrolls'));
                Yii::app()->user->setFlash('success', 'An email has been sent to your email address with autogenerated password, use that password for login.');
                AppLogging::addLog('Forget Password', 'success', 'application.user.controller.main');
                $this->redirect('login');
            } else {
                Yii::app()->user->setFlash('error-message', 'Email Address You Provide Does Not Exist In Speedy Payrolls.');
                $this->redirect('login');
            }
        }
        $this->render('forgot');
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new User('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['User']))
            $model->attributes = $_GET['User'];

        $this->render('admin', 'super_admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return User the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = User::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param User $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionLoginasCompany($id) {
        $company = Company::model()->findByPk($id);
        $user = $company->getadmin();
        if ($company) {
            Yii::app()->session['privileges'] = CompanyPriveledge::model()->findAllByAttributes(array("company_id" => $company->id, "status" => "active"));
        }
        Yii::app()->user->setState('sa_id', AppUser::getUserId());
        Yii::app()->user->setState('sa_fname', AppUser::getCurrentUser()->first_name);
        Yii::app()->user->setState('sa_lname', AppUser::getCurrentUser()->last_name);
        Yii::app()->user->setState('sa_email', AppUser::getCurrentUser()->email);
        Yii::app()->user->setState('sa_roles', 'super_admin');
        if (isset($user)) {

            Yii::app()->user->setState('id', $user->id);
            Yii::app()->user->setState('first_name', $user->first_name);
            Yii::app()->user->setState('last_name', $user->last_name);
            Yii::app()->user->setState('email', $user->email);
            Yii::app()->user->setState('roles', AppUser::getUserType($user->id));
            AppUser::updateUserToken($user);
            Yii::app()->user->setState('token', $user->token);
        }
        if (AppUser::isUserAdmin()) {
            $this->redirect(array('/site/index'));
        } else {
            if (!AppUser::isUserPayroller()) {
                $this->redirect(array('/company/companyShifts/timeinout/'));
            } else {
                $this->redirect(array('/site/index'));
            }
        }
    }

    public function actionLoginasUser($id) {
        Yii::app()->user->setState('sa_id', AppUser::getUserId());
        Yii::app()->user->setState('sa_fname', AppUser::getCurrentUser()->first_name);
        Yii::app()->user->setState('sa_lname', AppUser::getCurrentUser()->last_name);
        Yii::app()->user->setState('sa_email', AppUser::getCurrentUser()->email);
        Yii::app()->user->setState('sa_roles', 'super_admin');
        $user = User::model()->findByPk($id);
        if (isset($user)) {

            Yii::app()->user->setState('id', $id);
            Yii::app()->user->setState('first_name', $user->first_name);
            Yii::app()->user->setState('last_name', $user->last_name);
            Yii::app()->user->setState('email', $user->email);
            Yii::app()->user->setState('roles', AppUser::getUserType($id));
            AppUser::updateUserToken($user);
            Yii::app()->user->setState('token', $user->token);
        }
        $company = AppUser::getUserCompany();
        if ($company) {
            Yii::app()->session['privileges'] = CompanyPriveledge::model()->findAllByAttributes(array("company_id" => $company->id, "status" => "active"));
        }
        if (AppUser::isUserAdmin()) {
            $this->redirect(array('/site/index'));
        } else {
            if (!AppUser::isUserPayroller()) {
                $this->redirect(array('/company/companyShifts/timeinout/'));
            } else {
                $this->redirect(array('/site/index'));
            }
        }
    }

    public function actionLogoutUser() {
        Yii::app()->session['privileges'] = null;
        Yii::app()->user->setState('id', Yii::app()->user->getState('sa_id'));
        Yii::app()->user->setState('first_name', Yii::app()->user->getState('sa_name'));
        Yii::app()->user->setState('last_name', Yii::app()->user->getState('sa_lname'));
        Yii::app()->user->setState('email', Yii::app()->user->getState('sa_email'));
        Yii::app()->user->setState('roles', Yii::app()->user->getState('sa_roles'));

        $this->redirect(array('/site/index'));
    }

}
