<?php

class EmployeeController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('add', 'uploademployee', 'sendemployees', 'addmultiple', 'downloadtemplate'),
                'roles' => array('employee', 'admin'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('index', 'employeeList', 'export', 'assignshift', 'assignsupervisor', 'changepayrate'),
                'roles' => array('admin', 'employee'),
            ),
            array('deny', // deny all users
                'roles' => array('*'),
            ),
        );
    }

    public function actionExport() {
        $webroot = Yii::getPathOfAlias('webroot');
        if (!is_dir($webroot . '/uploads/timesheets')) {
            mkdir($webroot . '/uploads/timesheets');
            chmod(($webroot . '/uploads/timesheets'), 0755);
            // the default implementation makes it under 777 permission, which you could possibly change recursively before deployment, but here's less of a headache in case you don't
        }
        $file = $webroot . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'timesheets' . DIRECTORY_SEPARATOR . 'timesheets-' . date('d-m-Y-H-i') . '.csv';
        $handle = fopen($file, 'w');
        $user = null;
        $export_fields = null;
        if (isset($_POST['entry']))
            $user = $_POST['entry'];
        if (isset($_POST['export_fields'])) {
            $export_fields = $_POST['export_fields'];
        }
        $this->getTimesheetCSVContents($file, $user, $export_fields);
        fclose($handle);
        $curr_user = AppUser::getCurrentUser();
        $url = $this->createAbsoluteUrl('/uploads/timesheets/' . 'timesheets-' . date('d-m-Y-H-i') . '.csv');
        $this->layout = "//layouts/blank";
        $response = array('status' => false, 'data' => '', 'reason' => 'Not a Valid Ajax Request or Post Data is Missing');
        $response['status'] = true;
        $response['reason'] = '';
        $response['url'] = $url;
        $response['data'] = $this->render('viewlist', array(
            'file' => $file,
            'name' => 'timesheets-' . date('d-m-Y-H-i') . '.csv',
            'url' => $url
                ), true);
        AppSetting::generateAjaxResponse($response);

//        $mailer = new AppMailer();
//        $msg = 'Attached is the exported file you requested.';
//        $mailer->prepareBody('invoice', array('INVOICE' => $msg));
//        $mailer->sendSesMailWithAttach(array('email' => $curr_user->email, 'name' => $curr_user->full_name), array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'),'name' => 'Speedy Payrolls'),"SpeedyPayroll Notification",$msg,$file,'timesheets-'.date('d-m-Y-H-i').'.csv');
//        Yii::app()->user->setFlash('success', 'Export successfully, an email will be sent to you shortly.');
//        $this->redirect($this->createUrl("/user/employee"));
    }

    public function actionSendEmployees($file, $name) {
        $curr_user = AppUser::getCurrentUser();
        $mailer = new AppMailer();
        $msg = 'Attached is the exported file you requested.';
        $mailer->prepareBody('invoice', array('INVOICE' => $msg));

        $mailer->sendSesMailWithAttach(array('email' => $curr_user->email, 'name' => $curr_user->full_name), array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls'), "SpeedyPayroll Notification", $msg, $file, $name);
        Yii::app()->user->setFlash('success', 'Export successfully, an email will be sent to you shortly.');
        $this->redirect($this->createUrl("/user/employee"));
    }

    private function getTimesheetCSVContents($file, $users = null, $export_fields = null) {
        if (count($users) > 0) {
            $internalContracts = AppCompany::getAllInternalContractsByUsers(AppUser::getUserCompany()->id, $users);
        } else {

            $internalContracts = AppCompany::getAllInternalContracts(AppUser::getUserCompany()->id);
        }
        $ids = "";
        for ($i = 0; $i < count($internalContracts); $i++) {
            $ids.= $internalContracts[$i]->id . ", ";
        }
        $ids = substr($ids, 0, -2);
        $sql = "SELECT DISTINCT ";
        $labels = array();
        if (count($export_fields) > 0) {
            $last_item = end($_POST['export_fields']);
            foreach ($export_fields as $item) {
                if ($item == $last_item) {
                    $sql .= "u." . $item;
                } else {
                    $sql .= "u." . $item . ",";
                }
                array_push($labels, UserTimings::model()->getAttributeLabel($item));
            }
        } else {
            $sql .= "u.id AS user_id, u.first_name,u.last_name, "
                    . "u.email, u.ni_number, u.dob, u.phone, u.addressline_1, u.street, u.city, u.country, u.postcode";
            $labels = array(User::model()->getAttributeLabel("id"), User::model()->getAttributeLabel("first_name"),
                User::model()->getAttributeLabel("last_name"), User::model()->getAttributeLabel("email"),
                User::model()->getAttributeLabel("ni_number"), User::model()->getAttributeLabel("dob"),
                User::model()->getAttributeLabel("phone"), User::model()->getAttributeLabel("addressline_1"),
                User::model()->getAttributeLabel("street"), User::model()->getAttributeLabel("city"),
                User::model()->getAttributeLabel("country"), User::model()->getAttributeLabel("postcode"));
        }
        $sql .= " FROM "
                . "sp_user AS u INNER JOIN sp_contract c ON u.id=c.user_id WHERE "
                . "(c.id IN (" . $ids . ") OR parent_id IN (" . $ids . ")) AND c.role != 'admin' AND c.is_internal=1 "
                . "ORDER BY user_id";
        $command = Yii::app()->db->createCommand($sql);

        $result = $command->queryAll();
        foreach ($labels as $item) {
            file_put_contents($file, $item . ',', FILE_APPEND);
        }

        $payrate_names = PayrateName::model()->findAllByAttributes(array('company_id' => AppUser::getUserCompany()->id, 'is_item' => 0));

        foreach ($payrate_names as $pname) {
            if (!empty($pname)) {
                file_put_contents($file, $pname->name . ' (rate),', FILE_APPEND);
                file_put_contents($file, $pname->name . ' (time),', FILE_APPEND);
            }
        }
        $items = PayrateName::model()->findAllByAttributes(array('company_id' => AppUser::getUserCompany()->id, 'is_item' => 1));

        foreach ($items as $name) {
            if (!empty($name)) {
                file_put_contents($file, $name->name . ' (rate),', FILE_APPEND);
                file_put_contents($file, $name->name . ' (quantity),', FILE_APPEND);
            }
        }
        file_put_contents($file, "\r\n", FILE_APPEND);
        foreach ($result as $item) {
            foreach ($item as $key => $value) {
                file_put_contents($file, $value . ',', FILE_APPEND);
            }
            foreach ($payrate_names as $pname) {
                if (!empty($pname)) {
                    $cs = AppContract::getContractSettingsByKey($item['contract_id'], $pname->name);
                    if ($cs != null) {
                        $payrate = Payrate::model()->findByAttributes(array('contract_setting_id' => $cs->id, 'timesheet_id' => $result[$i]['timesheet_id']));
                        if ($payrate != null) {
                            file_put_contents($file, $payrate["value"] . ',', FILE_APPEND);
                            file_put_contents($file, $payrate["hours"] . ',', FILE_APPEND);
                        } else {
                            file_put_contents($file, '-,', FILE_APPEND);
                            file_put_contents($file, '-,', FILE_APPEND);
                        }
                    }
                }
            }
            foreach ($items as $name) {
                if (!empty($pname)) {
                    $cs = AppContract::getContractSettingsByKey($item['contract_id'], $pname->name);
                    if ($cs != null) {
                        $payrate = Payrate::model()->findByAttributes(array('contract_setting_id' => $cs->id, 'timesheet_id' => $result[$i]['timesheet_id']));
                        if ($payrate != null) {
                            file_put_contents($file, $payrate["value"] . ',', FILE_APPEND);
                            file_put_contents($file, $payrate["hours"] . ',', FILE_APPEND);
                        } else {
                            file_put_contents($file, '-,', FILE_APPEND);
                            file_put_contents($file, '-,', FILE_APPEND);
                        }
                    }
                }
            }

            file_put_contents($file, "\r\n", FILE_APPEND);
        }
    }

    public function actionAssignshift() {
//        dd($_POST);
        if ($_POST > 0 && isset($_POST['users']) && isset($_POST['shift_id'])) {
            foreach ($_POST['users'] as $id) {
                if ($id != 'on') {
                    $user_exist = AppUser::getUserById($id);
                }
                if (isset($user_exist)) {
                    $user_exist->shift_id = $_POST['shift_id'];
                    $user_exist->update();
                }
            }
        }
        $msg = ConstantMessages::$shift_created;
        Yii::app()->user->setFlash('success', $msg);
        return TRUE;
    }

    public function actionAssignsupervisor() {
//        dd($_POST
        if ($_POST > 0 && isset($_POST['users']) && isset($_POST['supervisor_id'])) {
            foreach ($_POST['users'] as $id) {
                if ($id != 'on') {
                    $contract_exist = AppContract::getInternalContract($id);
                }
                if (isset($contract_exist)) {
                    $contract_exist->approver_id = $_POST['supervisor_id'];
                    $contract_exist->update();
                }
            }
            $msg = ConstantMessages::$assign_supervisor;
            Yii::app()->user->setFlash('success', $msg);
            return TRUE;
        }
    }

    public function actionChangepayrate() {
        $data = array();
        parse_str($_POST['payrates'], $data);
        if (isset($_POST['users']) && isset($_POST['payrates'])) {
            foreach ($_POST['users'] as $id) {
                if ($id != 'on') {
                    $contract_exist = AppContract::getInternalContract($id);
                }
                if (isset($contract_exist)) {
                    $contract_setting = AppContract::getContractSetting($contract_exist->id);

                    if (isset($contract_setting)) {
                        foreach ($contract_setting as $item) {
                            $payrates = AppCompany::getPayrateNames();
                            foreach ($payrates as $payratename) {
//                                                                    dd($_POST['Payrate'][$contract->key]);
                                if ($payratename->name == $item->key) {
                                    $item->value = $data['Payrate'][$item->key];
                                    $item->update();
                                }
                            }
                            $payrates = AppCompany::getItems();
                            foreach ($payrates as $payratename) {
                                $item->value = $data['Item'][$payratename->name];
                                $item->update();
                            }
                        }
                    }
//                    success msg
                } else {
//     internal contract empty msg
                    $msg = 'Contract is not exist';
                    Yii::app()->user->setFlash('error', $msg);
                }
            }
            $msg = 'Successfully updated!';
            Yii::app()->user->setFlash('success', $msg);
        }

        return TRUE;
    }

    public function actionIndex() {
//        dd($_POST);
        $company = AppUser::getUserCompany();
        $shifts = CompanyShifts::model()->findAllByAttributes(array("company_id" => $company->id));

        if (isset($_POST['formData']['User']['first_name']) && isset($_POST['formData']['User']['last_name']) &&
                isset($_POST['formData']['User']['email']) && isset($_POST['formData']['User']['staff_id']) &&
                isset($_POST['formData']['User']['ni_number']) && isset($_POST['formData']['Contract']['start_time']) &&
                !empty($_POST['formData']['User']['first_name']) && !empty($_POST['formData']['User']['last_name']) &&
                !empty($_POST['formData']['User']['email']) && !empty($_POST['formData']['User']['staff_id']) &&
                !empty($_POST['formData']['User']['ni_number']) && !empty($_POST['formData']['Contract']['start_time'])) {
            $user_exist = User::prepareUserForAuthorisation($_POST['formData']['User']['email']);
            if (!$user_exist) {
                $user = AppUser::addStaff($_POST['formData'], AppUser::getUserId());
                if (isset($user)) {
                    $email = AppUser::sendEmail($user);
                    $msg = ConstantMessages::$user_created;
                    Yii::app()->user->setFlash('success', $msg);
                    $this->redirect('employee');
                }
            } else {
                $error = ConstantMessages::$user_exist;
                Yii::app()->user->setFlash('error', $error);
                $this->redirect('employee');
            }
        }

        if (count($_POST) > 0 && isset($_POST['entry'])) {
            $this->redirect(array('export', 'users' => $_POST['entry']));
        }
        $company = AppUser::getUserCompany();
        if ($company != null) {
            $users = AppCompany::getCompanyEmployee($company->id);
        } else {
            $users = null;
            $this->render('index', array('dataProvider' => $users, 'approvers' => AppContract::get_all_internal_users()));
        }
        $this->render('index', array('dataProvider' => $users, 'approvers' => AppContract::get_all_internal_users(), 'shifts' => $shifts));
    }

    public function actionEmployeeList() {
        $company = AppUser::getUserCompany();
        if ($company != null) {
            $employees = AppUser::get_all_users_by_company($company->id);
        } else {
            $employees = null;
            $this->render('employeelist', array('dataProvider' => $employees));
        }
    }

    public function actionAdd() {
        $error = '';
        $user = new User;
        $contract = new Contract;
        $company = AppUser::getUserCompany();
        $shifts = CompanyShifts::model()->findAllByAttributes(array("company_id" => $company->id));
        $company_shifts = array("" => "Please select a shift");
        foreach ($shifts as $item) {
            $company_shifts[$item->id] = $item->title;
        }
        if (isset($_POST['User'])) {
            $user->attributes = $_POST['User'];
            $contract->attributes = $_POST['Contract'];

            $user2 = User::prepareUserForAuthorisation($_POST['User']['email']);
            if (!$user2) {
                $user->id = AppInterface::getUniqueId();
                $user->password = md5($_POST['User']['password']);
                $user->created_at = time();
                $user->created_by = '0';
                $user->modified_at = time();
                $user->modified_by = '0';
                $user->type = 1;
                $user->is_verified = 1;
                $user->shift_id = $_POST['User']['shift_id'];
                $contract->id = AppInterface::getUniqueId();

                $contract->user_id = $user->id;
                $contract->company_id = $company->id;
                $contract->role = 'Employee';
                $contract->status = 1;
                $contract->start_time = DateTime::createFromFormat(AppInterface::getdateformat(), $_POST['Contract']['start_time'])->getTimestamp();
                if ($_POST['Contract']['end_time'])
                    $contract->end_time = DateTime::createFromFormat(AppInterface::getdateformat(), $_POST['Contract']['end_time'])->getTimestamp();
                else
                    $contract->end_time = "";
                $contract->type = "in";
                $contract->is_internal = 1;
                $contract->created_at = time();
                if (AppUser::canPayroll()) {
                    $contract->payroller_id = $company->payroller_id;
                }
                $contract->collection_type = $_POST['Contract']['collection_type'] == 1 ? ($_POST['Settings']['collection_day_first'] == 1 ? 1 : 2) : 0;
                $contract->created_by = 0;
                if (isset($_POST['Contract']['approver_id'])) {
                    $contract->approver_id = $_POST['Contract']['approver_id'];
                } else{
                    $contract->approver_id = AppUser::getUserId();
                }
                $contract->owner_id = AppUser::getUserId();
                if ($_FILES['Contract']['name']['file_name']) {
                    $contract->file_name = CUploadedFile::getInstance($contract, 'file_name');
                    $images_path = realpath(Yii::app()->basePath . '/../uploads/contracts');
                    $filename = explode('.', $contract->file_name);
                    $filename[0] = AppInterface::getFilename();
                    $newfilename = $filename[0] . '.' . $filename[1];
                    $contract->file_name->saveAs($images_path . '/' . $newfilename);
                    $contract->file_name = $newfilename;
                }


                $contract->created_at = time();
                $contract->modified_by = 0;
                $contract->modified_at = time();
                $contract->validate();

                if ($user->save()) {
                    $contract->save();
                    if (isset($_POST['Payrate'])) {
                        foreach ($_POST['Payrate'] as $name => $rate) {
                            AppCompany::addContractSetting(array('contract_id' => $contract->id, 'key' => $name, 'value' => $rate, 'category' => 'payrate', 'created_by' => AppUser::getUserId(), 'modified_by' => AppUser::getUserId(), 'created_at' => time(), 'modified_at' => time()));
                        }
                    }

                    if (isset($_POST['Item'])) {
                        foreach ($_POST['Item'] as $name => $rate) {
                            AppCompany::addContractSetting(array('contract_id' => $contract->id, 'key' => $name, 'value' => $rate, 'category' => 'item', 'created_by' => AppUser::getUserId(), 'modified_by' => AppUser::getUserId(), 'created_at' => time(), 'modified_at' => time()));
                        }
                    }
                    $owner = AppUser::getCurrentUser()->full_name;
                    $current_payroller = $company->payroller_id;
                    $payroller_email = User::model()->findByPk($current_payroller);
                    $notification = Yii::app()->getModule('notification');
                    if (AppUser::canPayroll() && isset($payroller_email)) {
                        $msg2 = 'A new employee ' . $user->first_name . ' has nominated you as their payroller via ' . AppUser::getUserCompany()->name . '. Please login to your SpeedyPayroll account and accept this request.';
                        $notification->send(array(
                            'email' =>
                            array('template' => 'Email', 'params' => array('BODY' => $msg2),
                                'to' => array(array('email' => $payroller_email->email, 'name' => $payroller_email->full_name)),
                                'from' => array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls')
                            )
                        ));
                    }
                    $msg = ConstantMessages::employeeAdd();
                    $not_result = AppInterface::notification($user->id, 1, 0, $contract->id, $user->id);
                    $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
                    $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;
                    $notifier = new Notification();
                    $notification_count = $notifier->getUserNotificationUnseenCount();
                    $notification->send(
                            array(
                                'email' =>
                                array('template' => 'addemployee', 'params' => array('CREATOR' => $owner, 'USERNAME' => $user->email, 'PASSWORD' => $_POST['User']['password'], 'NAME' => $user->first_name),
                                    'to' => array(array('email' => $user->email, 'name' => $user->first_name)),
                                    'from' => array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls')
                                ),
                                'email' =>
                                array('template' => 'Email', 'params' => array('BODY' => $msg),
                                    'to' => array(array('email' => AppUser::get_super_user()->email, 'name' => 'Super Admin')),
                                    'from' => array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls')
                                ),
                                'push' => array('server_notification',
                                    array('id' => $user->id,
                                        'msg' => sprintf($not_result->notificationType->notice, AppUser::getCurrentUser()->first_name), 'url' => $url, 'logo' => $logo, 'count' => $notification_count)
                                )
                    ));
//                    $notification = new PushNotify();
//                    $notification->emit('server_notification', array('id' => $user->id,'msg' => $msg));
                    Yii::app()->user->setFlash('success', 'A new Employee has been successfully added to your company');
                    $this->redirect('index');
                }
            } else {
                $error = 'User with this email address already exist.';
                Yii::app()->user->setFlash('error', $error);
                $this->redirect('add');
            }
        }
        $this->render('add', array(
            'approvers' => AppContract::get_all_internal_users(),
            'error' => $error,
            'user' => $user,
            'contract' => $contract,
            'shifts' => $company_shifts,
        ));
    }

    public function actionUploadEmployee() {
        if (Yii::app()->request->isPostRequest) {
            if ($_FILES['file_name']) {
                //dd('here');
                $fileUpload = CUploadedFile::getInstance($contract, 'file_name');
                $images_path = realpath(Yii::app()->basePath . '/../uploads/employees');
                $filename = explode('.', $fileUpload);
                $filename[0] = AppInterface::getFilename();
                $newfilename = $filename[0] . '.' . $filename[1];
                $fileUpload->saveAs($images_path . '/' . $newfilename);
                $fileUpload = $newfilename;
            }
        }
        $this->render('addmultiple');
    }

    private function import_emp_csv($file) {
        $handle = fopen($file, "r");
        while (!feof($handle)) {
            print_r(fgetcsv($file));
        }
    }

    public function download_template() {
        $this->create_template();
    }

    private function create_template() {
        $webroot = Yii::getPathOfAlias('webroot');
        $file = $webroot . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'contracts' . DIRECTORY_SEPARATOR . AppUser::getUserCompany()->id . '.csv';
        $handle = fopen($file, 'w');

        $this->getattributes($file);
        fclose($handle);
        Yii::app()->request->sendFile(AppInterface::getFilename() . '.csv', file_get_contents($file));
    }

    private function getattributes($file) {
        file_put_contents($file, 'first_name,', FILE_APPEND);
        file_put_contents($file, 'last_name,', FILE_APPEND);
        file_put_contents($file, 'email,', FILE_APPEND);
        file_put_contents($file, 'password,', FILE_APPEND);
        file_put_contents($file, 'ni_number,', FILE_APPEND);
        file_put_contents($file, 'employment type,', FILE_APPEND);
        file_put_contents($file, 'approver_email,', FILE_APPEND);
        file_put_contents($file, 'startdate,', FILE_APPEND);
        file_put_contents($file, 'enddate,', FILE_APPEND);
        file_put_contents($file, 'collection frequency,', FILE_APPEND);
        file_put_contents($file, 'day,', FILE_APPEND);
        file_put_contents($file, 'first or last,', FILE_APPEND);
        file_put_contents($file, 'staff_id,', FILE_APPEND);

        $payrate_names = PayrateName::model()->findAllByAttributes(array('company_id' => AppUser::getUserCompany()->id, 'is_item' => 0));

        foreach ($payrate_names as $pname) {
            if (!empty($pname)) {
                file_put_contents($file, $pname->name . '(payrate)' . ',', FILE_APPEND);
            }
        }
        $items = PayrateName::model()->findAllByAttributes(array('company_id' => AppUser::getUserCompany()->id, 'is_item' => 1));

        foreach ($items as $name) {
            if (!empty($name)) {
                file_put_contents($file, $name->name . '(item)' . ',', FILE_APPEND);
            }
        }
    }

    public $first = array();

    public function actionAddmultiple() {
        if (Yii::app()->request->isPostRequest) {

            $target_dir = "uploads/";
            $target_file = $target_dir . basename($_FILES["file_up"]["name"]);
            $uploadOk = 1;
            $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

            if (move_uploaded_file($_FILES["file_up"]["tmp_name"], $target_file)) {
                
            }
            $arr = array();
            $headerFlag = false;
            $count = 0;

            ////////Reading from  CSV file from Taha/////////////////

            $handle = fopen("uploads/" . basename($_FILES["file_up"]["name"]), "r");
            if ($handle) {
                $file = fread($handle, filesize("uploads/" . basename($_FILES["file_up"]["name"])));
                $l = preg_split('/\n|\r\n?/', $file); //explode("\r", $file);
                foreach ($l as $key => $val) {
                    if ($key == 0) {
                        $first = $l[$key];
                    } else {
                        $l[$key] = str_replace(array("\n", "\r"), '', $l[$key]);
                        $l[$key] = explode(",", $l[$key]);
                        $status = $this->addemp_from_csv($first, $l[$key]);
                        if ($status == true) {
                            $count++;
                        }
                    }
                }

                if ($count > 0)
                    Yii::app()->user->setFlash('success', $count . " user(s) has been succesfully added.");
                else
                    Yii::app()->user->setFlash('error', "Data Import Failed.");
                fclose($handle);
            } else {
                Yii::app()->user->setFlash('error', 'Problem adding employee list.');
                // error opening the file.
            }
        }
        $this->render('addmultiple');
    }

    private function addemp_from_csv($list_key, $list) {

        $length = count($list);
        $count = sizeof($list_key);
        $list_key = explode(",", $list_key);
        $curr_user = AppUser::getCurrentUser();
        if ($length > 1 && $length >= 13) {
            try {
                $user = new User();
                $user->id = AppInterface::getUniqueId();
                $user->first_name = $list[0];
                $user->last_name = $list[1];
                $user->email = $list[2];
                $user->password = md5($list[3]);
                $user->ni_number = $list[4];
                $user->type = 1;
                $user->created_at = time();
                $user->created_by = '0';
                $user->modified_at = time();
                $user->modified_by = '0';
                $user->is_verified = 1;
                $user->staff_id = $list[12];
                $user->shift_id = $curr_user->shift_id;
                $user->plan_type = $curr_user->plan_type;
                $user->save();

                $contract = new Contract();
                $contract->id = AppInterface::getUniqueId();
                $contract->user_id = $user->id;
                $contract->company_id = AppUser::getUserCompany()->id;
                $contract->role = 'Employee';
                $contract->employee_type = $list[5];
                $approver = User::model()->findByAttributes(array('email' => $list[6]));
                if ($approver) {
                    $contract->approver_id = $approver->id;
                }
                $contract->type = 'in';
                $contract->is_internal = 1;
                $contract->status = 1;
                $contract->payroller_id = AppUser::getUserCompany()->payroller_id;
                $contract->start_time = DateTime::createFromFormat(AppInterface::getdateformat(), $list[7])->getTimestamp();
                if ($list[8] != "")
                    $contract->end_time = DateTime::createFromFormat(AppInterface::getdateformat(), $list[8])->getTimestamp();

                if (strtolower($list[9]) == 'weekly') {
                    $contract->collection_type = 0;
                    $contract->collection_day = $list[10];
                } else {
                    $contract->collection_type = 1;
                    $contract->collection_day = $list[10];
                }
                $contract->created_at = time();
                $contract->modified_by = 0;
                $contract->created_by = 0;
                $contract->modified_at = time();
                $contract->owner_id = AppUser::getUserId();
                $contract->save();

                for ($i = 13; $i < count($list); $i++) {
                    $contractsettings = new ContractSetting;
                    $contractsettings->id = AppInterface::getUniqueId();
                    $contractsettings->contract_id = $contract->id;
                    if (strpos($list_key[$i], '(item)')) {
                        $contractsettings->category = 'item';
                    } else {
                        $contractsettings->category = 'payrate';
                    }
                    $contractsettings->created_by = AppUser::getUserId();
                    $contractsettings->modified_by = AppUser::getUserId();
                    $contractsettings->created_at = time();
                    $contractsettings->modified_at = time();
                    $contractsettings->key = trim(str_replace(array('(item)', '(payrate)'), array('', ''), $list_key[$i]));
                    $contractsettings->value = $list[$i];
                    $contractsettings->save();
                }
                return true;
            } catch (CException $e) {
                return false;
            }
        }
    }

    public function actionDownloadtemplate() {
        $this->download_template();
    }

}

