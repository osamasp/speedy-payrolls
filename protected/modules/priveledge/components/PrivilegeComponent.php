<?php

/*
 * Description: Component for User Module
 * Author: Usama Ayaz
 * Dated:  2-June-2014
 */

class PrivilegeComponent extends CComponent {

    public function __construct($ip = "") {
        
    }

    public static function privilegeWithoutFunction($privilege) {
        $cms = new Cms();
        $accessibility = array("access" => 0, "message" => "Sorry, you are already acceeds the quota for this action.", "title" => "Assigned Limit Exceeds");

        if (time() >= $privilege->start_time && (time() <= $privilege->end_time || $privilege->end_time == "" || $privilege->end_time == 0)) {
            $accessibility["access"] = 1;
            $accessibility["message"] = "success.";
            return $accessibility;
        } else {
            $temp = $cms->getCMSContentBySlug("privilege_duration");
            $accessibility["access"] = 0;
            $accessibility["message"] = $temp["content"];
            $accessibility["title"] = $temp["title"];
            return $accessibility;
        }
    }
    
public static function setCompanyPrivileges($formData){
        CompanyPriveledge::model()->updateAll(array("status" => 'in-active'), "company_id=".$formData["CompanyPriveledge"]["company_id"]);
            if (count($formData["privilege"]) > 0) {
                $query = "INSERT INTO sp_company_priveledge(id,priveledge_id,company_id,start_time,end_time,recursive,recursive_type,quota,status,created_at,modified_at,created_by,modified_by) VALUES";
                $arrayKeys = array_keys($formData["privilege"]);
                $lastelement = array_pop($arrayKeys);
                foreach ($formData["privilege"] as $key => $item) {
//                    $start_date = DateTime::createFromFormat('d/m/Y H:i:s', $formData['start_time'][$key] . ' 00:00:00');
                    if ($formData['end_time'][$key] == "") {
                        $end_time = 0;
                    } else {
                        $end_date = DateTime::createFromFormat('d/m/Y H:i:s', $formData['end_time'][$key] . ' 23:59:59');
                        $end_time = $end_date->getTimestamp();
                    }
                    $quota = $formData["quota"][$key];
                    $type = $formData["type"][$key];
                    $recursive = $formData["recursive"][$key];
                    $query .= "(" . AppInterface::getUniqueId() . "," . $key . "," . $formData["CompanyPriveledge"]["company_id"] . "," . DateTime::createFromFormat('d/m/Y H:i:s', $formData['start_time'][$key] . ' 00:00:00')->getTimestamp() . "," . $end_time . ",'" . $recursive . "','" . $type . "'," . $quota . ",'active'," . time() . "," . time() . "," . AppUser::getUserId() . "," . AppUser::getUserId() . ")";
                    if ($lastelement != $key) {
                        $query .= ",";
                    }
                }
                Yii::app()->db->createCommand($query)->execute();
            }
            CompanyPriveledge::model()->deleteAllByAttributes(array("company_id" => $formData["CompanyPriveledge"]["company_id"], "status" => "in-active"));
    }

    public static function createOneWayTimesheets($privilege) {
        $accessibility = array("access" => 0, "message" => "","title" => "");
        $cms = new Cms();
        $company = AppUser::getUserCompany();
        $user = AppUser::getUserId();
        $timesheets = QuickTimesheet::model()->findAllByAttributes(array("company_id" => $company->id, "created_by" => $user));
        if (time() >= $privilege->start_time && (time() <= $privilege["end_time"] || $privilege["end_time"] == "" || $privilege["end_time"] == 0)) {
            if ($privilege->quota == "-1") {
                $accessibility["access"] = 1;
                $accessibility["message"] = "success.";
                return $accessibility;
            } else if ($privilege->recursive == "Recursive") {
                if ($privilege->recursive_type == "Daily") {

//                dd(strtotime($d->format('d/m/Y') . ' 00:00:00') . " " . time());
                    $used_privilege = QuickTimesheet::model()->findAllBySql("select * from sp_quick_timesheet where company_id=" . $company->id . " and created_at BETWEEN " . strtotime(date('Y-m-d') . ' 00:00:00') . " AND " . time());
//                dd($used_privilege);
                    if (count($used_privilege) < $privilege->quota) {
                        $accessibility["access"] = 1;
                        $accessibility["message"] = "success.";
                        return $accessibility;
                    } else {
                        $temp = $cms->getCMSContentBySlug("privilege_limit");
                        $accessibility["access"] = 0;
                        $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                        return $accessibility;
                    }
                } else if ($privilege->recursive_type == "Weekly") {
                    $used_privilege = QuickTimesheet::model()->findAllBySql("select * from sp_quick_timesheet where company_id=" . $company->id . " and created_at between " . strtotime('last monday' . ' 00:00:00') . " AND " . time());
                    if (count($used_privilege) < $privilege->quota) {
                        $accessibility["access"] = 1;
                        $accessibility["message"] = "success.";
                        return $accessibility;
                    } else {
                        $temp = $cms->getCMSContentBySlug("privilege_limit");
                        $accessibility["access"] = 0;
                        $accessibility["message"] = $temp['limit'];
                        $accessibility["title"] = $temp['title'];
                        return $accessibility;
                    }
                } else if ($privilege->recursive_type == "Monthly") {
                    $d = new DateTime('first day of this month');
                    $used_privilege = QuickTimesheet::model()->findAllBySql("select * from sp_quick_timesheet where company_id=" . $company->id . " and created_at between " . strtotime($d->format('d/m/Y') . ' 00:00:00') . " AND " . time());
                    if (count($used_privilege) < $privilege->quota) {
                        $accessibility["access"] = true;
                        $accessibility["message"] = "success.";
                        return $accessibility;
                    } else {
                        $temp = $cms->getCMSContentBySlug("privilege_limit");
                        $accessibility["access"] = 0;
                        $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                        return $accessibility;
                    }
                }
            } else if ($privilege->recursive == "Non-Recursive") {
                if ($privilege->quota > count($timesheets)) {
                    $accessibility["access"] = 1;
                    $accessibility["message"] = "success.";
                    return $accessibility;
                } else {
                    $temp = $cms->getCMSContentBySlug("privilege_limit");
                    $accessibility["access"] = 0;
                    $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                    return $accessibility;
                }
            }
        } else {
            $temp = $cms->getCMSContentBySlug("privilege_duration");
            $accessibility["access"] = 0;
            $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
            return $accessibility;
        }
    }

    public static function createTwoWayTimesheets($privilege) {
        $cms = new Cms();
        $accessibility = array("access" => 0, "message" => "","title" => "");
        $company = AppUser::getUserCompany();
        $user = AppUser::getUserId();
//        $contract = AppContract::getEmployeeContracts($user, false, "out");
        $timesheets = Timesheet::model()->findAllByAttributes(array("created_by" => $user));
        if (time() >= $privilege->start_time && (time() <= $privilege->end_time || $privilege->end_time == "" || $privilege->end_time == 0)) {
            if ($privilege->quota == "-1") {
                $accessibility["access"] = 1;
                $accessibility["message"] = "success.";
                return $accessibility;
            } else if ($privilege->recursive == "Recursive") {
                if ($privilege->recursive_type == "Daily") {
//                dd(strtotime($d->format('d/m/Y') . ' 00:00:00') . " " . time());
                    $used_privilege = Timesheet::model()->findAllBySql("select * from sp_timesheet where created_by=" . $user . " and created_at BETWEEN " . strtotime(date('Y-m-d') . ' 00:00:00') . " AND " . time());
//                dd($used_privilege);
                    if (count($used_privilege) < $privilege->quota) {
                        $accessibility["access"] = 1;
                        $accessibility["message"] = "success.";
                        return $accessibility;
                    } else {
                        $temp = $cms->getCMSContentBySlug("privilege_limit");
                        $accessibility["access"] = 0;
                        $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                        return $accessibility;
                    }
                } else if ($privilege->recursive_type == "Weekly") {
                    $used_privilege = Timesheet::model()->findAllBySql("select * from sp_timesheet where created_by=" . $user . " and created_at between " . strtotime('last monday' . ' 00:00:00') . " AND " . time());
                    if (count($used_privilege) < $privilege->quota) {
                        $accessibility["access"] = 1;
                        $accessibility["message"] = "success.";
                        return $accessibility;
                    } else {
                        $temp = $cms->getCMSContentBySlug("privilege_limit");
                        $accessibility["access"] = 0;
                        $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                        return $accessibility;
                    }
                } else if ($privilege->recursive_type == "Monthly") {
                    $d = new DateTime('first day of this month');
                    $used_privilege = Timesheet::model()->findAllBySql("select * from sp_timesheet where created_by=" . $user . " and created_at between " . strtotime($d->format('d/m/Y') . ' 00:00:00') . " AND " . time());
                    if (count($used_privilege) < $privilege->quota) {
                        $accessibility["access"] = true;
                        $accessibility["message"] = "success.";
                        return $accessibility;
                    } else
                        $temp = $cms->getCMSContentBySlug("privilege_limit");
                    $accessibility["access"] = 0;
                    $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                    return $accessibility;
                }
            }
            else if ($privilege->recursive == "Non-Recursive") {
                if ($privilege->quota > count($timesheets)) {
                    $accessibility["access"] = 1;
                    $accessibility["message"] = "success.";
                    return $accessibility;
                } else {
                    $temp = $cms->getCMSContentBySlug("privilege_limit");
                    $accessibility["access"] = 0;
                    $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                    return $accessibility;
                }
            }
        } else {
            $temp = $cms->getCMSContentBySlug("privilege_duration");
            $accessibility["access"] = 0;
            $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
            return $accessibility;
        }
    }

    public static function createThreeWayTimesheets($privilege) {
        $cms = new Cms();
        $accessibility = array("access" => 0, "message" => "","title" => "");
        $company = AppUser::getUserCompany();
        $user = AppUser::getUserId();
//        $contract = AppContract::getEmployeeContracts($user, false, "out_b");
        $timesheets = Timesheet::model()->findAllByAttributes(array("created_by" => $user));
//        dd($timesheets);
        if (time() >= $privilege->start_time && (time() <= $privilege->end_time || $privilege->end_time == "" || $privilege->end_time == 0)) {
            if ($privilege->quota == "-1") {
                $accessibility["access"] = 1;
                $accessibility["message"] = "success.";
                return $accessibility;
            } else if ($privilege->recursive == "Recursive") {
                if ($privilege->recursive_type == "Daily") {
//                dd(strtotime($d->format('d/m/Y') . ' 00:00:00') . " " . time());
                    $used_privilege = Timesheet::model()->findAllBySql("select * from sp_timesheet where created_by=" . $user . " and created_at BETWEEN " . strtotime(date('Y-m-d') . ' 00:00:00') . " AND " . time());
//                dd($used_privilege);
                    if (count($used_privilege) < $privilege->quota) {
                        $accessibility["access"] = 1;
                        $accessibility["message"] = "success.";
                        return $accessibility;
                    } else {
                        $temp = $cms->getCMSContentBySlug("privilege_limit");
                        $accessibility["access"] = 0;
                        $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                        return $accessibility;
                    }
                } else if ($privilege->recursive_type == "Weekly") {
                    $used_privilege = Timesheet::model()->findAllBySql("select * from sp_timesheet where created_by=" . $user . " and created_at between " . strtotime('last monday' . ' 00:00:00') . " AND " . time());
                    if (count($used_privilege) < $privilege->quota) {
                        $accessibility["access"] = 1;
                        $accessibility["message"] = "success.";
                        return $accessibility;
                    } else {
                        $temp = $cms->getCMSContentBySlug("privilege_limit");
                        $accessibility["access"] = 0;
                        $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                        return $accessibility;
                    }
                } else if ($privilege->recursive_type == "Monthly") {
                    $d = new DateTime('first day of this month');
                    $used_privilege = Timesheet::model()->findAllBySql("select * from sp_timesheet where created_by=" . $user . " and created_at between " . strtotime($d->format('d/m/Y') . ' 00:00:00') . " AND " . time());
                    if (count($used_privilege) < $privilege->quota) {
                        $accessibility["access"] = true;
                        $accessibility["message"] = "success.";
                        return $accessibility;
                    } else
                        $temp = $cms->getCMSContentBySlug("privilege_limit");
                    $accessibility["access"] = 0;
                    $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                    return $accessibility;
                }
            }
            else if ($privilege->recursive == "Non-Recursive") {
                if ($privilege->quota > count($timesheets)) {
                    $accessibility["access"] = 1;
                    $accessibility["message"] = "success.";
                    return $accessibility;
                } else {
                    $temp = $cms->getCMSContentBySlug("privilege_limit");
                    $accessibility["access"] = 0;
                    $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                    return $accessibility;
                }
            }
        } else {
            $temp = $cms->getCMSContentBySlug("privilege_duration");
            $accessibility["access"] = 0;
            $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
            return $accessibility;
        }
    }

    public static function createTwoWayContractOut($privilege) {
        $cms = new Cms();
        $accessibility = array("access" => 0, "message" => "","title" => "");
        $company = AppUser::getUserCompany();
        $user = AppUser::getUserId();
//        dd($user);
        //Sub query
        //(SELECT user_id FROM sp_contract WHERE is_internal=1 AND company_id=".$company->id." AND role!='Director')
        $contracts = Contract::model()->findAllBySql("SELECT * FROM sp_contract WHERE user_id=" . $user . " AND sp_contract.type='out'");
//        dd($contracts);
        if (time() >= $privilege->start_time && (time() <= $privilege->end_time || $privilege->end_time == "" || $privilege->end_time == 0)) {
            if ($privilege->quota == "-1") {
                $accessibility["access"] = 1;
                $accessibility["message"] = "success.";
                return $accessibility;
            } else if ($privilege->recursive == "Recursive") {
                if ($privilege->recursive_type == "Daily") {
//                dd(strtotime($d->format('d/m/Y') . ' 00:00:00') . " " . time());
                    $used_privilege = Contract::model()->findAllBySql("SELECT * FROM sp_contract WHERE user_id=" . $user . " AND sp_contract.type='out' and created_at BETWEEN " . strtotime(date('Y-m-d') . ' 00:00:00') . " AND " . time());
//                dd($used_privilege);
                    if (count($used_privilege) < $privilege->quota) {
                        $accessibility["access"] = 1;
                        $accessibility["message"] = "success.";
                        return $accessibility;
                    } else {
                        $temp = $cms->getCMSContentBySlug("privilege_limit");
                        $accessibility["access"] = 0;
                        $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                        return $accessibility;
                    }
                } else if ($privilege->recursive_type == "Weekly") {
                    $used_privilege = Contract::model()->findAllBySql("SELECT * FROM sp_contract WHERE user_id=" . $user . " AND sp_contract.type='out' and created_at between " . strtotime('last monday' . ' 00:00:00') . " AND " . time());
                    if (count($used_privilege) < $privilege->quota) {
                        $accessibility["access"] = 1;
                        $accessibility["message"] = "success.";
                        return $accessibility;
                    } else {
                        $temp = $cms->getCMSContentBySlug("privilege_limit");
                        $accessibility["access"] = 0;
                        $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                        return $accessibility;
                    }
                } else if ($privilege->recursive_type == "Monthly") {
                    $d = new DateTime('first day of this month');
                    $used_privilege = Contract::model()->findAllBySql("SELECT * FROM sp_contract WHERE user_id=" . $user . " AND sp_contract.type='out' and created_at between " . strtotime($d->format('d/m/Y') . ' 00:00:00') . " AND " . time());
                    if (count($used_privilege) < $privilege->quota) {
                        $accessibility["access"] = true;
                        $accessibility["message"] = "success.";
                        return $accessibility;
                    } else
                        $temp = $cms->getCMSContentBySlug("privilege_limit");
                    $accessibility["access"] = 0;
                    $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                    return $accessibility;
                }
            }
            else if ($privilege->recursive == "Non-Recursive") {
                if ($privilege->quota > count($contracts)) {
                    $accessibility["access"] = 1;
                    $accessibility["message"] = "success.";
                    return $accessibility;
                } else {
                    $temp = $cms->getCMSContentBySlug("privilege_limit");
                    $accessibility["access"] = 0;
                    $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                    return $accessibility;
                }
            }
        } else {
            $temp = $cms->getCMSContentBySlug("privilege_duration");
            $accessibility["access"] = 0;
            $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
            return $accessibility;
        }
    }

    public static function createTwoWayContractIn($privilege) {
        $cms = new Cms();
        $accessibility = array("access" => 0, "message" => "","title" => "");
        $company = AppUser::getUserCompany();
        $user = AppUser::getUserId();
        $contracts = Contract::model()->findAllBySql("select * from sp_contract where (type='out') and company_id=" . $company->id);
        if (time() >= $privilege->start_time && (time() <= $privilege->end_time || $privilege->end_time == "" || $privilege->end_time == 0)) {
            if ($privilege->quota == "-1") {
                $accessibility["access"] = 1;
                $accessibility["message"] = "success.";
                return $accessibility;
            } else if ($privilege->recursive == "Recursive") {
                if ($privilege->recursive_type == "Daily") {
//                dd(strtotime($d->format('d/m/Y') . ' 00:00:00') . " " . time());
                    $used_privilege = Contract::model()->findAllBySql("select * from sp_contract where (type='out') and company_id=" . $company->id . " and created_at BETWEEN " . strtotime(date('Y-m-d') . ' 00:00:00') . " AND " . time());
//                dd($used_privilege);
                    if (count($used_privilege) < $privilege->quota) {
                        $accessibility["access"] = 1;
                        $accessibility["message"] = "success.";
                        return $accessibility;
                    } else {
                        $temp = $cms->getCMSContentBySlug("privilege_limit");
                        $accessibility["access"] = 0;
                        $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                        return $accessibility;
                    }
                } else if ($privilege->recursive_type == "Weekly") {
                    $used_privilege = Contract::model()->findAllBySql("select * from sp_contract where (type='out') and company_id=" . $company->id . " and created_at between " . strtotime('last monday' . ' 00:00:00') . " AND " . time());
                    if (count($used_privilege) < $privilege->quota) {
                        $accessibility["access"] = 1;
                        $accessibility["message"] = "success.";
                        return $accessibility;
                    } else {
                        $temp = $cms->getCMSContentBySlug("privilege_limit");
                        $accessibility["access"] = 0;
                        $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                        return $accessibility;
                    }
                } else if ($privilege->recursive_type == "Monthly") {
                    $d = new DateTime('first day of this month');
                    $used_privilege = Contract::model()->findAllBySql("select * from sp_contract where (type='out') and company_id=" . $company->id . " and created_at between " . strtotime($d->format('d/m/Y') . ' 00:00:00') . " AND " . time());
                    if (count($used_privilege) < $privilege->quota) {
                        $accessibility["access"] = true;
                        $accessibility["message"] = "success.";
                        return $accessibility;
                    } else
                        $temp = $cms->getCMSContentBySlug("privilege_limit");
                    $accessibility["access"] = 0;
                    $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                    return $accessibility;
                }
            }
            else if ($privilege->recursive == "Non-Recursive") {
                if ($privilege->quota > count($contracts)) {
                    $accessibility["access"] = 1;
                    $accessibility["message"] = "success.";
                    return $accessibility;
                } else {
                    $temp = $cms->getCMSContentBySlug("privilege_limit");
                    $accessibility["access"] = 0;
                    $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                    return $accessibility;
                }
            }
        } else {
            $temp = $cms->getCMSContentBySlug("privilege_duration");
            $accessibility["access"] = 0;
            $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
            return $accessibility;
        }
    }

    public static function createThreeWayContract($privilege) {
        $cms = new Cms();
        $accessibility = array("access" => 0, "message" => "","title" => "");
        $company = AppUser::getUserCompany();
        $user = AppUser::getUserId();
        $contracts = Contract::model()->findAllBySql("SELECT * FROM sp_contract WHERE user_id IN (SELECT user_id FROM sp_contract WHERE sp_contract.type='in_out' AND company_id=" . $company->id . ") AND sp_contract.type='out_b'");
        if (time() >= $privilege->start_time && (time() <= $privilege->end_time || $privilege->end_time == "" || $privilege->end_time == 0)) {
            if ($privilege->quota == "-1") {
                $accessibility["access"] = 1;
                $accessibility["message"] = "success.";
                return $accessibility;
            } else if ($privilege->recursive == "Recursive") {
                if ($privilege->recursive_type == "Daily") {
//                dd(strtotime($d->format('d/m/Y') . ' 00:00:00') . " " . time());
                    $used_privilege = Contract::model()->findAllBySql("SELECT * FROM sp_contract WHERE user_id IN (SELECT user_id FROM sp_contract WHERE sp_contract.type='in_out' AND company_id=" . $company->id . ") AND sp_contract.type='out_b' and created_at BETWEEN " . strtotime(date('Y-m-d') . ' 00:00:00') . " AND " . time());
//                dd($used_privilege);
                    if (count($used_privilege) < $privilege->quota) {
                        $accessibility["access"] = 1;
                        $accessibility["message"] = "success.";
                        return $accessibility;
                    } else {
                        $temp = $cms->getCMSContentBySlug("privilege_limit");
                        $accessibility["access"] = 0;
                        $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                        return $accessibility;
                    }
                } else if ($privilege->recursive_type == "Weekly") {
                    $used_privilege = Contract::model()->findAllBySql("SELECT * FROM sp_contract WHERE user_id IN (SELECT user_id FROM sp_contract WHERE sp_contract.type='in_out' AND company_id=" . $company->id . ") AND sp_contract.type='out_b' and created_at between " . strtotime('last monday' . ' 00:00:00') . " AND " . time());
                    if (count($used_privilege) < $privilege->quota) {
                        $accessibility["access"] = 1;
                        $accessibility["message"] = "success.";
                        return $accessibility;
                    } else {
                        $temp = $cms->getCMSContentBySlug("privilege_limit");
                        $accessibility["access"] = 0;
                        $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                        return $accessibility;
                    }
                } else if ($privilege->recursive_type == "Monthly") {
                    $d = new DateTime('first day of this month');
                    $used_privilege = Contract::model()->findAllBySql("SELECT * FROM sp_contract WHERE user_id IN (SELECT user_id FROM sp_contract WHERE sp_contract.type='in_out' AND company_id=" . $company->id . ") AND sp_contract.type='out_b' and created_at between " . strtotime($d->format('d/m/Y') . ' 00:00:00') . " AND " . time());
                    if (count($used_privilege) < $privilege->quota) {
                        $accessibility["access"] = true;
                        $accessibility["message"] = "success.";
                        return $accessibility;
                    } else
                        $temp = $cms->getCMSContentBySlug("privilege_limit");
                    $accessibility["access"] = 0;
                    $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                    return $accessibility;
                }
            }
            else if ($privilege->recursive == "Non-Recursive") {
                if ($privilege->quota > count($contracts)) {
                    $accessibility["access"] = 1;
                    $accessibility["message"] = "success.";
                    return $accessibility;
                } else {
                    $temp = $cms->getCMSContentBySlug("privilege_limit");
                    $accessibility["access"] = 0;
                    $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                    return $accessibility;
                }
            }
        } else {
            $temp = $cms->getCMSContentBySlug("privilege_duration");
            $accessibility["access"] = 0;
            $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
            return $accessibility;
        }
    }

    public static function createNonBillableTimesheets($privilege) {
        $cms = new Cms();
        $accessibility = array("access" => 0, "message" => "","title" => "");
        $company = AppUser::getUserCompany();
        $user = AppUser::getUserId();
        $contracts = Contract::model()->findAllBySql("select * from sp_contract where (user_id = " . $user . ") AND (is_internal=1)");
        if (time() >= $privilege->start_time && (time() <= $privilege->end_time || $privilege->end_time == "" || $privilege->end_time == 0)) {
            if ($privilege->quota == "-1") {
                $accessibility["access"] = 1;
                $accessibility["message"] = "success.";
                return $accessibility;
            } else if ($privilege->recursive == "Recursive") {
                if ($privilege->recursive_type == "Daily") {
//                dd(strtotime($d->format('d/m/Y') . ' 00:00:00') . " " . time());
                    $used_privilege = Contract::model()->findAllBySql("select * from sp_contract where (user_id = " . $user . ") AND (is_internal=1) and created_at BETWEEN " . strtotime(date('Y-m-d') . ' 00:00:00') . " AND " . time());
//                dd($used_privilege);
                    if (count($used_privilege) < $privilege->quota) {
                        $accessibility["access"] = 1;
                        $accessibility["message"] = "success.";
                        return $accessibility;
                    } else {
                        $temp = $cms->getCMSContentBySlug("privilege_limit");
                        $accessibility["access"] = 0;
                        $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                        return $accessibility;
                    }
                } else if ($privilege->recursive_type == "Weekly") {
                    $used_privilege = Contract::model()->findAllBySql("select * from sp_contract where (user_id = " . $user . ") AND (is_internal=1) and created_at between " . strtotime('last monday' . ' 00:00:00') . " AND " . time());
                    if (count($used_privilege) < $privilege->quota) {
                        $accessibility["access"] = 1;
                        $accessibility["message"] = "success.";
                        return $accessibility;
                    } else {
                        $temp = $cms->getCMSContentBySlug("privilege_limit");
                        $accessibility["access"] = 0;
                        $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                        return $accessibility;
                    }
                } else if ($privilege->recursive_type == "Monthly") {
                    $d = new DateTime('first day of this month');
                    $used_privilege = Contract::model()->findAllBySql("select * from sp_contract where (user_id = " . $user . ") AND (is_internal=1) and created_at between " . strtotime($d->format('d/m/Y') . ' 00:00:00') . " AND " . time());
                    if (count($used_privilege) < $privilege->quota) {
                        $accessibility["access"] = true;
                        $accessibility["message"] = "success.";
                        return $accessibility;
                    } else
                        $temp = $cms->getCMSContentBySlug("privilege_limit");
                    $accessibility["access"] = 0;
                    $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                    return $accessibility;
                }
            }
            else if ($privilege->recursive == "Non-Recursive") {
                if ($privilege->quota > count($contracts)) {
                    $accessibility["access"] = 1;
                    $accessibility["message"] = "success.";
                    return $accessibility;
                } else {
                    $temp = $cms->getCMSContentBySlug("privilege_limit");
                    $accessibility["access"] = 0;
                    $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                    return $accessibility;
                }
            }
        } else {
            $temp = $cms->getCMSContentBySlug("privilege_duration");
            $accessibility["access"] = 0;
            $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
            return $accessibility;
        }
    }

    public static function createEmployee($privilege) {
        $cms = new Cms();
        $accessibility = array("access" => 0, "message" => "","title" => "");
        $user = AppUser::getUserId();
        $company = AppUser::getUserCompany();
        if ($company != null)
            $users = AppCompany::getCompanyEmployee($company->id);
        if (time() >= $privilege->start_time && (time() <= $privilege->end_time || $privilege->end_time == "" || $privilege->end_time == 0)) {
            if ($privilege->quota == "-1") {
                $accessibility["access"] = 1;
                $accessibility["message"] = "success.";
                return $accessibility;
            } else if ($privilege->recursive == "Recursive") {
                if ($privilege->recursive_type == "Daily") {
//                dd(strtotime($d->format('d/m/Y') . ' 00:00:00') . " " . time());
                    $used_privilege = User::model()->findAllBySql("select * from sp_user t INNER JOIN sp_contract sc on sc.user_id=t.id where sc.company_id=" . $company->id . " and t.created_at BETWEEN " . strtotime(date('Y-m-d') . ' 00:00:00') . " AND " . time());
//                dd($used_privilege);
                    if (count($used_privilege) < $privilege->quota) {
                        $accessibility["access"] = 1;
                        $accessibility["message"] = "success.";
                        return $accessibility;
                    } else {
                        $temp = $cms->getCMSContentBySlug("privilege_limit");
                        $accessibility["access"] = 0;
                        $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                        return $accessibility;
                    }
                } else if ($privilege->recursive_type == "Weekly") {
                    $used_privilege = User::model()->findAllBySql("select * from sp_user t INNER JOIN sp_contract sc on sc.user_id=t.id where sc.company_id=" . $company->id . " and created_at between " . strtotime('last monday' . ' 00:00:00') . " AND " . time());
                    if (count($used_privilege) < $privilege->quota) {
                        $accessibility["access"] = 1;
                        $accessibility["message"] = "success.";
                        return $accessibility;
                    } else {
                        $temp = $cms->getCMSContentBySlug("privilege_limit");
                        $accessibility["access"] = 0;
                        $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                        return $accessibility;
                    }
                } else if ($privilege->recursive_type == "Monthly") {
                    $d = new DateTime('first day of this month');
                    $used_privilege = User::model()->findAllBySql("select * from sp_user t INNER JOIN sp_contract sc on sc.user_id=t.id where sc.company_id=" . $company->id . " and created_at between " . strtotime($d->format('d/m/Y') . ' 00:00:00') . " AND " . time());
                    if (count($used_privilege) < $privilege->quota) {
                        $accessibility["access"] = true;
                        $accessibility["message"] = "success.";
                        return $accessibility;
                    } else
                        $temp = $cms->getCMSContentBySlug("privilege_limit");
                    $accessibility["access"] = 0;
                    $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                    return $accessibility;
                }
            }
            else if ($privilege->recursive == "Non-Recursive") {
                if ($privilege->quota > count($users)) {
                    $accessibility["access"] = 1;
                    $accessibility["message"] = "success.";
                    return $accessibility;
                } else {
                    $temp = $cms->getCMSContentBySlug("privilege_limit");
                    $accessibility["access"] = 0;
                    $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                    return $accessibility;
                }
            }
        } else {
            $temp = $cms->getCMSContentBySlug("privilege_duration");
            $accessibility["access"] = 0;
            $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
            return $accessibility;
        }
    }

    public static function createMultipleEmployees($privilege) {
        return self::createEmployee($privilege);
    }
    
    public static function createShift($privilege) {
        $cms = new Cms();
        $accessibility = array("access" => 0, "message" => "","title" => "");
        $user = AppUser::getUserId();
        $company = AppUser::getUserCompany();
        if ($company != null)
            $shifts = CompanyShifts::model()->findAllByAttributes (array("company_id"=>$company->id));
        if (time() >= $privilege->start_time && (time() <= $privilege->end_time || $privilege->end_time == "" || $privilege->end_time == 0)) {
            if ($privilege->quota == "-1") {
                $accessibility["access"] = 1;
                $accessibility["message"] = "success.";
                return $accessibility;
            } else if ($privilege->recursive == "Recursive") {
                if ($privilege->recursive_type == "Daily") {
//                dd(strtotime($d->format('d/m/Y') . ' 00:00:00') . " " . time());
                    $used_privilege = User::model()->findAllBySql("select * from sp_company_shifts where company_id=" . $company->id . " and created_at BETWEEN " . strtotime(date('Y-m-d') . ' 00:00:00') . " AND " . time());
//                dd($used_privilege);
                    if (count($used_privilege) < $privilege->quota) {
                        $accessibility["access"] = 1;
                        $accessibility["message"] = "success.";
                        return $accessibility;
                    } else {
                        $temp = $cms->getCMSContentBySlug("privilege_limit");
                        $accessibility["access"] = 0;
                        $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                        return $accessibility;
                    }
                } else if ($privilege->recursive_type == "Weekly") {
                    $used_privilege = User::model()->findAllBySql("select * from sp_company_shifts where company_id=" . $company->id . " and created_at between " . strtotime('last monday' . ' 00:00:00') . " AND " . time());
                    if (count($used_privilege) < $privilege->quota) {
                        $accessibility["access"] = 1;
                        $accessibility["message"] = "success.";
                        return $accessibility;
                    } else {
                        $temp = $cms->getCMSContentBySlug("privilege_limit");
                        $accessibility["access"] = 0;
                        $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                        return $accessibility;
                    }
                } else if ($privilege->recursive_type == "Monthly") {
                    $d = new DateTime('first day of this month');
                    $used_privilege = User::model()->findAllBySql("select * from sp_company_shifts where company_id=" . $company->id . " and created_at between " . strtotime($d->format('d/m/Y') . ' 00:00:00') . " AND " . time());
                    if (count($used_privilege) < $privilege->quota) {
                        $accessibility["access"] = true;
                        $accessibility["message"] = "success.";
                        return $accessibility;
                    } else
                        $temp = $cms->getCMSContentBySlug("privilege_limit");
                    $accessibility["access"] = 0;
                    $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                    return $accessibility;
                }
            }
            else if ($privilege->recursive == "Non-Recursive") {
                if ($privilege->quota > count($shifts)) {
                    $accessibility["access"] = 1;
                    $accessibility["message"] = "success.";
                    return $accessibility;
                } else {
                    $temp = $cms->getCMSContentBySlug("privilege_limit");
                    $accessibility["access"] = 0;
                    $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                    return $accessibility;
                }
            }
        } else {
            $temp = $cms->getCMSContentBySlug("privilege_duration");
            $accessibility["access"] = 0;
            $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
            return $accessibility;
        }
    }

    public static function createNewInvoice($privilege) {
        $cms = new Cms();
        $accessibility = array("access" => 0, "message" => "","title" => "");
        $user = AppUser::getUserId();
        $company = AppUser::getUserCompany();
        $invoices = Invoice::model()->findAllByAttributes(array("created_by" => $user, "company_id" => $company->id));
        if (time() >= $privilege->start_time && (time() <= $privilege->end_time || $privilege->end_time == "" || $privilege->end_time == 0)) {
            if ($privilege->quota == "-1") {
                $accessibility["access"] = 1;
                $accessibility["message"] = "success.";
                return $accessibility;
            } else if ($privilege->recursive == "Recursive") {
                if ($privilege->recursive_type == "Daily") {
//                dd(strtotime($d->format('d/m/Y') . ' 00:00:00') . " " . time());
                    $used_privilege = Invoice::model()->findAllBySql("select * from sp_invoice where company_id=" . $company->id . " and created_by=" . $user . " and created_at BETWEEN " . strtotime(date('Y-m-d') . ' 00:00:00') . " AND " . time());
//                dd($used_privilege);
                    if (count($used_privilege) < $privilege->quota) {
                        $accessibility["access"] = 1;
                        $accessibility["message"] = "success.";
                        return $accessibility;
                    } else {
                        $temp = $cms->getCMSContentBySlug("privilege_limit");
                        $accessibility["access"] = 0;
                        $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                        return $accessibility;
                    }
                } else if ($privilege->recursive_type == "Weekly") {
                    $used_privilege = Invoice::model()->findAllBySql("select * from sp_invoice where company_id=" . $company->id . " and created_by=" . $user . " and created_at between " . strtotime('last monday' . ' 00:00:00') . " AND " . time());
                    if (count($used_privilege) < $privilege->quota) {
                        $accessibility["access"] = 1;
                        $accessibility["message"] = "success.";
                        return $accessibility;
                    } else {
                        $temp = $cms->getCMSContentBySlug("privilege_limit");
                        $accessibility["access"] = 0;
                        $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                        return $accessibility;
                    }
                } else if ($privilege->recursive_type == "Monthly") {
                    $d = new DateTime('first day of this month');
                    $used_privilege = Invoice::model()->findAllBySql("select * from sp_invoice where company_id=" . $company->id . " and created_by=" . $user . " and created_at between " . strtotime($d->format('d/m/Y') . ' 00:00:00') . " AND " . time());
                    if (count($used_privilege) < $privilege->quota) {
                        $accessibility["access"] = true;
                        $accessibility["message"] = "success.";
                        return $accessibility;
                    } else
                        $temp = $cms->getCMSContentBySlug("privilege_limit");
                    $accessibility["access"] = 0;
                    $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                    return $accessibility;
                }
            }
            else if ($privilege->recursive == "Non-Recursive") {
                if ($privilege->quota > count($invoices)) {
                    $accessibility["access"] = 1;
                    $accessibility["message"] = "success.";
                    return $accessibility;
                } else {
                    $temp = $cms->getCMSContentBySlug("privilege_limit");
            $accessibility["access"] = 0;
            $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
            return $accessibility;
                }
            }
        } else {
            $temp = $cms->getCMSContentBySlug("privilege_duration");
            $accessibility["access"] = 0;
            $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
            return $accessibility;
        }
    }

    public static function createContact($privilege) {
        $cms = new Cms();
        $company_id = AppUser::getUserCompany()->id;
        $accessibility = array("access" => 0, "message" => "","title" => "");
        $contacts = Contact::model()->findAllByAttributes(array("company_id" => $company_id));
        if (time() >= $privilege->start_time && (time() <= $privilege->end_time || $privilege->end_time == "" || $privilege->end_time == 0)) {
            if ($privilege->quota == "-1") {
                $accessibility["access"] = 1;
                $accessibility["message"] = "success.";
                return $accessibility;
            } else if ($privilege->recursive == "Recursive") {
                if ($privilege->recursive_type == "Daily") {

//                dd(strtotime($d->format('d/m/Y') . ' 00:00:00') . " " . time());
                    $used_privilege = Contact::model()->findAllBySql("select * from sp_contact where company_id=" . $company_id . " and created_at BETWEEN " . strtotime(date('Y-m-d') . ' 00:00:00') . " AND " . time());
//                dd($used_privilege);
                    if (count($used_privilege) < $privilege->quota) {
                        $accessibility["access"] = 1;
                        $accessibility["message"] = "success.";
                        return $accessibility;
                    } else {
                        $temp = $cms->getCMSContentBySlug("privilege_limit");
                        $accessibility["access"] = 0;
                        $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                        return $accessibility;
                    }
                } else if ($privilege->recursive_type == "Weekly") {
                    $used_privilege = Contact::model()->findAllBySql("select * from sp_contact where company_id=" . $company_id . " and created_at between " . strtotime('last monday' . ' 00:00:00') . " AND " . time());
                    if (count($used_privilege) < $privilege->quota) {
                        $accessibility["access"] = 1;
                        $accessibility["message"] = "success.";
                        return $accessibility;
                    } else {
                        $temp = $cms->getCMSContentBySlug("privilege_limit");
                        $accessibility["access"] = 0;
                        $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                        return $accessibility;
                    }
                } else if ($privilege->recursive_type == "Monthly") {
                    $d = new DateTime('first day of this month');
                    $used_privilege = Contact::model()->findAllBySql("select * from sp_contact where company_id=" . $company_id . " and created_at between " . strtotime($d->format('d/m/Y') . ' 00:00:00') . " AND " . time());
                    if (count($used_privilege) < $privilege->quota) {
                        $accessibility["access"] = true;
                        $accessibility["message"] = "success.";
                        return $accessibility;
                    } else
                        $temp = $cms->getCMSContentBySlug("privilege_limit");
                    $accessibility["access"] = 0;
                    $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                    return $accessibility;
                }
            }
            else if ($privilege->recursive == "Non-Recursive") {
                if ($privilege->quota > count($contacts)) {
                    $accessibility["access"] = 1;
                    $accessibility["message"] = "success.";
                    return $accessibility;
                } else {
                    $temp = $cms->getCMSContentBySlug("privilege_limit");
                    $accessibility["access"] = 0;
                    $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
                    return $accessibility;
                }
            }
        } else {
            $temp = $cms->getCMSContentBySlug("privilege_duration");
            $accessibility["access"] = 0;
            $accessibility["message"] = $temp['content'];
                        $accessibility["title"] = $temp['title'];
            return $accessibility;
        }
    }

}
