<?php

class PriveledgeController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
                //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'admin', 'delete', 'update', 'loadprivileges', 'assignPrivilege', 'removePrivilege', 'companyPrivilege'),
                'roles' => array('super_admin'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionCompanyPrivilege() {
        $model = new CompanyPriveledge();
//        $company_privilege = Yii::app()->db->createCommand("SELECT P.*,C.* FROM sp_priveledge P LEFT JOIN sp_company_priveledge C ON P.id=C.priveledge_id WHERE (company_id=14281294 OR company_id IS NULL) AND P.status=1 ORDER BY P.name");
//        dd($privilege);
        $_groups = PrivilegeGroups::model()->findAll();
        foreach ($_groups as $group) {
            $groups[$group->group] = $group->group;
        }
        if ((isset($_POST['group']) || isset($_POST['title']) || isset($_POST['start_date']) || isset($_POST['end_date']))) {
            $criteria = new CDbCriteria();
//            $criteria->select = "*";
//            $criteria->join = "left join sp_company_priveledge on t.id = sp_company_priveledge.priveledge_id";
//dd('here');
            $criteria->select = "t.id As p_id,t.name,sp_company_priveledge.*";
            $query = "t.status=1";
            $condition = "(spCompanyPriveledges.company_id=" . $_POST['company_id'] . " OR spCompanyPriveledges.company_id IS NULL)";
            if ($_POST['group'] != "") {
                $query .= " and t.group='" . $_POST['group'] . "'";
            }
            if ($_POST['title'] != "") {
                $query .= " and t.name like '%" . $_POST['title'] . "%'";
            }
            if ($_POST['start_date'] != "") {
                $start_date = DateTime::createFromFormat('d/m/Y H:i:s', $_POST['start_date'] . ' 00:00:00');
                $query .= " and sp_company_priveledge.start_time >=" . $start_date->getTimestamp();
            }
            if ($_POST['end_date'] != "") {
                $end_date = DateTime::createFromFormat('d/m/Y H:i:s', $_POST['end_date'] . ' 23:59:59');
                $query .= " and sp_company_priveledge.end_time<=" . $end_date->getTimestamp();
            }
//            $criteria->join = "left join sp_company_priveledge on t.id=sp_company_priveledge.priveledge_id";
            $criteria->condition = $query;
            $criteria->with = array('spCompanyPriveledges' => array('joinType' => 'LEFT JOIN', 'condition' => $condition));
//            $criteria->condition = $query;
            $criteria->order = "t.name";
//            dd($criteria);
            $pages = new CPagination(Priveledge::model()->count($criteria));

            // results per page
            $pages->pageSize = 10;
            $pages->applyLimit($criteria);
            $company_privilege = Priveledge::model()->findAll($criteria);
//            dd($company_privilege);
        } else if (isset($_POST['company_id'])) {
            //$query = "SELECT P.id AS p_id,P.name,C.* FROM sp_priveledge P LEFT JOIN sp_company_priveledge C ON P.id=C.priveledge_id WHERE (C.company_id=".$_POST['company_id']." OR C.company_id IS NULL) AND P.status=1 ORDER BY P.name";
//dd('there');
            $criteria = new CDbCriteria();
            $criteria->select = "t.id AS p_id,t.name,sp_company_priveledge.*";
            $criteria->with = array('spCompanyPriveledges' => array('joinType' => 'LEFT JOIN', 'condition' => "(spCompanyPriveledges.company_id=" . $_POST['company_id'] . " OR spCompanyPriveledges.company_id IS NULL) AND t.status=1"));
//            $criteria->join = "left join sp_company_priveledge p on p.priveledge_id = t.id";
//            $criteria->condition = "(spCompanyPriveledges.company_id=".$_POST['company_id']." OR spCompanyPriveledges.company_id IS NULL) AND t.status=1";
            $criteria->order = "t.name";
            $pages = new CPagination(Priveledge::model()->count($criteria));
//            $criteria->order = "t.name asc";
            // results per page
            $pages->pageSize = 10;
            $pages->applyLimit($criteria);
//            $company_privilege = Yii::app()->db->createCommand($query)->queryAll();
            $company_privilege = Priveledge::model()->findAll($criteria);
//        dd($company_privilege[10]->spCompanyPriveledges);
        }
        echo $this->renderPartial('_assign', array('model' => $model, 'pages' => $pages, 'company_privilege' => $company_privilege, 'groups' => $groups));
    }

    public function actionRemovePrivilege() {
        $model = CompanyPriveledge::model()->findByPk($_POST['cp_id']);
        if ($model->delete()) {
            echo "Company privilege has been removed successfully.";
//            if (isset($_POST['company_id'])) {
//            $company_privilege = CompanyPriveledge::model()->findAllByAttributes(array('company_id' => $_POST['company_id']));
//            if (!isset($_POST['privilege_id']))
//                echo $this->renderPartial('_assign', array('company_privilege' => $company_privilege));
//        }
        }
    }

    public function actionLoadPrivileges() {
        $company_id = $_POST["company_id"];

        $model = Yii::app()->db->createCommand("SELECT t.id AS group_id,t.group,p.*,l.id AS pp_id,l.priveledge_id,l.company_id,l.quota,l.start_time,l.end_time,l.recursive,l.recursive_type,l.status,l.created_at,l.modified_at FROM sp_privilege_groups t LEFT JOIN sp_priveledge p ON t.group = p.group LEFT JOIN sp_company_priveledge l ON ( p.id=l.priveledge_id AND ( l.company_id IS NULL OR l.company_id=" . $company_id . " ) AND l.status='active' ) ORDER BY t.group,p.name")->queryAll();

        echo $this->renderPartial('companyprivileges', array("model" => $model));
    }

    public function actionAssignPrivilege() {
        $model = new CompanyPriveledge();
        $all_companies = Company::model()->findAllBySql("select * from sp_company where status=1 order by name");
        $companies = array('' => 'Please select a company');
        foreach ($all_companies as $value) {
            $admin = AppCompany::getCompanyAdmin($value->id);
            $companies[$value->id] = $value->name . " ----------------------------- (" . $admin["email"] . ")";
        }
        $_groups = PrivilegeGroups::model()->findAllBySql("select * from sp_privilege_groups order by sp_privilege_groups.group");
        foreach ($_groups as $group) {
            $groups[$group->group] = $group->group;
        }
        $privilege = Priveledge::model()->findAllBySql("select * from sp_priveledge where status=1 order by name");
        if (isset($_POST['company_id'])) {
            $company_privilege = CompanyPriveledge::model()->findAllByAttributes(array('company_id' => $_POST['company_id']));
            if (!isset($_POST['privilege_id']))
                echo $this->renderPartial('_assign', array('company_privilege' => $company_privilege, 'groups' => $groups));
        }
        else {
            $company_privilege = null;
        }
        if ($_POST) {
            PrivilegeComponent::setCompanyPrivileges($_POST);
            Yii::app()->user->setFlash("success", "Company prilileges saved successfully");
        }

        $this->render('assign', array(
            'model' => $model,
            'companies' => $companies,
            'privileges' => $privilege,
            'company_privilege' => $company_privilege,
            'groups' => $groups,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Priveledge();
        $actions = array('' => 'Please select an action');
        $functions = array('' => 'Please select a function');

        $_groups = PrivilegeGroups::model()->findAll();
        $groups = array('' => 'All groups');
        foreach ($_groups as $group) {
            $groups[$group->group] = $group->group;
        }
        $component = new PrivilegeComponent();
        //To get all methods of the component
        $reflection = new ReflectionClass($component);
        $methods = $reflection->getMethods();
        foreach ($methods as $key => $value) {
            $functions[$value->name] = $value->name;
        }
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        $actions_data = FunctionList::model()->findAll();
        foreach ($actions_data as $key => $value) {
            $actions[$value->id] = $value->title;
        }
        if (isset($_POST['Priveledge'])) {
            $model->attributes = $_POST['Priveledge'];
            $model->id = AppInterface::getUniqueId();
            $model->created_at = time();
            $model->created_by = AppUser::getUserId();
            $model->modified_at = time();
            $model->modified_by = AppUser::getUserId();
            $model->status = 1;
            if ($model->save()) {
                Yii::app()->user->setFlash('success', "New Privilege has been created successfully");
                $this->redirect(array('index'));
            }
        }

        $this->render('create', array(
            'model' => $model,
            'groups' => $groups,
            'actions' => $actions,
            'functions' => $functions,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        $actions = array('' => 'Please select an action');
        $functions = array('' => 'Please select a function');
        $_groups = PrivilegeGroups::model()->findAll();
        $groups = array('' => 'Please select a group');
        foreach ($_groups as $group) {
            $groups[$group->group] = $group->group;
        }
        $component = new PrivilegeComponent();
        //To get all methods of the component
        $reflection = new ReflectionClass($component);
        $methods = $reflection->getMethods();
        foreach ($methods as $key => $value) {
            $functions[$value->name] = $value->name;
        }
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        $actions_data = FunctionList::model()->findAll();
        foreach ($actions_data as $key => $value) {
            $actions[$value->id] = $value->title;
        }
        if (isset($_POST['Priveledge'])) {
            $model->attributes = $_POST['Priveledge'];
            $model->created_at = $model->created_at;
            $model->created_by = $model->created_by;
            $model->status = $model->status;
            $model->modified_at = time();
            $model->modified_by = AppUser::getUserId();
            if ($model->save()) {
                Yii::app()->user->setFlash('success', "Privilege has been updated successfully");
                $this->redirect(array('index'));
            }
        }

        $this->render('update', array(
            'model' => $model,
            'functions' => $functions,
            'actions' => $actions,
            'groups' => $groups,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $model = $this->loadModel($id);
        $model->status = 0;
        $model->save();
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            Yii::app()->user->setFlash('success', "Privilege has been deleted successfully");
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = Priveledge::model()->findAllByAttributes(array('status' => 1));
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Priveledge('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Priveledge']))
            $model->attributes = $_GET['Priveledge'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Priveledge the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Priveledge::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Priveledge $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'priveledge-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
