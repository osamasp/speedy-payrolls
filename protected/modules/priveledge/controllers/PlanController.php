<?php

class PlanController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'admin', 'getcompanyplan', 'editcompanyplan', 'addprivilege', 'editprivileges', 'loadprivileges', 'delete'),
                'roles' => array('super_admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Plan;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Plan'])) {
            $model->attributes = $_POST['Plan'];
            $model->id = AppInterface::getUniqueId();
            $model->created_at = time();
            $model->modified_at = time();
            $model->created_by = AppUser::getUserId();
            $model->modified_by = AppUser::getUserId();
            if ($model->save())
                $this->redirect($this->createAbsoluteUrl('addprivilege', array("plan" => $model->id)));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Plan'])) {
            $model->attributes = $_POST['Plan'];
            $model->modified_at = time();
            $model->modified_by = AppUser::getUserId();
            if ($model->save())
                $this->redirect(array('index'));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionAddPrivilege() {
        $created_plan = "";
        if (isset($_GET["plan"]))
            $created_plan = $_GET["plan"];
        $groups = PrivilegeGroups::model()->findAll();
        $model = new Plan();
        $p_model = new PlanPriveledges();
        $_plans = Plan::model()->findAll();
        $plans = array('' => 'Please select a plan');
        foreach ($_plans as $item) {
            $plans[$item->id] = $item->plan_type;
        }
        
        if (isset($_POST) && count($_POST) > 0) {
            if ($_POST["status"]) {
                foreach ($_POST["privilege"] as $key => $item) {
                    $already_save = PlanPriveledges::model()->findByAttributes(array("priveledge_id" => $key, "plan_id" => $_POST["Plan"]["plan_type"]));
                    
                    if ($already_save==null) {
                        $quota = $_POST["quota"][$key];
                        $type = $_POST["type"][$key];
                        $plan_priv = new PlanPriveledges();
                        $plan_priv->id = AppInterface::getUniqueId();
                        $plan_priv->plan_id = $_POST["Plan"]["plan_type"];
                        $plan_priv->quota = $quota;
                        $plan_priv->type = $type;
                        $plan_priv->priveledge_id = $key;
                        $plan_priv->created_at = time();
                        $plan_priv->modified_at = time();
                        $plan_priv->created_by = AppUser::getUserId();
                        $plan_priv->modified_by = AppUser::getUserId();
                        
                        $plan_priv->insert();
                    } else {
                        $quota = $_POST["quota"][$key];
                        $type = $_POST["type"][$key];
                        $plan_priv = $already_save;
                        $plan_priv->quota = $quota;
                        $plan_priv->type = $type;
                        $plan_priv->modified_at = time();
                        $plan_priv->modified_by = AppUser::getUserId();
                        $plan_priv->update();
                    }
                }
                if (isset($_POST["saved_privileges"])) {
                    foreach ($_POST["saved_privileges"] as $item) {
                        if (!in_array($item, array_keys($_POST["privilege"]))) {
                            $privilege = PlanPriveledges::model()->findByAttributes(array("plan_id" => $_POST["Plan"]["plan_type"], "priveledge_id" => $item));
                            $privilege->delete();
                        }
                    }
                }
                $this->redirect('addprivilege');
            }
        }
        $this->render('addprivilege', array(
            'groups' => $groups,
            'model' => $model,
            'plans' => $plans,
            'p_model' => $p_model,
            'created_plan' => $created_plan,
        ));
    }

    public function actionEditCompanyPlan() {

        $error = "";
        $all_companies = Company::model()->findAllBySql("select * from sp_company where status=1 order by name");
        $all_plans = Plan::model()->findAll();
        $companies = array('' => 'Please select a company');
        $plans = array('' => 'Please select a plan');
        foreach ($all_companies as $value) {
            $admin = AppCompany::getCompanyAdmin($value->id);
            $companies[$value->id] = $value->name . " ----------------------------- (" . $admin["email"] . ")";
        }
        foreach ($all_plans as $item) {
            $plans[$item->id] = $item->plan_type;
        }
        if ($_POST) {
            if ($_POST["plan_id"] == "" || $_POST["company_id"] == "") {
                $error = "Please fill in all fields";
                $this->render('editprivilege', array("companies" => $companies, "plans" => $plans, "error" => $error));
                return;
            } else {
                $company = Company::model()->findByPk($_POST["company_id"]);
                $plan = Plan::model()->findByPk($_POST["plan_id"]);
                $company_privileges = CompanyPriveledge::model()->findAllByAttributes(array("company_id" => $_POST["company_id"]));
                foreach ($company_privileges as $item) {
                    $item->status = "in-active";
                    $item->modified_at = time();
                    $item->modified_by = AppUser::getUserId();
                    $item->save();
                }
                $user = AppCompany::getCompanyAdmin($company->id);
                if($user!=null){ 
                    $user->plan_type = $plan->id;
                    $user->modified_at = time();
                    $user->modified_by = AppUser::getUserId();
                    $user->update();
                }
                //            setting company priviledges
                $set_company_priviledges = AppCompany::setCompanyPlan($company->id, $plan->id);
                AppLogging::addLog($plan->plan_type . " has been assigned to the " . $company->name . " company.", 'success', 'application.priveledge.controller.plan');
                Yii::app()->user->setFlash('success', 'Plan successfully assigned to the selected company.');
                $this->redirect('editcompanyplan');
            }
        }
        $this->render('editprivilege', array("companies" => $companies, "plans" => $plans));
    }

    public function actionLoadPrivileges() {
        $plan_id = $_POST["plan_id"];
        $_POST = array();
        $model = Yii::app()->db->createCommand("SELECT t.id AS group_id,t.group,p.*,l.id AS pp_id,l.plan_id,l.priveledge_id,l.quota,l.type FROM sp_privilege_groups t LEFT JOIN sp_priveledge p ON t.group = p.group LEFT JOIN sp_plan_priveledges l ON p.id = l.priveledge_id AND (l.plan_id=" . $plan_id . " OR l.plan_id IS NULL) ORDER BY t.group,p.name")->queryAll();
//      dd($model);
        echo $this->renderPartial('privileges', array('model' => $model));
    }
    
    public function actionGetCompanyPlan()
    {
//        $this->layout = "//layouts/blank";
        $company_id = $_POST["company_id"];
        $user = AppCompany::getCompanyAdmin($company_id);
        if($user)
            echo $user->plan_type;
        else
            echo 0;
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = Plan::model()->findAll();
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Plan('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Plan']))
            $model->attributes = $_GET['Plan'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Plan the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Plan::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Plan $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'plan-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
