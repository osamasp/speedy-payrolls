<?php

/**
 * This is the model class for table "sp_plan_priveledges".
 *
 * The followings are the available columns in table 'sp_plan_priveledges':
 * @property string $id
 * @property string $plan_id
 * @property string $priveledge_id
 * @property string $quota
 * @property string $type
 * @property string $created_at
 * @property string $modified_at
 * @property string $created_by
 * @property string $modified_by
 *
 * The followings are the available model relations:
 * @property Plan $plan
 * @property Priveledge $priveledge
 * @property User $createdBy
 * @property User $modifiedBy
 */
class PlanPriveledges extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sp_plan_priveledges';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, quota', 'required'),
			array('id, plan_id, priveledge_id, created_at, modified_at, created_by, modified_by', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, plan_id, priveledge_id, quota, type, created_at, modified_at, created_by, modified_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'plan' => array(self::BELONGS_TO, 'Plan', 'plan_id'),
			'priveledge' => array(self::BELONGS_TO, 'Priveledge', 'priveledge_id'),
			'createdBy' => array(self::BELONGS_TO, 'User', 'created_by'),
			'modifiedBy' => array(self::BELONGS_TO, 'User', 'modified_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'plan_id' => 'Plan',
			'priveledge_id' => 'Priveledge',
			'created_at' => 'Created At',
                        'quota' => 'Quota',
                        'type' => 'Type',
			'modified_at' => 'Modified At',
			'created_by' => 'Created By',
			'modified_by' => 'Modified By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('plan_id',$this->plan_id,true);
		$criteria->compare('priveledge_id',$this->priveledge_id,true);
		$criteria->compare('priveledge_id',$this->quota,true);
		$criteria->compare('priveledge_id',$this->type,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('modified_at',$this->modified_at,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('modified_by',$this->modified_by,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PlanPriveledges the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
