<?php

/**
 * This is the model class for table "sp_company_priveledge".
 *
 * The followings are the available columns in table 'sp_company_priveledge':
 * @property string $id
 * @property string $priveledge_id
 * @property string $company_id
 * @property string $start_time
 * @property string $end_time
 * @property string $recursive
 * @property string $recursive_type
 * @property string $quota
 * @property string $status
 * @property string $created_at
 * @property string $modified_at
 * @property string $created_by
 * @property string $modified_by
 *
 * The followings are the available model relations:
 * @property Priveledge $priveledge
 * @property SpCompany $company
 * @property SpUser $createdBy
 * @property SpUser $modifiedBy
 */
class CompanyPriveledge extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sp_company_priveledge';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id', 'required'),
			array('id, priveledge_id, quota, company_id, start_time, end_time, created_at, modified_at, created_by, modified_by', 'length', 'max'=>20),
			array('recursive', 'length', 'max'=>13),
			array('recursive_type', 'length', 'max'=>7),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, priveledge_id, company_id, start_time, end_time, recursive, recursive_type, quota, created_at, modified_at, created_by, modified_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'priveledge' => array(self::BELONGS_TO, 'Priveledge', 'priveledge_id'),
			'company' => array(self::BELONGS_TO, 'SpCompany', 'company_id'),
			'createdBy' => array(self::BELONGS_TO, 'SpUser', 'created_by'),
			'modifiedBy' => array(self::BELONGS_TO, 'SpUser', 'modified_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'priveledge_id' => 'Priveledge',
			'company_id' => 'Company',
			'start_time' => 'Start Time',
			'end_time' => 'End Time',
			'recursive' => 'Recursive',
			'recursive_type' => 'Recursive Type',
			'quota' => 'Quota',
			'created_at' => 'Created At',
			'modified_at' => 'Modified At',
			'created_by' => 'Created By',
			'modified_by' => 'Modified By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                
		$criteria->compare('id',$this->id,true);
		$criteria->compare('priveledge_id',$this->priveledge_id,true);
		$criteria->compare('company_id',$this->company_id,true);
		$criteria->compare('start_time',$this->start_time,true);
		$criteria->compare('end_time',$this->end_time,true);
		$criteria->compare('recursive',$this->recursive,true);
		$criteria->compare('recursive_type',$this->recursive_type,true);
		$criteria->compare('quota',$this->quota);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('modified_at',$this->modified_at,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('modified_by',$this->modified_by,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CompanyPriveledge the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
