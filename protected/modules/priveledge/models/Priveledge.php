<?php

/**
 * This is the model class for table "sp_priveledge".
 *
 * The followings are the available columns in table 'sp_priveledge':
 * @property string $id
 * @property string $name
 * @property string $key
 * @property string $function
 * @property string $group
 * @property integer $status
 * @property string $function_list_id
 * @property string $created_at
 * @property string $modified_at
 * @property string $created_by
 * @property string $modified_by
 *
 * The followings are the available model relations:
 * @property SpCompanyPriveledge[] $spCompanyPriveledges
 * @property SpPlanPriveledges[] $spPlanPriveledges
 * @property SpUser $createdBy
 * @property SpUser $modifiedBy
 * @property SpFunctionList $functionList
 * @property SpUserPriveldge[] $spUserPriveldges
 */
class Priveledge extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sp_priveledge';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, function_list_id', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('id, function_list_id, created_at, modified_at, created_by, modified_by', 'length', 'max'=>20),
			array('name, key, function, group', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, key, function, group, status, function_list_id, created_at, modified_at, created_by, modified_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'spCompanyPriveledges' => array(self::HAS_MANY, 'CompanyPriveledge', 'priveledge_id'),
			'spPlanPriveledges' => array(self::HAS_MANY, 'SpPlanPriveledges', 'priveledge_id'),
			'createdBy' => array(self::BELONGS_TO, 'SpUser', 'created_by'),
			'modifiedBy' => array(self::BELONGS_TO, 'SpUser', 'modified_by'),
			'functionList' => array(self::BELONGS_TO, 'SpFunctionList', 'function_list_id'),
			'spUserPriveldges' => array(self::HAS_MANY, 'SpUserPriveldge', 'priveledge_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'key' => 'Key',
			'function' => 'Compute Function',
			'group' => 'Group',
			'status' => 'Status',
			'function_list_id' => 'Function List (Controller Mapping)',
			'created_at' => 'Created At',
			'modified_at' => 'Modified At',
			'created_by' => 'Created By',
			'modified_by' => 'Modified By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('key',$this->key,true);
		$criteria->compare('function',$this->function,true);
		$criteria->compare('group',$this->group,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('function_list_id',$this->function_list_id,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('modified_at',$this->modified_at,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('modified_by',$this->modified_by,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Priveledge the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
