<?php

/**
 * This is the model class for table "sp_template_item".
 *
 * The followings are the available columns in table 'sp_template_item':
 * @property string $id
 * @property string $template_id
 * @property string $name
 * @property integer $is_item
 * @property string $created_by
 * @property string $created_at
 * @property string $modified_by
 * @property string $modified_at
 *
 * The followings are the available model relations:
 * @property Template $template
 */
class TemplateItem extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sp_template_item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, template_id, name', 'required'),
			array('is_item', 'numerical', 'integerOnly'=>true),
			array('id, template_id, name, created_by, modified_by', 'length', 'max'=>255),
			array('created_at, modified_at', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, template_id, name, is_item, created_by, created_at, modified_by, modified_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'template' => array(self::BELONGS_TO, 'Template', 'template_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'template_id' => 'Template',
			'name' => 'Name',
			'is_item' => 'Is Item',
			'created_by' => 'Created By',
			'created_at' => 'Created At',
			'modified_by' => 'Modified By',
			'modified_at' => 'Modified At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('template_id',$this->template_id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('is_item',$this->is_item);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('modified_by',$this->modified_by,true);
		$criteria->compare('modified_at',$this->modified_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TemplateItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public static function create($tid,$name,$is_item){
            $templateitem = new TemplateItem;
            $templateitem->id = AppInterface::getUniqueId();
            $templateitem->template_id = $tid;
            $templateitem->name = $name;
            $templateitem->is_item = $is_item;
            $templateitem->created_at = time();
            $templateitem->modified_at = time();
            $templateitem->created_by = AppUser::getUserId();
            $templateitem->modified_by = AppUser::getUserId();
            if($templateitem->save()){
                return true;
            }
            return false;
        }
}
