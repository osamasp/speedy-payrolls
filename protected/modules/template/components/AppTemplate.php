<?php

/*
 * Description: Component for User Module
 * Author: Usama Ayaz
 * Dated:  2-June-2014
 */

class AppTemplate extends CComponent {

    public static function getItems($tid) {
        return TemplateItem::model()->findAllByAttributes(array('template_id' => $tid, 'is_item' => 1));
    }

    public static function getPayrates($tid) {
        return TemplateItem::model()->findAllByAttributes(array('template_id' => $tid, 'is_item' => 0));
    }

    public static function updatesItems($tid, $items) {
        $command = Yii::app()->db->createCommand();
        $sql = 'DELETE FROM sp_template_item WHERE template_id=:tid AND is_item=:ii';
        $params = array(
            ":tid" => $tid,
            ":ii" => 1
        );
        $command->setText($sql)->execute($params);

        foreach ($items as $name) {
            if ($name) {
                TemplateItem::create($tid, $name, 1);
            }
        }
    }

    public static function updatesPayrates($tid, $payrates) {
        $command = Yii::app()->db->createCommand();
        $sql = 'DELETE FROM sp_template_item WHERE template_id=:tid AND is_item=:ii';
        $params = array(
            ":tid" => $tid,
            ":ii" => 0
        );
        $command->setText($sql)->execute($params);
        
        foreach ($payrates as $name) {
            if ($name) {
                TemplateItem::create($tid, $name, 0);
            }
        }
    }
    
    public static function getItemsForView($tid){
        $items = TemplateItem::model()->findAllByAttributes(array('template_id' => $tid, 'is_item' => 1));
        $result = '';
        foreach($items as $item){
            $result .= $item->name.', ';
        }
        return rtrim(trim($result),',');
    } 
    
    public static function getPayratesForView($tid){
        $items = TemplateItem::model()->findAllByAttributes(array('template_id' => $tid, 'is_item' => 0));
        $result = '';
        foreach($items as $item){
            $result .= $item->name.', ';
        }
        return rtrim(trim($result),',');
    } 
    
    public static function getAllTemplates(){
        return Template::model()->findAllByAttributes(array('status' => 1));
    } 
    
    public static function getAllCompaniesWithOutTemplates(){
        $c = new CDbCriteria;
        $c->addCondition('t.id NOT IN (select DISTINCT company_id from sp_payrate_name)');
        return Company::model()->findAll($c);
    } 

}
