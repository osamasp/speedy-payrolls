<?php

class MainController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
                //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'apply', 'gettemplateitems', 'view', 'create', 'update', 'admin', 'delete'),
                'users' => array('*'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Template;

        if (isset($_POST['Template'])) {
            $model->attributes = $_POST['Template'];
            $model->id = AppInterface::getUniqueId();
            $model->created_at = time();
            $model->modified_at = time();
            $model->created_by = AppUser::getUserId();
            $model->modified_by = AppUser::getUserId();
            if ($model->save()) {
                foreach ($_POST['item'] as $item) {
                    if ($item) {
                        TemplateItem::create($model->id, $item, 1);
                    }
                }
                foreach ($_POST['payrate'] as $payrate) {
                    if ($payrate) {
                        TemplateItem::create($model->id, $payrate, 0);
                    }
                }
                Yii::app()->user->setFlash('success', 'Template created successfully');
                $this->redirect(array('index'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        if (isset($_POST['payrate']) || isset($_POST['item'])) {
            AppTemplate::updatesPayrates($id, $_POST['payrate']);
            AppTemplate::updatesItems($id, $_POST['item']);
            Yii::app()->user->setFlash('success', 'Template updated successfully');
            $this->redirect(array('index'));
        }

        $this->render('update', array(
            'model' => $model,
            'payrates' => AppTemplate::getPayrates($id),
            'items' => AppTemplate::getItems($id),
            'templates' => AppTemplate::getAllTemplates(),
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $template = Template::model()->findByPk($id);
        $template->status = 0;
        $template->save();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(array('index'));
    }

    public function actionApply() {
        if (isset($_POST['company_id'])) {
            $internal_contracts = AppCompany::getAllInternalContracts($_POST['company_id']);
            foreach ($internal_contracts as $contract) {
                if (isset($_POST['item'])) {
                    foreach ($_POST['item'] as $item) {
                        if (isset($item['enable'])) {
                            ContractSetting::create($contract->id, $item['name'], $item['value'] ? $item['value'] : 10, 1);
                        }
                    }
                }
                if (isset($_POST['payrate'])) {
                    foreach ($_POST['payrate'] as $payrate) {
                        if (isset($payrate['enable'])) {
                            ContractSetting::create($contract->id, $payrate['name'], 10, 0);
                        }
                    }
                }
            }
            if (isset($_POST['item'])) {
                foreach ($_POST['item'] as $item) {
                    if (isset($item['enable'])) {
                        PayrateName::create($_POST['company_id'], $item['name'], 1, $item['value']);
                    }
                }
            }
            if (isset($_POST['payrate'])) {
                foreach ($_POST['payrate'] as $payrate) {
                    if (isset($payrate['enable'])) {
                        PayrateName::create($_POST['company_id'], $payrate['name']);
                    }
                }
            }
            Yii::app()->user->setFlash('success', 'Template applied successfully');
        }
        $this->render('apply', array(
            'templates' => AppTemplate::getAllTemplates(),
            'companies' => AppTemplate::getAllCompaniesWithOutTemplates(),
        ));
    }

    public function actiongettemplateitems($id) {
        if (Yii::app()->request->isAjaxRequest) {
            $data = array(
                'status' => 0,
                'data' => '',
            );
            if (Template::model()->findByPk($id)) {
                $data['status'] = 1;
                $data['data'] = $this->renderPartial('_templateitems', array(
                    'payrates' => AppTemplate::getPayrates($id),
                    'items' => AppTemplate::getItems($id),
                        ), true);
            }
            AppSetting::generateAjaxResponse($data);
        } else {
            throw new CHttpException(400, 'Invalid Request');
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = AppTemplate::getAllTemplates();
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Template('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Template']))
            $model->attributes = $_GET['Template'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Template the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Template::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Template $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'template-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
