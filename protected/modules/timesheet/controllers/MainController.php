<?php

class MainController extends Controller {

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('index', 'addwizard', 'currenttimecard', 'export', 'timecardexport', 'gettypefrmcontrct', 'addtimesheet', 'archives', 'add', 'edit', 'GetOne', 'GetSheetCard', 'GetSubEntries'),
                'roles' => array('admin', 'employee'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('GetSheetCard', 'GetSubEntries'),
                'roles' => array('payroller'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('DispatchPayroll', 'DispatchCopy', 'addtimesheet', 'instantapprove', 'index'),
                'roles' => array('admin', 'super_admin',),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionExport() {
        $webroot = Yii::getPathOfAlias('webroot');
        if (!is_dir($webroot . '/uploads/timesheets')) {
            mkdir($webroot . '/uploads/timesheets');
            chmod(($webroot . '/uploads/timesheets'), 0755);
            // the default implementation makes it under 777 permission, which you could possibly change recursively before deployment, but here's less of a headache in case you don't
        }
        $file = $webroot . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'timesheets' . DIRECTORY_SEPARATOR . 'timesheet-' . date('d-m-Y-H-i') . '.csv';
        $handle = fopen($file, 'w');
        $user_id = AppUser::getUserId();
        $id = Yii::app()->getRequest()->getParam("id");
        $current = Yii::app()->getRequest()->getParam("current");
        $is_timecard = Yii::app()->getRequest()->getParam("is_timecard");
        $labels = array();
//        $data = AppCompany::getApprovalList();
        if (AppUser::isUserAdmin() && Yii::app()->getRequest()->getParam("contract") == '') {
            if ($id == 1) {
                if ($current == 1) {
                    $result = AppTimeSheet::getTimesheetOfTwoWayContract(array('dispatch_flag' => 0));
                } else if ($current == 0) {
                    $result = AppTimeSheet::getTimesheetOfTwoWayContract(array('dispatch_flag' => 1));
                } else {
                    $result = AppTimeSheet::getTimesheetOfThreeWayContract();
                }
            } else if ($id == 2) {
                if ($current == 1) {
                    $result = AppTimeSheet::getTimesheetOfThreeWayContract(array('dispatch_flag' => 0));
                } else if ($current == 0) {
                    $result = AppTimeSheet::getTimesheetOfThreeWayContract(array('dispatch_flag' => 1));
                } else {
                    $result = AppTimeSheet::getTimesheetOfThreeWayContract();
                }
            } else if ($id == 3) {
                $result = AppTimeSheet::getTimeSheetArchivesForAdmin();
            } else if ($id == 4) {
                $args = array('user' => Yii::app()->user->id);
                /*                 * * Filter By Contract Timesheets ** */
                if (Yii::app()->getRequest()->getParam("contract") != '') {
                    $args['contract'] = Yii::app()->getRequest()->getParam("contract");
                }
                $args['status'] = array("approved", "in-approval", "closed", "payrolled", "rejected");
                $args['contractType'] = 'internal';
                $args['company'] = AppUser::getUserCompany()->id;
                $result = AppTimeSheet::getTimeSheets($args);
                if ($current == 1) {
                    $result = AppTimeSheet::getTimesheetOfResource(array('dispatch_flag' => 0));
                } else if ($current == 0) {
                    $result = AppTimeSheet::getTimesheetOfResource(array('dispatch_flag' => 1));
                }
//                else {
//                    $result = AppTimeSheet::getTimesheetOfResource();
//                }
            } else if ($id == 5) {
                $args = array('approver' => Yii::app()->user->id, 'status' => 'in-approval');
                /*                 * * Filter By Contract Timesheets ** */
                if (Yii::app()->getRequest()->getParam("contract") != '') {
                    $args['contract'] = Yii::app()->getRequest()->getParam("contract");
                }

                $result = AppTimeSheet::getTimeSheets($args);
            } else {
                $args['status'] = 'pending';
                $args['contractType'] = 'internal';
                $args['company'] = AppUser::getUserCompany()->id;
                $result = AppTimeSheet::getTimeSheets($args);
            }
        } else {
            if (Yii::app()->getRequest()->getparam("contract") != '') {
                $result = AppTimeSheet::getTimeSheets($args);
            } else if ($id == 1) {
                if ($current == 1) {
                    $result = AppTimeSheet::getUserTimesheetOfTwoWayContract(array('dispatch_flag' => 0));
                } else if ($current == 0) {
                    $result = AppTimeSheet::getUserTimesheetOfTwoWayContract(array('dispatch_flag' => 1));
                } else {
                    $result = AppTimeSheet::getUserTimesheetOfThreeWayContract();
                }
            } else if ($id == 2) {
                if ($current == 1) {
                    $result = AppTimeSheet::getUserTimesheetOfThreeWayContract(array('dispatch_flag' => 0));
                } else if ($current == 0) {
                    $result = AppTimeSheet::getUserTimesheetOfThreeWayContract(array('dispatch_flag' => 1));
                } else {
                    $result = AppTimeSheet::getUserTimesheetOfThreeWayContract();
                }
            } else if ($id == 3) {
                $result = AppTimeSheet::getTimeSheetArchivesForAdmin();
            } else if ($id == 4) {
                $args = array('user' => Yii::app()->user->id);
                /*                 * * Filter By Contract Timesheets ** */
                if (Yii::app()->getRequest()->getParam("contract") != '') {
                    $args['contract'] = Yii::app()->getRequest()->getParam("contract");
                }
                $args['status'] = array("approved", "in-approval", "closed", "payrolled", "rejected");
                $args['contractType'] = 'internal';
                $args['company'] = AppUser::getUserCompany()->id;
                $result = AppTimeSheet::getTimeSheets($args);
                if ($current == 1) {
                    $result = AppTimeSheet::getTimesheetOfResource(array('dispatch_flag' => 0));
                } else if ($current == 0) {
                    $result = AppTimeSheet::getTimesheetOfResource(array('dispatch_flag' => 1));
                }
//                else {
//                    $result = AppTimeSheet::getTimesheetOfResource();
//                }
            } else if ($id == 5) {
                $args = array('approver' => Yii::app()->user->id, 'status' => 'in-approval');
                /*                 * * Filter By Contract Timesheets ** */
                if (Yii::app()->getRequest()->getParam("contract") != '') {
                    $args['contract'] = Yii::app()->getRequest()->getParam("contract");
                }

                $result = AppTimeSheet::getTimeSheets($args);
            } else {
                $args['status'] = 'pending';
                $args['contractType'] = 'internal';
                $args['company'] = AppUser::getUserCompany()->id;
                $result = AppTimeSheet::getTimeSheets($args);
            }
        }
        $data = array();
        $i = 0;
//        dd($data);
//        dd($_POST);
        foreach ($result['dataSet'] as $key => $item) {
            if (AppUser::isUserAdmin()) {
                if (isset($item->contract->parent)) {
                    if ($item->contract->type == 'out_b' && $item->contract->company_id != AppUser::getUserCompany()->id) {
                        $etype = ucwords(str_replace('_', ' ', $item->contract->parent->CustomEmploymentType));
                    }
                }
            } else {
                $etype = $item->contract->CustomName;
            }
            if (isset($_POST['entry'])) {
                if (in_array($item->id, $_POST['entry'])) {
                    if (isset($_POST['export_fields'])) {
                        foreach ($_POST['export_fields'] as $items) {
                            if ($items == 'contract_id') {
                                $data[$i]['contract_id'] = isset($etype) ? $etype : ucwords(str_replace('_', ' ', AppUser::isUserAdmin() ? $item->contract->CustomEmploymentType : $item->contract->CustomName));
                            } else if ($items == 'employee') {
                                $data[$i]['employee'] = $item->contract->user->full_name;
                            } else if ($items == 'approver') {
                                $data[$i]['approver'] = $item->contract->approver->full_name;
                            } else if ($items == 'period') {
                                $data[$i]['period'] = AppTimeSheet::formatSheetPeriod($item);
                            } else if ($items == 'status') {
                                $data[$i]['status'] = $item->status;
                            } else if ($items == 'payslip') {
                                $paySlipStatus = AppPayslip::isIssued($item);
                                $data[$i]['payslip'] = ($paySlipStatus) ? 'issued' : 'N/A';
                            } else if ($items == 'created_at') {
                                $data[$i]['created_at'] = $item->created_at;
                            } else if ($items == 'expense') {
                                $data[$i]['expense'] = $item->expense;
                            } else if ($items == 'staff_id') {
                                $data[$i]['staff_id'] = $item->contract->user->staff_id;
                            }
                        }
                    } else {
                        $data[$i]['contract'] = isset($etype) ? $etype : ucwords(str_replace('_', ' ', AppUser::isUserAdmin() ? $item->contract->CustomEmploymentType : $item->contract->CustomName));
                        $data[$i]['employee'] = $item->contract->user->full_name;
                        $data[$i]['approver'] = $item->contract->approver->full_name;
                        $data[$i]['period'] = AppTimeSheet::formatSheetPeriod($item);
                        $data[$i]['status'] = $item->status;
                        $paySlipStatus = AppPayslip::isIssued($item);
                        $data[$i]['payslip'] = ($paySlipStatus) ? 'issued' : 'N/A';
                        $data[$i]['expense'] = $item->expense;
                        $data[$i]['staff_id'] = $item->contract->user->staff_id;
                        $data[$i]['created_at'] = $item->created_at;
                    }
                }
                $i++;
            } else {
                if (isset($_POST['export_fields'])) {
                    foreach ($_POST['export_fields'] as $items) {
                        if ($items == 'contract_id') {
                            $data[$i]['contract_id'] = isset($etype) ? $etype : ucwords(str_replace('_', ' ', AppUser::isUserAdmin() ? $item->contract->CustomEmploymentType : $item->contract->CustomName));
                        } else if ($items == 'employee') {
                            $data[$i]['employee'] = $item->contract->user->full_name;
                        } else if ($items == 'approver') {
                            $data[$i]['approver'] = $item->contract->approver->full_name;
                        } else if ($items == 'period') {
                            $data[$i]['period'] = AppTimeSheet::formatSheetPeriod($item);
                        } else if ($items == 'status') {
                            $data[$i]['status'] = $item->status;
                        } else if ($items == 'payslip') {
                            $paySlipStatus = AppPayslip::isIssued($item);
                            $data[$i]['payslip'] = ($paySlipStatus) ? 'issued' : 'N/A';
                        } else if ($items == 'created_at') {
                            $data[$i]['created_at'] = $item->created_at;
                        } else if ($items == 'expense') {
                            $data[$i]['expense'] = $item->expense;
                        } else if ($items == 'staff_id') {
                            $data[$i]['staff_id'] = $item->contract->user->staff_id;
                        }
                    }
                } else {
                    $data[$i]['contract'] = isset($etype) ? $etype : ucwords(str_replace('_', ' ', AppUser::isUserAdmin() ? $item->contract->CustomEmploymentType : $item->contract->CustomName));
                    $data[$i]['employee'] = $item->contract->user->full_name;
                    $data[$i]['approver'] = $item->contract->approver->full_name;
                    $data[$i]['period'] = AppTimeSheet::formatSheetPeriod($item);
                    $data[$i]['status'] = $item->status;
                    $paySlipStatus = AppPayslip::isIssued($item);
                    $data[$i]['payslip'] = ($paySlipStatus) ? 'issued' : 'N/A';
                    $data[$i]['expense'] = $item->expense;
                    $data[$i]['staff_id'] = $item->contract->user->staff_id;
                    $data[$i]['created_at'] = $item->created_at;
                }
                $i++;
            }
        }

        if (isset($_POST['export_fields'])) {
            $last_element = end($_POST['export_fields']);
            foreach ($_POST['export_fields'] as $item) {
                array_push($labels, $item);
            }
        } else {
            $labels = array('contract', 'employee', 'approver', 'period', 'status', 'payslip', 'staff_id', 'created_at');
        }

        if ($is_timecard) {
            $timecard_data = [];
            $duration = 0;
            foreach ($result['dataSet'] as $entry) {
                $timecard = $this->GetTimeCardDetails($entry->id, false);
                if ($timecard["status"]) {
                    $timecard_data[$entry->id] = $timecard["data"];
                } else {
                    $timecard_data[$entry->id] = $timecard["reason"];
                }
            }
            $duration = $timecard['data']['detail']['entry']->start_time;
            $l_monday = AppInterface::last_monday($duration);
            if (date('d/m/Y', $timecard['data']['parent']->start_time) != date(AppInterface::getdateformat(), $l_monday)) {
                $duration = $l_monday;
                for ($i = 0; $i < 7; $i++) {
                    $criteria = new CDbCriteria();
//                                $criteria->addBetweenCondition('created_at', $duration, $duration+86399);
                    $criteria->condition = 'contract_id=' . $timecard['data']['parent']->contract_id . ' AND start_time >= ' . $duration . ' AND end_time <= ' . ($duration + 86400);
                    $timesheet = Timesheet::model()->find($criteria);
                    if (date('d/m/Y', $timecard['data']['parent']->start_time) == date(AppInterface::getdateformat(), $duration)) {
                        break;
                    }
                    $data[$i]['Date'] = date('d/m/Y', $duration);
                    $data[$i]['Day'] = date('l', $duration);
                    if ($timesheet != null) {
                        $data[$i]['Start'] = date('h:i A', $timesheet->start_time);
                        $data[$i]['Finish'] = date('h:i A', $timesheet->end_time);
                        $_breaks = UserLunch::model()->findAllByAttributes(array("user_timing_id" => $timesheet->id));
                        $_notes = TimesheetNotes::model()->findAllByAttributes(array("user_timing_id" => $timesheet->id));
                        if (count($_breaks) > 0) {
                            foreach ($_breaks as $break) {
                                if ($break->user_timing_id == $entry->id) {
                                    $break_hours += ($break->lunch_out - $break->lunch_in);
                                }
                            } $data[$i]['Breaks'] = date('h', $break_hours) . " H" . date('i', $break_hours) . " m";
                        } else {
                            $data[$i]['Breaks'] = "-";
                        }
                        if (count($timesheet->payrates) > 0) {
                            foreach ($timesheet->payrates as $p) {
                                if ($p->is_item == 0) {
                                    $data[$i]['Work'] = number_format($p->hours) . " H";
                                    $data[$i]['Total'] = ($p->hours - $break_hours) . " H";
                                } else {
                                    $data[$i]['Work'] = "-";
                                    $data[$i]['Total'] = "-";
                                }
                            }
                        } else {
                            $data[$i]['Work'] = "-";
                            $data[$i]['Total'] = "-";
                        }
                        foreach ($timesheet->payrates as $key => $p) {
                            if ($p->is_item == 0) {
                                
                            } else {
                                $expence += $p->value * $p->item_value;
                            }
                        }
                        $expence += $timesheet->expense;
                        $data[$i]['Extra Charges'] = number_format($expence);
                        if (count($_notes) > 0) {
                            foreach ($_notes as $note) {
                                if ($note->user_timing_id == $entry->id) {
                                    $data[$i]['Notes'] .= $note->note . " - ";
                                }
                            }
                        } else {
                            $data[$i]['Notes'] = "-";
                        }
                    } else {
                        $data[$i]['Start'] = "-";
                        $data[$i]['Finish'] = "-";
                        $data[$i]['Breaks'] = "-";
                        $data[$i]['Work'] = "-";
                        $data[$i]['Total'] = "-";
                        $data[$i]['Extra Charges'] = "-";
                        $data[$i]['Notes'] = "-";
                    }
                    $duration += 86400;
                }
            }
            $labels = array('Day', 'Date', 'Start', 'Finish', 'Breaks', 'Work', 'Total', 'Extra Charges', 'Notes');
        }
//        dd($data);
        AppInterface::GenerateCSV($file, $labels, $data);
        fclose($handle);
        $curr_user = AppUser::getCurrentUser();
        $url = $this->createAbsoluteUrl('/uploads/timesheets/' . 'timesheet-' . date('d-m-Y-H-i') . '.csv');
        $this->layout = "//layout/blank";
        $response = array('status' => false, 'data' => '', 'reason' => 'Not a Valid Ajax Request or Post Data is Missing');
        $response['status'] = true;
        $response['reason'] = '';
        $response['url'] = $url;
        $response['data'] = $this->render('//user/employee/viewlist', array(
            'file' => $file,
            'name' => 'timesheet-' . date('d-m-Y-H-i') . '.csv',
            'url' => $url
                ), true);
        AppSetting::generateAjaxResponse($response);
        Yii::app()->request->sendFile(AppInterface::getFilename() . '.csv', file_get_contents($file));
    }

    public function actionTimecardExport() {
        $webroot = Yii::getPathOfAlias('webroot');
        if (!is_dir($webroot . '/uploads/timesheets')) {
            mkdir($webroot . '/uploads/timesheets');
            chmod(($webroot . '/uploads/timesheets'), 0755);
            // the default implementation makes it under 777 permission, which you could possibly change recursively before deployment, but here's less of a headache in case you don't
        }
        $file = $webroot . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'timesheets' . DIRECTORY_SEPARATOR . 'timesheet-' . date('d-m-Y-H-i') . '.csv';
        $handle = fopen($file, 'w');
        $labels = array();
        $regSum = 0;
        $addSum = 0;
        $expSum = 0;
        $break_hours = 0;
        $total_hours = 0;
        $duration = 0;
        $expence = 0;

        $id = Yii::app()->getRequest()->getParam("timecard_id");
        $data = array();
        $result = AppTimeSheet::getTimecard($id);
        if (isset($result)) {
            $childs = $result['childs'];
            $last_element = end($childs["dataSet"]);
            $mergeChilds = $result['mergeChilds'];
            $parent = $result['parent'];
            $breaks = $result['breaks'];
            $notes = $result['notes'];
            $details = $result['details'];
            if (isset($childs['dataSet']) && count($childs['dataSet']) > 0) {
                $duration = $details['entry']->start_time;
                $l_monday = AppInterface::last_monday($duration);
                if (date('d/m/Y', $parent->start_time) != date(AppInterface::getdateformat(), $l_monday)) {
                    $duration = $l_monday;
                    for ($i = 0; $i < 7; $i++) {
                        $criteria = new CDbCriteria();
//                                $criteria->addBetweenCondition('created_at', $duration, $duration+86399);
                        $criteria->condition = 'contract_id=' . $parent->contract_id . ' AND start_time >= ' . $duration . ' AND end_time <= ' . ($duration + 86400);
                        $timesheet = Timesheet::model()->find($criteria);
                        if (date('d/m/Y', $parent->start_time) == date(AppInterface::getdateformat(), $duration)) {
                            break;
                        }
                        $data[$i]['Day'] = date('l', $duration);
                        $data[$i]['Date'] = date('d/m/Y', $duration);
                        if ($timesheet != null) {
                            $data[$i]['Start'] = date('h:i A', $timesheet->start_time);
                            $data[$i]['Finish'] = date('h:i A', $timesheet->end_time);
                            $_breaks = UserLunch::model()->findAllByAttributes(array("user_timing_id" => $timesheet->id));
                            $_notes = TimesheetNotes::model()->findAllByAttributes(array("user_timing_id" => $timesheet->id));
                            if (count($_breaks) > 0) {
                                foreach ($_breaks as $break) {
                                    if ($break->user_timing_id == $entry->id) {
                                        $break_hours += ($break->lunch_out - $break->lunch_in);
                                    }
                                } $data[$i]['Breaks'] = date('h', $break_hours) . " H" . date('i', $break_hours) . " m";
                            } else {
                                $data[$i]['Breaks'] = "-";
                            }
                            if (count($timesheet->payrates) > 0) {
                                foreach ($timesheet->payrates as $p) {
                                    if ($p->is_item == 0) {
                                        $data[$i]['Work'] = number_format($p->hours) . " H";
                                        $data[$i]['Total'] = ($p->hours - $break_hours) . " H";
                                    } else {
                                        $data[$i]['Work'] = "-";
                                        $data[$i]['Total'] = "-";
                                    }
                                }
                            } else {
                                $data[$i]['Work'] = "-";
                                $data[$i]['Total'] = "-";
                            }
                            foreach ($timesheet->payrates as $key => $p) {
                                if ($p->is_item == 0) {
                                    
                                } else {
                                    $expence += $p->value * $p->item_value;
                                }
                            }
                            $expence += $timesheet->expense;
                            $data[$i]['Extra Charges'] = number_format($expence);
                            if (count($_notes) > 0) {
                                foreach ($_notes as $note) {
                                    if ($note->user_timing_id == $entry->id) {
                                        $data[$i]['Notes'] .= $note->note . " - ";
                                    }
                                }
                            } else {
                                $data[$i]['Notes'] = "-";
                            }
                        } else {
                            $data[$i]['Start'] = "-";
                            $data[$i]['Finish'] = "-";
                            $data[$i]['Breaks'] = "-";
                            $data[$i]['Work'] = "-";
                            $data[$i]['Total'] = "-";
                            $data[$i]['Extra Charges'] = "-";
                            $data[$i]['Notes'] = "-";
                        }
                        $duration += 86400;
                    }
                }

                if (count($childs['dataSet']) > 0) {
                    $payhourstotal = array();
                    foreach ($childs['dataSet'] as $entry) {
                        $regularTime = AppTimeSheet::calHrsDiff($entry->start_time, $entry->end_time);
                        $regSum += $regularTime;
                        $addSum += $entry->overtime;

                        foreach ($entry->payrates as $key => $p) {
                            if ($p->is_item == 0) {
                                if (!isset($payhourstotal[$key])) {
                                    $payhourstotal[$key] = 0;
                                }
                                $payhourstotal[$key] += $p->hours;
                            } else {
                                $expence += $p->value * $p->item_value;
                            }
                        }
                        $expence += $entry->expense;
                        $expSum += $expence;

                        if (date('d/m/Y', $entry->start_time) != date(AppInterface::getdateformat(), $duration)) {
                            for ($i = 0; $i < 7; $i++) {
                                if (date('d/m/Y', $entry->start_time) == date(AppInterface::getdateformat(), $duration)) {
                                    break;
                                }
                                $data[$i]['Day'] = date('l', $duration);
                                $data[$i]['Date'] = date('d/m/Y', $duration);
                                $data[$i]['Start'] = "-";
                                $data[$i]['Finish'] = "-";
                                $data[$i]['Breaks'] = "-";
                                $data[$i]['Work'] = "-";
                                $data[$i]['Total'] = "-";
                                $data[$i]['Extra Charges'] = "-";
                                $data[$i]['Notes'] = "-";
                                $duration += 86400;
                            }
                        }
                        if (date('d/m/Y', $entry->start_time) == date(AppInterface::getdateformat(), $duration)) {
                            $data[$i]['Day'] = date('l', $entry->start_time);
                            $data[$i]['Date'] = date('d/m/Y', $entry->start_time);
                            $data[$i]['Start'] = date('h:i A', $entry->start_time);
                            $data[$i]['Finish'] = date('h:i A', $entry->end_time);
                            if (count($breaks) > 0) {
                                foreach ($breaks as $break) {
                                    if ($break->user_timing_id == $entry->id) {
                                        $break_hours += ($break->lunch_out - $break->lunch_in);
                                    }
                                } $data[$i]['Breaks'] = date('h', $break_hours) . " H" . date('i', $break_hours) . " m";
                            } else {
                                $data[$i]['Breaks'] = "-";
                            }
                            if (count($entry->payrates) > 0) {
                                foreach ($entry->payrates as $p) {
                                    if ($p->is_item == 0) {
                                        $data[$i]['Work'] = number_format($p->hours) . " H";
                                        $data[$i]['Total'] = ($p->hours - $break_hours) . " H";
                                    } else {
                                        $data[$i]['Work'] = "-";
                                        $data[$i]['Total'] = "-";
                                    }
                                }
                            } else {
                                $data[$i]['Work'] = "-";
                                $data[$i]['Total'] = "-";
                            }
                            $data[$i]['Extra Charges'] = number_format($expence);
                            if (count($notes) > 0) {
                                foreach ($notes as $note) {
                                    if ($note->user_timing_id == $entry->id) {
                                        $data[$i]['Notes'] .= $note->note;
                                    }
                                }
                            } else {
                                $data[$i]['Notes'] = "-";
                            }
                        } else {
                            $data[$i]['Day'] = date('l', $duration);
                            $data[$i]['Date'] = date('d/m/Y', $duration);
                            $data[$i]['Start'] = "-";
                            $data[$i]['Finish'] = "-";
                            $data[$i]['Breaks'] = "-";
                            $data[$i]['Work'] = "-";
                            $data[$i]['Total'] = "-";
                            $data[$i]['Extra Charges'] = "-";
                            $data[$i]['Notes'] = "-";
                        }
                        if ($entry == $last_element && date('d/m/Y', $entry->start_time) == date(AppInterface::getdateformat(), $duration) && date('d/m/Y', $details["entry"]->end_time) != date(AppInterface::getdateformat(), $duration)) {
                            $duration += 86400;
                            for ($i = 0; $i < 7; $i++) {
                                $data[$i]['Day'] = date('l', $duration);
                                $data[$i]['Date'] = date('d/m/Y', $duration);
                                $data[$i]['Start'] = "-";
                                $data[$i]['Finish'] = "-";
                                $data[$i]['Breaks'] = "-";
                                $data[$i]['Work'] = "-";
                                $data[$i]['Total'] = "-";
                                $data[$i]['Extra Charges'] = "-";
                                $data[$i]['Notes'] = "-";
                                if (date('d/m/Y', $details["entry"]->end_time) == date(AppInterface::getdateformat(), $duration)) {
                                    break;
                                }
                                $duration += 86400;
                            }
                        }
                        $duration += 86400;
                    }
                }
//                dd($data);
            }
        }

        $labels = array('Day', 'Date', 'Start', 'Finish', 'Breaks', 'Work', 'Total', 'Extra Charges', 'Notes');
        AppInterface::GenerateCSV($file, $labels, $data);
        fclose($handle);
        $url = $this->createAbsoluteUrl('/uploads/timesheets/' . 'timesheet-' . date('d-m-Y-H-i') . '.csv');
        $this->layout = "//layout/blank";
        $response = array('status' => false, 'data' => '', 'reason' => 'Not a Valid Ajax Request or Post Data is Missing');
        $response['status'] = true;
        $response['reason'] = '';
        $response['url'] = $url;
        $response['data'] = $this->render('//user/employee/viewlist', array(
            'file' => $file,
            'name' => 'timesheet-' . date('d-m-Y-H-i') . '.csv',
            'url' => $url
                ), true);
        AppSetting::generateAjaxResponse($response);
        Yii::app()->request->sendFile(AppInterface::getFilename() . '.csv', file_get_contents($file));
    }

    public function actionIndex() {
        // id : 1 -> 2 way contracts
        // id : 2 -> 3 way contracts
        // current : 1 -> current folder
        // current : 0 -> archive folder
        // id : null -> non billable
//        $notification_id = Yii::app()->request->getParam('notification_id');
//        if (isset($notification_id)) {
//            AppUser::updateNotificationStatus($notification_id);
//        }

        $id = Yii::app()->getRequest()->getParam("id");
        $current = Yii::app()->getRequest()->getParam("current");
        AppLogging::addLog('Viewing All Timesheets', 'success', 'application.timesheet.controller.main');

        $this->performAction();


        $args = array('user' => Yii::app()->user->id);
        /*         * * Filter By Contract Timesheets ** */
        if (Yii::app()->getRequest()->getParam("contract") != '') {
            $args['contract'] = Yii::app()->getRequest()->getParam("contract");
        }

        if (AppUser::isUserAdmin() && Yii::app()->getRequest()->getParam("contract") == '') {
            if ($id == 1) {
                if ($current == 1) {
                    $result = AppTimeSheet::getTimesheetOfTwoWayContract(array('dispatch_flag' => 0));
                } else if ($current == 0) {
                    $result = AppTimeSheet::getTimesheetOfTwoWayContract(array('dispatch_flag' => 1));
                } else {
                    $result = AppTimeSheet::getTimesheetOfThreeWayContract();
                }
            } else if ($id == 2) {
                if ($current == 1) {
                    $result = AppTimeSheet::getTimesheetOfThreeWayContract(array('dispatch_flag' => 0));
                } else if ($current == 0) {
                    $result = AppTimeSheet::getTimesheetOfThreeWayContract(array('dispatch_flag' => 1));
                } else {
                    $result = AppTimeSheet::getTimesheetOfThreeWayContract();
                }
            } else if ($id == 3) {
                $result = AppTimeSheet::getTimeSheetArchivesForAdmin();
            } else if ($id == 4) {
                if ($current == 1) {
                    $result = AppTimeSheet::getTimesheetOfResource(array('dispatch_flag' => 0));
                } else if ($current == 0) {
                    $result = AppTimeSheet::getTimesheetOfResource(array('dispatch_flag' => 1));
                } else {
                    $result = AppTimeSheet::getTimesheetOfResource();
                }
            } else {
                $args['status'] = 'pending';
                $args['contractType'] = 'internal';
                $args['company'] = AppUser::getUserCompany()->id;
                $result = AppTimeSheet::getTimeSheets($args);
            }
        } else {
            if (Yii::app()->getRequest()->getparam("contract") != '') {
                $result = AppTimeSheet::getTimeSheets($args);
            } else if ($id == 1) {
                if ($current == 1) {
                    $result = AppTimeSheet::getUserTimesheetOfTwoWayContract(array('dispatch_flag' => 0));
                } else if ($current == 0) {
                    $result = AppTimeSheet::getUserTimesheetOfTwoWayContract(array('dispatch_flag' => 1));
                } else {
                    $result = AppTimeSheet::getUserTimesheetOfThreeWayContract();
                }
            } else if ($id == 2) {
                if ($current == 1) {
                    $result = AppTimeSheet::getUserTimesheetOfThreeWayContract(array('dispatch_flag' => 0));
                } else if ($current == 0) {
                    $result = AppTimeSheet::getUserTimesheetOfThreeWayContract(array('dispatch_flag' => 1));
                } else {
                    $result = AppTimeSheet::getUserTimesheetOfThreeWayContract();
                }
            } else if ($id == 3) {
                $result = AppTimeSheet::getTimeSheetArchivesForAdmin();
            } else if ($id == 4) {
                if ($current == 1) {
                    $result = AppTimeSheet::getTimsheetOfResource(array('dispatch_flag' => 0));
                } else if ($current == 0) {
                    $result = AppTimeSheet::getTimsheetOfResource(array('dispatch_flag' => 1));
                } else {
                    $result = AppTimeSheet::getTimsheetOfResource();
                }
            } else {
                $args['status'] = 'pending';
                $args['contractType'] = 'internal';
                $args['company'] = AppUser::getUserCompany()->id;
                $result = AppTimeSheet::getTimeSheets($args);
            }
        }

        $this->render('index', array('entries' => $result['dataSet'], 'pages' => $result['pages'], 'ids' => $id, 'current' => $current, 'contract' => Yii::app()->getRequest()->getParam("contract")));
    }

    public function actionCurrentTimecard() {
        // id : 1 -> 2 way contracts
        // id : 2 -> 3 way contracts
        // current : 1 -> current folder
        // current : 0 -> archive folder
        // id : null -> non billable

        $id = Yii::app()->getRequest()->getParam("id");
        $current = Yii::app()->getRequest()->getParam("current");
        AppLogging::addLog('Viewing All Timesheets', 'success', 'application.timesheet.controller.main');

        $this->performAction();


        $args = array('user' => Yii::app()->user->id);
        /*         * * Filter By Contract Timesheets ** */
        if (Yii::app()->getRequest()->getParam("contract") != '') {
            $args['contract'] = Yii::app()->getRequest()->getParam("contract");
        }

        if (AppUser::isUserAdmin() && Yii::app()->getRequest()->getParam("contract") == '') {
            if ($id == 1) {
                if ($current == 1) {
                    $result = AppTimeSheet::getTimesheetOfTwoWayContract(array('dispatch_flag' => 0));
                }
            } else if ($id == 2) {
                if ($current == 1) {
                    $result = AppTimeSheet::getTimesheetOfThreeWayContract(array('dispatch_flag' => 0));
                }
            } else if ($id == 4) {
                if ($current == 1) {
//                    $result = AppTimeSheet::getTimsheetOfResource(array('dispatch_flag' => 0));
                    $result = AppTimeSheet::getTimesheetOfResource(array('dispatch_flag' => 0));
                }
            }
        } else {
            if (Yii::app()->getRequest()->getparam("contract") != '') {
                $result = AppTimeSheet::getTimeSheets($args);
            }
        }
        $timecard_data = [];
        foreach ($result['dataSet'] as $entry) {
            $timecard = $this->GetTimeCardDetails($entry->id);
            if ($timecard["status"]) {
                $timecard_data[$entry->id] = $timecard["data"];
            } else {
                $timecard_data[$entry->id] = $timecard["reason"];
            }
        }
//        dd($timecard_data);
        $this->render('currenttimecard', array('entries' => $result['dataSet'], 'timecard' => $timecard_data, 'pages' => $result['pages'], 'ids' => $id, 'current' => $current, 'contract' => Yii::app()->getRequest()->getParam("contract")));
    }

    public function actionArchives() {
        $args = array('user' => Yii::app()->user->id);
        /*         * * Filter By Contract Timesheets ** */
        if (Yii::app()->getRequest()->getParam("contract") != '') {
            $args['contract'] = Yii::app()->getRequest()->getParam("contract");
        }
        $args['status'] = array("approved", "in-approval", "closed", "payrolled", "rejected");
        $args['contractType'] = 'internal';
        $args['company'] = AppUser::getUserCompany()->id;
        $result = AppTimeSheet::getTimeSheets($args);

        $this->render('archives', array('entries' => $result['dataSet'], 'pages' => $result['pages']));
    }

    private function performAction() {
        $response = null;

        if (count($_POST) > 0 && isset($_POST['action']) && isset($_POST['entry']) && count($_POST['entry']) > 0) {

            switch (strtolower($_POST['action'])) {
                case 'merge':
                    $response = AppTimeSheet::mergeTimeSheets($_POST['entry']);
                    if ($response['status'] == true) {
                        AppLogging::addLog('Successfully Merged Timesheets', 'success', 'application.timesheet.controller.main');
                        Yii::app()->user->setFlash('success', 'Successfully Merged Timesheets');
                        $this->redirect(array('index'));
                    } else {
                        AppLogging::addLog($response['reason'], 'error', 'application.timesheet.controller.main');
                        Yii::app()->user->setFlash('error', $response['reason']);
                    }



                    break;
                case 'payroll':
                    $response = AppTimeSheet::dispatchTimeSheetPayroll($_POST['entry']);
                    if ($response['status'] == true) {
                        AppLogging::addLog('Successfully Forwarded Timesheets To Payroller', 'success', 'application.payroller.controller.main');
                        Yii::app()->user->setFlash('success', $response['reason']);
                        AppLogging::addLog("Timesheet Forwarded to Payroller", 'success', 'application.payroller.main');

                        $this->redirect(array('/payroller/main/DispatchPayroll'));
                    } else {
                        AppLogging::addLog($response['reason'], 'error', 'application.timesheet.controller.main');
                        Yii::app()->user->setFlash('error', $response['reason']);
                    }
                    break;


                case 'dispatch':
                    $response = AppTimeSheet::dispatchTimeSheet($_POST['entry']);
                    if ($response['status'] == true) {
                        AppLogging::addLog('Successfully Forwarded Timesheets To Payroller', 'success', 'application.payroller.controller.main');
                        Yii::app()->user->setFlash('success', $response['reason']);
                        AppLogging::addLog("Timesheet Forwarded to Payroller", 'success', 'application.payroller.main');

                        $this->redirect(array('/payroller/main/DispatchPayroll'));
                    } else {
                        AppLogging::addLog($response['reason'], 'error', 'application.timesheet.controller.main');
                        Yii::app()->user->setFlash('error', $response['reason']);
                    }
                    break;
            }
        } elseif (isset($_POST['action']) && !isset($_POST['entry'])) {
            Yii::app()->user->setFlash('error', 'Please select timesheets to perform this action.');
        }
        return true;
    }

    public function actionAddWizard() {
        $types = Yii::app()->getRequest()->getParam('types');
        $userContracts = AppContract::getEmployeeContracts(AppUser::getUserId(), false, $types);
        $tSheetType = AppTimeSheet::getTimesheetType();
        if ($types == "out") {
            $canAccess = WebUser::canAccess("Two_Way");
        } else if ($types == "out_b") {
            $canAccess = WebUser::canAccess("Three_Way");
        } else if ($types == "in") {
            $canAccess = WebUser::canAccess("Non_Billable");
        }
        if ($canAccess["access"]) {
            $error = '';

            if (!empty($_POST)) {

                if (Yii::app()->getRequest()->getPost('contract') != '0' && Yii::app()->getRequest()->getPost('type') != '0') {
                    $cid = Yii::app()->getRequest()->getPost('contract');
                    $type = Yii::app()->getRequest()->getPost('type');
                    $contract = Contract::model()->findByPk($cid);

                    if ($contract != null) {

                        $contract->timesheetTypeSel = $type;
                        if ($contract->update()) {
                            if (Yii::app()->getRequest()->getParam('ajax')) {
                                echo 1;
                                return;
                            } else {
                                $this->redirect(array('main/add', 'contract' => $cid, 'types' => $types));
                            }
                        } else {
                            $error = "Contract cannot be updated";
                        }
                    } else {
                        $error = "Contract Not Found";
                    }
                } else {
                    $error = "Please select Contract or Timesheet Type";
                }
            }
            $this->renderAddWizardJs();

            /*             * *** User Contract Check ** */
            if ($userContracts == null || count($userContracts) == 0) {
                $error = "No Contract has been found";
            } /*             * ********* */

            if ($error != "") {
                Yii::app()->user->setFlash('error', $error);
                AppLogging::addLog($error, 'error', 'application.timesheet.controller.main.addWizard');
                throw new CHttpException(403, $error);
            }
            $this->render('addWizard', array('contracts' => $userContracts, 'tSheetType' => $tSheetType, 'error' => $error, 'type' => $types));
        } else {
            throw new CHttpException(403, $canAccess["message"]);
        }
    }

    public function actiongetTypeFrmContrct() {

        $output = array('status' => false, 'reason' => 'Invalid Request');
        if (Yii::app()->getRequest()->isAjaxRequest) {

            if (Yii::app()->getRequest()->getPost('contractId') != '') {
                $contract = Contract::model()->findByPk(Yii::app()->getRequest()->getPost('contractId'));
                if ($contract != null && $contract->timesheetTypeSel != '') {
                    $output['data'] = ($contract->timesheetTypeSel);
                    $output['status'] = true;
                    $output['reason'] = '';
                } elseif ($contract != null) {
                    $output['reason'] = 'No Type Selected';
                }
            }
        } else {
            $output['reason'] = 'Not an Ajax Request';
        }
        AppSetting::generateAjaxResponse($output);
    }

    public function actionAddTimesheet() {
//        dd(strtotime(date('m/d/Y'))." ".strtotime("last monday"));
        $clocks = UserTimings::model()->findAllBySql("SELECT * FROM sp_user_timings WHERE created_at BETWEEN " . strtotime('last monday') . " AND " . strtotime(date('m/d/Y')));
//        dd($clocks);
        foreach ($clocks as $item) {
            $model = new Timesheet();
            $model->id = AppInterface::getUniqueId();
            $model->contract_id = $item->contract_id;
            $model->type = 'daily';
            $model->start_time = $item->time_in;
            $model->end_time = $item->time_out;
            $model->time = $item->total_time;
            $model->lunchtime = $item->total_lunch;
            if ($item->status == 'approved')
                $model->status = 'approved';
            else if ($item->status == 'pending')
                $model->status = 'pending';
            else
                $model->status = 'in-approval';
            $model->created_at = time();
            $model->created_by = 1;
            $model->modified_by = 1;
            $model->modified_at = time();
            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'Timesheet inserted successfully');
                AppLogging::addLog("Timesheet added", 'success', 'application.timesheet.main');
            } else {
                dd($model->errors);
            }
            $this->redirect(Yii::app()->baseUrl . "/site/index");
        }
    }

    public function actionAdd() {

        $model = new Timesheet();
        $types = Yii::app()->getRequest()->getParam('types');
        $userContracts = AppContract::getEmployeeContracts(Yii::app()->user->id, false, $types);
        $tSheetTypes = AppTimeSheet::getTimesheetType();
        $tSheetType = '';
        $contract = null;

        $timeIn = Yii::app()->getRequest()->getPost("timeIn");
        $timeOut = Yii::app()->getRequest()->getPost("timeOut");


        if (Yii::app()->getRequest()->getParam('contract') != '') {
            $contract = Contract::model()->findByPk(Yii::app()->getRequest()->getParam('contract'));

            if ($contract != null) {
                $tSheetType = $contract->timesheetTypeSel;
            }
        }



        if (count($_POST) > 0) {

            $model->attributes = Yii::app()->getRequest()->getPost("Timesheet");
            $tempTimeEntry = $model->start_time;
            $error = '';
            $parent = null;
//$response['status'] = true;
//                $response['reason'] = '';
//                if ($types == 'out') {
//                   $response['data'] = $this->createAbsoluteUrl('/main/index/id/1/current/1');
//                } else if ($types == 'in') {
//                   $response['data'] = $this->createAbsoluteUrl('index');
//                } else {
//                   $response['data'] = $this->createAbsoluteUrl('/main/index/id/2/current/1');
//                }
//                
//                AppSetting::generateAjaxResponse($response);

            if ($contract->timesheetTypeSel == "daily" && Yii::app()->getRequest()->getPost("timeIn") != "" && Yii::app()->getRequest()->getPost("timeOut") != "") {


                $model->start_time = DateTime::createFromFormat('d/m/Y h:i a', $tempTimeEntry . ' ' . $timeIn)->getTimestamp(); //strtotime($tempTimeEntry . ' ' . $timeIn);
                $model->end_time = DateTime::createFromFormat('d/m/Y h:i a', $tempTimeEntry . ' ' . $timeOut)->getTimestamp(); //strtotime($tempTimeEntry . ' ' . $timeOut);


                /**                 * * Restrict For Already Exist * */
                $checkExist = AppTimeSheet::checkAlreadyExist($tempTimeEntry, $tempTimeEntry, $contract, 'daily');


                if ($checkExist != null && isset($checkExist['status']) && $checkExist['status'] == true) {
                    $sheetModel = $checkExist['model'];/**                     * */
                    $error = $checkExist['error'];
                } elseif ($model->start_time > $model->end_time) {
                    $error = "Start Time cannot be greater than End Time";
                } else {

                    $parent = AppTimeSheet::getParentTSheet($contract->id, date('m/d/Y', DateTime::createFromFormat('d/m/Y', $tempTimeEntry)->getTimestamp()));

                    if ($parent != null)
                        $model->parent = $parent->id;
                }
            } else {
                $timeInput = explode(" to ", $tempTimeEntry);

                $checkExist = AppTimeSheet::checkAlreadyExist($timeInput[0], $timeInput[1], $contract, 'custom');

                if ($checkExist != null && isset($checkExist['status']) && $checkExist['status'] == true) {
                    $sheetModel = $checkExist['model'];/**                     * */
                    $error = $checkExist['error'];
                }
                $model->start_time = DateTime::createFromFormat('d/m/Y H:i:s', $timeInput[0] . " 00:00:00")->getTimestamp(); //strtotime($timeInput[0]);
                $model->end_time = DateTime::createFromFormat('d/m/Y H:i:s', $timeInput[1] . " 00:00:00")->getTimestamp(); //strtotime($timeInput[1]);
            }
            $model->created_by = AppUser::getUserId();
            $model->modified_by = AppUser::getUserId();
            $model->created_at = time();
            $model->modified_at = time();

            if ($contract->timesheetTypeSel != null)
                $model->type = $contract->timesheetTypeSel;

            $model->id = AppInterface::getUniqueId();

            if (Yii::app()->getRequest()->getPost('type') != 0 && !empty($model->contract_id)) {
                $type = Yii::app()->getRequest()->getPost('type');

                if ($contract != null) {
                    $contract->timesheetTypeSel = $type;
                    $contract->update();
                }
            }

            if ($error == '' && $model->insert()) {
                if ($model->type == 'daily') {
                    $setting = AppContract::getContractSettingsByKey($model->contract_id, 'regular');
                    if ($setting != null)
                        $_POST['hours'][$setting->id] = AppTimeSheet::calHrsDiff($model->start_time, $model->end_time); /// SEC TO HOUR   
                }
                if (isset($_POST['hours']))
                    $model->addPayrateEntries($_POST['hours']); /// Payrate Entries Added

                if (isset($_POST['items'])) {
                    $model->addPayrateEntries($_POST['items'], true); /// Items added
                }
                Yii::app()->user->setFlash('success', 'Timesheet has been successfully Added');

                $not_result = AppInterface::notification($contract->approver_id, 6, $model->id, $model->contract->id, $model->contract->user_id);
                $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
                $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;
                $notifier = new Notification();
                $notification_count = $notifier->getUserNotificationUnseenCount($contract->approver_id);
                $notification = Yii::app()->getModule('notification');
//                $notification->send(
//                        array(
//                            'push' => array('server_notification',
//                                array('id' => $contract->approver_id,
//                                    'msg' => sprintf($not_result->notificationType->notice, $contract->user->first_name, AppUser::getCurrentUser()->first_name), 'url' => $url, 'logo' => $logo, 'count' => $notification_count)
//                            )
//                ));

                AppLogging::addLog('Timesheet has been successfully Added', 'success', 'application.timesheet.controller.main.add');
                
                $response['status'] = true;
                $response['reason'] = '';
                if ($types == 'out') {
                   $response['data'] = $this->createAbsoluteUrl('/main/index/id/1/current/1');
                } else if ($types == 'in') {
                   $response['data'] = $this->createAbsoluteUrl('index');
                } else {
                   $response['data'] = $this->createAbsoluteUrl('/main/index/id/2/current/1');
                }
                $response['id'] = $model->id;
                $response['contract_id'] = $model->contract_id;
                AppSetting::generateAjaxResponse($response);
                
            } else {
                AppLogging::addLog('Unable to Add Timesheet Entry - ' . $error, 'error', 'application.timesheet.controller.main.add');
                Yii::app()->user->setFlash('error', 'Unable to Add Timesheet Entry - ' . $error);
                $model->start_time = $tempTimeEntry;
                $response['status'] = false;
                AppSetting::generateAjaxResponse($response);
            }
        } else {
            $model->contract_id = Yii::app()->getRequest()->getParam('contract');
            $model->contract = $contract;
        }



        /*         * *** User Contract Check ** */
//        if ($userContracts == null || count($userContracts) == 0) {
//            Yii::app()->user->setFlash('error', "No Contract has been found");
//        } /*         * ********* */

        $this->renderAddPageJs();
        $this->render('add', array(
            'model' => $model,
            'notes' => array(),
            'contracts' => $userContracts,
            'selectedType' => $tSheetType,
            'tSheetTypes' => $tSheetTypes,
            'payrateSettings' => AppContract::getContractSettings($model->contract_id, 'payrate'),
            'itemSettings' => AppContract::getContractSettings($model->contract_id, 'item'),
        ));
    }

    public function actionEdit() {

        if (Yii::app()->getRequest()->getParam('entry') != '') {

            $model = Timesheet::model()->findByPk(Yii::app()->getRequest()->getParam('entry'));

            $userContracts = AppContract::getEmployeeContracts($model->contract->user->id);
            $tSheetTypes = AppTimeSheet::getTimesheetType();
            $tSheetType = $model->type;
            $tempStartTime = $model->start_time;
            $error = '';
            if (count($_POST) < 1) {
                $return_url = Yii::app()->getRequest()->getUrlReferrer();
            }
            if (count($_POST) > 0) {

                $model->attributes = Yii::app()->getRequest()->getPost("Timesheet");
                $tempTimeEntry = $model->start_time;

                $return_url = Yii::app()->getRequest()->getUrlReferrer();
                if ($model->contract->timesheetTypeSel == "daily" && Yii::app()->getRequest()->getPost("timeIn") != "" && Yii::app()->getRequest()->getPost("timeOut") != "") {
                    $timeIn = Yii::app()->getRequest()->getPost("timeIn");
                    $timeOut = Yii::app()->getRequest()->getPost("timeOut");

                    $model->start_time = DateTime::createFromFormat('d/m/Y H:i a', $tempTimeEntry . ' ' . $timeIn)->getTimestamp(); //strtotime($tempTimeEntry . ' ' . $timeIn);
                    $model->end_time = DateTime::createFromFormat('d/m/Y H:i a', $tempTimeEntry . ' ' . $timeOut)->getTimestamp(); //strtotime($tempTimeEntry . ' ' . $timeOut);
                    /*                     * ** Restrict For Already Exist * */
                    $criteria = new CDbCriteria;
                    $criteria->addCondition(" t.start_time BETWEEN :startTime AND :endTime AND t.contract_id = :contractId AND t.id <> :entryId  ");
                    $criteria->params = array(":startTime" => DateTime::createFromFormat('d/m/Y H:i:s', $tempTimeEntry . ' 00:00:00')->getTimestamp(),
                        ":endTime" => DateTime::createFromFormat('d/m/Y H:i:s', $tempTimeEntry . ' 23:59:59')->getTimestamp(), //strtotime($tempTimeEntry . ' 23:59:59'),
                        ":contractId" => $model->contract->id,
                        ":entryId" => $model->id
                    );
                    $checkDayTSheet = Timesheet::model()->count($criteria);

                    if ($checkDayTSheet > 0) {
                        $sheetModel = Timesheet::model()->find($criteria); /*                         * * */

                        $error = "Timsheet with this date " . $tempTimeEntry . " is already Entered. <a href='" . Yii::app()->createUrl('/contract/timesheet/main/edit/', array('entry' => $sheetModel->id)) . "'>Click Here To Edit</a>";
                    } elseif ($model->start_time > $model->end_time) {
                        $error = "Start Time cannot be greater than End Time";
                    }
                } elseif (strpos($model->start_time, 'to') !== false) {
                    $timeInput = explode(" to ", $tempTimeEntry);
                    $model->start_time = DateTime::createFromFormat('d/m/Y', $timeInput[0])->getTimestamp(); //strtotime($timeInput[0]);
                    $model->end_time = DateTime::createFromFormat('d/m/Y', $timeInput[1])->getTimestamp(); //strtotime($timeInput[1]);
                }


                $model->modified_by = AppUser::getUserId();
                $model->modified_at = time();


                if ($error == '' && $model->update()) {

                    /*                     * *Updating Payrates & Hours Together ** */
                    $rateArr = array();
                    $hoursArr = array();
                    $itemsArr = array();
                    $itemvalArr = array();
                    if (isset($_POST['rates']) && count($_POST['rates']) > 0)
                        $rateArr = $_POST['rates'];
                    if (isset($_POST['item_value']) && count($_POST['item_value']) > 0)
                        $itemvalArr = $_POST['item_value'];

                    if ($model->type == 'daily') {
                        $payrate = AppTimeSheet::getPayrateByKey($model, 'regular');


                        if ($payrate != null)
                            $_POST['hours'][$payrate->id] = AppTimeSheet::calHrsDiff($model->start_time, $model->end_time); /// SEC TO HOUR   
                    }

                    if (isset($_POST['hours']) && count($_POST['hours']) > 0)
                        $hoursArr = $_POST['hours'];
                    if (isset($_POST['items']) && count($_POST['items']) > 0)
                        $itemsArr = $_POST['items'];

                    $model->updatePayrateEntries($rateArr, $hoursArr);
                    $model->updatePayrateEntries($itemvalArr, $itemsArr, 1);
                    /**/

                    Yii::app()->user->setFlash('success', 'Timesheet has been successfully Updated');

                    $this->redirect(Yii::app()->getRequest()->getPost("return_url"));
                } else {
                    Yii::app()->user->setFlash('error', 'Unable to Update Timesheet - ' . $error);
                    AppLogging::addLog('Unable to Update Timesheet - ' . $error, 'error', 'application.timesheet.conrtoller.edit');
                    $model->start_time = $tempTimeEntry;
                }
            } else if ($model->contract->timesheetTypeSel == "daily") {
                $model->start_time = date('d/m/Y', $tempStartTime);
            } else {
                $model->start_time = date('d/m/Y', $model->start_time) . ' to ' . date('d/m/Y', $model->end_time);
            }

            if (!isset($tSheetType))
                $tSheetType = $model->contract->timesheetTypeSel;
            $model->overtime = number_format($model->overtime, 1);
            $model->time = number_format($model->time, 1);
            $notes = TimesheetNotes::model()->findAllByAttributes(array("user_timing_id" => $model->id));
            $this->renderAddPageJs();
            $this->render('edit', array('model' => $model, 'notes' => $notes, 'contracts' => $userContracts, 'selectedType' => $tSheetType, 'tSheetTypes' => $tSheetTypes, 'tempStartTime' => $tempStartTime, 'payrateSettings' => AppContract::getContractSettings($model->contract_id, 'payrate'), 'return_url' => $return_url));
        } else {
            Yii::app()->user->setFlash('error', 'Entry Not Found');
            AppLogging::addLog('Entry Not Found' . $error, 'error', 'application.timesheet.controller.main.edit');
            $this->redirect(Yii::app()->getRequest()->getPost("return_url"));
        }
    }

    public function actionInstantapprove($tid) {
        if ($timesheet = Timesheet::model()->findByPk($tid)) {
            $timesheet->status = 'approved';
            $timesheet->save();
            Yii::app()->user->setFlash('success', 'Timesheet successfully approved.');
        } else {
            Yii::app()->user->setFlash('error', 'System could not find the requested Timesheet');
        }
        $this->redirect(Yii::app()->request->urlReferrer);
    }

    private function renderAddWizardJs() {

        $getContractUrl = $this->createUrl("/timesheet/main/getTypeFrmContrct");
        $script = <<<EOL
            jQuery(document).ready(function(){
                 /**** Add Timesheet Wizard JS ****/
                    jQuery(".contractDD").on("change",function(){
                         var ele = jQuery(this);
                         if(ele!=null && ele.val()!=''){

                             $.ajax({
                                    type: "POST",
                                    url: '{$getContractUrl}',
                                    data: {'contractId':ele.val()},
                                    success: function(response){
                                        if(response.status==true){
                                            //jQuery('.tsheetDD').hide();
                                            jQuery('.tsheetDD option:selected').removeAttr('selected');
                                            jQuery('.tsheetDD').find('option:contains("'+response.data+'")').attr("selected","selected");
                                            //jQuery('.tsheetLabel').text(response.data);
                                            //jQuery('.tsheetLabel').show();
                                        }else{
                                            jQuery('.tsheetDD option:selected').removeAttr('selected');
                                            jQuery('.tsheetDD').show();
                                            jQuery('.tsheetLabel').hide();
                                            jQuery('.tsheetLabel').val('');
                                    
                                        }
                                    },
                                    dataType: 'json'
                                  });

                         }

                    });
            });
           
           
EOL;

        Yii::app()->clientScript->registerScript('addWizard-js', $script, CClientScript::POS_END);
    }

    public function actionDispatchCopy() {
        $response = null;

        if (count($_GET) > 0 && isset($_GET['entry'])) {


            $response = AppTimeSheet::dispatchTimeSheet(array($_GET['entry']));


            if ($response['status'] == true) {
                AppLogging::addLog('Successfully Copied to Payroll Dispatch', 'success', 'application.timesheet.controller.main');
                Yii::app()->user->setFlash('success', $response['reason']);
                $this->redirect(array('/payroller/main/DispatchPayroll'));
            } else {
                AppLogging::addLog($response['reason'], 'error', 'application.timesheet.controller.main');
                Yii::app()->user->setFlash('error', $response['reason']);
            }
        } else {
            Yii::app()->user->setFlash('error', 'Timesheet Id is missing');
        }

        if (isset($_SERVER['HTTP_REFERER']))
            $this->redirect($_SERVER['HTTP_REFERER']);
        else
            $this->redirect(array('timsheet/main/index'));
    }

    /*     * ** Ajax Request For Card Compilation * */

    public function actionGetOne() {
        $this->layout = "//layouts/blank";
        $id = Yii::app()->getRequest()->getPost("id");
        $model = $this->loadModel($id);
        $response = array('status' => false, 'data' => '', 'reason' => 'Not a Valid Ajax Request or Post Data is Missing');
        $reciepts = QuickTsReciepts::model()->findAllByAttributes(array('ref_id' => $id));
        $reciept_files = array();
        $i = 0;
        foreach ($reciepts as $item) {
            $reciept_files[$i] = $item->reciept_file;
            $i++;
        }

        $contact = Contact::model()->findByPk($model->contact_id);
        $currency = Currency::model()->findByPk($model->currency_id);
        $response['status'] = true;
        $response['reason'] = '';

        $response['data'] = $this->render('view', array(
            'model' => $this->loadModel($id),
            'contact' => $contact,
            'currency' => $currency,
            'files' => $reciept_files,
                ), true);
        AppSetting::generateAjaxResponse($response);
    }

    public function GetTimeCardDetails($entryId, $is_view = true) {
//        $this->layout = "//layouts/blank";
        $response = array('status' => false, 'data' => '', 'reason' => 'Not a Valid Ajax Request or Post Data is Missing');

        if ($entryId != "") {
            $parent = Timesheet::model()->findByPk($entryId);
            $details = AppTimeSheet::compileCardDetail($entryId);
            $mergeChilds = null;
            $notes = TimesheetNotes::model()->findAllByAttributes(array("user_timing_id" => $entryId));
            $breaks = UserLunch::model()->findAllByAttributes(array("user_timing_id" => $entryId));

            if ($parent->merge_status == 1)
                $childs = AppTimeSheet::getTimeSheets(array('size' => '1000', 'old_parent' => $entryId, 'sort' => ' t.start_time ASC '));
            else {
                /*                 * * Inherited Childs * */
                $childs = AppTimeSheet::getTimeSheets(array('size' => '1000', 'parent' => $entryId, 'sort' => ' t.start_time ASC ', 'childStatus' => 1));
                /*                 * * Merged Childs * */
                $mergeChilds = AppTimeSheet::getTimeSheets(array('size' => '1000', 'parent' => $entryId, 'sort' => ' t.start_time ASC ', 'childStatus' => 2));
            }
            if (isset($childs['dataSet']) && count($childs['dataSet']) > 0 && $is_view) {
                $response['status'] = true;
                $response['reason'] = '';
                $response['data'] = $this->renderPartial('timecardentries', array_merge(array('parent' => $parent), array('inChild' => $childs), array('breaks' => $breaks), array('notes' => $notes), array('merChild' => $mergeChilds), array('detail' => $details)), true);
            } else if (!$is_view) {
                $response['data'] = array_merge(array('parent' => $parent), array('inChild' => $childs), array('breaks' => $breaks), array('notes' => $notes), array('merChild' => $mergeChilds), array('detail' => $details));
                $response['status'] = true;
            } else {
                $response['status'] = false;
                $response['reason'] = "No Record Found";
            }
        }
        return $response;
    }

    public function actionGetSheetCard() {
        $this->layout = "//layouts/blank";
        $response = array('status' => false, 'data' => '', 'reason' => 'Not a Valid Ajax Request or Post Data is Missing');
        if (Yii::app()->getRequest()->isAjaxRequest && Yii::app()->getRequest()->getPost("entryId") != "") {

            $details = AppTimeSheet::compileCardDetail(Yii::app()->getRequest()->getPost("entryId"));
            if (isset($details['employeeId'])) {
                $response['status'] = true;
                $response['reason'] = '';
                if (AppUser::isUserAdmin()) {
                    $details['canSendInvoice'] = true;
                } else {
                    $details['canSendInvoice'] = false;
                }

                $response['data'] = $this->render('timeCard', array('tdetails' => $details), true);
            } else {
                $response['status'] = false;
                $response['reason'] = "No Record Found";
            }
        }

        AppSetting::generateAjaxResponse($response);
    }

    /*     * ************************************** */

    /*     * ** Ajax Request for SubEntries * */

    public function actionGetSubEntries() {
        $data = AppTimeSheet::getTimecard(Yii::app()->getRequest()->getPost("parent"));
        if (isset($data)) {
            $childs = $data['childs'];
            $mergeChilds = $data['mergeChilds'];
            $parent = $data['parent'];
            $breaks = $data['breaks'];
            $notes = $data['notes'];
            $details = $data['details'];
            if (isset($childs['dataSet']) && count($childs['dataSet']) > 0) {
                $response['status'] = true;
                $response['reason'] = '';
                $response['data'] = $this->renderPartial('childEntries', array_merge(array('parent' => $parent), array('inChild' => $childs), array('breaks' => $breaks), array('notes' => $notes), array('merChild' => $mergeChilds), array('detail' => $details)), true);
            } else {
                $response['status'] = false;
                $response['reason'] = "No Record Found";
            }
            AppSetting::generateAjaxResponse($response);
        } else {
            echo "No Record Found.....";
        }
    }

    private function renderAddPageJs() {

        $getContractUrl = $this->createUrl("/timesheet/main/add/contract");
        $minDate = date(AppInterface::getdateformat(), strtotime(date('m/d/Y') . ' - 1 month'));
        $maxDate = date(AppInterface::getdateformat(), strtotime(date('m/d/Y')));
        $script = <<<EOL
            jQuery(document).ready(function(){
                 /**** Add Timesheet Wizard JS ****/
                    jQuery(".autoPostDD").on("change",function(){
                           if(jQuery(this).val()!=0) 
                           window.location.href = '{$getContractUrl}/'+jQuery(this).val();
                    });
                    
                    jQuery('#timesheet-form').on('submit',function(){
                          
                           if(jQuery("#Timesheet_start_time").val()==null || jQuery("#Timesheet_start_time").val()=='')
                           {
                             alert("* Please Select Start & End Date");
                             return false;
                           }
                           
                           return true;
                           
                    });          
                 
                                    
                 jQuery('.dRngPicker').daterangepicker({
//                    timePicker: true,
//                    timePickerIncrement: 30,
                    minDate: '{$minDate}',
                    //maxDate:'{$maxDate}',
                    format: 'DD/MM/YYYY',
                    opens: 'left',
                    separator: ' to '
                  }, function(start, end, label) {
                    console.log(start.toISOString(), end.toISOString(), label);
                  });          
                 jQuery('.datePicker').datepicker({
			format: 'dd/mm/yyyy',
                        todayHighlight: true,
                        autoclose: true,
                        endDate:'{$maxDate}'
                 });
                   jQuery('.disableEle').attr('readonly', 'readonly'); 
                   jQuery('.numPicker').bootstrapNumber({
                       upClass: 'danger',
                       downClass: 'success'

                    });
                    
                  
                    
                  $(".timepickerIn").timepicker({
                    showInputs: false,
                    defaultTime: '09:00 AM'
                });
                    
                  $(".timepickerOut").timepicker({
                    showInputs: false,
                    defaultTime: '06:00 PM'
                });
                    
                    
                    
                    
 
                           
            });
           
           
EOL;

        Yii::app()->clientScript->registerScript('addWizard-js', $script, CClientScript::POS_END);
    }

    public function actionChangeStatus() {

        if (Yii::app()->getRequest()->getParam('entry') != "" || Yii::app()->getRequest()->getPost('entry') != "") {
            $entryId = ((Yii::app()->getRequest()->getPost('entry') != "") ? Yii::app()->getRequest()->getPost('entry') : Yii::app()->getRequest()->getParam('entry'));
            $action = ((Yii::app()->getRequest()->getPost('action') != "") ? Yii::app()->getRequest()->getPost('action') : Yii::app()->getRequest()->getParam('action'));
            $model = Timesheet::model()->findByPk($entryId);

            if ($model != null && $action != "") {
                $model->status = $action;
                $model->modified_at = time();
                $model->modified_by = AppUser::getUserId();
                if ($model->update()) {

                    switch ($action) {
                        case 'closed':


                            AppTimeSheet::notifyUsersOnClosed($model);

                            break;
                        case 'decline':
                            AppTimeSheet::notifyUsersOnDecline($model);
                            break;


                        case 'approved':

                            AppTimeSheet::notifyUsersOnApproved($model);
                            break;
                    }


                    AppLogging::addLog("Timesheet Status has been Succesfully Changed to " . strtoupper($action), 'success', 'application.timesheet.controller.main.changeStatus');
                    Yii::app()->user->setFlash('success', "Timesheet Status has been Succesfully Changed to " . strtoupper($action));
                } else {
                    AppLogging::addLog("Unable to Update the Timesheet", 'error', 'application.timesheet.controller.main.changeStatus');
                    Yii::app()->user->setFlash('error', "Unable to Update the Timesheet");
                }

                if (isset($_SERVER['HTTP_REFERER'])) {

                    $this->redirect($_SERVER['HTTP_REFERER']);
                } else
                    $this->redirect(array('timesheet/main'));
            }else {
                Yii::app()->user->setFlash('error', "Timesheet Not Found or Action is Undefined");
            }
        }
    }

}
