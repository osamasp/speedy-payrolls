<?php

class QuickTimesheetController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'GetView', 'completed'),
                'roles' => array('employee', 'admin'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'export', 'resendinvoice', 'invoice', 'sendinvoice', 'deleteReceipt', 'completedEmail', 'getContactDetails'),
                'roles' => array('employee', 'admin'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionGetView() {
        $this->layout = "//layouts/blank";
        $id = Yii::app()->getRequest()->getPost("id");
        $model = $this->loadModel($id);
        $response = array('status' => false, 'data' => '', 'reason' => 'Not a Valid Ajax Request or Post Data is Missing');
        $reciepts = QuickTsReciepts::model()->findAllByAttributes(array('ref_id' => $id));
        $reciept_files = array();
        $i = 0;
        foreach ($reciepts as $item) {
            $reciept_files[$i] = $item->reciept_file;
            $i++;
        }

        $contact = Contact::model()->findByPk($model->contact_id);
        $currency = Currency::model()->findByPk($model->currency_id);
        $response['status'] = true;
        $response['reason'] = '';

        $response['data'] = $this->render('view', array(
            'model' => $this->loadModel($id),
            'contact' => $contact,
            'currency' => $currency,
            'files' => $reciept_files,
                ), true);
        AppSetting::generateAjaxResponse($response);
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new QuickTimesheet;
        $contacts = AppCompany::getCompanyContacts();
        $currencies = Currency::model()->findAll();

        if (isset($_POST['QuickTimesheet'])) {
//            if (AppCompany::getQuickTimesheetByDate(DateTime::createFromFormat('d/m/Y H:i:s', $_POST['QuickTimesheet']['date'] . " 00:00:00")->getTimestamp())) {
//                Yii::app()->user->setFlash('error', "Timesheet with this date already exist");
//                $this->redirect(array('create'));
//            } else {
                $model->attributes = $_POST['QuickTimesheet'];
                if (isset($_POST['contacts']) && $_POST['contacts'] != 'out') {
                    $select_email = Contact::model()->findByAttributes(array('id' => $_POST['contacts']))->email;
                    $select_name = Contact::model()->findByAttributes(array('id' => $_POST['contacts']))->name;
                    $contact_id = $_POST['contacts'];
                } else {
                    $select_email = $_POST['contact-email'];
                    $select_name = $_POST['contact-name'];
                    $contact = AppCompany::addContact(array('name' => $select_name, 'email' => $select_email, 'type' => 'Individual', 'company_id' => AppUser::getUserCompany(AppUser::getUserId(), false)->id, 'created_by' => AppUser::getUserId(), 'modified_by' => AppUser::getUserId(), 'created_at' => time(), 'modified_at' => time(), 'status' => 1, 'address' => $_POST['client_add'], 'country' => $_POST['client_country'], 'post_code' => $_POST['client_pc'], 'city' => $_POST['client_city']));

                    if (isset($contact)) {
                        $contact_id = $contact->id;
                    } else {
                        Yii::app()->user->setFlash('error', 'An error occured while creating invoice');
                        $this->redirect(array('index'));
                    }
                }
                $model->contact_id = $contact_id;
                $model->date = DateTime::createFromFormat('d/m/Y H:i:s', $_POST['QuickTimesheet']['date'] . " 00:00:00")->getTimestamp();
                $model->hours = strtotime($_POST['QuickTimesheet']['hours']);
                $model->total = $_POST['QuickTimesheet']['total'];
                $model->job_title = $_POST['QuickTimesheet']['job_title'];
                $model->job_reference = $_POST['QuickTimesheet']['job_reference'];
                $model->job_description = $_POST['QuickTimesheet']['job_description'];
                $model->status = 'Open';
                $model->company_id = AppUser::getUserCompany(AppUser::getUserId(), false)->id;
                $model->currency_id = $_POST['currency'];
                $model->created_at = time();
                $model->rate = $_POST['QuickTimesheet']['rate'];
                $model->modified_at = 0;
                $model->created_by = AppUser::getUserId();
                $model->modified_by = 0;
                $model->id = AppInterface::getUniqueId();
                $temp = CUploadedFile::getInstances($model, 'reciept_file');
                if ($model->save()) {
                    if ($_FILES['QuickTimesheet']['name']['reciept_file'][0]) {
                        for ($i = 0; $i < count($_FILES['QuickTimesheet']['name']['reciept_file']); $i++) {
                            $reciepts = new QuickTsReciepts;

                            if (!is_dir(Yii::getPathOfAlias('webroot') . '/uploads/receipts')) {
                                mkdir(Yii::getPathOfAlias('webroot') . '/uploads/receipts');
                                chmod((Yii::getPathOfAlias('webroot') . '/uploads/receipts'), 0755);
                                // the default implementation makes it under 777 permission, which you could possibly change recursively before deployment, but here's less of a headache in case you don't
                            }
                            $images_path = realpath(Yii::app()->basePath . '/../uploads/receipts');
                            $filename = explode('.', $temp[$i]->name);
                            $filename[0] = AppInterface::getFilename();
                            $newfilename = $filename[0] . '.' . $filename[1];
                            $temp[$i]->saveAs($images_path . '/' . $newfilename);
                            $reciepts->reciept_file = $newfilename;
                            $reciepts->id = AppInterface::getUniqueId();
                            $reciepts->ref_id = $model->id;
                            $reciepts->created_by = AppUser::getUserId();
                            $reciepts->created_at = time();
                            $reciepts->save();
                        }
                    }
                    for ($x = 1; $x <= $_POST['count']; $x++) {
                        $expenses = new QuickTsExpenses();
                        $expenses->expense_name = $_POST['expense_name' . $x];
                        $expenses->expense_value = $_POST['expense_value' . $x];
                        $expenses->comment = $_POST['comment' . $x];
                        $expenses->id = AppInterface::getUniqueId();
                        $expenses->quick_time_id = $model->id;
                        $expenses->created_at = time();
                        $expenses->created_by = AppUser::getUserId();
                        $expenses->modified_at = 0;
                        $expenses->modified_by = 0;
                        $expenses->save();
                    }
                    Yii::app()->user->setFlash('success', "1 Way Timesheet has been addedd successfully.");
                    $this->redirect(array('index'));
                }
//            }
        }

        $this->render('create', array(
            'model' => $model,
            'contacts' => $contacts,
            'currencies' => $currencies,
        ));
    }

    public function actionExport() {
        $webroot = Yii::getPathOfAlias('webroot');
        if (!is_dir($webroot . '/uploads/timesheets')) {
            mkdir($webroot . '/uploads/timesheets');
            chmod(($webroot . '/uploads/timesheets'), 0755);
            // the default implementation makes it under 777 permission, which you could possibly change recursively before deployment, but here's less of a headache in case you don't
        }
        $labels = array();
        $file = $webroot . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'timesheets' . DIRECTORY_SEPARATOR . 'timesheet-' . date('d-m-Y-H-i') . '.csv';
        $handle = fopen($file, 'w');
        $user_id = AppUser::getUserId();
        if (isset($_POST['status']))
            $status = $_POST['status'];
        else {
            $status = "Emailed";
        }
        $result = array();
        $i = 0;
//        $data = AppCompany::getApprovalList();
        $data = AppCompany::getCurrentTimesheetsData($status, isset($_POST['export_fields']) ? $_POST['export_fields'] : null, isset($_POST['entry']) ? $_POST['entry'] : null);
//        dd($data);
        foreach ($data as $item) {
            $result[$i] = $item;
            if (isset($item['rate']) && isset($item['hours'])){
                if(isset($_POST['export_fields'])){
                    if(in_array('hourly_total', $_POST['export_fields']))
                        $result[$i]['hourly_total'] = $item['rate'] * (date("h",$item['hours'])+date("i",$item['hours']));
                } else{
                    $result[$i]['hourly_total'] = $item['rate']*(date("h",$item['hours'])+date("i",$item['hours']));
                }
            }
            if (isset($item['id'])) {
                $expenses = QuickTsExpenses::model()->findAllByAttributes(array('quick_time_id' => $item["id"]));
                $expense_total = 0;
                $result[$i]['expense_details'] = "";
                foreach ($expenses as $expense) {
                    $expense_total += $expense->expense_value;
                    if (isset($_POST['export_fields'])) {
                        if (in_array('expense_details', $_POST['export_fields'])) {
                            $result[$i]['expense_details'] .= $expense->expense_name . " : " . $expense->expense_value . " : " . $expense->comment . "-";
                        }
                    } else{
                        $result[$i]['expense_details'] .= $expense->expense_name." : ".$expense->expense_value." : ".$expense->comment."-";
                    }
                }
                $result[$i]["expense"] = $expense_total;
            }
            
            $i++;
        }
//        dd($result);
        if (isset($_POST['export_fields'])) {
            $last_element = end($_POST['export_fields']);
            foreach ($_POST['export_fields'] as $item) {
                array_push($labels, $item);
            }
        } else {
            $labels = array('name', 'email', 'date', 'job_title', 'job_reference', 'job_description', 'hours', 'rate', 'expense_value', 'total', 'hourly_total', 'expense_details');
        }
        AppInterface::GenerateCSV($file, $labels, $result);
        fclose($handle);
        $curr_user = AppUser::getCurrentUser();
        $url = $this->createAbsoluteUrl('/uploads/timesheets/' . 'timesheet-' . date('d-m-Y-H-i') . '.csv');
        $this->layout = "//layout/blank";
        $response = array('status' => false, 'data' => '', 'reason' => 'Not a Valid Ajax Request or Post Data is Missing');
        $response['status'] = true;
        $response['reason'] = '';
        $response['url'] = $url;
        $response['data'] = $this->render('//user/employee/viewlist', array(
            'file' => $file,
            'name' => 'company_shift-' . date('d-m-Y-H-i') . '.csv',
            'url' => $url
                ), true);
        AppSetting::generateAjaxResponse($response);
        Yii::app()->request->sendFile(AppInterface::getFilename() . '.csv', file_get_contents($file));
    }

    public function actionGetContactDetails() {
        $contact_id = $_POST['contact_id'];
        $contact_details = Contact::model()->findByPk($contact_id);

        if ($contact_details) {
            $result['name'] = $contact_details->name;
            $result['email'] = $contact_details->email;
            $result['address'] = $contact_details->address;
            $result['post_code'] = $contact_details->post_code;
            $result['country'] = $contact_details->country;
            $result['city'] = $contact_details->city;
        } else {
            $result = null;
        }
        echo json_encode($result);
    }

    public function actionInvoice($id) {
        $this->layout = "//layouts/blank";
        $response = array('status' => false, 'data' => '', 'reason' => 'Not a Valid Ajax Request or Post Data is Missing');
        $model = QuickTimesheet::model()->findByPk($id);
        $contact_details = Contact::model()->findByPk($model->contact_id);
        $details = AppTimeSheet::setInvoiceData($id, $model, $contact_details);
        $temp = AppTimeSheet::actionCreatepdf($details);
        $attachement = Yii::getPathOfAlias('webroot') . '/uploads/invoicespdf/' . $details['company'] . '-' . $id . '.pdf';
        $invoice = $this->createAbsoluteUrl('/uploads/invoicespdf/' . $details['company'] . '-' . $id . '.pdf');
//        dd($invoice);
        $response['status'] = true;
        $response['reason'] = '';

        $response['data'] = $this->render('viewinvoice', array(
            'invoice' => $attachement,
            'url' => $invoice,
            'id' => $id,
            'contact_id' => $model->contact_id,
            'company' => $details["company"]
                ), true);
        AppSetting::generateAjaxResponse($response);
//        $mailer = new AppMailer();
//        $msg = "Invoice";
//        $mailer->prepareBody('invoice', array('INVOICE' => $msg));
//        $model->updateByPk($id, array('status' => 'Emailed'));
//        $response = $mailer->sendSesMailWithAttach(array('email' => $contact_details->email, 'name' => $contact_details->name), array('email' => 'support@speedypayrolls.com', 'name' => 'Support'), "Invoice Email", "Hello, <br/> Please find attachment.", $attachement, $details['company'] . '-' . $id . '.pdf');
//        Yii::app()->user->setFlash('success', 'Invoice sent to selected user.');
//        $this->redirect(array('index'));
    }

    public function actionResendInvoice($id, $attachment, $contact_id, $company) {
        $contact_details = Contact::model()->findByPk($contact_id);
        $mailer = new AppMailer();
        $msg = "Invoice";
        $mailer->prepareBody('invoice', array('INVOICE' => $msg));
        $response = $mailer->sendSesMailWithAttach(array('email' => $contact_details->email, 'name' => $contact_details->name), array('email' => 'support@speedypayrolls.com', 'name' => 'Support'), "Invoice Email", "Hello, <br/> Please find attachment.", $attachment, $company . '-' . $id . '.pdf');
        Yii::app()->user->setFlash('success', 'Invoice sent to selected user.');
        $this->redirect(array('completed'));
    }

    public function actionSendInvoice($id, $attachment, $contact_id, $company) {
        $contact_details = Contact::model()->findByPk($contact_id);
        $model = $this->loadModel($id);
        $mailer = new AppMailer();
        $msg = "Invoice";
        $mailer->prepareBody('invoice', array('INVOICE' => $msg));
        $model->updateByPk($id, array('status' => 'Emailed'));
        $response = $mailer->sendSesMailWithAttach(array('email' => $contact_details->email, 'name' => $contact_details->name), array('email' => 'support@speedypayrolls.com', 'name' => 'Support'), "Invoice Email", "Hello, <br/> Please find attachment.", $attachment, $company . '-' . $id . '.pdf');
        Yii::app()->user->setFlash('success', 'Invoice sent to selected user.');
        $this->redirect(array('index'));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);
//        $model->hours = $model->hours*60;$_POST['QuickTimesheet']['hours']
        $model->date = date('d/m/Y', $model->date);
        $contacts = AppCompany::getCompanyContacts();
        $currencies = Currency::model()->findAll();
        $reciepts = QuickTsReciepts::model()->findAllByAttributes(array('ref_id' => $id));
        $reciept_files = array();
        $i = 0;
        foreach ($reciepts as $item) {
            $reciept_files[$i] = $item->reciept_file;
            $i++;
        }
        $contact_details = Contact::model()->findByPk($model->contact_id);
        $expenses = QuickTsExpenses::model()->findAllByAttributes(array('quick_time_id' => $id));
        if (isset($_POST['QuickTimesheet'])) {
            if (isset($_POST['contacts']) && $_POST['contacts'] != 'out') {
                $contact = Contact::model()->findByAttributes(array('id' => $_POST['contacts']));
                $select_email = $contact->email;
                $select_name = $contact->name;
                $contact_id = $_POST['contacts'];
            } else {
                $select_email = $_POST['contact-email'];
                $select_name = $_POST['contact-name'];
                $contact = AppCompany::addContact(array('name' => $select_name, 'email' => $select_email, 'type' => 'Individual', 'company_id' => AppUser::getUserCompany()->id, 'created_by' => AppUser::getUserId(), 'modified_by' => AppUser::getUserId(), 'created_at' => time(), 'modified_at' => time(), 'status' => 1, 'address' => $_POST['client_add'], 'country' => $_POST['client_country'], 'post_code' => $_POST['client_pc'], 'city' => $_POST['client_city']));

                if (isset($contact)) {
                    $contact_id = $contact->id;
                } else {
                    Yii::app()->user->setFlash('error', 'An error occured while creating invoice');
                    $this->redirect(array('index'));
                }
            }


            if (AppCompany::addQuickTimesheet($_POST, $contact_id, $model) instanceof QuickTimesheet) {
                if ($_FILES['QuickTimesheet']['name']['reciept_file'][0]) {
                    AppCompany::saveQuickTimesheetReciepts($id, $model, $_FILES);
                }
                AppCompany::saveQuickTimesheetExpense($id, $_POST, $model);

                Yii::app()->user->setFlash('success', "1 Way Timesheet has been updated successfully.");
                $this->redirect(array('index'));
            }
        }

        $this->render('update', array(
            'model' => $model,
            'contacts' => $contacts,
            'currencies' => $currencies,
            'contact_details' => $contact_details,
            'expenses' => $expenses,
            'files' => $reciept_files,
        ));
    }

    public function actionCompletedEmail($id) {
        $response = array('status' => false, 'data' => '', 'reason' => 'Not a Valid Ajax Request or Post Data is Missing');
        $model = $this->loadModel($id);
        $contacts = AppCompany::getCompanyContacts();
        $currency = Currency::model()->findByPk($model->currency_id);
        $reciepts = QuickTsReciepts::model()->findAllByAttributes(array('ref_id' => $id));
        $reciept_files = array();
        $i = 0;
        foreach ($reciepts as $item) {
            $reciept_files[$i] = $item->reciept_file;
            $i++;
        }
        $contact_details = Contact::model()->findByPk($model->contact_id);
        $expenses = QuickTsExpenses::model()->findAllByAttributes(array('quick_time_id' => $id));

        if (isset($_POST["QuickTimesheet"])) {
            $company = AppUser::getUserCompany();

            if (isset($_POST['contacts']) && $_POST['contacts'] != 'out') {
                $select_email = Contact::model()->findByAttributes(array('id' => $_POST['contacts']))->email;
                $select_name = Contact::model()->findByAttributes(array('id' => $_POST['contacts']))->name;
                $contact_id = $_POST['contacts'];
                $contact_details = Contact::model()->findByPk($contact_id);
            } else {
                $select_email = $_POST['contact-email'];
                $select_name = $_POST['contact-name'];
                $contact = AppCompany::addContact(array('name' => $select_name, 'email' => $select_email, 'type' => 'Individual', 'company_id' => AppUser::getUserCompany(AppUser::getUserId(), false)->id, 'created_by' => AppUser::getUserId(), 'modified_by' => AppUser::getUserId(), 'created_at' => time(), 'modified_at' => time(), 'status' => 1, 'address' => $_POST['client_add'], 'country' => $_POST['client_country'], 'post_code' => $_POST['client_pc'], 'city' => $_POST['client_city']));

                if (isset($contact)) {
                    $contact_id = $contact->id;
                    $contact_details = Contact::model()->findByPk($contact_id);
                } else {
                    Yii::app()->user->setFlash('error', 'An error occured while creating invoice');
                    $this->redirect(array('index'));
                }
            }
            $details['company'] = $company['name'];
            $details['companyAddress'] = $company['address'];
            $details['post_code'] = $company['post_code'];
            $details['companyCity'] = $company['city'];
            $details['companyCountry'] = $company['country'];
            $details['logo'] = $company['logo'];
            $details['client_info'] = $contact_details;
            $model->hours = date('H.i', $model->hours);
            $details['hours'] = $model->hours;
            $currency = Currency::model()->findByPk($model->currency_id)->name;
            if ($currency == "GBP")
                $details['currency'] = chr(163);
            else if ($currency == "USD")
                $details['currency'] = "$";
            else if ($currency == "EURO")
                $details['currency'] = chr(128);
            $details['date'] = date('d/m/Y', $model->date);
            $details['total'] = $model->total;
            $details['rates'] = $model->rate;
            $details['expenses'] = $expenses;
            $details['contractor_name'] = $company['name'];
            $details['contractor_address'] = $company['address'];
            $details['vat_number'] = $company['vat_number'];
            $details['id'] = $id;
            $details['hourly_total'] = $model->hours * $model->rate;
            $expense_total = 0;
            foreach ($expenses as $expense)
                $expense_total += $expense->expense_value;
            $details['expense_total'] = $expense_total;
            $temp = AppTimeSheet::actionCreatepdf($details);
            $attachement = Yii::getPathOfAlias('webroot') . '/uploads/invoicespdf/' . $details['company'] . '-' . $id . '.pdf';

            $this->layout = "//layouts/blank";
            $invoice = $this->createAbsoluteUrl('/uploads/invoicespdf/' . $details['company'] . '-' . $id . '.pdf');
//        dd($invoice);
            $response['status'] = true;
            $response['reason'] = '';

            $response['data'] = $this->render('resend_viewinvoice', array(
                'invoice' => $attachement,
                'url' => $invoice,
                'id' => $id,
                'contact_id' => $model->contact_id,
                'company' => $details["company"]
                    ), true);
            AppSetting::generateAjaxResponse($response);
            return;
//            $mailer = new AppMailer();
//            $msg = "Invoice";
//            $mailer->prepareBody('invoice', array('INVOICE' => $msg));
//            $response = $mailer->sendSesMailWithAttach(array('email' => $contact_details->email, 'name' => $contact_details->name), array('email' => 'support@speedypayrolls.com', 'name' => 'Support'), "Invoice Email", "Hello, <br/> Please find attachment.", $attachement, $details['company'] . '-' . $id . '.pdf');
//            Yii::app()->user->setFlash('success', 'Invoice sent to selected user.');
//            $this->redirect(array('index'));
        }
        $this->render('cemail', array(
            'model' => $model,
            'contacts' => $contacts,
            'currency' => $currency,
            'contact_details' => $contact_details,
            'expenses' => $expenses,
            'files' => $reciept_files,
        ));
    }

    public function actionDeleteReceipt($name) {
        $reciept = QuickTsReciepts::model()->findByAttributes(array('reciept_file' => $name));
        QuickTsReciepts::model()->deleteAllByAttributes(array('reciept_file' => $name));
        Yii::app()->user->setFlash('success', 'Receipt deleted successfully.');
        $this->redirect($this->createUrl('/timesheet/quickTimesheet/update/id/' . $reciept->ref_id));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();
        Yii::app()->user->setFlash('success', "Successfully deleted.");

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = AppCompany::getNonbillableTimesheets(array('status' => 'Open'));
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionCompleted() {
        $dataProvider = AppCompany::getNonbillableTimesheets(array('status' => 'Emailed'));
        $this->render('completed', array('dataProvider' => $dataProvider));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new QuickTimesheet('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['QuickTimesheet']))
            $model->attributes = $_GET['QuickTimesheet'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return QuickTimesheet the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = QuickTimesheet::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param QuickTimesheet $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'quick-timesheet-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
