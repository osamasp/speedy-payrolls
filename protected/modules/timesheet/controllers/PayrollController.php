<?php

class PayrollController extends Controller {

    public function actionIndex() {
      
        $args = array('payroller'=>Yii::app()->user->id,'status'=>'approved');
        
        /*** Filter By Contract Timesheets ***/
        if(Yii::app()->getRequest()->getParam("contract")!=''){
            $args['contract'] = Yii::app()->getRequest()->getParam("contract");
        }
        
        $result = AppTimeSheet::getTimeSheets($args);
        $this->render('index', array('entries' => $result['dataSet'], 'pages' => $result['pages']));
    }
    
   
    
    
}
