<?php

class ApproveController extends Controller {

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index'),
                'roles' => array('payroller'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('update', 'index', 'decline', 'export'),
                'roles' => array('admin', 'super_admin', 'employee'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionExport() {
        $webroot = Yii::getPathOfAlias('webroot');
        if (!is_dir($webroot . '/uploads/approval_list')) {
            mkdir($webroot . '/uploads/approval_list');
            chmod(($webroot . '/uploads/approval_list'), 0755);
            // the default implementation makes it under 777 permission, which you could possibly change recursively before deployment, but here's less of a headache in case you don't
        }
        $file = $webroot . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'approval_list' . DIRECTORY_SEPARATOR . 'approval_list-' . date('d-m-Y-H-i') . '.csv';
        $handle = fopen($file, 'w');
        $user_id = AppUser::getUserId();

        $args = array('approver' => Yii::app()->user->id, 'status' => 'in-approval');
        /*         * * Filter By Contract Timesheets ** */
        if (Yii::app()->getRequest()->getParam("contract") != '') {
            $args['contract'] = Yii::app()->getRequest()->getParam("contract");
        }
        $result = AppTimeSheet::getTimeSheets($args);

        $data = array();
        $i = 0;
        foreach ($result['dataSet'] as $item) {
            if (isset($_POST['export_fields'])) {
                foreach ($_POST['export_fields'] as $item) {
                    if ($item == 'created_at') {
                        $data[$i]['created_at'] = $item->created_at;
                    } elseif ($item == 'title') {
                        $data[$i]['title'] = $item->title;
                    } elseif ($item == 'start_time') {
                        $data[$i]['start_time'] = $item->start_time;
                    } elseif ($item == 'end_time') {
                        $data[$i]['end_time'] = $item->end_time;
                    }
                }
                $i++;
            } else {
                $data[$i]['created_at'] = $item->created_at;
                $data[$i]['title'] = $item->title;
                $data[$i]['start_time'] = $item->start_time;
                $data[$i]['end_time'] = $item->end_time;
                $i++;
            }
        }

        AppInterface::GenerateCSV($file, array('created_at', 'title', 'start_time', 'end_time'), $data);
        fclose($handle);
//        $curr_user = AppUser::getCurrentUser();
        $url = $this->createAbsoluteUrl('/uploads/approval_list/' . 'approval_list-' . date('d-m-Y-H-i') . '.csv');
        $this->layout = "//layouts/blank";
        $response = array('status' => false, 'data' => '', 'reason' => 'Not a Valid Ajax Request or Post Data is Missing');
        $response['status'] = true;
        $response['reason'] = '';
        $response['url'] = $url;
        $response['data'] = $this->render('//user/employee/viewlist', array(
            'file' => $file,
            'name' => 'approval_list-' . date('d-m-Y-H-i') . '.csv',
            'url' => $url
                ), true);
        AppSetting::generateAjaxResponse($response);
        Yii::app()->request->sendFile(AppInterface::getFilename() . '.csv', file_get_contents($file));
    }

    public function actionIndex() {
        AppLogging::addLog('Viewing Timesheet For Approval', 'success', 'application.timesheet.controller.approve');
        $args = array('approver' => Yii::app()->user->id, 'status' => 'in-approval');
        /*         * * Filter By Contract Timesheets ** */
        if (Yii::app()->getRequest()->getParam("contract") != '') {
            $args['contract'] = Yii::app()->getRequest()->getParam("contract");
        }

        $result = AppTimeSheet::getTimeSheets($args);
//        dd($result);

        $this->render('index', array('entries' => $result['dataSet'], 'pages' => $result['pages']));
    }

    public function actionUpdate() {

        if (Yii::app()->getRequest()->getParam('entry') != "") {
            $model = Timesheet::model()->findByPk(Yii::app()->getRequest()->getParam('entry'));
            if ($model != null && isset($model->contract->approver_id) && $model->contract->approver_id == AppUser::getUserId()) {
                $model->status = 'approved';
                $model->modified_by = AppUser::getUserId();
                if ($model->update()) {
                    AppLogging::addLog("You have successfully approved the timesheet.", 'success', 'application.timesheet.controller.approve.update');

                    /*                     * * Email Notificaiton * */
                    AppTimeSheet::notifyUsersOnApproved($model);
                    /*                     * */

                    Yii::app()->user->setFlash("success", "You have successfully approved the timesheet.");
                    $this->redirect(array('index'));
                } else {
                    AppLogging::addLog("Unable to approve the timesheet.", 'error', 'application.timesheet.controller.approve.update');
                    Yii::app()->user->setFlash("error", "Unable to approve the timesheet.");
                    $this->redirect(array('index'));
                }
            } else {
                AppLogging::addLog("Timesheet Not Found / You are Not Approver for this Timesheet.", 'error', 'application.timesheet.controller.approve.update');

                Yii::app()->user->setFlash("error", "Timesheet Not Found / You are Not Approver for this Timesheet.");
                $this->redirect(array('index'));
            }
        } else {
            Yii::app()->user->setFlash("error", "Timesheet ID is missing.");
            $this->redirect(array('index'));
        }
    }

    public function actionDecline() {

        if (Yii::app()->getRequest()->getParam('entry') != "") {
            $model = Timesheet::model()->findByPk(Yii::app()->getRequest()->getParam('entry'));
            if ($model != null && isset($model->contract->approver_id) && $model->contract->approver_id == AppUser::getUserId()) {
                $model->status = 'rejected';
                $model->modified_by = AppUser::getUserId();
                if ($model->update()) {
                    Yii::app()->user->setFlash("success", "You have successfully decline the timesheet.");

                    /*                     * * Email Notificaiton * */
                    AppTimeSheet::notifyUsersOnDecline($model);
                    /*                     * */

                    $this->redirect(array('index'));
                } else {
                    Yii::app()->user->setFlash("error", "Unable to decline the timesheet.");
                    $this->redirect(array('index'));
                }
            } else {
                Yii::app()->user->setFlash("error", "Timesheet Not Found / You are Not Approver for this Timesheet.");
                $this->redirect(array('index'));
            }
        } else {
            Yii::app()->user->setFlash("error", "Timesheet ID is missing.");
            $this->redirect(array('index'));
        }
    }

}
