<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AppContract
 *
 * @author Usama Ayaz
 */
class AppTimeSheet extends CComponent {

    public static function getTimeSheet() {

        if (AppUser::isUserAdmin()) {
            $dataProvider = QuickTimesheet::model()->findAllByAttributes(array('status' => 'Open', 'company_id' => $company->id));
            $this->render('index', array(
                'dataProvider' => $dataProvider,
            ));
        } else {
            $dataProvider = QuickTimesheet::model()->findAllByAttributes(array('status' => 'Open', 'company_id' => $company->id, 'created_by' => AppUser::getUserId()));
            $this->render('index', array(
                'dataProvider' => $dataProvider,
            ));
        }
    }

    //method for webservices
    public static function getAllTimeSheets($company_id) {

        if (AppUser::isUserAdmin()) {
            $dataProvider = QuickTimesheet::model()->findAllByAttributes(array('status' => 'Open', 'company_id' => $company_id));
            return $dataProvider;
        } else {
            $dataProvider = QuickTimesheet::model()->findAllByAttributes(array('status' => 'Open', 'company_id' => $company_id, 'created_by' => AppUser::getUserId()));
            return $dataProvider;
        }
    }

    public static function getTimesheetType() {

        return array('daily', 'custom');
    }

    public static function canDownloadPayslip(&$entry) {
        $employeeid = $entry->payroll->timesheet->contract->user_id;
        $internalcontract = AppContract::getInternalContract($employeeid);
        if ($entry != null && ($entry->user_id == AppUser::getUserId() || $employeeid == AppUser::getUserId() || $internalcontract->company->admin->id == AppUser::getUserId()) && file_exists(Yii::app()->params['uploads']['payslip'] . $entry->file)) {
            return true;
        }
    }

    public static function actionCreatepdf($details) {
        $pdf = new SPPDF();
        $pdf->details = $details;
        $pdf->AddPage();

        $pdf->SetTextColor(0, 0, 0);
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->SetFont("Arial", "B", "13");
        $pdf->Write("4", "Client:");
        $pdf->SetFont("Arial", "", "12");
        $pdf->Cell('0', '0', $details['date'], '', '0', 'R');
        $pdf->Cell('0', '7', '', '', '1');
        $pdf->Write("4", $details['client_info']['name']);
        $pdf->Cell("0", "5", "", "", "1");
        $pdf->Write("4", $details['client_info']['address']);
        $pdf->Cell("0", "5", "", "", "1");
        $pdf->Write("4", $details['client_info']['city'] . " " . $details['client_info']['post_code']);
        $pdf->Cell("0", "5", "", "", "1");
        $pdf->Write("4", $details['client_info']['country']);
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->Write("4", "Currency :" . $details['currency']);
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->Write("4", "Hours : " . $details['hours']);
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->Write("4", "Rate :" . $details['rates']);
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->Write("4", "Hourly Total :" . $details['hourly_total']);
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->SetFont("Arial", "B", "12");
        $pdf->Cell('80', '10', 'Expense Name', '', '0', 'L');
        $pdf->Cell('30', '10', 'Expense Value', '', '0', 'C');
        $pdf->Cell('0', '10', "", "", "1");
        $pdf->SetFont("Arial", "", "12");
        $pdf->SetLineWidth(0.6);
        $pdf->Line(11, 150, 190, 150);
        $ylength = 150;
        foreach ($details['expenses'] as $item) {
            $pdf->Cell('80', '10', $item->expense_name, "", "0", 'L');
            $pdf->Cell('30', '10', $item->expense_value, "", "0", 'C');
            $pdf->Cell('0', '10', "", "", "1");
            $ylength += 10;
        }
//loop
        $pdf->SetLineWidth(0.6);
        $pdf->Line(11, $ylength + 2, 190, $ylength + 2);
        $pdf->Cell('0', '10', "", "", "1");
        $pdf->Cell('80', '10', 'Expense Total :', '', '0', 'C');
        $pdf->Cell('30', '10', $details['expense_total'], '', '0', 'C');
        $pdf->SetFont("Arial", "B", "12");
        $pdf->Cell('0', '10', "", "", "1");
        $pdf->Cell('80', '10', 'Timesheet Total :', '', '0', 'C');
        $pdf->Cell('30', '10', $details['total'], '', '0', 'C');
        $pdf->SetFont("Arial", "", "11");
        $pdf->Cell('0', '10', "", "", "1");
        $pdf->Cell('0', '10', "", "", "1");
        // make the directory to store the pic:
        if (!is_dir(Yii::getPathOfAlias('webroot') . '/uploads/invoicespdf')) {
            mkdir(Yii::getPathOfAlias('webroot') . '/uploads/invoicespdf');
            chmod((Yii::getPathOfAlias('webroot') . '/uploads/invoicespdf'), 0755);
            // the default implementation makes it under 777 permission, which you could possibly change recursively before deployment, but here's less of a headache in case you don't
        }
        return $pdf->Output(Yii::getPathOfAlias('webroot') . '/uploads/invoicespdf/' . $details['company'] . '-' . $details['id'] . '.pdf');
    }

    public static function setInvoiceData($id, $model, $contact_details) {
        $model->hours = date('H.i', $model->hours);
        $model->date = date('d/m/Y', $model->date);
        $expenses = QuickTsExpenses::model()->findAllByAttributes(array('quick_time_id' => $id));
        $company = AppUser::getUserCompany();

        $details['company'] = $company['name'];
        $details['companyAddress'] = $company['address'];
        $details['post_code'] = $company['post_code'];
        $details['companyCity'] = $company['city'];
        $details['companyCountry'] = $company['country'];
        $details['logo'] = $company['logo'];
        $details['client_info'] = $contact_details;
        $details['hours'] = $model->hours;
        $currency = Currency::model()->findByPk($model->currency_id)->name;
        if ($currency == "GBP")
            $details['currency'] = chr(163);
        else if ($currency == "USD")
            $details['currency'] = "$";
        else if ($currency == "EURO")
            $details['currency'] = chr(128);
        $details['date'] = $model->date;
        $details['total'] = $model->total;
        $details['rates'] = $model->rate;
        $details['expenses'] = $expenses;
        $details['contractor_name'] = $company['name'];
        $details['contractor_address'] = $company['address'];
        $details['vat_number'] = $company['vat_number'];
        $details['id'] = $id;
        $details['hourly_total'] = $model->hours * $model->rate;
        $expense_total = 0;
        foreach ($expenses as $expense)
            $expense_total += $expense->expense_value;
        $details['expense_total'] = $expense_total;
        return $details;
    }

    public static function deleteQuickTimesheet($id) {
        QuickTsExpenses::model()->deleteAllByAttributes(array("quick_time_id" => $id));
        QuickTsReciepts::model()->deleteAllByAttributes(array("ref_id" => $id));
        if (QuickTimesheet::model()->deleteByPk($id)) {

            return TRUE;
        }

        return FALSE;
    }

    public static function getTimeSheetForAdmin($args = array()) {


        $company = AppUser::getUserCompany();

        $select = "SELECT distinct sc.company_id,sc.type,st.* ";
        $cselect = "SELECT count(*) as total ";


        if (!isset($args['user']) || $args['user'] == '') {

            $sql = '  FROM sp_timesheet st '
                    . 'INNER JOIN sp_contract sc ON st.contract_id = sc.id WHERE st.parent IS NULL '
                    . 'AND (st.contract_id IN (SELECT id FROM sp_contract WHERE  company_id = "' . $company->id . '"  ) '
                    . 'OR st.contract_id IN (SELECT id FROM sp_contract WHERE  user_id IN (SELECT user_id FROM sp_contract WHERE company_id = "' . $company->id . '" AND is_internal = 1 ) ) '
                    . 'OR st.contract_id IN (SELECT id FROM sp_contract WHERE sp_contract.type = "out_b" '
                    . 'AND parent_id IN (SELECT id FROM sp_contract WHERE company_id = "' . $company->id . '" and type="in_out" ) )'
                    . ')'
                    . ' ';
        } else {

            $sql = '  FROM sp_timesheet st '
                    . 'INNER JOIN sp_contract sc ON st.contract_id = sc.id WHERE st.parent IS NULL '
                    . 'AND (st.contract_id IN (SELECT id FROM sp_contract WHERE  company_id = "' . $company->id . '"  ) '
                    . 'OR st.contract_id IN (SELECT id FROM sp_contract WHERE  user_id IN ("' . $args['user'] . '") ) '
                    . 'OR st.contract_id IN (SELECT id FROM sp_contract WHERE sp_contract.type = "out_b" '
                    . 'AND parent_id IN (SELECT id FROM sp_contract WHERE company_id = "' . $company->id . '" and type="in_out" ) )'
                    . ') AND sc.user_id IN("' . $args['user'] . '") '
                    . ' ';
        }

        $countSQL = 'SELECT count(*) as total FROM sp_timesheet st INNER JOIN sp_contract sc ON st.contract_id = sc.id WHERE st.parent IS NULL AND (st.contract_id IN (SELECT id FROM sp_contract WHERE  company_id = "' . $company->id . '"  ) OR st.contract_id IN (SELECT id FROM sp_contract WHERE sp_contract.type = "out_b" AND parent_id IN (SELECT id FROM sp_contract WHERE company_id = "' . $company->id . '" and type="in_out" ) )) ';
        if (isset($args['dispatch_flag']) && $args['dispatch_flag'] == 1) {
            $sql .= " AND  st.dispatch_flag = 1 ";
            $countSQL .= " AND  st.dispatch_flag = 1 ";
        }
        $command = Yii::app()->db->createCommand($cselect . $sql);

        $result = $command->queryRow();

        $count = $result['total'];


        $pages = new CPagination($count);

        $pages->pageSize = (isset($args['size'])) ? (int) $args['size'] : Yii::app()->params['pagination']['tsheetSize'];
        /*         * * Applying Limit * */
        $sql = $select . $sql . " LIMIT " . $pages->getCurrentPage() * $pages->getPageSize() . ',' . $pages->getPageSize();

        $entries = Timesheet::model()->findAllBySql($sql);
        return array('dataSet' => $entries, 'pages' => $pages);
    }

    public static function getTimeSheetArchivesForAdmin($args = array()) {
        $company = AppUser::getUserCompany();

        $select = "SELECT distinct sc.company_id,sc.type,st.* ";
        $cselect = "SELECT count(*) as total ";
        if (!isset($args['user']) || $args['user'] == '') {

            $sql = '  FROM sp_timesheet st '
                    . 'INNER JOIN sp_contract sc ON st.contract_id = sc.id WHERE st.parent IS NULL AND st.status!="pending" '
                    . 'AND (st.contract_id IN (SELECT id FROM sp_contract WHERE  company_id = "' . $company->id . '"  ) '
                    . 'OR st.contract_id IN (SELECT id FROM sp_contract WHERE  user_id IN (SELECT user_id FROM sp_contract WHERE company_id = "' . $company->id . '" AND is_internal = 1 ) ) '
                    . 'OR st.contract_id IN (SELECT id FROM sp_contract WHERE sp_contract.type = "out_b" '
                    . 'AND parent_id IN (SELECT id FROM sp_contract WHERE company_id = "' . $company->id . '" and type="in_out" ) )'
                    . ')'
                    . ' ';
        } else {

            $sql = '  FROM sp_timesheet st '
                    . 'INNER JOIN sp_contract sc ON st.contract_id = sc.id WHERE st.parent IS NULL '
                    . 'AND (st.contract_id IN (SELECT id FROM sp_contract WHERE  company_id = "' . $company->id . '"  ) '
                    . 'OR st.contract_id IN (SELECT id FROM sp_contract WHERE  user_id IN ("' . $args['user'] . '") ) '
                    . 'OR st.contract_id IN (SELECT id FROM sp_contract WHERE sp_contract.type = "out_b" '
                    . 'AND parent_id IN (SELECT id FROM sp_contract WHERE company_id = "' . $company->id . '" and type="in_out" ) )'
                    . ') AND sc.user_id IN("' . $args['user'] . '") '
                    . ' ';
        }

        $countSQL = 'SELECT count(*) as total FROM sp_timesheet st INNER JOIN sp_contract sc ON st.contract_id = sc.id WHERE st.parent IS NULL AND (st.contract_id IN (SELECT id FROM sp_contract WHERE  company_id = "' . $company->id . '"  ) OR st.contract_id IN (SELECT id FROM sp_contract WHERE sp_contract.type = "out_b" AND parent_id IN (SELECT id FROM sp_contract WHERE company_id = "' . $company->id . '" and type="in_out" ) )) ';

        if (isset($args['dispatch_flag']) && $args['dispatch_flag'] == 1) {
            $sql .= " AND  st.dispatch_flag = 1 ";
            $countSQL .= " AND  st.dispatch_flag = 1 ";
        }
        $sql .= " ORDER BY st.created_at desc";
        $command = Yii::app()->db->createCommand($cselect . $sql);

        $result = $command->queryRow();

        $count = $result['total'];

        $pages = new CPagination($count);

        $pages->pageSize = (isset($args['size'])) ? (int) $args['size'] : Yii::app()->params['pagination']['tsheetSize'];
        /*         * * Applying Limit * */
        $sql = $select . $sql . " LIMIT " . $pages->getCurrentPage() * $pages->getPageSize() . ',' . $pages->getPageSize();
        $entries = Timesheet::model()->findAllBySql($sql);
        return array('dataSet' => $entries, 'pages' => $pages);
    }

    public static function getTimesheetOfTwoWayContract($args = array(), $is_api = false) {
        if (isset($args['user'])) {
            $company = AppUser::getUserCompany($args['user']);
        } else {
            $company = AppUser::getUserCompany();
        }
        $select = "SELECT distinct sc.company_id,sc.type,st.* ";
        $cselect = "SELECT count(*) as total ";
        $sql = '  FROM sp_timesheet st '
                . 'INNER JOIN sp_contract sc ON st.contract_id = sc.id WHERE st.parent IS NULL '
                . 'AND ( '
                . 'st.contract_id IN (SELECT id FROM sp_contract WHERE type = "out" AND company_id = "' . $company->id . '" ) '
                . ' OR st.contract_id IN (SELECT id FROM sp_contract WHERE type = "out" AND parent_id IN (SELECT id FROM sp_contract WHERE company_id = "' . $company->id . '" AND is_internal = 1 ))'
                . ') '
                . ' ';

        if (isset($args['dispatch_flag']) && $args['dispatch_flag'] == 0) {
            $sql .= " AND  st.dispatch_flag = 0 ";
        } else if (isset($args['dispatch_flag']) && $args['dispatch_flag'] == 1) {
            $sql .= " AND  st.dispatch_flag = 1 ";
        }
        if (isset($args['contract_id'])) {
            $sql .= " AND st.contract_id=" . $args['contract_id'];
        }
        $sql .= " ORDER BY st.created_at desc";

        $command = Yii::app()->db->createCommand($cselect . $sql);

        $result = $command->queryRow();

        $count = $result['total'];


        $pages = new CPagination($count);

        $pages->pageSize = (isset($args['size'])) ? (int) $args['size'] : Yii::app()->params['pagination']['tsheetSize'];
        /*         * * Applying Limit * */
        $sql = $select . $sql . " LIMIT " . $pages->getCurrentPage() * $pages->getPageSize() . ',' . $pages->getPageSize();
        $entries = Timesheet::model()->findAllBySql($sql);
        if ($is_api) {
            return array("dataSet" => $entries, 'pages' => $pages->getPageCount(), "current_page" => $pages->getCurrentPage());
        } else {
            return array('dataSet' => $entries, 'pages' => $pages);
        }
    }

    public static function getUserTimesheetOfTwoWayContract($args = array()) {
        $company = AppUser::getUserCompany();
        $select = "SELECT distinct sc.company_id,sc.type,st.* ";
        $cselect = "SELECT count(*) as total ";
        $sql = '  FROM sp_timesheet st '
                . 'INNER JOIN sp_contract sc ON st.contract_id = sc.id WHERE st.parent IS NULL '
                . 'AND ( '
                . 'st.contract_id IN (SELECT id FROM sp_contract WHERE type = "out" AND company_id = "' . $company->id . '" AND user_id = "' . AppUser::getUserId() . '" ) '
                . ' OR st.contract_id IN (SELECT id FROM sp_contract WHERE type = "out" AND parent_id IN (SELECT id FROM sp_contract WHERE company_id = "' . $company->id . '" AND user_id = "' . AppUser::getUserId() . '" AND is_internal = 1 ))'
                . ') '
                . ' ';

        if (isset($args['dispatch_flag']) && $args['dispatch_flag'] == 0) {
            $sql .= " AND  st.dispatch_flag = 0 ";
        } else if (isset($args['dispatch_flag']) && $args['dispatch_flag'] == 1) {
            $sql .= " AND  st.dispatch_flag = 1 ";
        }

        $command = Yii::app()->db->createCommand($cselect . $sql);

        $result = $command->queryRow();

        $count = $result['total'];


        $pages = new CPagination($count);

        $pages->pageSize = (isset($args['size'])) ? (int) $args['size'] : Yii::app()->params['pagination']['tsheetSize'];
        /*         * * Applying Limit * */
        $sql = $select . $sql . " LIMIT " . $pages->getCurrentPage() * $pages->getPageSize() . ',' . $pages->getPageSize();
        $entries = Timesheet::model()->findAllBySql($sql);
        return array('dataSet' => $entries, 'pages' => $pages);
    }

    public static function getTimesheetOfResource($args = array(), $is_api = false) {

        if (isset($args['user'])) {
            $company = AppUser::getUserCompany($args['user']);
        } else {
            $company = AppUser::getUserCompany();
        }
        $select = "SELECT distinct sc.company_id,sc.type,st.* ";
        $cselect = "SELECT count(*) as total ";
        $sql = '  FROM sp_timesheet st '
                . 'INNER JOIN sp_contract sc ON st.contract_id = sc.id WHERE st.parent IS NULL '
                . 'AND ( '
                . 'st.contract_id IN (SELECT id FROM sp_contract WHERE (type = "out" OR type ="out_b")'
                . ' AND company_id = "' . $company->id . '" ) '
                . ' OR st.contract_id IN (SELECT id FROM sp_contract WHERE (type = "out" OR type ="out_b")'
                . ' AND parent_id IN (SELECT id FROM sp_contract WHERE company_id = "' . $company->id . '" '
                . 'AND is_internal = 1 ))'
                . ') '
                . ' ';

        if (isset($args['dispatch_flag']) && $args['dispatch_flag'] == 0) {
            $sql .= " AND  st.dispatch_flag = 0 ";
        } else if (isset($args['dispatch_flag']) && $args['dispatch_flag'] == 1) {
            $sql .= " AND  st.dispatch_flag = 1 ";
        }
        $sql .= " ORDER BY st.created_at desc";
        $command = Yii::app()->db->createCommand($cselect . $sql);

        $result = $command->queryRow();

        $count = $result['total'];


        $pages = new CPagination($count);

        $pages->pageSize = (isset($args['size'])) ? (int) $args['size'] : Yii::app()->params['pagination']['tsheetSize'];
        /*         * * Applying Limit * */
        $sql = $select . $sql . " LIMIT " . $pages->getCurrentPage() * $pages->getPageSize() . ',' . $pages->getPageSize();
        $entries = Timesheet::model()->findAllBySql($sql);
        if ($is_api) {
            return array("dataSet" => $entries, 'pages' => $pages->getPageCount(), "current_page" => $pages->getCurrentPage());
        } else {
            return array('dataSet' => $entries, 'pages' => $pages);
        }
    }

    public static function getTimesheetOfThreeWayContract($args = array(), $is_api = false) {
        if (isset($args['user'])) {
            $company = AppUser::getUserCompany($args['user']);
        } else {
            $company = AppUser::getUserCompany();
        }
        $select = "SELECT distinct sc.company_id,sc.type,st.* ";
        $cselect = "SELECT count(*) as total ";
        $sql = '  FROM sp_timesheet st '
                . 'INNER JOIN sp_contract sc ON st.contract_id = sc.id WHERE st.parent IS NULL '
                . 'AND (st.contract_id IN (SELECT id FROM sp_contract WHERE sp_contract.type = "out_b" '
                . 'AND parent_id IN (SELECT id FROM sp_contract WHERE company_id = "' . $company->id . '" and type="in_out" ) )'
                . ')'
                . ' ';

        if (isset($args['dispatch_flag']) && $args['dispatch_flag'] == 0) {
            $sql .= " AND  st.dispatch_flag = 0 ";
        } else if (isset($args['dispatch_flag']) && $args['dispatch_flag'] == 1) {
            $sql .= " AND  st.dispatch_flag = 1 ";
        }
        if (isset($args['contract_id'])) {
            $sql .= " AND st.contract_id=" . $args['contract_id'];
        }
        $sql .= " ORDER BY st.created_at desc";

        $command = Yii::app()->db->createCommand($cselect . $sql);

        $result = $command->queryRow();

        $count = $result['total'];


        $pages = new CPagination($count);

        $pages->pageSize = (isset($args['size'])) ? (int) $args['size'] : Yii::app()->params['pagination']['tsheetSize'];
        /*         * * Applying Limit * */
        $sql = $select . $sql . " LIMIT " . $pages->getCurrentPage() * $pages->getPageSize() . ',' . $pages->getPageSize();
        $entries = Timesheet::model()->findAllBySql($sql);
        if ($is_api) {
            return array("dataSet" => $entries, "pages" => $pages->getPageCount(), "current_page" => $pages->getCurrentPage());
        } else {
            return array('dataSet' => $entries, 'pages' => $pages);
        }
    }

    public static function getTimecard($parent_id) {
        $response = array('status' => false, 'data' => '', 'reason' => 'Not a Valid Ajax Request or Post Data is Missing');
        if ($parent_id != "") {
            $parent = Timesheet::model()->findByPk($parent_id);
            $details = AppTimeSheet::compileCardDetail($parent_id);
            $mergeChilds = null;
            $notes = TimesheetNotes::model()->findAllByAttributes(array("user_timing_id" => $parent_id));
            $breaks = UserLunch::model()->findAllByAttributes(array("user_timing_id" => $parent_id));

            if ($parent->merge_status == 1)
                $childs = AppTimeSheet::getTimeSheets(array('size' => '1000', 'old_parent' => $parent_id, 'sort' => ' t.start_time ASC '));
            else {
                /*                 * * Inherited Childs * */
                $childs = AppTimeSheet::getTimeSheets(array('size' => '1000', 'parent' => $parent_id, 'sort' => ' t.start_time ASC ', 'childStatus' => 1));
                /*                 * * Merged Childs * */
                $mergeChilds = AppTimeSheet::getTimeSheets(array('size' => '1000', 'parent' => $parent_id, 'sort' => ' t.start_time ASC ', 'childStatus' => 2));
            }
            return array('childs' => $childs, 'mergeChilds' => $mergeChilds, 'parent' => $parent, 'breaks' => $breaks, 'notes' => $notes, 'details' => $details);
        }
        return null;
    }

    public static function getUserTimesheetOfThreeWayContract($args = array()) {
        $company = AppUser::getUserCompany();
        $select = "SELECT distinct sc.company_id,sc.type,st.* ";
        $cselect = "SELECT count(*) as total ";
        $sql = '  FROM sp_timesheet st '
                . 'INNER JOIN sp_contract sc ON st.contract_id = sc.id WHERE st.parent IS NULL '
                . 'AND (st.contract_id IN (SELECT id FROM sp_contract WHERE sp_contract.type = "out_b" '
                . 'AND parent_id IN (SELECT id FROM sp_contract WHERE company_id = "' . $company->id . '" and user_id = "' . AppUser::getUserId() . '" and type="in_out" ) )'
                . ')'
                . ' ';

        if (isset($args['dispatch_flag']) && $args['dispatch_flag'] == 0) {
            $sql .= " AND  st.dispatch_flag = 0 ";
        } else if (isset($args['dispatch_flag']) && $args['dispatch_flag'] == 1) {
            $sql .= " AND  st.dispatch_flag = 1 ";
        }

        $command = Yii::app()->db->createCommand($cselect . $sql);

        $result = $command->queryRow();

        $count = $result['total'];


        $pages = new CPagination($count);

        $pages->pageSize = (isset($args['size'])) ? (int) $args['size'] : Yii::app()->params['pagination']['tsheetSize'];
        /*         * * Applying Limit * */
        $sql = $select . $sql . " LIMIT " . $pages->getCurrentPage() * $pages->getPageSize() . ',' . $pages->getPageSize();
        $entries = Timesheet::model()->findAllBySql($sql);
        return array('dataSet' => $entries, 'pages' => $pages);
    }

    /*     * ******** */
    /*     * **
     *  Function That Returns The getTimeSheets
     *  Input Parms: $args = array('size'=>15,'contract'=>CONTRACTID,
     *                              'user'=>"USERID","parent"=>"PARENTID",
     *                              "sort"=>"t.start_time ASC",'approver'=>'USERID',
     *                             'payroller'=>'USERID','status'=>array('pendding'),
     *                             'childStatus'=>0)
     *     $args['size'] => For Page Size 
     *     $args['contract'] => Filter By Contracts 
     *     $args['user'] => Filter By Users  
     *     $args['parent'] => Filter By Parents 
     *     $args['old_parent'] => Filter By Old Parents 
     *     $args['sort'] => ORDER BY PARAMS
     *     $args['approver'] => Filter By Approver
     *     $args['payroller'] => Filter By Payroller  
     *     $args['status'] => Filter By Status 
     *     $args['childStatus'] = 0,1,2 ###### 0 - Both (Default Option - includes merge and Inherited)  , 1 - Inherited Childs Only , 2 - Merge Childs Only
     *     $args['dispatch_flag'] = 1
     *   
     * 
     */

    public static function getTimeSheets($args, $is_api = false) {

        $c = new CDbCriteria();
        $paramArray = array();

        $c->join = "INNER JOIN sp_contract con on con.id = t.contract_id ";


        /*         * *** Filter By Parent *** */
        if (isset($args['parent'])) {
            $c->addCondition(' t.parent = :parentId ');
            $paramArray[':parentId'] = $args['parent'];
        } elseif (isset($args['old_parent'])) {
            $c->addCondition(' t.old_parent = :parentId ');
            $paramArray[':parentId'] = $args['old_parent'];
        } else { /// Display Only TimeCards
            if (isset($args['contract_id']))
                $c->addCondition(" CASE t.type WHEN 'daily' THEN t.parent IS NOT NULL ELSE t.parent IS NULL END");
            else
                $c->addCondition(' t.parent IS NULL');
        }
        /**/


        /*         * ** Child Filter Merge/Inherited * */

        if (isset($args['childStatus'])) {
            switch ($args['childStatus']) {
                case 1: $c->addCondition(' t.old_parent IS NULL');
                    break;
                case 2: $c->addCondition(' t.old_parent IS NOT NULL');
                    break;
            }
        }

        /*         * *** */



        /*         * *** Filter By User *** */
        if (isset($args['user']) && AppUser::isUserAdmin() == false) {
            $c->addCondition(' con.user_id = :userId ');
            $paramArray[':userId'] = $args['user'];
        }
        /** Filter By Dispatch Copy ** */
        if (isset($args['dispatch_flag'])) {
            $c->addCondition('t.dispatch_flag = :dispatchFlag');
            $paramArray[':dispatchFlag'] = $args['dispatch_flag'];
        }

        /*         * */


        /** Filter By Dispatch Copy ** */
        if (isset($args['dispatch_status']) && !is_array($args['dispatch_status'])) {
            $c->addCondition('t.dispatch_status = :dispatchStatus');
            $paramArray[':dispatchStatus'] = $args['dispatch_status'];
        } elseif (isset($args['dispatch_status'])) {
            $str = implode("','", $args['dispatch_status']);
            $c->addCondition("t.dispatch_status IN ('" . $str . "')");
        }

        /*         * */
        /*         * ** Filter By Approver ** */
        if (isset($args['approver'])) {
            $c->addCondition('con.approver_id = :appUID');
            $paramArray[':appUID'] = $args['approver'];
        }
        /*         * *********************** */

        /*         * ** Filter By Payroller ** */
        if (isset($args['payroller'])) {
            $c->addCondition('con.payroller_id = :payUID');
            $paramArray[':payUID'] = $args['payroller'];
        }
        /*         * *********************** */




        /*         * ***** Filter By Contract ** */
        if (isset($args['contract'])) {
            $c->addCondition(' con.id = :contractId ');
            $paramArray[':contractId'] = $args['contract'];
        }
        /**/

        if (isset($args['contract_id'])) {
            $c->addCondition(' con.id = :contractId ');
            $paramArray[':contractId'] = $args['contract_id'];
        }

        /*         * ***** Filter By Contract Type ** */
        if (isset($args['contractType'])) {
            if ($args['contractType'] == 'internal') {
                $c->addCondition(' con.is_internal = 1 ');
                $args['contractType'] = 'in';
            }
            $c->addCondition(' con.type = :contractType ');
            $paramArray[':contractType'] = $args['contractType'];
        }
        /**/

        /*         * ***** Filter By Company ** */
        if (isset($args['company'])) {
            $c->addCondition(' con.company_id = :companyId ');
            $paramArray[':companyId'] = $args['company'];
        }
        /**/
        /*         * * If Status is Provided * */
        if (isset($args['status']) && is_array($args['status'])) {
            $statusArr = implode("','", $args['status']);
            $c->addCondition("t.status IN ('" . $statusArr . "')");
        } elseif (isset($args['status'])) {
            $c->addCondition('t.status = :sheetStatus');
            $paramArray[':sheetStatus'] = $args['status'];
        }

        /*         * ************************ */


        /*         * ****** Order By Query *** */
        if (isset($args['sort'])) {
            $c->order = $args['sort'];
        } else {
            $c->order = ' t.created_at desc';
        }





        $c->params = $paramArray;

        $c->together = true;

        $count = Timesheet::model()->count($c);
        $pages = new CPagination($count);

        $pages->pageSize = (isset($args['size'])) ? (int) $args['size'] : Yii::app()->params['pagination']['tsheetSize'];
        $pages->applyLimit($c);
        $entries = Timesheet::model()->findAll($c);
        if ($is_api) {
            return array("dataSet" => $entries, "pages" => $pages->getPageCount(), "current_page" => $pages->getCurrentPage());
        } else {
            return array('dataSet' => $entries, 'pages' => $pages);
        }
    }

    /*     * ***
     * Extract Parent of Daily Entry TimSheet If Not Found then it creates it
     */

    public static function getParentTSheet($contractId, $currentDate, &$contractObj = null) {

        if ($contractObj != null)
            $contract = $contractObj;
        else
            $contract = Contract::model()->findByPk($contractId);

        if ($contract != null && $currentDate != null) {
            switch ($contract->collection_type) {

                case 0:    // Weekly Type
                    $time = self::getStEnDateWeekly($contract->collection_day, $currentDate);

                    break;
                default: // First & Last Week Of Every Month 
                    $time = self::getStEnDateMonthly($contract->collection_type, $contract->collection_day, $currentDate);

                    break;
            }


            $criteria = new CDbCriteria();
            $criteria->addCondition(' t.contract_id = :contractId AND t.start_time >= :startTime AND t.end_time <= :endTime AND parent is NULL AND status = :sheetStatus AND t.type = :type');
            $criteria->params = array(":startTime" => strtotime($time['start']), ":endTime" => strtotime($time['end']), ":sheetStatus" => 'pending', ":contractId" => $contract->id, ":type" => 'daily');
            $tSheet = Timesheet::model()->find($criteria);

            if ($tSheet != null) {
                return $tSheet;
            } else {

                $parent = self::addParentTSheet($contractId, strtotime($time['start']), strtotime($time['end']), 'pending');

                if ($parent != null) {
                    return $parent;
                }
            }
        }
        return null;
    }

    /*     * ****
     *  Get Start And End Date Weekly W.R.T Collection Day & Provided Date
     */

    public static function getStEnDateWeekly($collectionDay, $entryDate) {

        $colldayOfWeek = date('N', strtotime($collectionDay));
        $entrydayOfWeek = date('N', strtotime($entryDate));
        $response = array();

        if ($entrydayOfWeek > $colldayOfWeek) {
            $diff = $entrydayOfWeek - $colldayOfWeek;
            $response['start'] = strtotime($entryDate . ' 00:00:00 -' . ($diff - 1) . ' days');
            $response['end'] = strtotime($entryDate . ' 23:59:59 +' . (7 - $diff) . ' days');
        } else {
            $diff = $colldayOfWeek - $entrydayOfWeek;

            $response['end'] = strtotime($entryDate . ' 23:59:59 +' . $diff . ' days');
            $response['start'] = strtotime($entryDate . ' 00:00:00 -' . (6 - $diff) . ' days');
        }



        foreach ($response as $k => $v)
            $response[$k] = date('m/d/Y h:i:s A', $v);

        return $response;
    }

    /*     * ****
     *  Get Start And End Date Monthly W.R.T Collection Week , Collection Day & Provided Date
     */

    public static function getStEnDateMonthly($colltWeek, $collectionDay, $entryDate) {

        $entryTime = strtotime($entryDate);
        $entryMonthYr = date('M Y', $entryTime);
        $NextMonthYr = date('M Y', strtotime($entryMonthYr . ' +1 months '));

        if ($colltWeek == 1) {
            $response['start'] = strtotime('first ' . $collectionDay . ' of ' . $entryMonthYr);
            $response['end'] = strtotime('first ' . $collectionDay . ' of ' . $NextMonthYr) - 1;
        } else {
            $response['start'] = strtotime('last ' . $collectionDay . ' of ' . $entryMonthYr);
            $response['end'] = strtotime('last ' . $collectionDay . ' of ' . $NextMonthYr) - 1;
        }




        foreach ($response as $k => $v)
            $response[$k] = date('m/d/Y h:i:s A', $v);


        return $response;
    }

    /*     * **
     * Add Parent Timesheet Entry Depending on Start & End Time
     */

    public static function addParentTSheet($contractId, $startTime, $endTime, $status = 'current') {

        $contract = Contract::model()->findByPk($contractId);

        if ($contract != null) {
            $model = new Timesheet();
            $model->id = AppInterface::getUniqueId();
            $model->contract_id = $contractId;
            $model->start_time = $startTime;
            $model->end_time = $endTime;
            $model->status = $status;
            $model->type = $contract->timesheetTypeSel;
            $model->created_at = time();
            $model->modified_at = time();
            $model->created_by = AppUser::getUserId();
            $model->modified_by = AppUser::getUserId();

            if ($model->insert()) {
                return $model;
            }
        }

        return null;
    }

    public static function calHrsDiff($sTime, $eTime) { /// Time in Sec Inputs
        $diff = $eTime - $sTime;
        return $diff / 3600;
    }

    /*     * **
     *  Compile Card Details
     */

    public static function compileCardDetail($tSheetId) {

        $timeSheet = Timesheet::model()->findByPk($tSheetId);

        if ($timeSheet != null) {
            if (AppUser::isUserAdmin()) {
                if (isset($timeSheet->contract->parent)) {
                    if ($timeSheet->contract->type == 'out_b' && $timeSheet->contract->company_id != AppUser::getUserCompany()->id) {
                        $response['employeeType'] = ucwords(str_replace('_', ' ', $timeSheet->contract->parent->CustomEmploymentType));
                    } else {
                        $response['employeeType'] = $timeSheet->contract->CustomEmploymentType;
                    }
                } else {
                    $response['employeeType'] = $timeSheet->contract->CustomEmploymentType;
                }
            } else {
                $response['employeeType'] = $timeSheet->contract->CustomName;
            }
            $response['employeeName'] = $timeSheet->contract->user->full_name;
            $response['employeeEmail'] = $timeSheet->contract->user->email;
            $response['company'] = $timeSheet->contract->company->name;
            $response['vat'] = $timeSheet->contract->company->vat_number;
            $response['post_code'] = $timeSheet->contract->company->post_code;
            $response['companyCity'] = $timeSheet->contract->company->city;
            $response['companyAddress'] = $timeSheet->contract->company->address;
            $response['companyCountry'] = $timeSheet->contract->company->country;
            $response['reg_number'] = $timeSheet->contract->company->registration_number;
            $response['logo'] = $timeSheet->contract->company->logo;
            $response['employeeId'] = AppInterface::formatID($timeSheet->contract->user->id);
            $response['regularTime'] = $timeSheet->time;
            $response['timesheetId'] = $timeSheet->id;
            $response['ni_number'] = $timeSheet->contract->user->ni_number;
            $response['approver'] = $timeSheet->contract->approver->full_name;
            $response['approverEmail'] = $timeSheet->contract->approver->email;
            $response['company_policies'] = $timeSheet->contract->policy;
            $response['payHours'] = array();
            $response['payRates'] = array();
            $response['entry'] = $timeSheet;
            $expSum = 0;
            $rateVal = null;
            if (isset($timeSheet->timesheets) && count($timeSheet->timesheets) > 0) {

                foreach ($timeSheet->timesheets as $sheet) {

                    foreach ($sheet->payrates as $payRate) {

                        if ($payRate->is_item == 0) {
                            if (isset($payRate->contractSettig)) {
                                if (!isset($response['payHours'][$payRate->contractSetting->key]))
                                    $response['payHours'][$payRate->contractSetting->key] = $payRate->hours;
                                else
                                    $response['payHours'][$payRate->contractSetting->key] += $payRate->hours;

                                $rateVal = self::sheetPayrate($sheet, $payRate->contractSetting->key);
                            }
                            if ($payRate->contractSetting) {
                                if (!isset($response['payRates'][$payRate->contractSetting->key]) && $rateVal != null) {
                                    if (is_array($rateVal)) {
                                        $response['payRates'][$payRate->contractSetting->key] = $rateVal[0]->value;
                                        $response['payRates2'][$payRate->contractSetting->key] = $rateVal[1]->value;
                                    } else {
                                        $response['payRates'][$payRate->contractSetting->key] = $rateVal->value;
                                    }
                                }
                            }
                        } else {
                            if (array_key_exists("itemCount", $response)) {
                                if (!isset($response['itemCount'][$payRate->contractSetting->key]))
                                    $response['itemCount'][$payRate->contractSetting->key] = $payRate->item_value;
                                else
                                    $response['itemCount'][$payRate->contractSetting->key] += $payRate->item_value;

                                $rateVal = self::sheetPayrate($sheet, $payRate->contractSetting->key);
                            }
                            if (array_key_exists("itemRates", $response)) {
                                if (!isset($response['itemRates'][$payRate->contractSetting->key]) && $rateVal != null) {
                                    if (is_array($rateVal)) {
                                        $response['itemRates'][$payRate->contractSetting->key] = $rateVal[0]->value;
                                        $response['itemRates2'][$payRate->contractSetting->key] = $rateVal[1]->value;
                                    } else {
                                        $response['itemRates'][$payRate->contractSetting->key] = $rateVal->value;
                                    }
                                }
                            }
                        }
                    }

                    $expSum += $sheet->expense;
                }

                $response['expense'] = $expSum;
            } else {

                foreach ($timeSheet->payrates as $payRate) {
                    if ($payRate->is_item == 0) {
                        if (!isset($response['payHours'][$payRate->contractSetting->key]))
                            $response['payHours'][$payRate->contractSetting->key] = $payRate->hours;
                        else
                            $response['payHours'][$payRate->contractSetting->key] += $payRate->hours;

                        $rateVal = self::sheetPayrate($timeSheet, $payRate->contractSetting->key);

                        if (!isset($response['payRates'][$payRate->contractSetting->key]) && $rateVal != null) {
                            if (is_array($rateVal)) {
                                $response['payRates'][$payRate->contractSetting->key] = $rateVal[0]->value;
                                $response['payRates2'][$payRate->contractSetting->key] = $rateVal[1]->value;
                            } else {
                                $response['payRates'][$payRate->contractSetting->key] = $rateVal->value;
                            }
                        }
                    } else {
                        if (!isset($response['itemCount'][$payRate->contractSetting->key]))
                            $response['itemCount'][$payRate->contractSetting->key] = $payRate->item_value;
                        else
                            $response['itemCount'][$payRate->contractSetting->key] += $payRate->item_value;

                        $rateVal = self::sheetPayrate($timeSheet, $payRate->contractSetting->key);

                        if (!isset($response['itemRates'][$payRate->contractSetting->key]) && $rateVal != null) {
                            if (is_array($rateVal)) {
                                $response['itemRates'][$payRate->contractSetting->key] = $rateVal[0]->value;
                                $response['itemRates2'][$payRate->contractSetting->key] = $rateVal[1]->value;
                            } else {
                                $response['itemRates'][$payRate->contractSetting->key] = $rateVal->value;
                            }
                        }
                    }
                }

                $response['expense'] = $timeSheet->expense;
            }


            $response['date'] = $timeSheet->start_time;


            return $response;
        }
        return null;
    }

    public static function sheetPayrate(&$tSheet, $key) {

        $criteria = new CDbCriteria();

        $role = AppUser::getUserType();
        $internalContract = AppContract::getInternalContract($tSheet->contract->user_id);

        if ($tSheet != null && isset($tSheet->contract) && isset($internalContract)) {

            
            /*             * * This Timesheet is his/her internal timesheet * */
            if ($tSheet->contract->id == $internalContract->id) {

                $criteria->select = "t.*, cs.key";
                $criteria->join = " INNER JOIN sp_contract_setting cs ON cs.id = t.contract_setting_id ";
                $criteria->addCondition("t.timesheet_id = :tSheetId");
                $criteria->addCondition("cs.key LIKE ('%" . $key . "%')");
                $criteria->params = array(":tSheetId" => $tSheet->id);

                $model = Payrate::model()->find($criteria); 

            } elseif (($role == 'employee' || $role == 'payroller') && ($tSheet->contract->type == 'out' || $tSheet->contract->type == 'out_b')) {

                /*                 * * Employee can see his/her internal rates for all kind of out case * */
                $cSetting = AppContract::getContractSettingsByKey($internalContract->id, $key, 'payrate');
                if ($cSetting == null) {
                    $cSetting = AppContract::getContractSettingsByKey($internalContract->id, $key, 'item');
                }
                $model = new Payrate();

                $model->value = null;

                if ($cSetting != null) {


                    $model->timesheet_id = $tSheet->id;

                    $model->value = $cSetting->value;

                    $model->status = 1;
                    return $model;
                }
            } elseif ($role == "admin") {

                /*                 * * Display In-Out Rates to Company Admin ** */

                if (AppUser::isCompanyAdmin($tSheet->contract->company_id, AppUser::getUserId()) || ( $tSheet->contract->type == 'in' ) || $tSheet->contract->type == 'out') {

                    $criteria->select = "t.*, cs.key";
                    $criteria->join = " INNER JOIN sp_contract_setting cs ON cs.id = t.contract_setting_id ";
                    $criteria->addCondition("t.timesheet_id = :tSheetId");
                    $criteria->addCondition("cs.key LIKE ('%" . $key . "%')");
                    $criteria->params = array(":tSheetId" => $tSheet->id);

                    $model = Payrate::model()->find($criteria);
                } else if ($tSheet->contract->type == 'out_b' && isset($tSheet->contract->parent) && AppUser::isCompanyAdmin($tSheet->contract->parent->company_id, AppUser::getUserId())) {
                    $tmpContract = $tSheet->contract->parent;
                    $contract = $tSheet->contract;

                    $cSetting = AppContract::getContractSettingsByKey($contract->id, $key, 'payrate');
                    if ($cSetting == null) {
                        $cSetting = AppContract::getContractSettingsByKey($contract->id, $key, 'item');
                    }
                    $billmodel = new Payrate();
                    $billmodel->value = null;

                    if ($cSetting != null) {
                        $billmodel->timesheet_id = $tSheet->id;
                        $billmodel->value = $cSetting->value;
                        $billmodel->status = 1;
                    }

                    $cSetting = AppContract::getContractSettingsByKey($tmpContract->id, $key, 'payrate');
                    if ($cSetting == null) {
                        $cSetting = AppContract::getContractSettingsByKey($tmpContract->id, $key, 'item');
                    }
                    $costmodel = new Payrate();
                    $costmodel->value = null;

                    if ($cSetting != null) {
                        $costmodel->timesheet_id = $tSheet->id;
                        $costmodel->value = $cSetting->value;
                        $costmodel->status = 1;
                    }

                    return array($costmodel, $billmodel);
                } else if ($tSheet->contract->type == 'out_b') {

                    $internalContract = AppContract::getInternalContract($tSheet->contract->user->id);
                    $tmpContract = $internalContract;

                    if (isset($tSheet->contract->parent)) {
                        $tmpContract = $tSheet->contract->parent;
                    }

                    $cSetting = AppContract::getContractSettingsByKey($tmpContract->id, $key, 'payrate');
                    if ($cSetting == null) {
                        $cSetting = AppContract::getContractSettingsByKey($tmpContract->id, $key, 'item');
                    }
                    $model = new Payrate();
                    $model->value = null;

                    if ($cSetting != null) {
                        $model->timesheet_id = $tSheet->id;
                        $model->value = $cSetting->value;
                        $model->status = 1;
                    }
                }
            }


            return $model;
        }



        return null;
    }

    public static function mergeTimeSheets($sheets) {


        $response = array('status' => false, 'reason' => '');
        if (count($sheets) > 0) {
            $destSheet = self::getDestSheet($sheets);
            $sourceSheet = self::getSourceSheet($sheets);

            if ($destSheet != null && $sourceSheet != null) {

                foreach ($sourceSheet as $sheet) {

                    if (self::compareAndMerge($sheet, $destSheet)) {
                        $response['status'] = true;
                    }
                }

                if ($response['status'] == false) {
                    $response['reason'] = "Timesheets cannot be merged, Doesn't fit the criteria: 1) Should Match the Type 2) Only Approved Timesheets are required 3) One Timesheet has to be Internal Timesheet.";
                }
            } else {
                $response['reason'] = "Timesheets cannot be merged, One has to be Internal and Rest of the other should be external";
            }
        }
        return $response;
    }

    public static function compareAndMerge(&$source, &$destination) {

        $state = false;
        if ($source != null && $destination != null && $destination->contract->is_internal == 1 && ( $source->type == $destination->type ) && ($source->status == "approved") && ($destination->status == "approved")) {

            $source->merge_status = 1;
            $source->modified_by = AppUser::getUserId();
            $source->modified_at = time();

            if (isset($source->timesheets) && count($source->timesheets) > 0) {
                foreach ($source->timesheets as $entry) {
                    $entry->parent = $destination->id;
                    $entry->modified_by = AppUser::getUserId();
                    $entry->modified_at = time();
                    $entry->old_parent = $source->id;

                    $entry->update();
                }
            } else {
                $source->parent = $destination->id;
            }

            $source->status = "closed";
            return $source->update();
        }
        return false;
    }

    public static function getDestSheet($sheets) {

        $criteria = new CDbCriteria();
        foreach ($sheets as $entry)
            $criteria->addCondition(" t.id = '" . $entry . "'", 'OR');

        $criteria->join = " INNER JOIN sp_contract conn ON conn.id = t.contract_id ";
        $criteria->addCondition("conn.is_internal = 1 ");

        $sourceEntry = Timesheet::model()->find($criteria);

        return $sourceEntry;
    }

    public static function getSourceSheet($sheets) {

        $criteria = new CDbCriteria();
        foreach ($sheets as $entry)
            $criteria->addCondition(" t.id = '" . $entry . "'", 'OR');

        $criteria->join = " INNER JOIN sp_contract conn ON conn.id = t.contract_id ";
        $criteria->addCondition("conn.is_internal <> 1 ");

        $destEntry = Timesheet::model()->findAll($criteria);

        return $destEntry;
    }

    public static function notifyUsersInApproval(&$sheets) {
        if ($sheets != null) {
            $uname = $sheets->contract->user->first_name . ' ' . $sheets->contract->user->last_name;
            $appname = $sheets->contract->approver->first_name . ' ' . $sheets->contract->approver->last_name;
            $employeeMsg = "Your Timesheet of " . self::genTimeSheetName($sheets) . ' has been sent for approval now.';
            $approverMsg = "You have received a timesheet for approval - " . self::genTimeSheetName($sheets) . '. <a href="' . Yii::app()->createAbsoluteUrl('timesheet/approve') . '">Click Here To View It</a>';

            /*             * * Sent to Employeer * */
            $mailer = new AppMailer();
            $mailer->prepareBody('timesheet-notify', array('NAME' => $uname, "MESSAGE" => $employeeMsg));
            $mailer->sendMail(array(array('email' => $sheets->contract->user->email, 'name' => $uname)), array('email' => AppSetting::getSettingByKey("SUPPORT_EMAIL")->value, 'name' => 'Support'));

            /*             * * Sent to Approver * */
            $mailer = new AppMailer();
            $mailer->prepareBody('timesheet-notify', array('NAME' => $appname, "MESSAGE" => $approverMsg));
            return $mailer->sendMail(array(array('email' => $sheets->contract->approver->email, 'name' => $appname)), array('email' => AppSetting::getSettingByKey("SUPPORT_EMAIL")->value, 'name' => 'Support'));
        }

        return false;
    }

    public static function notifyUsersOnApproved(&$sheets) {
        if ($sheets != null) {
            $uname = $sheets->contract->user->first_name . ' ' . $sheets->contract->user->last_name;
            $appname = $sheets->contract->approver->first_name . ' ' . $sheets->contract->approver->last_name;
            $payRollername = $sheets->contract->payroller->first_name . ' ' . $sheets->contract->payroller->last_name;

            $employeeMsg = "Your Timesheet of " . self::genTimeSheetName($sheets) . ' has been approved by ' . $appname . ". Please login to your panel for further details.";
            $payMsg = "Timesheet of " . self::genTimeSheetName($sheets) . ' has been received for payslip generation.';
            $appMsg = $uname . ' has submitted a new timesheet for approval. Please login to your SpeedyPayrolls account to take appropriate action.';

            /*             * * Sent to Employeer * */
            $mailer = new AppMailer();
            $mailer->prepareBody('timesheet-notify', array('NAME' => $uname, "MESSAGE" => $employeeMsg));
            return $mailer->sendMail(array(array('email' => $sheets->contract->user->email, 'name' => $uname)), array('email' => AppSetting::getSettingByKey("SUPPORT_EMAIL")->value, 'name' => 'Support'));

//            Sent to Approver
            $mailer = new AppMailer();
            $mailer->prepareBody('timesheet-notify', array('NAME' => $appname, "MESSAGE" => $appMsg));
            return $mailer->sendMail(array(array('email' => $sheets->contract->approver->email, 'name' => $appname)), array('email' => AppSetting::getSettingByKey("SUPPORT_EMAIL")->value, 'name' => 'Support'));

            /*             * * Sent to Payroller * */
            $mailer = new AppMailer();
            $mailer->prepareBody('timesheet-notify', array('NAME' => $payRollername, "MESSAGE" => $payMsg));
            return $mailer->sendMail(array('email' => $sheets->contract->payroller->email, 'name' => $uname), array(array('email' => AppSetting::getSettingByKey("SUPPORT_EMAIL"), 'name' => 'Support')));
//            
        }


        return false;
    }

    public static function notifyUsersOnPayrollRequest(&$sheets) {
        if ($sheets != null) {
            $uname = $sheets->contract->user->first_name . ' ' . $sheets->contract->user->last_name;
            $payRollername = $sheets->contract->payroller->first_name . ' ' . $sheets->contract->payroller->last_name;


            $employeeMsg = "Your Timesheet of " . self::genTimeSheetName($sheets) . ' has been forwarded to payroller.';
            $payMsg = "Timesheet of " . self::genTimeSheetName($sheets) . ' has been received for payslip generation.';

            /*             * * Sent to Employeer * */
            $mailer = new AppMailer();
            $mailer->prepareBody('timesheet-notify', array('NAME' => $uname, "MESSAGE" => $employeeMsg));
            $mailer->sendMail(array(array('email' => $sheets->contract->user->email, 'name' => $uname)), array('email' => AppSetting::getSettingByKey("SUPPORT_EMAIL")->value, 'name' => 'Support'));

            /*             * * Sent to Payroller * */
            $mailer = new AppMailer();
            $mailer->prepareBody('timesheet-notify', array('NAME' => $payRollername, "MESSAGE" => $payMsg));
            return $mailer->sendMail(array(array('email' => $sheets->contract->payroller->email, 'name' => $uname)), array('email' => AppSetting::getSettingByKey("SUPPORT_EMAIL")->value, 'name' => 'Support'));
        }


        return false;
    }

    public static function notifyUsersOnDecline(&$sheets) {
        if ($sheets != null) {
            $uname = $sheets->contract->user->first_name . ' ' . $sheets->contract->user->last_name;
            $appname = $sheets->contract->approver->first_name . ' ' . $sheets->contract->approver->last_name;
            $employeeMsg = "Your Timesheet of " . self::genTimeSheetName($sheets) . ' has been declined by ' . $appname . " .";

            /*             * * Sent to Employeer * */
            $mailer = new AppMailer();
            $mailer->prepareBody('timesheet-notify', array('USER' => $uname, "MESSAGE" => $employeeMsg));
            return $mailer->sendMail(array('email' => $sheets->contract->user->email, 'name' => $uname), array('email' => AppSetting::getSettingByKey("SUPPORT_EMAIL")->value, 'name' => 'Support'));
        }

        return false;
    }

    public static function notifyUsersOnClosed(&$sheets) {
        if ($sheets != null) {
            $uname = $sheets->contract->user->first_name . ' ' . $sheets->contract->user->last_name;
            $employeeMsg = "Your Timesheet of " . self::genTimeSheetName($sheets) . ' has been closed now.';

            /*             * * Sent to Employeer * */
            $mailer = new AppMailer();
            $mailer->prepareBody('timesheet-notify', array('USER' => $uname, "MESSAGE" => $employeeMsg));
            return $mailer->sendMail(array('email' => $sheets->contract->user->email, 'name' => $uname), array('email' => AppSetting::getSettingByKey("SUPPORT_EMAIL")->value, 'name' => 'Support'));
        }

        return false;
    }

    public static function genTimeSheetName(&$sheets) {

        if ($sheets == NULL)
            return '';
        else
            return $sheets->contract->company->name . ' ' . strtoupper($sheets->contract->type) . ' - ' . date(AppInterface::getdateformat(), $sheets->start_time) . ' to ' . date(AppInterface::getdateformat(), $sheets->end_time);
    }

    public static function dispatchTimeSheetPayroll($sheets) {



        $response = array('status' => false, 'reason' => '');
        if (count($sheets) > 0) {
            $inCorrectCount = 0;
            $cCount = 0;
            foreach ($sheets as $sheet) {
                $model = Timesheet::model()->findByPk($sheet);

                /*                 * * Change Request Made Now External Contracts can be sent to payroller */
                if ($model->status == "approved" && count($model->payrolls) == 0) {
                    $payroll = new Payroll();
                    $payroll->timesheet_id = $model->id;
                    $payroll->status = "requested";
                    $payroll->created_at = time();
                    $payroll->created_by = AppUser::getUserId();
                    if ($payroll->insert()) {
                        AppTimeSheet::notifyUsersOnPayrollRequest($model);
                        $msg = ConstantMessages::copyToPayroller($model);
                        $not_result = AppInterface::notification($model->contract->approver->id, 5, $model->id, $model->contract->id, $model->contract->user_id);
                        $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
                        $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;
                        $notifier = new Notification();
                        $notification_count = $notifier->getUserNotificationUnseenCount($model->contract->approver->id);
                        $notification = Yii::app()->getModule('notification');
                        $notification->send(array('push' => array('server_notification', array('id' => $model->contract->approver->id, 'msg' => sprintf($not_result->notificationType->notice, AppTimeSheet::genTimeSheetName($model)), 'url' => $url, 'logo' => $logo, 'count' => $notification_count))));
                        $model->dispatch_status = "dispatched";
                        $model->update();
                        $response['status'] = true;
                        $cCount++;
                    }
                } else {
                    $inCorrectCount++;
                }
            }
            if ($cCount > 0) {

                $response['status'] = true;
                $response['reason'] = $cCount . " timsheet(s) has been succesfully forwarded to payroller.";
            }

            if ($inCorrectCount > 0) {

                if ($cCount == 0)
                    $response['reason'] = $inCorrectCount . " timsheets(s) request failed due to approve status or already request forwarded.";

                Yii::app()->user->setFlash('error', $inCorrectCount . " timsheets(s) request failed due to following reasons: <br/> 1) Approve Sheets can be dispatched.<br/>2. Already Request forwarded.<br/>3. Internal Timesheets can only be forwarded. ");
            }
        }

        return $response;
    }

    public static function dispatchTimeSheet($sheets) {
        $response = array('status' => false, 'reason' => '');
        if (count($sheets) > 0) {
            $inCorrectCount = 0;
            $cCount = 0;
            foreach ($sheets as $sheet) {
                $model = Timesheet::model()->findByPk($sheet);
                if ($model->dispatch_flag == 0 && $model->status == "approved") {
                    $model->dispatch_status = 'copied';
                    $model->dispatch_flag = 1;
                    $model->update();
                    $cCount++;
                } else {
                    $inCorrectCount++;
                }
            }

            if ($cCount > 0) {

                $response['status'] = true;
                $response['reason'] = $cCount . " timsheet(s) has been succesfully copied to dispatch payroll.";
            }

            if ($inCorrectCount > 0) {

                if ($cCount == 0)
                    $response['reason'] = $inCorrectCount . " timsheets(s) request failed due to already copied to dispatch folder or status unapproved.";

                Yii::app()->user->setFlash('error', $inCorrectCount . " timsheets(s) request failed due to already copied to dispatch folder or status unapproved.");
            }
        }

        return $response;
    }

    public static function printMergeLogName(&$tSheet) {
        /*         * * Printing External Contract Details ** */
        if ($tSheet != null && isset($tSheet->timesheets) && count($tSheet->timesheets) > 0) {
            $mergeParent = array();
            foreach ($tSheet->timesheets as $sheet) {

                if ($sheet->oldParent != null && !isset($mergeParent[$sheet->oldParent->id])) {

                    $mergeParent[$sheet->oldParent->id] = $sheet->oldParent;
                }
            }

            $str = '';
            foreach ($mergeParent as $mergAcc) {

                switch ($mergAcc) {
                    case 'daily':

                        $str .= '<br/>' . AppContract::getContractCustomName($mergAcc->contract) . ' WK ' . date('W', $mergAcc->end_time) . '';
                        break;
                    default :
                        $str .= '<br/>' . AppContract::getContractCustomName($mergAcc->contract) . ' CUSTOM ' . date(AppInterface::getdateformat(), $mergAcc->end_time) . ' to ' . date(AppInterface::getdateformat(), $mergAcc->end_time) . '';
                        break;
                }
            }

            if ($str != '') {
                $str = '<small><br/>Merge Log:<br/>' . AppContract::getContractCustomName($tSheet->contract) . ' WK ' . date('W', $tSheet->end_time) . $str . '</small>';
            }

            return $str;
        }

        return false;
        /*         * **** */
    }

    private static function getIsoWeeksInYear($year) {
        $date = new DateTime;
        $date->setISODate($year, 53);
        return ($date->format("W") === "53" ? 53 : 52);
    }

    public static function getWeekNumber($date) {
        $rawweek = date('W', $date) - date('W', strtotime('first monday of april ' . date('Y'))) + 1;
        return $rawweek <= 0 ? $rawweek + self::getIsoWeeksInYear(date('Y') - 1) : $rawweek;
    }

    public static function formatSheetPeriod(&$entry) {
        $str = '';
        if ($entry != null) {

            switch ($entry->type) {
                case 'daily':
                    $str = date(AppInterface::getdateformat(), $entry->start_time) . ' to ' . date(AppInterface::getdateformat(), $entry->end_time);
                    break;
                default:
                    $str = 'Custom - <small>(' . date(AppInterface::getdateformat(), $entry->start_time) . ' to ' . date(AppInterface::getdateformat(), $entry->end_time) . ')</small>';
                    break;
            }
        }

        return $str;
    }

    public static function isRegularPayRates(&$input) {

        if ($input != null) {


            $pos = true;

            if ($input instanceof Timesheet)
                $pos = strpos(strtolower($input->contractSetting->key), 'regular');
            elseif ($input instanceof ContractSetting)
                $pos = strpos(strtolower($input->key), 'regular');
            else if($input instanceof Payrate)
                $pos = strpos (strtolower($input->contractSetting->key), 'regular');
            
            if ($pos !== false)
                return true;
        }

        return false;
    }

    public static function getTimesheetPayrates($timesheet_id) {
        $criteria = new CDbCriteria();
        $criteria->select = "t.*";
        $criteria->addCondition("t.timesheet_id= :timesheet_id");
        $criteria->params = array(":timesheet_id" => $timesheet_id);

        $result = array();
        $i = 0;
        $contract_settings = array();
        $payrates = Payrate::model()->findAll($criteria);
        foreach ($payrates as $item) {
            $result[$i]['payrates'] = $item;
            $result[$i]['contract_settings'] = $item->contractSetting;
            $i++;
        }
        return $result;
    }

    public static function getPayrateByKey(&$sheet, $key) {

        $criteria = new CDbCriteria();
        $criteria->select = "t.*, cs.key";
        $criteria->join = " INNER JOIN sp_contract_setting cs ON cs.id = t.contract_setting_id ";
        $criteria->addCondition("t.timesheet_id = :tSheetId");
        $criteria->addCondition("cs.key LIKE ('%" . $key . "%')");
        $criteria->params = array(":tSheetId" => $sheet->id);

        $model = Payrate::model()->find($criteria);

        return $model;
    }

    public static function checkAlreadyExist($stTime, $endTime, &$contract, $type = TIMESHEET_TYPE_DAILY) {

        if ($contract != null) {
            $criteria = new CDbCriteria;
            if ($type == TIMESHEET_TYPE_DAILY) { ///*** Should come from Constants
                $criteria->addCondition(" ( t.start_time >= :startTime AND t.end_time < :endTime ) "
                        . " AND t.contract_id = :contractId AND t.parent IS NOT NULL  ");
                $criteria->params = array(":startTime" => DateTime::createFromFormat('d/m/Y H:i:s', $stTime . ' 00:00:00')->getTimestamp(), //strtotime($tempTimeEntry . ' 00:00:00'),
                    ":endTime" => DateTime::createFromFormat('d/m/Y H:i:s', $stTime . ' 23:59:59')->getTimestamp(), //strtotime($tempTimeEntry . ' 23:59:59'),
                    ":contractId" => $contract->id
                );
            } else {

                $criteria->addCondition(" ( t.start_time >= :startTime AND t.end_time < :endTime )  "
                        . "AND t.contract_id = :contractId  ");

                $criteria->params = array(":startTime" => DateTime::createFromFormat('d/m/Y H:i:s', $stTime . ' 00:00:00')->getTimestamp(), //strtotime($tempTimeEntry . ' 00:00:00'),
                    ":endTime" => DateTime::createFromFormat('d/m/Y H:i:s', $endTime . ' 23:59:59')->getTimestamp(), //strtotime($tempTimeEntry . ' 23:59:59'),
                    ":contractId" => $contract->id
                );
            }


            $checkDayTSheet = Timesheet::model()->count($criteria);
            if ($checkDayTSheet > 0) {
                $sheetModel = Timesheet::model()->find($criteria);/**                 * */
                $error = "Timsheet with this date " . $stTime . " is already entered.";
                return array('status' => true, 'model' => $sheetModel, 'error' => $error);
            }
            return array('status' => false);
        }
        return null;
    }

    public static function addTimesheet($formData, $type, $contract, $tempTimeEntry, $timein, $timeout, $id) {
        if (isset($formData['Timesheet']['id'])) {
            $model = Timesheet::model()->findByPk($formData['Timesheet']['id']);
            $model->attributes = $formData['Timesheet'];
        } else {
            $model = new Timesheet();
            $model->attributes = $formData['Timesheet'];
            $model->id = AppInterface::getUniqueId();
            $model->dispatch_flag = DISPATCH_FLAG_OFF;
            $model->contract_id = $contract->id;
            $model->status = TIMESHEET_STATUS_PENDING;
            $model->created_by = $id;
            $model->modified_by = $id;
            $model->created_at = time();
        }
        ///*** Should come from constants
        if ($type == TIMESHEET_TYPE_DAILY) { ///*** Should come from constants
            $parent = AppTimeSheet::getParentTSheet($contract->id, date('m/d/Y', DateTime::createFromFormat('d/m/Y', $tempTimeEntry)->getTimestamp()));
            $todate = $formData['Timesheet']['start_time'];
            $model->type = $type;
            $model->start_time = DateTime::createFromFormat('d/m/Y h:i a', $todate . ' ' . $timein)->getTimestamp(); //strtotime($tempTimeEntry . ' ' . $timeIn);
            $model->end_time = DateTime::createFromFormat('d/m/Y h:i a', $todate . ' ' . $timeout)->getTimestamp(); //strtotime($tempTimeEntry . ' ' . $timeOut);
            $model->parent = $parent->id;
            if ($model->start_time > $model->end_time) {
                $status = false;
                return false;
            }
            ///*** Should come from constants
        } elseif ($type == TIMESHEET_TYPE_CUSTOM) { ///*** Should come from constants
            $timeInput = explode(" to ", $tempTimeEntry);
            if ($timeInput[0] > $timeInput[1]) {
                $status = false;
                return false;
            }
            if ($contract->timesheetTypeSel != null)
                $model->type = $contract->timesheetTypeSel;
            if (isset($type)) {
                $model->type = $type;
            }
            $model->start_time = DateTime::createFromFormat('d/m/Y H:i:s', $timeInput[0] . " 00:00:00")->getTimestamp(); //strtotime($timeInput[0]);
            $model->end_time = DateTime::createFromFormat('d/m/Y H:i:s', $timeInput[1] . " 00:00:00")->getTimestamp(); //strtotime($timeInput[1]);
        }

//        $model->expense = $formData['Timesheet']['expense'];
        $model->modified_at = time();
        $model->scenario = Timesheet::SCENARIO_TIMESHEET_ADD;
        if ($model->save()) {
            $rateArr = array();
            $hoursArr = array();
            $itemsArr = array();
            $itemvalArr = array();
            if (isset($formData['rates']) && count($formData['rates']) > 0)
                $rateArr = $formData['rates'];
            if (isset($formData['item_value']) && count($formData['item_value']) > 0)
                $itemvalArr = $formData['item_value'];

            if ($model->type == 'daily') {
                $payrate = AppTimeSheet::getPayrateByKey($model, 'regular');

                if ($payrate != null)
                    $formData['hours'][$payrate->id] = AppTimeSheet::calHrsDiff($model->start_time, $model->end_time); /// SEC TO HOUR   
            }

            if (isset($formData['hours'])) {
                if (isset($formData['Timesheet']['id'])) {
                    $hoursArr = $formData['hours'];
                    $model->updatePayrateEntries($rateArr, $hoursArr);
                } else {
                    $model->addPayrateEntries($formData['hours']); /// Payrate Entries Added
                }
            }
            if (isset($formData['items'])) {
                if (isset($formData['Timesheet']['id'])) {
                    $itemsArr = $formData['items'];
                    $model->updatePayrateEntries($itemvalArr, $itemsArr, 1);
                } else {
                    $model->addPayrateEntries($formData['items'], true); /// Items added
                }
            }
            return $model;
        }
        return $model->errors;
    }

    public static function decodePayrate($payrate) {
        return ucwords(str_replace('_', ' ', $payrate));
    }

    public static function encodePayrate($payrate) {
        return strtolower(trim(str_replace(' ', '_', $payrate)));
    }

    public static function get1WayTSheetTtl($tSheet) {

        if ($tSheet instanceof QuickTimesheet) {
            $total = ($tSheet->hours / 60) * $tSheet->rate;
            $total += self::get1WayExp($tSheet->id);
            return number_format((float) $total, 2, '.', '');
        }

        return 0;
    }

    public static function get1WayExp($id) {

        $sql = "SELECT SUM(expense_value) AS exTotal,quick_time_id FROM sp_quick_ts_expenses where quick_time_id = " . $id . " GROUP BY quick_time_id";


        $command = Yii::app()->db->createCommand($sql);

        $result = $command->queryRow();

        if (isset($result['exTotal'])) {
            return $result['exTotal'];
        }
        return 0;
    }

}
