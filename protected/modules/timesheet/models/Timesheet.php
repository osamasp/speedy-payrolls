<?php

/**
 * This is the model class for table "sp_timesheet".
 *
 * The followings are the available columns in table 'sp_timesheet':
 * @property string $id
 * @property string $contract_id
 * @property string $type
 * @property string $start_time
 * @property string $end_time
 * @property double $time
 * @property double $overtime
 * @property string $lunchtime
 * @property string $status
 * @property string $dispatch_status
 * @property integer $dispatch_flag
 * @property string $created_at
 * @property string $created_by
 * @property string $modified_at
 * @property string $modified_by
 * @property string $parent
 * @property double $expense
 * @property integer $merge_status
 * @property string $old_parent
 *
 * The followings are the available model relations:
 * @property Invoice[] $invoices
 * @property Payrate[] $payrates
 * @property Payroll[] $payrolls
 * @property Timesheet $parent0
 * @property Timesheet[] $timesheets
 * @property Timesheet $oldParent
 * @property Timesheet[] $timesheets1
 * @property Contract $contract
 * @property User $createdBy
 * @property User $modifiedBy
 */
class Timesheet extends CActiveRecord {

    const SCENARIO_TIMESHEET_ADD = "add";
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'sp_timesheet';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id, contract_id, type, start_time, end_time', 'required', 'on' => self::SCENARIO_TIMESHEET_ADD),
            array('id, contract_id, type, start_time, end_time, created_at, created_by, modified_at, modified_by', 'required'),
            array('dispatch_flag, merge_status', 'numerical', 'integerOnly' => true),
            array('time, overtime, expense', 'numerical'),
            array('id, contract_id, created_by, modified_by, parent, old_parent', 'length', 'max' => 255),
            array('type', 'length', 'max' => 7),
            array('lunchtime, created_at, modified_at', 'length', 'max' => 20),
            array('status', 'length', 'max' => 11),
            array('dispatch_status', 'length', 'max' => 10),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, contract_id, type, start_time, end_time, time, overtime, lunchtime, status, dispatch_status, dispatch_flag, created_at, created_by, modified_at, modified_by, parent, expense, merge_status, old_parent', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'invoices' => array(self::HAS_MANY, 'Invoice', 'timesheet_id'),
            'payrates' => array(self::HAS_MANY, 'Payrate', 'timesheet_id'),
            'payrolls' => array(self::HAS_MANY, 'Payroll', 'timesheet_id'),
            'parent0' => array(self::BELONGS_TO, 'Timesheet', 'parent'),
            'timesheets' => array(self::HAS_MANY, 'Timesheet', 'parent'),
            'oldParent' => array(self::BELONGS_TO, 'Timesheet', 'old_parent'),
            'timesheets1' => array(self::HAS_MANY, 'Timesheet', 'old_parent'),
            'contract' => array(self::BELONGS_TO, 'Contract', 'contract_id'),
            'createdBy' => array(self::BELONGS_TO, 'User', 'created_by'),
            'modifiedBy' => array(self::BELONGS_TO, 'User', 'modified_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'contract_id' => 'Contract',
            'type' => 'Type',
            'start_time' => 'Start Time',
            'end_time' => 'End Time',
            'time' => 'Time',
            'overtime' => 'Overtime',
            'lunchtime' => 'Lunchtime',
            'status' => 'Status',
            'dispatch_status' => 'Dispatch Status',
            'dispatch_flag' => 'Dispatch Flag',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'modified_at' => 'Modified At',
            'modified_by' => 'Modified By',
            'parent' => 'Parent',
            'expense' => 'Expense',
            'merge_status' => 'Merge Status',
            'old_parent' => 'Old Parent',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('contract_id', $this->contract_id, true);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('start_time', $this->start_time, true);
        $criteria->compare('end_time', $this->end_time, true);
        $criteria->compare('time', $this->time);
        $criteria->compare('overtime', $this->overtime);
        $criteria->compare('lunchtime', $this->lunchtime, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('dispatch_status', $this->dispatch_status, true);
        $criteria->compare('dispatch_flag', $this->dispatch_flag);
        $criteria->compare('created_at', $this->created_at, true);
        $criteria->compare('created_by', $this->created_by, true);
        $criteria->compare('modified_at', $this->modified_at, true);
        $criteria->compare('modified_by', $this->modified_by, true);
        $criteria->compare('parent', $this->parent, true);
        $criteria->compare('expense', $this->expense);
        $criteria->compare('merge_status', $this->merge_status);
        $criteria->compare('old_parent', $this->old_parent, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Timesheet the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function addPayrateEntries($cSettings = array(), $isItem = false) {



        if ($this->contract_id != null) {

            foreach ($cSettings as $key => $val) {

                $setting = AppContract::getContractSettingsByID($key);

                if ($setting == null)
                    continue;

                $payrate = new Payrate();
                $payrate->id = AppInterface::getUniqueId();
                $payrate->timesheet_id = $this->id;
                $payrate->contract_setting_id = $key;
                $payrate->value = $setting->value;
                $payrate->is_item = $isItem;
                if (!$isItem) {
                    $payrate->hours = $val;
                } else {
                    $payrate->item_value = $val;
                }
                $payrate->created_by = AppUser::getUserId();
                $payrate->modified_by = AppUser::getUserId();
                $payrate->created_at = time();

                $payrate->insert();
            }
        }
    }

    public function updatePayrateEntries($postArr = array(), $hoursArry = array(), $is_items = 0) {

        if (count($postArr) > 0) {

            foreach ($postArr as $key => $val) {

                $model = Payrate::model()->findByPk($key);
                if ($model != null && is_numeric($val)) {
                    $model->value = $val;

                    if (isset($hoursArry[$key])) {
                        if ($is_items == 0) {
                            $model->hours = $hoursArry[$key];
                        } else {
                            $model->item_value = $hoursArry[$key];
                        }
                    }

                    $model->modified_by = AppUser::getUserId();
                    $model->modified_at = time();
                    $model->update();
                }
            }
        } else {

            foreach ($hoursArry as $key => $val) {

                $model = Payrate::model()->findByPk($key);
                if ($model != null && is_numeric($val)) {
                    if ($is_items == 0) {
                        $model->hours = $val;
                    } else {
                        $model->item_value = $val;
                    }
                    $model->modified_by = AppUser::getUserId();
                    $model->modified_at = time();
                    $model->update();
                }
            }
        }
    }

    public function getcustompayrates() {
        return Payrate::model()->findAllByAttributes(array('timesheet_id' => $this->id, 'is_item' => 0));
    }

    public function getcustomitems() {
        return Payrate::model()->findAllByAttributes(array('timesheet_id' => $this->id, 'is_item' => 1));
    }

}
