<?php

/**
 * This is the model class for table "sp_payrate".
 *
 * The followings are the available columns in table 'sp_payrate':
 * @property string $id
 * @property string $timesheet_id
 * @property string $contract_setting_id
 * @property string $value
 * @property integer $status
 * @property string $created_at
 * @property string $created_by
 * @property string $modified_at
 * @property string $modified_by
 * @property double $hours
 * @property integer $is_item
 * @property string $item_value
 *
 * The followings are the available model relations:
 * @property Timesheet $timesheet
 * @property User $createdBy
 * @property User $modifiedBy
 * @property ContractSetting $contractSetting
 */
class Payrate extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sp_payrate';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, timesheet_id, contract_setting_id, value, status, created_at, created_by, modified_at, modified_by', 'required'),
			array('status, is_item', 'numerical', 'integerOnly'=>true),
			array('hours', 'numerical'),
			array('id, timesheet_id, contract_setting_id, value, created_by, modified_by', 'length', 'max'=>255),
			array('created_at, modified_at', 'length', 'max'=>20),
			array('item_value', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, timesheet_id, contract_setting_id, value, status, created_at, created_by, modified_at, modified_by, hours, is_item, item_value', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'timesheet' => array(self::BELONGS_TO, 'Timesheet', 'timesheet_id'),
			'createdBy' => array(self::BELONGS_TO, 'User', 'created_by'),
			'modifiedBy' => array(self::BELONGS_TO, 'User', 'modified_by'),
			'contractSetting' => array(self::BELONGS_TO, 'ContractSetting', 'contract_setting_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'timesheet_id' => 'Timesheet',
			'contract_setting_id' => 'Contract Setting',
			'value' => 'Value',
			'status' => 'Status',
			'created_at' => 'Created At',
			'created_by' => 'Created By',
			'modified_at' => 'Modified At',
			'modified_by' => 'Modified By',
			'hours' => 'Hours',
			'is_item' => 'Is Item',
			'item_value' => 'Item Value',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('timesheet_id',$this->timesheet_id,true);
		$criteria->compare('contract_setting_id',$this->contract_setting_id,true);
		$criteria->compare('value',$this->value,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('modified_at',$this->modified_at,true);
		$criteria->compare('modified_by',$this->modified_by,true);
		$criteria->compare('hours',$this->hours);
		$criteria->compare('is_item',$this->is_item);
		$criteria->compare('item_value',$this->item_value,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Payrate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
