<?php

/**
 * This is the model class for table "sp_quick_ts_expenses".
 *
 * The followings are the available columns in table 'sp_quick_ts_expenses':
 * @property string $id
 * @property string $expense_name
 * @property double $expense_value
 * @property string $comment
 * @property string $quick_time_id
 * @property double $created_at
 * @property double $created_by
 * @property double $modified_at
 * @property double $modified_by
 *
 * The followings are the available model relations:
 * @property QuickTimesheet $quickTime
 */
class QuickTsExpenses extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sp_quick_ts_expenses';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, expense_name, expense_value, quick_time_id,created_at, created_by, modified_at, modified_by', 'required'),
			array('expense_value', 'numerical'),
			array('id, quick_time_id', 'length', 'max'=>255),
			array('comment', 'length', 'max'=>512),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, expense_name, expense_value, comment, quick_time_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'quickTime' => array(self::BELONGS_TO, 'QuickTimesheet', 'quick_time_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'expense_name' => 'Expense Name',
                        'expense_value' => 'Expense Value',
			'comment' => 'Comment',
			'quick_time_id' => 'Quick Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('expense_name',$this->expense_name);
                $criteria->compare('expense_value', $this->expense_value);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('quick_time_id',$this->quick_time_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return QuickTsExpenses the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
