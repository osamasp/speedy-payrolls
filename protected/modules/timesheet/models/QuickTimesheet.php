<?php

/**
 * This is the model class for table "sp_quick_timesheet".
 *
 * The followings are the available columns in table 'sp_quick_timesheet':
 * @property double $id
 * @property double $hours
 * @property double $contact_id
 * @property double $date
 * @property string $status
 * @property double $currency_id
 * @property double $company_id
 * @property double $created_at
 * @property double $created_by
 * @property double $modified_at
 * @property double $modified_by
 * @property float $total
 * @property float $rate
 * @property string $job_title
 * @property string $job_reference
 * @property string $job_description
 * @property string reciept_file
 *
 * The followings are the available model relations:
 * @property QuickTsExpenses[] $quickTsExpenses
 */
class QuickTimesheet extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sp_quick_timesheet';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('hours, date, status, total, contact_id, job_title, job_reference, job_description, company_id, rate, currency_id, created_at, created_by, modified_at, modified_by', 'required'),
			array('hours, total, rate', 'numerical'),
			//array('id, contact_id', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, hours, contact_id, date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'hours' => 'Hours',
			'contact_id' => 'Contact',
			'date' => 'Date',
                        'status' => 'Status',
                        'total' => 'Total',
                        'rate' => 'Rate',
                        'job_title' => 'Matter',
                        'job_reference' => 'Activity',
                        'job_description' => 'Comment',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('hours',$this->hours);
		$criteria->compare('contact_id',$this->contact_id,true);
		$criteria->compare('date',$this->date,true);
                $criteria->compare('total', $this->total);
                $criteria->compare('rate', $this->rate);
                $criteria->compare('company_id', $this->company_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return QuickTimesheet the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
