<?php

class DefaultController extends Controller {

    public function actionIndex() {
        if(isset($_GET['id']))
        {
       $users = AppUser::get_all_users_by_company($_GET['id']);
        }
        else   $users = User::model()->findAll();
            
        $this->render('index', array('dataProvider' => $users));
    }

    public function actionViewTimesheets() {
        if (isset($_GET['id'])) {
            AppLogging::addLog('Viewing All Timesheets', 'success', 'application.timesheet.controller.main');

            $args = array('user' => $_GET['id']);
            /*             * * Filter By Contract Timesheets ** */
            if (Yii::app()->getRequest()->getParam("contract") != '') {
                $args['contract'] = Yii::app()->getRequest()->getParam("contract");
            }

            $result = AppTimeSheet::getTimeSheets($args);
            $this->render('//timesheet/main/index', array('entries' => $result['dataSet'], 'pages' => $result['pages']));
        }
    }

}
