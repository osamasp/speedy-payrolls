<?php

class MainController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
                //'postOnly ', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('signup'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'roles' => array('admin', 'super_admin'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('view'),
                'roles' => array('payroller'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete', 'export', 'service', 'getCompanies'),
                'roles' => array('admin', 'super_admin'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('usagepermission'),
                'roles' => array('super_admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionService() {
        $xml = "<Register>     <!--Optional:-->     <request>         <Customer>Demo Merchant</Customer>         <Language>eng</Language>         <version>2.0</version>         <Amount>10.00</Amount>         <Currency>AED</Currency>         <!--Optional:-->         <ExtraDataList>             <!--Zero or more repetitions:-->             <ExtraDataEntry>                 <!--Optional:-->                 <Name>Test Param</Name>                 <!--Optional:-->                 <Value>12</Value>             </ExtraDataEntry>         </ExtraDataList>         <!--Optional:-->         <OrderID> 123</OrderID>         <!--Optional:-->         <OrderInfo> Test Info</OrderInfo>         <!--Optional:-->         <OrderName> Test Name</OrderName>         <!--Optional:-->                  <ReturnPath>" . $this->createUrl('/site/index') . "</ReturnPath>         <!--Optional:-->         <TransactionHint>CPT:Y</TransactionHint>     </request> </Register>  ";
        try {
            $cer = "G:\Software\DemoMerchant-Certificates\Demo Merchant.cer";
            $sClient = new SoapClient("https://demo-ipg.comtrust.ae:2443/MerchantAPI.svc?singleWsdl", array("local_cert" => $cer, 'trace' => 1, "exception" => 0));
            $params = array("Customer" => "Demo Merchant",
                "Language" => "eng",
                "version" => 2.0,
                "Amount" => 10.00,
                "Currency" => "AED",
                "OrderID" => 123,
                "OrderInfo" => "Test Info",
                "OrderName" => "Test Name",
                "ReturnPath" => "http://yahoo.com",
                "TransactionHint" => "CPT:Y");


            //$response = $sClient->__soapCall("Register", $params);
            $response = $sClient->Register(array('request' => $params));

            var_dump($response);
        } catch (SoapFault $e) {
            var_dump($e->getMessage());
        }
    }

    public function actionExport() {
        $company = AppUser::getUserCompany();
        AppInterface::export(Company::model()->findAllByPk($company->id), array('name' => array(), 'email' => array(), 'phone' => array(), 'sector' => array()), true);
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->layout = '//layouts/main';
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionUsagePermission() {
        $payrollers = User::model()->findAllByAttributes(array('type' => 2));
        $companies = Company::model()->findAllByAttributes(array("status" => 1));
        if (isset($_POST['Company']) && isset($_POST['Payroller'])) {
            $id = $_POST['Company'];
            $payroller_id = $_POST['Payroller'];
            $company = Company::model()->findByAttributes(array('id' => $id));

            $company->usage = 1;
            $company->payroller_id = $payroller_id;
            $admin = $company->getadmin();

            $contracts = Contract::model()->findAllByAttributes(array('owner_id' => $admin->id));
            foreach ($contracts as $contract) {
                $contract->payroller_id = $payroller_id;
                $contract->update();
            }
            if ($company->update()) {
                Yii::app()->user->setFlash('success', 'Company successfully updated.');
                $this->redirect(array('/admin/default'));
            }
        }

        $this->render('usage', array(
            'companies' => $companies,
            'payrollers' => $payrollers,
        ));
    }

    public function actionPreferences() {
        $model = AppUser::getUserCompany();
        if (isset($_POST['Company'])) {

            $model->legal_name = $_POST['Company']['legal_name'];
            $model->is_enable_logo = isset($_POST['is_enable_logo']) && $_POST['is_enable_logo'] ? "1" : "0";
            $model->usage = $_POST['Company']['usage'];

            if ($_FILES['Company']['name']['logo']) {
                $model->logo = CUploadedFile::getInstance($model, 'logo');
                $images_path = realpath(Yii::app()->basePath . '/../uploads/photos');
                $filename = explode('.', $model->logo);
                $filename[0] = AppInterface::getFilename();
                $newfilename = $filename[0] . '.' . $filename[1];
                $model->logo->saveAs($images_path . '/' . $newfilename);
                $model->logo = $newfilename;
            }
            if ($model->update()) {
                Yii::app()->user->setFlash('success', 'Settings successfully updated.');
                $this->redirect(array('/user/main/view'));
            }
        }

        $this->render('preferences', array(
            'model' => $model,
        ));
    }

    public function actionEditProfile() {
        $user = AppUser::getCurrentUser();
        $model = AppUser::getUserCompany();
        if (isset($_POST['Company'])) {
            $user_email = AppUser::checkEmailExist($_POST['User']['email']);
            if ($user_email && $user->email != $_POST['User']['email']) {
                Yii::app()->user->setFlash('error', "This email already exist.");
                $this->redirect('editprofile');
            } else {
                $model->name = $_POST['Company']['name'];
                $user->first_name = $_POST['User']['first_name'];
                $user->last_name = $_POST['User']['last_name'];
                $user->email = $_POST['User']['email'];
                $model->phone = $_POST['Company']['phone'];
                $model->registration_number = $_POST['Company']['registration_number'];
                $model->vat_number = $_POST['Company']['vat_number'];
                $model->address = $_POST['Company']['address'];
                $model->country = $_POST['Company']['country'];
                $model->street = $_POST['Company']['street'];
                $model->city = $_POST['Company']['city'];
                $model->post_code = $_POST['Company']['post_code'];
                $model->created_by = 0;
                $model->created_at = time();
                $model->modified_by = 0;
                $model->modified_at = time();

                if ($_FILES['Company']['name']['logo']) {
                    $model->logo = CUploadedFile::getInstance($model, 'logo');
                    $images_path = realpath(Yii::app()->basePath . '/../uploads/photos');
                    $filename = explode('.', $model->logo);
                    $filename[0] = AppInterface::getFilename();
                    $newfilename = $filename[0] . '.' . $filename[1];
                    $model->logo->saveAs($images_path . '/' . $newfilename);
                    $model->logo = $newfilename;
                }
                if ($_FILES['Company']['name']['passport_copy']) {
                    $model->passport_copy = CUploadedFile::getInstance($model, 'passport_copy');
                    $images_path = realpath(Yii::app()->basePath . '/../uploads/photos');
                    $filename = explode('.', $model->passport_copy);
                    $filename[0] = AppInterface::getFilename();
                    $newfilename = $filename[0] . '.' . $filename[1];
                    $model->passport_copy->saveAs($images_path . '/' . $newfilename);
                    $model->passport_copy = $newfilename;
                }
                if ($_FILES['Company']['name']['bill_copy']) {
                    $model->bill_copy = CUploadedFile::getInstance($model, 'bill_copy');
                    $images_path = realpath(Yii::app()->basePath . '/../uploads/photos');
                    $filename = explode('.', $model->bill_copy);
                    $filename[0] = AppInterface::getFilename();
                    $newfilename = $filename[0] . '.' . $filename[1];
                    $model->bill_copy->saveAs($images_path . '/' . $newfilename);
                    $model->bill_copy = $newfilename;
                }
                if ($model->save() && $user->save()) {
                    Yii::app()->user->setFlash('success', 'Profile successfully updated.');
                    $this->redirect(array('/company/main/profile'));
                }
            }
        }

        $this->render('editing', array(
            'model' => $model,
            'user' => $user,
        ));
    }

    public function actionProfile() {
        $model = User::model()->findByPk(AppUser::getUserId());
        $company = AppUser::getUserCompany();
        $this->render('profile', array(
            'model' => $model,
            'company' => $company,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Company;

        if (isset($_POST['Company'])) {
            $model->attributes = $_POST['Company'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionGetcompanydetails($email) {
        $company = NULL;
        if ($user = User::prepareUserForAuthorisation($email)) {
            if (AppUser::isUserAdmin($user->id)) {
                $company = AppUser::getUserCompany($user->id);
            }
        }


        $response = array('status' => 1, 'data' => $this->renderPartial("_companyDetails", array("company" => $company, 'email' => $email, 'inputArrName' => (Yii::app()->request->getParam('inputArrName') != '') ? Yii::app()->request->getParam('inputArrName') : ''), true), 'company' => ($company != null) ? $company->id : 0);
        AppSetting::generateAjaxResponse($response);
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Company'])) {
            $model->attributes = $_POST['Company'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $company = $this->loadModel($id);
        $company->status = 0;
        $company->update();
        $this->redirect(array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $this->layout = '//layouts/column1';
        AppLogging::addLog('Company List', 'success', 'application.company.controller.main');
        $dataProvider = Company::model()->findAll();
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Company('search');
        $query = "SELECT DISTINCT c.user_id,c.company_id,co.*,u.plan_type,u.email FROM sp_contract c INNER JOIN sp_company co ON c.company_id=co.id INNER JOIN sp_user u ON c.user_id=u.id where co.status=1 and c.role='admin'";
        $models = Yii::app()->db->createCommand($query)->queryAll();
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Company']))
            $model->attributes = $_GET['Company'];

        $this->render('admin', array(
            'model' => $model,
            'models' => $models
        ));
    }

    public function actionSignup() {
        $this->layout = '//layouts/login';
        $company = new Company();
        $user = new User();
        $yiiuser = Yii::app()->user;
        if ($yiiuser->hasState('signup_email') && $yiiuser->hasState('signup_password')) {
            $user->email = $yiiuser->getState('signup_email');
            $user->password = $yiiuser->getState('signup_password');
            $user->plan_type = $yiiuser->getState('plan_type');
        } else {
            throw new CHttpException(400, 'Invalid Request.');
        }
        $user2 = new User();
        if (isset($_POST['Company'])) {
            $company->id = AppInterface::getUniqueId();
            $company->attributes = $_POST['Company'];
            $company->created_at = time();
            $company->modified_at = time();
            $company->created_by = 0;
            $company->modified_by = 0;
            $company->status = 1;
            $company->type = 1;
            if ($_FILES['Company']['name']['logo']) {
                $company->logo = CUploadedFile::getInstance($company, 'logo');
                $images_path = realpath(Yii::app()->basePath . '/../uploads/photos');
                $filename = explode('.', $company->logo);
                $filename[0] = AppInterface::getFilename();
                $newfilename = $filename[0] . '.' . $filename[1];
                $company->logo->saveAs($images_path . '/' . $newfilename);
                $company->logo = $newfilename;
            } else {
                $company->logo = "";
            }
            $company->save(false);

            $user->id = AppInterface::getUniqueId();
            $user->attributes = $_POST['User'][1];
            $user->created_at = time();
            $user->created_by = '0';
            $user->modified_at = time();
            $user->modified_by = '0';
            $user->plan_type = $yiiuser->getState('plan_type');
            $user->type = 1;
            $user->shift_id = 0;
            $user->staff_id = $_POST['User'][1]['staff_id'];
            $user->save();

//            setting company priviledges
            $set_company_priviledges = AppCompany::setCompanyPlan($company->id, $yiiuser->getState('plan_id'));

            //Add New Director Contract
            AppCompany::addContract(array('user_id' => $user->id, 'company_id' => $company->id, 'approver_id' => $user->id, 'role' => 'Director', 'status' => 1, 'start_time' => time(), 'end_time' => -1, 'type' => 'in', 'employee_type' => 'full_time', 'created_by' => 0, 'created_at' => time(), 'modified_by' => 0, 'modified_at' => time(), 'is_internal' => 1, 'owner_id' => $user->id));

            //Add New Admin Contract
            AppCompany::addContract(array('user_id' => $user->id, 'company_id' => $company->id, 'approver_id' => $user->id, 'role' => 'admin', 'status' => 1, 'start_time' => time(), 'end_time' => -1, 'type' => 'in', 'employee_type' => 'full_time', 'created_by' => 0, 'created_at' => time(), 'modified_by' => 0, 'modified_at' => time(), 'is_internal' => 1));

            $user->verification_code = User::createRandomPassword(50);
            $user->save();
            $url = Yii::app()->createAbsoluteUrl('/user/main/verifyemail') . '/vcode/' . $user->verification_code;
            $mailer = new AppMailer();
            $mailer->prepareBody('ConfirmEmail', array('NAME' => $user->full_name, 'CONFIRMLINK' => $url, 'EMAIL' => $user->email));
            $mailer->sendMail(array(array('email' => $user->email, 'name' => $user->full_name)), array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'SpeedyPayrolls'));
            
            $super_user = AppUser::get_super_user();
            //Mail to super admin
            $mailer1 = new AppMailer();
            $msg = 'A new company ' . $company->name . ' with admin user: ' . $user->full_name . ' with email address: ' . $user->email . ' has just signed up.';
            $mailer1->prepareBody('Email', array('BODY' => $msg));
            $mailer1->sendMail(array(array('email' => $super_user->email, 'name' => 'Super Admin')), array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls'));

            //notification to super admin
            $not_result = AppInterface::notification($super_user->id, 18, 0, 0, $user->id);
//            $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
//            $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;
//            $notifier = new Notification();
//            $notification_count = $notifier->getUserNotificationUnseenCount();
//            $notification = Yii::app()->getModule('notification');
//            $notification->send(array('server_notification', array('id' => $super_user->id, 'msg' => sprintf($not_result->notificationType->notice, $company->name), 'url' => $url, 'logo' => $logo, 'count' => $notification_count)), 'push');

            AppLogging::addLog('Company Signed up', 'success', 'application.company.controller.main');
            AppLogging::addLog('Contract1 Created', 'success', 'application.company.controller.main');
            AppLogging::addLog('Contract2 Created', 'success', 'application.company.controller.main');
            AppLogging::addLog('User1 Created', 'success', 'application.company.controller.main');
            AppLogging::addLog('User2 Created', 'success', 'application.company.controller.main');

            $this->redirect(Yii::app()->createUrl('/user/main/applyTemplate'));
        }
        $this->render('signup', array(
            'user' => $user,
            'user2' => $user2,
            'company' => $company
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Company the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Company::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Company $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'company-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionGetCompanies($id) {
        $response = array();
        $this->layout = "//layouts/blank";
        if (Yii::app()->getRequest()->isAjaxRequest) {
            $query = "SELECT DISTINCT c.user_id,c.company_id,co.*,u.plan_type,u.email FROM sp_contract c INNER JOIN sp_company co ON c.company_id=co.id INNER JOIN sp_user u ON c.user_id=u.id where co.status=" . $id . " and c.role='admin'";
            $models = Yii::app()->db->createCommand($query)->queryAll();
            if (count($models) > 0) {
                $pages = new CPagination(count($models));

                // results per page
                $pages->pageSize = 10;
                $pages->setPageSize(Yii::app()->params['example1_length']);
                $response = $this->renderPartial("getCompanies", array("model" => $models, 'pages' => $pages, 'page_size' => Yii::app()->params['example1_length']), true);
            } else {
                $response = "No Record Found";
            }
        } else {
            $response = "Not a Valid Ajax Request";
        }
        AppSetting::generateAjaxResponse($response);
    }

    public function actionGetEmployee() {

        $response = array('status' => false, "reason" => "");
        $this->layout = "//layouts/blank";
        if (Yii::app()->getRequest()->isAjaxRequest) {
            $companyId = Yii::app()->getRequest()->getPost("company_id");
            $isinout = Yii::app()->getRequest()->getPost("isinout");
            $data = AppCompany::getCompanyEmployee($companyId);
            if (count($data) > 0) {
                $payrates = AppCompany::getPayrateNames($companyId);
                $items = AppCompany::getItems($companyId);
                if (!$payrates) {
                    $template = Template::model()->findByAttributes(array('name' => 'Default'));
                    $payrates = AppTemplate::getPayrates($template->id);
                    $items = AppTemplate::getItems($template->id);
                }
                $response['payrates'] = $this->renderPartial('_payratesanditems', array('payrates' => $payrates, 'items' => $items, 'isinout' => $isinout), true);
                $response['data'] = $this->renderPartial("_getEmployee", array("contracts" => $data), true);
                $response['status'] = true;
            } else {
                $template = Template::model()->findByAttributes(array('name' => 'Default'));
                $payrates = AppTemplate::getPayrates($template->id);
                $items = AppTemplate::getItems($template->id);
                $response['payrates'] = $this->renderPartial('_payratesanditems', array('payrates' => $payrates, 'items' => $items, 'isinout' => $isinout), true);
                $response['status'] = true;
                $response['data'] = "";
                $response['reason'] = "No Record Found";
            }
        } else {
            $response['reason'] = "Not a Valid Ajax Request";
        }
        AppSetting::generateAjaxResponse($response);
    }

}
