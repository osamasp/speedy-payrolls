<?php

class CompanyShiftsController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'viewshiftusers', 'shiftexport', 'export', 'updatestatus', 'clockentries', 'timeinout', 'timesheetreport', 'updateapprovallist', 'viewdetails'),
                'roles' => array('employee', 'admin'),
            ),
            array('allow',
                'actions' => array('missingentries', 'alreadytimein', 'approvallist', 'addnote', 'timein', 'lunchin', 'lunchout', 'timeout', 'updateshift'),
                'users' => array('*'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'roles' => array('employee', 'admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->layout = "//layouts/blank";
        $model = $this->loadModel($id);
        $response = array('status' => false, 'data' => '', 'reason' => 'Not a Valid Ajax Request or Post Data is Missing');
        $model->start_time = date('h:i a', $model->start_time);
        $model->end_time = date('h:i a', $model->end_time);
        $response['status'] = true;
        $response['reason'] = '';

        $response['data'] = $this->render('view', array(
            'model' => $model,
                ), true);

        AppSetting::generateAjaxResponse($response);
    }

    public function actionClockEntries() {
        $user = AppUser::getUserId();
        $my_shifts = AppCompany::getClockEntries($user);
        $this->render('clockentries', array("my_shifts" => $my_shifts));
    }

    public function actionViewShiftUsers() {
        $users = AppCompany::getShiftUsers();
//        dd($users);
        $this->render('shiftusers', array('users' => $users));
    }

    public function actionAddNote() {
        if ($_POST) {
            $model = AppCompany::addNote($_POST, ConstantMessages::$note_type_user_timing);
            if (isset($model)) {
                Yii::app()->user->setFlash('success', "Note has been addedd successfully.");
                if(isset($_POST['url']))
                    $this->redirect ($_POST['url']);
                else
                    $this->redirect($this->createUrl('/company/companyShifts/timeinout/contract_id/' . $_POST['contract_id']));
            } else {
                dd($model->errors);
            }
        }

        $this->layout = "//layouts/blank";
        $user_timing_id = Yii::app()->getRequest()->getParam('id');
        $contract_id = Yii::app()->getRequest()->getParam('contract_id');
        $response = array('status' => false, 'data' => '', 'reason' => 'Not a Valid Ajax Request or Post Data is Missing');
        $model = UserTimings::model()->findByPk($user_timing_id);
//            $lunches = UserLunch::model()->findAllByAttributes(array("user_timing_id"=>$user_timing_id));
        $response['status'] = true;
        $response['reason'] = '';
        $response['data'] = $this->render('addnote', array('model' => $model, 'id' => $user_timing_id, 'contract_id' => $contract_id), true);
        AppSetting::generateAjaxResponse($response);
    }

    public function actionUpdateStatus() {
        $this->layout = "//layouts/blank";
        $user_timing_id = $_GET["id"];
        $model = UserTimings::model()->findByPk($user_timing_id);
        $response = array('status' => false, 'data' => '', 'reason' => 'Not a Valid Ajax Request or Post Data is Missing');
        $lunches = UserLunch::model()->findAllByAttributes(array("user_timing_id" => $user_timing_id));
        $notes = TimesheetNotes::model()->findAllByAttributes(array("user_timing_id" => $user_timing_id));
        $response['status'] = true;
        $response['reason'] = '';

        $response['data'] = $this->render('approvalListDetails', array(
            'model' => $model,
            'lunches' => $lunches,
            'notes' => $notes,
                ), true);
        AppSetting::generateAjaxResponse($response);
    }

    public function actionUpdateShift() {
        if ($_POST) {
            $total_time = 0;
            $total_lunch = 0;
            $user = AppUser::getUserId();
            $model = UserTimings::model()->findByPk($_POST["id"]);
            $client_timezone = Yii::app()->session["client_timezone"];
            preg_match_all('!\d+!', $client_timezone, $matches);
//                dd(date('h:i a', strtotime($_POST["UserTimings"]["time_in"])-($matches[0][0]*3600)));
            UserLunch::model()->deleteAllByAttributes(array("user_timing_id" => $_POST["id"]));
            for ($i = 1; $i <= $_POST["count"]; $i++) {
                $lunch = new UserLunch();
                $lunch->id = AppInterface::getUniqueId();
                $lunch_in = strtotime($_POST["lunch_in" . $i]) - (($matches[0][0] * 3600) - 3600);
                $lunch_out = strtotime($_POST["lunch_out" . $i]) - (($matches[0][0] * 3600) - 3600);
                $lunch->lunch_in = $lunch_in;
                $lunch->lunch_out = $lunch_out;
                $lunch->user_timing_id = $_POST["id"];
                $lunch->created_at = time();
                $lunch->created_by = $user;
                $lunch->modified_at = time();
                $lunch->modified_by = $user;
                $lunch->save();
            }
            $lunches = UserLunch::model()->findAllByAttributes(array("user_timing_id" => $_POST["id"]));
            foreach ($lunches as $lunch) {
                $temp = $lunch->lunch_out - $lunch->lunch_in;
                $total_lunch += $temp;
            }
            $model->time_in = strtotime($_POST["UserTimings"]["time_in"]) - (($matches[0][0] * 3600) - 3600);
            $model->time_out = strtotime($_POST["UserTimings"]["time_out"]) - (($matches[0][0] * 3600) - 3600);
            $total_time = ($model->time_out - $model->time_in) - $total_lunch;
            $model->total_time = $total_time;
            $model->total_lunch = $total_lunch;
            $model->modified_at = time();
            $model->modified_by = AppUser::getUserId();
            if ($model->save()) {
                Yii::app()->user->setFlash('success', "Timesheet updated successfully.");
                $this->redirect('clockentries');
            }
        }
        $this->layout = "//layouts/blank";
        $user_timing_id = $_GET["id"];
        $response = array('status' => false, 'data' => '', 'reason' => 'Not a Valid Ajax Request or Post Data is Missing');
        $model = UserTimings::model()->findByPk($user_timing_id);
        $lunches = UserLunch::model()->findAllByAttributes(array("user_timing_id" => $user_timing_id));
        $response['status'] = true;
        $response['reason'] = '';
        $response['data'] = $this->render('updateShift', array('model' => $model, 'lunches' => $lunches), true);
        AppSetting::generateAjaxResponse($response);
    }

    public function actionViewDetails() {
        $this->layout = "//layouts/blank";
        $user_timing_id = Yii::app()->getRequest()->getPost("id");
        $data = AppCompany::getClockEntryDetails($user_timing_id);
        $model = $data['model'];
        $response = array('status' => false, 'data' => '', 'reason' => 'Not a Valid Ajax Request or Post Data is Missing');
        $lunches = $data['lunches'];
        $notes = $data['notes'];
        $response['status'] = true;
        $response['reason'] = '';

        $response['data'] = $this->render('timingDetails', array(
            'model' => $model,
            'lunches' => $lunches,
            'notes' => $notes,
                ), true);
        AppSetting::generateAjaxResponse($response);
    }

    public function actionTimesheetReport() {
        $model = new UserTimings();
        $isAdmin = AppUser::isUserAdmin();
        $employees = null;
        $date_group = false;
        $is_html = false;
        $is_general = Yii::app()->getRequest()->queryString;
        if ($isAdmin && $is_general != '') {
            $employees = AppUser::get_all_users_by_company();
        }
        if ($_POST) {
            $data = AppCompany::getTimesheetsForReport($_POST, $is_general);
            $timesheets = $data["timesheets"];
            $date_group = $data["date_group"];
            $this->layout = '//layouts/blank_head';
            if ($_POST["export"] == 1) {
                $this->layout = '//layouts/column1';
                //If export data in html
                $is_html = true;
                $this->render('generatereport', array('model' => $timesheets, 'employees' => $employees, 'date_group' => $date_group, 'is_html' => $is_html));
                return;
            } else {
                //if export data in pdf
                $is_html = false;
                $mPDF1 = new mpdf();
                $mPDF1->WriteHTML($this->render('generatereport', array('model' => $timesheets, 'employees' => $employees, 'date_group' => $date_group, 'is_html' => $is_html), true));
//                    dd($html);
                $mPDF1->Output();
                return;
            }
        }
        $this->layout = '//layouts/column1';
        $this->render('report', array('model' => $model, 'employees' => $employees, 'is_general' => $is_general));
    }

    public function GetReport($timesheets) {
        $R = new PHPReport();
        $R->load(array(
            'id' => 'product',
            'header' => array(
                'created_at' => 'Date', 'time_in' => 'Time In', 'time_out' => 'Time Out', 'lunch' => 'Lunch', 'total_time' => 'Total Hours'
            ),
//			'footer'=>array(
//					'product'=>'','price'=>28.54,'tax'=>17.89
//				),
            'config' => array(
                'header' => array(
                    'created_at' => array('width' => 40, 'align' => 'center'),
                    'time_in' => array('width' => 40, 'align' => 'center'),
                    'time_out' => array('width' => 40, 'align' => 'center'),
                    'lunch' => array('width' => 40, 'align' => 'center'),
                    'total_time' => array('width' => 40, 'align' => 'center')
                ),
                'data' => array(
                    'created_at' => array('align' => 'left'),
                    'time_in' => array('align' => 'right'),
                    'time_out' => array('align' => 'right'),
                    'lunch' => array('align' => 'right'),
                    'total_time' => array('align' => 'right')
                )
//					'footer'=>array(
//						'price'=>array('align'=>'right'),
//						'tax'=>array('align'=>'right')
//					)
            ),
            'data' => $timesheets
//			'group'=>array(
//					'caption'=>array(
//						'Category 1', 'Another category'
//					),
//					'rows'=>array(
//						array(0),
//						array(1,2)
//					),
//					'summary'=>array(
//						array('product'=>'','price'=>23.99,'tax'=>12),
//						array('product'=>'','price'=>5.45,'tax'=>5.75)
//					)
//				),
//			'format'=>array(
//					'price'=>array('number'=>array('prefix'=>'$','decimals'=>2)),
//					'tax'=>array('number'=>array('sufix'=>' EUR','decimals'=>1))
//				)
                )
        );

        echo $R->render('html');
    }

    public function actionAlreadyTimeIn($contract_id) {
        $user_id = AppUser::getUserId();
        $already_timein = UserTimings::model()->findAllByAttributes(array("user_id" => $user_id, "contract_id" => $contract_id, "created_at" => strtotime(date('m/d/Y'))));
        if (count($already_timein) > 0) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function actionTimeInOut() {
        $user = AppUser::getUserId();
        $model = new UserTimings();
        $lunches = array();
        $notes = array();
        $timeIn = false;
        $contracts = AppContract::getAllContracts($user);

        $contract_id = AppContract::getInternalContract($user)->id;

        if (Yii::app()->request->getParam('contract_id') != NULL)
            $contract_id = Yii::app()->request->getParam('contract_id');

        $model->contract_id = $contract_id;

        // add Condition here for between timein and out ... DONOT USE CREATED_AT field....
        $already_timein = UserTimings::model()->findByAttributes(array("user_id" => $user, "contract_id" => $contract_id, "created_at" => strtotime(date('m/d/Y'))));
        $timeIn = count($already_timein) > 0 ? true : false;


        if ($timeIn) {
            $model = $already_timein;
            $lunches = UserLunch::model()->findAllByAttributes(array("user_timing_id" => $already_timein->id));
            $notes = TimesheetNotes::model()->findAllByAttributes(array("user_timing_id" => $already_timein->id));
        } else {
            $model->contract_id = $contract_id;
        }

        $this->render('timings', array(
            'model' => $model,
            'lunches' => $lunches,
            'timeIn' => $timeIn,
            'notes' => $notes,
            'contracts' => $contracts,
            'contract_id' => $contract_id,
        ));
    }

    public function actionLunchIn() {
        $user = AppUser::getUserId();
        $time_id = $_GET['time_id'];
        $lunches = UserLunch::model()->findAllByAttributes(array("user_timing_id" => $time_id));
        $models = UserTimings::model()->findByPk($time_id);
        $model = AppCompany::lunchIn($user, $time_id);
        if (isset($model)) {
            $lunches = UserLunch::model()->findAllByAttributes(array("user_timing_id" => $time_id));
            echo $this->renderPartial("clocks", array("model" => $models, "lunches" => $lunches, "lunch_id" => $model->id));
        } else
            dd($model->errors);
    }

    public function actionLunchOut() {
        $user = AppUser::getUserId();
        $time_id = $_GET['time_id'];
        $lunch_id = $_GET['lunch_id'];
        $models = UserTimings::model()->findByPk($time_id);
        $model = AppCompany::lunchOut($lunch_id);
        if (isset($model)) {
            $lunches = UserLunch::model()->findAllByAttributes(array("user_timing_id" => $time_id));
            echo $this->renderPartial("clocks", array("model" => $models, "lunches" => $lunches, "lunch_id" => $model->id));
        } else {
            dd($model->errors);
        }
    }

    public function actionTimeIn() {
        $user = AppUser::getUserId();
//        $contract = AppContract::getInternalContract($user);
        $contract_id = Yii::app()->request->getParam('contract');
        $already_timein = AppCompany::alreadyTimeIn($user, $contract_id);
        if (count($already_timein) > 0) {
            echo "null";
        } else {
            $model = AppCompany::timeIn($user, $contract_id);
            if (isset($model)) {
                $lunches = UserLunch::model()->findAllByAttributes(array("user_timing_id" => $model->id));
                echo $this->renderPartial("clocks", array("model" => $model, "lunches" => $lunches, "lunch_id" => 0));
            } else
                dd($model->errors);
        }
    }

    public function actionApprovalList() {
        $model = AppCompany::getApprovalList();
        $this->render('approvallist', array("model" => $model));
    }

    public function actionUpdateApprovalList() {
        $timesheet_map = Timesheet::model()->findByPk($_POST["id"]);
        if (count($timesheet_map) > 0) {
            echo "failed";
//                $this->redirect(array('approvallist'));
        } else {
            if (AppCompany::updateApprovalListStatus($_POST))
                echo "success";
        }
    }

    public function actionMissingEntries() {
        $user_id = AppUser::getUserId();
        $user = User::model()->findByPk($user_id);
        $model = UserTimings::model()->findAllByAttributes(array("user_id" => $user_id, "created_at" => strtotime(date('d/m/Y')), "time_out" => 0));
        if (count($model) > 0) {
            $mailer = new AppMailer();
            $msg = "Dear " . $user->first_name . ",<br>It is to notify you that you forgot to time out for yesterday timesheet.<br>You are requested to update your timesheet.<br><br>Thanks";
            $mailer->prepareBody('Email', array('BODY' => $msg));
            $mailer->sendMail(array(array('email' => $user->email, 'name' => $user->first_name)), array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls'));
            Yii::app()->user->setFlash('danger', $msg);
        }
        $this->redirect(Yii::app()->createAbsoluteUrl('site/index'));
    }

    public function actionTimeOut() {
        $time_id = $_GET['time_id'];
        $total_time = $_GET['total_time'];
        $lunches = UserLunch::model()->findAllByAttributes(array("user_timing_id" => $time_id));
        $model = AppCompany::timeOut($time_id);
        if (isset($model)) {
            echo $this->renderPartial("clocks", array("model" => $model, "lunches" => $lunches, "lunch_id" => 0));
        } else
            dd($model->errors);
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new CompanyShifts;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['CompanyShifts'])) {
//                    dd(DateTime::createFromFormat('h:i a', $_POST['CompanyShifts']['end_time'])->getTimestamp());

            $model = AppCompany::addCompanyShift($_POST['CompanyShifts'], AppUser::getUserId(), AppUser::getUserCompany()->id);
            if ($model instanceof CompanyShifts) {
                Yii::app()->user->setFlash('success', "New shift has been addedd successfully.");
                $this->redirect(array('index'));
            } else if($model == false){
                $model = new CompanyShifts;
                Yii::app()->user->setFlash('error', "End Time cannot be greater than Start Time");
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        $model->start_time = date('h:i a', $model->start_time);
        $model->end_time = date('h:i a', $model->end_time);
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['CompanyShifts'])) {
//                    dd($_POST['CompanyShifts']);
            $model->attributes = $_POST['CompanyShifts'];
            $model->start_time = DateTime::createFromFormat('h:i a', $_POST['CompanyShifts']['start_time'])->getTimestamp();
            $model->end_time = DateTime::createFromFormat('h:i a', $_POST['CompanyShifts']['end_time'])->getTimestamp();
            $model->modified_at = time();
            $model->modified_by = AppUser::getUserId();
            if ($model->save()) {
                Yii::app()->user->setFlash('success', "Company shift has been updated successfully.");
                $this->redirect(array('index'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $is_assigned = AppCompany::companyShiftAssigned($id);

        if ($is_assigned) {
            Yii::app()->user->setFlash('error', 'Cannot delete this shift because the users are working on this shift.');
            $this->redirect(array('index'));
        } else {
            $model = $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
//		if($model->delete())
            Yii::app()->user->setFlash('success', 'Successfully deleted.');
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $company = AppUser::getUserCompany();
        $dataProvider = CompanyShifts::model()->findAllByAttributes(array("company_id" => $company->id));
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionExport() {
        $webroot = Yii::getPathOfAlias('webroot');
        if (!is_dir($webroot . '/uploads/company_shift')) {
            mkdir($webroot . '/uploads/company_shift');
            chmod(($webroot . '/uploads/company_shift'), 0755);
            // the default implementation makes it under 777 permission, which you could possibly change recursively before deployment, but here's less of a headache in case you don't
        }
        $file = $webroot . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'company_shift' . DIRECTORY_SEPARATOR . 'approval_list-' . date('d-m-Y-H-i') . '.csv';
        $handle = fopen($file, 'w');
        $user_id = AppUser::getUserId();
        $sql = "SELECT ";
        $labels = array();
        if (isset($_POST['export_fields'])) {
            $last_item = end($_POST['export_fields']);
            foreach ($_POST['export_fields'] as $item) {
                if ($item == $last_item) {
                    $sql .= "U." . $item;
                } else {
                    $sql .= "U." . $item . ",";
                }
                array_push($labels, $item);
            }
        } else {
            $sql .= "U.created_at,U.title,U.start_time,U.end_time,C.name";
//            $labels = array(UserTimings::model()->getAttributeLabel('created_at'), UserTimings::model()->getAttributeLabel('time_in'), UserTimings::model()->getAttributeLabel('time_out'), UserTimings::model()->getAttributeLabel('total_time'), UserTimings::model()->getAttributeLabel('total_lunch'), UserTimings::model()->getAttributeLabel('status'));
            $labels = array('created_at', 'title', 'start_time', 'end_time', 'name');
        }
        if (isset($_POST['entry'])) {
            $selected_rows = "";
            $last_row = end($_POST['entry']);
            foreach ($_POST['entry'] as $entry) {
                if ($entry == $last_row) {
                    $selected_rows .= $entry;
                } else {
                    $selected_rows .= $entry . ",";
                }
            }
            $sql .= " FROM sp_company_shifts U INNER JOIN sp_company C ON U.company_id=C.id WHERE U.company_id=" . AppUser::getUserCompany()->id . " AND U.id IN(" . $selected_rows . ") order by U.created_at";
        } else {
            $sql .= " FROM sp_company_shifts U INNER JOIN sp_company C ON U.company_id=C.id WHERE U.company_id=" . AppUser::getUserCompany()->id . " order by U.created_at";
        }
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        AppInterface::GenerateCSV($file, $labels, $data);
        fclose($handle);
        $curr_user = AppUser::getCurrentUser();
        $url = $this->createAbsoluteUrl('/uploads/company_shift/' . 'approval_list-' . date('d-m-Y-H-i') . '.csv');
        $this->layout = "//layouts/blank";
        $response = array('status' => false, 'data' => '', 'reason' => 'Not a Valid Ajax Request or Post Data is Missing');
        $response['status'] = true;
        $response['reason'] = '';
        $response['url'] = $url;
        $response['data'] = $this->render('viewlist', array(
            'file' => $file,
            'name' => 'timesheets-' . date('d-m-Y-H-i') . '.csv',
            'url' => $url
                ), true);
        AppSetting::generateAjaxResponse($response);
    }

    public function actionSendEmployees($file, $name) {
        $curr_user = AppUser::getCurrentUser();
        $mailer = new AppMailer();
        $msg = 'Attached is the exported file you requested.';
        $mailer->prepareBody('invoice', array('INVOICE' => $msg));

        $mailer->sendSesMailWithAttach(array('email' => $curr_user->email, 'name' => $curr_user->full_name), array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls'), "SpeedyPayroll Notification", $msg, $file, $name);
        Yii::app()->user->setFlash('success', 'Export successfully, an email will be sent to you shortly.');
        $this->redirect($this->createUrl("/user/employee"));
    }

    public function actionShiftExport() {
        $webroot = Yii::getPathOfAlias('webroot');
        if (!is_dir($webroot . '/uploads/company_shift')) {
            mkdir($webroot . '/uploads/company_shift');
            chmod(($webroot . '/uploads/company_shift'), 0755);
            // the default implementation makes it under 777 permission, which you could possibly change recursively before deployment, but here's less of a headache in case you don't
        }
        $file = $webroot . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'company_shift' . DIRECTORY_SEPARATOR . 'company_shift-' . date('d-m-Y-H-i') . '.csv';
        $handle = fopen($file, 'w');
        $user_id = AppUser::getUserId();
//        $data = AppCompany::getApprovalList();
        $sql = "SELECT ";
        $labels = array();
        if (isset($_POST['export_fields'])) {
            $last_item = end($_POST['export_fields']);
            foreach ($_POST['export_fields'] as $item) {
                if ($item == $last_item) {
                    if ($item == "user_id") {
                        $sql .= "CONCAT(C.first_name,' ',C.last_name) AS name";
                    } else {
                        $sql .= "U." . $item;
                    }
                } else {
                    if ($item == "user_id") {
                        $sql .= "CONCAT(C.first_name,' ',C.last_name) AS name,";
                    } else {
                        $sql .= "U." . $item . ",";
                    }
                }
                if ($item == "user_id") {
                    array_push($labels, "name");
                } else {
                    array_push($labels, $item);
                }
            }
        } else {
            $sql .= "CONCAT(C.first_name,' ',C.last_name) AS name, U.created_at,U.time_in,U.time_out,U.total_time,U.total_lunch,U.status";
//            $labels = array(UserTimings::model()->getAttributeLabel('created_at'), UserTimings::model()->getAttributeLabel('time_in'), UserTimings::model()->getAttributeLabel('time_out'), UserTimings::model()->getAttributeLabel('total_time'), UserTimings::model()->getAttributeLabel('total_lunch'), UserTimings::model()->getAttributeLabel('status'));
            $labels = array('name', 'created_at', 'time_in', 'time_out', 'total_time', 'total_lunch', 'status');
        }
        if (isset($_POST['entry'])) {
            $selected_rows = "";
            $last_row = end($_POST['entry']);
            foreach ($_POST['entry'] as $entry) {
                if ($entry == $last_row) {
                    $selected_rows .= $entry;
                } else {
                    $selected_rows .= $entry . ",";
                }
            }
            $sql .= " FROM sp_user_timings U INNER JOIN sp_user C ON U.user_id=C.id WHERE U.user_id=" . $user_id . " AND U.id IN(" . $selected_rows . ") order by U.created_at";
        } else {
            $sql .= " FROM sp_user_timings U INNER JOIN sp_user C ON U.user_id=C.id WHERE U.user_id=" . $user_id . " order by U.created_at";
        }
        $data = Yii::app()->db->createCommand($sql)->queryAll();
//        dd($data);
        AppInterface::GenerateCSV($file, $labels, $data);
        fclose($handle);
        $url = $this->createAbsoluteUrl('/uploads/company_shift/' . 'company_shift-' . date('d-m-Y-H-i') . '.csv');
//        header("Content-disposition: attachment;filename=".AppInterface::getFilename().".csv");
        $this->layout = "//layout/blank";
        $response = array('status' => false, 'data' => '', 'reason' => 'Not a Valid Ajax Request or Post Data is Missing');
        $response['status'] = true;
        $response['reason'] = '';
        $response['url'] = $url;
        $response['data'] = $this->render('//user/employee/viewlist', array(
            'file' => $file,
            'name' => 'company_shift-' . date('d-m-Y-H-i') . '.csv',
            'url' => $url
                ), true);
        AppSetting::generateAjaxResponse($response);
        Yii::app()->request->sendFile(AppInterface::getFilename() . '.csv', file_get_contents($file));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new CompanyShifts('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['CompanyShifts']))
            $model->attributes = $_GET['CompanyShifts'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return CompanyShifts the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = CompanyShifts::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CompanyShifts $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'company-shifts-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
