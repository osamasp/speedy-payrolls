<?php

class MainController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('signup'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'index', 'view',),
                'roles' => array('admin', 'super_admin',),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('view'),
                'roles' => array('payroller'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'roles' => array('admin', 'super_admin',),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->layout = '//layouts/main';
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Company;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Company'])) {
            $model->attributes = $_POST['Company'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Company'])) {
            $model->attributes = $_POST['Company'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $this->layout = '//layouts/column1';
        AppLogging::addLog('Company List', 'success', 'application.company.controller.main');
        $dataProvider = Company::model()->findAll();
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Company('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Company']))
            $model->attributes = $_GET['Company'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionSignup() {
        $this->layout = '//layouts/login';
        $payrollers = User::model()->findAllByAttributes(array('type' => 2));
        $company = new Company();
        $user = new User();
        //dd($_POST);
        $yiiuser = Yii::app()->user;
        if ($yiiuser->hasState('signup_email') && $yiiuser->hasState('signup_password')) {
            $user->email = $yiiuser->getState('signup_email');
            $user->password = $yiiuser->getState('signup_password');
        } else {
            throw new CHttpException(400, 'Invalid Request.');
        }
        $user2 = new User();
        if (isset($_POST['Company'])) {
            $company->id = AppInterface::getUniqueId();
            $company->attributes = $_POST['Company'];
            $company->usage = $_POST['company_usage'];
            $company->payroller_id = $_POST['payroller'];
            $company->created_at = time();
            $company->modified_at = time();
            $company->created_by = '0';
            $company->modified_by = '0';
            $company->status = 1;
            $company->type = 1;
            $company->save();


            $payrollers = User::model()->findByPk($company->payroller_id);


            $mailer = new AppMailer();
            $msg = 'You have been nominated by ' . AppUser::getCurrentUser()->first_name . ' to become their Approver on SpeedyPayrolls.';
            $mailer->prepareBody('SpeedyPayrolls Notification', array('BODY' => $msg));
            $mailer->sendMail(array(array('email' => $payrollers->email, 'name' => $payrollers->first_name)), array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payroll'));
            AppInterface::notification($msg, $payrollers->id);


            if ($_POST['PayrateName']) {
                foreach ($_POST['PayrateName'] as $name) {
                    $pn = new PayrateName;
                    $pn->id = AppInterface::getUniqueId();
                    $pn->company_id = $company->id;
                    $pn->name = str_replace(' ', '_', strtolower($name));
                    $pn->save();
                }
            }

            $user->id = AppInterface::getUniqueId();
            $user->attributes = $_POST['User'][1];
            $user->created_at = time();
            $user->created_by = '0';
            $user->modified_at = time();
            $user->modified_by = '0';
            $user->type = 1;
            $user->save();

            $contract2 = new Contract;
            $contract2->id = AppInterface::getUniqueId();
            $contract2->user_id = $user->id;
            $contract2->company_id = $company->id;
            $contract2->approver_id = $user->id;
            $contract2->role = 'Director';
            $contract2->status = 1;
            $contract2->start_time = time();
            $contract2->end_time = -1;
            $contract2->type = "in";
            $contract2->employee_type = "full_time";
            $contract2->created_by = 0;
            $contract2->created_at = time();
            $contract2->modified_by = 0;
            $contract2->modified_at = time();
            $contract2->is_internal = 1;
            $contract2->save();

            if ($_POST['Settings']['admin'] == 2) {
                $user2->id = AppInterface::getUniqueId();
                $user2->attributes = $_POST['User'][2];
                $user2->password = md5($_POST['User'][2]['password']);
                $user2->created_at = time();
                $user2->created_by = '0';
                $user2->modified_at = time();
                $user2->modified_by = '0';
                $user2->type = 1;
                $user2->save();

                $contract = new Contract;
                $contract->id = AppInterface::getUniqueId();
                $contract->user_id = $user2->id;
                $contract->company_id = $company->id;
                $contract->approver_id = $user2->id;
                $contract->role = $_POST['Contract'][2]['role'];
                $contract->status = 1;
                $contract->start_time = time();
                $contract->end_time = -1;
                $contract->type = "in";
                $contract->employee_type = "full_time";
                $contract->created_by = 0;
                $contract->created_at = time();
                $contract->modified_by = 0;
                $contract->modified_at = time();
                $contract2->is_internal = 1;
                $contract->save();

                $contract3 = new Contract;
                $contract3->id = AppInterface::getUniqueId();
                $contract3->user_id = $user2->id;
                $contract3->company_id = $company->id;
                $contract3->approver_id = $user->id;
                $contract3->role = 'admin';
                $contract3->status = 1;
                $contract3->start_time = time();
                $contract3->end_time = -1;
                $contract3->type = "in";
                $contract3->employee_type = "full_time";
                $contract3->created_by = 0;
                $contract3->created_at = time();
                $contract3->modified_by = 0;
                $contract3->modified_at = time();
                $contract3->save();
            } else {
                $contract4 = new Contract;
                $contract4->id = AppInterface::getUniqueId();
                $contract4->user_id = $user->id;
                $contract4->company_id = $company->id;
                $contract4->approver_id = $user->id;
                $contract4->role = 'admin';
                $contract4->status = 1;
                $contract4->start_time = time();
                $contract4->end_time = -1;
                $contract4->type = "in";
                $contract4->employee_type = "full_time";
                $contract4->created_by = 0;
                $contract4->created_at = time();
                $contract4->modified_by = 0;
                $contract4->modified_at = time();
                $contract4->save();
            }

            $user->verification_code = User::createRandomPassword(50);
            $user->save();
            $url = Yii::app()->createAbsoluteUrl('/user/main/verifyemail') . '/vcode/' . $user->verification_code;
            $mailer = new AppMailer();
            $mailer->prepareBody('ConfirmEmail', array('NAME' => $user->full_name, 'CONFIRMLINK' => $url, 'EMAIL' => $user->email));
            $mailer->sendMail(array(array('email' => $user->email, 'name' => $user->full_name)), array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'SpeedyPayrolls'));

            $status = AppUser::loginUser($user->email, $user->password);
            if ($status === false) {
                $error = Yii::app()->user->setFlash('message', "Form should be filled properly");
                AppLogging::addLog($error, 'error', 'application.payroller.controller.main');
            } else {
                AppLogging::addLog('Company Signed up', 'success', 'application.company.controller.main');
                AppLogging::addLog('Contract1 Created', 'success', 'application.company.controller.main');
                AppLogging::addLog('Contract2 Created', 'success', 'application.company.controller.main');
                AppLogging::addLog('User1 Created', 'success', 'application.company.controller.main');
                AppLogging::addLog('User2 Created', 'success', 'application.company.controller.main');


<<<<<<< HEAD
=======
                $user->verification_code = User::createRandomPassword(50);
                $user->save();
                $url = Yii::app()->createAbsoluteUrl('/user/main/verifyemail') . '/vcode/' . $user->verification_code;

                $mailer = new AppMailer();
                $mailer->prepareBody('ConfirmEmail', array('NAME' => $user->full_name, 'CONFIRMLINK' => $url, 'EMAIL' => $user->email));
                $mailer->sendMail(array(array('email' => $user->email, 'name' => $user->full_name)), array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'SpeedyPayrolls'));
>>>>>>> origin/Module2
                $error = Yii::app()->user->setFlash('message', "Thankyou For Signing Up With Speedy Payrolls.");
                $error = Yii::app()->user->setFlash('message1', "Your Employee & Timesheet Account Automatically Created.");
//              $mailer = new AppMailer();
//              $mailer->prepareBody('SignUpEmail', array('NAME' => $model->first_name));
//              $mailer->sendMail(array(array('email' => $model->user_email, 'name' => $model->first_name)), array('email' => 'admin@directinterconnect.com', 'name' => 'Direct Interconnect'));
                if (AppUser::isUserAdmin()) {
                    $this->redirect(array('/user/main/invite'));
                }
                $this->redirect(array('/site/index'));
                //Yii::app()->user->setFlash('toast', 'Welcome to Direct Interconnect, You have successfully registered and logged in. Complete your profile for complete route management');
                //$this->redirect(array('/user/main/edit', 'new' => '1'));
            }
        }
        $this->render('signup', array(
            'user' => $user,
            'user2' => $user2,
            'company' => $company,
            'payrollers' => $payrollers,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Company the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Company::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Company $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'company-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
