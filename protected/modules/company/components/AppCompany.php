<?php

/*
 * Description: Component for User Module
 * Author: Usama Ayaz
 * Dated:  2-June-2014
 */

class AppCompany extends CComponent {

    public function __construct($ip = "") {
        
    }

    public static function isUserInBranch($lat, $long) {
        $branches = Branch::model()
                ->findAll('getDistance(latitude, longitude,"' . $lat . '","' . $long . '") <= radius');
               if (count($branches)) {
            return true;
        }
        return false;
    }

    public static function getPayrollerName($id) {

        if ($company = Company::model()->findByPk($id)) {
            if (isset($company->payroller)) {
                return $company->payroller->full_name;
            }
        }
        return '-';
    }

    public static function getCurrentTimesheetsData($status = 'Open', $export_fields = null, $entries = null) {
        $sql = "SELECT ";
        if (isset($export_fields)) {
            $last_item = end($export_fields);
            foreach ($export_fields as $item) {
                if($item != "hourly_total" && $item != "expense_details"){
                    if ($item == $last_item) {
                        if ($item == "name" || $item == "email") {
                            $sql .= "c." . $item;
                        } else {
                            $sql .= "q." . $item;
                        }
                    } else {
                        if ($item == "name" || $item == "email") {
                            $sql .= "c." . $item;
                        } else {
                            $sql .= "q." . $item . ",";
                        }
                    }
                }
            }
            $sql .= " FROM sp_quick_timesheet q INNER JOIN sp_contact c ON c.id=q.contact_id";
        } else {
            $sql = "SELECT c.name,c.email,q.date,q.job_title,q.job_reference, q.job_description,q.hours,q.rate,e.expense_value,q.total,q.id FROM sp_quick_timesheet q INNER JOIN sp_contact c ON c.id=q.contact_id LEFT JOIN sp_quick_ts_expenses e on e.quick_time_id=q.id";
        }
        $company = AppUser::getUserCompany();
        if ($company) {
            if (AppUser::isUserAdmin()) {
                if (isset($entries)) {
                    $selected_rows = "";
                    $last_row = end($entries);
                    foreach ($entries as $entry) {
                        if ($entry == $last_row) {
                            $selected_rows .= $entry;
                        } else {
                            $selected_rows .= $entry . ",";
                        }
                    }
                    $sql .= " WHERE q.status='" . $status . "' AND q.id IN(" . $selected_rows . ")";
                    $dataProvider = YII::app()->db->createCommand($sql)->queryAll();
                } else {

                    $sql .= " where q.status = '" . $status . "' AND q.company_id = " . $company->id;
                    $dataProvider = YII::app()->db->createCommand($sql)->queryAll();
                }
            } else {
                if (isset($entries)) {
                    $selected_rows = "";
                    $last_row = end($entries);
                    foreach ($entries as $entry) {
                        if ($entry == $last_row) {
                            $selected_rows .= $entry;
                        } else {
                            $selected_rows .= $entry . ",";
                        }
                    }
                    $sql .= " WHERE q.status='" . $status . "' AND q.id IN(" . $selected_rows . ") and created_by =" . AppUser::getUserId();
                    $dataProvider = YII::app()->db->createCommand($sql)->queryAll();
                } else {

                    $sql .= " where q.status = '" . $status . "' and q.company_id = " . $company->id . " and created_by =" . AppUser::getUserId();
                    $dataProvider = YII::app()->db->createCommand($sql)->queryAll();
                }
//                $dataProvider = YII::app()->db->createCommand($sql . " where q.status = '" . $status . "' and q.company_id = " . $company->id . " and created_by =" . AppUser::getUserId())->queryAll();
            }
        } else {
            if (AppUser::isUserAdmin()) {
                $dataProvider = YII::app()->db->createCommand($sql . " where q.status = '" . $status . "' and q.company_id = " . AppUser::getUserCompany(AppUser::getUserId(), false)->id)->queryAll();
            } else {
                $dataProvider = YII::app()->db->createCommand($sql . " where q.status = '" . $status . "' and q.company_id = " . AppUser::getUserCompany(AppUser::getUserId(), false)->id . "and q.created_by = " . AppUser::getUserId())->queryAll();
            }
        }
        return $dataProvider;
    }

    public static function getCompanyShifts($company_id) {
        return CompanyShifts::model()->findAllByAttributes(array("company_id" => $company_id));
    }

    public static function getClockEntryDetails($user_timing_id, $user_id = null, $date = null, $contract_id = null) {
        if (isset($user_id)) {
            if (isset($contract_id) && isset($date)) {
                $model = UserTimings::model()->findByAttributes(array("user_id" => $user_id, "created_at" => $date, "contract_id" => $contract_id));
            } else if (isset($contract_id)) {
                $model = UserTimings::model()->findByAttributes(array("user_id" => $user_id, "contract_id" => $contract_id));
            } else {
                $model = UserTimings::model()->findByAttributes(array("user_id" => $user_id, "created_at" => $date));
            }
        } else {
            $model = UserTimings::model()->findByPk($user_timing_id);
        }
        $lunches = UserLunch::model()->findAllByAttributes(array("user_timing_id" => isset($model->id) ? $model->id : 0));
        $notes = TimesheetNotes::model()->findAllByAttributes(array("user_timing_id" => isset($model->id) ? $model->id : 0));
        return array('model' => $model, 'lunches' => $lunches, 'notes' => $notes, 'timezone' => date_default_timezone_get());
    }

    public static function lunchOut($lunch_id) {
        $model = UserLunch::model()->findByPk($lunch_id);
        $model->lunch_out = time();
        $model->modified_at = time();
        $model->scenario = UserLunch::SCENARIO_LUNCH_OUT;

        if ($model->save()) {
            return $model;
        }
        return $model->errors;
    }

    public static function lunchIn($user, $time_id) {
        $model = new UserLunch();
        $model->id = AppInterface::getUniqueId();
        $model->lunch_in = time();
        $model->lunch_out = 0;
        $model->created_at = time();
        $model->created_by = $user;
        $model->modified_at = time();
        $model->modified_by = $user;
        $model->user_timing_id = $time_id;
        $model->scenario = UserLunch::SCENARIO_LUNCH_IN;

        if ($model->save()) {
            return $model;
        }
        return $model->errors;
    }

    public static function timeOut($time_id) {
        $total_lunch = Yii::app()->db->createCommand("select SUM(lunch_out-lunch_in) as total_lunch from sp_user_lunch "
                        . "where user_timing_id=" . $time_id)->queryAll();
        $model = UserTimings::model()->findByPk($time_id);
        // dd( $time_id);
        $model->time_out = time();
        $model->modified_at = time();
        $model->total_time = $model->time_out - $model->time_in;
        $model->total_lunch = $total_lunch[0]["total_lunch"];
        $model->scenario = UserTimings::SCENARIO_TIME_OUT;
        

        if ($model->save()) {
            return $model;
        }
        return $model->errors;
    }

    public static function timeIn($userId, $contract_id) {
        $model = UserTimings::model()->findByAttributes(array("user_id" => $userId, "contract_id" => $contract_id, "created_at" => strtotime(date('m/d/Y'))));
        if ($model instanceof UserTimings) {
            return $model;
        } else {
            $model = new UserTimings();
            $model->id = AppInterface::getUniqueId();
            $model->created_at = strtotime(date('m/d/Y'));
            $model->created_by = $userId;
            $model->modified_at = time();
            $model->modified_by = $userId;
            $model->time_in = time();
            $model->user_id = $userId;
            $model->status = TIMESHEET_STATUS_PENDING;
            $model->contract_id = $contract_id;
            $model->scenario = UserTimings::SCENARIO_TIME_IN;

            if ($model->save()) {
                return $model;
            }
            return $model->errors;
        }
    }

    public static function alreadyTimeIn($user, $contract_id) {
        return UserTimings::model()->findAllByAttributes(array("user_id" => $user, "contract_id" => $contract_id, "created_at" => strtotime(date('m/d/Y'))));
    }

    public static function updateApprovalListStatus($formData, $id = 0) {
        $model = UserTimings::model()->findByPk((isset($formData["id"])) ? $formData['id'] : $id);
        $model->status = $formData["status"];
        $model->modified_at = time();
        $model->modified_by = AppUser::getUserId();
        $model->scenario = UserTimings::SCENARIO_UPDATE_STATUS;


        if (isset($formData["reason"]))
            $model->reason = $formData["reason"];

        if ($model->save()) {
            return $model;
        }

        return $model->errors;
    }

    public static function getTimesheetsForReport($formData, $is_general) {
        $date_range = explode(' ', $formData["UserTimings"]["created_at"]);
        $from = DateTime::createFromFormat('d/m/Y', $date_range[0])->getTimestamp();
        $to = DateTime::createFromFormat('d/m/Y', $date_range[2])->getTimestamp();
        $date_group = false;
        $sql = "SELECT t.*,u.id As user_id,u.first_name,u.last_name,u.email FROM sp_user u "
                . "LEFT JOIN sp_user_timings t ON t.user_id=u.id "
                . "LEFT JOIN sp_contract l ON l.id=t.contract_id "
                . "WHERE t.created_at BETWEEN " . strtotime(date('m/d/Y', $from)) . ""
                . " AND " . strtotime(date('m/d/Y', $to));

        if (isset($formData["employee"])) {
            $count = count($formData["employee"]);
            if (count($count) > 0) {
                $sql .= " AND (";
                $employees = array();
                foreach ($formData["employee"] as $item) {
                    if ($item != 0) {
                        $emp = User::model()->findByPk($item);
                        array_push($employees, $emp);
                        if ($count != 1)
                            $sql .= "u.id=" . $item . " OR ";
                        else
                            $sql .= "u.id=" . $item;
                    }
                    $count--;
                }
                $sql .= ")";
            }
        }
        else {
            if (isset($is_general)) {
                $sql .= " AND t.user_id=" . AppUser::getUserId();
            }
        }
        if (isset($formData["type"])) {
            $type_count = count($formData["type"]);
            foreach ($formData["type"] as $type) {
                if ($type == 2 && $type_count == 1) {
                    $sql .= " AND l.approver_id=" . AppUser::getuserid();
                }
                if ($type == 2 && $type_count == 2) {
                    $sql .= " OR l.approver_id=" . AppUser::getUserId();
                }
            }
        }

        if (isset($formData["group"]) && isset($formData["sort"])) {
            if ($formData["group"] == 2) {
                $date_group = true;
            }
            if ($formData["sort"] == 1) {
                $sql .= " ORDER BY u.first_name";
            } else {
                $sql .= " ORDER BY u.last_name";
            }
        }
//                    dd($sql);
        ///*** Use CDB Crietria here
        ///*** Use CDB Crietria here

        $timesheets = Yii::app()->db->createCommand($sql)->queryAll();
        return array("timesheets" => $timesheets, "date_group" => $date_group);
    }

    public static function getClockEntries($user, $is_api = false, $page = 1, $size = 10) {
        if ($is_api) {
            $criteria = new CDbCriteria();
            $count = UserTimings::model()->countByAttributes(array("user_id" => $user));

            ///*** Use CPagination here
            $pages = new CPagination($count);
            $criteria->condition = "user_id =" . $user;
            $criteria->order = "created_at desc";
            $pages->pageSize = $size;
            $pages->setCurrentPage($page);
            $pages->applyLimit($criteria);
            $model = UserTimings::model()->findAll($criteria);
            return array("model" => $model, "pages" => $pages->getPageCount(), "current_page" => $pages->getCurrentPage());
        } else {
            $model = UserTimings::model()->findAllByAttributes(array("user_id" => $user), array('order' => 'created_at desc'));
            return $model;
        }
    }

    public static function getCurrentTimesheet() {

        ///**** This should cater pagination , status filter etc. $args

        $company = AppUser::getUserCompany();
        if ($company) {
            if (AppUser::isUserAdmin()) {
                $dataProvider = QuickTimesheet::model()->findAllByAttributes(
                        array('status' => 'Open', 'company_id' => $company->id), array('order' => 'created_at desc'));
            } else {
                $dataProvider = QuickTimesheet::model()->findAllByAttributes(
                        array('status' => 'Open', 'company_id' => $company->id,
                    'created_by' => AppUser::getUserId()), array('order' => 'created_at desc'));
            }
        } else {
            if (AppUser::isUserAdmin()) {
                $dataProvider = QuickTimesheet::model()->findAllByAttributes(
                        array('status' => 'Open',
                    'company_id' => AppUser::getUserCompany(AppUser::getUserId(), false)->id), array('order' => 'created_at desc'));
            } else {
                $dataProvider = QuickTimesheet::model()->findAllByAttributes(
                        array('status' => 'Open',
                    'company_id' => AppUser::getUserCompany(AppUser::getUserId(), false)->id,
                    'created_by' => AppUser::getUserId()), array('order' => 'created_at desc'));
            }
        }
        return $dataProvider;
    }

    public static function getQuickTimesheetByDate($date) {
        return QuickTimesheet::model()->findByAttributes(array("date" => $date));
    }

    public static function addQuickTimesheet($formData, $contact_id, $model) {
        $model->attributes = $formData['QuickTimesheet'];
        if (!isset($model->id)) {
            $model->id = AppInterface::getUniqueId();
            $model->created_at = time();
            $model->created_by = AppUser::getUserId();
        }
        $model->company_id = AppUser::getUserCompany(AppUser::getUserId(), false)->id;
        $model->date = DateTime::createFromFormat('d/m/Y H:i:s', $formData['QuickTimesheet']['date'] . " 00:00:00")->getTimestamp();
//        $model->hours = str_replace(":", ".", $formData['QuickTimesheet']['hours']);
//        if(($model->hours*3600) > 3600){
//            $time = floor($model->hours);
//            if(($model->hours - $time) > 0){
//            $min = ($model->hours - $time)/60*100;
//            $hour = ($time * 3600);
//            $model->hours = ($hour + ($min*3600))-3600;
//            }
//            else{
//                $model->hours = ($model->hours*3600)-3600;
//            }
//        }
//        else{
//            $model->hours = $model->hours/60*100;
//            $model->hours = ($model->hours * 3600)-3600;
//        }
        $model->hours = strtotime($formData['QuickTimesheet']['hours']);
        $model->contact_id = $contact_id;
        $model->currency_id = $formData['currency'];
        $model->modified_at = time();
        $model->modified_by = AppUser::getUserId();
        if ($model->save()) {
            return $model;
        }
        return $model->errors;
    }

    public static function saveQuickTimesheetExpense($id, $formData, $model) {
        QuickTsExpenses::model()->deleteAllByAttributes(array('quick_time_id' => $id));
        for ($x = 1; $x <= $formData['count']; $x++) {
            $expenses = new QuickTsExpenses();
            $expenses->expense_name = $formData['expense_name' . $x];
            $expenses->expense_value = $formData['expense_value' . $x];
            $expenses->comment = $formData['comment' . $x];
            $expenses->id = AppInterface::getUniqueId();
            $expenses->quick_time_id = $model->id;
            $expenses->modified_at = time();
            $expenses->modified_by = AppUser::getUserId();
            $expenses->created_by = $model->created_by;
            $expenses->created_at = $model->created_at;
            $expenses->save();
        }
    }

    public static function saveQuickTimesheetReciepts($id, $model, $files) {
        QuickTsReciepts::model()->deleteAllByAttributes(array('ref_id' => $id));
        $temp = CUploadedFile::getInstances($model, 'reciept_file');

        for ($i = 0; $i < count($files['QuickTimesheet']['name']['reciept_file']); $i++) {
            $reciepts = new QuickTsReciepts;
            if ($files['QuickTimesheet']['name']['reciept_file']) {
                $images_path = realpath(Yii::app()->basePath . '/../uploads/receipts');
                $filename = explode('.', $temp[$i]->name);
                $filename[0] = AppInterface::getFilename();
                $newfilename = $filename[0] . '.' . $filename[1];
                $temp[$i]->saveAs($images_path . '/' . $newfilename);
                $reciepts->reciept_file = $newfilename;
                $reciepts->id = AppInterface::getUniqueId();
                $reciepts->ref_id = $model->id;
                $reciepts->created_by = AppUser::getUserId();
                $reciepts->created_at = time();
                $reciepts->save();
            }
        }
    }

    public static function getCompletedNonbillableTimesheets() {
        $company = AppUser::getUserCompany();
        if ($company) {
            if (AppUser::isUserAdmin()) {
                $dataProvider = QuickTimesheet::model()->findAllByAttributes(array('status' => 'Emailed', 'company_id' => $company->id));
            } else {
                $dataProvider = QuickTimesheet::model()->findAllByAttributes(array('status' => 'Emailed', 'company_id' => $company->id, 'created_by' => AppUser::getUserId()));
            }
        } else {
            if (AppUser::isUserAdmin()) {
                $dataProvider = QuickTimesheet::model()->findAllByAttributes(array('status' => 'Emailed', 'company_id' => AppUser::getUserCompany(AppUser::getUserId(), false)->id));
            } else {
                $dataProvider = QuickTimesheet::model()->findAllByAttributes(array('status' => 'Emailed', 'company_id' => AppUser::getUserCompany(AppUser::getUserId(), false)->id, 'created_by' => AppUser::getUserId()));
            }
        }
        return $dataProvider;
    }

    public static function getNonbillableTimesheets($args = array(), $is_api = false, $page = 1, $size = 10) {
        $company = AppUser::getUserCompany();
        $user_id = AppUser::getUserId();
        $criteria = new CDbCriteria();
        if (!isset($company)) {
            $company = AppUser::getUserCompany($user_id, false);
        }
        if (isset($args['status'])) {
            $criteria->condition = "status='" . $args['status'] . "' AND company_id=" . $company->id;
        } else {
            $criteria->condition = "company_id=" . $company->id;
        }
        if (isset($args['contact_id'])) {
            $criteria->addCondition("contact_id=" . $args['contact_id']);
        }
        $count = count(QuickTimesheet::model()->findAll($criteria));
        if (!AppUser::isUserAdmin($user_id)) {
            $criteria->addCondition('created_by=' . $user_id);
            $count = count(QuickTimesheet::model()->findAll($criteria));
        }
        $criteria->order = "created_at desc";

        if ($is_api) {
            $pages = new CPagination($count);
            $pages->pageSize = $size;
            $pages->setCurrentPage($page);
            $pages->applyLimit($criteria);
            $timesheets = QuickTimesheet::model()->findAll($criteria);
            return array("timesheets" => $timesheets, 'timezone' => date_default_timezone_get(), "pages" => $pages->getPageCount(), "current_page" => $pages->getCurrentPage());
        } else {
            $timesheets = QuickTimesheet::model()->findAll($criteria);
            return $timesheets;
        }
    }

    public static function setCompanyPlan($company_id, $plan_id) {

        $plan_priviledges = PlanPriveledges::model()->findAllByAttributes(array('plan_id' => $plan_id));
        foreach ($plan_priviledges as $plan_priviledge) {
            $pp = new CompanyPriveledge();
            $pp->id = AppInterface::getUniqueId();
            $pp->company_id = $company_id;
            $pp->priveledge_id = $plan_priviledge->priveledge_id;
            $pp->start_time = time();
            $pp->quota = $plan_priviledge->quota;
            $pp->recursive_type = $plan_priviledge->type;
            if ($plan_priviledge->type == 'Daily') {
                $pp->end_time = strtotime('+1 day');
                $pp->recursive = 'Recursive';
            } else if ($plan_priviledge->type == 'Weekly') {
                $pp->end_time = strtotime('+1 week');
                $pp->recursive = 'Recursive';
            } else if ($plan_priviledge->type == 'Monthly') {
                $pp->end_time = strtotime('+1 month');
                $pp->recursive = 'Recursive';
            } else if ($plan_priviledge->type == 'None') {
                $pp->end_time = strtotime('+1 year');
                $pp->recursive = 'Non-Recursive';
            }
            $pp->status = 'active';
            $pp->created_at = time();
            $pp->modified_at = time();
            $pp->created_by = 1;
            $pp->modified_by = 1;
            $pp->save();
        }
        AppLogging::addLog($company_id . ' has been signed up with plan id ' . $plan_id, 'success', 'application.company.controller.main');
        return true;
    }

    public static function getCompanyAdmin($company_id) {
        if ($company_id) {
            $admin = User::model()->findBySql("select * from sp_user where id=(select user_id from sp_contract where company_id=" . $company_id . " and role='admin')");
            return $admin;
        }
        return null;
    }

    public static function addContract($contract1) {
        $contract = new Contract;
        if (isset($contract1['attributes']))
            $contract->attributes = $contract1['attributes'];
        $contract->id = AppInterface::getUniqueId();
        $contract->user_id = $contract1['user_id'];
        if (!empty($contract1['company_id'])) {
            $contract->company_id = $contract1['company_id'];
        }
        $contract->approver_id = $contract1['approver_id'];
        $contract->role = $contract1['role'];
        $contract->status = $contract1['status'];
        $contract->start_time = $contract1['start_time'];
        $contract->end_time = $contract1['end_time'];
        $contract->type = $contract1['type'];
        $contract->created_by = $contract1['created_by'];
        $contract->created_at = $contract1['created_at'];
        $contract->modified_by = $contract1['modified_by'];
        $contract->modified_at = $contract1['modified_at'];

        if (isset($contract1['is_internal']))
            $contract->is_internal = $contract1['is_internal'];
        if (isset($contract1['employee_type']))
            $contract->employee_type = $contract1['employee_type'];
        if (isset($contract1['owner_id']))
            $contract->owner_id = $contract1['owner_id'];
        if (isset($contract1['collection_day']))
            $contract->collection_day = $contract1['collection_day'];
        if (isset($contract1['collection_type']))
            $contract->collection_type = $contract1['collection_type'];
        if (isset($contract1['invitation_code']))
            $contract->invitation_code = $contract1['invitation_code'];
        if (isset($contract1['payroller_id']))
            $contract->payroller_id = $contract1['payroller_id'];
        if (isset($contract1['parent_id']))
            $contract->parent_id = $contract1['parent_id'];
        if (isset($contract1['file_name'])) {
            $contract->file_name = $contract1['file_name'];
        }
        if ($contract->save()) {///*** return errors as well in response
            return $contract;
        }
        return $contract->errors;
    }

    public static function isEmailExists($email) {
        return (Company::model()->countByAttributes(array('email' => $email))) > 0 ? true : false;
    }

    public static function addContractSetting($cs) {
        $contractsettings = new ContractSetting;
        $contractsettings->id = AppInterface::getUniqueId();
        $contractsettings->contract_id = $cs['contract_id'];
        $contractsettings->key = $cs['key'];
        $contractsettings->value = $cs['value'];
        $contractsettings->category = $cs['category'];
        $contractsettings->created_by = $cs['created_by'];
        $contractsettings->modified_by = $cs['modified_by'];
        $contractsettings->created_at = $cs['created_at'];
        $contractsettings->modified_at = $cs['modified_at'];
        $contractsettings->save();
    }

    public static function updateContractSetting($contract_id, $key, $category, $rate) {
        if ($contractsettings = ContractSetting::model()->findByAttributes(array('contract_id' => $contract_id, 'key' => $key, 'category' => $category))) {
            $contractsettings->value = $rate;
            $contractsettings->modified_by = AppUser::getUserId();
            $contractsettings->modified_at = time();
            $contractsettings->save();
        }
    }

    public static function addInvoice($data) {
        $invoice = new Invoice;
        $invoice->id = AppInterface::getUniqueId();
        if (isset($data['attributes']))
            $invoice->attributes = $data['attributes'];
        $invoice->html = $data['html'];
        $invoice->status = $data['status'];
        $invoice->created_by = $data['created_by'];
        $invoice->modified_by = $data['modified_by'];
        $invoice->created_at = $data['created_at'];
        $invoice->modified_at = $data['modified_at'];
        $invoice->timesheet_id = $data['timesheet_id'];
        $invoice->company_id = AppUser::getUserCompany()->id;
        $invoice->date = DateTime::createFromFormat('d/m/Y H:i:s', $data['date'] . " 00:00:00")->getTimestamp();
        if (isset($data['contact_id'])) {
            $invoice->contact_id = $data['contact_id'];
        } else {
            $invoice->contact_id = '0';
        }
        if (isset($data['logo']))
            $invoice->logo_url = $data['logo'];
        else
            $invoice->logo_url = "";
        if (isset($data['timesheet_id'])) {
            $invoice->timesheet_id = $data['timesheet_id'];
        }
        if (isset($data['invoice_ref'])) {
            $invoice->invoice_reference = $data['invoice_ref'];
            $invoice->sub_total = $data['sub_total'];
            $invoice->vat = $data['vat'];
            $invoice->total = $data['total'];
            $invoice->company_id = $data['company_id'];
            $invoice->contractor_name = $data['contractor_name'];
            $invoice->contractor_address = $data['contractor_address'];
            $invoice->vat_number = $data['vat_number'];
        }

        if ($invoice->save()) {
            return $invoice;
        } else {
            return $invoice->errors;
        }
    }

    public static function addContact($cont) {
        $contact = new Contact;
        $contact->id = AppInterface::getUniqueId();
        $contact->name = $cont['name'];
        $contact->email = $cont['email'];
        $contact->type = $cont['type'];
        $contact->company_id = $cont['company_id'];
        $contact->created_by = $cont['created_by'];
        $contact->created_at = $cont['created_at'];
        $contact->modified_by = $cont['modified_by'];
        $contact->modified_at = $cont['modified_at'];
        $contact->status = $cont['status'];
        if (isset($cont['address'])) {
            $contact->address = $cont['address'];
            $contact->country = $cont['country'];
            $contact->city = $cont['city'];
            $contact->post_code = $cont['post_code'];
        }
        if ($contact->save()) {
            return $contact;
        } else {
            return $contact->errors;
        }
    }

    public static function saveContact($user_id, $formData) {
        $model = new Contact;
        $model->id = AppInterface::getUniqueId();
        $model->attributes = $formData;
        $company = AppUser::getUserCompany($user_id, false);
        if (isset($company)) {
            $model->company_id = $company->id;
        }
        $model->created_by = $user_id;
        $model->created_at = time();
        $model->modified_by = $user_id;
        $model->modified_at = time();
        $model->status = FLAG_ON;
        $model->scenario = Contact::SCENARIO_ADD_CONTACT;


        if ($model->save()) {
            return $model;
        }
        return $model->errors;
    }

    public static function getCompanyContacts($is_api = false, $page = 1, $size = 10) {
        if (AppUser::isUserAdmin()) {
            $company = AppUser::getUserCompany();

            if (isset($company)) {
                if ($is_api) {

                    ///*** Use CPagination here

                    $criteria = new CDbCriteria();
                    $count = Contact::model()->countByAttributes(array("company_id" => $company->id));
                    $pages = new CPagination($count);
                    $criteria->condition = "company_id =" . $company->id;
                    $criteria->order = "created_at desc";
                    $pages->pageSize = $size;
                    $pages->setCurrentPage($page);
                    $pages->applyLimit($criteria);
                    $model = Contact::model()->findAll($criteria);
                    return array("model" => $model, "pages" => $pages->getPageCount(), "current_page" => $pages->getCurrentPage());
                } else {
                    $criteria = new CDbCriteria();
                    $criteria->condition = "company_id =" . $company->id;
                    $criteria->order = "created_at desc";
                    $model = Contact::model()->findAll($criteria);
                    return $model;
                }
            }
            return null;
        }
        return "No contact found";
    }

    public static function getTimesheetCount(&$company) {
        $company_admin = AppCompany::getCompanyAdmin($company->id);
        if ($company_admin == null) {
            return 0;
        }
        $c = new CDbCriteria;
        $c->join = 'inner join sp_contract c on t.contract_id = c.id';
        $c->addCondition('c.owner_id = :owner');
        $c->addCondition('t.created_at > :billtime');
        $c->addCondition('t.type != "daily" OR t.parent IS NOT NULL');
        $c->params = array(
            ':owner' => $company_admin->id,
            ':billtime' => $company->ts_reset_time,
        );
        return Timesheet::model()->count($c);
    }

    public static function getPayslipCount(&$company) {
        $company_admin = AppCompany::getCompanyAdmin($company->id);
        if ($company_admin == null) {
            return 0;
        }
        $c = new CDbCriteria;
        $c->join = 'inner join sp_payroll pr on pr.id = t.payroll_id';
        $c->join .= ' inner join sp_timesheet ts on ts.id = pr.timesheet_id';
        $c->join .= ' inner join sp_contract c on ts.contract_id = c.id';
        $c->addCondition('c.owner_id = :owner');
        $c->addCondition('t.created_at > :billtime');
        $c->params = array(
            ':owner' => $company_admin->id,
            ':billtime' => $company->ps_reset_time,
        );
        return PayslipEntry::model()->count($c);
    }

    public static function getPayrateNames($cid = '') {
        $criteria = new CDbCriteria(array('order' => "FIELD(NAME, 'Regular',NAME)"));
        if ($cid) {
            $pn = PayrateName::model()->findAllByAttributes(array('company_id' => $cid, 'is_item' => 0), $criteria);
            return $pn;
        } else if (AppUser::isUserAdmin()) {
            $company = AppUser::getUserCompany()->id;
            $pn = PayrateName::model()->findAllByAttributes(array('company_id' => $company, 'is_item' => 0), $criteria);
            return $pn;
        } else {
            return NULL;
        }
    }

    public static function getItems($cid = '') {
        if ($cid) {
            $pn = PayrateName::model()->findAllByAttributes(array('company_id' => $cid, 'is_item' => 1));
            return $pn;
        } else if (AppUser::isUserAdmin()) {
            $company = AppUser::getUserCompany()->id;
            $pn = PayrateName::model()->findAllByAttributes(array('company_id' => $company, 'is_item' => 1));
            return $pn;
        } else {
            return NULL;
        }
    }

    public static function getApprovalList($is_api = false, $page = 1, $size = 10) {
        $user_id = AppUser::getUserId();
        if ($is_api) {
            $criteria = new CDbCriteria();
            $count = UserTimings::model()->findAllBySql("SELECT U.* FROM sp_user_timings U INNER "
                    . "JOIN sp_contract C ON U.contract_id=C.id WHERE C.approver_id=" . $user_id . " "
                    . "order by U.created_at desc");
            $pages = new CPagination(count($count));
            $criteria->join = "INNER JOIN sp_contract C ON t.contract_id=C.id";
            $criteria->condition = "C.approver_id=" . $user_id;
            $criteria->order = "t.created_at desc";
            $pages->pageSize = $size;
            $pages->setCurrentPage($page);
            $pages->applyLimit($criteria);
            $model = UserTimings::model()->findAll($criteria);
            return array("model" => $model, "pages" => $pages->getPageCount(), "current_page" => $pages->getCurrentPage());
        } else {
            $criteria = new CDbCriteria();
            $criteria->join = "INNER JOIN sp_contract C ON t.contract_id=C.id";
            $criteria->condition = "C.approver_id=" . $user_id;
            $criteria->order = "t.created_at desc";
            $model = UserTimings::model()->findAll($criteria);
            return $model;
        }
    }
    
    public static function getNotesById($id){
        $notes = TimesheetNotes::model()->findAllByAttributes(array("user_timing_id" => $id));
        return $notes;
    }

    public static function addNote($formData, $type) {
        $model = new TimesheetNotes();
        $model->id = AppInterface::getUniqueId();
        $model->note = $formData["note"];
        $model->user_timing_id = $formData["id"];
        $model->type = $type;
        $model->created_at = time();
        $model->created_by = AppUser::getUserId();
        $model->modified_by = AppUser::getUserId();
        $model->modified_at = time();
        if ($model->save()) {
            return $model;
        }///*** handle errors 
        return $model->errors;
    }

    public static function getShiftUsers() {
        $company = AppUser::getUserCompany();
        $criteria = new CDbCriteria();
        $criteria->join = "INNER JOIN sp_contract sc ON sc.user_id = t.id LEFT JOIN sp_company_shifts cs ON cs.id = t.shift_id";
        $criteria->addCondition('sc.company_id = :comID');

        $criteria->addCondition('sc.is_internal = 1');

        $criteria->params = array('comID' => $company->id);
        $criteria->order = "cs.title";
        $users = User::model()->findAll($criteria);
        return $users;
    }

    public static function getCompanyInteralUserCount($companyId) {

        $criteria = new CDbCriteria();
        $criteria->addCondition('t.company_id = :companyId');
        $criteria->params = array(':companyId' => $companyId);
        $criteria->addCondition('t.is_internal = 1 ');
        $criteria->addCondition("t.role != 'Director'");

        return Contract::model()->count($criteria);
    }

    public static function getAllInternalContracts($companyId = '') {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.company_id = :companyId');
        $criteria->params = array(':companyId' => $companyId);
        $criteria->addCondition('t.is_internal = 1 ');

        return Contract::model()->findAll($criteria);
    }

    public static function getAllInternalContractsByUsers($companyId = '', $users) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.company_id = :companyId');
        $criteria->params = array(':companyId' => $companyId);
        $criteria->addCondition('t.is_internal = 1 ');
        $temp = "";
        foreach ($users as $user_id) {
            if (end($users) == $user_id) {
                $temp .= "t.user_id = " . $user_id;
            } else {
                $temp .= "t.user_id = " . $user_id . " OR ";
            }
        }
        $criteria->addCondition($temp);

        return Contract::model()->findAll($criteria);
    }

    public static function getAllCompanies($excludeMe = false) {


        if ($excludeMe) {

            $criteria = new CDbCriteria();
            $criteria->addCondition("t.id <> :companyID ");
            $criteria->params = array(':companyID' => AppUser::getUserCompany()->id);
            return Company::model()->findAll($criteria);
        }

        return Company::model()->findAll();
    }

    public static function getCompanyEmployee($companyId, $isInternal = 1, $is_api = false, $page = 1, $size = 10) {

        $criteria = new CDbCriteria();
        $criteria->join = "INNER JOIN sp_contract sc ON sc.user_id = t.id ";
        $criteria->addCondition('sc.company_id = :comID');
        $criteria->addCondition('sc.role != "Director"');
        if ($isInternal)
            $criteria->addCondition('sc.is_internal = 1');

        $criteria->params = array('comID' => $companyId);
        $criteria->order = "t.created_at desc";
        if ($is_api) {
            $count = User::model()->count($criteria);
            $pages = new CPagination($count);
            $pages->pageSize = $size;
            $pages->setCurrentPage($page);
            $pages->applyLimit($criteria);
            return array("employees" => User::model()->findAll($criteria), "pages" => $pages->getPageCount(), "current_page" => $pages->getCurrentPage());
        } else {
            return User::model()->findAll($criteria);
        }
    }

    public static function getEmployeeData($companyId, $isInternal = 1, $users = null) {
        $criteria = new CDbCriteria();
        $criteria->join = "INNER JOIN sp_contract sc ON sc.user_id = t.id ";
        $criteria->join .= "INNER JOIN sp_timesheet st ON sc.id = st.contract_id";
        $criteria->addCondition('sc.company_id = :comID');
        if ($users != null) {
            $temp = "";
            foreach ($users as $user_id) {
                if (end($users) == $user_id) {
                    $temp .= "sc.user_id = " . $user_id;
                } else {
                    $temp .= "sc.user_id = " . $user_id . " OR ";
                }
            }
            $criteria->addCondition($temp);
        }
        $criteria->params = array('comID' => $companyId);
        return User::model()->findAll($criteria);
    }

    public static function addCompany($name, $type = 1, $status = 1) {
        $already_exist = Company::model()->countByAttributes(array("email" => $input['user_email']));
        if ($already_exist > 0) {
            return "Company with this email already exist.";
        } else {
            $company = new Company();
            $company->id = AppInterface::getUniqueId();
            $company->name = $name;
            $company->type = $type;
            $company->status = $status;
            $company->created_by = AppUser::getUserId();
            $company->modified_by = AppUser::getUserId();
            $company->created_at = time();
            $company->modified_at = time();

            if ($company->insert()) {
                return $company;
            }
        }
        return null;
    }

    ///*** Both second and third parameters should have default value that should come from FLAG
    public static function addCompanyWithAddress($input, $type = 1, $status = 1) {
        $already_exist = Company::model()->countByAttributes(array("email" => $input['user_email']));
        if ($already_exist > 0) {
            return "Company with this email already exist.";
        } else {
            $company = new Company();
            $company->id = AppInterface::getUniqueId();

            ///** Use Massive Assignment here $company->attributes = $input;

            $company->name = $input['company_name'];
            $company->address = $input['company_address'];
            $company->city = $input['city'];
            $company->post_code = $input['postcode'];
            $company->country = $input['country'];
            $company->phone = $input['company_phone'];
            $company->email = $input['user_email'];
            $company->type = $type;
            $company->status = $status;
            $company->created_by = AppUser::getUserId();
            $company->modified_by = AppUser::getUserId();
            $company->created_at = time();  /// This value should come from App Interface::getCurrentTime()
            $company->modified_at = time();
            if (isset($input['logo'])) {
                $company->logo = $input['logo'];
            }
            if ($company->save()) {  ///*** call save method instead of insert
                return $company;
            } ///*** incase of failure return error that should be handled outside this function

            return $company->errors;
        }
    }

    public static function validateCompany($input, $type = 1, $status = 1) {
        $company = new Company();
        $company->id = AppInterface::getUniqueId();

        ///** Use Massive Assignment here $company->attributes = $input;

        $company->name = $input['company_name'];
        $company->address = $input['company_address'];
        $company->city = $input['city'];
        $company->post_code = $input['postcode'];
        $company->country = $input['country'];
        $company->phone = $input['company_phone'];
        $company->email = $input['user_email'];
        $company->type = $type;
        $company->status = $status;
        $company->created_by = AppUser::getUserId();
        $company->modified_by = AppUser::getUserId();
        $company->created_at = time();  /// This value should come from App Interface::getCurrentTime()
        $company->modified_at = time();
        if (isset($input['logo'])) {
            $company->logo = $input['logo'];
        }
        if ($company->validate()) {  ///*** call save method instead of insert
            return $company;
        } ///*** incase of failure return error that should be handled outside this function

        return $company->errors;
    }

    public static function getUserCompanyShifts($user_id, $is_api = false, $page = 1, $size = 10) {
        if ($is_api) {
            $count = CompanyShifts::model()->findAllByAttributes(array('created_by' => $user_id));
            $criteria = new CDbCriteria();
            $pages = new CPagination($count);
            $criteria->condition = "created_by =" . $user_id;
            $criteria->order = "created_at desc";
            $pages->pageSize = $size;
            $pages->setCurrentPage($page);
            $pages->applyLimit($criteria);
            $shifts = CompanyShifts::model()->findAll($criteria);
            return array("shifts" => $shifts, "pages" => $pages->getPageCount(), "current_page" => $pages->getCurrentPage());
        } else {
            $criteria = new CDbCriteria();
            $criteria->condition = "created_by =" . $user_id;
            $criteria->order = "created_at desc";
            $shifts = CompanyShifts::model()->findAll($criteria);
            return $shifts;
        }
        return "No Shift Found";
    }

    public static function getCompanyShiftsByAttributes($start_time, $end_time, $title) {
        $shifts = CompanyShifts::model()->findByAttributes(array('start_time' => $start_time, 'end_time' => $end_time, 'title' => $title));
        return $shifts;
    }

    public static function updateCompanyShift($formData, $id = 0) {

        $model = AppCompany::getCompanyShiftById($id);

        if (isset($model)) {
            $model->title = $formData['title'];
            $model->start_time = strtotime($formData['start_time']);
            $model->end_time = strtotime($formData['end_time']);
            $model->modified_at = time();
            $model->modified_by = AppUser::getUserId();
            $model->scenario = CompanyShifts::SCENARIO_SHIFT_UPDATE;

            ///*** Start Time & End Time great check should be placed here

            if ($model->start_time > $model->end_time) {
                return false;
            } else {
                if ($model->save()) {
                    return $model;
                }
                return $model->errors;
            }
        }
    }

    public static function getCompanyShiftById($shift_Id) {

        return CompanyShifts::model()->findByPk($shift_Id);
    }

    public static function deleteCompanyShift($id) {
        $model = CompanyShifts::model()->findByPk($id);
        if ($model) {
            $model->delete();
            return true;
        }
        return $model->errors;
    }

    public static function addCompanyShift($formData, $user_id, $company_id) {
        $model = new CompanyShifts;
        $model->attributes = $formData;
        $model->id = AppInterface::getUniqueId();
        $model->start_time = DateTime::createFromFormat('h:i a', $formData['start_time'])->getTimestamp();
        $model->end_time = DateTime::createFromFormat('h:i a', $formData['end_time'])->getTimestamp();
        $model->company_id = $company_id;
        $model->created_at = time();
        $model->modified_at = time();
        $model->created_by = $user_id;
        $model->modified_by = $user_id;
        $model->scenario = CompanyShifts::SCENARIO_SHIFT_CREATE;
        ///*** Just use validation scenario

        if ($model->start_time > $model->end_time) {
            return false;
        } else {
            if ($model->save()) {
                return $model;
            }
            return $model->errors;
        }
    }

    public static function companyShiftAssigned($id) {

        ///*** Dont fetch all the record set use count method to get the count only 
        $shift = User::model()->findAllByAttributes(array("shift_id" => $id));
        if (count($shift) > 0) {
            return true;
        }
        return false;
    }

    public static function addDummyCompany($input, $type = 1, $status = 1) {
        $dummyCompany = array();
        if (isset($input['company_address'])) {
            $dummyCompany['company'] = AppCompany::addCompanyWithAddress($input, $type, $status);
        } else {
            $dummyCompany['company'] = AppCompany::addCompany($input['company_name'], $type, $status);
        }

        if ($dummyCompany['company'] instanceof Company) {
            $user = AppUser::addUser($input['user_name'], $input['user_email'], $input['password'], '', '', 1, 0, 29450400);
            $dummyCompany['user_id'] = $user->id;
            if ($dummyCompany != null && $user != null) {

                $contract = new Contract();
                $contract->id = AppInterface::getUniqueId();
                $contract->user_id = $user->id;
                $contract->company_id = $dummyCompany['company']->id;
                $contract->created_at = time();
                $contract->created_by = '0';
                $contract->modified_at = time();
                $contract->modified_by = '0';
                $contract->start_time = time();
                $contract->end_time = -1;
                $contract->status = 0;
                $contract->type = 'in';
                $contract->approver_id = $user->id;
                //$contract->payroller_id = AppUser::getUserCompany()->payroller_id;
                $contract->collection_type = 0;
                $contract->collection_day = 'friday';
                $contract->role = 'Employee';
                $contract->is_internal = 1;
                $contract->owner_id = $user->id;
                $contract->insert();

                $admincontract = new Contract();
                $admincontract->id = AppInterface::getUniqueId();
                $admincontract->user_id = $user->id;
                $admincontract->company_id = $dummyCompany['company']->id;
                $admincontract->created_at = time();
                $admincontract->created_by = '0';
                $admincontract->modified_at = time();
                $admincontract->modified_by = '0';
                $admincontract->start_time = time();
                $admincontract->end_time = -1;
                $admincontract->status = 0;
                $admincontract->type = 'in';
                $admincontract->role = 'admin';
                $admincontract->approver_id = $user->id;
                $admincontract->payroller_id = AppUser::getUserCompany()->payroller_id;
                $admincontract->collection_type = 0;
                $admincontract->collection_day = 'friday';
                $admincontract->is_internal = 1;
                $admincontract->invitation_code = User::createRandomPassword(20);
                $admincontract->save();

                AppUser::assignDefaultTemplate($dummyCompany['company']->id);

                $url = Yii::app()->createAbsoluteUrl('/user/main/signup') . '/invite/' . $admincontract->invitation_code . '/contract/' . $admincontract->id;
                $mailer = new AppMailer();
                $user_contract = $user->email;
//            $mailer->prepareBody('invite_admin', array('NAME' => $user->first_name, 'INVITED_BY' => AppUser::getCurrentUser()->full_name, 'USERNAME' => $user->email, 'PASSWORD' => $input['pwd'], 'LINK'=>Yii::app()->createAbsoluteUrl('/user/main/login')));
//            $mailer->sendMail(array(array('email' => $user_contract, 'name' => $user->first_name)), array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls'));
            }
            return $dummyCompany;
        } else {
            return $dummyCompany;
        }
    }

}
