<?php

/**
 * This is the model class for table "sp_company".
 *
 * The followings are the available columns in table 'sp_company':
 * @property double $id
 * @property string $name
 * @property string $logo
 * @property string $email
 * @property string $phone
 * @property string $sector
 * @property string $registration_number
 * @property string $payroller_id
 * @property string $vat_number
 * @property string $address
 * @property string $street
 * @property string $city
 * @property string $country
 * @property string $post_code
 * @property integer $type
 * @property integer $status
 * @property double $created_at
 * @property double $created_by
 * @property double $modified_at
 * @property double $modified_by
 * @property integer $usage
 * @property integer $is_enable_logo
 * @property string $passport_copy
 * @property string $bill_copy
 * @property string $legal_name
 * @property integer $billing_date
 * @property string $ts_reset_time
 * @property string $ps_reset_time
 * @property string $contract_note
 *
 * The followings are the available model relations:
 * @property User $createdBy
 * @property User $modifiedBy
 * @property Contact[] $contacts
 * @property Contract[] $contracts
 * @property PayrateName[] $payrateNames
 * @property User $admin
 */
class Company extends CActiveRecord {

    const SCENARIO_SAVE_COMPANY_LOGO = "logo";
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'sp_company';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('logo','required','on' => self::SCENARIO_SAVE_COMPANY_LOGO),
            array('id, name, type, status, created_at, created_by, modified_at, modified_by', 'required'),
            array('type, status, usage', 'numerical', 'integerOnly' => true),
            array('id, name, email, phone, sector, registration_number,payroller_id, vat_number, address, street, city, country, created_by, modified_by', 'length', 'max' => 255),
            array('post_code', 'length', 'max' => 10),
//            array('logo', 'file', 'types'=>'jpg, gif, png', 'safe' => false),
            array('contract_note', 'safe'),
            array('created_at, modified_at', 'length', 'max' => 20),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, name, logo, email, phone, sector, registration_number, vat_number, address, street, city, country, post_code, type, status, created_at, created_by, modified_at, modified_by, usage, is_enable_logo, passport_copy, bill_copy, legal_name, billing_date, ts_reset_time, ps_reset_time, contract_note', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'createdBy' => array(self::BELONGS_TO, 'User', 'created_by'),
            'modifiedBy' => array(self::BELONGS_TO, 'User', 'modified_by'),
            'contacts' => array(self::HAS_MANY, 'Contact', 'company_id'),
            'contracts' => array(self::HAS_MANY, 'Contract', 'company_id'),
            'payrateNames' => array(self::HAS_MANY, 'PayrateName', 'company_id'),
            'payroller' => array(self::BELONGS_TO, 'User', 'payroller_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'logo' => 'Logo',
            'email' => 'Email',
            'phone' => 'Phone',
            'sector' => 'Sector',
            'registration_number' => 'Registration Number',
            'payroller_id' => 'Payroller',
            'vat_number' => 'Vat Number',
            'address' => 'Address',
            'street' => 'Street',
            'city' => 'City',
            'country' => 'Country',
            'post_code' => 'Post Code',
            'type' => 'Type',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'modified_at' => 'Modified At',
            'modified_by' => 'Modified By',
            'usage' => 'Usage',
            'is_enable_logo' => 'Is Enable Logo',
            'passport_copy' => 'Passport Copy',
            'bill_copy' => 'Bill Copy',
            'legal_name' => 'Legal Name',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('logo', $this->logo, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('phone', $this->phone, true);
        $criteria->compare('sector', $this->sector, true);
        $criteria->compare('registration_number', $this->registration_number, true);
        $criteria->compare('payroller_id', $this->payroller_id, true);
        $criteria->compare('vat_number', $this->vat_number, true);
        $criteria->compare('address', $this->address, true);
        $criteria->compare('street', $this->street, true);
        $criteria->compare('city', $this->city, true);
        $criteria->compare('country', $this->country, true);
        $criteria->compare('post_code', $this->post_code, true);
        $criteria->compare('type', $this->type);
        $criteria->compare('status', $this->status);
        $criteria->compare('created_at', $this->created_at, true);
        $criteria->compare('created_by', $this->created_by, true);
        $criteria->compare('modified_at', $this->modified_at, true);
        $criteria->compare('modified_by', $this->modified_by, true);
        $criteria->compare('usage', $this->usage);
        $criteria->compare('is_enable_logo', $this->is_enable_logo);
        $criteria->compare('passport_copy', $this->passport_copy, true);
        $criteria->compare('bill_copy', $this->bill_copy, true);
        $criteria->compare('legal_name', $this->legal_name, true);
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Company the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getadmin() {
        if ($contract = Contract::model()->findByAttributes(array('company_id' => $this->id, 'role' => 'admin'))) {
            return $contract->user;
        }
        return null;
    }
    
    public static function resetTimesheetCount($id){
        if($company = Company::model()->findByPk($id)){
            $company->ts_reset_time = time();
            $company->save();
        }
    }
    
    public static function resetPayslipCount($id){
        if($company = Company::model()->findByPk($id)){
            $company->ps_reset_time = time();
            $company->save();
        }
    }

}
