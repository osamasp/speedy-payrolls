<?php

/**
 * This is the model class for table "sp_company_branch".
 *
 * The followings are the available columns in table 'sp_company_branch':
 * @property string $id
 * @property string $name
 * @property double $latitude
 * @property double $longitude
 * @property string $address
 * @property double $radius 
 * @property string $company_id
 * @property string $created_at
 * @property string $created_by
 * @property string $modified_by
 * @property string $modified_at
 *
 * The followings are the available model relations:
 * @property Company $company
 * @property User $createdBy
 * @property User $modifiedBy
 */
class Branch extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sp_company_branch';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, name, latitude, longitude, address, radius, company_id, created_at, created_by, modified_by, modified_at', 'required'),
			array('latitude, longitude, radius', 'numerical'),
			array('id, company_id', 'length', 'max'=>30),
			array('name, address', 'length', 'max'=>255),
			array('created_at, created_by, modified_by, modified_at', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, latitude, longitude, address, radius, company_id, created_at, created_by, modified_by, modified_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
			'createdBy' => array(self::BELONGS_TO, 'User', 'created_by'),
			'modifiedBy' => array(self::BELONGS_TO, 'User', 'modified_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'latitude' => 'Latitude',
			'longitude' => 'Longitude',
			'address' => 'Address',
			'radius' => 'Radius',
			'company_id' => 'Company',
			'created_at' => 'Created At',
			'created_by' => 'Created By',
			'modified_by' => 'Modified By',
			'modified_at' => 'Modified At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('latitude',$this->latitude);
		$criteria->compare('longitude',$this->longitude);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('radius',$this->radius);
		$criteria->compare('company_id',$this->company_id,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('modified_by',$this->modified_by,true);
		$criteria->compare('modified_at',$this->modified_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Branch the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
