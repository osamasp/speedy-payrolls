<?php

/**
 * This is the model class for table "sp_contact".
 *
 * The followings are the available columns in table 'sp_contact':
 * @property string $id
 * @property string $company_id
 * @property string $name
 * @property string $email
 * @property string $type
 * @property integer $status
 * @property string $created_at
 * @property string $created_by
 * @property string $modified_at
 * @property string $modified_by
 * @property string $address
 * @property string $country
 * @property string $post_code
 * @property string $city
 *
 * The followings are the available model relations:
 * @property Company $company
 * @property User $createdBy
 * @property User $modifiedBy
 */
class Contact extends CActiveRecord {

    const SCENARIO_ADD_CONTACT = "add";
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'sp_contact';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id, email, address, country, city, post_code', 'required', 'on' => self::SCENARIO_ADD_CONTACT),
            array('id, company_id, email, status, created_at, created_by, modified_at, modified_by, address, country, city, post_code', 'required'),
            array('status', 'numerical', 'integerOnly' => true),
            array('id, company_id, name, email, created_by, modified_by, address, post_code, country, city', 'length', 'max' => 255),
            array('type', 'length', 'max' => 10),
            array('created_at, modified_at', 'length', 'max' => 20),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, company_id, name, email, type, status, created_at, created_by, modified_at, modified_by, address, post_code, country, city', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
            'createdBy' => array(self::BELONGS_TO, 'User', 'created_by'),
            'modifiedBy' => array(self::BELONGS_TO, 'User', 'modified_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'company_id' => 'Company',
            'name' => 'Name',
            'email' => 'Email',
            'type' => 'Type',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'modified_at' => 'Modified At',
            'modified_by' => 'Modified By',
            'address' => 'Address',
            'country' => 'Country',
            'post_code' => 'Post Code',
            'city' => 'City',
        );
    }

    public static function addContact($user_id, $formData) {
        $model = new Contact;
        $model->id = AppInterface::getUniqueId();
        $model->attributes = $formData;
        $company = AppUser::getUserCompany($user_id, false);
        if(isset($company)){
            $model->company_id = $company->id; ///*** Handle if company not found 
        }
        $model->created_by = $user_id;
        $model->created_at = time();
        $model->modified_by = $user_id;
        $model->modified_at = time();
        $model->status = FLAG_ON; ///*** Take it from Constants
        $model->scenario = Contact::SCENARIO_ADD_CONTACT;
        ///*** Use validation scenario
        
        if($model->save()){
            return $model;
        }
        return $model->errors;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('company_id', $this->company_id, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('created_at', $this->created_at, true);
        $criteria->compare('created_by', $this->created_by, true);
        $criteria->compare('modified_at', $this->modified_at, true);
        $criteria->compare('modified_by', $this->modified_by, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Contact the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
