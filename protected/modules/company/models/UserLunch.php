<?php

/**
 * This is the model class for table "sp_user_lunch".
 *
 * The followings are the available columns in table 'sp_user_lunch':
 * @property string $id
 * @property string $lunch_in
 * @property string $lunch_out
 * @property string $user_timing_id
 * @property string $created_by
 * @property string $created_at
 * @property string $modified_by
 * @property string $modified_at
 *
 * The followings are the available model relations:
 * @property SpUserTimings $userTiming
 * @property SpUser $createdBy
 * @property SpUser $modifiedBy
 */
class UserLunch extends CActiveRecord
{
    const SCENARIO_LUNCH_IN = "in";
    const SCENARIO_LUNCH_OUT = "out";
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sp_user_lunch';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
                        array('id, lunch_out', 'required', 'on' => self::SCENARIO_LUNCH_OUT),
                        array('id, lunch_in, user_timing_id', 'required', 'on' => self::SCENARIO_LUNCH_IN),
			array('id', 'required'),
			array('id, lunch_in, lunch_out, user_timing_id, created_by, created_at, modified_by, modified_at', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, lunch_in, lunch_out, user_timing_id, created_by, created_at, modified_by, modified_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'userTiming' => array(self::BELONGS_TO, 'SpUserTimings', 'user_timing_id'),
			'createdBy' => array(self::BELONGS_TO, 'SpUser', 'created_by'),
			'modifiedBy' => array(self::BELONGS_TO, 'SpUser', 'modified_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'lunch_in' => 'Lunch In',
			'lunch_out' => 'Lunch Out',
			'user_timing_id' => 'User Timing',
			'created_by' => 'Created By',
			'created_at' => 'Created At',
			'modified_by' => 'Modified By',
			'modified_at' => 'Modified At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('lunch_in',$this->lunch_in,true);
		$criteria->compare('lunch_out',$this->lunch_out,true);
		$criteria->compare('user_timing_id',$this->user_timing_id,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('modified_by',$this->modified_by,true);
		$criteria->compare('modified_at',$this->modified_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserLunch the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
