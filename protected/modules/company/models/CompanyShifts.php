<?php

/**
 * This is the model class for table "sp_company_shifts".
 *
 * The followings are the available columns in table 'sp_company_shifts':
 * @property string $id
 * @property string $title
 * @property string $start_time
 * @property string $end_time
 * @property string $company_id
 * @property string $created_at
 * @property string $modified_at
 * @property string $created_by
 * @property string $modified_by
 */
class CompanyShifts extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
    const SCENARIO_SHIFT_UPDATE = "shift";
    const SCENARIO_SHIFT_CREATE = "create";
    
	public function tableName()
	{
		return 'sp_company_shifts';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
                        array('id, title, start_time, end_time, company_id', 'required', 'on' => self::SCENARIO_SHIFT_CREATE),
                        array('title, start_time, end_time','required','on'=>  self::SCENARIO_SHIFT_UPDATE ),
			array('title, start_time, end_time', 'required'),
			array('id, start_time, end_time, company_id', 'length', 'max'=>20),
			array('title', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, start_time, end_time, company_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'start_time' => 'Start Time',
			'end_time' => 'End Time',
			'company_id' => 'Company',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('start_time',$this->start_time,true);
		$criteria->compare('end_time',$this->end_time,true);
		$criteria->compare('company_id',$this->company_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CompanyShifts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
