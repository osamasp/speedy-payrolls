<?php

class PayslipController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('issue', 'payslippdf', 'request', 'showInTemplate', 'download', 'view', 'BulkUpload', 'FormatDownload'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('update', 'main'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'roles' => array('admin', 'super_admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView() {
        
    }

    public function actionTimeSheets() {
        AppLogging::addLog("Viewing Payroller Timesheets", 'success', 'application.payroller.main');

        $args = array('payroller' => Yii::app()->user->id, 'status' => 'approved');

        /*         * * Filter By Contract Timesheets ** */
        if (Yii::app()->getRequest()->getParam("contract") != '') {
            $args['contract'] = Yii::app()->getRequest()->getParam("contract");
        }

        $result = AppTimeSheet::getTimeSheets($args);
        $this->render('timesheets', array('entries' => $result['dataSet'], 'pages' => $result['pages']));
    }

    public function actionRequest() {
//        $notification_id = Yii::app()->request->getParam('notification_id');
//        if(isset($notification_id))
//        {
//            AppUser::updateNotificationStatus($notification_id);
//        }
        AppLogging::addLog("Viewing PaySlip Request", 'success', 'application.payroller.main.paysliprequest');
        $args = array('payroller' => Yii::app()->user->id);

        /*         * * Filter By Contract Timesheets ** */
        if (Yii::app()->getRequest()->getParam("contract") != '') {
            $args['contract'] = Yii::app()->getRequest()->getParam("contract");
        }

        $result = AppPayroller::getPayrollerEntries($args);
        $this->render('request', array('entries' => $result['dataSet'], 'pages' => $result['pages']));
    }

    public function actionBulkUpload() {


        if (Yii::app()->getRequest()->isPostRequest) {
            $webroot = Yii::getPathOfAlias('webroot');
            if (!is_dir($webroot . '/uploads/payslips')) {
                mkdir($webroot . '/uploads/payslips');
                chmod(($webroot . '/uploads/payslips'), 0755);
                // the default implementation makes it under 777 permission, which you could possibly change recursively before deployment, but here's less of a headache in case you don't
            }
            $fileUpload = CUploadedFile::getInstanceByName('csvUpload');
            if (isset($fileUpload->error) && $fileUpload->error == UPLOAD_ERR_OK) {
                $fileName = AppSetting::genAutoFileName($fileUpload->name);
                $fullPath = Yii::app()->params['uploads']['payslip'] . $fileName;


                if ($fileUpload->saveAs($fullPath)) {
                    $paySlipEntry = new PayslipEntry();
                    $paySlipEntry->created_at = time();
                    $paySlipEntry->user_id = AppUser::getUserId();
                    $paySlipEntry->type = 'csv';
                    $paySlipEntry->file = $fileName;

                    $res = AppPayslip::importPaySlip($paySlipEntry, $fullPath, true);

                    if ($res['status']) {
                        Yii::app()->user->setFlash('success', $res['message']);
                    } else {
                        Yii::app()->user->setFlash('error', 'PaySlip Upload Error. ' . $res['message']);
                    }
                }
            }
        }

        $this->render('bulkUpload');
    }

    public function actionFormatDownload() {

        $fullPath = Yii::app()->params['uploads']['defaultPaySlip']['path'] . Yii::app()->params['uploads']['defaultPaySlip']['name'];

        if (file_exists($fullPath)) {
            AppLogging::addLog('Download CSV Format', 'success', 'application.payroller.controller.payslip');
            Yii::app()->getRequest()->sendFile(AppSetting::genAutoFileName(Yii::app()->params['uploads']['defaultPaySlip']['name']), file_get_contents($fullPath));
        }
    }

    public function actionPayslippdf($id) {
        $payslip = PayslipEntry::model()->findByPk($id);
        $pdf = new FPDF('P', 'mm', 'A4');
        $pdf->AddPage();
        $pdf->Image(Yii::app()->basePath . '\..\themes\speedypayroll\img\logo1.png', 10, 6, 30, 30);
        $pdf->SetXY(50, 10);
        $pdf->SetFont('Times', '', 14);
        $pdf->Write(10, 'Payroller Name');
        $pdf->SetXY(50, 20);
        $pdf->Write(10, 'Payroller Address');
        $pdf->SetFont("Courier", "B", "20");
        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFont("Arial", "", "11");
        $pdf->SetFillColor(81, 146, 26);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->Cell("52", "10", $payslip->T1, "1", "0", 'L', true);
        $pdf->Cell("53", "10", $payslip->T2, "1", "0", 'L', true);
        $pdf->Cell("45", "10", $payslip->T3, "1", "0", 'L', true);
        $pdf->Cell("45", "10", $payslip->T4, "1", "0", 'L', true);
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->SetTextColor(0, 0, 0);
        $pdf->Cell("52", "10", $payslip->V1, "1", "0");
        $pdf->Cell("53", "10", $payslip->V2, "1", "0");
        $pdf->Cell("45", "10", $payslip->V3, "1", "0");
        $pdf->Cell("45", "10", $payslip->V4, "1", "0");
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->SetTextColor(255, 255, 255);
        $pdf->Cell("52", "10", $payslip->T5, "1", "0", 'L', true);
        $pdf->Cell("53", "10", $payslip->T6, "1", "0", 'L', true);
        $pdf->Cell("45", "10", $payslip->T7, "1", "0", 'L', true);
        $pdf->Cell("45", "10", $payslip->T8, "1", "0", 'L', true);
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->SetTextColor(0, 0, 0);
        $pdf->Cell("52", "10", $payslip->V5, "1", "0");
        $pdf->Cell("53", "10", $payslip->V6, "1", "0");
        $pdf->Cell("45", "10", $payslip->V7, "1", "0");
        $pdf->Cell("45", "10", $payslip->V8, "1", "0");
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->Cell("105", "100", "", "1", "0");
        $pdf->SetX(115);
        $pdf->Cell("90", "50", "", "1", "0");
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->Ln();
        $pdf->SetX(115);
        $pdf->Cell("90", "50", "", "1", "0");
        $pdf->Cell("0", "50", "", "", "1");
        $pdf->SetTextColor(255, 255, 255);
        $pdf->Cell("40", "10", $payslip->T25, "1", "0", 'L', true);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->Cell("25", "10", $payslip->V25, "1", "0");
        $pdf->SetTextColor(255, 255, 255);
        $pdf->Cell("40", "10", $payslip->T27, "1", "0", 'L', true);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->Cell("25", "10", $payslip->V27, "1", "0");
        $pdf->SetTextColor(255, 255, 255);
        $pdf->Cell("40", "10", $payslip->T29, "1", "0", 'L', true);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->Cell("25", "10", $payslip->V29, "1", "0");
        $pdf->Cell("0", "10", "", "", "1");
        $pdf->SetTextColor(255, 255, 255);
        $pdf->Cell("40", "10", $payslip->T26, "1", "0", 'L', true);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->Cell("25", "10", $payslip->V26, "1", "0");
        $pdf->SetTextColor(255, 255, 255);
        $pdf->Cell("40", "10", $payslip->T28, "1", "0", 'L', true);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->Cell("25", "10", $payslip->V28, "1", "0");
        $pdf->SetTextColor(255, 255, 255);
        $pdf->Cell("40", "10", $payslip->T30, "1", "0", 'L', true);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->Cell("25", "10", $payslip->V30, "1", "0");
        $pdf->SetFont("Arial", "", "11");
        $pdf->Text(15, 90, "Work Hours");
        $pdf->Text(15, 98, $payslip->T9);
        $pdf->Text(15, 106, $payslip->T10);
        $pdf->Text(15, 114, $payslip->T11);
        $pdf->Text(15, 122, $payslip->T12);
        $pdf->Text(15, 130, $payslip->T13);
        $pdf->Text(15, 138, $payslip->T14);
        $pdf->Text(90, 98, $payslip->V9);
        $pdf->Text(90, 106, $payslip->V10);
        $pdf->Text(90, 114, $payslip->V11);
        $pdf->Text(90, 122, $payslip->V12);
        $pdf->Text(90, 130, $payslip->V13);
        $pdf->Text(90, 138, $payslip->V14);
        $pdf->Text(120, 90, "Bonus/Allowances");
        $pdf->Text(120, 98, $payslip->T15);
        $pdf->Text(120, 106, $payslip->T16);
        $pdf->Text(120, 114, $payslip->T17);
        $pdf->Text(120, 122, $payslip->T18);
        $pdf->Text(180, 98, $payslip->V15);
        $pdf->Text(180, 106, $payslip->V16);
        $pdf->Text(180, 114, $payslip->V17);
        $pdf->Text(180, 122, $payslip->V18);
        $pdf->Text(120, 140, "Deductions");
        $pdf->Text(120, 148, $payslip->T20);
        $pdf->Text(120, 156, $payslip->T21);
        $pdf->Text(120, 164, $payslip->T22);
        $pdf->Text(120, 172, $payslip->T23);
        $pdf->Text(180, 148, $payslip->V20);
        $pdf->Text(180, 156, $payslip->V21);
        $pdf->Text(180, 164, $payslip->V22);
        $pdf->Text(180, 172, $payslip->V23);
        $pdf->SetXY(148, 124);
        $pdf->Cell(45, 5, "Total");
        $pdf->SetXY(160, 124);
        $pdf->Cell(45, 6, $payslip->V19, 1);
        $pdf->SetXY(148, 174);
        $pdf->Cell(45, 5, "Total");
        $pdf->SetXY(160, 174);
        $pdf->Cell(45, 6, $payslip->V24, 1, 0);
        $pdf->Output();
    }

    public function actionIssue($id = "") {
        if ($id != "") {
            $model = Payroll::model()->findByPk($id);
            $paySlipEntry = new PayslipEntry();
            if ($model != null) {
                $tdetails = AppTimeSheet::compileCardDetail($model->timesheet_id);
                $this->PerformUpload($model);
                $this->render('issue', array('model' => $model, 'entry' => $paySlipEntry, 'tdetails' => $tdetails));
            }
//            else 
//            {
//                Yii::app()->user->setFlash('error', "Payroll ID is missing");
//                $this->redirect('payroller/main/PaySlipRequest');
//            }
        }
//        else {
//            Yii::app()->user->setFlash('error', "Payroll ID is missing");
//            $this->redirect('payroller/main/PaySlipRequest');
//        }
    }

    public function actionDownload($id = '') {

        if ($id != '') {

            $entry = PayslipEntry::model()->findByPk($id);
            if (AppTimeSheet::canDownloadPayslip($entry)) {
                AppLogging::addLog('Download File', 'success', 'application.payroller.controller.payslip');
                Yii::app()->getRequest()->sendFile(AppSetting::genAutoFileName($entry->file), file_get_contents(Yii::app()->params['uploads']['payslip'] . $entry->file));
            } else {
                Yii::app()->user->setFlash('error', 'PaySlip not found!');
                $this->redirect(Yii::app()->baseUrl);
            }
        } else {
            Yii::app()->user->setFlash('error', 'Download Failed!');
            $this->redirect(Yii::app()->baseUrl);
        }
    }

    private function PerformUpload(&$payroll) {
        $timesheet = $payroll->timesheet;
        if (Yii::app()->getRequest()->getPost('action') != "") {
            $user = AppUser::getCurrentUser();
            switch (Yii::app()->getRequest()->getPost('action')) {
                case 'upload':

                    $paySlipEntry = new PayslipEntry();
                    if (AppPayslip::isIssued($payroll)) {
                        $entry = $payroll->payslipEntries;
                        $paySlipEntry = $entry[0];
                    } else {
                        $paySlipEntry->user_id = $user->id;
                        $paySlipEntry->payroll_id = $payroll->id;
                        $paySlipEntry->created_at = time();
                        $paySlipEntry->type = "other";
                    }
                    $paySlipEntry->file = CUploadedFile::getInstanceByName('otherFile');
                    $fileName = '';
                    $fullPath = '';
                    if (isset($paySlipEntry->file->name)) {
                        $fileName = AppSetting::genAutoFileName($paySlipEntry->file->name);
                        $fullPath = Yii::app()->params['uploads']['payslip'] . $fileName;
                    }
                    if (isset($paySlipEntry->file->name) && $paySlipEntry->file->saveAs($fullPath)) {
                        $paySlipEntry->file = $fileName;
                        if ($paySlipEntry->isNewRecord) {
                            Yii::app()->user->setFlash('success', 'Payslip has been successfully uploaded and issued.');
                            $paySlipEntry->insert();
                            $timesheet->status = "payrolled";
                            $timesheet->dispatch_status = "completed";
                            $timesheet->update();

                            $payroll->status = "completed";
                            $payroll->update();
                            AppLogging::addLog('Payslip has been successfully uploaded and issued.', 'success', 'application.payroller.controller.payslip.issue');
                        } else {
                            Yii::app()->user->setFlash('success', 'Payslip has been successfully modified and re-issued.');
                            AppLogging::addLog('Payslip has been successfully modified and re-issued.', 'success', 'application.payroller.controller.payslip.issue');
                            $timesheet->status = "payrolled";
                            $timesheet->dispatch_status = "completed";
                            $timesheet->update();

                            $payroll->status = "completed";
                            $payroll->update();

                            $paySlipEntry->update();
                        }
                        AppPayslip::notifyUsersOnPaySlipIssued($payroll);
                        $this->redirect(array('request'));
                    } else {
                        Yii::app()->user->setFlash('error', 'Upload failed.');
                    }


                    break;
                case 'csvUpload':

                    $paySlipEntry = new PayslipEntry();
                    if (AppPayslip::isIssued($payroll)) {
                        $entry = $payroll->payslipEntries;
                        $paySlipEntry = $entry[0];
                        $paySlipEntry->type = "csv";
                    } else {
                        $paySlipEntry->user_id = AppUser::getUserId();
                        $paySlipEntry->payroll_id = $payroll->id;
                        $paySlipEntry->created_at = time();
                        $paySlipEntry->type = "csv";
                    }
                    $paySlipEntry->file = CUploadedFile::getInstanceByName('csvFile');
                    $fileName = '';
                    $fullPath = '';
                    if (isset($paySlipEntry->file->name)) {
                        $fileName = AppSetting::genAutoFileName($paySlipEntry->file->name);
                        $fullPath = Yii::app()->params['uploads']['payslip'] . $fileName;
                    }

                    if (isset($paySlipEntry->file->name) && $paySlipEntry->file->saveAs($fullPath)) {
                        $paySlipEntry->file = $fileName;
                        $res = AppPayslip::importPaySlip($paySlipEntry, $fullPath, false);
                        if ($res['status']) {
                            $timesheet->status = "payrolled";
                            $timesheet->update();

                            $payroll->status = "completed";
                            $payroll->update();

                            Yii::app()->user->setFlash('success', $res['message']);
                            AppLogging::addLog($res['message'], 'success', 'application.payroller.controller.payslip.issue');
                            AppPayslip::notifyUsersOnPaySlipIssued($payroll);
                            $this->redirect(array('request'));
                        }

                        $this->redirect(array('request'));
                    } else {
                        Yii::app()->user->setFlash('error', 'Upload failed.');
                    }

                    break;
            }
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $this->layout = '//layouts/login';
        $model = new User;

        if (isset($_POST['User'])) {

            $model->id = AppInterface::getUniqueId();
            $model->email = Yii::app()->user->getState('signup_email');
            $model->password = Yii::app()->user->getState('signup_password');
            $model->attributes = $_POST['User'];
            $model->type = 2;
            $model->created_at = time();
            $model->created_by = 0;
            $model->modified_by = 0;
            $model->modified_at = time();
            if ($model->save())
                $status = AppUser::loginUser($model->email, $model->password);
            AppLogging::addLog("Payroller Created!", 'success', 'application.payroller.main.Create');
            $this->redirect(array('/site/index'));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            if ($model->save()) {
                AppLogging::addLog("Payroller Updated!", 'success', 'application.payroller.main.Create');
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = User::model()->findAll();
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionMain() {
        $this->render('main');
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new User('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['User']))
            $model->attributes = $_GET['User'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return User the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = User::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionShowInTemplate() {
        $response = array('status' => false, 'reason' => 'Not a valid Ajax Request or ID is missing.');
        if (Yii::app()->getRequest()->isAjaxRequest && Yii::app()->getRequest()->getPost('entryId')) {

            $model = PayslipEntry::model()->findByAttributes(array('payroll_id' => Yii::app()->getRequest()->getPost('entryId')));
            if ($model != null) {
                $data = AppPayslip::generateMarkup($model->id);
//dd($data);
                $response['data'] = $data;
                $response['status'] = true;
            } else {
                $response['reason'] = "PaySlip not found.";
            }
        }
        AppSetting::generateAjaxResponse($response);
    }

    /**
     * Performs the AJAX validation.
     * @param User $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
