<?php

class MainController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'viewpayrolls', 'view', 'export', 'exportdispatch', 'exportpayrolls', 'create', 'timesheetinbox', 'TimeSheets', 'PaySlipRequest', 'IssuePaySlip', 'profile', 'editprofile', 'cpayrolls', 'ppayrolls'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete', 'DispatchPayroll'),
                'roles' => array('admin', 'super_admin',),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('company', 'employee'),
                'roles' => array('payroller', 'super_admin'),
            ),
            array('allow', // allow only super admin
                'actions' => array('bulkpermission'),
                'roles' => array('super_admin'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('AdminEmployee', 'AdminTimesheets'),
                'roles' => array('admin', 'super_admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionTimeSheets() {


        AppLogging::addLog("Viewing Payroller Timesheets", 'success', 'application.payroller.main');
        $this->layout = "//layouts/column1";


        $args = array('payroller' => Yii::app()->user->id);

        $user = new User();
        if (Yii::app()->getRequest()->getParam('user') != '') {
            $args['user'] = Yii::app()->getRequest()->getParam('user');
            $user = $user->findByPk($args['user']);
        }

        /*         * * Filter By Contract Timesheets ** */
        if (Yii::app()->getRequest()->getParam("contract") != '') {
            $args['contract'] = Yii::app()->getRequest()->getParam("contract");
        }

        $args['dispatch_flag'] = 1;
        $args['dispatch_status'] = array('dispatched', 'completed');
        $result = AppTimeSheet::getTimeSheets($args);
        $this->render('timesheets', array('user' => $user, 'entries' => $result['dataSet'], 'pages' => $result['pages']));
    }

    public function actionTimeSheetInbox() {
        AppLogging::addLog("Viewing Payroller Timesheets", 'success', 'application.payroller.main');
        $this->layout = "//layouts/column1";
        $args = array('payroller' => Yii::app()->user->id);

        $user = new User();
        if (Yii::app()->getRequest()->getParam('user') != '') {
            $args['user'] = Yii::app()->getRequest()->getParam('user');
            $user = $user->findByPk($args['user']);
        }

        /*         * * Filter By Contract Timesheets ** */
        if (Yii::app()->getRequest()->getParam("contract") != '') {
            $args['contract'] = Yii::app()->getRequest()->getParam("contract");
        }
        $args['dispatch_flag'] = 1;
        $args['dispatch_status'] = array('dispatched', 'completed');
        $args['status'] = "approved";
        $result = AppTimeSheet::getTimeSheets($args);
        $this->render('timesheets', array('user' => $user, 'entries' => $result['dataSet'], 'pages' => $result['pages'], 'status' => 'completed'));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionBulkPermission() {

        $payrollers = Payroller::model()->findAllByAttributes(array('can_bulk_import' => 0));
        if (isset($_POST['Payroller'])) {
            $id = $_POST['Payroller'];
            $payroller = Payroller::model()->findByAttributes(array('id' => $id));
            $payroller->can_bulk_import = 1;
            if ($payroller->update()) {
                Yii::app()->user->setFlash('success', 'Payroller successfully updated.');
                $this->redirect(array('/admin/default'));
            }
        }


        $this->render('bulkpermission', array('payrollers' => $payrollers));
    }

    public function actionCreate() {
        $this->layout = '//layouts/login';
        $model = new User;
        $payroller = new Payroller;

        if (isset($_POST['User'])) {

            $model->id = AppInterface::getUniqueId();
            $model->email = Yii::app()->user->getState('signup_email');
            $model->password = Yii::app()->user->getState('signup_password');
            $model->plan_type = Yii::app()->user->getState('plan_type');
            $model->attributes = $_POST['User'];
            $model->type = 2;
            $payroller->id = AppInterface::getUniqueId();
            $payroller->payroller_id = $model->id;
            $payroller->company_name = $_POST['Company']['name'];
            $payroller->company_address = $_POST['Company']['address'];
            $payroller->company_street = $_POST['Company']['street'];
            $payroller->company_city = $_POST['Company']['city'];
            $payroller->company_country = $_POST['Company']['country'];
            $payroller->company_postcode = $_POST['Company']['postcode'];
            $payroller->company_phone = $_POST['Company']['phone'];
            $payroller->company_sector = $_POST['Company']['sector'];

            $model->created_at = time();
            $model->created_by = 0;
            $model->modified_by = 0;
            $model->modified_at = time();

            if ($model->save()) {
                $payroller->insert();
                $model->verification_code = User::createRandomPassword(50);
                $model->save();
                $url = Yii::app()->createAbsoluteUrl('/user/main/verifyemail') . '/vcode/' . $model->verification_code;
                $mailer = new AppMailer();
                $mailer->prepareBody('ConfirmEmail', array('NAME' => $model->full_name, 'CONFIRMLINK' => $url, 'EMAIL' => $model->email));
                $mailer->sendMail(array(array('email' => $model->email, 'name' => $model->full_name)), array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'SpeedyPayrolls'));
                $status = AppUser::loginUser($model->email, $model->password);
                AppLogging::addLog("Payroller Created!", 'success', 'application.payroller.main.Create');

                $super_user = AppUser::get_super_user();
                $mailer1 = new AppMailer();
                $msg = 'A new signup of ' . $model->first_name . 'with email address: ' . $model->email . 'is pending.';
                $mailer1->prepareBody('Email', array('BODY' => $msg));
                $mailer1->sendMail(array(array('email' => $super_user->email, 'name' => 'Super Admin')), array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls'));
                $this->redirect(array('/site/index'));

                $not_result = AppInterface::notification($super_user, 19, 0, 0, $model->id);
            }
        }

        $this->render('create', array(
            'model' => $model, 'payroller' => $payroller,
        ));
    }

    public function actionEditProfile() {
        $user = AppUser::getCurrentUser();
        $model = Payroller::model()->findByAttributes(array('payroller_id' => $user->id));
        if (isset($_POST['Payroller'])) {
            $user_email = AppUser::checkEmailExist($_POST['User']['email']);
            if ($user_email && $user->email != $_POST['User']['email']) {
                Yii::app()->user->setFlash('error', "This email already exist.");
                $this->redirect('editprofile');
            } else {
                $model->attributes = $_POST['User'];
                $user->first_name = $_POST['User']['first_name'];
                $user->last_name = $_POST['User']['last_name'];
                $model->company_name = $_POST['Payroller']['company_name'];
                $model->company_address = $_POST['Payroller']['company_address'];
                $model->company_street = $_POST['Payroller']['company_street'];
                $model->company_city = $_POST['Payroller']['company_city'];
                $model->company_postcode = $_POST['Payroller']['company_postcode'];
                $model->company_email = 1;
                $model->company_country = $_POST['Payroller']['company_country'];
                $model->company_phone = $_POST['Payroller']['company_phone'];
                $model->vat_number = $_POST['Payroller']['vat_number'];

                if ($_FILES['Payroller']['name']['logo']) {
                    $model->logo = CUploadedFile::getInstance($model, 'logo');
                    $images_path = realpath(Yii::app()->basePath . '/../uploads/photos');
                    $filename = explode('.', $model->logo);
                    $filename[0] = AppInterface::getFilename();
                    $newfilename = $filename[0] . '.' . $filename[1];
                    $model->logo->saveAs($images_path . '/' . $newfilename);
                    $model->logo = $newfilename;
                }
                if ($model->validate()) {
                    $model->update();
                    $user->update();
                    Yii::app()->user->setFlash('success', 'Profile successfully updated.');
                    $this->redirect(array('/payroller/main/profile'));
                } else {
                    Yii::app()->user->setFlash('error', CHtml::errorSummary($model));
                }
            }
        }

        $this->render('editing', array(
            'model' => $model,
            'user' => $user,
        ));
    }

    public function actionProfile() {
        $model = User::model()->findByPk(AppUser::getUserId());
        $payroller = Payroller::model()->findByAttributes(array('payroller_id' => AppUser::getUserId()));
        $this->render('profile', array(
            'model' => $model,
            'payroller' => $payroller,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            if ($model->save()) {
                AppLogging::addLog("Payroller Updated!", 'success', 'application.payroller.main.Create');
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDispatchPayroll() {
        $response = null;
        $this->layout = '//layouts/column1';

        $args = array('dispatch_flag' => 1);
        /*         * * Filter By Contract Timesheets ** */
        if (Yii::app()->getRequest()->getParam("contract") != '') {
            $args['contract'] = Yii::app()->getRequest()->getParam("contract");
        }

        $this->performAction();

        $result = AppTimeSheet::getTimeSheetForAdmin($args);

        if (count($_GET) > 0 && isset($_GET['entry'])) {


            $response = AppTimeSheet::dispatchTimeSheetPayroll(array($_GET['entry']));


            if ($response['status'] == true) {
                AppLogging::addLog('Successfully Forwarded Timesheets To Payroller', 'success', 'application.timesheet.controller.main');
                Yii::app()->user->setFlash('success', $response['reason']);
                $this->redirect(array('index'));
            } else {
                AppLogging::addLog($response['reason'], 'error', 'application.timesheet.controller.main');
                Yii::app()->user->setFlash('error', $response['reason']);
            }
        }

        $this->render('dispatchPayroll', array('entries' => $result['dataSet'], 'pages' => $result['pages']));
    }

    public function actionviewpayrolls() {
        $c = new CDbCriteria();
        $c->join = 'INNER JOIN sp_timesheet ts ON t.timesheet_id = ts.id';
        $c->join .= ' INNER JOIN sp_contract c ON ts.contract_id = c.id';
        if (isset($_GET["id"]) && $_GET["id"] != '') {
            $c->addCondition('c.user_id = :uid');
            $c->params = array(':uid' => $_GET["id"]);
        }
        $payrolls = Payroll::model()->findAll($c);
        $count = Payroll::model()->count($c);
        $pages = new CPagination($count);

        $pages->pageSize = 15;
        $pages->applyLimit($c);
$this->render('viewpayrolls', array('entries' => AppPayroller::getPayrolls("", (AppUser::isUserAdmin() && !isset($_GET['id'])) ? '' : (isset($_GET['id']) ? $_GET['id'] : AppUser::getUserId())), 'status' => 'completed', 'type' => '2'));
//        $this->render('viewpayrolls', array('payrolls' => $payrolls, 'pages' => $pages, 'status' => 'completed', 'type' => '2'));
    }

    public function actionppayrolls() {
        $this->render('viewpayrolls', array('entries' => AppPayroller::getPayrolls("requested", (AppUser::isUserAdmin() && !isset($_GET['id'])) ? '' : (isset($_GET['id']) ? $_GET['id'] : AppUser::getUserId())), 'status' => 'requested', 'type' => '1'));
    }

    public function actionExport() {
        $webroot = Yii::getPathOfAlias('webroot');
        if (!is_dir($webroot . '/uploads/payslips')) {
            mkdir($webroot . '/uploads/payslips');
            chmod(($webroot . '/uploads/payslips'), 0755);
            // the default implementation makes it under 777 permission, which you could possibly change recursively before deployment, but here's less of a headache in case you don't
        }
        $file = $webroot . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'payslips' . DIRECTORY_SEPARATOR . 'payslip-' . date('d-m-Y-H-i') . '.csv';
        $handle = fopen($file, 'w');
        $status = Yii::app()->getRequest()->getParam("status");
        if ($status == 'requested') {
            $result = AppPayroller::getPayrolls("requested", (AppUser::isUserAdmin() && !isset($_GET['id'])) ? '' : (isset($_GET['id']) ? $_GET['id'] : AppUser::getUserId()));
        } else {
            $result = AppPayroller::getPayrolls("completed", (AppUser::isUserAdmin() && !isset($_GET['id'])) ? '' : (isset($_GET['id']) ? $_GET['id'] : AppUser::getUserId()));
        }
        $data = array();
        $i = 0;
        foreach ($result['payrolls'] as $item) {
            if (isset($_POST['entry'])) {
                if(in_array($item->id, $_POST['entry'])){
                    if (isset($_POST['export_fields'])) {
                    foreach ($_POST['export_fields'] as $items) {
                        if ($items == 'employee') {
                            $data[$i]['employee'] = $item->timesheet->contract->user->full_name;
                        } elseif ($items == 'contract') {
                            $data[$i]['contract'] = AppContract::getContractCustomName($item->timesheet->contract) . AppTimeSheet::printMergeLogName($item->timesheet);
                        } elseif ($items == 'approver') {
                            $data[$i]['approver'] = $item->timesheet->contract->approver->full_name;
                        } elseif ($items == 'period') {
                            $data[$i]['period'] = AppTimeSheet::formatSheetPeriod($item->timesheet);
                        } elseif ($items == 'status') {
                            $data[$i]['status'] = $item->status;
                        }
                    }
                } else {
                    $data[$i]['employee'] = $item->timesheet->contract->user->full_name;
                    $data[$i]['contract'] = AppContract::getContractCustomName($item->timesheet->contract) . AppTimeSheet::printMergeLogName($item->timesheet);
                    $data[$i]['approver'] = $item->timesheet->contract->approver->full_name;
                    $data[$i]['period'] = AppTimeSheet::formatSheetPeriod($item->timesheet);
                    $data[$i]['status'] = $item->status;
                }
                }
            } else {
                if (isset($_POST['export_fields'])) {
                    foreach ($_POST['export_fields'] as $items) {
                        if ($items == 'employee') {
                            $data[$i]['employee'] = $item->timesheet->contract->user->full_name;
                        } elseif ($items == 'contract') {
                            $data[$i]['contract'] = AppContract::getContractCustomName($item->timesheet->contract) . AppTimeSheet::printMergeLogName($item->timesheet);
                        } elseif ($items == 'approver') {
                            $data[$i]['approver'] = $item->timesheet->contract->approver->full_name;
                        } elseif ($items == 'period') {
                            $data[$i]['period'] = AppTimeSheet::formatSheetPeriod($item->timesheet);
                        } elseif ($items == 'status') {
                            $data[$i]['status'] = $item->status;
                        }
                    }
                } else {
                    $data[$i]['employee'] = $item->timesheet->contract->user->full_name;
                    $data[$i]['contract'] = AppContract::getContractCustomName($item->timesheet->contract) . AppTimeSheet::printMergeLogName($item->timesheet);
                    $data[$i]['approver'] = $item->timesheet->contract->approver->full_name;
                    $data[$i]['period'] = AppTimeSheet::formatSheetPeriod($item->timesheet);
                    $data[$i]['status'] = $item->status;
                }
            }
                    $i++;
        }
        AppInterface::GenerateCSV($file, array('employee', 'contract', 'approver', 'period', 'status'), $data);
        fclose($handle);
        $url = $this->createAbsoluteUrl('/uploads/payslips/' . 'payslip-' . date('d-m-Y-H-i') . '.csv');
        $this->layout = "//layout/blank";
        $response = array('status' => false, 'data' => '', 'reason' => 'Not a Valid Ajax Request or Post Data is Missing');
        $response['status'] = true;
        $response['reason'] = '';
        $response['url'] = $url;
        $response['data'] = $this->render('//user/employee/viewlist', array(
            'file' => $file,
            'name' => 'payslip-' . date('d-m-Y-H-i') . '.csv',
            'url' => $url
                ), true);
        AppSetting::generateAjaxResponse($response);
        Yii::app()->request->sendFile(AppInterface::getFilename() . '.csv', file_get_contents($file));
    }

    public function actionExportDispatch() {
        $webroot = Yii::getPathOfAlias('webroot');
        if (!is_dir($webroot . '/uploads/payrolls')) {
            mkdir($webroot . '/uploads/payrolls');
            chmod(($webroot . '/uploads/payrolls'), 0755);
            // the default implementation makes it under 777 permission, which you could possibly change recursively before deployment, but here's less of a headache in case you don't
        }
        $file = $webroot . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'payrolls' . DIRECTORY_SEPARATOR . 'payroll-' . date('d-m-Y-H-i') . '.csv';
        $handle = fopen($file, 'w');
        $args = array('dispatch_flag' => 1);
        /*         * * Filter By Contract Timesheets ** */
        if (Yii::app()->getRequest()->getParam("contract") != '') {
            $args['contract'] = Yii::app()->getRequest()->getParam("contract");
        }

        $result = AppTimeSheet::getTimeSheetForAdmin($args);

        $data = array();
        $i = 0;
        foreach ($result['dataSet'] as $entry) {
            $data[$i]['employee'] = $entry->contract->user->full_name;
            $data[$i]['contract'] = AppContract::getContractCustomName($entry->contract) . AppTimeSheet::printMergeLogName($entry);
            $data[$i]['period'] = AppTimeSheet::formatSheetPeriod($entry);
            $data[$i]['status'] = $entry->dispatch_status;
            $data[$i]['dispatch'] = ($entry->dispatch_status == "copied") ? 'Ready' : '';
            $i++;
        }
        AppInterface::GenerateCSV($file, array('employee', 'contract', 'period', 'status', 'dispatch'), $data);
        fclose($handle);
        $url = $this->createAbsoluteUrl('/uploads/payrolls/' . 'payroll-' . date('d-m-Y-H-i') . '.csv');
        $this->redirect($url);
    }

    public function actionExportPayrolls() {
        $webroot = Yii::getPathOfAlias('webroot');
        if (!is_dir($webroot . '/uploads/payrolls')) {
            mkdir($webroot . '/uploads/payrolls');
            chmod(($webroot . '/uploads/payrolls'), 0755);
            // the default implementation makes it under 777 permission, which you could possibly change recursively before deployment, but here's less of a headache in case you don't
        }
        $file = $webroot . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'payrolls' . DIRECTORY_SEPARATOR . 'payroll-' . date('d-m-Y-H-i') . '.csv';
        $handle = fopen($file, 'w');
        $status = Yii::app()->getRequest()->getParam("status");
        $company = AppUser::getUserCompany();

        if ($company != null)
            $result = AppPayroller::getPayrollerEmployees($company->id, false);
        $data = array();
        $i = 0;
        foreach ($result['dataSet'] as $entry) {
            $data[$i]['name'] = $entry->user->full_name;
            $data[$i]['id'] = AppInterface::formatID($entry->user->id);
            $data[$i]['type'] = AppContract::formatEmployeType($entry->employee_type);
            $data[$i]['start_date'] = date(AppInterface::getdateformat(), $entry->start_time);
            $data[$i]['end_date'] = ($entry->end_time != -1) ? date(AppInterface::getdateformat(), $entry->end_time) : '-';
            $data[$i]['location'] = $entry->company ? $entry->company->country : '-';
            $data[$i]['supervisor'] = $entry->approver ? $entry->approver->full_name : '-';
            $i++;
        }
        AppInterface::GenerateCSV($file, array('name', 'id', 'type', 'start_date', 'end_date', 'location', 'supervisor'), $data);
        fclose($handle);
        $url = $this->createAbsoluteUrl('/uploads/payrolls/' . 'payroll-' . date('d-m-Y-H-i') . '.csv');
        $this->redirect($url);
    }

    public function actioncpayrolls() {
        $this->render('viewpayrolls', array('entries' => AppPayroller::getPayrolls("completed", (AppUser::isUserAdmin() && !isset($_GET['id'])) ? '' : (isset($_GET['id']) ? $_GET['id'] : AppUser::getUserId())), 'status' => 'completed', 'type' => '2'));
    }

    private function performAction() {
        $response = null;
        if (count($_POST) > 0 && isset($_POST['action']) && isset($_POST['entry']) && count($_POST['entry']) > 0) {
            switch (strtolower($_POST['action'][0])) {
                case 'merge':
                    $response = AppTimeSheet::mergeTimeSheets($_POST['entry']);
                    if ($response['status'] == true) {
                        AppLogging::addLog('Successfully Merged Timesheets', 'success', 'application.payroller.controller.main');
                        Yii::app()->user->setFlash('success', 'Successfully Merged Timesheets');
                        $this->redirect(array('/payroller/main/DispatchPayroll'));
                    } else {
                        AppLogging::addLog($response['reason'], 'error', 'application.payroller.controller.main');
                        Yii::app()->user->setFlash('error', $response['reason']);
                    }
                    break;
                case 'payroll':
                    $response = AppTimeSheet::dispatchTimeSheetPayroll($_POST['entry']);
                    if ($response['status'] == true) {
                        AppLogging::addLog('Successfully Forwarded Timesheets To Payroller', 'success', 'application.payroller.controller.main');
                        Yii::app()->user->setFlash('success', $response['reason']);
                        AppLogging::addLog("Timesheet Forwarded to Payroller", 'success', 'application.payroller.main');
                        $sheets = Timesheet::model()->findByPk($_POST['entry']);
                        if (isset($sheets->contract->company->payroller_id)) {
                            $not_result = AppInterface::notification($sheets->contract->company->payroller_id, 0, $sheets->id, $sheets->contract->id, $sheets->contract->company->payroller_id);
                            $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
                            $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;
                            $notifier = new Notification();
                            $notification_count = $notifier->getUserNotificationUnseenCount($sheets->contract->user->id);
                            $notification = Yii::app()->getModule('notification');
                            $notification->send(array('push' => array('server_notification', array('id' => $sheets->contract->company->payroller_id, 'msg' => sprintf($not_result->notificationType->notice, AppUser::getUserById($not_result->sender_id)->full_name), 'url' => $url, 'logo' => $logo, 'count' => $notification_count))));
                        }
                        $this->redirect(array('/payroller/main/DispatchPayroll'));
                    } else {
                        AppLogging::addLog($response['reason'], 'error', 'application.timesheet.controller.main');
                        Yii::app()->user->setFlash('error', $response['reason']);
                    }
                    break;


                case 'dispatch':
                    $response = AppTimeSheet::dispatchTimeSheet($_POST['entry']);
                    if ($response['status'] == true) {
                        AppLogging::addLog('Successfully Forwarded Timesheets To Payroller', 'success', 'application.payroller.controller.main');
                        Yii::app()->user->setFlash('success', $response['reason']);
                        AppLogging::addLog("Timesheet Forwarded to Payroller", 'success', 'application.payroller.main');

                        $this->redirect(array('/payroller/main/DispatchPayroll'));
                    } else {
                        AppLogging::addLog($response['reason'], 'error', 'application.timesheet.controller.main');
                        Yii::app()->user->setFlash('error', $response['reason']);
                    }
                    break;
            }
        } elseif (isset($_POST['action']) && !isset($_POST['entry'])) {
            Yii::app()->user->setFlash('error', 'Please select timesheets to perform this action.');
        }

        return true;
    }

    public function actionCompany() {

        $this->layout = "//layouts/column1";
        $result = AppPayroller::getPayrollerCompanies();
        $this->render('company', array('entries' => $result['dataSet'], 'pages' => $result['pages']));
    }

    public function actionEmployee() {

        $this->layout = "//layouts/column1";

        $result = AppPayroller::getPayrollerEmployees(Yii::app()->getRequest()->getParam('company'));



        $this->render('employee', array('entries' => $result['dataSet'], 'pages' => $result['pages']));
    }

    public function actionAdminEmployee() {

        $this->layout = "//layouts/column1";
        $company = AppUser::getUserCompany();
        $result = array('dataSet' => null, 'pages' => null);

        if ($company != null)
            $result = AppPayroller::getPayrollerEmployees($company->id, false);


        $this->render('AdminEmployee', array('entries' => $result['dataSet'], 'pages' => $result['pages']));
    }

    public function actionAdminTimesheets() {

        $this->layout = "//layouts/column1";
        $company = AppUser::getUserCompany();
        $result = array('dataSet' => null, 'pages' => null);

        $this->performAction();

        if (AppUser::isUserAdmin())
            $result = AppTimeSheet::getTimeSheetForAdmin(array('user' => Yii::app()->getRequest()->getParam('user')));
        else
            $result = AppTimeSheet::getTimeSheets($args);

        $this->render('AdminTimeSheets', array('entries' => $result['dataSet'], 'pages' => $result['pages']));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = User::model()->findAll();
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new User('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['User']))
            $model->attributes = $_GET['User'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return User the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = User::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param User $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
