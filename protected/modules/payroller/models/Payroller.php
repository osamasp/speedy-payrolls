<?php

/**
 * This is the model class for table "sp_payroller".
 *
 * The followings are the available columns in table 'sp_payroller':
 * @property string $id
 * @property string $company_name
 * @property string $logo
 * @property string $company_address
 * @property string $company_street
 * @property string $company_city
 * @property integer $company_postcode
 * @property string $company_country
 * @property string $company_phone
 * @property string $company_sector
 * @property string $company_email
 * @property string $vat_number
 * @property string $payroller_id
 * @property integer $can_bulk_import
 *
 * The followings are the available model relations:
 * @property User $payroller
 */
class Payroller extends CActiveRecord
{
    
        const SCENARIO_ADD_PAYROLLER = "add";
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sp_payroller';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
                        array('id, company_name, company_address, company_street, company_city, company_postcode, company_country, payroller_id','required','on' => self::SCENARIO_ADD_PAYROLLER),
			array('id, company_name, company_address, company_street, company_city, company_postcode, company_country, company_email, vat_number, payroller_id', 'required'),
			array('company_postcode , can_bulk_import', 'numerical', 'integerOnly'=>true),
			array('id, company_name, logo, company_address, company_street, company_city, company_country, company_phone, company_sector, company_email, vat_number, payroller_id', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, company_name, logo, company_address, company_street, company_city, company_postcode, company_country, company_phone, company_sector, company_email, vat_number, payroller_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'payroller' => array(self::BELONGS_TO, 'User', 'payroller_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'company_name' => 'Company Name',
			'logo' => 'Logo',
			'company_address' => 'Company Address',
			'company_street' => 'Company Street',
			'company_city' => 'Company City',
			'company_postcode' => 'Company Postcode',
			'company_country' => 'Company Country',
			'company_phone' => 'Company Phone',
			'company_sector' => 'Company Sector',
			'company_email' => 'Company Email',
			'vat_number' => 'Vat Number',
			'payroller_id' => 'Payroller',
                        'can_bulk_import' => 'Can Bulk Import',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('company_name',$this->company_name,true);
		$criteria->compare('logo',$this->logo,true);
		$criteria->compare('company_address',$this->company_address,true);
		$criteria->compare('company_street',$this->company_street,true);
		$criteria->compare('company_city',$this->company_city,true);
		$criteria->compare('company_postcode',$this->company_postcode);
		$criteria->compare('company_country',$this->company_country,true);
		$criteria->compare('company_phone',$this->company_phone,true);
		$criteria->compare('company_sector',$this->company_sector,true);
		$criteria->compare('company_email',$this->company_email,true);
		$criteria->compare('vat_number',$this->vat_number,true);
		$criteria->compare('payroller_id',$this->payroller_id,true);
                $criteria->compare('can_bulk_import',$this->can_bulk_import);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Payroller the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
