<?php

/**
 * This is the model class for table "sp_payslip_entry".
 *
 * The followings are the available columns in table 'sp_payslip_entry':
 * @property string $id
 * @property string $T1
 * @property string $V1
 * @property string $T2
 * @property string $V2
 * @property string $T3
 * @property string $V3
 * @property string $T4
 * @property string $V4
 * @property string $T5
 * @property string $V5
 * @property string $T6
 * @property string $V6
 * @property string $T7
 * @property string $V7
 * @property string $T8
 * @property string $V8
 * @property string $T9
 * @property string $V9
 * @property string $T10
 * @property string $V10
 * @property string $T11
 * @property string $V11
 * @property string $T12
 * @property string $V12
 * @property string $T13
 * @property string $V13
 * @property string $T14
 * @property string $V14
 * @property string $T15
 * @property string $V15
 * @property string $T16
 * @property string $V16
 * @property string $T17
 * @property string $V17
 * @property string $T18
 * @property string $V18
 * @property string $T19
 * @property string $V19
 * @property string $T20
 * @property string $V20
 * @property string $T21
 * @property string $V21
 * @property string $T22
 * @property string $V22
 * @property string $T23
 * @property string $V23
 * @property string $T24
 * @property string $V24
 * @property string $T25
 * @property string $V25
 * @property string $T26
 * @property string $V26
 * @property string $T27
 * @property string $V27
 * @property string $T28
 * @property string $V28
 * @property string $T29
 * @property string $V29
 * @property string $T30
 * @property string $V30
 * @property string $user_id
 * @property string $payroll_id
 * @property string $file
 * @property string $type
 * @property string $created_at
 *
 * The followings are the available model relations:
 * @property Payroll $payroll
 * @property User $user
 */
class PayslipEntry extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sp_payslip_entry';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
		array('T1, V1, T2, V2, T3, V3, T4, V4, T5, V5, T6, V6, T7, V7, T8, V8, T9, V9, T10, V10, T11, V11, T12, V12, T13, V13, T14, V14, T15, V15, T16, V16, T17, V17, T18, V18, T19, V19, T20, V20, T21, V21, T22, V22, T23, V23, T24, V24, T25, V25, T26, V26, T27, V27, T28, V28, T29, V29, T30, V30, user_id, file', 'length', 'max'=>255),
			array('payroll_id, created_at', 'length', 'max'=>50),
			array('type', 'length', 'max'=>5),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, T1, V1, T2, V2, T3, V3, T4, V4, T5, V5, T6, V6, T7, V7, T8, V8, T9, V9, T10, V10, T11, V11, T12, V12, T13, V13, T14, V14, T15, V15, T16, V16, T17, V17, T18, V18, T19, V19, T20, V20, T21, V21, T22, V22, T23, V23, T24, V24, T25, V25, T26, V26, T27, V27, T28, V28, T29, V29, T30, V30, user_id, payroll_id, file, type, created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'payroll' => array(self::BELONGS_TO, 'Payroll', 'payroll_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'T1' => 'T1',
			'V1' => 'V1',
			'T2' => 'T2',
			'V2' => 'V2',
			'T3' => 'T3',
			'V3' => 'V3',
			'T4' => 'T4',
			'V4' => 'V4',
			'T5' => 'T5',
			'V5' => 'V5',
			'T6' => 'T6',
			'V6' => 'V6',
			'T7' => 'T7',
			'V7' => 'V7',
			'T8' => 'T8',
			'V8' => 'V8',
			'T9' => 'T9',
			'V9' => 'V9',
			'T10' => 'T10',
			'V10' => 'V10',
			'T11' => 'T11',
			'V11' => 'V11',
			'T12' => 'T12',
			'V12' => 'V12',
			'T13' => 'T13',
			'V13' => 'V13',
			'T14' => 'T14',
			'V14' => 'V14',
			'T15' => 'T15',
			'V15' => 'V15',
			'T16' => 'T16',
			'V16' => 'V16',
			'T17' => 'T17',
			'V17' => 'V17',
			'T18' => 'T18',
			'V18' => 'V18',
			'T19' => 'T19',
			'V19' => 'V19',
			'T20' => 'T20',
			'V20' => 'V20',
			'T21' => 'T21',
			'V21' => 'V21',
			'T22' => 'T22',
			'V22' => 'V22',
			'T23' => 'T23',
			'V23' => 'V23',
			'T24' => 'T24',
			'V24' => 'V24',
			'T25' => 'T25',
			'V25' => 'V25',
			'T26' => 'T26',
			'V26' => 'V26',
			'T27' => 'T27',
			'V27' => 'V27',
			'T28' => 'T28',
			'V28' => 'V28',
			'T29' => 'T29',
			'V29' => 'V29',
			'T30' => 'T30',
			'V30' => 'V30',
			'user_id' => 'User',
			'payroll_id' => 'Payroll',
			'file' => 'File',
			'type' => 'Type',
			'created_at' => 'Created At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('T1',$this->T1,true);
		$criteria->compare('V1',$this->V1,true);
		$criteria->compare('T2',$this->T2,true);
		$criteria->compare('V2',$this->V2,true);
		$criteria->compare('T3',$this->T3,true);
		$criteria->compare('V3',$this->V3,true);
		$criteria->compare('T4',$this->T4,true);
		$criteria->compare('V4',$this->V4,true);
		$criteria->compare('T5',$this->T5,true);
		$criteria->compare('V5',$this->V5,true);
		$criteria->compare('T6',$this->T6,true);
		$criteria->compare('V6',$this->V6,true);
		$criteria->compare('T7',$this->T7,true);
		$criteria->compare('V7',$this->V7,true);
		$criteria->compare('T8',$this->T8,true);
		$criteria->compare('V8',$this->V8,true);
		$criteria->compare('T9',$this->T9,true);
		$criteria->compare('V9',$this->V9,true);
		$criteria->compare('T10',$this->T10,true);
		$criteria->compare('V10',$this->V10,true);
		$criteria->compare('T11',$this->T11,true);
		$criteria->compare('V11',$this->V11,true);
		$criteria->compare('T12',$this->T12,true);
		$criteria->compare('V12',$this->V12,true);
		$criteria->compare('T13',$this->T13,true);
		$criteria->compare('V13',$this->V13,true);
		$criteria->compare('T14',$this->T14,true);
		$criteria->compare('V14',$this->V14,true);
		$criteria->compare('T15',$this->T15,true);
		$criteria->compare('V15',$this->V15,true);
		$criteria->compare('T16',$this->T16,true);
		$criteria->compare('V16',$this->V16,true);
		$criteria->compare('T17',$this->T17,true);
		$criteria->compare('V17',$this->V17,true);
		$criteria->compare('T18',$this->T18,true);
		$criteria->compare('V18',$this->V18,true);
		$criteria->compare('T19',$this->T19,true);
		$criteria->compare('V19',$this->V19,true);
		$criteria->compare('T20',$this->T20,true);
		$criteria->compare('V20',$this->V20,true);
		$criteria->compare('T21',$this->T21,true);
		$criteria->compare('V21',$this->V21,true);
		$criteria->compare('T22',$this->T22,true);
		$criteria->compare('V22',$this->V22,true);
		$criteria->compare('T23',$this->T23,true);
		$criteria->compare('V23',$this->V23,true);
		$criteria->compare('T24',$this->T24,true);
		$criteria->compare('V24',$this->V24,true);
		$criteria->compare('T25',$this->T25,true);
		$criteria->compare('V25',$this->V25,true);
		$criteria->compare('T26',$this->T26,true);
		$criteria->compare('V26',$this->V26,true);
		$criteria->compare('T27',$this->T27,true);
		$criteria->compare('V27',$this->V27,true);
		$criteria->compare('T28',$this->T28,true);
		$criteria->compare('V28',$this->V28,true);
		$criteria->compare('T29',$this->T29,true);
		$criteria->compare('V29',$this->V29,true);
		$criteria->compare('T30',$this->T30,true);
		$criteria->compare('V30',$this->V30,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('payroll_id',$this->payroll_id,true);
		$criteria->compare('file',$this->file,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('created_at',$this->created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PayslipEntry the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
