<?php

/*
 * Description: Component for User Module
 * Author: Usama Ayaz
 * Dated:  2-June-2014
 */

class AppPayslip extends CComponent {

    public function __construct($ip = "") {
        
    }

    public static function getPayslip($p_id) {
        return PayslipEntry::model()->findByAttributes(array('payroll_id' => $p_id));
    }

    public static function generateMarkup($payEntryId) {

        $model = PayslipEntry::model()->findByPk($payEntryId);
        $obj = new Cms();
        $cms = $obj->getCMSContentBySlug('testSheetIco');


        if ($model != null && $cms != null) {

            $str = $cms['content'];
            foreach ($model->attributes as $key => $val) {
                $attName = str_replace('_', '', $key);
                $str = str_replace('[{' . $attName . '}]', $val, $str);
            }

            /*             * ** Fill Payroller Details ** */
            $payroller = $model->payroll->timesheet->contract->payroller;
            $str = str_replace('[{PayrollerName}]', $payroller->full_name, $str);
            $str = str_replace('[{PayrollerAddress}]', AppUser::formatUserAddres($payroller), $str);
            $userCompany = AppUser::getUserCompany($model->payroll->timesheet->contract->user_id, FALSE);

            if (isset($userCompany->is_enable_logo) && $userCompany->is_enable_logo == 1)
                $str = str_replace('[{PayrollerLogo}]', isset($userCompany->logo)? Yii::app()->baseUrl ."/uploads/photos/".$userCompany->logo:Yii::app()->theme->base->baseUrl . "/img/default.png", $str);
            else {
                $payroller = AppPayroller::getPayrollerCompanyByPayrollerID($model->payroll->timesheet->contract->payroller->id);
                $str = str_replace('[{PayrollerLogo}]', isset($payroller->logo)? Yii::app()->baseUrl ."/uploads/photos/".$payroller->logo:Yii::app()->theme->base->baseUrl . "/img/default.png", $str);
            }
            /*             * *************************** */


            return $str;
        }


        return null;
    }

    public static function isIssued($model) {

        $payRoll = $model;



        if (( $model instanceof Timesheet) && isset($model->payrolls[0])) {
            $payRoll = $model->payrolls[0];
        }

        if ($payRoll != null) {
            if (isset($payRoll->payslipEntries) && count($payRoll->payslipEntries) > 0)
                return true;
        }


        return false;
    }

    public static function importPaySlip(&$paySlipEntry, $file, $bulkImport = false) {

        //Import uploaded file to Database
        $handle = fopen($file, "r");

        $headerFlag = false;
        $csvHeader = array();
        $importCounter = 0;
        $updateCounter = 0;
        $msg = 'No Data Import';
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {


            if ($headerFlag == false) {
                $headerFlag = true;

                foreach ($data as $column)
                    $csvHeader[] = $column;
            } else {

                $model = clone $paySlipEntry;
                $inputArr = array();
                foreach ($data as $key => $val) {

                    $attName = $csvHeader[$key];

                    $inputArr[$attName] = $val;
                }

                $model->attributes = $inputArr;
                $model->created_at = time();

                if ((isset($model->payroll_id) && AppPayroller::authorizeOwner($model->payroll_id))) {

                    $oldEntry = PayslipEntry::model()->findByAttributes(array('payroll_id' => $model->payroll_id));

                    if ($oldEntry != null) {
                        $oldEntry->attributes = $model->attributes;
                        $oldEntry->update();
                        $updateCounter++;
                    } elseif (($model->isNewRecord) ? $model->insert() : $model->update()) {
                        $importCounter++;
                    }

                    if ($bulkImport == false && ($importCounter + $updateCounter) > 0)
                        break;
                }
            }
        }

        $state = false;
        if ($importCounter > 0 && $updateCounter > 0) {
            $state = true;
            $msg = $importCounter . ' payslip(s) have been successfully added and ' . $updateCounter . ' payslip(s) have been successfully updated.';
        } elseif ($importCounter > 0) {
            $state = true;
            $msg = $importCounter . ' payslip(s) have been successfully added.';
        } elseif ($updateCounter > 0) {
            $state = true;
            $msg = $updateCounter . ' payslip(s) have been successfully added.';
        }

        return array('status' => $state, 'message' => $msg);
    }

    public static function notifyUsersOnPaySlipIssued(&$payRoll) {
        if ($payRoll != null) {
            $sheets = $payRoll->timesheet;
            $uname = $sheets->contract->user->first_name . ' ' . $sheets->contract->user->last_name;


            $employeeMsg = "PaySlip of your Timesheet - " . AppTimeSheet::genTimeSheetName($sheets) . ' has been generated, please contact Company Admin for further proceedings.';
            /*             * * Sent to Employeer * */
            $not_result = AppInterface::notification($sheets->contract->user->id, 4, 0, $sheets->contract->id, $sheets->contract->user_id);
            $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
            $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;
            $notifier = new Notification();
            $notification_count = $notifier->getUserNotificationUnseenCount($sheets->contract->user->id);
            $notification = Yii::app()->getModule('notification');
            $notification->send(array(
                'push' => array('server_notification',
                    array('id' => $sheets->contract->user->id,
                        'msg' => sprintf($not_result->notificationType->notice, AppTimeSheet::genTimeSheetName($sheets)),
                        'url' => $url, 'logo' => $logo, 'count' => $notification_count
                    )
                ),
                'email' => array(
                    'template' => 'timesheet-notify',
                    'params' => array('NAME' => $uname, "MESSAGE" => $employeeMsg),
                    'to' => array(array('email' => $sheets->contract->user->email, 'name' => $uname)),
                    'from' => array('email' => AppSetting::getSettingByKey("SUPPORT_EMAIL")->value, 'name' => 'Support')
                )
                    )
            );

            // Sent to Company admin
            $not_result = AppInterface::notification($sheets->contract->company->admin->id, 4, 0, $sheets->contract->id, $sheets->contract->user_id);
            $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
            $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;
            $notifier = new Notification();
            $notification_count = $notifier->getUserNotificationUnseenCount($sheets->contract->user->id);
            $notification = Yii::app()->getModule('notification');
            $notification->send(array('push' => array('server_notification', array('id' => $sheets->contract->company->admin->id, 'msg' => sprintf($not_result->notificationType->notice, AppTimeSheet::genTimeSheetName($sheets)), 'url' => $url, 'logo' => $logo, 'count' => $notification_count))));

//            Sent to super admin
            $msg = $uname . ' has been issued a new Payslip.';
            $not_result = AppInterface::notification(AppUser::get_super_user()->id, 12, 0, $sheets->contract->id, $sheets->contract->user_id);
            $url = AppUser::getNotificationLink($not_result->id, $not_result->notification_type_id, $not_result->timesheet_id, $not_result->contract_id, $not_result->user_id);
            $logo = NotificationType::model()->findByPk($not_result->notification_type_id)->logo;
            $notifier = new Notification();
            $notification_count = $notifier->getUserNotificationUnseenCount(AppUser::get_super_user()->id);
            $notification->send(array(
                'push' => array('server_notification', array('id' => AppUser::get_super_user()->id, 'msg' => sprintf($not_result->notificationType->notice, $uname), 'url' => $url, 'logo' => $logo, 'count' => $notification_count)),
                'email' => array(
                    'template' => 'Email',
                    'params' => array('BODY' => $msg),
                    'to' => array(array('email' => AppUser::get_super_user()->email, 'name' => 'Super Admin')),
                    'from' => array('email' => AppSetting::getSettingValueByKey('SUPPORT_EMAIL'), 'name' => 'Speedy Payrolls')
            )));
        }


        return false;
    }

}
?>

