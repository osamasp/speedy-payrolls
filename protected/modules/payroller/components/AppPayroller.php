<?php

/*
 * Description: Component for User Module
 * Author: Usama Ayaz
 * Dated:  2-June-2014
 */

class AppPayroller extends CComponent {

    public function __construct($ip = "") {
        
    }

    /*     * ******** */
    /*     * **
     *  Function That Returns The getPayrollerEntries
     *  Input Parms: $args = array( 
     *                              'size'=>15,
     *                              'contract'=>CONTRACTID,
     *                              'user'=>"USERID",
     *                              "sort"=>"t.start_time ASC",
     *                              'payroller'=>'USERID',
     *                              'status'=>array('pendding')
     *                            )
     *     $args['size'] => For Page Size 
     *     $args['contract'] => Filter By Contracts 
     *     $args['user'] => Filter By Users  
     *     $args['sort'] => ORDER BY PARAMS
     *     $args['payroller'] => Filter By Payroller  
     *     $args['status'] => Filter By Status 
     *       
     * 
     */
    
    public static function getPayroller($id){
        $payroller = Payroller::model()->findByPk($id);
        return $payroller;
    } 
     public static function getPayrollerCompanyByPayrollerID($id){
        $payroller = Payroller::model()->findByAttributes(array('payroller_id' => $id));
        return $payroller;
    }
    public static function getPayrollerEntries($args) {

        $c = new CDbCriteria();
        $paramArray = array();

        if (isset($args['user']) || isset($args['payroller']))
            $c->join = "INNER JOIN sp_timesheet tsheet on tsheet.id = t.timesheet_id INNER JOIN sp_contract con ON con.id = tsheet.contract_id ";
        else
            $c->join = "INNER JOIN sp_timesheet tsheet on tsheet.id = t.timesheet_id ";




        /*         * *** Filter By User *** */
        if (isset($args['user']) && AppUser::isUserAdmin() == false) {

            $c->addCondition(' con.user_id = :userId ');
            $paramArray[':userId'] = $args['user'];
        } elseif (AppUser::isUserAdmin()) {
            $contracts = Contract::model()->findByAttributes(array('role' => 'admin', 'user_id' => AppUser::getUserId()));
            if ($contracts != null) {
                $c->join .= " INNER JOIN  sp_company sc on sc.id = con.company_id  ";
                $c->addCondition('sc.id = :companyId');
                $paramArray[':companyId'] = $contracts->company_id;
            }
        }
        /**/

        /*         * ** Filter By Payroller ** */
        if (isset($args['payroller'])) {
            $c->addCondition('con.payroller_id = :payUID');
            $paramArray[':payUID'] = $args['payroller'];
        }
        /*         * *********************** */

        /*         * ***** Filter By Contract ** */
        if (isset($args['contract'])) {
            $c->addCondition(' con.id = :contractId ');
            $paramArray[':contractId'] = $args['contract'];
        }
        /**/

        /*         * * If Status is Provided * */
        if (isset($args['status']) && is_array($args['status'])) {
            $statusArr = implode("','", $args['status']);
            $statusArr = "'" . substr($statusArr, 0, strlen($statusArr) - 2);
            $c->addCondition('t.status IN (:sheetStatus)');
            $paramArray[':sheetStatus'] = $statusArr;
        } elseif (isset($args['status'])) {
            $c->addCondition('t.status = :sheetStatus');
            $paramArray[':sheetStatus'] = $args['status'];
        }

        /*         * ************************ */


        /*         * ****** Order By Query *** */
        if (isset($args['sort'])) {
            $c->order = $args['sort'];
        } else {
            $c->order = ' t.created_at desc';
        }





        $c->params = $paramArray;

        $c->together = true;



        $count = Payroll::model()->count($c);
        $pages = new CPagination($count);

        $pages->pageSize = (isset($args['size'])) ? (int) $args['size'] : Yii::app()->params['pagination']['payrollSize'];
        $pages->applyLimit($c);
        $entries = Payroll::model()->findAll($c);

        return array('dataSet' => $entries, 'pages' => $pages);
    }

    public static function authorizeOwner($payroll) {

        if (!($payroll instanceof Payroll)) {
            $payroll = Payroll::model()->findByPk($payroll);
        }

        return ($payroll != null && $payroll->timesheet->contract->payroller_id == AppUser::getUserId()) ? true : false;
    }

    public static function getPayrolls($status = '', $uid = '', $is_api = false, $page = 1, $size = 10) {
        $c = new CDbCriteria();
        $c->join = 'INNER JOIN sp_timesheet ts ON t.timesheet_id = ts.id';
        $c->join .= ' INNER JOIN sp_contract c ON ts.contract_id = c.id'
                . ' AND (c.company_id = ' . AppUser::getUserCompany()->id
                . ' OR c.parent_id IN (SELECT id FROM sp_contract'
                . ' WHERE company_id = "' . AppUser::getUserCompany()->id . '" '
                . 'AND is_internal = 1 ))';


        if ($status != '') {
            $c->addCondition('t.status = "' . $status . '"');
        }

        if ($uid != '') {
            $c->addCondition('c.user_id = ' . $uid);
        }

        $c->order = "c.created_at desc";
        $payrolls = Payroll::model()->findAll($c);
        $count = Payroll::model()->count($c);
        $pages = new CPagination($count);
        $result = array();
        $pages->pageSize = $size; ///*** This should come from constant
        $pages->applyLimit($c);
        if ($is_api) {
            $i = 0;
            foreach ($payrolls as $payroll) {
               $result[$i]['Result'] = $payroll; 
               $result[$i]['contract_name'] = AppContract::getContractCustomName($payroll->timesheet->contract).AppTimeSheet::printMergeLogName($payroll->timesheet);
               $i++;
            }
            $response = array('payrolls' => $result, 'pages' => $pages->getPageCount(), 'current_page' => $pages->getCurrentPage());
            return $response;
        } else {
            $response = array('payrolls' => $payrolls, 'pages' => $pages);
            return $response;
        }
    }

    public static function addPayroller($formData, $user_id) {
        $payroller = new Payroller();
        $payroller->id = AppInterface::getUniqueId();
        $payroller->payroller_id = $user_id;
        $payroller->company_name = $formData['Company']['name'];
        $payroller->company_address = $formData['Company']['address'];
        $payroller->company_street = $formData['Company']['street'];
        $payroller->company_city = $formData['Company']['city'];
        $payroller->company_country = $formData['Company']['country'];
        $payroller->company_postcode = $formData['Company']['postcode'];
        $payroller->company_phone = $formData['Company']['phone'];
        $payroller->company_sector = $formData['Company']['sector'];
        $payroller->scenario = Payroller::SCENARIO_ADD_PAYROLLER;
        if ($payroller->insert()) {
            return $payroller;
        }
        return $payroller->errors;
    }

    public static function getPayrollerCompanies() {

        $criteria = new CDbCriteria();
        $criteria->distinct = true;
        $criteria->join = " INNER JOIN sp_contract con on con.company_id = t.id ";
        $criteria->addCondition('t.payroller_id = :payrollerId ');
        $criteria->addCondition('t.status=1');
        $criteria->params = array(":payrollerId" => AppUser::getUserId());
        $criteria->together = true;

        //dd($criteria);
        $count = Company::model()->count($criteria);
        $pages = new CPagination($count);

        $pages->pageSize = 15;
        $pages->applyLimit($criteria);



        $entries = Company::model()->findAll($criteria);
        return array('dataSet' => $entries, 'pages' => $pages);
    }

    public static function getPayrollerEmployees($companyId = '', $payrollerCheck = true) {

        $criteria = new CDbCriteria();
        $criteria->distinct = true;

        if ($companyId != '') {

            $criteria->addCondition(' t.company_id = :companyId');
            $criteria->params = array(':companyId' => $companyId);
        }

        if ($payrollerCheck) {
            $criteria->addCondition(' t.payroller_id = :payrollId ');
            $criteria->params[':payrollId'] = AppUser::getUserId();
        }
        $criteria->join = "INNER JOIN sp_company c on c.id=t.company_id";
        $criteria->addCondition('c.status=1');
        $criteria->addCondition(' t.is_internal = 1');
        $criteria->addCondition("t.role != 'Director'");
        $criteria->together = true;

        $count = Contract::model()->count($criteria);
        $pages = new CPagination($count);

        $pages->pageSize = 15;
        $pages->applyLimit($criteria);



        $entries = Contract::model()->findAll($criteria);
        return array('dataSet' => $entries, 'pages' => $pages);
    }

    public static function checkBulkPermission() {
        $payroller = Payroller::model()->findByAttributes(array('payroller_id' => AppUser::getUserId()));
        if ($payroller->can_bulk_import == 1) {
            return true;
        } else {
            return false;
        }
    }

}
?>

