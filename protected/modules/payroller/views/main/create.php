<?php
/* @var $this MainController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List User', 'url'=>array('index')),
	array('label'=>'Manage User', 'url'=>array('admin')),
);
?>

<div class="box-header">
    <h3 class="box-title">Create User</h3>
</div>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>