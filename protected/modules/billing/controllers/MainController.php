<?php

class MainController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array(),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index'),
                'roles' => array('super_admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        if (isset($_POST['reset'])) {
            if (isset($_POST['timesheet'])) {
                foreach ($_POST['reset'] as $companyID => $on) {
                    Company::resetTimesheetCount($companyID);
                }
            } else if (isset($_POST['payslip'])) {
                foreach ($_POST['reset'] as $companyID => $on) {
                    Company::resetPayslipCount($companyID);
                }
            }
        }
        $companies = Company::model()->findAllByAttributes(array("status" => 1));
//        dd($companies);
        $this->render('index', array(
            'companies' => $companies,
        ));
    }

    public function actionEdit($id) {
        $company = Company::model()->findByPk($id);
        if(isset($_POST['Company'])){
            $company->billing_date = $_POST['Company']['billing_date'];
            $company->contract_note = $_POST['Company']['contract_note'];
            $company->save();
            Yii::app()->user->setFlash('success',"Billing information of <b>".$company->name."</b> is updated successfully.");
            $this->redirect(array('index'));
        }
        $this->render('edit', array(
            'company' => $company,
        ));
    }

}
