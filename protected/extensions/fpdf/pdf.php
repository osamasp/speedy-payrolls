<?php
define('FPDF_FONTPATH','font/');
require('fpdf.php');

class PDF extends FPDF
{
    public $details;
    

    function Header() {
        $this->setY(0);
        $this->SetFont("Arial", "B", 20);
        $this->Cell("0", "10", "", "", "1");
        if(!empty($this->details['logo']))
        {
            $image = Yii::app()->createAbsoluteUrl('/uploads/photos').'/'.$this->details['logo'];
            $this->Cell ($this->Image($image, $this->GetX(), $this->GetY(), 30));
        }
        else
        {
            $this->Cell('10', '10', $this->details['company'], '','0','L');
        }
        $this->SetFont("Arial", "", "11");
        $this->SetTextColor(180, 180, 180);
        $this->Cell('0', '5', $this->details['company'] , '', '0', 'R');
        $this->Cell("0", "5", "", "", "1");
        $this->Cell('0', '5', $this->details['companyAddress'] , '', '0', 'R');
        $this->Cell("0", "5", "", "", "1");
        $this->Cell('0', '5', $this->details['post_code'] .' '. $this->details['companyCity'] , '', '0', 'R');
    }
    
    function Footer() {
        $this->SetY(-20);
        $this->SetTextColor(180,180,180);
	$this->Cell('0','10',"Name: ". $this->details['contractor_name'],'','0','L');
        $this->Cell('0','10',"VAT NO.:".$this->details['vat_number'],'','0','R');
        $this->Cell('0', '10', "", "", "1");
        $this->Cell("0",'10',"Address".$this->details['contractor_address'],'','0','L');
    }
}