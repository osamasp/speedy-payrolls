<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DOMPdf
 *
 *  Yii::import('ext.vendors.pdf.DOMPdf',true);
 *     $model = $this->loadModel();
 *     $html = $this->renderPartial('_print', array('model'=>$model), true, true);
 *     $pdf = new DOMPdf();
 *     $pdf->render($html, 'FILENAME_PDF');
 * 
 * 
 * @author Usama
 */
class DOMPdf {
    public function render($html,$filename,$attachment=1,$paper='a4',$orientation='portrait')
	{
		Yii::import('application.extensions.pdf.dompdf.*');
		require_once ('dompdf_config.inc.php');
		Yii::registerAutoloader('DOMPDF_autoload');
		$dompdf=new DOMPDF();
		$dompdf->load_html($html);
		$dompdf->set_paper($paper,$orientation);
		$dompdf->render();
		$dompdf->stream($filename.".pdf", array("Attachment"=>$attachment));
	}
}
