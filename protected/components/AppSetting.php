<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class AppSetting extends CComponent {

    
    public static function getSettingByKey($key) {
        $setting = Settings::model()->findByAttributes(array('key'=>$key));
        if($setting instanceof Settings){
            return $setting;
        }
        return null;
    } 
    public static function getSettingValueByKey($key) {
        $setting = Settings::model()->findByAttributes(array('key'=>$key));
        if($setting instanceof Settings){
            return $setting->value;
        }
        return null;
    }
    public static function getSettingsByCategory($category){
        $settings = Settings::model()->findAllByAttributes(array('category'=>$category));
        if($settings){
            return $settings;
        }
        return null;
    }
    
    public static function generateAjaxResponse($input)
    {
        if(is_array($input))
        echo json_encode($input);
        else
        echo $input;
        
        
        die();
    }
       
    public static function formatUploadName($str){
        
        $str = rand(10000,200000).'_'.str_replace(' ', '', $str);
        return $str;       
    }
    
public static function genAutoFileName($fileName){
    $arr = explode('.', $fileName);
    $rand = time().'_'.mt_rand(1000, 10000);   
    return $rand.'.'.$arr[count($arr)-1];   
}    
}

?>
