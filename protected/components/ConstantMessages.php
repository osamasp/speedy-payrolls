<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ConstantMessages extends CComponent {
    public static function employeeAdd(){
        return "You have been invited by ".AppUser::getCurrentUser()->first_name;
    }
    public static function contractApprover(){
        return 'You have been nominated by '.AppUser::getCurrentUser()->first_name.' to become their Timesheet approver on SpeedyPayrolls.';
    }
    public static function copyToPayroller($sheets){
        return "Your Timesheet of " . AppTimeSheet::genTimeSheetName($sheets) . ' has been forwarded to payroller.';
    }
    
    public static $flag_on = 1;
    public static $flag_off = 0;
    
    //Web service messages
    public static $payroller_created = "Payroller Created!";
    public static $req_select = "Please select any raw...";
    public static $timesheet_cannot_created = "TimeSheet cannot be created";
    public static $clock_in = "Clock In Successfully";
    public static $lunch_in = "Lunch In Successfully";
    public static $lunch_out = "Lunch Out Successfully";
    public static $clock_out = "Time Out Successfully";
    public static $note_added = "Note Added Successfully";
    public static $notes_found = "Notes Found";
    public static $notes_not_found = "Notes Not Found";
    public static $oneWay_current = "Current 1 Way Timesheets";
    public static $oneWay_completed = "Completed 1 Way Timesheets";
    public static $assign_supervisor = "Assign Supervisor Successfully.";
    public static $approver_not_found = "Unable to find approver. You can set yourself as approver.";
    public static $payslips = "Payslips";
    public static $note_type_timesheet = "timesheet";
    public static $note_type_quick_timesheet = "quick_timesheet";
    public static $note_type_user_timing = "user_timing";
    public static $activities_found = "User activities found";
    public static $activity_not_found = "No activity found";
    public static $approver_not_found_self = "Unable to find approver. You can set yourself as approver.";
    public static $approver_in_company = 'in_company';
    public static $payslip_not_found = "Payslip Not Found";
    public static $notifications_found = "Notifications Found";
    public static $notifications_not_found = "Notifications Not Found";
    public static $employees_found = "Employees Found";
    public static $employees_not_found = "Employees Not Found";
    public static $payslip_list = "Payslips List";
    public static $contact_list = "Contact List";
    public static $session_expired = "Your session is expired, login again.";
    public static $timesheet_expenses = "Timsheet Expenses";
    public static $timseeheet_expense_not_found = "Timesheet Expense Not Found";
    public static $cannot_update_token = "Cannot Update User Token";
    public static $user_invalid = "User name or password is invalid";
    public static $login_success = "Login Successfully";
    public static $logout_success = 'User Successfully logout';
    public static $account_not_verified = "Your account is not verified yet, please check verification email that was sent to you.";
    public static $cannot_create_payroller = "Payroller cannot be created";
    public static $timesheet_exist = "TimeSheet Already Exist";
    public static $user_exist = "User with this email address already exist";
    public static $staff_added = "Staff added successfully";
    public static $no_record_found = "No Record Found";
    public static $shift_already_exist = "Shift Already Exist";
    public static $no_contact_list_found = "No Contact List Found";
    public static $cannot_create_contract = "Contracts cannot be created";
    public static $cannot_create_company = "Company cannot be created";
    public static $email_already_exist = "Email already exist";
    public static $password_reset = "Password reset successfully. A new password has been sent to your email.";
    public static $password_cannot_update = "Cannot update password";
    public static $email_not_exist = "Email Address You Provide Does Not Exist In The System";
    public static $company_shifts = "Company Shifts";
    public static $shift_not_found = "Shift not found";
    public static $company_shift_updated = "Company Shift Updated";
    public static $cannot_update_shift = "Cannot update company shift";
    public static $timesheet_status_updated = "Timesheet status updated successfully";
    public static $timesheet_details = "Timesheet Details";
    public static $clock_entries = "Clock Entries";
    public static $approval_list = "Approval List";
    public static $oneWayTimesheetInvoice = "1 Way Timesheet Invoice";
    public static $invoice_sent = "Invoice Sent Successfully.";
    public static $invoice_not_send = "Invoice Not Send.";
    public static $invoices_found = "Invoices Found";
    public static $invoices_not_found = "Invoice Not Found";
    public static $timesheet_saved = "Timesheet saved successfully";
    public static $prompt_for_note = "Do you want to add note?";
    public static $timesheet_deleted = "Timesheet deleted successfully";
    public static $cannot_delete_timesheet = "Cannot delete timesheet";
    public static $cannot_save_timesheet = "Cannot save timesheet";
    public static $not_valid_token = "Not a valid token , login again.";
    public static $shift_updated = "Company shift has been updated successfully.";
    public static $not_delete_shift_user_exist = "Cannot delete this shift because the users are working on this shift.";
    public static $shift_deleted = "Company shift deleted.";
    public static $cannot_delete_shift = "Cannot delete company Shift.";
    public static $shift_created = "Shift has been successfully created.";
    public static $start_time_greater_end_time = "Start Time cannot be greater than End Time.";
    public static $cannot_create_shift = "Shift cannot be created.";
    public static $fields_missing = "Input fields are missing.";
    public static $headers_data_missing = "Headers data is missing.";
    public static $current_timesheets = "Current Timesheets";
    public static $completed_timesheets = "Completed Timesheets";
    public static $undefined_type = "Timesheet type is undefined";
    public static $user_registered_success = "You have successfully registered! Verification email is sent to your email address please verify your email.";
    public static $cannot_create_contact = "Cannot create contact";
    public static $cannot_create_note = "Cannot create note";
    public static $timesheet_already_exist = "Timesheet with this date already exist";
    public static $cannot_update_timesheet = "Cannot update timesheet";
    public static $failed_to_clock_in = "Failed to clock in";
    public static $failed_to_lunch_in = "Failed to lunch in";
    public static $failed_to_lunch_out = "Failed to lunch out";
    public static $failed_to_clock_out = "Failed to clock out";
    public static $failed_to_add_note = "Failed to add note";
    public static $user_not_created = "Cannot create user";
    public static $user_created = "User created successfully";
    public static $company_not_created = "Cannot create company";
    
    //*** Timesheet Constants **//
    
    public static $timesheet_type_current = "current";
    public static $timesheet_type_completed = "completed";
    public static $dispatch_flag_off = 0;
    public static $dispatch_flag_on = 1;
    public static $timesheet_type_out = "out";
    public static $timesheet_type_inout = "in_out";
    public static $timesheet_type_outb = "out_b";
    public static $timesheet_type_in = "in";
    public static $timesheet_type_custom = "custom";
    public static $timesheet_type_monthly = "monthly";
    public static $timesheet_type_daily = "daily";
    public static $timesheet_status_pending = "pending";
    public static $timesheet_status_approved = "approved";
    public static $timesheet_status_disapproved = "disapproved";
    public static $contract_type_internal = "internal";
    public static $two_way = "2 Way ";
    public static $three_way = "3 Way ";
    public static $non_billable = "Non Billable ";
    public static $out_of_branch = "You are not allowed to perform this action. Please make sure you are close to a branch.";
    public static $payrates_found = "Payrates and Items Found";
    public static $payrates_not_found = "No Payrates and Items Found";
    
    public static $employee_type_employee = "Employee";
    public static $employee_type_full = "full_time";
    public static $employee_type_director = "Director";
    public static $employee_type_admin = "admin";
    public static $employee_type_individual = "Individual";
    
    public static $user_type_employee = 2;
    public static $user_type_admin = 1;
    
    public static $clock_entry_list = 1;
    public static $clock_approval_list =2;
    public static $clocked_in = 'in';
    public static $lunched_in = 'l_in';
    public static $lunched_out = 'l_out';
    public static $clocked_out = 'out';
    public static $note = 'note';
    public static $contact_added = 'Contact Successfully added.';
    
    public static $default_privilege_id = 29450400;
}
