<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *  $mailer = new AppMailer();
 *  $mailer->prepareBody('SignUpEmail',array('USER'=>'John Doe', 'ACTIVATIONKEY' => '121312{8ASDASQZqqw' )  ); 
 *  $mailer->sendMail(array('email'=>'john@abc.com' , 'name'=>'john doe') , array('email'=>'support@site.com' name=>'Support'));
 * 
 * 
 */
if (version_compare(PHP_VERSION, '5.3.0', '<')) {
            @set_magic_quotes_runtime(false);
            ini_set('magic_quotes_runtime', 0);
}
class AppMailer extends CComponent {

    protected $model = null;
    protected $bodyTemplate = '';
    protected $body = '';
    protected $siteEmailBodySlug = 'SiteEmailBody';
    protected $emailBodyTag = 'BODYCONTENT';
    protected $beforeTag = '[{';
    protected $endTag = '}]';
    protected $subject = 'Support';
    protected $is_ses = false;

    public function __construct() {
        Yii::import('application.models.Cms');
        
        Yii::import('application.extensions.phpmailer.JPhpMailer');
              
        $this->setCMSModel();
        $this->setBodyTemplate();
    }

    public function setsiteEmailBodySlug($input) {
        if ($input != "")
            $this->siteEmailBodySlug = $input;
    }

    public function setCMSModel() {
        $this->model = new Cms();
    }

    public function setBodyTemplate() {
        if ($this->model != null) {
            $result = $this->model->getCMSContentBySlug($this->siteEmailBodySlug);
            $this->bodyTemplate = $result['content'];
            $tempStr = $this->bodyTemplate;
            $setting = new AppSetting;
            $replaceitems = $setting->getSettingsByCategory('email');
            if ($replaceitems) {
                foreach ($replaceitems as $val) {
                    $tempStr = str_replace($this->beforeTag . $val->key . $this->endTag, $val->value, $tempStr);
                }
            }
            $this->bodyTemplate = $tempStr;
            return true;
        } else {

            return false;
        }
    }

    /*     * **
     * Prepare Body Function: it prepares the body of email and replace the variable/key with the value in the template body content.
     * Input Parameters: contentSlug , ele
     *   ContentSlug: template slug of the email which is defined in the cms table.
     *   ele        : its an array which contains the key value elements which will be replaced by the template content which is going to be extracted through content slug.
     *                eg. USERNAME => 'John Doe' this will be replaced by beforeTag+USERNAME+endTag = john doe in content string 
     * 
     */

    public function prepareSubject($subject) {
        $this->subject = $subject;
    }


    public function prepareBody($contentSlug, $ele) {
        if ($contentSlug != "") {
            $result = $this->model->getCMSContentBySlug($contentSlug);
            $this->body = $result['content'];
            $this->subject = $result['title'];
        } else {
            return false;
        }
        $loginUrl = AppInterface::getAbsoluteUrl('/user/main/login') ;
        $tempStr = $this->body;
        $ele['LOGIN_URL'] = $loginUrl;

        if ($this->body != "") {
            foreach ($ele as $key => $val)
                $tempStr = str_replace($this->beforeTag . $key . $this->endTag, $val, $tempStr);
        }

        $this->body = str_replace($this->beforeTag . $this->emailBodyTag . $this->endTag, $tempStr, $this->bodyTemplate,$loginUlr);

        return true;
    }

    /*     * **
     *  Input Parameters: $to , $from , $cc , $bcc
     *  to    :   this array contains the information about the recepient(s). It should be in this format array('email'=>'xyz@abc.com' , 'name'=>'John Doe')
     *  from  :   this array contains the information about the sender. It should be in this format array('email'=>'abc@abc.com' , 'name'=>'John Doe')
     *  cc    :   this array contains the information about the cc addresses. It should be in this format array('email'=>'qwe@abc.com' , 'name'=>'John Doe')
     *  bcc   :   this array contains the information about the bcc addresses. It should be in this format array('email'=>'xyz@abc.com' , 'name'=>'John Doe')
     */

    public function sendMail($to, $from = array(), $cc = array(), $bcc = array()) {

        if (is_array($to) && count($to)) {
            $mail = new JPhpMailer;
            $mail->SetFrom($from['email'], $from['name']);
            $mail->Subject = $this->subject;
            $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
            $mail->MsgHTML($this->body);
            foreach ($to as $user){
                $mail->AddAddress($user['email'], $user['name']);
                
            }
            if (is_array($cc) && count($cc) > 0) {
                foreach ($cc as $user)
                    $mail->AddCC($user['email'], $user['name']);
            }

            if (is_array($bcc) && count($bcc) > 0) {
                foreach ($bcc as $user)
                    $mail->AddBCC($user['email'], $user['name']);
            }
            if($this->getIs_ses())
            {
                $response = array();
                /* @var $user type */
                if (!$this->is_local()) {
                    foreach ($to as $user) {
                        $response[$user['email']] = Yii::app()->ses->email()
                                ->addTo($user['email'])
                                ->setFrom($from['email'])
                                ->setSubject($this->subject)
                                ->setMessageFromString("", $this->body) //Pass a string body and html body
                                ->setSubjectCharset('ISO-8859-1')
                                ->setMessageCharset('ISO-8859-1')
                                ->send();
                    }
                }
                return $response;
            }
            else
            {
                $mail->isSMTP();                                      // Set mailer to use SMTP
                $mail->Host = smtp_host_name;                     // Specify main and backup SMTP servers
                $mail->SMTPAuth = true;                               // Enable SMTP authentication
                $mail->Username = smtp_user_name;                 // SMTP username
                $mail->Password = smtp_password;                           // SMTP password
                $mail->SMTPSecure = smtp_secure;                            // Enable TLS encryption, `ssl` also accepted
                $mail->Port = 587;                                    // TCP port to connect to
                return $mail->Send();
//                return $mail->Send();
            }
        } else {
            return false;
        }
    }
    
    public function sendMailWithAttach($to, $from, $attachement)
    {
        if (is_array($to) && count($to)) {
            $mail = new JPhpMailer;
            $mail->SetFrom($from['email'], $from['name']);
            $mail->Subject = $this->subject;
            $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
            $mail->AddAttachment($attachement);
            $mail->MsgHTML($this->body);

            $mail->AddAddress($to['email'], $to['name']);
            if ($this->is_local()) {
                    //local work if required in place of email
                } else {
                    return $mail->Send();
                }
        } else {
            return false;
        }
    }
    
    public function sendSesMailWithAttach($to, $from, $subject,$body,$attachement, $file_name)
    {
        if($this->getIs_ses()){
            $rn = "\n";
            $msg = "To: ".$to["email"].$rn;
            $msg .="From: ".$from["email"].$rn;
            //in case you have funny characters in the subject
            $subject = mb_encode_mimeheader($subject, 'UTF-8');
            $msg .="Subject: $subject".$rn;
            $msg .="MIME-Version: 1.0".$rn;
            $msg .="Content-Type: multipart/alternative;".$rn;
            $boundary = uniqid("_Part_".time(), true); //random unique string
            $msg .=" boundary=\"$boundary\"".$rn;
            $msg .=$rn;
            //now the actual message
            $msg .="--$boundary".$rn;
            //first, the plain text
            $msg .="Content-Type: text/plain; charset=utf-8".$rn;
            $msg .="Content-Transfer-Encoding: 7bit".$rn;
            $msg .=$rn;
            $msg .=strip_tags($body);
            $msg .=$rn;
            //now, the html text
            $msg .="--$boundary".$rn;
            $msg .="Content-Type: text/html; charset=utf-8".$rn;
            $msg .="Content-Transfer-Encoding: 7bit".$rn;
            $msg .=$rn;
            $msg .=$body;
            $msg .=$rn;
            //add attachments
            if (!empty($attachement)) {
                $msg .="--$boundary".$rn;
                $msg .="Content-Transfer-Encoding: base64".$rn;
                $clean_filename = mb_substr($file_name, 0, 60);
                $msg .='Content-Type: application/octet-stream; name="'.$clean_filename.'"'.$rn;
                $msg .='Content-Disposition: attachment; filename="'.$clean_filename.'"'.$rn;
                $msg .=$rn;
                $msg .=base64_encode(file_get_contents($attachement));
                    $msg .="==".$rn;
                $msg .="--$boundary";
            }
            //close email
            $msg .="--".$rn;
            $response = 0;
            if($this->is_local()){

            }
            else{
                $response = Yii::app()->ses->sendRaw($msg);
            }
            return $response;
        }
        else{
            if (is_array($to) && count($to)) {
                $mail = new JPhpMailer;
                $mail->SetFrom($from['email'], $from['name']);
                $mail->Subject = $this->subject;
                $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
                $mail->AddAttachment($attachement);
                $mail->MsgHTML($this->body);
                $mail->AddAddress($to['email'], $to['name']);
                $mail->isSMTP();                                      // Set mailer to use SMTP
                $mail->Host = smtp_host_name;                     // Specify main and backup SMTP servers
                $mail->SMTPAuth = true;                               // Enable SMTP authentication
                $mail->Username = smtp_user_name;                 // SMTP username
                $mail->Password = smtp_password;                           // SMTP password
                $mail->SMTPSecure = smtp_secure;                            // Enable TLS encryption, `ssl` also accepted
                $mail->Port = 587;                                    // TCP port to connect to
            
            if ($this->is_local()) {
                    //local work if required in place of email
                } else {
                    return $mail->Send();
                }
        } else {
            return false;
        }
        }
    }
    
    public static function setIs_ses($boolVal){
        $this->is_ses = $boolVal;
    }
    public function getIs_ses(){
        return FALSE;
        if (strpos($_SERVER['REQUEST_URI'], 'speedypayrolls.com')){
            return TRUE;
        }
        else
            return FALSE;
    }
    public function is_local(){
        if ($_SERVER['SERVER_NAME'] == "localhost"){
            return TRUE;
        }
        else
            return FALSE;
    }
}

?>
