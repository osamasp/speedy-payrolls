<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class AppLogging extends CComponent {
    
   /**** Log Add Method **/
   /** Function:addLog($message,$type,$path) 
    * 
    * @param type $message = "TEST MESSAGE"
    * @param type $type = "success" OR "error"
    * @param type $path = "application.timesheet.main.index"
    */
   /**/ 
    
   public static function addLog($message,$type,$path){
       
       $level = "info";
       
       switch($type){
           case 'success':$level = "info";break;
           case 'error':$level = "error";break;
           
       }
       
       Yii::log($message,$level,$path);   
   }
    

}

?>
