<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class AppInterface extends CComponent {

    public static function showDialogBox($title, $msg) {
        Yii::app()->user->setFlash('dialog_message', $msg);
        Yii::app()->user->setFlash('dialog_title', $title);
    }

    public static function getProjectName() {
        return 'Seminant Solutions Ltd';
    }

    public static function last_monday($date) {
        if (date('w', $date) == 1)
            return $date;
        else
            return strtotime(
                    'last monday', $date
            );
    }
    
    public static function getRealPath($folder){
        return Yii::getPathOfAlias('webroot') . '/uploads/'.$folder.'/';
    }
    
    public static function getHeadersData($key){
        // dd($_SERVER['HTTP_'.strtoupper($key)]);
        if(isset($_SERVER['HTTP_'.strtoupper($key)])){
        return $_SERVER['HTTP_'.strtoupper($key)];
        }
        return null;
    }
    
    public static function getPutRequestData(){
        parse_str(file_get_contents("php://input"), $put_vars);
        return $put_vars;
    }
    
    public static function getHash($string){
        return md5($string);
    }
    
    public static function getAbsoluteUrl($url,$params=array()){
        return Yii::app()->createAbsoluteUrl($url,$params);
    }
    
    public static function generateToken($len = 16) {
        $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $len);
        return $randomString;
    }

    public static function GenerateCSV($file, $head = array(), $data = array()) {
        foreach ($head as $item) {
            file_put_contents($file, $item . ',', FILE_APPEND);
        }
        file_put_contents($file, "\r\n", FILE_APPEND);
        foreach ($data as $item) {
            foreach ($item as $key => $value) {
                if ($key == 'created_at')
                    $value = date('d/m/Y', $value);
                if ($key == 'time_in')
                    $value = date('h:i a', $value);
                if ($key == 'time_out')
                    $value = date('h:i a', $value);
                if ($key == 'hours')
                    $value = date('h:i', $value);
                if ($key == 'date'){
                    $value = date('d/m/Y', $value);
                }
                if ($key == 'start_time'){
                    $value = date('h:i a', $value);
                }
                if ($key == 'end_time'){
                    $value = date('h:i a', $value);
                }
                if (in_array($key, $head))
                    file_put_contents($file, $value . ',', FILE_APPEND);
//                else
//                    file_put_contents ($file, $value.',', FILE_APPEND);
            }
            file_put_contents($file, "\r\n", FILE_APPEND);
        }
    }

    public static function secondsToTime($seconds, $format = array()) {
        $dtF = new DateTime("@0");
        $dtT = new DateTime("@$seconds");
        $last_element = end($format);
        $_format = "";
        foreach ($format as $item) {
            if ($item != $last_element)
                $_format .= "%" . $item . ":";
            else
                $_format .= "%" . $item;
        }
        return $dtF->diff($dtT)->format($_format);
    }

    public static function getdateformat($isJs = false) {
        if ($isJs)
            return 'dd/mm/yyyy';
        return 'd/m/Y';
    }
    
    public static function customMimeType($filename)
    {
        if(!function_exists('mime_content_type')) {
        $mime_types = array(

            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',

            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',

            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',

            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );
        }
    }

    public static function getClientIP() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public static function generateCompanyCode($name, $charlen = 3, $numlen = 3) {
        $name = strtoupper(trim(preg_replace('/\s+/', ' ', $name)));
        $words = explode(' ', $name);
        $length = $charlen - count($words) + 1;
        $length = $length <= 0 ? 1 : $length;
        $code = strlen($words[0]) >= $length ? substr($words[0], 0, $length) : $words[0];
        for ($i = 1; $i < count($words) && $i < $charlen; $i++) {
            $code .= substr($words[$i], 0, 1);
        }
        while (strlen($code) < 3) {
            $code .= rand(0, 9);
        }
        for ($i = 0; $i < $numlen; $i++) {
            $code .= rand(0, 9);
        }
        return $code;
    }

    public static function generateBranchCode($company_id) {
        $company = Company::model()->findByPk($company_id);
        $company->branch_count++;
        $code = str_pad($company->branch_count, 3, '0', STR_PAD_LEFT);
        $company->save();
        return $code;
    }

    public static function generateSalesCode($company_branch_id) {
        $companyBranch = CompanyBranch::model()->findByPk($company_branch_id);
        $companyBranch->sales_count++;
        $companyBranch->save();
        //dd($companyBranch->errors);
        $code = str_pad($companyBranch->sales_count, 6, '0', STR_PAD_LEFT);
        return $code;
    }

    public static function getUploadedFiles(&$model, $name, $attribute = 'file_name', $folder = 'contracts') {

        if ($model->$attribute) {
            $url = Yii::app()->baseUrl . "/uploads/$folder/" . $model->$attribute;
            $value = CHtml::link('Download', $url, array('target' => '_blank'));
            $type = 'raw';
            return array('name' => $name, 'value' => $value, 'url' => $url, 'type' => $type);
        } else {
            return array('name' => $name, 'value' => '-');
        }
    }

    public static function export($rows, $coldefs, $boolPrintRows = true, $csvFileName = null, $separator = ',') {
        $endLine = PHP_EOL;
        $returnVal = '';
        if ($csvFileName == null) {
            $csvFileName = "timesheet--" . date('d-m-Y-H-i') . ".csv";
        }

        if ($csvFileName != null) {
            header("Cache-Control: public");
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=" . $csvFileName);
            header("Content-Type: application/octet-stream");
            header("Content-Transfer-Encoding: binary");
        }

        if ($boolPrintRows == true) {
            $names = '';
            foreach ($coldefs as $col => $config) {
                $names .= $col . $separator;
            }
            $names = rtrim($names, $separator);
            if ($csvFileName != null) {
                echo $names . $endLine;
            } else
                $returnVal .= $names . $endLine;
        }

        $count = 0;
        foreach ($rows as $row) {
            $r = '';
            foreach ($coldefs as $col => $config) {

                if (isset($row[$col])) {

                    $val = $row[$col];
                    foreach ($config as $conf)
                        if (!empty($conf))
                            $val = Yii::app()->format->format($val, $conf);
                    if (is_string($val))
                        $r .= $val . $separator;
                }
                else {
                    if ($col == "company_name")
                        $r .= AppUser::getUserCompany()->name . $separator;
                    else {
                        if ($col == "start_time") {
                            if ($count < count($row['timesheets']))
                                $r .= date('h i s', $row['timesheets'][$count]['start_time']) . $separator;
                            else {
                                $r .= $separator;
                            }
                        } else if ($col == "contract_name") {
                            if ($count < count($row['contracts'])) {
                                if ($row['contracts'][$count]->is_internal == true)
                                    $r .= $row['contracts'][$count]->user->first_name . ' (Internal - ' . $row['contracts'][$count]->company->name . ')' . $separator;
                                else
                                    $r .= $row['contracts'][$count]->user->first_name . ' (' . ucwords(str_replace('_', ' ', $row['contracts'][$count]->type)) . ' - ' . $row['contracts'][$count]->company->name . ')' . $separator;
                                //$r .= AppContract::getContractCustomName($row['contracts'][$count]).$separator;
                            }
                            else {
                                $r .= $separator;
                            }
                        } else if ($col == "end_time") {
                            if ($count < count($row['timesheets']))
                                $r .= date('h i s', $row['timesheets'][$count]['end_time']) . $separator;
                            else {
                                $r .= $separator;
                            }
                        } else if ($col == "collection_day") {
                            if ($count < count($row['contracts']))
                                $r .= $row['contracts'][$count]['collection_day'] . $separator;
                            else {
                                $r .= $separator;
                            }
                        } else if ($col == "start_date") {
                            if ($count < count($row['contracts']))
                                $r .= date(AppInterface::getdateformat(), $row['contracts'][$count]['start_time']) . $separator;
                            else {
                                $r .= $separator;
                            }
                        } else if ($col == "end_date") {
                            if ($count < count($row['contracts'])) {
                                if ($row['contracts'][$count]['end_time'] != "-1")
                                    $r .= date(AppInterface::getdateformat(), $row['contracts'][$count]['end_time']) . $separator;
                                else
                                    $r .= "-" . $separator;
                            }
                            else {
                                $r .= $separator;
                            }
                        } else if ($col == "timesheet_entry_date") {
                            if ($count < count($row['timesheets']))
                                $r .= date(AppInterface::getdateformat(), $row['timesheets'][$count]['start_time']) . $separator;
                            else {
                                $r .= $separator;
                            }
                        } else if ($col == "pay_rates") {
                            //$details = AppTimeSheet::compileCardDetail($row['timesheets'][$count]['id']);
                            if ($count < count($row['timesheets'])) {
                                foreach ($row['timesheets'][$count]['payrates'] as $payrate) {
                                    if ($payrate->is_item == 0) {
                                        $r .= ucwords(str_replace('_', ' ', $payrate->contractSetting->key)) . " : " . $payrate->value . " ";
                                    }
                                }
                            }
                            $r .= $separator;
                        } else if ($col == "items") {
                            if ($count < count($row['timesheets'])) {
                                foreach ($row['timesheets'][$count]['customitems'] as $rates) {
                                    $r .= ucwords($rates->contractSetting->key) . " : " . $rates->item_value . " ";
                                }
                            }
                            $r .= $separator;
                        } else if ($col == "timesheet_entry_day") {
                            if ($count < count($row['timesheets']))
                                $r .= date("l", $row['timesheets'][$count]['start_time']) . $separator;
                            else {
                                $r .= $separator;
                            }
                        } else {
                            $r .= "-" . $separator;
                        }
                    }
                }
            }
            $count++;
            $item = trim(rtrim($r, $separator)) . $endLine;
            if ($csvFileName != null) {
                echo $item;
            } else {
                $returnVal .= $item;
            }
        }
        return $returnVal;
    }

    public static function getUploadedReceipts($model, $name, $attribute = 'file_name', $folder = 'receipts', $onlyLink = FALSE) {


        if (count($model) > 0) {
            $temp = array();
            $raw = "";
            for ($i = 0; $i < count($model); $i++) {
                $url[$i] = Yii::app()->baseUrl . "/uploads/$folder/" . $model[$i];
                $value[$i] = CHtml::link($model[$i], $url, array('target' => '_blank'));
                $type[$i] = 'raw';
                if ($onlyLink) {
                    $raw .= '<a href="' . $url[$i] . '" target="_blank">' . $model[$i] . '</a><br>';
                } else {
                    $raw .= '<a href="' . $url[$i] . '" target="_black">' . $model[$i] . '</a><a href="' . Yii::app()->baseUrl . '/timesheet/quickTimesheet/deleteReceipt/name/' . $model[$i] . '"><img src="' . Yii::app()->baseUrl . '/uploads/photos/icon-x.png" width="20" height="20"></a>';
                }
                $temp[$i] = array('name' => $name, 'value' => $value[$i], 'url' => $url[$i], 'type' => $type[$i]);
            }
            return $raw;
        } else {
            return array('name' => $name, 'value' => '-');
        }
    }

    public static function getUserId($comInit = '', $comInitCount = '', $comEmpCount = '') {
        if ($comInit != '') {
            $comInit = Yii::app()->db->createCommand("SELECT CONCAT(LEFT(NAME,1),RIGHT(NAME,1)) AS companyInitials FROM sp_company Where id = '" . $company->id . "'");
            $comInit->queryRow();
        }
        if ($comInitCount != '') {
            $comInitCount = Yii::app()->db->createCommand("SELECT COUNT(*) AS initCount FROM sp_company WHERE CONCAT(LEFT(NAME,1),RIGHT(NAME,1)) = '" . $comInit . "'");
            $comInitCount->queryRow();
        }
        if ($comEmpCount != '') {
            $comEmpCount = Yii::app()->db->createCommand("SELECT COUNT(*) FROM sp_contract WHERE  is_internal = 1 AND company_id = '" . $company->id . "'");
            $comEmpCount->queryRow();
        }
        $emp_id = sprintf("%s-%04d-%07d", $comInit, $comInitCount, $comEmpCount);
        return $emp_id;
    }

    public static function getUniqueId($include_braces = false) {
        return self::getCustomUniqueID();
        if (function_exists('com_create_guid')) {
            if ($include_braces === true) {
                return com_create_guid();
            } else {
                return substr(com_create_guid(), 1, 36);
            }
        } else {
            mt_srand((double) microtime() * 10000);
            $charid = strtoupper(md5(uniqid(rand(), true)));

            $guid = substr($charid, 0, 8) . '-' .
                    substr($charid, 8, 4) . '-' .
                    substr($charid, 12, 4) . '-' .
                    substr($charid, 16, 4) . '-' .
                    substr($charid, 20, 12);

            if ($include_braces) {
                $guid = '{' . $guid . '}';
            }

            return $guid;
        }
    }

    private static function getCustomUniqueID() {
        $id = microtime(true) - '1400000000';
        $id += ip2long(self::getClientIP());
        $id -= self::generateRandomNumber(8);
        if (strpos($id, '.')) {
            list($whole, $fraction) = explode('.', $id);
            $id = $whole - $fraction;
        }
        return abs($id);
    }

    private static function generateRandomNumber($length) {
        $result = '';
        for ($i = 0; $i < $length; $i++) {
            $result .= mt_rand(0, 9);
        }
        return $result;
    }

    public static function getFilename($include_braces = false) {
        if (function_exists('com_create_guid')) {
            if ($include_braces === true) {
                return com_create_guid();
            } else {
                return substr(com_create_guid(), 1, 36);
            }
        } else {
            mt_srand((double) microtime() * 10000);
            $charid = strtoupper(md5(uniqid(rand(), true)));

            $guid = substr($charid, 0, 8) . '-' .
                    substr($charid, 8, 4);


            if ($include_braces) {
                $guid = '{' . $guid . '}';
            }

            return $guid;
        }
    }

    public static function getTimeAgo($ptime) {
        preg_match_all('!\d+!', Yii::app()->session["client_timezone"], $matches);
        $estimate_time = (time() + ($matches[0][0] * 3600)) - $ptime;

        if ($estimate_time < 60) {
            return 'Just Now';
        }
        if($estimate_time > 86400){
            return date('h:i a, d M, Y',$ptime);
        }

        $condition = array(
            12 * 30 * 24 * 60 * 60 => 'year',
            30 * 24 * 60 * 60 => 'month',
            24 * 60 * 60 => 'day',
            60 * 60 => 'hour',
            60 => 'minute',
            1 => 'second'
        );

        foreach ($condition as $secs => $str) {
            $d = $estimate_time / $secs;

            if ($d >= 1) {
                $r = round($d);
                return 'about ' . $r . ' ' . $str . ( $r > 1 ? 's' : '' ) . ' ago';
            }
        }
    }

    public static function notification($receiver, $notification_type_id = 4, $timesheet_id = 0, $contract_id = 0, $user_id = 0, $title = null, $message = null) {
        $current_user_id = AppUser::getUserId();
        $notice = new Notification();
        $notice->receiver_id = $receiver;
        $notice->sender_id = ($current_user_id)?$current_user_id:$user_id;
        $notice->notification_type_id = $notification_type_id;
        $notice->is_read = '0';
        if ($timesheet_id != 0)
            $notice->timesheet_id = $timesheet_id;
        if ($contract_id != 0)
            $notice->contract_id = $contract_id;
        if ($user_id != 0)
            $notice->user_id = $user_id;
        if(isset($title) && isset($message)){
            $notice->title = $title;
            $notice->notice = $message;
        }
        $notice->created_by = ($current_user_id)?$current_user_id:$user_id;
        $notice->modified_by = ($current_user_id)?$current_user_id:$user_id;
        $notice->modified_at = time();
        $notice->created_at = time();
        if ($notice->save()) {
            return $notice;
        }
        return null;
    }

    public static function getSkin() {
        return 'skin-blue';
    }

    public static function getUsage() {
        return array(
            '0' => 'Speedy Timesheets',
            '1' => 'Speedy Timesheets & Payrolls',
        );
    }

    public static function getHeading() {
        $currentmodule = Yii::app()->controller->module ? Yii::app()->controller->module->id : '-';
        $currentcontroller = Yii::app()->controller->id;
        $currentaction = Yii::app()->controller->action->id;
        $heading = Yii::app()->controller->id . ' ' . Yii::app()->controller->action->id;
        $headings = array(
            '-/site/index' => 'Dashboard',
            'user/main/invite' => 'Employee',
            'contract/main/index' => 'SpeedyTimesheets Accounts',
            'timesheet/main/add' => 'Add SpeedyTimesheet',
            'timesheet/main/addWizard' => 'Add SpeedyTimesheet',
            'contract/main/new' => 'Add New SpeedyTimesheet Account',
            'contract/main/in' => 'Add New SpeedyTimesheet Account',
            'contract/main/out' => 'Add New SpeedyTimesheet Account',
            'contract/main/inout' => 'Add New SpeedyTimesheet Account',
            'timesheet/main/index' => 'SpeedyTimesheets',
            'timesheet/approve/index' => 'Approve Timesheets',
            'payroller/main/AdminEmployee' => 'Accounts',
            'payroller/main/viewpayrolls' => 'Payrolls',
            'payroller/main/dispatchPayroll' => 'Dispatch For Payroll',
            'payroller/main/profile' => 'Profile',
            'company/main/profile' => 'Company Profile',
            'user/main/view' => 'Profile',
            'company/main/preferences' => 'Preferences',
            'company/contact/index' => 'Contact List',
            'invoice/main/send' => 'Create New Invoice',
            'invoice/main/index' => 'Invoice Archives',
            'user/employee/*' => 'Employees',
            'Company/contact/*' => 'Contacts',
        );
        foreach ($headings as $key => $value) {
            $temp = explode('/', trim($key, '/'));
            $module = trim($temp[0]);
            $controller = trim($temp[1]);
            $action = trim($temp[2]);
            if ($currentmodule == $module) {
                if ($currentcontroller == $controller || $controller == '*') {
                    if ($currentaction == $action || $action == '*') {
                        return $value;
                    }
                }
            }
        }
        return $heading;
    }

    public static function formatID($id) {
        return strtoupper(dechex($id));
    }

    public static function getCountries() {
        return array(
            "0" => "Please select a country",
            "Afghanistan" => "Afghanistan",
            "Albania" => "Albania",
            "Algeria" => "Algeria",
            "American Samoa" => "American Samoa",
            "Andorra" => "Andorra",
            "Angola" => "Angola",
            "Anguilla" => "Anguilla",
            "Antartica" => "Antarctica",
            "Antigua and Barbuda" => "Antigua and Barbuda",
            "Argentina" => "Argentina",
            "Armenia" => "Armenia",
            "Aruba" => "Aruba",
            "Australia" => "Australia",
            "Austria" => "Austria",
            "Azerbaijan" => "Azerbaijan",
            "Bahamas" => "Bahamas",
            "Bahrain" => "Bahrain",
            "Bangladesh" => "Bangladesh",
            "Barbados" => "Barbados",
            "Belarus" => "Belarus",
            "Belgium" => "Belgium",
            "Belize" => "Belize",
            "Benin" => "Benin",
            "Bermuda" => "Bermuda",
            "Bhutan" => "Bhutan",
            "Bolivia" => "Bolivia",
            "Bosnia and Herzegowina" => "Bosnia and Herzegowina",
            "Botswana" => "Botswana",
            "Bouvet Island" => "Bouvet Island",
            "Brazil" => "Brazil",
            "British Indian Ocean Territory" => "British Indian Ocean Territory",
            "Brunei Darussalam" => "Brunei Darussalam",
            "Bulgaria" => "Bulgaria",
            "Burkina Faso" => "Burkina Faso",
            "Burundi" => "Burundi",
            "Cambodia" => "Cambodia",
            "Cameroon" => "Cameroon",
            "Canada" => "Canada",
            "Cape Verde" => "Cape Verde",
            "Cayman Islands" => "Cayman Islands",
            "Central African Republic" => "Central African Republic",
            "Chad" => "Chad",
            "Chile" => "Chile",
            "China" => "China",
            "Christmas Island" => "Christmas Island",
            "Cocos Islands" => "Cocos (Keeling) Islands",
            "Colombia" => "Colombia",
            "Comoros" => "Comoros",
            "Congo" => "Congo",
            "Congo" => "Congo, the Democratic Republic of the",
            "Cook Islands" => "Cook Islands",
            "Costa Rica" => "Costa Rica",
            "Cota D'Ivoire" => "Cote d'Ivoire",
            "Croatia" => "Croatia (Hrvatska)",
            "Cuba" => "Cuba",
            "Cyprus" => "Cyprus",
            "Czech Republic" => "Czech Republic",
            "Denmark" => "Denmark",
            "Djibouti" => "Djibouti",
            "Dominica" => "Dominica",
            "Dominican Republic" => "Dominican Republic",
            "East Timor" => "East Timor",
            "Ecuador" => "Ecuador",
            "Egypt" => "Egypt",
            "El Salvador" => "El Salvador",
            "Equatorial Guinea" => "Equatorial Guinea",
            "Eritrea" => "Eritrea",
            "Estonia" => "Estonia",
            "Ethiopia" => "Ethiopia",
            "Falkland Islands" => "Falkland Islands (Malvinas)",
            "Faroe Islands" => "Faroe Islands",
            "Fiji" => "Fiji",
            "Finland" => "Finland",
            "France" => "France",
            "France Metropolitan" => "France, Metropolitan",
            "French Guiana" => "French Guiana",
            "French Polynesia" => "French Polynesia",
            "French Southern Territories" => "French Southern Territories",
            "Gabon" => "Gabon",
            "Gambia" => "Gambia",
            "Georgia" => "Georgia",
            "Germany" => "Germany",
            "Ghana" => "Ghana",
            "Gibraltar" => "Gibraltar",
            "Greece" => "Greece",
            "Greenland" => "Greenland",
            "Grenada" => "Grenada",
            "Guadeloupe" => "Guadeloupe",
            "Guam" => "Guam",
            "Guatemala" => "Guatemala",
            "Guinea" => "Guinea",
            "Guinea-Bissau" => "Guinea-Bissau",
            "Guyana" => "Guyana",
            "Haiti" => "Haiti",
            "Heard and McDonald Islands" => "Heard and Mc Donald Islands",
            "Holy See" => "Holy See (Vatican City State)",
            "Honduras" => "Honduras",
            "Hong Kong" => "Hong Kong",
            "Hungary" => "Hungary",
            "Iceland" => "Iceland",
            "India" => "India",
            "Indonesia" => "Indonesia",
            "Iran" => "Iran (Islamic Republic of)",
            "Iraq" => "Iraq",
            "Ireland" => "Ireland",
            "Israel" => "Israel",
            "Italy" => "Italy",
            "Jamaica" => "Jamaica",
            "Japan" => "Japan",
            "Jordan" => "Jordan",
            "Kazakhstan" => "Kazakhstan",
            "Kenya" => "Kenya",
            "Kiribati" => "Kiribati",
            "Democratic People's Republic of Korea" => "Korea, Democratic People's Republic of",
            "Korea" => "Korea, Republic of",
            "Kuwait" => "Kuwait",
            "Kyrgyzstan" => "Kyrgyzstan",
            "Lao" => "Lao People's Democratic Republic",
            "Latvia" => "Latvia",
            "Lebanon" => "Lebanon",
            "Lesotho" => "Lesotho",
            "Liberia" => "Liberia",
            "Libyan Arab Jamahiriya" => "Libyan Arab Jamahiriya",
            "Liechtenstein" => "Liechtenstein",
            "Lithuania" => "Lithuania",
            "Luxembourg" => "Luxembourg",
            "Macau" => "Macau",
            "Macedonia" => "Macedonia, The Former Yugoslav Republic of",
            "Madagascar" => "Madagascar",
            "Malawi" => "Malawi",
            "Malaysia" => "Malaysia",
            "Maldives" => "Maldives",
            "Mali" => "Mali",
            "Malta" => "Malta",
            "Marshall Islands" => "Marshall Islands",
            "Martinique" => "Martinique",
            "Mauritania" => "Mauritania",
            "Mauritius" => "Mauritius",
            "Mayotte" => "Mayotte",
            "Mexico" => "Mexico",
            "Micronesia" => "Micronesia, Federated States of",
            "Moldova" => "Moldova, Republic of",
            "Monaco" => "Monaco",
            "Mongolia" => "Mongolia",
            "Montserrat" => "Montserrat",
            "Morocco" => "Morocco",
            "Mozambique" => "Mozambique",
            "Myanmar" => "Myanmar",
            "Namibia" => "Namibia",
            "Nauru" => "Nauru",
            "Nepal" => "Nepal",
            "Netherlands" => "Netherlands",
            "Netherlands Antilles" => "Netherlands Antilles",
            "New Caledonia" => "New Caledonia",
            "New Zealand" => "New Zealand",
            "Nicaragua" => "Nicaragua",
            "Niger" => "Niger",
            "Nigeria" => "Nigeria",
            "Niue" => "Niue",
            "Norfolk Island" => "Norfolk Island",
            "Northern Mariana Islands" => "Northern Mariana Islands",
            "Norway" => "Norway",
            "Oman" => "Oman",
            "Pakistan" => "Pakistan",
            "Palau" => "Palau",
            "Panama" => "Panama",
            "Papua New Guinea" => "Papua New Guinea",
            "Paraguay" => "Paraguay",
            "Peru" => "Peru",
            "Philippines" => "Philippines",
            "Pitcairn" => "Pitcairn",
            "Poland" => "Poland",
            "Portugal" => "Portugal",
            "Puerto Rico" => "Puerto Rico",
            "Qatar" => "Qatar",
            "Reunion" => "Reunion",
            "Romania" => "Romania",
            "Russia" => "Russian Federation",
            "Rwanda" => "Rwanda",
            "Saint Kitts and Nevis" => "Saint Kitts and Nevis",
            "Saint LUCIA" => "Saint LUCIA",
            "Saint Vincent" => "Saint Vincent and the Grenadines",
            "Samoa" => "Samoa",
            "San Marino" => "San Marino",
            "Sao Tome and Principe" => "Sao Tome and Principe",
            "Saudi Arabia" => "Saudi Arabia",
            "Senegal" => "Senegal",
            "Seychelles" => "Seychelles",
            "Sierra" => "Sierra Leone",
            "Singapore" => "Singapore",
            "Slovakia" => "Slovakia (Slovak Republic)",
            "Slovenia" => "Slovenia",
            "Solomon Islands" => "Solomon Islands",
            "Somalia" => "Somalia",
            "South Africa" => "South Africa",
            "South Georgia" => "South Georgia and the South Sandwich Islands",
            "Span" => "Spain",
            "SriLanka" => "Sri Lanka",
            "St. Helena" => "St. Helena",
            "St. Pierre and Miguelon" => "St. Pierre and Miquelon",
            "Sudan" => "Sudan",
            "Suriname" => "Suriname",
            "Svalbard" => "Svalbard and Jan Mayen Islands",
            "Swaziland" => "Swaziland",
            "Sweden" => "Sweden",
            "Switzerland" => "Switzerland",
            "Syria" => "Syrian Arab Republic",
            "Taiwan" => "Taiwan, Province of China",
            "Tajikistan" => "Tajikistan",
            "Tanzania" => "Tanzania, United Republic of",
            "Thailand" => "Thailand",
            "Togo" => "Togo",
            "Tokelau" => "Tokelau",
            "Tonga" => "Tonga",
            "Trinidad and Tobago" => "Trinidad and Tobago",
            "Tunisia" => "Tunisia",
            "Turkey" => "Turkey",
            "Turkmenistan" => "Turkmenistan",
            "Turks and Caicos" => "Turks and Caicos Islands",
            "Tuvalu" => "Tuvalu",
            "Uganda" => "Uganda",
            "Ukraine" => "Ukraine",
            "United Arab Emirates" => "United Arab Emirates",
            "United Kingdom" => "United Kingdom",
            "United States" => "United States",
            "United States Minor Outlying Islands" => "United States Minor Outlying Islands",
            "Uruguay" => "Uruguay",
            "Uzbekistan" => "Uzbekistan",
            "Vanuatu" => "Vanuatu",
            "Venezuela" => "Venezuela",
            "Vietnam" => "Viet Nam",
            "Virgin Islands (British)" => "Virgin Islands (British)",
            "Virgin Islands (U.S)" => "Virgin Islands (U.S.)",
            "Wallis and Futana Islands" => "Wallis and Futuna Islands",
            "Western Sahara" => "Western Sahara",
            "Yemen" => "Yemen",
            "Yugoslavia" => "Yugoslavia",
            "Zambia" => "Zambia",
            "Zimbabwe" => "Zimbabwe",
        );
    }

    public static function getCustomHeading($route) {
        switch ($route) {
            case "user/main":
                echo "Employee";
                break;
            case "user/employee":
                echo "Employee";
                break;
            case "site":
                echo "Dashboard";
                break;
            case "timesheet/main":
                echo "Add New";
                break;
            case "contract/main";
                echo "Account";
                break;
        }
    }

}

?>
