<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class WebUser extends CWebUser {

    /**
     * Overrides a Yii method that is used for roles in controllers (accessRules).
     *
     * @param string $operation Name of the operation required (here, a role).
     * @param mixed $params (opt) Parameters for this operation, usually the object to access.
     * @return bool Permission granted?
     */
    public function checkAccess($operation, $params = array(), $allowCaching = true) {
        if ($operation == '*') {
            // allow access to everyone
            return true;
        }

        if (!AppUser::isUserLogin()) {
            Yii::app()->request->redirect(Yii::app()->createUrl('user/main/login'));
        }

        if (empty($this->id)) {
            // Not identified => no rights
            return false;
        }

        $role = $this->getState("roles");
        // allow access if the operation request is the current user's role
        if($role == "super_admin"){
        return true;}
        else if($role == "payroller")
        {
            return true;
        }
        else{
            $group = Yii::app()->getRequest()->queryString;
            $access = self::canAccess($group);
            if($access["access"])
            {
                return true;
            }
            else
            {
                Yii::app()->user->setFlash('error', $access["message"]);
            }
//        return ($operation === $role);
    }
    }

    public static function canAccess($param = null) {
        $company = AppUser::getUserCompany();
        $company_id = 0;
        if($company){
            $company_id = $company->id;
        }
        $user = AppUser::getCurrentUser();
        $cms = new Cms();
        $accessibility = array("access"=>0,"message"=>"");
        $currentmodule = Yii::app()->controller->module->id;
        $currentcontroller = Yii::app()->controller->id;
        $currentaction = Yii::app()->controller->action->id;
        $currentcontroller .= "Controller.php";
        $privilege = array();
        $function_list = FunctionList::model()->findAllByAttributes(array("module"=>$currentmodule,"controller"=>$currentcontroller,"action"=>$currentaction));
//        dd($currentmodule." ".$currentcontroller." ".$currentaction);
//        dd($function_list);
//        dd($param);
        foreach ($function_list as $item) {
            if($param){
                $data = PrivilegeGroups::model()->findByAttributes(array("group"=>$param));
                if($data){
                $privilege = Priveledge::model()->findByAttributes(array("function_list_id"=>$item->id,"group"=>$param));}
                else{
                    $privilege = Priveledge::model()->findByAttributes(array("function_list_id"=>$item->id));
                }
            }
            else
            {
                $privilege = Priveledge::model()->findByAttributes(array("function_list_id"=>$item->id));
            }
            if(count($privilege)>0){
                break;
            }
        }
//        dd($privilege);
        if(count($privilege)>0){
        $company_privileges = CompanyPriveledge::model()->findAllByAttributes(array("priveledge_id" => $privilege->id, "company_id" => $company_id,'status'=>'active'));
//        dd($company_privileges);
        if (count($company_privileges) > 0) {
//            dd($function_list[0]->module." ".lcfirst($function_list[0]->controller)." ".  strtolower($function_list[0]->action)." ".$currentmodule." ".$currentcontroller." ".$currentaction);
            if ($currentmodule == lcfirst($function_list[0]->module) && $currentcontroller == lcfirst($function_list[0]->controller) && strtolower($currentaction) == strtolower($function_list[0]->action)) {
                if ($method = $privilege->function) {
                    $accessibility = PrivilegeComponent::$method($company_privileges[0]);
                } 
                else {
                    $accessibility = PrivilegeComponent::privilegeWithoutFunction($company_privileges[0]);
                }
            }
            return $accessibility;
        } else {
            $temp = $cms->getCMSContentBySlug("privilege_access");
            $accessibility["access"] = 0;
            $accessibility["message"] = $temp['content'];
            return $accessibility;
        }
        }
        else{
            $temp = $cms->getCMSContentBySlug("privilege_access");
            $accessibility["access"] = 0;
            $accessibility["message"] = $temp['content'];
            return $accessibility;
        }
    }

}

?>