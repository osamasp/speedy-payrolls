<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    public $_id;
    public $full_name;
    const ERROR_USER_NOT_VERIFIED= 3;

    /**
     * Overrided parent method
     * @return type 
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * Authenticate user
     * @return type 
     */
    public function authenticate() {
        $user = User::prepareUserForAuthorisation($this->username);
        
        if ($user === NULL) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } else if (!$user->ValidatePassword($this->password)) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else {
            if(!$user->is_verified){
                Yii::app()->user->setState('verify_email' , $user->email);                
                $this->errorCode = self::ERROR_USER_NOT_VERIFIED;
            }
            else{
                if(!AppUser::isUserSuperAdmin($user->id)){
                    if(!AppUser::isUserPayroller($user->id)){
                   }
                }
                $this->_id = $user->id; 
                $this->setState('id', $user->id);
                $this->setState('first_name', $user->first_name);
                $this->setState('last_name', $user->last_name);
                $this->setState('email', $user->email);
                if(AppUser::isUserSuperAdmin()){
                    $this->setState('roles','super_admin');
                }
                else{
                    $this->setState('roles',AppUser::getUserType($user->id));
                }
                $this->errorCode = self::ERROR_NONE;
            }         
        }

        return !$this->errorCode;
    }

    public static function userLoginStatus() {

        if (Yii::app()->user->isGuest)
            return false;
        else
            return true;
    }

}
