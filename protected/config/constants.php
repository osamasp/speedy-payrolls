<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/** HTTP STATUS CODE **/
define('HTTPCODE_SUCCESS',200);
define('HTTPCODE_CREATED',201);
define('HTTPCODE_FAILURE',400);
define('HTTPCODE_UNAUTHORIZED',401);
define('HTTPCODE_NOT_FOUND',404);
define('HTTPCODE_SERVER_ERROR',500);
/***/

/** HTTP METHOD **/
define('HTTPMETHOD_POST','POST');
define('HTTPMETHOD_GET','GET');
define('HTTPMETHOD_DELETE','DELETE');
define('HTTPMETHOD_PUT','PUT');
/***/

/** FLAGS **/
define('FLAG_USER_UNVERIFIED',0);
define('COMPANY_STATUS', 1);
define('COMPANY_TYPE', 1);


/** W/S Respons TYPE **/
define('RESPONSE_TYPE_JSON','json');
define('RESPONSE_TYPE_XML','xml');


/** Speedy Payrolls System Constants **/
define('SYSTEM_NAME', 'Speedypayrolls Ltd.');
define('USER_DEVICE_IOS', 'ios');
define('USER_DEVICE_ANDROID', 'android');

/** Timesheet Constants **/
define('TIMESHEET_TYPE_CURRENT', 'current');
define('TIMESHEET_TYPE_COMPLETED', 'completed');
define('DISPATCH_FLAG_OFF', 0);
define('DISPATCH_FLAG_ON', 1);
define('TIMESHEET_TYPE_OUT', 'out');
define('TIMESHEET_TYPE_IN', 'in');
define('TIMESHEET_TYPE_IN_OUT', 'in_out');
define('TIMESHEET_TYPE_OUTB', 'out_b');
define('TIMESHEET_TYPE_INTERNAL', 'internal');
define('TIMESHEET_TYPE_CUSTOM', 'custom');
define('TIMESHEET_TYPE_DAILY', 'daily');
define('TIMESHEET_STATUS_PENDING', 'pending');
define('TIMESHEET_STATUS_APPROVED', 'approved');
define('TIMESHEET_STATUS_DISAPPROVED', 'disapproved');
define('TWO_WAY', '2 Way');
define('THREE_WAY', '3 Way');
define('NON_BILLABLE', 'Non Billable');
define('APPROVER_IN_COMPANY', 'in_company');

/** USER CONSTANTS **/
define('EMPLOYEE_TYPE_EMPLOYEE','employee');
define('EMPLOYEE_TYPE_ADMIN','admin');
define('EMPLOYEE_TYPE_DIRECTOR','director');
define('EMPLOYEE_TYPE_INDIVIDUAL','individual');
define('EMPLOYEE_TYPE_FULL_TIME','full_time');
define('USER_TYPE_EMPLOYEE',2);
define('USER_TYPE_ADMIN',1);
define('PAYROLLER_SIGNUP_MESSAGE', 'A new signup of %s with email address: %s is pending.');

/** CLOCK IN/OUT CONSTANTS **/
define('CLOCK_ENTRY_LIST','clock_entries');
define('CLOCK_APPROVAL_LIST','approval_list');
define('CLOCK_IN','in');
define('LUNCH_IN','l_in');
define('LUNCH_OUT','l_out');
define('CLOCK_OUT','out');
define('NOTE','note');
define('REPORT', 'report');

define('FLAG_ON', 1);
define('FLAG_OFF', 0);
define('FLAG', 0);
define('FROM_API', true);
define('SYSTEM_ADMIN', 0);
define('SUPER_ADMIN', 1);
define('CONTRACT_PATH', realpath(Yii::app()->basePath . '/../uploads/contracts'));
define('PAYSLIP_PATH', realPath(Yii::app()->basePath.'/../uploads/payslips/'));
define('DEFAULT_PRIVILEGE_ID', 29450400);

//define('smtp_host_name', 'smtp.gmail.com');
//define('smtp_user_name', 'noreply.speedy@gmail.com');
//define('smtp_password', 'H3ll0123');
define('smtp_host_name', 'email-smtp.us-east-1.amazonaws.com');
define('smtp_user_name', 'AKIAJRNO5SBRXB2AQPPA');
define('smtp_password', 'AsNahIqOoqVdExP+oCk4aNhmoDdWfM75ae/pCXRnHY58');

define('smtp_secure', 'tls');
define('system_name', "Speedypayrolls Ltd.");
define('facebook', 'facebook');
define('linkedin', 'linkedin');
