<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

include_once 'config_settings.php';
include_once 'constants.php';
function dd($obj) {
    echo "<pre>";
    print_r($obj);
    echo "<pre>";
    die();
}

function d($obj) {
    echo "<pre>";
    print_r($obj);
    echo "<pre>";
}

function get_placing_string($placing) {
    $i = intval($placing % 10);
    $place = substr($placing, -2); //For 11,12,13 places

    if ($i == 1 && $place != '11') {
        return $placing . 'st';
    } else if ($i == 2 && $place != '12') {
        return $placing . 'nd';
    } else if ($i == 3 && $place != '13') {
        return $placing . 'rd';
    }
    return $placing . 'th';
}
$db_name = config_settings::$db_name;
$db_user = config_settings::$db_user;
$db_password = config_settings::$db_password;
$db_host = config_settings::$db_host;



//if ($_SERVER['SERVER_NAME'] == "localhost") {
//    $db_password = '';
//    $db_user = 'root';
//    $db_name = "speedy_payroll";
//} else if (strpos($_SERVER['REQUEST_URI'], 'speedy-stage')) {
//    $db_password = 'k~pbpw!uU[p!';
//    $db_user = 'vistecho_speedy';
//    $db_name = "vistecho_speedyStage";
//
//    ini_set('display_errors', 0);
//    error_reporting(~E_ALL & ~E_NOTICE);
//} else if (strpos($_SERVER['REQUEST_URI'], 'speedyalpha')) {
//    $db_password = 'k~pbpw!uU[p!';
//    $db_user = 'vistecho_speedy';
//    $db_name = "vistecho_speedy_alpha";
//
// //   ini_set('display_errors', 0);
// //   error_reporting(~E_ALL & ~E_NOTICE);
//}else if ($_SERVER['HTTP_HOST']=="dev.siliconplex.com") {
//    $db_password = 'uaqbxb';
//    $db_user = 'dev_speedy';
//    $db_name = "dev_speedy";
//}
//else if (strpos($_SERVER['REQUEST_URI'], 'sptest')) {
//    $db_password = 'bitnami';
//    $db_user = 'root';
//    $db_name = "speedy_payrolltest";
//    ini_set('display_errors', 0);
//    error_reporting(~E_ALL & ~E_NOTICE);
//}
//else if (strpos($_SERVER['REQUEST_URI'], 'met')) {
//    $db_password = 'bitnami';
//    $db_user = 'root'; 
//    $db_name = "speedy_payrollmet";
//    ini_set('display_errors', 0);
//    error_reporting(~E_ALL & ~E_NOTICE);
//}
//else {
//    $db_password = 'bitnami';
//    $db_user = 'root';
//    $db_name = "speedy_payroll";
//    ini_set('display_errors', 0);
//    error_reporting(~E_ALL & ~E_NOTICE);
//}

$docRoot = str_replace('index.php', '', $_SERVER['SCRIPT_FILENAME']);
date_default_timezone_set("Europe/London");

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Speedy Payrolls',
    // preloading 'log' component
    'preload' => array('log'),
    'theme' => 'speedypayroll',
    'timeZone' => 'Europe/London',
    // autoloading model and component classes
    'import' => array(
        'ext.ESES.*',
        'application.models.*',
        'application.controllers.*',
        'application.components.*',
        'application.extensions.*',
        'application.extensions.fpdf.*',
        'ext.yii-pdf.*',
        'application.extensions.mpdf.*',
        'application.extensions.html2pdf.*',
        'application.modules.user.models.*',
        'application.modules.user.components.*',
        'application.modules.notification.*',
        'application.modules.contract.models.*',
        'application.modules.contract.components.*',
        'application.modules.invoice.models.*',
        'application.modules.invoice.components.*',
        'application.modules.importcsv.models.*',
        'application.modules.importcsv.components.*',
        'application.modules.payroller.models.*',
        'application.modules.payroller.components.*',
        'application.modules.company.models.*',
        'application.modules.company.components.*',
        'application.modules.timesheet.models.*',
        'application.modules.timesheet.components.*',
        'application.modules.template.models.*',
        'application.modules.template.components.*',
        'application.modules.billing.models.*',
        'application.modules.billing.components.*',
        'application.modules.help.models.*',
        'application.modules.help.components.*',
        'application.modules.priveledge.models.*',
        'application.modules.user.controllers.*',
        'application.modules.contract.controllers.*',
        'application.modules.contact.controllers.*',
        'application.modules.admin.controllers.*',
        'application.modules.billing.controllers.*',
        'application.modules.company.controllers.*',
        'application.modules.help.controllers.*',
        'application.modules.importcsv.controllers.*',
        'application.modules.invoice.controllers.*',
        'application.modules.payroller.controllers.*',
        'application.modules.priveledge.controllers.*',
        'application.modules.template.controllers.*',
        'application.modules.timesheet.controllers.*',
        'application.modules.priveledge.components.*',
        'application.modules.notification.extensions.*',
        'application.modules.notification.models.*',
        'application.modules.notification.components.*',
        
    ),
    'modules' => array(
        'user',
        'company',
        'contract',
        'timesheet',
        'invoice',
         'notification'=>array(
            'class'=>'application.modules.notification.NotificationModule',
//            'childs'=> array('ParseNotify','MailerNotify','PushNotify'),
            'childs'=> array('MailerNotify','PushNotify'),
            'components'=>array(
                'ParseNotify' => array(
                    'class' => 'ParseNotify',
                    'appId' => '',
                    'rest_key' => '',
                    'master_key' => '',
                ),
                'MailerNotify' => array(
                    'class' => 'MailerNotify',
                    'smtp' => array(
                        'host'=> 'smtp.mandrillapp.com',
                        'port'=> '587',
                        'username'=>'',
                        'pwd'=>''
                    )
                ),
                'PushNotify' => array(
                    'class' => 'PushNotify',
                    'host' => config_settings::$node_host,
                    'port' => config_settings::$node_port,
                    'host2' => config_settings::$node_host_2,
                ),

            ),
        ),
        'payroller',
        'importcsv',
        'admin',
        'template',
        'billing',
        'help',
        'priveledge',
        // uncomment the following to enable the Gii tool
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'test12',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
        ),
    ),
    // application components
    'components' => array(
      'metadata'=>array('class'=>'Metadata'),
        'ses'=>array(
        'class'=>'ext.ESES.ESES',
        'access_key'=>'AKIAJU76VTJTA2I6L36A',
        'secret_key'=>'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',
        'host'=>'email.us-east-1.amazonaws.com',
        ),
        'ePdf' => array(
        'class'         => 'ext.yii-pdf.EYiiPdf',
        'params'        => array(
            'mpdf'     => array(
                'librarySourcePath' => 'application.extensions.mpdf.*',
                'constants'         => array(
                    '_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.runtime'),
                ),
                'class'=>'mpdf',),
            'HTML2PDF' => array(
                'librarySourcePath' => 'application.extensions.html2pdf.*',
                'classFile'         => 'html2pdf.class.php',)
            ),
            ),
        'user' => array(
            // enable cookie-based authentication
            'allowAutoLogin' => false,
            'loginUrl' => array('/user/main/login'),
            'class' => 'application.components.WebUser',
        ),
        // uncomment the following to enable URLs in path-format
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
//		'db'=>array(
//			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
//		),
        // uncomment the following to use a MySQL database
        'db' => array(
            'connectionString' => 'mysql:host='.$db_host.';dbname=' . $db_name,
            'emulatePrepare' => true,
            'username' => $db_user,
            'password' => $db_password,
            'charset' => 'utf8',
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CDbLogRoute',
                    'connectionID' => 'db',
//                    'autoCreateLogTable'=>true,
                    'levels' => 'error, warning, info',
                    'filter' => 'CLogFilter',
                    'logTableName' => 'sp_log'
                ),
            // uncomment the following to show log messages on web pages
            /*
              array(
              'class'=>'CWebLogRoute',
              ),
             */
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'webmaster@example.com',
        'node_host' => sprintf('%s:%s',config_settings::$node_host,config_settings::$node_port),
        'pagination' => array(
            'tsheetSize' => 15,
            'payrollSize' => 15
        ),
        'uploads' => array(
            'payslip' => $docRoot . 'uploads/payslips/',
            'defaultPaySlip' => array('path' => $docRoot . 'uploads/', 'name' => 'default-payslip.csv')
        )
    ),
);
?>
