<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.

include_once 'config_settings.php';

$db_name = config_settings::$db_name;
$db_user = config_settings::$db_user;
$db_password = config_settings::$db_password;

	
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Console Application',

	// preloading 'log' component
	'preload'=>array('log'),

	// application components
	'components'=>array(
/*		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),*/
		// uncomment the following to use a MySQL database
		
		'db'=>array(
			  'connectionString' => 'mysql:host=localhost;dbname=' . $db_name,
            'emulatePrepare' => true,
            'username' => $db_user,
            'password' => $db_password,
            'charset' => 'utf8',
		),
		
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
	),
);